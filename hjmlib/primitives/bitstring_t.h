/*
 * bitstring_t.h
 */


#ifndef BITSTRING_T_H
#define BITSTRING_T_H


/**
 * A string of bits. Each bit can be addressed to be set or cleared.
 *
 * @author Hj.Malthaner
 * @date 04-May-02
 * @update 04-May-02
 */
class bitstring_t
{
 private:

  int bitlen;
  int bytelen;

  unsigned char * buf;

 public:


  /**
   * Creates a new bitstring.
   *
   * @param len number of bits to allocate
   * @author Hj.Malthaner
   */
  bitstring_t(int len);


  /**
   * Destructor
   * @author Hj. Malthaner
   */
  ~bitstring_t();


  /**
   * Sets all bits
   * @author Hj. Malthaner
   */
  void set();


  /**
   * Unsets all bits
   * @author Hj. Malthaner
   */
  void clr();


  /**
   * Sets a bit
   * @author Hj. Malthaner
   */
  inline void set_bit(int b) {
    buf[b >> 3] |= 1 << (b & 7);
  }


  /**
   * Clears a bit
   * @author Hj. Malthaner
   */
  inline void clr_bit(int b) {
    buf[b >> 3] &= ~(1 << (b & 7));
  }
};

#endif // BITSTRING_T_H
