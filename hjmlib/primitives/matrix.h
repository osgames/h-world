/* 
 * matrix.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef MATRIX_H
#define MATRIX_H

#include "koord.h"

class matrix {
public:

  int a;
  int b;
  int c;
  int d;

private:

    /** @link dependency 
     * @stereotype access*/
    /*#  koord lnkkoord; */

public:

  matrix() {a=b=c=d=0;};
  matrix(int aa, int bb, int cc, int dd) {
    a = aa;
    b = bb;
    c = cc;
    d = dd;
  };

  koord operator* (const koord & k) const {
    return koord(a*k.x + b*k.y, c*k.x + d*k.y);
  } 
};


#endif //MATRIX_H
