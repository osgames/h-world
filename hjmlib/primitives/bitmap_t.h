#ifndef bitmap_t_h
#define bitmap_t_h

/**
 * A class for a 2 dimensional bitmap.
 *
 * @author Hj. Malthaner
 * @date 16-Aug-03
 * @update 16-Aug-03
 */
class bitmap_t 
{
private:
  unsigned int width;
  unsigned int height;
  unsigned int bpr;
  unsigned int *field;
  
  void copy_to(bitmap_t *) const;

public:

  bitmap_t(unsigned int width, unsigned int height);
  ~bitmap_t();

  void set_map(void);
  void clr_map(void);

  inline void clr_bit(unsigned int x, unsigned int y) { 
    field[(y*bpr) + (x >> 5)] &= ~(1 << (x & 31)); 
  }

  inline void set_bit(unsigned int x, unsigned int y) {
    field[(y*bpr) + (x >> 5)] |= (1 << (x & 31));
  }    

  inline bool get_bit(unsigned int x,unsigned int y) const {
    return (field[(y*bpr) + (x >> 5)] >> (x & 31)) & 1;
  }    

  void operator= (const bitmap_t & other);
};     

#endif
