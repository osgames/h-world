#ifndef invalid_argument_exception_t_h
#define invalid_argument_exception_t_h


/**
 * Exception class for hadnling invalid arguments
 * @author Hj. Malthaner
 */
class invalid_argument_exception_t
{
 public:

  /**
   * Problem description, preferred to be handled read only
   * @author Hj. Malthaner
   */
  char reason [1024]; 

  invalid_argument_exception_t() {reason[0] = '\0';};

  invalid_argument_exception_t(const char * reason);
};


#endif
