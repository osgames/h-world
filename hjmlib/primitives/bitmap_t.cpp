#include <string.h>
#include <string.h>

#include "bitmap_t.h"
#include "invalid_argument_exception_t.h"


bitmap_t::bitmap_t(unsigned int width, unsigned int height)
{
  this->width = width;            
  this->height = height;
  this->bpr   = (width + 31)/32;
  this->field = new unsigned int[bpr*height]; 
  
  if(sizeof(unsigned int) != 4) {
    invalid_argument_exception_t ex;
    strcpy(ex.reason, "bitmap_t needs sizeof(unsigned int) == 4.");
    throw ex;
  }
}


bitmap_t::~bitmap_t(void)
{
  delete [] field;
  field = 0;
}


void bitmap_t::copy_to(bitmap_t *dest) const
{
  dest->width =  width;
  dest->height = height;
  dest->bpr   =  bpr;
  
  delete [] dest->field;
  dest->field = new unsigned int[bpr*height]; 
  
  memcpy(dest->field, field, bpr*height*4); 
}


void bitmap_t::clr_map(void)
{
  memset(field, 0, bpr*height*4);
}


void bitmap_t::set_map(void)
{
  memset(field, 255, bpr*height*4);
}


void bitmap_t::operator= (const bitmap_t & other)
{
  other.copy_to(this);
}
