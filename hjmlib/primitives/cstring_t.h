
#ifndef CSTRING_T_H
#define CSTRING_T_H


/**
 * A simple string class.
 * @author Hj. Malthaner 
 * @date 12-Jan-2002
 */
class cstring_t 
{
private:
    /**
     * The buffer may never be null for a living cstring_t. 
     * It is initialized in every constructor
     * @author Hj. Malthaner
     */
    char *buf;

        
public:


    /**
     * Builds an empty string
     * @author Hj. Malthaner
     */
    cstring_t();
                

    /**
     * Builds a string as a copy of a char array
     * @author Hj. Malthaner
     */
    cstring_t(const char *other);


    /**
     * Builds a string as a copy of a string
     * @author Hj. Malthaner
     */
    cstring_t(const cstring_t &other);

                         
    ~cstring_t();


    /**
     * Concatenates this string and a char array
     * @author Hj. Malthaner
     */
    cstring_t operator+ (const char *) const;


    /**
     * Concatenates this string and a char array
     * @author Hj. Malthaner
     */
    void operator+= (const char *);
                            

    /**
     * Appends an integer to this string
     * @author Hj. Malthaner
     */
    cstring_t operator+ (long v) const;


    /**
     * Assignement operator
     * @author Hj. Malthaner
     */
    cstring_t & operator= (const cstring_t &);


    /**
     * Comparison operator
     * @author Hj. Malthaner
     */
    bool operator== (const cstring_t &) const;


    /**
     * Comparison operator
     * @author Hj. Malthaner
     */
    bool operator== (const char *) const;


    bool operator!= (const cstring_t &) const;


    bool operator!= (const char *) const;


    /**
     * Automagic conversion to a const char* for backwards compatibility
     * @author Hj. Malthaner
     */       
    operator const char *() const;


    /**
     * Varargs like printf(...)  don't work with automagic conversion
     * so we need a explict conversion method
     * @author Hj. Malthaner
     */       
    const char * chars() const {return buf;};

            
    /**
     * @return Number of characters in this string
     * @author Hj. Malthaner
     */
    unsigned int len() const; 


    /**
     * Substring operator
     * @param first first char to include
     * @param last position after last char to include
     * @author Hj. Malthaner
     */
    cstring_t substr(unsigned int first, unsigned int last);


    /**
     * @return rightmost n characters
     */
    cstring_t right(unsigned int newlen)
    {
	unsigned int oldlen = len();
	
	if(newlen > oldlen)
	    return *this;
	else
	    return substr(oldlen - newlen, oldlen);
    }


    /**
     * Find character backwards
     * @return index of character or -1 if not found
     */
    int find_back(char c) const;


    /**
     * Bounds checked, fairly slow. 
     *
     * @author Hj. Malthaner
     */
    char char_at(unsigned int i) const;
};


/**
 * Concatenates a char array and a string
 * @author Hj. Malthaner
 */
//cstring_t operator+ (const char *, const cstring_t &);


#endif
