/* Copyright by Hj. Malthaner */

#ifndef AREA_T_H
#define AREA_T_H


#include "koord.h"

/**
 * Area descriptor.
 *
 * @author Hansj�rg Malthaner
 */
class area_t {
 public:

  /**
   * Top left position
   * @author Hansj�rg Malthaner
   */
  koord tl;


  /**
   * Bottom right position
   * @author Hansj�rg Malthaner
   */
  koord br;

  area_t() {};

  area_t(koord k1, koord k2) {tl=k1; br=k2;};
  

  int get_width() const {return br.x - tl.x + 1;};
  int get_height() const {return br.y - tl.y + 1;};

};


/* To iterate all coordinates of a area use this:
 * <p>
 * a.begin();
 * while(a.next()) {
 *   koord k = a.get_current();
 * }
 * </p> 
 * @author Hansj�rg Malthaner
 */
class area_iterator_t
{
 private:

  koord k;
  const area_t * a;

 public:
  void begin() {
    k = a->tl + koord(-1,0);
  };

  bool next() {
    k.x ++;

    // Line done?
    if(k.x > a->br.x) {
      k.x = a->tl.x;
      k.y ++;
    }

    return k.y <= a->br.y;
  }

  const koord & get_current() const {return k;};


  area_iterator_t(const area_t & a) {this->a = &a; begin();};
  area_iterator_t(const area_t * a) {this->a = a; begin();};

};



/* To iterate all coordinates of a area boundray use this:
 * <p>
 * a.begin();
 * while(a.next()) {
 *   koord k = a.get_current();
 * }
 * </p> 
 * @author Hansj�rg Malthaner
 */
class boundary_iterator_t
{
 private:

  koord k;
  const area_t * a;

 public:
  void begin();

  bool next();

  const koord & get_current() const {return k;};


  boundary_iterator_t(const area_t & a) {this->a = &a; begin();};
  boundary_iterator_t(const area_t * a) {this->a = a; begin();};

};

#endif //AREA_T_H
