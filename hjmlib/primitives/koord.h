/*
 * Copyright (c) 2001 Hansj�rg Malthaner
 *
 * Usage for Iso-Angband is granted. Usage in other projects is not
 * permitted without the agreement of the author.
 */

#ifndef koord_h
#define koord_h
               
class mempool_t;
 
/**
 * 2d koordinaten
 *
 * @author Hj. Malthaner
 */
class koord
{
private:

public:
    short x; short y;

    koord() {x=y=0;};
    koord(short xp, short yp) {x=xp; y=yp;};

    /**
     * approximate distance (fast, but less precise than pythagoras)
     * @author Hj. Malthaner
     */
    int apx_distance_to(const koord k) const;


    inline bool operator== (const koord & k) const {
      //return (this->x == k.x && this->y == k.y);

      // Hajo: dirty trick to speed things up
      return *((const int*)this) == *((const int*)&k);
    };


    inline bool operator!= (const koord & k) const
    {
      // return (a.x != b.x) || (a.y != b.y);

      // Hajo: dirty trick to speed things up
      return *((const int*)this) != *((const int*)&k);
    } 


    inline koord operator* (const int m) const {
      return koord(x*m, y*m);
    };

    inline koord operator*= (const int m) {
      x *= m;
      y *= m;
      return *this;
    };

    inline koord operator/ (const int m) const {
      return koord(x/m, y/m);
    };

    inline koord operator/= (const int m) {
      x /= m;
      y /= m;
      return *this;
    };


    inline bool operator> (const koord k) const {
      return k.x > x && k.y > y;
    };


    inline bool operator< (const koord k) const {
      return k.x < x && k.y < y;
    };

    
    /**
     * sign values for every component (-1, 0, 1)
     * @author Hj. Malthaner
     */
    inline koord sign() {
      return koord(x > 0 ? 1 : x == 0 ? 0 : -1, y > 0 ? 1 : y == 0 ? 0 : -1);
    };


    /**
     * Rotate a vector by 45 degress right, component range must be -1..1.
     * @param octants number of octants to turn, can be negative
     * @return the roteted vector
     * @author Hj. Malthaner
     */
    const koord & rotate(int octants) const;
};


inline koord operator+ (const koord & a, const koord & b)
{
    return koord(a.x+b.x, a.y+b.y);
} 


inline koord operator- (const koord & a, const koord & b)
{
    return koord(a.x-b.x, a.y-b.y);
} 


inline const koord& operator+= (koord & a, const koord & b)
{
    a.x+=b.x;
    a.y+=b.y;
    return a;
} 


inline const koord& operator-= (koord & a, const koord & b)
{
    a.x-=b.x;
    a.y-=b.y;
    return a;
} 

inline koord operator- (const koord & a)
{
    return koord(-a.x, -a.y);
} 


#endif  
