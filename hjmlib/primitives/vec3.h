/* 
 * vec3.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef vec3_h
#define vec3_h

template <class T> class vec3
{
 private:

 public:

  /**
   * Element values
   * 
   * @author Hj. Malthaner
   */
  T x,y,z;


  vec3() : x(0), y(0), z(0) {};

  vec3(T a, T b, T c) : x(a), y(b), z(c) {};


  void operator+= (const vec3<T> &other) {
    x += other.x;
    y += other.y;
    z += other.z;
  } 


  vec3 <T> operator+ (const vec3<T> &other) const {
    vec3 <T> result(*this); 
    result.x += other.x;
    result.y += other.y;
    result.z += other.z;

    return result;
  } 


  vec3 <T> operator- (const vec3<T> &other) const {
    vec3 <T> result(*this); 
    result.x -= other.x;
    result.y -= other.y;
    result.z -= other.z;

    return result;
  } 


  /**
   * @returns length^2
   * @author Hj. Malthaner
   */
  T length2() {
    return x*x + y*y + z*z;
  }

};

#endif // vec3_h
