/* Copyright by Hj. Malthaner */

#include "koord.h"
#include "invalid_argument_exception_t.h"

static const koord octants[8] =
{
  koord( 1, 0),
  koord( 1, 1),
  koord( 0, 1),
  koord(-1, 1),

  koord(-1, 0),
  koord(-1,-1),
  koord( 0,-1),
  koord( 1,-1)
};



int koord::apx_distance_to(const koord k) const
{
  /* Find the absolute y/x distance components */
  const int dy = (y > k.y) ? (y - k.y) : (k.y - y);
  const int dx = (x > k.x) ? (x - k.x) : (k.x - x);
  
  /* approximate the distance */
  const int d = (dy > dx) ? (dy + (dx>>1)) : (dx + (dy>>1));
  
  /* Return the distance */
  return d;
}


/**
 * Rotate a vector by 45 degress right, component range must be -1..1.
 * @param octants number of octants to turn, can be negative
 * @return the roteted vector
 * @author Hj. Malthaner
 */
const koord & koord::rotate(int turns) const
{
  int oct = -1;

  do {
    oct ++;
  } while(oct < 8 && octants[oct] != *this);


  if(oct < 8) {
    
    oct += turns;

    if(oct < 0) {
      oct = 8-oct;
    }

    if(oct > 7) {
      oct = oct-8;
    }

  } else {
    throw invalid_argument_exception_t("koord::rotate(): component of *this is not i range -1..1");
  }

  return octants[oct];
}
