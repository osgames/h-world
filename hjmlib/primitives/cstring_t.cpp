#include <stdio.h>

#include <string.h>
#include "cstring_t.h"

// ------------- global operators --------------------


/**
 * Concatenates a char array and a string
 * @author Hj. Malthaner
 */

/*
cstring_t operator+ (const char *buf, const cstring_t &other)
{
    const int len = strlen(buf) + other.len() + 1;

    char *tmp = new char[len]; 

    strcpy(tmp, buf);
    strcat(tmp, other);
           
    cstring_t result (tmp);
    delete tmp;

    return result;
  
}
*/


/*
 * We want to avoid allocation of memory for empty strings.
 * Thus all empty strings share this memory area.
 * @author Hj. Malthaner
 */
static char cs_empty_string = '\0';


/*
 * Allocate "size" characters.
 * Empty strings (size == 0) use shared memory.
 * @author Hj. Malthaner
 */
static char * cs_alloc(int size)
{
  char * result = &cs_empty_string;

  if(size > 0) {
    result = new char [size];
  }

  return result;
}


/*
 * Free memory. Handle shared memory of empty strings
 * @author Hj. Malthaner
 */
static void cs_free(char * buf)
{
  if(buf != &cs_empty_string) {
    delete [] buf;
  }
}


// ------------- cstring class --------------------


/**
 * Builds an empty string
 * @author Hj. Malthaner
 */
cstring_t::cstring_t()
{                     
  //printf("cstring_t::cstring_t()\n");

  buf = cs_alloc(0);
}
                

/**
 * Builds a string as a copy of a char array
 * @author Hj. Malthaner
 */
cstring_t::cstring_t(const char *other)
{                                      
  //printf("cstring_t::cstring_t(const char *other)\n");

  if(other) {
    const int len = strlen(other)+1;

    buf = cs_alloc(len);

    strcpy(buf, other);

    //printf(" buf = %s %p\n", buf, buf);
  } else {
    buf = cs_alloc(0);
  }
}


/**
 * Builds a string as a copy of a string
 * @author Hj. Malthaner
 */
cstring_t::cstring_t(const cstring_t & other)
{                                           
    //printf("cstring_t::cstring_t(const cstring_t &other)\n");
    const int len = other.len()+1;

    buf = cs_alloc(len);

    strcpy(buf, other.buf);

    //printf(" buf = %s %p\n", buf, buf);
}

                         
cstring_t::~cstring_t()
{                      
    //printf("cstring_t::~cstring_t()\n buf=%s %p\n", buf, buf);
    cs_free(buf);
    buf = 0;
}
                       

/**
 * Appends a char array to this string
 * @author Hj. Malthaner
 */
cstring_t cstring_t::operator+ (const char *other) const
{
    //printf("cstring_t cstring_t::operator+ (const char *other) const\n");
    const int len = strlen(buf) + strlen(other) + 1;

    //printf(" buf = %s  other = %s\n", buf, other);

    char tmp [len]; 

    strcpy(tmp, buf);
    strcat(tmp, other);
           
    cstring_t result (tmp);

    //printf(" buf = %s %p\n", tmp, tmp);

    return result;
}                



/**
 * Concatenates this string and a char array
 * @author Hj. Malthaner
 */
void cstring_t::operator+= (const char *other)
{
  *this = *this + other;
}



/**
 * Appends an integer to this string
 * @author Hj. Malthaner
 */
cstring_t cstring_t::operator+ (long v) const
{
  char buf[64];

  sprintf(buf, "%ld", v);

  return *this + buf;
}


cstring_t & cstring_t::operator= (const cstring_t &other)
{
    // printf("cstring_t & cstring_t::operator= (const cstring &other)\n");
    cs_free(buf);
    buf = 0;

    if(other.buf) {
      buf = cs_alloc(other.len()+1); 
    
      strcpy(buf, other.buf);
    }

    return *this;
}    


/**
 * Comparison operator
 * @author Hj. Malthaner
 */
bool cstring_t::operator== (const cstring_t &other) const
{
  return strcmp(buf, other.buf) == 0;
}


/**
 * Comparison operator
 * @author Hj. Malthaner
 */
bool cstring_t::operator== (const char *other) const
{
  return strcmp(buf, other) == 0;
}


bool cstring_t::operator!= (const cstring_t &other) const
{
  return strcmp(buf, other.buf) != 0;
}


bool cstring_t::operator!= (const char *other) const
{
  return strcmp(buf, other) != 0;
}


/**
 * Automagic conversion to a const char* for backwards compatibility
 * @author Hj. Malthaner
 */       
cstring_t::operator const char *() const
{
    //printf("cstring_t::operator const char *() const\n");
    return buf;
}


/**
 * @return Number of characters in this string
 * @author Hj. Malthaner
 */
unsigned int cstring_t::len() const
{         
    //printf("cstring_t::len() const\n");
    return (unsigned int)strlen(buf);
}


/**
 * Substring operator
 * @param first first char to include
 * @param last position after last char to include
 * @author Hj. Malthaner
 */
cstring_t cstring_t::substr(unsigned int first, unsigned int last)
{
  const unsigned int orglen = len();
  const unsigned int len  = last > orglen ? orglen-first : last-first;

  char *src  = buf+first;
  char dest [len+1];

  for(unsigned int i=0; i<len; i++) {
    dest[i] = src[i];
  }
  
  dest[len] = '\0';

  cstring_t result (dest);

  return result;
}


/**
 * Find character backwards
 * @return index of character or -1 if not found
 */
int cstring_t::find_back(const char c) const
{
    if(buf) {
	const char * p = buf + len();

        while(p-- != buf) {
	    if(*p == c) {
		return p - buf;
	    }
	}
    }
    return -1;
}


/**
 * Bounds checked, fairly slow. 
 *
 * @author Hj. Malthaner
 */
char cstring_t::char_at(unsigned int i) const
{
  if(buf && i<strlen(buf)) {
    return buf[i];
  } else {
    return '\0';
  }
}
