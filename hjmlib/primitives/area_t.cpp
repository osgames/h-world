#include "area_t.h"


void boundary_iterator_t::begin()
{
  k = a->tl + koord(-1,0);
}


bool boundary_iterator_t::next()
{
  k.x ++;

  // Line done?
  if(k.x > a->br.x) {
    k.x = a->tl.x;
    k.y ++;
  }

  // left border done?
  if(k.y > a->tl.y && k.x > a->tl.x) {
    k.x = a->br.x;
  } 

  return k.y <= a->br.y;
}
