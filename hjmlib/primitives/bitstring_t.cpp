#include <string.h>
#include "bitstring_t.h"


/**
 * Creates a new bitstring.
 *
 * @param len number of bits to allocate
 * @author Hj.Malthaner
 */
bitstring_t::bitstring_t(int len)
{
  bitlen = len;
  bytelen = (len+7)/8;

  buf = new unsigned char [bytelen];
}


/**
 * Destructor
 * @author Hj. Malthaner
 */
bitstring_t::~bitstring_t()
{
  delete buf;
  buf = 0;
}


/**
 * Sets all bits
 * @author Hj. Malthaner
 */
void bitstring_t::set()
{
  memset(buf, 255, bytelen);
}


/**
 * Unsets all bits
 * @author Hj. Malthaner
 */
void bitstring_t::clr()
{
  memset(buf, 0, bytelen);
}
