/* Copyright by Hj. Malthaner */

#ifndef SDL_WINDOW_T_H
#define SDL_WINDOW_T_H

#include "system_window_t.h"
#include "event_t.h"
class koord;

class sdl_window_t : public system_window_t 
{
public:

  static const unsigned int F_NONE;
  static const unsigned int F_OPENGL;
  static const unsigned int F_FULLSCREEN;

private:
  event_t event;

  unsigned int sdl_video_flags;

  /**
   * Internal input event handling
   * @author Hj. Malthaner
   */
  void get_event_aux(bool wait);

  void dump_screen();

protected:    

  virtual bool system_init(const char * caption);

  virtual bool system_exit();

public:

  virtual unsigned int get_system_color(unsigned int r, unsigned int g, unsigned int b) const;


  sdl_window_t(unsigned int video_flags);
  virtual ~sdl_window_t();

  
  virtual koord get_mouse_pos() {return event.mpos;};


  /**
   * Paints rectangular area onto screen.
   *
   * @author Hj. Malthaner
   */
  virtual void redraw(int x, int y, int w, int h);



  /**
   * Get system time in milliseconds since program start
   *
   * @author Hj. Malthaner
   */
  virtual unsigned long get_time() const;


  /**
   * Sleeps (delays program execution) for some milliseconds
   *
   * @author Hj. Malthaner
   */
  virtual void sleep(unsigned long milliseconds);


  /**
   * Polls for input events. Do not store references to events, this
   * return always a reference to the same input event object.
   * @author Hj. Malthaner
   */
  virtual event_t & poll_event();


  /**
   * Waits for input events
   * @author Hj. Malthaner
   */
  virtual event_t & get_event();

};
#endif //SDL_WINDOW_T_H
