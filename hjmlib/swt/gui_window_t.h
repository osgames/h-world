/* Copyright by Hj. Malthaner */

#ifndef GUI_WINDOW_T_H
#define GUI_WINDOW_T_H


#ifndef DRAWABLE_T_H
#include "drawable_t.h"
#endif

class koord;

class gui_component_t;
class gui_container_t;
class graphics_t;
class font_t;
class event_t;
class color_t;
class tile_descriptor_t;
class event_listener_t;


/**
 * A borderless window. Can hold components.
 * @author Hj. Malthaner
 */
class gui_window_t : public drawable_t 
{
private:

  gui_container_t * container;


  gui_component_t * get_component_at(gui_component_t *c, 
				     koord topleft, koord pos);

  koord get_component_location(koord topleft,
			       const gui_component_t *search,
			       gui_component_t *comparison);
  
protected:

  gui_container_t * get_container() {return container;};
  bool opaque;

public:

  gui_window_t();
  virtual ~gui_window_t();


  /**
   * setzt die Transparenz
   * @author Hj. Malthaner
   */
  void set_opaque(bool janein);
                 
  void set_color(const color_t & c);
  void set_background(const tile_descriptor_t *t);
                 

  /**
   * Setzt die Fenstergroesse
   * @author Hj. Malthaner
   */
  virtual void set_size(koord size);
         

  /**
   * F�gt eine Komponente zum Fenster hinzu.
   * @author Hj. Malthaner
   */     
  void add_component(gui_component_t *component);
    

  /**
   * Entfernt eine Komponente aus dem Container.
   * @author Hj. Malthaner
   */     
  void remove_component(gui_component_t *comp);


  /**
   * Only removes, does not delete!
   * @author Hj. Malthaner
   */     
  void remove_all_components();


  gui_component_t * get_component_at(koord pos);


  /**
   * Searches the screen (displayed) location of a component of this frame
   * @return koord of component if found, (-1,-1) otherwise
   * @author Hj. Malthaner
   */
  koord get_component_location(const gui_component_t *c);


  /**
   * Draws the window and all components (by calling update
   * on the container).
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);    
  

  void add_event_listener(event_listener_t * evl);    


  /**
   * Subclasses that want to get event must override this 
   * to return true.
   * @author Hj. Malthaner
   */
  virtual bool consume_events() const {return true;};


  bool process_event(const event_t &ev);
};
#endif //GUI_WINDOW_T_H
