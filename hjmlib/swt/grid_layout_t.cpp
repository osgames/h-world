/* 
 * grid_layout_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the SWT project and may not be used
 * in other projects without written permission of the author.
 */


#include "grid_layout_t.h"
#include "gui_component_t.h"
class koord;

// #include <stdio.h>

koord grid_layout_t::get_dimension()
{
  return bottom_right + koord(border, border);
}


void grid_layout_t::set_component(koord pos, gui_component_t * c)
{
  if(pos.x < tlc.x) tlc.x = pos.x;
  if(pos.y < tlc.y) tlc.y = pos.y;
  if(pos.x > brc.x) brc.x = pos.x; 
  if(pos.y > brc.y) brc.y = pos.y; 

  components[pos.x][pos.y] = c;
}


void grid_layout_t::reset(int n)
{
  for(int j=0; j<n; j++) {
    for(int i=0; i<n; i++) {
      components[i][j] = 0;
    }
  }

  tlc = koord (n-1, n-1);
  brc = koord (0,0);
}


grid_layout_t::grid_layout_t(int n)
{
  row_heights = new int[n];
  column_widths = new int[n];

  components = new gui_component_t ** [n];

  for(int i=0; i<n; i++) {
    components[i] = new gui_component_t * [n];
  }

  reset(n);
}


void grid_layout_t::do_layout()
{
  for(int j=tlc.y; j<=brc.y; j++) {
    row_heights[j] = 0;
  }
   
  for(int i=tlc.x; i<=brc.x; i++) {
    column_widths[i] = 0;
  }

  for(int j=tlc.y; j<=brc.y; j++) {
    for(int i=tlc.x; i<=brc.x; i++) {

      if(components[i][j]) {
	const koord size = components[i][j]->get_size();

	if(size.x > column_widths[i]) column_widths[i] = size.x;
	if(size.y > row_heights[j]) row_heights[j] = size.y;
	
      }
    }
  }

  koord pos (border, border);

  bottom_right = koord(0,0);

  for(int j=tlc.y; j<=brc.y; j++) {
    pos.x = border;

    for(int i=tlc.x; i<=brc.x; i++) {

      if(components[i][j]) {
	const koord size = components[i][j]->get_size();
	components[i][j]->set_pos(pos+koord((column_widths[i]-size.x)/2, (row_heights[j]-size.y)/2));

	if(pos.x+size.x > bottom_right.x) bottom_right.x = pos.x+size.x;
	if(pos.y+size.y > bottom_right.y) bottom_right.y = pos.y+size.y;
      }

      pos.x += column_widths[i]+2;
    }
    pos.y += row_heights[j]+2;
  }
}


void grid_layout_t::shrink_vertical(int d_min)
{
  int move_sum = 0;

  // exclude last row
  for(int j=tlc.y; j<brc.y; j++) {
    int distance = 999999;

    // find minimum distance
    for(int i=tlc.x; i<=brc.x; i++) {
      gui_component_t *top = components[i][j]; 
      gui_component_t *bot = components[i][j+1]; 

      int d = 999999;

      if(top && bot) {
	d = bot->get_pos().y - (top->get_pos().y + top->get_size().y);
      }

      if(d < distance) {
	distance = d;
      }
    }

    // printf("Row %d raw distance %d\n", j, distance);

    // find minimum distance
    for(int i=tlc.x; i<=brc.x; i++) {
      gui_component_t *bot = components[i][j+1];

      if(bot && bot->get_pos().y < distance) {
	distance = bot->get_pos().y;
      }
    }

    // printf("Row %d raw distance %d\n", j, distance);

    // move all lower rows upwards

    if(distance < 999999 && distance > d_min) {

      const koord move (0, d_min-distance);
      move_sum += move.y; 

      for(int jj=j+1; jj<=brc.y; jj++) {
	for(int i=tlc.x; i<=brc.x; i++) {
	  gui_component_t *comp = components[i][jj]; 
	  
	  if(comp) {
	    comp->set_pos( comp->get_pos() + move );
	  }
	}
      }  
    }
  }

  bottom_right.y += move_sum;
}
