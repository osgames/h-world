/* Copyright by Hj. Malthaner */

#include "drop_target_t.h"
#include "drag_controller_t.h"


bool drop_target_t::start_dragging(drop_source_t * source,
				   drawable_t * drawable) 
{
  if(drag_control) {
    drag_control->start_dragging(source, drawable);
  }

  return drag_control != 0;
}



drop_target_t::drop_target_t ()
{
  drag_control = 0;
}


drop_target_t::~drop_target_t ()
{
  drag_control = 0;
}
