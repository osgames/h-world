/* Copyright by Hj. Malthaner */

#ifndef EVENT_QUEUE_T_H
#define EVENT_QUEUE_T_H

#include "event_t.h"

class system_window_t;

class event_queue_t {
protected:

    event_queue_t();

public:    

    event_t & poll_event();

    void init(system_window_t * syswin);

    static event_queue_t * get_instance();

private:

    /**
     * @link
     * @shapeType PatternLink
     * @pattern Singleton
     * @supplierRole Singleton factory 
     */
    /*# event_queue_t __event_queue_t; */
    static event_queue_t * single_instance;

    /** @link dependency 
     * @stereotype use*/
    /*#  event_t lnkevent_t; */



    system_window_t *syswin;
};
#endif //EVENT_QUEUE_T_H
