/* Copyright by Hj. Malthaner */

#ifndef SYSTEM_WINDOW_T_H
#define SYSTEM_WINDOW_T_H

#ifndef system_color_t_h
#include "system_color_t.h"
#endif

class koord;
class graphics_t;
class font_t;
class event_t;


class system_window_t : public system_color_t
{
private:    

  static system_window_t * single_instance;

  /**
   * Width of this system window.
   * @author Hj. Malthaner
   */
  int width;


  /**
   * Height of this system window.
   * @author Hj. Malthaner
   */
  int height;

protected:

  graphics_t * root_graphics;
  font_t * default_font;

  virtual bool system_init(const char * caption) = 0;
  virtual bool system_exit() = 0;

public:

  static system_window_t * get_instance() {return single_instance;};


  /**
   * Gets the default font
   * @author Hj. Malthaner
   */
  font_t * get_default_font() const;


  /**
   * Gets the width.
   * @author Hj. Malthaner
   */
  int get_width() const {return width;};


  /**
   * Gets the height.
   * @author Hj. Malthaner
   */
  int get_height() const {return height;};


  virtual koord get_mouse_pos() = 0;

  system_window_t();


  /**
   * Initializes this system window - must be called before
   * the window can be used
   * @author Hj. Malthaner
   */
  bool initialize(int width, int height, 
		  const char * caption,
		  const char * def_font_name);


  void close();


  virtual ~system_window_t();


  /**
   * Paints rectangular area onto screen.
   *
   * @author Hj. Malthaner
   */
  virtual void redraw(int x, int y, int w, int h) = 0;


  graphics_t * get_graphics() const;


  /**
   * get system time in milliseconds since program start
   *
   * @author Hj. Malthaner
   */
  virtual unsigned long get_time() const = 0;


  /**
   * Sleeps (delays program execution) for some milliseconds
   *
   * @author Hj. Malthaner
   */
  virtual void sleep(unsigned long milliseconds) = 0;
  

  /**
   * Polls for input events
   * @author Hj. Malthaner
   */
  virtual event_t & poll_event() = 0;


  /**
   * Waits for input events
   * @author Hj. Malthaner
   */
  virtual event_t & get_event() = 0;
};
#endif //SYSTEM_WINDOW_T_H
