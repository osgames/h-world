#include "gui_window_t.h"
#include "event_t.h"
#include "gui_container_t.h"
#include "tpl/slist_tpl.h"

gui_window_t::gui_window_t()
{
  container = new gui_container_t();
  set_opaque(false);
}


gui_window_t::~gui_window_t()
{
  delete container;
  container = 0;
}


gui_component_t * gui_window_t::get_component_at(gui_component_t *c, 
						koord topleft, koord pos)
{
  gui_component_t * result = NULL;

  if(c) {
    const slist_tpl<gui_component_t *> * children = c->get_children();

    if(children) {
      slist_iterator_tpl <gui_component_t *> iter (children);

      topleft += c->get_pos();

      while(!result && iter.next()) { 
	result = get_component_at(iter.get_current(), topleft, pos);
      }
    } else {
      if(c->is_inside(pos-topleft)) {
	result = c;
      }
    }

  }

  return result;
}


koord gui_window_t::get_component_location(koord topleft, 
					  const gui_component_t *search,
					  gui_component_t *comparison)
{
  koord result (-1,-1);

  topleft += comparison->get_pos();

  if(comparison == search) {
    result = topleft;

  } else {
    const slist_tpl<gui_component_t *> * children = comparison->get_children();

    if(children) {
      slist_iterator_tpl <gui_component_t *> iter (children);

      while(result == koord(-1,-1) && iter.next()) { 
	result = get_component_location(topleft, search, iter.get_current());
      }
    }
  }

  return result;
}


gui_component_t * gui_window_t::get_component_at(koord pos)
{
  koord topleft = get_pos();

  return get_component_at(container, topleft, pos);
}


/**
 * Searches the screen (displayed) location of a component of this frame
 * @return koord of component if found, (-1,-1) otherwise
 * @author Hj. Malthaner
 */
koord gui_window_t::get_component_location(const gui_component_t *c)
{
  koord topleft = get_pos();

  return get_component_location(topleft, c, container);
}



void gui_window_t::remove_component(gui_component_t *c)
{
    container->remove_component(c);
}


/**
 * Only removes, does not delete!
 * @author Hj. Malthaner
 */     
void gui_window_t::remove_all_components()
{
  container->remove_all();
}


void gui_window_t::add_component(gui_component_t *c)
{
    container->add_component(c);
}


void gui_window_t::set_opaque(bool janein)
{
    opaque = janein;
    container->set_opaque(janein);
}


/**
 * Setzt die Fenstergroesse
 * @author Hj. Malthaner
 */
void gui_window_t::set_size(koord size) {
  drawable_t::set_size(size);
  container->set_size(size);
}


void gui_window_t::set_color(const color_t & c) 
{
  container->set_color(c);
}


void gui_window_t::set_background(const tile_descriptor_t *t) 
{
  container->set_background(t);
}


/**
 * Draws the window and all components (by calling update
 * on the container).
 * @author Hj. Malthaner
 */
void gui_window_t::draw(graphics_t *gr, koord pos)
{
  container->update(gr, pos);
}


void gui_window_t::add_event_listener(event_listener_t * evl)
{
  container->add_event_listener(evl);
}


bool gui_window_t::process_event(const event_t &ev)
{
  event_t ev2 = ev.translate(-get_pos());
  bool swallowed = container->process_event(ev2);

  return swallowed;
}
