/* Copyright by Hj. Malthaner */

#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>

#include <sys/shm.h>
#include <sys/time.h>
#include <X11/extensions/XShm.h>

#include "x11_window_t.h"
#include "graphics_t.h"
#include "util/debug_t.h"


// Time at program start

static struct timeval start;

static XImage *texturimg;


void x11_window_t::init_cursors()
{   
  static char bits[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  Pixmap pix;
  XColor cfg;

  // create invisible cursor

  cfg.red = cfg.green = cfg.blue = 0;
  pix = XCreateBitmapFromData(x11_display, x11_window, bits, 8, 8);
  invisible_cursor = XCreatePixmapCursor(x11_display, pix, pix, &cfg, &cfg, 0, 0);
  XFreePixmap(x11_display, pix);
  
  // create standard cursor
  
  standard_cursor = XCreateFontCursor(x11_display, 130);
}


event_t & x11_window_t::poll_event() 
{ 
  // get event, do not wait
  get_event_aux(false);

  return event;
}


event_t & x11_window_t::get_event() 
{ 
  // get event, do wait
  get_event_aux(true);

  return event;
}


unsigned long x11_window_t::get_time() const
{
  struct timeval now;
  unsigned long time;

  gettimeofday(&now, NULL);
  time=(now.tv_sec-start.tv_sec)*1000+(now.tv_usec-start.tv_usec)/1000;
  return time;
}


/**
 * Sleeps (delays program execution) for some milliseconds
 *
 * @author Hj. Malthaner
 */
void x11_window_t::sleep(unsigned long milliseconds)
{
  usleep(milliseconds*1000);
}



/**
 * redraw only the area. XXX curently redraws full width!
 *
 * @author Hj. Malthaner
 */ 
void x11_window_t::redraw(int x, int y, int w, int h)
{
  dbg->message("x11_window_t::redraw()", "Area x=%d y=%d w=%d h=%d", x,y,w,h); 

  XPutImage(x11_display, x11_window, x11_gc,texturimg, x, y, x, y, w, h);
}


x11_window_t::~x11_window_t() 
{
  system_exit();
}


unsigned int x11_window_t::get_system_color(unsigned int r, unsigned int g, unsigned int b) const
{
  return ((r >> 3) << 10) + ((g >> 3) << 5) + ((b >> 3) << 0);
}


bool x11_window_t::system_exit()
{
  if(x11_display) {
    XCloseDisplay(x11_display); // raeumt alles auf
    x11_display = 0;
  }

  // never fails
  return true;
}


bool x11_window_t::system_init()
{
  // init time count
  gettimeofday(&start, NULL);


  // init window
  XSetWindowAttributes attr;
  XSizeHints mh;

  x11_display=XOpenDisplay(NULL);

  if(x11_display==NULL) {
    fprintf(stderr, "Error: x11_window_t::system_init()\tCan't open display.");
    return false;
  }

  x11_screen=DefaultScreen(x11_display);
	
  display_depth = DefaultDepth(x11_display, x11_screen);

  fprintf(stderr,
	  "Message: x11_window_t::system_init()\tUsing %d bpp\n", 
	  display_depth); 

  
  if(display_depth >= 15) {
    fprintf(stderr,
	    "Message: x11_window_t::system_init()\tUsing truecolor mode\n");

  } else {
    fprintf(stderr,"Error: x11_window_t::system_init()");
    fprintf(stderr,"\t\tThe simple windowing toolkit can only operate\n"); 
    fprintf(stderr,"\t\twith 8 bit colormapped or 15 bit or higher\n");
    fprintf(stderr,"\t\ttrue color modes\n\n");
    fprintf(stderr,"Exiting ...\n");
    return false;
  }
  
  unsigned long mbg = WhitePixel(x11_display,x11_screen);
  unsigned long mfg = BlackPixel(x11_display,x11_screen);

  mh.x = 0;                               /* Fenster sollte im sichtbaren */
  mh.y = 0;                                /* bereich liegen */

  // set window size hints
  // min_size = size = max_size  -> no resizing 
  
  mh.width = get_width();
  mh.height = get_height();
  
  mh.min_width = mh.max_width = get_width();
  mh.min_height = mh.max_height = get_height();
  
  mh.flags=PSize|PMinSize|PMaxSize;
  
  fprintf(stderr,"Message: x11_window_t::system_init(): Opening program window ...\n");

  x11_window=XCreateSimpleWindow(x11_display,DefaultRootWindow(x11_display),
			 mh.x,mh.y,mh.width,mh.height, 5,
			 mfg,mbg);

  XSetStandardProperties(x11_display,
			 x11_window,
			 "SWT Window",
			 "SWT",
			 None,
			 NULL,
			 0,
			 NULL);

  x11_gc = XCreateGC(x11_display,x11_window,0,0);

  XSetBackground(x11_display, x11_gc, mbg);
  XSetForeground(x11_display, x11_gc, mfg);

  XSelectInput(x11_display,x11_window,  
	       VisibilityChangeMask|
	       ButtonPressMask|ButtonReleaseMask|
	       KeyPressMask|KeyReleaseMask|
	       PointerMotionMask);

  attr.backing_store=Always;
  XChangeWindowAttributes(x11_display, x11_window, CWBackingStore, &attr);


  XMapRaised(x11_display, x11_window);

  init_cursors();
  
  XDefineCursor(x11_display, x11_window, standard_cursor);

  const int depth = 16;
  char * buffer = (char*)malloc(get_width()*get_height()*(depth/8));

  texturimg=XCreateImage(x11_display, 
			 DefaultVisual(x11_display, x11_screen),
			 display_depth, ZPixmap, 0,
			 buffer,
			 get_width(), get_height(), 
			 depth, 
			 get_width()*(depth/8));


  root_graphics = new graphics_t(this, get_width(), get_height(), 16);
  root_graphics->set_data((unsigned short *)buffer);


  return true;
}


void x11_window_t::dump_screen()
{
  // XXX todo
}


void x11_window_t::get_event_aux(bool wait)
{
  XEvent x11_event;
  char   text[4];
  KeySym mykey;
  
  if(wait) {
    do {
      XNextEvent(x11_display, &x11_event);
    }while(x11_event.type==GraphicsExpose ||
	   x11_event.type==NoExpose ||
	   (XEventsQueued(x11_display, QueuedAlready) > 0 && 
	    x11_event.type==MotionNotify)
	   );
  } else {
    do {
      XNextEvent(x11_display, &x11_event);     
    }while(x11_event.type==GraphicsExpose ||
	   x11_event.type==NoExpose ||
	   (XEventsQueued(x11_display, QueuedAfterFlush) > 0 && 
	    x11_event.type==MotionNotify)
	   );
  }

  switch(x11_event.type) {
  case ButtonPress:
    event.type = event_t::BUTTON_PRESS;
    event.mpos.x = x11_event.xbutton.x;
    event.mpos.y = x11_event.xbutton.y;
    event.cpos.x = x11_event.xbutton.x;
    event.cpos.y = x11_event.xbutton.y;

    switch(x11_event.xbutton.button) {
    case 1:
      event.code=event_t::BUTTON_LEFT;
      event.buttons |= event_t::BUTTON_LEFT;
      break;
    case 2:
      event.code=event_t::BUTTON_MID;
      event.buttons |= event_t::BUTTON_MID;
      break;
    case 3:
      event.code=event_t::BUTTON_RIGHT;
      event.buttons |= event_t::BUTTON_RIGHT;
      break;
    default:
      event.code=event_t::BUTTON_UNKNOWN;
      break;
    }
    break;
    
  case ButtonRelease:
    event.type = event_t::BUTTON_RELEASE;
    event.mpos.x = x11_event.xbutton.x;
    event.mpos.y = x11_event.xbutton.y;
    
    switch(x11_event.xbutton.button) {
    case 1:
      event.code=event_t::BUTTON_LEFT;
      event.buttons &= ~event_t::BUTTON_LEFT;
      break;
    case 2:
      event.code=event_t::BUTTON_MID;
      event.buttons &= ~event_t::BUTTON_MID;
      break;
    case 3:
      event.code=event_t::BUTTON_RIGHT;
      event.buttons &= ~event_t::BUTTON_RIGHT;
      break;
    default:
      event.code=event_t::BUTTON_UNKNOWN;
      break;
    }
    break;

  case KeyPress:
    event.type=event_t::KEY_PRESS;
    
    if(XLookupString(&x11_event.xkey,text,1,&mykey,0)) {
      event.code=text[0];
    } else {
      event.code=0;
    }              
    
    break;
  case MotionNotify:
    if(event.buttons != 0) {
      event.type = event_t::MOUSE_DRAG;
    } else {
      event.type = event_t::MOUSE_MOVE;
    }
    event.code = 0;

    event.mpos.x = x11_event.xmotion.x;
    event.mpos.y = x11_event.xmotion.y;
    break;
  case 15:
    // exposure oder so
    redraw(0, 0, get_width(), get_height());
    event.type=event_t::IGNORE_EVENT;
    event.code=0; 
    break;
  case KeyRelease:
    event.type=event_t::IGNORE_EVENT;
    event.code=0; 
    break;
  default:
    printf("Unbekanntes Ereignis # %d!\n",x11_event.type);
    event.type=event_t::IGNORE_EVENT;
    event.code=0;
  }
  
  // event.dump();
}
