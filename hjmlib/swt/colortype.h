#ifndef colortype_t_h
#define colortype_t_h

/**
 * Currently supported colortypes
 * @author Hj. Malthaner
 */
enum colortype {RGB555, RGB565, RGB888};

#endif // colortype_t_h


