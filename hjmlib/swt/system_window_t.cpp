/* Copyright by Hj. Malthaner */

#include "system_window_t.h"
#include "graphics_t.h"
#include "color_t.h"
#include "font_t.h"
#include "event_t.h"


system_window_t *system_window_t::single_instance = 0;


graphics_t * system_window_t::get_graphics() const
{
  return root_graphics;
}


/**
 * Gets the default font
 * @author Hj. Malthaner
 */
font_t * system_window_t::get_default_font() const
{
  return default_font;
}


system_window_t::system_window_t() 
{
  width = 0;
  height = 0;
  root_graphics = 0;
  default_font = 0;

  single_instance = this;
}


system_window_t::~system_window_t() 
{
  width = 0;
  height = 0;
  
  delete root_graphics;
  root_graphics = 0;
}


void system_window_t::close()
{
  system_exit();

  width = 0;
  height = 0;
  
  delete root_graphics;
  root_graphics = 0;
}


/**
 * Initializes this system window - must be called before
 * the window can be used
 * @author Hj. Malthaner
 */
bool system_window_t::initialize(int width, int height, 
				 const char * caption,
				 const char * def_font_name)
{
  // dbg->message("system_window_t::initialize()",
  //	       "Creating new window with size %d,%d", width, height);

  this->width = width;
  this->height = height;

  this->default_font = font_t::load(def_font_name, true, 0, 0);

  const bool ok = system_init(caption);
  
  // dbg->message("system_window_t::initialize()",
  //	       "... done.");

  return ok;
}
