/* Copyright by Hj. Malthaner */

#include "color_t.h"
#include "system_color_t.h"


color_t color_t::BLACK (0,0,0);
color_t color_t::WHITE (255,255,255);

color_t color_t::GREY32 (32,32,32);
color_t color_t::GREY64 (64,64,64);
color_t color_t::GREY96 (96,96,96);
color_t color_t::GREY128 (128,128,128);
color_t color_t::GREY160 (160,160,160);
color_t color_t::GREY192 (192,192,192);
color_t color_t::GREY224 (224,224,224);


color_t::color_t(unsigned char r, unsigned char g, unsigned char b)
{
  this->r = r;
  this->g = g;
  this->b = b;

  system_color = 0;
  need_system = true;
}


color_t color_t::lighter(int n) const
{
  unsigned char nr = r + n > 255 ? 255 : r+n < 0 ? 0 : r+n;
  unsigned char ng = g + n > 255 ? 255 : g+n < 0 ? 0 : g+n;
  unsigned char nb = b + n > 255 ? 255 : b+n < 0 ? 0 : b+n;

  return color_t(nr, ng, nb);
}


/**
 * Determine system representation of a color.
 * Can only be called after the system window was opened!
 * @author Hj. Malthaner
 */
unsigned int color_t::get_system_color(const system_color_t *syscol)
{
  if( need_system ) {
    system_color = syscol->get_system_color(r, g, b);
    need_system = false;
  } 

  return system_color;
}
