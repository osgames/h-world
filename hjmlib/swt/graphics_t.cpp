/* 
 * graphics_t.h
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "primitives/koord.h"
#include "graphics_t.h"
#include "font_t.h"
#include "color_t.h"
#include "image_t.h"
#include "tile_descriptor_t.h"
#include "system_color_t.h"
#include "primitives/invalid_argument_exception_t.h"


// XXX mabe this should be moved to a file
class RGB555_color_t : public system_color_t
{
public:

  unsigned int get_system_color(unsigned int r, unsigned int g, unsigned int b) const
  {
    return
    	((r & 0xF8) << 7) +
	((g & 0xF8) << 2) +
	((b & 0xF8) >> 3);
  }
};


// XXX mabe this should be moved to a file
class RGB565_color_t : public system_color_t
{
public:

  unsigned int get_system_color(unsigned int r, unsigned int g, unsigned int b) const
  {
    return
    	((r & 0xF8) << 8) +
	((g & 0xFC) << 3) +
	((b & 0xF8) >> 3);
  }
};


// Hajo: allocate only one instance of each and use that one
// all over
static RGB555_color_t the_RGB555_color;
static RGB565_color_t the_RGB565_color;



void (*pixcopy)(PIXVAL *dest, const PIXVAL *src, int len);


static int r_shade;
static const int * recolor_tab;


/**
 * Darkens a pixel. Can be used to lighten a pixel, too, but there
 * is no bounds check for overflows. RGB 565 version.
 * @author Hj. Malthaner
 */
static inline PIXEL darken16(const int dunkel, 
			     unsigned int farbe)
{
  /*
    return
      (
      (((farbe & 0x7C00) * dunkel*2) & (0x1F0000)) |
      (((farbe & 0x03E0) * dunkel*2) & (0xF800)) |  
      (((farbe & 0x1F) * dunkel))
      ) >> 5;
  */

  // input is RGB 555
  farbe = (((((farbe << 16) | farbe) & 0x3E07C1F) * dunkel) >> 5) & 0x3E07C1F;
  return (((farbe >> 16) | (farbe & 0x7C00)) << 1) | (farbe & 0x1F); 

}


/**
 * Darkens a pixel. Can be used to lighten a pixel, too, but there
 * is no bounds check for overflows. RGB 555 version.
 * @author Hj. Malthaner
 */
static inline PIXEL darken15(const int dunkel, 
			     unsigned int farbe)
{
  /*
    return
      (
      (((farbe & 0x7C00) * dunkel) & (0xF8000)) |
      (((farbe & 0x03E0) * dunkel) & (0x7C00)) |  
      (((farbe & 0x1F) * dunkel))
      ) >> 5;
  */

  farbe = (((((farbe << 16) | farbe) & 0x3E07C1F) * dunkel) >> 5) & 0x3E07C1F;
  return ((farbe >> 16) | farbe);
}


/**
 * Copies pixels from src to dest, darkens pixels during copy
 * @author Hj. Malthaner
 */
static void pix_darken15(PIXVAL *dest,
			 const PIXVAL *src,
			 int len)
{
  while(len--) {
    *dest++ = darken15(r_shade, *src ++);
  }
}


/**
 * Copies pixels from src to dest, darkens pixels during copy
 * @author Hj. Malthaner
 */
static void pix_darken16(PIXVAL *dest,
			 const PIXVAL *src,
			 int len)
{
  while(len--) {
    *dest++ = darken16(r_shade, *src ++);
  }
}


/**
 * Copies pixels from src to dest, darkens pixels during copy
 * @author Hj. Malthaner
 */
static void pix_recolor15(PIXVAL *dest,
			 const PIXVAL *src,
			 int len)
{
  while(len--) {
    const PIXVAL val = *src ++;

    if(val & 0x8000) {
      *dest++ = darken15(r_shade, recolor_tab[val & 0x7FFF]);
    } else {
      *dest++ = darken15(r_shade, val);
    }
  }
}


/**
 * Copies pixels from src to dest, darkens pixels during copy
 * @author Hj. Malthaner
 */
static void pix_recolor16(PIXVAL *dest,
			 const PIXVAL *src,
			 int len)
{
  while(len--) {
    const PIXVAL val = *src ++;

    if(val & 0x8000) {
      *dest++ = darken16(r_shade, recolor_tab[val & 0x7FFF]);
    } else {
      *dest++ = darken16(r_shade, val);
    }
  }
}


/**
 * Copies pixels from src to dest
 * @author Hj. Malthaner
 */
static void pix_replace15(PIXVAL *dest,
			const PIXVAL *src,
			int len)
{
  while(len--) {
    *dest++ = *src ++;
  }
}

/**
 * Copies pixels from src to dest
 * @author Hj. Malthaner
 */
static void pix_replace16(PIXVAL *dest,
			const PIXVAL *src,
			int len)
{
  while(len--) {
    const PIXVAL val = *src ++;
    *dest++ = ((val & 0x7FE0) << 1) + (val & 0x1F);
  }
}


/**
 * Copies pixels from src to dest
 * @author Hj. Malthaner
 */
static void pix_blend16(PIXVAL *dest,
			const PIXVAL *src,
			int len)
{
  const PIXVAL mask = 0xF7DE;
    
  while(len--) {
    PIXVAL pix1 = *src ++;
    PIXVAL pix2 = *dest;
    
    *dest++ = ((darken16(r_shade, pix1) & mask) + (pix2 & mask)) >> 1;
  }
}


static void pix_blend15(PIXVAL *dest,
			const PIXVAL *src,
			int len)
{
  const PIXVAL mask = 0x7BDE;
    
  while(len--) {
    PIXVAL pix1 = *src ++;
    PIXVAL pix2 = *dest;
    
    *dest++ = ((darken15(r_shade, pix1) & mask) + (pix2 & mask)) >> 1;
  }
}


/**
 * Copies pixels from src to dest, darkens pixels during copy
 * @author Hj. Malthaner
 */
static void pix_blend_recolor15(PIXVAL *dest,
				const PIXVAL *src,
				int len)
{
  const PIXVAL mask = 0x7BDE;

  while(len--) {
    PIXVAL pix1 = *src ++;
    PIXVAL pix2 = *dest;

    if(pix1 & 0x8000) {
      pix1 = recolor_tab[pix1 & 0x7FFF];
    }

    *dest++ = ((darken15(r_shade, pix1) & mask) + (pix2 & mask)) >> 1;
  }
}


/**
 * Copies pixels from src to dest, darkens pixels during copy
 * @author Hj. Malthaner
 */
static void pix_blend_recolor16(PIXVAL *dest,
				const PIXVAL *src,
				int len)
{
  const PIXVAL mask = 0xF7DE;

  while(len--) {
    PIXVAL pix1 = *src ++;
    PIXVAL pix2 = *dest;

    if(pix1 & 0x8000) {
      pix1 = recolor_tab[pix1 & 0x7FFF];
    }

    *dest++ = ((darken16(r_shade, pix1) & mask) + (pix2 & mask)) >> 1;
  }
}


void graphics_t::pixcopy_clipped(PIXVAL *dest, const PIXVAL *src, int xpos, int len) const
{
  int ldiff = 0;
  int rdiff = 0;

  if(xpos < clip_rect.x) {   // left outside ?
    ldiff = clip_rect.x - xpos;
  }

  if(xpos+len-1 > clip_rect.xx) { // right outside ?
    rdiff = clip_rect.xx - (xpos+len-1);
  }

  len += rdiff - ldiff;

  if(len > 0) {
    pixcopy(dest + xpos + ldiff, src+ldiff, len);
  }
}


/**
 * This sets width < 0 if the range is out of bounds
 * so check the value after calling and before displaying
 * @author Hj. Malthaner
 */
int graphics_t::clip_wh(int *x, int *width, 
			const int min_x, const int max_x)
{
  int ldiff = 0;
  int rdiff = 0;

  if(*x < min_x) {   // left outside ?
    ldiff = min_x - *x;
  }

  if(*x+*width-1 > max_x) { // right outside ?
    rdiff = max_x - (*x+*width-1);
  }

  *x += ldiff;
  *width += rdiff - ldiff;

  return ldiff;
} 



void graphics_t::draw_tile_shaded(const tile_descriptor_t * tile, 
				  int xpos, int ypos, int shade)
{
  if(shade != 32) {
    // lighter or darker than default

    if(bit_depth == 16) {
      pixcopy = pix_darken16;
    } else {
      pixcopy = pix_darken15;
    }

    r_shade = shade;

    if(r_shade < 0) {
      r_shade = 0;
    }
  } else {
    // not shaded, just copy
    pixcopy = (bit_depth == 15) ? pix_replace15 : pix_replace16;
  }

  draw_tile_aux(tile, xpos, ypos);
}


void graphics_t::draw_tile_transparent(const tile_descriptor_t * tile, 
				       int xpos, int ypos, int shade)
{
  if(bit_depth == 16) {
    pixcopy = pix_blend16;
  } else {
    pixcopy = pix_blend15;
  }

  r_shade = shade;

  if(r_shade < 0) {
    r_shade = 0;
  }

  draw_tile_aux(tile, xpos, ypos);
}


/**
 * Draws a tile onto this graphics at location xpos, ypos
 * relative to the top-left corner of this graphics. Recolors
 * the special areas to a colormap given as 32-bit padded RGB24
 * @author Hj. Malthaner
 */
void graphics_t::draw_tile_recolored(const tile_descriptor_t * tile, 
				     int xpos, int ypos, int shade, 
				     const int * color_tab)
{
  if(bit_depth == 16) {
    pixcopy = pix_recolor16;
  } else {
    pixcopy = pix_recolor15;
  }


  recolor_tab = color_tab;

  r_shade = shade;

  if(r_shade < 0) {
    r_shade = 0;
  }


  draw_tile_aux(tile, xpos, ypos);
}


/**
 * Draws a tile semi-transparent onto this graphics at location xpos, ypos
 * relative to the top-left corner of this graphics. Recolors
 * the special areas to a colormap given as 32-bit padded RGB24
 * @author Hj. Malthaner
 */
void graphics_t::draw_tile_transparent_and_recolored(const tile_descriptor_t * tile, 
						     int xpos, int ypos, int shade, 
						     const int * color_tab)
{
  if(bit_depth == 16) {
    pixcopy = pix_blend_recolor16;
  } else {
    pixcopy = pix_blend_recolor15;
  }

  recolor_tab = color_tab;

  r_shade = shade > 0 ? shade : 0;

  draw_tile_aux(tile, xpos, ypos);
}


void graphics_t::draw_tile(const tile_descriptor_t * tile, int xpos, int ypos)
{
  pixcopy = (bit_depth == 15) ? pix_replace15 : pix_replace16;

  draw_tile_aux(tile, xpos, ypos);
}


enum colortype graphics_t::get_colortype() const
{
  switch(bit_depth) {
  case 15:
    return RGB555;
  case 16:
    return RGB565;
  }

  // Hajo: If nothing broke consitency, the code 
  // should nerver reach this line
  return RGB555;
}


graphics_t::graphics_t(system_color_t *syscol, 
		       int width, int height, int depth) 
{
  //  dbg->message("graphics_t::graphics_t()", 
  //	       "%dx%dx%dbpp", width, height, depth);

  this->width = width;
  this->height = height;
  this->depth = depth;
  this->syscol = syscol;

  this->font = 0;
  this->data = 0;

  this->dither = false;

  if(depth == 16) {

    if(syscol->get_system_color(255, 255, 255) == 0xFFFF) { 
      bit_depth = 16;
    } else {
      bit_depth = 15;
    }
  } else {
    invalid_argument_exception_t ex;
    strcpy(ex.reason, "graphics_t::graphics_t(): Only 15 and 16 bpp supported so far.");
    throw ex;
  }

  // set clip to full size
  set_clip_wh(0,0,width, height);
}


graphics_t::graphics_t(int width, int height, enum colortype type) 
{
  //  dbg->message("graphics_t::graphics_t()", 
  //	       "%dx%d colortype=%d", width, height, type);

  this->width = width;
  this->height = height;
  this->depth = 16;

  this->font = 0;
  this->data = 0;

  this->dither = false;

  if(type == RGB555) {
    this->syscol = &the_RGB555_color;
    bit_depth = 15;

  } else if(type == RGB565) {
    this->syscol = &the_RGB565_color;
    bit_depth = 16;

  } else {
    invalid_argument_exception_t ex;
    strcpy(ex.reason, "graphics_t::graphics_t(): Graphics only with RGB 555 or RGB 565 supported so far.");
    throw ex;
  }

  // set clip to full size
  set_clip_wh(0,0,width, height);
}


graphics_t::~graphics_t()
{
  // do not free, just unreference, data is a foreign buffer!
  data = 0;
}


/**
 * En/disable triangle border dithering
 * @author Hj. Malthaner
 */
void graphics_t::set_border_dither(bool yesno)
{
  dither = yesno;
}


void graphics_t::set_clip_wh(int x, int y, int w, int h)
{
  // printf("Requested clip area %d,%d,%d,%d\n", x, y, x+w-1, y+h-1);


  clip_wh(&x, &w, 0, width-1);
  clip_wh(&y, &h, 0, height-1);
  
  //    if(w < 0) w = 0;
  //    if(h < 0) h = 0;
  
  clip_rect.x = x;
  clip_rect.y = y;
  clip_rect.w = w;
  clip_rect.h = h;    
  
  clip_rect.xx = x+w-1;
  clip_rect.yy = y+h-1;
  

  // printf("graphics_t::set_clip_wh() area %d,%d,%d,%d\n", clip_rect.x, clip_rect.y, clip_rect.xx, clip_rect.yy);
}


void graphics_t::draw_tile_aux(const tile_descriptor_t * tile,
			       int xpos, int ypos)
{
  // printf("tile aux %d %d clip x=%d y=%d xx=%d yy=%d\n", 
  // xpos, ypos, clip_rect.x, clip_rect.y, clip_rect.xx, clip_rect.yy);

  if(tile) {

    if(xpos >= clip_rect.x && 
       ypos >= clip_rect.y && 
       xpos < clip_rect.xx-tile->rwidth && 
       ypos < clip_rect.yy-tile->rwidth) {
      
      draw_tile_unclipped(tile, xpos, ypos);
      // draw_tile_clipped(tile, xpos, ypos);
      
    } else if(xpos > clip_rect.x-tile->rwidth && 
	      ypos > clip_rect.y-tile->rwidth && 
	      xpos <= clip_rect.xx && 
	      ypos <= clip_rect.yy &&
	      clip_rect.xx >= clip_rect.x && 
	      clip_rect.yy >= clip_rect.y) {
      
      draw_tile_clipped(tile, xpos, ypos);                              
    }   
  }
}


void graphics_t::draw_tile_unclipped(const tile_descriptor_t * tile, 
				     const int xpos, const int ypos)
{
    int h = tile->h;

    if(h > 0) {
      const PIXVAL *sp = tile->data;
      PIXVAL *tp = data + xpos + (ypos + tile->y)*width;


      // bild darstellen

      do { // zeilen dekodieren
		
	int runlen = *sp++;
	PIXVAL *tpp = tp;

	// eine Zeile dekodieren
	do { 

	  // wir starten mit einem clear run
	  tpp += runlen;               
	  
	  // jetzt kommen farbige pixel
	  runlen = *sp++;
	  
	  if(runlen) {
	    
	    pixcopy(tpp, sp, runlen);
	    sp += runlen; 
	    tpp += runlen;
	  }

	  runlen = *sp++;
	  
	} while(runlen);
	
	tp += width;
	
      } while(--h);
    }
}


void graphics_t::draw_tile_clipped(const tile_descriptor_t * tile, 
				   const int xp, const int yp)
{
    int h = tile->h;
    int y = yp + tile->y;
    int yoff = clip_wh(&y, &h, clip_rect.y, clip_rect.yy);
    
    if(h > 0) {
      const PIXVAL *sp = tile->data;
      PIXVAL *tp = data + y*width;

      // oben clippen

      while(yoff--) {
	int runlen = *sp++;
	do {
	  runlen = *sp++;
	  if(runlen) {
	    sp += runlen;
	  }                        		    
	  runlen = *sp++;
	} while(runlen);
      }		    

      do { // zeilen dekodieren
	int xpos = xp;
	
	// bild darstellen
	
	int runlen = *sp++;
	
	do { 
	  // wir starten mit einem clear run
	  
	  xpos += runlen;               
	  
	  // jetzt kommen farbige pixel
	  runlen = *sp++;
	  
	  if(runlen) {  

	    pixcopy_clipped(tp, sp, xpos, runlen);

	    sp += runlen;
	    xpos += runlen;
	  }                        		    

	  runlen = *sp ++;
	} while(runlen);
	
	tp += width;
	
      } while(--h);
    }
}


/**
 * Draws an image onto this graphics at location xpos, ypos
 * relative to the top-left corner.
 * @author Hj. Malthaner
 */
void graphics_t::draw_image(image_t *img, int xpos, int ypos)
{
  PIXEL * dest = data + ypos*width + xpos;
  const PIXEL * src = img->get_data();


  for(int y=0; y<img->get_height(); y++) {

    if(ypos + y >= clip_rect.y &&   
       ypos + y <= clip_rect.yy) {

      memcpy(dest, src, img->get_width()*sizeof(PIXEL));
    }

    dest += width;
    src += img->get_width();
  }
}


/**
 * Draws an image onto this graphics at location xpos, ypos
 * Scales the image to fit into w, h
 * relative to the top-left corner.
 * @author Hj. Malthaner
 */
void graphics_t::draw_image(image_t *img, int xpos, int ypos, int w, int h,
			    PIXEL transkey)
{
  if(xpos+w >= clip_rect.x && ypos+h >= clip_rect.y &&
     xpos <= clip_rect.xx && ypos <= clip_rect.yy) {


    const long wstep = img->get_width()*0xFFFF/w;
    const long hstep = img->get_height()*0xFFFF/h;
    
    long source_y = 0;

    for(int y=ypos; y<ypos+h; y++) {
      if(y>=clip_rect.y && y<=clip_rect.yy) {  
	long source_x = 0;
	const PIXEL * const src = img->get_data() + (source_y >> 16) * img->get_width();

	for(int x=xpos; x<xpos+w; x++) {
	  PIXEL val = src[source_x >> 16];
	  if(x>=clip_rect.x && x<=clip_rect.xx && val != transkey) {
	    draw_pixel_nc(x, y, val);
	  }

	  source_x += wstep;
	}
      }
      
      source_y += hstep;
    }  
  }
}


/**
 * Sets a pixel, does clipping
 * @author Hj. Malthaner
 */
void graphics_t::draw_pixel(int x, int y, PIXEL RGB) {

  if(x>=clip_rect.x && y>=clip_rect.y &&  
     x<=clip_rect.xx && y<=clip_rect.yy) {  
    data[y*width+x] = RGB;
  }
}


/**
 * Sets a pixel, does clipping
 * @author Hj. Malthaner
 */
void graphics_t::draw_pixel(int x, int y, color_t &color) {

  if(x>=clip_rect.x && y>=clip_rect.y &&  
     x<=clip_rect.xx && y<=clip_rect.yy) {  
    data[y*width+x] = color.get_system_color(syscol);
  }
}



/**
 * Zeichnet gefuelltes Rechteck
 * @author Hj. Malthaner
 */
void graphics_t::fillbox_wh(int x, int y, int w, int h, color_t &color)
{
  const PIXVAL sys_color = color.get_system_color(syscol);

  // printf("fillbox_wh request %d %d %d %d\n", x, y, w, h);


  clip_wh(&x, &w, clip_rect.x, clip_rect.xx);
  clip_wh(&y, &h, clip_rect.y, clip_rect.yy);

  //printf("fillbox_wh %d %d %d %d clip x=%d y=%d xx=%d yy=%d\n", 
  // x, y, w, h, clip_rect.x, clip_rect.y, clip_rect.xx, clip_rect.yy);


  if(w > 0 && h > 0) {
    PIXVAL *p = data + x + y*width;
    
    do {
      int ww = w;
      do {
	*p++ = sys_color;
      } while(--ww);
      p += width-w;
    } while(--h);
  }
}


inline void graphics_t::triangle_line15(const int y, 
					int start, int end, 
					int b1, const int b2, 
					const int xoff, const int yoff,
					const PIXEL * img_data)
{
  if(y >= clip_rect.y && y <= clip_rect.yy) {

    // is triangle visible?
    if(end >= clip_rect.x && start <= clip_rect.xx) {

      // do left-right clippping
      if(start < clip_rect.x) start = clip_rect.x;
      if(end > clip_rect.xx) end = clip_rect.xx+1;

      // brighntess slope
      const int bdiff = (b2 - b1) / (end - start + 1);

      // set up src and dest text pointer
      PIXEL * const dest_tex = data + y * width;
      img_data += (((y+yoff) & 63) << 6);


      // dither left start
      if(dither && start > clip_rect.x+4) {
	dest_tex[start-5] = darken15(b1 >> 16, img_data[(start+xoff-5) & 63]);
	dest_tex[start-2] = darken15(b1 >> 16, img_data[(start+xoff-2) & 63]);
      }
      // dither left end


      do {
	dest_tex[start] = darken15(b1 >> 16, img_data[(start+xoff) & 63]);

	start++;
	// slope correction
	b1 += bdiff;
      } while(start<end);


      // dither left start
      if(dither && start < clip_rect.xx-4) {
	dest_tex[start+5] = darken15(b1 >> 16, img_data[(start+xoff+5) & 63]);
	dest_tex[start+2] = darken15(b1 >> 16, img_data[(start+xoff+2) & 63]);
      }
      // dither left end


    }
  }
}


inline void graphics_t::triangle_line16(const int y, 
					int start, int end, 
					int b1, const int b2, 
					const int xoff, const int yoff,
					const PIXEL * img_data)
{
  if(y >= clip_rect.y && y <= clip_rect.yy) {

    // is triangle visible?
    if(end >= clip_rect.x && start <= clip_rect.xx) {

      // do left-right clippping
      if(start < clip_rect.x) start = clip_rect.x;
      if(end > clip_rect.xx) end = clip_rect.xx+1;


      // brighntess slope
      const int bdiff = (b2 - b1) / (end - start + 1);

      // set up src and dest text pointer
      PIXEL * const dest_tex = data + y * width;
      img_data += (((y+yoff) & 63) << 6);


      // dither left start
      if(dither && start > clip_rect.x+4) {
	dest_tex[start-5] = darken16(b1 >> 16, img_data[(start+xoff-5) & 63]);
	dest_tex[start-2] = darken16(b1 >> 16, img_data[(start+xoff-2) & 63]);
      }
      // dither left end


      do {
	dest_tex[start] = darken16(b1 >> 16, img_data[(start+xoff) & 63]);

	start++;
	// slope correction
	b1 += bdiff;
      } while(start<end);


      // dither left start
      if(dither && start < clip_rect.xx-4) {
	dest_tex[start+5] = darken16(b1 >> 16, img_data[(start+xoff+5) & 63]);
	dest_tex[start+2] = darken16(b1 >> 16, img_data[(start+xoff+2) & 63]);
      }
      // dither left end


    }
  }
}


/**
 * Paint gouraud shaded triangle. The points must be delivered in ascending
 * y order, that means point[0] must have the smallest y coordinate and
 * point[2] must have the largest y coordinate.
 *
 * Brightness values should be in range 0..32
 *
 * @author Hj. Malthaner
 */
void graphics_t::draw_triangle(const koord* points, 
			       const signed char *bright, 
			       int xoff, int yoff,
			       const image_t *img)
{

  // Ensure that p1 is right of p2
  if(points[1].x > points[2].x) {

    draw_top_tri(points[0], points[1], points[2], 
		 bright[0], bright[1], bright[2],
		 xoff, yoff, img);


  } else {

    draw_top_tri(points[0], points[2], points[1], 
		 bright[0], bright[2], bright[1],
		 xoff, yoff, img);


  }


  // Ensure that p1 is right of p2
  if(points[1].x > points[0].x) {

    draw_bottom_tri(points[2], points[1], points[0], 
		    bright[2], bright[1], bright[0],
		    xoff, yoff, img);

  } else {

    draw_bottom_tri(points[2], points[0], points[1], 
		    bright[2], bright[0], bright[1],
		    xoff, yoff, img);

  }
}


void graphics_t::draw_top_tri(koord p0, koord p1, koord p2,
			      int b0, int b1, int b2,
			      int xoff, int yoff,
			      const image_t *img)
{
  const int ydiff2 = p2.y - p0.y;
  const int ydiff1 = p1.y - p0.y;

  const int ydiff = ydiff1 < ydiff2 ? ydiff1 : ydiff2;

  if (ydiff > 0) {

    int xdiff2 = ((p2.x - p0.x) << 16) / ydiff2;
    int bdiff2 = ((b2 - b0) << 16) / ydiff2;

    int xdiff1 = ((p1.x - p0.x) << 16) / ydiff1;
    int bdiff1 = ((b1 - b0 << 16)) / ydiff1;

    int xd2 = p0.x << 16;
    int bd2 = b0 << 16;
    int xd1 = xd2; 
    int bd1 = bd2; 


    // printf("%p %p %p %p\n", &xd2, &bd2, &xd1, &bd1);
  
    // upper part of triangle
    for (int y=0; y<ydiff; y++) {

      if(bit_depth == 15) {
	triangle_line15(y+p0.y, 
			(xd2 >> 16), (xd1 >> 16), 
			bd2, bd1,
			xoff, yoff,
			img->get_data());
      } else {
	triangle_line16(y+p0.y, 
			(xd2 >> 16), (xd1 >> 16), 
			bd2, bd1,
			xoff, yoff,
			img->get_data());
      }

      xd2 += xdiff2;
      bd2 += bdiff2;
      xd1 += xdiff1;
      bd1 += bdiff1;
    }
  }    
}



void graphics_t::draw_bottom_tri(koord p0, koord p1, koord p2,
				 int b0, int b1, int b2,
				 int xoff, int yoff,
				 const image_t *img)
{
  const int ydiff2 = p2.y - p0.y;
  const int ydiff1 = p1.y - p0.y;

  const int ydiff = ydiff1 > ydiff2 ? ydiff1 : ydiff2;

  if (ydiff < 0) {

    int xdiff2 = -((p2.x - p0.x) << 16) / ydiff2;
    int bdiff2 = -((b2 - b0) << 16) / ydiff2;

    int xdiff1 = -((p1.x - p0.x) << 16) / ydiff1;
    int bdiff1 = -((b1 - b0 << 16)) / ydiff1;

    int xd2 = p0.x << 16;
    int bd2 = b0 << 16;
    int xd1 = xd2; 
    int bd1 = bd2; 


    // printf("%p %p %p %p\n", &xd2, &bd2, &xd1, &bd1);
  
    for (int y=0; y>=ydiff; y--) {

      if(bit_depth == 15) {
	triangle_line15(y+p0.y, 
			(xd2 >> 16), (xd1 >> 16), 
			bd2, bd1,
			xoff, yoff,
			img->get_data());
      } else {
	triangle_line16(y+p0.y, 
			(xd2 >> 16), (xd1 >> 16), 
			bd2, bd1,
			xoff, yoff,
			img->get_data());
      }

      xd2 += xdiff2;
      bd2 += bdiff2;
      xd1 += xdiff1;
      bd1 += bdiff1;
    }
  }    
}
