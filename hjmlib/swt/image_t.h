/* 
 * image_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the SWT project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef IMAGE_T_H
#define IMAGE_T_H

#include "colortype.h"
#include "pixeltype.h"

class graphics_t;
class tile_descriptor_t;
struct dimension;

/**
 * A representation of a flat 2D image. Currently supports only the RGB555
 * pixel type.
 * @author Hj. Malthaner
 */
class image_t
{
 private:

  /**
   * Image data format is always the same as the system output format
   * to avoid conversions during runtime/rendering
   * @author Hj. Malthaner
   */
  PIXEL * data;
  graphics_t * graphics;

  int width;
  int height;


  void read(const char *filename, enum colortype ct);

  void init_dim(struct dimension *dim);

 public:

  int get_width() const {return width;};
  int get_height() const {return height;};


  PIXEL * get_data() const {return data;};
  graphics_t * get_graphics() const {return graphics;};


  /**
   * Encode image to RLE sprite as used by draw_tile
   * @author Hj. Malthaner
   */
  tile_descriptor_t * encode_tile();


  /**
   * Reads image from file. Currently only PNG files are supported.
   * @author Hj. Malthaner
   */
  image_t(const char * filename);


  /**
   * Reads image from file. Currently only PNG files are supported.
   * @author Hj. Malthaner
   */
  image_t(const char * filename, enum colortype ct);


  /**
   * Constructs a blank image of a given color type
   * @author Hj. Malthaner
   */
  image_t(int width, int height, enum colortype ct);


  ~image_t();
};

#endif
