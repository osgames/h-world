/* Copyright by Hj. Malthaner */

#include <stdlib.h>
#include <SDL/SDL.h>
#include "sdl_window_t.h"
#include "graphics_t.h"
#include "primitives/invalid_argument_exception_t.h"


static SDL_Surface * screen = 0;


const unsigned int sdl_window_t::F_NONE = 0;
const unsigned int sdl_window_t::F_OPENGL = SDL_OPENGL;
const unsigned int sdl_window_t::F_FULLSCREEN = SDL_FULLSCREEN;


event_t & sdl_window_t::poll_event() 
{ 
  // get event, do not wait
  get_event_aux(false);

  return event;
}


/**
 * Waits for input events
 * @author Hj. Malthaner
 */
event_t & sdl_window_t::get_event()
{ 
  // wait for event
  get_event_aux(true);

  return event;
}


unsigned long sdl_window_t::get_time() const
{
  return SDL_GetTicks();
}


/**
 * Sleeps (delays program execution) for some milliseconds
 *
 * @author Hj. Malthaner
 */
void sdl_window_t::sleep(unsigned long milliseconds)
{
  SDL_Delay(milliseconds);
}



/**
 * redraw only the area. XXX curently redraws full width!
 *
 * @author Hj. Malthaner
 */ 
void sdl_window_t::redraw(int x, int y, int w, int h)
{
  //  dbg->message("sdl_window_t::redraw()", "Area x=%d y=%d w=%d h=%d", x,y,w,h); 

  const int width = get_width();
  const int height = get_height();


  if(x < 0) {
    w = width + x;
    x = 0;
  }

  if(x+w > width) {
    w = width - x;
  }


  if(y < 0) {
    h = height+y;
    y = 0;
  }

  if(y+h > height) {
    h = height - y;
  }

  if(w>0 && h>0) {
    SDL_UpdateRect(screen, x, y, w, h);
  }


  // SDL_UpdateRect(screen, 0, 0, width, height);
}


sdl_window_t::sdl_window_t(unsigned int flags) 
{
  this->sdl_video_flags = flags;
}


sdl_window_t::~sdl_window_t() 
{
  system_exit();
}


unsigned int sdl_window_t::get_system_color(unsigned int r, unsigned int g, unsigned int b) const
{
  // printf("Getting system color for %x, %x, %x -> %x\n", r,g,b, SDL_MapRGB(screen->format, r, g, b));

  return SDL_MapRGB(screen->format, r, g, b);
}


bool sdl_window_t::system_exit()
{
  if(screen) {
    SDL_FreeSurface(screen);
    screen = NULL;
  }

  return true;
}


bool sdl_window_t::system_init(const char * caption)
{
  // initialize SDL
  int ok = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_NOPARACHUTE);
  if(ok != 0) {
    fprintf(stderr, "Couldn't initialize SDL video: %s\n", SDL_GetError());
  }

  // else if((ok = SDL_InitSubSystem(SDL_INIT_TIMER)) != 0 ) {
  //    fprintf(stderr, "Couldn't initialize SDL timer: %s\n", SDL_GetError());
  //  }
  
#ifdef USE_SOUND
  if(SDL_InitSubSystem(SDL_INIT_AUDIO) != 0) {
    printf("SDL_INIT_AUDIO failed: %s\n", SDL_GetError());
  } else {
    if (Mix_OpenAudio(audio_rate, audio_format, audio_channels, 4096) != 0) {
      fprintf(stderr, "Mix_OpenAudio failed: %s\n", SDL_GetError());
    } else {
      sound_ok=1;
    }
  }
#endif
  
  // if SDL gets initialized normally, return zero
  atexit(SDL_Quit); // clean up on exit

  if(ok == 0) {

    // open the window now
    screen = SDL_SetVideoMode(get_width(), get_height(), 16, 
			      SDL_HWPALETTE | this->sdl_video_flags);
    if (screen == NULL) {
      fprintf(stderr, "Couldn't open the window: %s\n", SDL_GetError());
      ok = 1;
    } else {
      SDL_EnableUNICODE(true);
      SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,
			  SDL_DEFAULT_REPEAT_INTERVAL);
    
      // set the caption for the window
      SDL_WM_SetCaption(caption, NULL);


      root_graphics = new graphics_t(this, get_width(), get_height(), 16);
      root_graphics->set_data((unsigned short *) (screen->pixels));

    }

    if (screen->pitch != get_width()*2) {
      invalid_argument_exception_t ex;
      sprintf(ex.reason, 
	      "sdl_window_t::system_init():"
	      " Screen byte pitch (%d) != screen byte width (%d)" , 
	      screen->pitch, get_width()*2);
      throw ex;
    }
  }

  return ok == 0;  
}


void sdl_window_t::dump_screen()
{
  if (SDL_SaveBMP(screen, "newshot.bmp")) {
    fprintf(stderr, "Screenshot failed.\n");
  } else {
    FILE *tmp = NULL;
    char buf[256];

    for (int i=0; i<999; i++) {
      sprintf(buf, "%03d.bmp", i);
      if ((tmp = fopen(buf, "rb")) != NULL) {
	fclose(tmp);
      } else {
	rename("newshot.bmp", buf);
      }
    }
  }
}


void sdl_window_t::get_event_aux(bool wait)
{
  SDL_Event sdl_event;
  sdl_event.type = 1;

  // assume we get no event
  event.type = event_t::NO_EVENT;
  event.code = 0;

  if(wait) {
    int n = 0;

    do {        
      SDL_WaitEvent(&sdl_event);
      n = SDL_PollEvent(NULL);
    }  while (n != 0 && sdl_event.type==SDL_MOUSEMOTION);

  } else {   
    int n = 0;
    bool got_one = false;
    
    do {
      n = SDL_PollEvent(&sdl_event);
      
      if(n != 0) {
	got_one = true;
	
	if(sdl_event.type == SDL_MOUSEMOTION) {	
	  event.type = event_t::MOUSE_MOVE;
	  event.code = 0;
	  event.mpos.x = sdl_event.motion.x;
	  event.mpos.y = sdl_event.motion.y;     
	}
      }

    } while (n != 0 && sdl_event.type==SDL_MOUSEMOTION);
    
    if(!got_one) {
      return;
    }
  }


  switch(sdl_event.type) {
  case SDL_MOUSEBUTTONDOWN:
    event.type = event_t::BUTTON_PRESS;
    event.mpos.x = sdl_event.button.x;
    event.mpos.y = sdl_event.button.y;
    event.cpos.x = sdl_event.button.x;
    event.cpos.y = sdl_event.button.y;

    switch(sdl_event.button.button) {
    case 1:
      event.code=event_t::BUTTON_LEFT;
      event.buttons |= event_t::BUTTON_LEFT;
      break;
    case 2:
      event.code=event_t::BUTTON_MID;
      event.buttons |= event_t::BUTTON_MID;
      break;
    case 3:
      event.code=event_t::BUTTON_RIGHT;
      event.buttons |= event_t::BUTTON_RIGHT;
      break;
    default:
      event.code=event_t::BUTTON_UNKNOWN;
      break;
    }
    break;

  case SDL_MOUSEBUTTONUP:
    event.type = event_t::BUTTON_RELEASE;
    event.mpos.x = sdl_event.button.x;
    event.mpos.y = sdl_event.button.y;

    switch(sdl_event.button.button) {
    case 1:
      event.code=event_t::BUTTON_LEFT;
      event.buttons &= ~event_t::BUTTON_LEFT;
      break;
    case 2:
      event.code=event_t::BUTTON_MID;
      event.buttons &= ~event_t::BUTTON_MID;
      break;
    case 3:
      event.code=event_t::BUTTON_RIGHT;
      event.buttons &= ~event_t::BUTTON_RIGHT;
      break;
    default:
      event.code=event_t::BUTTON_UNKNOWN;
      break;
    }
    break;

  case SDL_KEYDOWN:
    event.type=event_t::KEY_PRESS;

    // do low level special stuff here
    switch(sdl_event.key.keysym.sym) {

    case SDLK_KP1:
    case SDLK_DOWN:
      event.code = '1';
      break;
    case SDLK_KP2:
      event.code = '2';
      break;
    case SDLK_KP3:
    case SDLK_RIGHT:
      event.code = '3';
      break;
    case SDLK_KP4:
      event.code = '4';
      break;
    case SDLK_KP5:
      event.code = '5';
      break;
    case SDLK_KP6:
      event.code = '6';
      break;
    case SDLK_KP7:
    case SDLK_LEFT:
      event.code = '7';
      break;
    case SDLK_KP8:
      event.code = '8';
      break;
    case SDLK_KP9:
    case SDLK_UP:
      event.code = '9';
      break;


    case SDLK_PRINT:
      /* Try to save a screenshot. */
      dump_screen();
      break;
    default:

      // process keys to report to application
      if(sdl_event.key.keysym.sym >= 32) {
	event.code=sdl_event.key.keysym.unicode;

	// Hajo: some mapping of 'special' keys
	if(sdl_event.key.keysym.sym == SDLK_F1) {
	  event.code = event_t::KEY_F1;
	}


      } else {
	event.code=sdl_event.key.keysym.sym;
      }
      break;
    }


    printf("Key '%c' (%d) was pressed\n", event.code, event.code);
    break;

  case SDL_MOUSEMOTION:
    if(event.buttons != 0) {
      event.type = event_t::MOUSE_DRAG;
    } else {
      event.type = event_t::MOUSE_MOVE;
    }
    event.code = 0;
    event.mpos.x = sdl_event.motion.x;
    event.mpos.y = sdl_event.motion.y;
    break;

  case SDL_KEYUP:
    event.type=event_t::KEY_RELEASE;
    event.code=0; 
    break;

  default:
    // printf("Unbekanntes Ereignis # %d!\n",sdl_event.type);
    event.type=event_t::IGNORE_EVENT;
    event.code=0;
  }

  // event.dump();
}
