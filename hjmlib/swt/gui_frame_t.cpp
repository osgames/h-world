/* 
 * gui_frame_t.cpp
 *
 * Copyright (c) 2001 - 2005 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */

#include <stdlib.h>

#include "system_window_t.h"
#include "window_manager_t.h"
#include "gui_frame_t.h"
#include "gui_container_t.h"
#include "graphics_t.h"
#include "color_t.h"
#include "font_t.h"
#include "event_t.h"
#include "painter_t.h"


void gui_frame_t::set_font(font_t *font)
{
  // Hajo: don't delete old font, they are cached globally!

  this->font = font;
}


font_t *gui_frame_t::get_font()
{
  return font;
}


void gui_frame_t::set_title_font(font_t *font)
{
  title_font = font;
}


font_t * gui_frame_t::get_title_font()
{
  return title_font;
}


void gui_frame_t::draw_close_box(graphics_t *gr, 
				 int x, int y, color_t & color, 
				 bool pushed)
{
  if (pushed) {
    gr->fillbox_wh(x+4,y+4, 7, 1,color_t::BLACK);
    gr->fillbox_wh(x+4,y+5, 1, 6,color_t::BLACK);
    gr->fillbox_wh(x+4,y+11,8, 1,color);
    gr->fillbox_wh(x+11,y+4,1, 7,color);
  } else {
    // centre
    gr->fillbox_wh(x+4,y+4, 7, 1, color);
    gr->fillbox_wh(x+4,y+5, 1, 6, color);
    gr->fillbox_wh(x+4,y+11,8, 1, color_t::BLACK);
    gr->fillbox_wh(x+11,y+4,1, 7, color_t::BLACK);
  }

  gr->fillbox_wh(x,y+1 , 15, 1, color);
  gr->fillbox_wh(x,y+13, 15, 1, color);
  gr->fillbox_wh(x+2,y+2,11, 1, color_t::BLACK);
  gr->fillbox_wh(x+2,y+3, 1,10, color_t::BLACK);
  gr->fillbox_wh(x,   y+2,2,11, color);
  gr->fillbox_wh(x+13,y+2,2,11, color);
}


/**
 * Draws the window frame and all components (by calling update
 * on the container).
 * @author Hj. Malthaner
 */
void gui_frame_t::draw(graphics_t *gr, koord pos)
{
  window_color_t & f = get_colors();

  /*
  color_t title_color2 = f.title.lighter(32);

  // draw window title

  gr->fillbox_wh(pos.x, pos.y, get_size().x, 1, f.title);
  gr->fillbox_wh(pos.x, pos.y+1, get_size().x, 1, color_t::BLACK);
  gr->fillbox_wh(pos.x, pos.y+2, get_size().x, 12, title_color2);
  gr->fillbox_wh(pos.x, pos.y+14, get_size().x, 1, title_color2);
  gr->fillbox_wh(pos.x, pos.y+15, get_size().x, 1, color_t::BLACK);
    
  gr->fillbox_wh(pos.x+15, pos.y+2, 1, 12, color_t::BLACK);

  gr->fillbox_wh(pos.x+get_size().x-2, pos.y+1, 1, 13, title_color2);
  gr->fillbox_wh(pos.x+get_size().x-1, pos.y,  1, 15, color_t::BLACK);

  draw_close_box(gr, pos.x, pos.y, f.title, closing);
  */

  gr->set_font(font);
  gui_window_t::draw(gr, pos);


  koord size = get_size();
  gr->set_clip_wh(pos.x, pos.y, size.x, size.y);


  for(int i=0; i<16; i++) { 
    gr->fillbox_wh(pos.x, pos.y+i, size.x-1, 1, 
		   (i & 1) ? color_t::GREY128 : color_t::GREY160);
  }
  draw_close_box(gr, pos.x, pos.y, color_t::GREY160, closing);
  gr->fillbox_wh(pos.x+size.x-1, pos.y,  1, 15, f.dark);


  painter_t pt (gr);
  pt.draw_string_bold(title_font, title, pos + koord(18, 3), 1,
		      color_t::WHITE, color_t::GREY192);

  if(opaque) {
    // fensterkoerper zeichnen

    gr->fillbox_wh(pos.x, pos.y+size.y-1, size.x, 1, f.dark);    
    gr->fillbox_wh(pos.x, pos.y+16, 1, size.y-16, f.light);
    gr->fillbox_wh(pos.x+size.x-1, pos.y+16, 1, size.y-16, f.dark);
  }
}


bool gui_frame_t::process_event(const event_t &ev)
{
  const koord pos = get_pos();
  const koord size = get_size();

  bool swallowed = false;

  if((ev.mpos.x >= pos.x && ev.mpos.x < pos.x+size.x &&
      ev.mpos.y >= pos.y && ev.mpos.y < pos.y+size.y) ||
     dragging ||
     closing ||
     ev.type == event_t::KEY_PRESS || 
     ev.type == event_t::KEY_RELEASE ||
     ev.type == event_t::WINDOW) {

    
    // title bar ?

    if((ev.mpos.y < pos.y+16 || 
	dragging || 
	closing) && !(ev.type == event_t::KEY_PRESS || 
		      ev.type == event_t::KEY_RELEASE ||
		      ev.type == event_t::WINDOW)) {

      if(ev.type == event_t::BUTTON_PRESS) {
	drag_start = get_pos();

	if(ev.cpos.x < pos.x+16) {
	  closing = true;
	} else {
	  window_manager_t::get_instance()->top_window(this);
	}
	swallowed = true;
      }

      if(ev.type == event_t::BUTTON_RELEASE) {

	if(!dragging && 
	   closing && 
	   ev.mpos.x >= pos.x && ev.mpos.x < pos.x+16 && 
	   ev.mpos.y >= pos.y && ev.mpos.y < pos.y+16) {

	  // clean up
	  set_visible(false);
	  closing = false;
	  dragging = false;

	  // send close event
	  event_t event (ev);
	  event.type = event_t::WINDOW;
	  event.code = event_t::CLOSE;
	  process_event(event);

	  // window structure might have been deleted on
	  // close, so we better return now.
	  return true;
	}

	closing = false;
	dragging = false;
      }

      if(!closing && ev.type == event_t::MOUSE_DRAG) {
	if(ev.mpos.x >= pos.x+16 || 
	   dragging) {
	  dragging = true;

	  set_pos(drag_start+(ev.mpos-ev.cpos));

	  swallowed = true;
	}
      }

    } else {

      // Hajo: only tell components of the top level window
      // about events.
      if(window_manager_t::get_instance()->is_top_event_window(this)) {
	swallowed = gui_window_t::process_event(ev);
      }
    }
  }

  return swallowed;
}


void gui_frame_t::set_title(const char *title) 
{
    this->title = title;
}


const char * gui_frame_t::get_title()  const
{
    return title;
}


gui_frame_t::gui_frame_t(const char *title)
{
  this->title = title;
  
  set_size( koord(200, 100) );
  
  colors.title = color_t(180, 140, 100);
  colors.light = color_t(224, 224, 224);
  colors.body = color_t(192, 192, 192);
  colors.dark = color_t(64, 64, 64);

  set_opaque(true);

  get_container()->set_pos(koord(0,16));

  closing = dragging = false;

  font = system_window_t::get_instance()->get_default_font();
  title_font = font;
}


window_color_t & gui_frame_t::get_colors()
{
  return colors;
}
