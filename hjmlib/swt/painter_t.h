/* 
 * painter_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the SWT project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef PAINTER_T_H
#define PAINTER_T_H

#include "primitives/koord.h"

class graphics_t;
class color_t;
class font_t;

/**
 * Higher level painting tools that utilize the lower level
 * graphics class to paint more complex shapes
 *
 * @author Hj. Malthaner
 */
class painter_t {
 private:

  graphics_t * gr;

 public:
  
  painter_t(graphics_t *gr) {this->gr = gr;};


  /**
   * Paints a box
   * @author Hj. Malthaner
   */
  void box_wh(koord pos, koord size, color_t & color);


  /**
   * Paints a 3d box
   * @author Hj. Malthaner
   */
  void ddd_box_wh(koord pos, koord size, 
		  color_t & color_light, color_t & color_dark);

  /**
   * Draws a string fake bold
   * @author Hj. Malthaner
   */
  void draw_string_bold(const font_t * font,
			const char * text, koord pos, int spacing, 
			color_t & color1,
			color_t & color2);
};

#endif
