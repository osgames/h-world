/* Copyright by Hj. Malthaner */

#ifndef COLOR_T_H
#define COLOR_T_H

class system_color_t;

class color_t {
private:

    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char need_system;

    unsigned int system_color;
    
public:

    static color_t BLACK;
    static color_t WHITE;
    static color_t GREY32;
    static color_t GREY64;
    static color_t GREY96;
    static color_t GREY128;
    static color_t GREY160;
    static color_t GREY192;
    static color_t GREY224;


    color_t(unsigned char r, unsigned char g, unsigned char b);


    color_t lighter(int n) const;



    /**
     * Determine system representation of a color.
     * Can only be called after the system window was opened!
     * @author Hj. Malthaner
     */
    unsigned int get_system_color(const system_color_t *syscol);


};
#endif //COLOR_T_H
