/* Copyright by Hj. Malthaner */

#ifndef EVENT_T_H
#define EVENT_T_H

#ifndef koord_h
#include "primitives/koord.h"
#endif


/**
 * Input event, system independant format.
 * @author Hansj�rg Malthaner
 */
class event_t {
public:

    /**
     * The event types.
     * @author Hansj�rg Malthaner
     */
    enum event_type_t {
      NO_EVENT,                  // poll reported in no event
      IGNORE_EVENT,              // an unknown event was detected
      KEY_PRESS,                 // a key was pressed
      KEY_RELEASE,               // a key was released
      BUTTON_PRESS,              // a mouse button was pressed 
      BUTTON_RELEASE,            // a mouse button was released
      MOUSE_MOVE,                // the mouse was moved
      MOUSE_DRAG,
      CONTAINER,                 // container event, i.e. enter, leave
      WINDOW                     // window event, i.e. close
    };


    /**
     * depending on the event type the code field may have one of
     * the following values. The buttons are organised as bitfield
     * values and the state can be read from the 'buttons' variable
     * @author Hansj�rg Malthaner
     */
    enum event_code_t {BUTTON_LEFT=1, 
		       BUTTON_MID=2, 
		       BUTTON_RIGHT=4, 
		       BUTTON_UNKNOWN=8,
                       ENTER,                // CONTAINER
                       LEAVE,                // CONTAINER
		       CLOSE,                // WINDOW
		       SHOW                  // WINDOW
    };

    enum event_keycode_t {
      KEY_F1 = 256
    };


    enum event_type_t type;

    /**
     * Translates coordinates. 
     * @return new event_t object
     * @author Hansj�rg Malthaner
     */
    event_t translate(koord offset) const;

    /**
     * Detailed information, i.e. which button was pressed
     * @author Hansj�rg Malthaner
     */
    int code;


    /**
     * Current state of mouse buttons
     * @author Hansj�rg Malthaner
     */
    int buttons;


    koord mpos;

    /**
     * Set to mouse coordinates if a mouse button is pressed
     * @author Hansj�rg Malthaner
     */
    koord cpos;

    event_t();


    void dump();
};
#endif //EVENT_T_H
