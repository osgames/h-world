#include "color_t.h"


/**
 * Farbdaten f�r die Fensterahmen und -k�rper.
 * @author Hj. Malthaner
 * @version $Revision$
 */
class window_color_t
{
 public:
  color_t title;
  color_t light;
  color_t body;
  color_t dark;

  window_color_t() : title(160,160,200), light(200,200,200), body(140,140,140), dark(80,80,80) {};
};

