/* Copyright by Hj. Malthaner */

#ifndef X11_WINDOW_T_H
#define X11_WINDOW_T_H

#include "system_window_t.h"
#include "event_t.h"

#include <X11/Xlib.h>

class koord;

/**
 * Simple windowing toolkit system window class for X11 based systems.
 * @author Hj. Malthaner
 */
class x11_window_t : public system_window_t 
{
private:
  event_t event;

  /**
   * x11 display
   * @author Hj. Malthaner
   */
  Display *x11_display;


  /**
   * x11 window
   * @author Hj. Malthaner
   */
  Window x11_window;


  /**
   * x11 graphics context
   * @author Hj. Malthaner
   */
  GC x11_gc;


  /**
   * x11 screen
   * @author Hj. Malthaner
   */
  long    x11_screen;

  /**
   * x11 standard cursor
   * @author Hj. Malthaner
   */
  Cursor standard_cursor;

  /**
   * selfmade invisible cursor
   * @author Hj. Malthaner
   */
  Cursor invisible_cursor;                    

  /**
   * Display color depth.
   * @author Hj. Malthaner
   */
  int display_depth;

  /**
   * Inits the cursor data
   * @author Hj. Malthaner
   */
  void init_cursors();

  /**
   * Internal input event handling
   * @author Hj. Malthaner
   */
  void get_event_aux(bool wait);

  void dump_screen();

protected:    

  virtual bool system_init();

  virtual bool system_exit();

public:

  virtual unsigned int get_system_color(unsigned int r, unsigned int g, unsigned int b) const;


  virtual ~x11_window_t();

  
  virtual koord get_mouse_pos() {return event.mpos;};


  /**
   * Paints rectangular area onto screen.
   *
   * @author Hj. Malthaner
   */
  virtual void redraw(int x, int y, int w, int h);



  /**
   * Get system time in milliseconds since program start
   *
   * @author Hj. Malthaner
   */
  virtual unsigned long get_time() const;


  /**
   * Sleeps (delays program execution) for some milliseconds
   *
   * @author Hj. Malthaner
   */
  virtual void sleep(unsigned long milliseconds);


  /**
   * Polls for input events. Do not store references to events, this
   * return always a reference to the same input event object.
   * @author Hj. Malthaner
   */
  virtual event_t & poll_event();


  virtual event_t & get_event();

};
#endif //X11_WINDOW_T_H
