#ifndef system_color_t_h
#define system_color_t_h

/**
 * Defines system specific color format
 * @author Hj. Malthaner
 */
class system_color_t
{
 public:
  /**
   * Convert RGB triple into system color
   * @author Hj. Malthaner
   */
  virtual unsigned int get_system_color(unsigned int r, unsigned int g, unsigned int b) const = 0;


};

#endif // system_color_t_h
