/* 
 * graphics_t.h
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef GRAPHICS_T_H
#define GRAPHICS_T_H

#ifndef colortype_t_h
#include "colortype.h"
#endif

#include "pixeltype.h"

#ifndef CLIP_DIMENSION_T_H
#include "clip_dimension_t.h"
#endif

#ifndef koord_h
#include "primitives/koord.h"
#endif


class font_t;
class image_t;
class tile_descriptor_t;
class system_color_t;
class color_t;

/**
 * Graphics are rectangular regions of the system window which can serve
 * as drawing surfaces. Overlapping graphics are stacked.
 *
 * @author Hj. Malthaner
 */

class graphics_t {
private:

    /**
     * Coordinate in the system window
     * @author Hj. Malthaner
     */
    koord pos;


    /**
     * Width of this graphics.
     * @author Hj. Malthaner
     */
    int width;


    /**
     * Height of this graphics.
     * @author Hj. Malthaner
     */
    int height;


    int depth;


    /**
     * The font used for drawing strings
     * @author Hj. Malthaner
     */
    font_t * font;


    /**
     * data is a external buffer, getting set by set_data
     * this class is not owner of data, that means data must be
     * allocated and free'd by someone else.
     * @author Hj. Malthaner
     */
    PIXEL * data;


    /**
     * 16 for RGB 565, 15 for RGB 555
     * @author Hj. Malthaner
     */    
    int bit_depth;


    /**
     * graphics are system dependant and need to ask the system
     * for color representations
     * @author Hj. Malthaner
     */
    system_color_t *syscol;


    /**
     * En/disable triangle border dithering
     * @author Hj. Malthaner
     */
    bool dither;


    /**
     * clipping rectangle
     * @author Hj. Malthaner
     */
    clip_dimension_t clip_rect;


    void pixcopy_clipped(unsigned short *dest, 
			 const unsigned short *src, int xpos, int len) const;

    /**
     * This sets width < 0 if the range is out of bounds
     * so check the value after calling and before displaying
     * @author Hj. Malthaner
     */
    static int clip_wh(int *x, int *width, 
		       const int min_width, const int max_width);


    void draw_tile_clipped(const tile_descriptor_t * tile, int xpos, int ypos);
    void draw_tile_unclipped(const tile_descriptor_t * tile, int xpos, int ypos);

    void draw_tile_aux(const tile_descriptor_t * tile, int xpos, int ypos);


    void triangle_line15(int y, int start, int end, int b1, int b2, 
			 int xoff, int yoff,
			 const PIXEL * img_data);

    void triangle_line16(int y, int start, int end, int b1, int b2, 
			 int xoff, int yoff,
			 const PIXEL * img_data);



    /**
     * Draws a glyph at x,y in color cval using the font that is
     * currently set on this graphics object
     * @author Hj. Malthaner
     */
    int draw_glyph(int x, int y, int glyph, PIXEL cval);


    /**
     * Unclipped pixel setting
     * @author Hj. Malthaner
     */
    void draw_pixel_nc(int x, int y, PIXEL RGB) {
      data[y*width+x] = RGB;
    }


    /**
     * Sets a pixel, does clipping
     * @author Hj. Malthaner
     */
    void draw_pixel(int x, int y,  PIXEL RGB);


    void draw_top_tri(koord p0, koord p1, koord p2,
		      int b0, int b1, int b2,
		      int xoff, int yoff,
		      const image_t *img);

    void draw_bottom_tri(koord p0, koord p1, koord p2,
			 int b0, int b1, int b2,
			 int xoff, int yoff,
			 const image_t *img);
public:


    const system_color_t * get_system_color() const {return syscol;};


    /**
     * En/disable triangle border dithering
     * @author Hj. Malthaner
     */
    void set_border_dither(bool yesno);

    
    unsigned short * get_data() const {return data;};


    /**
     * sets a new buffer. Should not be used by classes outside the SWT!
     * @author Hj. Malthaner
     */
    void set_data(unsigned short *d) {data = d;};


    void set_font(font_t * font) {this->font = font;};
    const font_t * get_font() const {return font;};

    /**
     * Gets the width.
     * @author Hj. Malthaner
     */
    int get_width() const {return width;};


    /**
     * Gets the height.
     * @author Hj. Malthaner
     */
    int get_height() const {return height;};


    /**
     * Gets the position in the system window at which this
     * graphics wil be displayed.
     * @author Hj. Malthaner
     */
    koord get_pos() const {return pos;};


    /**
     * Sets the position in the system window at which this
     * graphics wil be displayed.
     * @author Hj. Malthaner
     */
    void set_pos(koord pos) {this->pos = pos;};


    /**
     * Gets the size of this graphics.
     * @author Hj. Malthaner
     */
    koord get_size() const {return koord(width, height);};


    void set_clip_wh(int x, int y, int w, int h);

    const clip_dimension_t & get_clip(void) const {return clip_rect;};

    enum colortype get_colortype() const;


    graphics_t(system_color_t *syscol, int width, int height, int depth);

    graphics_t(int width, int height, enum colortype);


    ~graphics_t();



    /**
     * Draws a tile onto this graphics at location xpos, ypos
     * relative to the top-left corner of this graphics.
     * @author Hj. Malthaner
     */
    void draw_tile(const tile_descriptor_t * tile, int xpos, int ypos);


    void draw_tile_transparent(const tile_descriptor_t * tile, 
			       int xpos, int ypos, int shade);


    void draw_tile_shaded(const tile_descriptor_t * tile, 
			  int xpos, int ypos, int shade);


    /**
     * Draws a tile onto this graphics at location xpos, ypos
     * relative to the top-left corner of this graphics. Recolors
     * the special areas to a colormap given as 32-bit padded RGB24
     * @author Hj. Malthaner
     */
    void draw_tile_recolored(const tile_descriptor_t * tile, 
			     int xpos, int ypos, int shade, 
			     const int * color_tab);


    /**
     * Draws a tile semi-transparent onto this graphics at location xpos, ypos
     * relative to the top-left corner of this graphics. Recolors
     * the special areas to a colormap given as 32-bit padded RGB24
     * @author Hj. Malthaner
     */
    void draw_tile_transparent_and_recolored(const tile_descriptor_t * tile, 
					     int xpos, int ypos, int shade, 
					     const int * color_tab);


    /**
     * Draws an image onto this graphics at location xpos, ypos
     * relative to the top-left corner.
     * @author Hj. Malthaner
     */
    void draw_image(image_t *img, int xpos, int ypos);


    /**
     * Draws an image onto this graphics at location xpos, ypos
     * Scales the image to fit into w, h
     * relative to the top-left corner.
     * @author Hj. Malthaner
     */
    void draw_image(image_t *img, int xpos, int ypos, int w, int h,
		    PIXEL transkey);


    /**
     * Sets a pixel, does clipping
     * @author Hj. Malthaner
     */
    void draw_pixel(int x, int y, color_t &color);


    /**
     * Zeichnet gefuelltes Rechteck
     * @author Hj. Malthaner
     */
    void fillbox_wh(int x, int y, int w, int h, color_t &color);


    /**
     * Paint gouraud shaded triangle. The points must be delivered in ascending
     * y order, that means point[0] must have the smallest y coordinate and
     * point[2] must have the largest y coordinate.
     *
     * Brightness values should be in range 0..32
     *
     * @author Hj. Malthaner
     */
    void graphics_t::draw_triangle(const koord* points, 
				   const signed char *shade, 
				   int xoff, int yoff,
				   const image_t *img);
};
#endif //GRAPHICS_T_H
