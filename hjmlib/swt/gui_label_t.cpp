#include "gui_label_t.h"


#include "system_window_t.h"
#include "font_t.h"
#include "graphics_t.h"


gui_label_t::gui_label_t() : color(color_t::BLACK)
{
  text = "";

  font = system_window_t::get_instance()->get_default_font();
}


gui_label_t::gui_label_t(const char *t) : color(color_t::BLACK)
{
  text = t;

  font = system_window_t::get_instance()->get_default_font();
}


void gui_label_t::set_text(const char *text) {
  this->text = text;
  redraw();
};


/**
 * Draws the component
 * @author Hj. Malthaner
 */
void gui_label_t::draw(graphics_t *gr, koord pos)
{
  pos += get_pos();
  font->draw_string(gr, text, pos.x, pos.y, 0, color);
}

