/* Copyright by Hj. Malthaner */

#ifndef TILE_DESCRIPTOR_T_H
#define TILE_DESCRIPTOR_T_H



/*
 * pixel value type, RGB 555
 */
typedef unsigned short PIXVAL;
 

/**
 * Descriptor for a graphical tile.
 *
 * @author Hj. Malthaner
 */

class tile_descriptor_t {
public:

    /** 
     * Left offset
     */
    unsigned char x;

    /** 
     * Top offset
     */
    unsigned char y;

    /**
     * Image width
     */
    unsigned char w;

    /**
     * Image height
     */
    unsigned char h;

    /**
     * Data length in entities of PIXVAL size
     */
    int len;         


    /**
     * Tile raster width
     * @author Hj. Malthaner
     */
    int rwidth;


    /**
     * Image data
     */
    PIXVAL * data;



    tile_descriptor_t() {
      x=y=w=h = 0;
      len = rwidth = 0;
      data = 0;
    };
};
#endif //TILE_DESCRIPTOR_T_H
