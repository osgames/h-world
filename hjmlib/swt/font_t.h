/* 
 * font_t.h
 *
 * Copyright (c) 2001 - 2005 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef FONT_T_H
#define FONT_T_H

class graphics_t;
class color_t;

/**
 * A font is a set of glyphs.
 * @author Hj. Malthaner
 */
class font_t {
private:

    unsigned short *data;
    unsigned char *widths;

    int get_glyph_width(int glyph);


    /**
     * Reads a .bdf or .hex font from a file
     * @prop in case of a .hex font this tells if it is a proportional font or not
     * @author Hj. Malthaner
     */
    void load_from_file(const char *filename, bool proportional);


    font_t(int w, int h);
    ~font_t();


    /**
     * Reads a .hex font file
     * @author Hj. Malthaner
     */
    void load_from_hex(const char * filename);


    /**
     * Reads a .bdf font file
     * @author Hj. Malthaner
     */
    void load_from_bdf(const char * filename);


    /**
     * Draws a glyph at x,y in color cval
     * @author Hj. Malthaner
     */
    int draw_glyph(graphics_t * gr, const int x, int y, 
		   const int glyph, const unsigned short cval) const;


protected:

    int width;
    int height;

public:    

    /**
     * Width and height are ignored in case of BDF fonts, BDF
     * fonts contain font geometry in the file and the reader
     * will read and use the real font geometry
     * @author Hj. Malthaner
     */
    static font_t * load(const char *filename, 
			 bool proportional, int w, int h);


    int get_height() const {return height;};
    int get_width() const {return width;};

    int get_width(int glyph) const {return widths[glyph];};


    /**
     * This is needed to allow other classes to render the glyphs
     * @autor Hj. Malthaner 
     */
    const unsigned short * get_glyph_data(int glyph) const {
      return data + glyph * height;
    }


    /**
     * Determines the width of a string written in this font
     * @autor Hj. Malthaner 
     */
    int get_string_width(const char * text, int spacing) const;


    /**
     * Draws a string at x,y in color color onto the graphics object
     * @author Hj. Malthaner
     */
    void draw_string(graphics_t * gr, const char * text, 
		     int x, int y, int spacing, color_t & color) const;



};
#endif //FONT_T_H
