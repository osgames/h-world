/* Copyright by Hj. Malthaner */

#include "gui_container_t.h"
#include "graphics_t.h"
#include "event_t.h"
#include "color_t.h"

#include "tpl/slist_tpl.h"


gui_container_t::gui_container_t() 
{
  background = 0;
  opaque = true;
  color = new color_t(color_t::GREY128);
  components = new slist_tpl <gui_component_t *> (); 
}


gui_container_t::~gui_container_t()
{
  delete components;
  components = 0;

  delete color;
  color = 0;
}


void gui_container_t::set_color(const color_t & c)
{
  delete color;
  color = new color_t(c);
}


void gui_container_t::draw(graphics_t *gr, koord pos)
{
  const koord screen_pos = get_pos() + pos;

  if(opaque) {
    if(background) {
      for(int j=0; j<get_size().y; j+=64) {
	for(int i=0; i<get_size().x; i+=64) {
	  gr->draw_tile(background, screen_pos.x+i, screen_pos.y+j);
	}
      }
    } else {
      gr->fillbox_wh(screen_pos.x, screen_pos.y, 
		     get_size().x, get_size().y, *color);
    }
  }
  
  slist_iterator_tpl<gui_component_t *> iter (components);
  
  while(iter.next()) {
    gui_component_t * c = iter.get_current();

    koord xy = c->get_pos() + screen_pos;
    koord wh = c->get_size();

    gr->set_clip_wh(xy.x, xy.y, wh.x, wh.y);

    iter.get_current()->update(gr, screen_pos);
  }
}


bool gui_container_t::process_event(const event_t & ev)
{
  // printf("gui_container_t::process_events(): called\n");

  bool swallowed = gui_component_t::process_event(ev);


  // printf("%p:\tevent type=%d code=%d swallowed=%d\n", this, ev.type, ev.code, swallowed); 


  if(!swallowed) {
    event_t ev2 = ev.translate(-get_pos());

    slist_iterator_tpl <gui_component_t *> iter (components);

    bool ok = iter.next();

    // Hajo: first generate LEAVE events - all leaves must be reported
    //       before ENTER is generated

    while(ok) {
      gui_component_t *c = iter.get_current();

      // Hajo: advance iterator, so that we can remove the current object
      // safely
      ok = iter.next();

      const bool new_in = c->is_inside(ev2.mpos);
      const bool old_in = c->is_inside(old_mpos);

      if(!new_in && old_in) {
	event_t event (ev2);
	event.type = event_t::CONTAINER;
	event.code = event_t::LEAVE;
	c->process_event(event);
      }
    }


    iter.begin();
      
    ok = iter.next();

    while(ok && !list_dirty) {
      gui_component_t *c = iter.get_current();

      // Hajo: advance iterator, so that we can remove the current object
      // safely
      ok = iter.next();


      // printf("gui_container_t::process_events(): processing %p\n", c);

      const bool new_in = c->is_inside(ev2.mpos);
      const bool old_in = c->is_inside(old_mpos);

      if(new_in && !old_in) {
	event_t event (ev2);
	event.type = event_t::CONTAINER;
	event.code = event_t::ENTER;
	c->process_event(event);
      }

      if(swallowed == false && 
	 (new_in || 
	  c->is_inside(ev2.cpos) || 
	  ev2.type == event_t::KEY_PRESS || 
	  ev2.type == event_t::KEY_RELEASE)) {

	// Komponente getroffen
	swallowed |= c->process_event(ev2);

	// printf("comp=%p:\tevent type=%d code=%d swallowed=%d\n", c, ev.type, ev.code, swallowed); 
      }
    }

    old_mpos = ev2.mpos;
  }

  list_dirty = false;

  // printf("gui_container_t::process_events(): done\n");

  return swallowed;
}


void gui_container_t::remove_all() 
{
  components->clear();
  list_dirty = true;
}


void gui_container_t::remove_component(gui_component_t * c)
{
  components->remove(c);
  list_dirty = true;
}


void gui_container_t::add_component(gui_component_t * c)
{
  //  if(components.contains(c)) {
  //    abort();
  //  }

  components->insert(c);
  list_dirty = true;
}


const slist_tpl <gui_component_t *> * gui_container_t::get_children() const
{
  return components;
}
