/* 
 * gui_check_t.cpp
 *
 * Copyright (c) 2001 - 2005 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */

#include "gui_check_t.h"

#include "system_window_t.h"
#include "font_t.h"
#include "graphics_t.h"
#include "event_t.h"


gui_check_t::gui_check_t() : color(color_t::BLACK)
{
  yesno = false;

  font = system_window_t::get_instance()->get_default_font();
}


gui_check_t::gui_check_t(bool y, const char *text) : color(color_t::BLACK)
{
  yesno = y;
  this->text = text;

  font = system_window_t::get_instance()->get_default_font();
}


void gui_check_t::set_text(const char *text) {
  this->text = text;
  redraw();
};


void gui_check_t::set_selected(bool y) 
{
  yesno = y;
  redraw();
};


/**
 * Draws the component
 * @author Hj. Malthaner
 */
void gui_check_t::draw(graphics_t *gr, koord pos)
{
  pos += get_pos();
  
  gr->fillbox_wh(pos.x, pos.y, 9, 9, color_t::GREY160);

  if(yesno) {
    gr->fillbox_wh(pos.x+2, pos.y+2, 5, 5, color_t::BLACK);
  }

  font->draw_string(gr, text, pos.x+16, pos.y, 0, color);
}

/**
 * Process an event.
 * @return true if event was swallowed, false otherwise
 * @author Hj. Malthaner
 */
bool gui_check_t::process_event(const event_t &ev)
{
  bool ok = false;

  if(ev.type == event_t::BUTTON_RELEASE) { 
    yesno = !yesno;
    redraw();
    call_listeners();
    ok = true;
  }

  return ok;
}
