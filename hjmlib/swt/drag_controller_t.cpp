#include "tpl/slist_tpl.h"

#include "system_window_t.h"
#include "window_manager_t.h"
#include "drag_controller_t.h"
#include "gui_frame_t.h"
#include "drop_source_t.h"
#include "drop_target_t.h"
#include "event_t.h"


drag_controller_t::drag_controller_t()
{
  frame = 0;
  item = 0;
  source = 0;

  targets = new slist_tpl <drop_target_t *> (); 
}


drag_controller_t::~drag_controller_t() 
{
  delete targets;
  targets = 0;
  frame = 0;
  item = 0;
  source = 0;
}


void drag_controller_t::set_frame(gui_frame_t *frame)
{
   this->frame = frame;
   
   frame->add_event_listener(this);
}


void drag_controller_t::add_drop_target(drop_target_t *target)
{
   if(target) {
      targets->insert(target);
      target->set_drag_control(this);
   }
}


void drag_controller_t::remove_drop_target(drop_target_t *target)
{
   if(target) {
      targets->remove(target);
      target->set_drag_control(0);
   }
}


void drag_controller_t::start_dragging(drop_source_t * s,
				       drawable_t *drawable)
{
   if(drawable) {
      source = s;

      item = drawable;   
      item->set_pos(system_window_t::get_instance()->get_mouse_pos() - item->get_size()/2);
      item->set_visible(true);
      window_manager_t::get_instance()->add_window(drawable);
   }
}


/**
 * Tells this controller to abort the current dragging action
 * @author Hj. Malthaner
 */
void drag_controller_t::stop_dragging()
{
  // printf("source=%p, item=%p\n", source, item);

  if(source) {
    if(item) {
      source->stop_dragging(item);
      item = 0;
    }
    source = 0;
  }
}



/**
 * Process an event.
 * @return true if event was swallowed, false otherwise
 * @author Hj. Malthaner
 */
bool drag_controller_t::process_event(const event_t &ev)
{
  bool swallow = false;
   
  if(item) {
    swallow = true;
    item->set_pos(system_window_t::get_instance()->get_mouse_pos() - item->get_size()/2);
    
    if(ev.type == event_t::BUTTON_RELEASE ||
       ev.type == event_t::MOUSE_MOVE) { 

      slist_iterator_tpl <drop_target_t *> iter (targets);
      gui_component_t * component = frame->get_component_at(system_window_t::get_instance()->get_mouse_pos());
   
      while(iter.next()) {
         
	if(iter.get_current() == component) {  
	  koord cpos = item->get_pos() - 
	    frame->get_component_location(component);
	    
	  // printf("%d,%d\n", cpos.x, cpos.y);
	  
	  // check for drop
	  if(ev.type == event_t::BUTTON_RELEASE) {
	    if(iter.get_current()->drop(item, cpos)) {
	      item->set_visible(false);
	      item->close();
	      
	      // XXX should item be deleted here?

	      item = 0;
	      source = 0;
	    }
	  } else {
	    // only hover (with item)
	    iter.get_current()->hover(item, cpos);
	  }

	} else {
	  // hover without item
	  iter.get_current()->hover(0, koord(-1,-1));
	}
      }
    }
  }
   
  return swallow;
}
