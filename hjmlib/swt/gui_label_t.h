/* Copyright by Hj. Malthaner */

#ifndef GUI_LABEL_T_H
#define GUI_LABEL_T_H

#include "gui_component_t.h"
#include "color_t.h"
class koord;

class font_t;

class gui_label_t : public gui_component_t {
 private:
  
  const char * text;

  font_t * font;
  color_t color;

 public:
  

  void set_text(const char *text);
  void set_font(font_t *font) {this->font = font;};
  void set_color(color_t color) {this->color = color;};


  gui_label_t();
  gui_label_t(const char *text);


  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);

};


#endif
