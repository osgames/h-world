/* 
 * gui_container.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 * This file is part of the Simutrans project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef gui_container_h
#define gui_container_h


#ifndef swt_gui_component_h
#include "gui_component_t.h"
#endif


class koord;
class event_t;
class color_t;
class graphics_t;
class tile_descriptor_t;
template <class T> class slist_tpl;


/**
 * Ein Beh�lter f�r andere gui_komponenten. Ist selbst eine
 * gui_komponente, kann also geschachtelt werden.
 *
 * @author Hj. Malthaner
 * @date 03-Mar-01
 * @version $Revision$
 */
class gui_container_t : public gui_component_t
{
private:

  /**
   * Old (previous) mouse position
   * @author Hj. Malthaner
   */     
  koord old_mpos;
       
  color_t * color;
  const tile_descriptor_t * background;


  unsigned char opaque;

  /**
   * set by operations that modify the component list.
   * must be considered by all opration that iterate that list.
   * @author Hj. Malthaner
   */     
  unsigned char list_dirty;
  

 protected:


  /**
   * The components in this container
   * @author Hj. Malthaner
   */     
  slist_tpl <gui_component_t *> * components;


 public:

  gui_container_t();
  virtual ~gui_container_t();


  void set_opaque(unsigned char yesno) {opaque=yesno;};
  void set_color(const color_t & c);
  void set_background(const tile_descriptor_t *t) {background = t;};
  
  /**
   * F�gt eine Komponente zum Container hinzu.
   * @author Hj. Malthaner
   */     
  void add_component(gui_component_t * component);
  

  /**
   * Entfernt eine Komponente aus dem Container.
   * @author Hj. Malthaner
   */     
  void remove_component(gui_component_t * component);


  void remove_all();


  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t & ev);


  /**
   * Zeichnet die Komponente
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);

  virtual const slist_tpl <gui_component_t *> * get_children() const;
};

#endif
