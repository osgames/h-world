/* 
 * window_manager_t.cpp
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib/SWT project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "window_manager_t.h"
#include "system_window_t.h"
#include "drawable_t.h"
#include "graphics_t.h"

#include "font_t.h"
#include "color_t.h"
#include "event_t.h"

#include "tpl/slist_tpl.h"
#include "tpl/minivec_tpl.h"



window_manager_t * window_manager_t::single_instance = 0;


void window_manager_t::remove_all_windows() 
{
  windows->clear();
  redraw();
}


bool window_manager_t::is_window_managed(drawable_t *drable) const
{
  return windows->contains(drable);
}


/**
 * Checks if the given window is the top level window that
 * consumes events
 * @author Hj. Malthaner
 */
bool window_manager_t::is_top_event_window(drawable_t *drable) const
{
  const drawable_t * drable_top = 0;

  for(int i=windows->count()-1; i>=0 && drable_top == 0; i--) { 
    if(windows->at(i)->is_visible() && windows->get(i)->consume_events()) {
      drable_top = windows->get(i);
    }
  }
  
  
  return drable_top != 0 && drable_top == drable;
}


void window_manager_t::add_window(drawable_t *drable) 
{
  if(drable != NULL) {

    windows->append(drable);

    redraw(drable->get_pos(), drable->get_size());

    event_t ev;
    ev.type = event_t::WINDOW;
    ev.code = event_t::SHOW;

    drable->process_event(ev);
  } else {
    // dbg->warning("window_manager_t::add_window()",
    //		 "Drawable to add is NULL."); 
  }
}


void window_manager_t::remove_window(drawable_t *drable) 
{
  // printf("window_manager_t::remove_window(): removing %p", drable); 

  if(drable != NULL && windows->contains(drable)) {
    kill_list->append(drable);

    redraw(drable->get_pos(), drable->get_size());

  } else {
    // dbg->warning("window_manager_t::remove_window()",
    //		 "Drawable to remove is NULL."); 
  }
}


/**
 * Brings a drawable (window) to front.
 * @author Hj. Malthaner
 */
void window_manager_t::top_window(drawable_t *drable)
{
  windows->remove(drable);
  windows->append(drable);

  redraw(drable->get_pos(), drable->get_size());
}


int window_manager_t::get_height()
{
  return syswin->get_height();
}


int window_manager_t::get_width()
{
  return syswin->get_width();
}


void window_manager_t::redraw(int x, int y, int w, int h) 
{
  // printf("window_manager_t::redraw(): Area x=%d y=%d w=%d h=%d\n", x,y,w,h); 

  if(x < 0) {
    w += x;
    x = 0;
  }

  if(y < 0) {
    h += y;
    y = 0;
  }

  const int width = syswin->get_width();
  const int height = syswin->get_height();

  if(x+w > width) {
    w = width - x;
  }

  if(y+h > height) {
    h = height - y;
  }
  

  graphics_t * gr = syswin->get_graphics();

  gr->set_clip_wh(x, y, w, h);

  for(int i=0; i<windows->count(); i++) {
    drawable_t *drable = windows->at(i);

    if(!kill_list->contains(drable) && 
       drable->is_visible()) 
    {
      const koord pos = drable->get_pos();
      const koord size = drable->get_size();

      if(!(pos.x > x+w-1 || pos.y > y+h-1 ||
	   pos.x+size.x-1 < x || pos.y+size.y-1 < y)) 
      {
	drable->update(gr, pos);
      }
    }
  }

  if(tooltip) {
    const koord pos = tooltip->get_pos();    
    tooltip->update(gr, pos);
  }

  syswin->redraw(x, y, w, h);
}


void window_manager_t::redraw() 
{
  redraw (0, 0, get_width(), get_height());
}


window_manager_t::window_manager_t(system_window_t *sys)
{
  windows = new minivec_tpl <drawable_t *> (32);
  kill_list = new minivec_tpl <drawable_t *> (32);

  syswin = sys;

  tooltip = 0;

  // dbg->message("window_manager_t::window_manager_t()",
  //	       "syswin is %p", syswin); 

  // drawables need to know the window manager
  drawable_t::set_window_manager(this);

  // save last cretaed instzance for public access
  single_instance = this;
}


window_manager_t::~window_manager_t()
{
  remove_all_windows();
  syswin = NULL;

  delete windows;
  windows = 0;

  delete kill_list;
  kill_list = 0;
}


/**
 * Cleans up list of removed windows. Only needed to be called if
 * windows are removed outside of the event handling!
 * @author Hj. Malthaner
 */
void window_manager_t::process_kill_list()
{
  if(kill_list->count()) {

    minivec_iterator_tpl <drawable_t *> killer (kill_list);
    
    while(killer.next()) {
      if(!windows->remove(killer.get_current())) {
	// dbg->warning("window_manager_t::remove_window()",
	//     "Window was not in list."); 
      }
    }

    kill_list->clear();
  }
}


bool window_manager_t::process_event(event_t & ev)
{
  // puts("plopp1");

  bool swallowed = false;

  if(ev.type != event_t::NO_EVENT &&
     ev.type != event_t::IGNORE_EVENT) {

    for(int i=windows->count()-1; i>=0 && kill_list->count() == 0 && !swallowed; i--) {

      // printf("%d\n", i);

      drawable_t *drable = windows->at(i);

      if(drable->is_visible() && drable->consume_events()) {
	swallowed = drable->process_event(ev);
      }
    }
  }

  process_kill_list();

  return swallowed;
}


/**
 * Checks system window for new events and hands them to the 
 * drawables.
 * @author Hj. Malthaner
 */
event_t & window_manager_t::poll_and_process_events()
{
  event_t & ev = syswin->poll_event();

  if(process_event(ev)) {
    // swallowed events
    ev.type = event_t::IGNORE_EVENT;
  }

  return ev;
}



event_t & window_manager_t::get_and_process_events()
{
  event_t & ev = syswin->get_event();

  if(process_event(ev)) {
    // swallowed events
    ev.type = event_t::IGNORE_EVENT;
  }

  return ev;
}


/**
 * Show a tool tip like text
 * @author Hj. Malthaner
 */
void window_manager_t::show_tool_tip(drawable_t * tt, koord pos)
{
  tooltip = tt;
  tooltip->set_pos(pos);
  redraw(pos, tt->get_size());
}

    
/**
 * Hide the tool tip text
 * @author Hj. Malthaner
 */
void window_manager_t::unshow_tool_tip()
{
  if(tooltip) {
    const koord pos = tooltip->get_pos();
    const koord size = tooltip->get_size();

    tooltip = 0;

    redraw(pos, size);
  }
}
