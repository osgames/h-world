/*
 * gui_component.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 */

#ifndef swt_gui_component_h
#define swt_gui_component_h


#ifndef EVENT_LISTENER_T_H
#include "event_listener_t.h"
#endif

#ifndef koord_h
#include "primitives/koord.h"
#endif


class event_t;
class graphics_t;
template <class T> class slist_tpl;


/**
 * Komponenten von Fenstern sollten von dieser Klassse abgeleitet werden.
 *
 * @autor Hj. Malthaner
 * @version $Revision$
 */
class gui_component_t : public event_listener_t
{
private:
    /**
     * Gr��e der Komponente.
     * @author Hj. Malthaner
     */
    koord size;


    /**
     * Position der Komponente. Eintraege sind relativ zu links/oben der
     * umgebenden Komponente.
     * @author Hj. Malthaner
     */
    koord pos;


    /**
     * Last screen position where this component was painted.
     * @author Hj. Malthaner
     */
    koord screenpos;


    slist_tpl <event_listener_t *> * listeners;

public:                            


    gui_component_t();


    /**
     * Virtueller Destruktor, damit Klassen sauber abgeleitet werden k�nnen
     * @author Hj. Malthaner
     */
    virtual ~gui_component_t();


    /**
     * Sets the position. Triggers a redraw.
     * @author Hj. Malthaner
     */
    void set_pos(koord pos);


    /**
     * Sets the position. Triggers no redraw.
     * @author Hj. Malthaner
     */
    void set_pos_quiet(koord pos) {this->pos = pos;};
 

    /**
     * Gets the position
     * @author Hj. Malthaner
     */
    koord get_pos() const {
	return pos;
    }


    /**
     * Gets the onscreen position. Only valid after the component was 
     * drawn onscreen.
     * @author Hj. Malthaner
     */
    koord get_screenpos() const {
	return screenpos;
    }


    /**
     * Sets the size
     * @author Hj. Malthaner
     */
    virtual void set_size(koord size);


    /**
     * Gets the size
     * @author Hj. Malthaner
     */
    koord get_size() const {
	return size;
    }

    
    /**
     * Pr�ft, ob eine Position innerhalb der Komponente liegt.
     * @author Hj. Malthaner
     */
    virtual bool is_inside(koord k) const
    {          
	return (pos.x <= k.x && pos.y <= k.y && 
		(pos.x+size.x) >= k.x && (pos.y+size.y) >= k.y);
    };


    /**
     * Events werden hiermit an die GUI-Komponenten
     * gemeldet
     * @author Hj. Malthaner
     */
    virtual bool process_event(const event_t & );


    /**
     * Updates the component by calling draw(). Saves onscreen position of 
     * the component
     * @author Hj. Malthaner
     */
    void update(graphics_t *gr, koord pos) {
      screenpos = this->pos + pos;
      draw(gr, pos);
    };


    /**
     * Draws the component
     * @author Hj. Malthaner
     */
    virtual void draw(graphics_t *gr, koord pos) = 0;


    /**
     * Request the system to update the display so that this component
     * gets painted again in it's current state.
     * @author Hj. Malthaner
     */
    void redraw();


    /**
     * Adds an event listener
     * @author Hj. Malthaner
     */
    void add_event_listener(event_listener_t * evl);


    /**
     * Removes an event listener
     * @author Hj. Malthaner
     */
    void remove_event_listener(event_listener_t * evl);


    /**
     * Retrieves all children of this component.
     * @returns always NULL
     * @author Hj. Malthaner
     */
    virtual const slist_tpl <gui_component_t *> * get_children() const {
      return 0;
    };
};
 
#endif
