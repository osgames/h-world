/* Copyright by Hj. Malthaner */

#ifndef EVENT_LISTENER_T_H
#define EVENT_LISTENER_T_H

class event_t;

class event_listener_t 
{
public:

   /**
    * Process an event.
    * @return true if event was swallowed, false otherwise
    * @author Hj. Malthaner
    */
   virtual bool process_event(const event_t &) = 0;

};

#endif
