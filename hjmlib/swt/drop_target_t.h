/* 
 * drop_target_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the SWT project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef DROP_TARGET_T_H
#define DROP_TARGET_T_H

#include "gui_component_t.h"

class koord;
class drawable_t;
class drag_controller_t;
class drop_source_t;


class drop_target_t : public gui_component_t {
private:
  drag_controller_t *drag_control;

public:    

  void set_drag_control(drag_controller_t *d) {
    drag_control = d;
  };


  drag_controller_t * get_drag_control() {
    return drag_control;
  };


  bool start_dragging(drop_source_t * source, drawable_t * drawable);


  drop_target_t ();
  virtual ~drop_target_t ();


  /**
   * called if item is dropped onto the drop target
   * @author Hj. Malthaner
   */
  virtual bool drop(drawable_t * item, koord at) = 0;


  /**
   * called if item is dragged over the drop target, but not yet dropped
   * @author Hj. Malthaner
   */
  virtual void hover(drawable_t * item, koord at) = 0;
};

#endif //DROP_TARGET_T_H
