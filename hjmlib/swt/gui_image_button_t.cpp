/* Copyright by Hj. Malthaner */

#include "gui_image_button_t.h"
#include "graphics_t.h"
#include "event_t.h"
class koord;

void gui_image_button_t::set_tile(const tile_descriptor_t * tile)
{
  this->tile = tile;
}


void gui_image_button_t::set_tile_pressed(const tile_descriptor_t * tile)
{
  this->tile_pressed = tile;
}




gui_image_button_t::gui_image_button_t()
{
  pressed = false;

  tile = tile_pressed = 0;
}



/**
 * Draws the component
 * @author Hj. Malthaner
 */
void gui_image_button_t::draw(graphics_t *gr, koord pos)
{
  pos += get_pos();

  if(pressed && tile_pressed) {
    gr->draw_tile(tile_pressed, pos.x, pos.y);
  } else {
    gr->draw_tile(tile, pos.x, pos.y);
  }
}


/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool gui_image_button_t::process_event(const event_t &ev)
{
  if(ev.type == event_t::BUTTON_PRESS) {
    pressed = true;
    redraw();

  } else if(ev.type == event_t::BUTTON_RELEASE) {
    call_listeners();
    pressed = false;
    redraw();
  }

  return false;
}
