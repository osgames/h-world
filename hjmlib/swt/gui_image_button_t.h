/* Copyright by Hj. Malthaner */

#ifndef GUI_IMAGE_BUTTON_T_H
#define GUI_IMAGE_BUTTON_T_H

#include "gui_action_trigger_t.h"
#include "tile_descriptor_t.h"
class koord;

class gui_image_button_t : public gui_action_trigger_t {

private:
    const tile_descriptor_t * tile;
    const tile_descriptor_t * tile_pressed;

    bool pressed;
    
public:

    void set_tile(const tile_descriptor_t * tile);
    void set_tile_pressed(const tile_descriptor_t * tile);


    gui_image_button_t();


    /**
     * Draws the component
     * @author Hj. Malthaner
     */
    virtual void draw(graphics_t *gr, koord pos);


    /**
     * Events werden hiermit an die GUI-Komponenten
     * gemeldet
     * @author Hj. Malthaner
     */
    virtual bool process_event(const event_t & );


};
#endif //GUI_IMAGE_BUTTON_T_H
