/* Copyright by Hj. Malthaner */

#include "gui_component_t.h"
#include "window_manager_t.h"
#include "tpl/slist_tpl.h"



gui_component_t::gui_component_t()
{
  listeners = new slist_tpl <event_listener_t *>;
  screenpos = koord(-1,-1);
}


gui_component_t::~gui_component_t()
{
  delete listeners;
  listeners = 0;
}



/**
 * Request the system to update the display so that this component
 * gets painted again in it's current state.
 * @author Hj. Malthaner
 */
void gui_component_t::redraw()
{
  window_manager_t::get_instance()->redraw(get_screenpos(), get_size());
}


/**
 * Sets the position
 * @author Hj. Malthaner
 */
void gui_component_t::set_pos(koord pos) 
{
  const koord old_screenpos = screenpos;

  this->pos = pos;

  if(old_screenpos != koord(-1,-1)) {
    // redraws old area. Sets screenpos top new position!
    window_manager_t::get_instance()->redraw(old_screenpos, get_size());
    // redraws new area. Sets screenpos top new position!
    window_manager_t::get_instance()->redraw(get_screenpos(), get_size());
  }
}


/**
 * Sets the size
 * @author Hj. Malthaner
 */
void gui_component_t::set_size(koord size) 
{
  const koord old_size = this->size;

  this->size = size;


  if(get_screenpos() != koord(-1,-1)) {
    const koord area (old_size.x > size.x ? old_size.x : size.x,
		      old_size.y > size.y ? old_size.y : size.y);

    window_manager_t::get_instance()->redraw(get_screenpos(), area);
  }
}



void gui_component_t::add_event_listener(event_listener_t * evl)
{
  if(evl) {
    listeners->insert(evl);
  }
}


void gui_component_t::remove_event_listener(event_listener_t * evl)
{
  listeners->remove(evl);
}


bool gui_component_t::process_event(const event_t &ev)
{
  bool swallowed = false;

  slist_iterator_tpl <event_listener_t *> iter (listeners);

  while(iter.next()) {
    swallowed |= iter.get_current()->process_event(ev);
  }

  return swallowed;
}
