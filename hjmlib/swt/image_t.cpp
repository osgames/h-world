#include <stdlib.h>
#include <stdio.h>

#include "graphics_t.h"
#include "tile_descriptor_t.h"
#include "system_window_t.h"
#include "image_t.h"
#include "read_png.h"

struct dimension {
    int xmin,xmax,ymin,ymax;
};


#define RGB_888_TO_555(r,g,b) (((r) & 0xF8) << 7) + (((g) & 0xF8) << 2) + (((b) & 0xF8) >> 3)


void image_t::read(const char *filename, enum colortype ct)
{
  image_data * block = read_png(filename);

  if(ct == RGB888) {
    data = new PIXEL[block->width * block->height*2];
  } else {
    data = new PIXEL[block->width * block->height];
  }

  if(ct == RGB888) {
    for(int j=0; j<block->height; j++) { 
      const int line  = j*block->width;
      
      for(int i=0; i<block->width; i++) { 
	data[(line+i)*2] = block->data[line*3 + i*3+0];
	data[(line+i)*2+1] = (block->data[line*3 + i*3+1] << 8) | block->data[line*3 + i*3+2];
      }
    }

    graphics = new graphics_t(block->width, block->height, RGB555);

  } else if(ct == RGB555) {

    // RGB 888 to RGB 555 conversion
    for(int j=0; j<block->height; j++) { 
      const int line  = j*block->width;
      
      for(int i=0; i<block->width; i++) { 
	data[line+i] = RGB_888_TO_555(block->data[line*3 + i*3+0],
				      block->data[line*3 + i*3+1],
				      block->data[line*3 + i*3+2]);
      }
    }
    
    graphics = new graphics_t(block->width, block->height, RGB555);
  } else {

    // RGB 888 to RGB 565 conversion
    for(int j=0; j<block->height; j++) { 
      const int line  = j*block->width;
      
      for(int i=0; i<block->width; i++) { 
	data[line+i] = 
	  ((block->data[line*3 + i*3+0] & 0xF8) << 8) +
	  ((block->data[line*3 + i*3+1] & 0xFC) << 3) +
	  ((block->data[line*3 + i*3+2] & 0xF8) >> 3);
      }
    }
    
    graphics = new graphics_t(block->width, block->height, RGB565);
  }


  graphics->set_data(data);

  width = block->width;
  height = block->height;
  
  free(block->data);
  free(block);
}


/**
 * Reads image from file. Currently only PNG files are supported.
 * @author Hj. Malthaner
 */
image_t::image_t(const char * filename, enum colortype ct)
{
  read(filename, ct);
}



image_t::image_t(const char *filename)
{
  if(0x7FFF==system_window_t::get_instance()->get_system_color(255,255,255)) {
    read(filename, RGB555);
  } else {
    read(filename, RGB565);
  }
}


image_t::image_t(int width, int height, enum colortype ct)
{
  this->width = width;
  this->height = height;

  data = new PIXEL[width * height];
  
  graphics = new graphics_t(width, height, ct);
  graphics->set_data(data);
}


image_t::~image_t()
{
  delete graphics;
  graphics = 0;

  delete [] data;
  data = 0;
}

#define TRANSPARENT     0x4211

// number of special colors
#define SPECIAL 8

static PIXEL rgbtab[SPECIAL] =
{
  /*
  0x001C1C,
  0x003838,
  0x005555,
  0x007171,

  0x008D8D,
  0x00AAAA,
  0x00C6C6,
  0x00E2E2
  */
  
    RGB_888_TO_555(0x00, 0x1C, 0x1C),
    RGB_888_TO_555(0x00, 0x38, 0x38),
    RGB_888_TO_555(0x00, 0x55, 0x55),
    RGB_888_TO_555(0x00, 0x71, 0x71),
    
    RGB_888_TO_555(0x00, 0x8D, 0x8D),
    RGB_888_TO_555(0x00, 0xAA, 0xAA),
    RGB_888_TO_555(0x00, 0xC6, 0xC6),
    RGB_888_TO_555(0x00, 0xE2, 0xE2)

};


static PIXEL datatopix(const PIXEL v)
{
  for(int i=0; i<SPECIAL; i++) {
    if(rgbtab[i] == v) {
      return 0x8000 + i;
    }
  }

  return v;
}


void image_t::init_dim(struct dimension *dim)
{
    int x,y;

    dim->xmin = width;
    dim->ymin = height;
    dim->xmax = 0;
    dim->ymax = 0;

    for(y=0; y<height; y++) {
	for(x=0; x<width; x++) {
	    if(data[x+y*width] != TRANSPARENT) {
		if(x<dim->xmin)
		    dim->xmin = x;
		if(y<dim->ymin)
		    dim->ymin = y;
		if(x>dim->xmax)
		    dim->xmax = x;
		if(y>dim->ymax)
		    dim->ymax = y;
	    }
	}
    }


    // fprintf(stderr, "w=%d h=%d xmin=%d ymin=%d xmax=%d ymax=%d\n", width, height, dim->xmin, dim->ymin, dim->xmax, dim->ymax);
}


/**
 * Encode image to RLE sprite as used by draw_tile
 * @author Hj. Malthaner
 */
tile_descriptor_t * image_t::encode_tile()
{
  tile_descriptor_t *tile = new tile_descriptor_t();

  int line;
  PIXEL *dest;
  PIXEL *dest_base = new PIXEL [width*height*2];
  PIXEL *run_counter;

  struct dimension dim;

  init_dim(&dim);

  int y = dim.ymin;
  int x = 0;

  dest = dest_base;
  
  for(line=0; line<(dim.ymax-dim.ymin+1); line++) {
    PIXEL pix = data[x + (y+line)*width];
    int   row = 0;
    int   count = 0;
    
    do {
      count = 0;
      while(pix == TRANSPARENT && row < width) {

	row ++;
	count ++;
	
	if(row < width) {
	  pix = data[(x+row) + (y+line)*width];
	}
      }
      
      // printf("T:%d ", count);
      
      *dest++ = count;
      
      run_counter = dest++;
      count = 0;
      
      while(pix != TRANSPARENT && row < width) {
	*dest++ = datatopix(pix);

	row ++;
	count ++;

	if(row < width) {
	  pix = data[(x+row) + (y+line)*width];
	}
      }
      *run_counter = count;
      
      // printf("C:%d ", count);
      
      
    } while(row < width);
    
    // printf("\n");
    
    *dest++ = 0;
  }
  
  tile->x = dim.xmin;
  tile->y = dim.ymin;

  tile->w = (dim.xmax - dim.xmin) + 1; 
  tile->h = (dim.ymax - dim.ymin) + 1; 

  tile->len = (dest - dest_base);
  tile->data = dest_base;
  tile->rwidth = width;

  return tile;
}
