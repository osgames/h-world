/* Copyright by Hj. Malthaner */

#include <stdio.h>
#include "event_t.h"

event_t event_t::translate(koord offset) const
{
  event_t result (*this);

  result.mpos += offset;
  result.cpos += offset;

  return result;
}

event_t::event_t()
{
  type = NO_EVENT;
  code = 0;
  buttons = 0;
}


void event_t::dump()
{
  printf("Type: %d\n", type);
  printf("Code: %d\n", code);
  printf("Butn: %d\n", buttons);
  printf("MPos: %d %d\n", mpos.x, mpos.y);
  printf("CPos: %d %d\n", cpos.x, cpos.y);
}
