#include <stdlib.h>

#include "drawable_t.h"
#include "graphics_t.h"
#include "window_manager_t.h"


window_manager_t * drawable_t::winman = 0;


void drawable_t::set_window_manager(window_manager_t *w)
{
  winman = w;
}

void drawable_t::set_pos(koord pos) {
  const koord old_pos = get_pos();

  this->pos = pos;

  if(visible) {
    const koord new_pos = get_pos();

    const int xmin = old_pos.x < new_pos.x ? old_pos.x : new_pos.x;
    const int ymin = old_pos.y < new_pos.y ? old_pos.y : new_pos.y;

    const int w = get_size().x + abs(old_pos.x - new_pos.x);
    const int h = get_size().y + abs(old_pos.y - new_pos.y);

    window_manager_t::get_instance()->redraw(xmin, ymin, w, h);
  }
}


void drawable_t::set_size(koord size) {
  this->size = size;

  if(visible) {
    redraw();
  }
}

 
/**
 * Shows the drawable onscreen
 * @author Hj. Malthaner
 */
void drawable_t::set_visible(bool yesno)
{
  visible = yesno;
  redraw();
}


drawable_t::drawable_t()
{
  visible = false;
  // winman->add_window(this);
}


void drawable_t::close()
{
  winman->remove_window(this);
}


/**
 * Updates the drawable  by calling draw(). Sets the appropriate clipping
 * area on gr before calling draw and restores original clip before 
 * returning.
 * @author Hj. Malthaner
 */
void drawable_t::update(graphics_t *gr, koord pos)
{
  clip_dimension_t clip = gr->get_clip();

  const int left = pos.x > clip.x ? pos.x : clip.x;
  const int top = pos.y > clip.y ? pos.y : clip.y;

  const int right = pos.x+size.x < clip.x+clip.w ? pos.x+size.x : clip.x+clip.w;
  const int bottom = pos.y+size.y < clip.y+clip.h ? pos.y+size.y : clip.y+clip.h;

  // set clip to intersection
  gr->set_clip_wh(left, top, right-left, bottom-top); 

  draw(gr, pos);

  // restore clip
  gr->set_clip_wh(clip.x, clip.y, clip.w, clip.h);
}


void drawable_t::redraw()
{
  winman->redraw(get_pos(), get_size());
}


/**
 * Redraw an area of this drawable.
 * @param pos relative to top-left corner of this drawable
 * @author Hj. Malthaner
 */
void drawable_t::redraw(koord pos, koord size)
{
  winman->redraw(get_pos() + pos, size);
}
