/* 
 * painter_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the SWT project and may not be used
 * in other projects without written permission of the author.
 */


#include "painter_t.h"
#include "graphics_t.h"
#include "font_t.h"

/**
 * Paints a box
 * @author Hj. Malthaner
 */
void painter_t::box_wh(koord pos, koord size, color_t & color)
{
  gr->fillbox_wh(pos.x, pos.y, size.x, 1, color);
  gr->fillbox_wh(pos.x, pos.y, 1, size.y, color);

  gr->fillbox_wh(pos.x, pos.y+size.y-1, size.x, 1, color);
  gr->fillbox_wh(pos.x+size.x-1, pos.y, 1, size.y, color);
}


/**
 * Paints a 3d box
 * @author Hj. Malthaner
 */
void painter_t::ddd_box_wh(koord pos, koord size, 
			   color_t & color_tl, color_t & color_br)
{
  gr->fillbox_wh(pos.x, pos.y, size.x, 1, color_tl);
  gr->fillbox_wh(pos.x, pos.y, 1, size.y, color_tl);

  gr->fillbox_wh(pos.x, pos.y+size.y-1, size.x, 1, color_br);
  gr->fillbox_wh(pos.x+size.x-1, pos.y, 1, size.y, color_br);
}


/**
 * Draws a string fake bold
 * @author Hj. Malthaner
 */
void painter_t::draw_string_bold(const font_t * font,
				 const char * text, koord pos, 
				 int spacing,
				 color_t & color1,
				 color_t & color2)
{
  font->draw_string(gr, text, pos.x+1, pos.y, spacing, color2);
  font->draw_string(gr, text, pos.x, pos.y, spacing, color1);
}
