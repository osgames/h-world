#ifndef drop_source_t_h
#define drop_source_t_h

class drawable_t;

class drop_source_t
{

 private:

 public:

  virtual ~drop_source_t() {};


  /**
   * If dragging is stopped, the item needs to be put back
   * @author Hj. Malthaner
   */
  virtual void stop_dragging(drawable_t *item) = 0;

};

#endif // drop_source_t_h
