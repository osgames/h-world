/* Copyright by Hj. Malthaner */

#ifndef GUI_CHECK_T_H
#define GUI_CHECK_T_H

#include "gui_action_trigger_t.h"
#include "color_t.h"
class koord;

class font_t;

class gui_check_t : public gui_action_trigger_t {
 private:
  
  bool yesno;
  const char * text;

  font_t * font;
  color_t color;

 public:
  

  bool get_selected() const {return yesno;};
  void set_selected(bool y);
  void set_text(const char *text);
  void set_font(font_t *font) {this->font = font;};
  void set_color(color_t color) {this->color = color;};


  gui_check_t();
  gui_check_t(bool yesno, const char *text);


  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);

  /**
   * Process an event.
   * @return true if event was swallowed, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t &);
};


#endif
