/* 
 * gui_button_t.cpp
 *
 * Copyright (c) 2001 - 2005 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */


#include <string.h>

#include "system_window_t.h"
#include "gui_button_t.h"
#include "graphics_t.h"
#include "color_t.h"
#include "event_t.h"
#include "font_t.h"
#include "painter_t.h"


void gui_button_t::set_font(font_t * font)
{
  this->font = font;
}

const char * gui_button_t::get_text() const
{
  return text;
}

void gui_button_t::set_text(const char *t)
{
  strncpy(text, t, 127);
  text[127] = '\0';
}


gui_button_t::gui_button_t(const char *text)
{
  font = system_window_t::get_instance()->get_default_font();

  pressed = false;
  set_text(text);
}


/**
 * Draws the component
 * @author Hj. Malthaner
 */
void gui_button_t::draw(graphics_t *gr, koord screenpos)
{
  const koord pos = get_pos()+screenpos;
  const koord size = get_size();
  const int width = font->get_string_width(text, 0);
  painter_t pt (gr);

  pt.box_wh(pos, size, color_t::GREY64);

  if(pressed) {
    gr->fillbox_wh(pos.x+1, pos.y+1, size.x-2, size.y-2, color_t::GREY64);
  } else {
    gr->fillbox_wh(pos.x+1, pos.y+1, size.x-2, size.y-2, color_t::GREY224);
  }

  font->draw_string(gr, text,
		    pos.x+(size.x-width)/2, 
		    pos.y+(size.y-10)/2,
		    0,
		    color_t::BLACK);

}


/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool gui_button_t::process_event(const event_t &ev)
{
  bool swallowed = false;

  if(ev.type == event_t::BUTTON_PRESS) {
    pressed = true;
    redraw();
    swallowed = true;

  } else if(ev.type == event_t::BUTTON_RELEASE) {
    pressed = false;
    redraw();
    swallowed = true;
    call_listeners();
  }

  return swallowed;
}
