/*
 * gui_tile_t.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 */

#ifndef GUI_TILE_T_H
#define GUI_TILE_T_H
   
#include "gui_component_t.h"
class koord;

class tile_descriptor_t;

/**
 * Komponenten von Fenstern sollten von dieser Klassse abgeleitet werden.
 *
 * @autor Hj. Malthaner
 * @version $Revision$
 */
class gui_tile_t : public gui_component_t
{
private:
  tile_descriptor_t *tile;

public:

  gui_tile_t();
  ~gui_tile_t();


  void set_model(tile_descriptor_t *tile) {this->tile = tile;};



  /**
   * Paints the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos) const;
};

#endif
