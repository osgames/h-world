/* Copyright by Hj. Malthaner */

#ifndef GUI_ACTION_TRIGGER_T_H
#define GUI_ACTION_TRIGGER_T_H

#include "gui_component_t.h"

class action_listener_t;

class gui_action_trigger_t : public gui_component_t {
 private:

  /**
   * Our listeners.
   * @author Hj. Malthaner
     */
  slist_tpl <action_listener_t *> * listeners;
  

 protected:

  /**
   * Inform all listeners that an action was triggered.
   * @author Hj. Malthaner
   */
  void call_listeners();


 public:

  gui_action_trigger_t();
  virtual ~gui_action_trigger_t();


  /**
   * Add a new listener to this button.
   * @author Hj. Malthaner
   */
  void add_listener(action_listener_t * l);


  /**
   * Remove a listener from this button.
   * @author Hj. Malthaner
   */
  bool remove_listener(action_listener_t * l);
};



#endif //GUI_ACTION_TRIGGER_T_H
