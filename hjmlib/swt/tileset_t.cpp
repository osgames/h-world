/* 
 * tileset_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include <stdio.h>
#include <string.h>

#include "tileset_t.h"
#include "tile_descriptor_t.h"
#include "primitives/invalid_argument_exception_t.h"


/**
 * Liest 32Bit wert Plattfromunabh�ngig
 * @author Hj. Malthaner
 */
static int fread_int(FILE *f)
{
    int i = 0;

    i += fgetc(f);
    i += fgetc(f) << 8;
    i += fgetc(f) << 16;
    i += fgetc(f) << 24;

    return i;
}


/**
 * Gets a tile from this tile set.
 * @param image image number (note: this is unsigned!)
 * @author Hj. Malthaner
 */
const tile_descriptor_t * tileset_t::get_tile(unsigned int image) const 
{
  tile_descriptor_t * tile = 0;

  if(image < count) {
    tile =  &tiles[image];
  }

  return tile;
}


tile_descriptor_t tileset_t::set_tile(unsigned int image,
				      tile_descriptor_t *neu)
{
  tile_descriptor_t old;

  if(neu && image < count) {
    old = tiles[image];

    tiles[image] = *neu;
  }

  return old;
}




/**
 * Loads a tileset from a file.
 * @author Hj. Malthaner
 */
bool tileset_t::load(const char * filename, int rwidth) 
{
  FILE * file;

  file = fopen(filename, "rb");

  if(file) {

    char buf[32];

    fread(buf, 23, 1, file);
    buf[23] = '\0';

    if(strcmp("H-World daten.pak 0.0.1", buf) != 0) {
      invalid_argument_exception_t ex;
      sprintf(ex.reason, "tileset_t::load(): '%s' has wrong format '%s'!", filename, buf); 
      throw ex;
    }

    size = count = fread_int(file); 

    // dbg->message("tileset_t::load()", "Reading %u tiles from '%s'", 
    //	 count, 
    //	 filename);

    tiles = new tile_descriptor_t [count];

    for(unsigned int i=0; i<count; i++) {
      tiles[i].x      = fgetc(file);
      tiles[i].y      = fgetc(file);
      tiles[i].w      = fgetc(file);
      tiles[i].h      = fgetc(file);
      tiles[i].rwidth = rwidth;
      tiles[i].len    = fread_int(file);


      // dbg->message("tileset_t::load()", "tile %d: y=%d, h=%d, len=%d", i, tiles[i].y, tiles[i].h, tiles[i].len); 


      if(tiles[i].h > 0) {            
	tiles[i].data = new PIXVAL[tiles[i].len];
	fread(tiles[i].data, tiles[i].len*sizeof(PIXVAL), 1, file);
	
      } else {
	tiles[i].data = NULL;
      }          

    }
    fclose(file);

    // dbg->message("tileset_t::load()", "... done"); 

  } else {
    // dbg->warning("tileset_t::load()", "Can't open '%s' for reading.", filename);
  }


  tile_raster = rwidth;


  return (file != 0);
}


/**
 * Basic constructor, builds an empty set of tiles.
 * @author Hj. Malthaner
 */
tileset_t::tileset_t() 
{
  count = 0;
  size = 0;
  tiles = 0;
  tile_raster = 0;
}


/**
 * Builds a tileset with size tiles
 * @author Hj. Malthaner
 */
tileset_t::tileset_t(unsigned int s)
{
  count = 0;
  tiles = 0;
  tile_raster = 0;

  size = s;

  tiles = new tile_descriptor_t [s];
}


/**
 * Destructor, frees memory.
 * @author Hj. Malthaner
 */
tileset_t::~tileset_t() 
{
  if(tiles) {

    for(unsigned int i=0; i<size; i++) {
      if(tiles[i].data) {            
	delete [] tiles[i].data;    
	tiles[i].data = 0;    
      }
    }

    delete [] tiles;
  }

  count = 0;
  tiles = 0;
  tile_raster = 0;
}


/**
 * Append a tile to this tile set.
 * @return new tile number
 * @author Hj. Malthaner
 */
int tileset_t::append(tile_descriptor_t *t)
{
  if(count < size) {
    tiles[count] = *t;
    count ++;
  }

  return count-1;
}
