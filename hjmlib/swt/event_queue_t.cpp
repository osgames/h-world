/* Copyright by Hj. Malthaner */

#include "event_queue_t.h"
#include "system_window_t.h"

event_queue_t * event_queue_t::single_instance= 0;


event_queue_t * event_queue_t::get_instance()
{
  if (single_instance == 0) {
    single_instance = new event_queue_t();
  }
  return single_instance;
}


void event_queue_t::init(system_window_t * syswin)
{
  this->syswin = syswin;
}


event_t & event_queue_t::poll_event()
{ 
  return syswin->poll_event();
}


event_queue_t::event_queue_t()
{
  syswin = 0;
}
