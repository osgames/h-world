/* 
 * grid_layout_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the SWT project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef GRID_LAYOUT_T_H
#define GRID_LAYOUT_T_H

#include "primitives/koord.h"

class gui_component_t;

class grid_layout_t {
private:

    gui_component_t ***components;

    koord bottom_right;

    koord tlc;
    koord brc;

    int border;

    /**
     * Column widths.
     */
    int *column_widths;

    /**
     * Row heights.
     */
    int *row_heights;


public:

    void set_border(int b) {border = b;};

    void set_component(koord pos, gui_component_t * c);

    koord get_top_left() const {return tlc;};
    koord get_bottom_right() const {return brc;};

    koord get_dimension();

    void reset(int n);


    grid_layout_t(int n);

    void do_layout();

    /**
     * Call after do_layout(). Minimizes vertical distances between
     * the rows.
     * @param d minimal distance between elements
     * @author Hj. Malthaner
     */
    void shrink_vertical(int d);

};
#endif //GRID_LAYOUT_T_H
