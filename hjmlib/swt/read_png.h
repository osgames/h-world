#ifdef __cplusplus
extern "C" {
#endif

typedef struct 
{
  int width, height;
  unsigned char * data;
} image_data;


/**
 * Reads a PNG image as a RGB 888 buffer
 * @author Hj. Malthaner
 */
image_data * read_png(const char *filename);


#ifdef __cplusplus
}
#endif
