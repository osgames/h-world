/* Copyright by Hj. Malthaner */

#ifndef DRAWABLE_T_H
#define DRAWABLE_T_H

#ifndef koord_h
#include "primitives/koord.h"
#endif


class event_t;
class graphics_t;
class window_manager_t;

class drawable_t {
private:

  static window_manager_t * winman;
  
  /**
   * Screen position.
   * @author Hj. Malthaner
   */
  koord pos;


  /**
   * Size
   * @author Hj. Malthaner
   */
  koord size;

  bool visible;

public:

  static void set_window_manager(window_manager_t *winman);

  koord get_pos() const {return pos;};

  void set_pos(koord pos);


  /**
   * @return the size
   * @author Hj. Malthaner
   */
  virtual koord get_size() const {return size;}; 


  /**
   * Setzt die Fenstergroesse
   * @author Hj. Malthaner
   */
  virtual void set_size(koord size);


  bool is_visible() const {return visible;};


  /**
   * Shows the drawable onscreen
   * @author Hj. Malthaner
   */
  void set_visible(bool yesno);


  drawable_t();


  virtual ~drawable_t() {}; 


  /**
   * removes window from window manager. Does not
   * delete this object!
   *
   * @author Hj. Malthaner
   */
  void close();


  /**
   * Updates the drawable  by calling draw(). Sets the appropriate clipping
   * area on gr before calling draw and restores original clip before 
   * returning.
   * @author Hj. Malthaner
   */
  void update(graphics_t *gr, koord pos);


  virtual void draw(graphics_t *gr, koord pos) = 0;



  void redraw();


  /**
   * Redraw an area of this drawable.
   * @param pos relative to top-left corner of this drawable
   * @author Hj. Malthaner
   */
  void redraw(koord pos, koord size);


  /**
   * Subclasses that want to get event must override this 
   * to return true.
   * @author Hj. Malthaner
   */
  virtual bool consume_events() const {return false;};


  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t & ) {return false;}
};
#endif //DRAWABLE_T_H
