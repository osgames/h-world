/* 
 * tileset_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef TILESET_T_H
#define TILESET_T_H

class tile_descriptor_t;
class koord;

/**
 * A set of graphical tiles.
 * @author Hj. Malthaner
 */
class tileset_t {
private:
    /**
     * Default raster distance (grid) of this tileset.
     * @author Hj. Malthaner
     */
    int tile_raster;


    /**
     * Number of tiles used in this tileset.
     * @author Hj. Malthaner
     */
    unsigned int count;


    /**
     * Max number of tiles in this tileset.
     * @author Hj. Malthaner
     */
    unsigned int size;


    /**
     * The tiles of this set.
     * @author Hj. Malthaner
     * @supplierCardinality 0..*
     */
    tile_descriptor_t * tiles;

public:    

    /**
     * Gets the number of tiles in this tileset.
     * @author Hj. Malthaner
     */
    int get_count() const {return count;};


    /**
     * Basic constructor, builds an empty set of tiles.
     * @author Hj. Malthaner
     */
    tileset_t();


    /**
     * Builds a tileset with size tiles
     * @author Hj. Malthaner
     */
    tileset_t(unsigned int size);


    /**
     * Destructor, frees memory.
     * @author Hj. Malthaner
     */
    ~tileset_t();


    /**
     * Loads a tileset from a file.
     * @author Hj. Malthaner
     */
    bool load(const char * filename, int rwidth);


    int get_tile_raster() const {return tile_raster;};



    /**
     * Gets a tile from this tile set.
     * @param image image number (note: this is unsigned!)
     * @author Hj. Malthaner
     */
    const tile_descriptor_t * get_tile(unsigned int image) const;


    tile_descriptor_t set_tile(unsigned int image,
			       tile_descriptor_t *neu);
  

    /**
     * Append a tile to this tile set.
     * @return new tile number
     * @author Hj. Malthaner
     */
    int append(tile_descriptor_t *t);

};
#endif //TILESET_T_H
