/* 
 * gui_frame_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the SWT project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef gui_gui_frame_h
#define gui_gui_frame_h

#include "gui_window_t.h"
#include "window_color_t.h"

class color_t;

/**
 * Eine Klasse f�r Fenster mit Komponenten.
 * Anders als die anderen Fensterklasen in Simutrans ist dies
 * ein richtig Komponentenorientiertes Fenster, das alle 
 * aktionen an die Komponenten delegiert.
 *
 * @author Hj. Malthaner
 * @version $Revision$
 */
class gui_frame_t : public gui_window_t
{
private:

    const char * title;

    window_color_t colors;

    font_t * title_font;
    font_t * font;

    /**
     * starting position if window is dragged
     * @author Hj. Malthaner
     */
    koord drag_start;

    bool closing;
    bool dragging;

    void gui_frame_t::draw_close_box(graphics_t *gr, 
				     int x, int y, color_t & color, 
				     bool pushed);
public:


    void set_font(font_t *font);
    font_t *get_font();

    void set_title_font(font_t *font);
    font_t *get_title_font();


    /**
     * Konstruktor
     * @author Hj. Malthaner
     */
    gui_frame_t(const char *name);


    /**
     * Destructor, does nothing.
     * @author Hj. Malthaner
     */
    virtual ~gui_frame_t() {};


    /**
     * Der Name wird in der Titelzeile dargestellt
     * @return den nicht uebersetzten Namen der Komponente
     * @author Hj. Malthaner
     */
    virtual const char * get_title() const;


    /**
     * setzt den Namen (Fenstertitel)
     * @author Hj. Malthaner
     */
    void set_title(const char *name);


    /**
     * Events werden hiermit an die GUI-Komponenten
     * gemeldet
     * @author Hj. Malthaner
     */
    virtual bool process_event(const event_t &ev);
    

    /**
     * Draws the window frame and all components (by calling update
     * on the container).
     * @author Hj. Malthaner
     */
    virtual void draw(graphics_t *gr, koord pos);    


protected:
    /**
     * gibt farbinformationen fuer Fenstertitel, -r�nder und -k�rper
     * zur�ck
     * @author Hj. Malthaner
     */
    virtual window_color_t & get_colors();
};

#endif
