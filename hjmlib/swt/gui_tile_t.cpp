/*
 * gui_tile_t.cpp
 *
 * Copyright (c) 2001 Hansj�rg Malthaner
 *
 */

#include "gui_tile_t.h"
#include "graphics_t.h"
class koord;


gui_tile_t::gui_tile_t()
{
  tile = 0;
}


gui_tile_t::~gui_tile_t()
{
  tile = 0;
}


void gui_tile_t::draw(graphics_t *gr, koord pos) const
{
  gr->draw_tile(tile, pos.x, pos.y);
}
