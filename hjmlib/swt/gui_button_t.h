/* Copyright by Hj. Malthaner */

#ifndef GUI_BUTTON_T_H
#define GUI_BUTTON_T_H

#include "gui_action_trigger_t.h"

class font_t;

class gui_button_t : public gui_action_trigger_t 
{
protected:

  char text[128];
  font_t *font;
  bool pressed;
    
public:
  
  void set_font(font_t * font);

  const char * get_text() const;
  void set_text(const char *text);

  gui_button_t(const char *text);

  
  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);

  
  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t & );

};
#endif //GUI_BUTTON_T_H
