
#include "ifc/action_listener_t.h"
#include "tpl/slist_tpl.h"

#include "gui_action_trigger_t.h"


gui_action_trigger_t::gui_action_trigger_t()
{
  listeners = new slist_tpl <action_listener_t *>;
}


gui_action_trigger_t::~gui_action_trigger_t()
{
  delete listeners;
  listeners = 0;
}


/**
 * Inform all listeners that an action was triggered.
 * @author Hj. Malthaner
 */
void gui_action_trigger_t::call_listeners() 
{
  slist_iterator_tpl<action_listener_t *> iter (listeners);

  while(iter.next()) {
    iter.get_current()->action_triggered(this);
  }
}


/**
 * Add a new listener to this button.
 * @author Hj. Malthaner
 */
void gui_action_trigger_t::add_listener(action_listener_t * l) 
{
  listeners->insert( l );
}


/**
 * Remove a listener from this button.
 * @author Hj. Malthaner
 */
bool gui_action_trigger_t::remove_listener(action_listener_t * l)
{
  return listeners->remove( l );
}
