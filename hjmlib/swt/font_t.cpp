/* 
 * font_t.cpp
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */

#include "font_t.h"
#include "color_t.h"
#include "graphics_t.h"
#include "primitives/invalid_argument_exception_t.h"
#include "tpl/stringhashtable_tpl.h"


static stringhashtable_tpl <font_t *> fix_fonts;
static stringhashtable_tpl <font_t *> prop_fonts;


/**
 * Width and height are ignored in case of BDF fonts, BDF
 * fonts contain font geometry in the file and the reader
 * will read and use the real font geometry
 * @author Hj. Malthaner
 */
font_t * font_t::load(const char *filename, bool proportional, int w, int h)
{
  font_t *font = 0;

  if(proportional) {
    font = fix_fonts.get(filename);
  } else {
    font = prop_fonts.get(filename);
  }

  if(font == 0) {
    font = new font_t(w,h);
    font->load_from_file(filename, proportional);
    
    if(proportional) {
      fix_fonts.put(strdup(filename), font);
    } else {
      prop_fonts.put(strdup(filename), font);
    }
  } else {

    //    printf("Reused font %s\n", filename);
  }

  return font;
}


int font_t::get_glyph_width(int glyph)
{
  int maxw = glyph != 32 ? -1 : width/2;

  int base = glyph * height;
  const int end = base+height;

  do {
    const unsigned short c = data[base++];       /* Eine Zeile des Zeichens */
    int b;
      
    for(b=0; b<width; b++) {
      if(c & (1 << b)) {
	if(b > maxw) {
	  maxw = b;
	}
      } 
    }
  } while(base < end);

  return maxw+1;
}


font_t::font_t(int w, int h)
{
  width = w;
  height = h;

  data = 0;
  widths = 0;
}


font_t::~font_t()
{
  delete [] data;
  data = NULL;
  delete [] widths;
  widths = NULL;
}



// --------------- HEX related routines ----------------

static void dump_bits(int v)
{
  printf("%4x : ", v);

  for(int i=0; i<32; i++) { 
    printf(((1 << i) & v) ? "#" : "."); 
  }
  printf("\n");
}


static int nibble(char c)
{
  const int n = (c > '9') ? 10 + (c - 'A') : c - '0';
  int v = 0;

  for(int i=0; i<4; i++) { 
    v |= ((n >> i) & 1) << (3-i);
  }
  
  return v;
}


/**
 * Reads a .hex font file
 * @author Hj. Malthaner
 */
void font_t::load_from_hex(const char * filename)
{
  data = new unsigned short [width*height*256];
  widths = new unsigned char [width*height*256];

  memset(data, 0, width*height*256*2);
  memset(widths, 0, width*height*256);

  FILE *file = fopen(filename, "rb");

  if(file) {

    char buf [80];
    int  n, line;
    char *p;
    
    while(fgets(buf, 79, file) != NULL) {
    
      sscanf(buf, "%4x", &n);
      
      p = buf+5;

      for(line=0; line<height; line ++) {
	unsigned int val =  nibble(p[0]) | (nibble(p[1]) << 4);
	data[n*height + line] = val;
	p += 2;
	// dump_bits(val);
      }
      // printf("\n");
      
    }

  } else {
    perror(filename);

    invalid_argument_exception_t ex;
    sprintf(ex.reason, "font_t::load(): Can't open file '%s' for reading\n", filename);
    throw ex;
  }
}


// --------------- BDF related routines ----------------


/**
 * Removes whitespace from the end of the string.
 * Modifies the argument!
 * @author Hj. Malthaner
 */
static void rtrim(char * buf)
{
  int l = strlen(buf) - 1;

  while(l >= 0 && buf[l] > 0 && buf[l] <= 32) {
    buf[l--] = '\0';
  }
}


static int bdf_bits(const int value, const int g_width, const int g_left)
{
  const int w = g_width + g_left;
  int mark = 1 << 7;
  int r = 0;

  for(int i=0; i<w; i++) { 
    r |= (value & mark) ? (1 << i) : 0;
    mark >>= 1;
  }

  return r;
}


static void read_bdf_bitmap(FILE * file,
			    unsigned short * data,
			    int f_width, int f_height, int f_lead, int f_desc,
			    int w, int h, int g_left, int g_desc)
{
  const int top = f_height + f_desc - h - g_desc;

  for(int i=0; i<h; i++) { 
    int v;

    fscanf(file, "%x", &v);

    data[top + i] = bdf_bits(v, w, g_left);

    // dump_bits(data[top + i]);
  }
  // printf("\n");
}


static void read_bdf_glyph(FILE * file, 
			   unsigned short * data,
			   unsigned char * widths,
			   const char * chr_s,
			   int f_width, int f_height, int f_lead, int f_desc)
{
  char buf[256];
  int g_width, g_height, g_left, g_desc;

  int chr_n = -1;
  
  do {
    fgets(buf, 255, file);
    buf[255] = '\0';

    // printf("info: %s: line '%s'\n", chr_s, buf);

    rtrim(buf);

    char * val = strchr(buf, ' ');

    if(val) {
      *val++ = '\0';

      if(strcmp(buf, "BBX") == 0) {
	sscanf(val, "%d %d %d %d", &g_width, &g_height, &g_left, &g_desc);
	// printf("Reading char %d\n", chr_n);
	// printf("Bounding box is g_width=%d h=%d c=%d g_desc=%d\n", g_width, g_height, c, g_desc);
      } else if(strcmp(buf, "ENCODING") == 0) {
	int n = -1;

	sscanf(val, "%d %d", &chr_n, &n);

	if(chr_n == -1) {
	  chr_n = n;
	}

	if(chr_n == -1) {
	  // Hajo: tell about error, stop loading this glyph
	  printf("warning: %s: illegal glyph encoding '%s'\n", chr_s, buf);
	  break;
	}
      }

      // fprintf(stdout, "key='%s' val='%s'\n", buf, val);

    } else if(strcmp(buf, "BITMAP") ==0 ) {

      widths[chr_n] = g_width + g_left;

      read_bdf_bitmap(file,
		      data + (chr_n * f_height),
		      f_width, f_height, f_lead, f_desc,
		      g_width, g_height, g_left, g_desc);

    } else if(strcmp(buf, "ENDCHAR") == 0 ) {
      // Hajo: ok, end of loop condition

    } else if(*buf == '\0' ) {
      // Hajo: ok, empty line

    } else {
      printf("warning: %s: unexpected format on line '%s'\n", chr_s, buf);
    }

  } while(strcmp(buf, "ENDCHAR") != 0);
}


/**
 * Reads a .bdf font file
 * @author Hj. Malthaner
 */
void font_t::load_from_bdf(const char * filename)
{
  FILE * file = fopen(filename, "rb");
  char buf[256];

  // Hajo: font geometry
  int f_width;
  int f_height;
  int f_lead;
  int f_desc;

  // stringhashtable_tpl <const char *> font_header;


  if(file) {

    // Hajo: read font header

    do {

      fgets(buf, 255, file);
      rtrim(buf);

      char * val = strchr(buf, ' ');

      if(val) {
	*val++ = '\0';

	// fprintf(stdout, "key='%s' val='%s'\n", buf, val);

	if(strcmp(buf, "FONTBOUNDINGBOX") == 0) { 
	  sscanf(val,
		 "%d %d %d %d", 
		 &f_width, &f_height, &f_lead, &f_desc);      
	}

      } else if(strcmp(buf, "ENDPROPERTIES") == 0) {
	// Hajo: Ok, go on
      } else {
	fprintf(stderr, "error: missing space on line '%s'\n", buf);
	exit(1);
      }

    } while(strcmp(buf, "CHARS") != 0);



    // Hajo: now set real font size

    width = f_width;
    height = f_height;

    data = new unsigned short [width*height*256];
    widths = new unsigned char [width*height*256];

    memset(data, 0, width*height*256*2);
    memset(widths, 0, width*height*256);


    do {
      fgets(buf, 255, file);

      if(strncmp(buf, "STARTCHAR", 9) == 0) {
	read_bdf_glyph(file, data, widths, 
		       buf, f_width, f_height, f_lead, f_desc);
      }
    } while(!feof(file));


    // Hajo: hack: guess space width
    widths[32] = width/2;

    fclose(file);

  } else {
    perror(filename);

    invalid_argument_exception_t ex;
    sprintf(ex.reason, "font_t::load(): Can't open file '%s' for reading\n", filename);
    throw ex;
  }

}


/**
 * Reads a .bdf or .hex font from a file
 * @prop in case of a .hex font this tells if it is a proportional font or not
 * @author Hj. Malthaner
 */
void font_t::load_from_file(const char *filename, bool prop)
{
  const int l = strlen(filename);
  const char * p = filename + l - 4;

  if(strcmp(p, ".hex") == 0) {
    load_from_hex(filename);

    if(prop) {
      for(int i=0; i<256; i++) {
	widths[i] = get_glyph_width(i);
      }
    } else {
      for(int i=0; i<256; i++) {
	widths[i] = width;
      }
    }

  } else if(strcmp(p, ".bdf") == 0) {
    load_from_bdf(filename);

  } else {
    invalid_argument_exception_t ex;
    sprintf(ex.reason, "font_t::load(): wrong file type '%s'\n", p);
    throw ex;
  }
}


/**
 * Determines the width of a string written in this font
 * @autor Hj. Malthaner 
 */
int font_t::get_string_width(const char * text, int spacing) const
{
  int w = 0;

  while(*text) {
    w += get_width(((unsigned char)(*text++))) + spacing; 
  }

  return w;
}



/**
 * Draws a glyph at x,y in color cval
 * @author Hj. Malthaner
 */
int font_t::draw_glyph(graphics_t * gr, const int x, int y, 
		       const int glyph, const unsigned short cval) const 
{
  const unsigned short * glyph_data = get_glyph_data(glyph);
  const int glyph_width = get_width(glyph);
  const int graph_width = gr->get_width();
  const unsigned short * end = glyph_data + get_height();

  const clip_dimension_t & clip = gr->get_clip();

  unsigned short * data = gr->get_data();
  int screen_pos = y*graph_width + x;
    
  do {
    unsigned short c = *glyph_data++;       /* Eine Zeile des Zeichens */

    if(y >= clip.y && y <= clip.yy) {

      for(int b=0; b<glyph_width; b++) {
	if((c & 1) && b+x >= clip.x && b+x <= clip.xx) {
	  data[screen_pos+b] = cval;
	}

	c >>= 1;
      }
    }
    screen_pos += graph_width;
    y++;
  } while(glyph_data < end);

  return glyph_width;
}


/**
 * Draws a string at x,y in color color onto the graphics object
 * @author Hj. Malthaner
 */
void font_t::draw_string(graphics_t * gr, const char * text, 
			 int x, int y, int spacing, color_t & color) const
{
  const unsigned short cval = color.get_system_color(gr->get_system_color());

  while(*text) {
    unsigned char c = ((unsigned char)(*text++));
    if(c == 9) {
      // Hajo: tabstops every 8 pixels
      x = (x+8) & 0x7FFFFFF8;
    } else {
      x += draw_glyph(gr, x, y, c, cval) + spacing; 
    }
  }
}
