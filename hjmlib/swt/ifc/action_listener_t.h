/* Copyright by Hj. Malthaner */

#ifndef ACTION_LISTENER_T_H
#define ACTION_LISTENER_T_H

class gui_action_trigger_t;


/** @interface */
class action_listener_t {
public:    

    virtual void action_triggered(gui_action_trigger_t * trigger) = 0;
};
#endif //ACTION_LISTENER_T_H
