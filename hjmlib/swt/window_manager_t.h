/* 
 * window_manager_t.h
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib/SWT project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef WINDOW_MANAGER_T_H
#define WINDOW_MANAGER_T_H


#ifndef koord_h
#include "primitives/koord.h"
#endif

class system_window_t;
class drawable_t;
class event_t;

template <class T> class minivec_tpl;

/**
 * The window mager can be put upon a system window to manage placement and
 * movement of drawables as well as event handling. Drawables can be windows
 * or any other drawable things.
 * @author Hj. Malthaner
 */
class window_manager_t {
private:

  static window_manager_t * single_instance;

  /**
   * The list of all managed drawable objects (windows and others)
   * @author Hj. Malthaner
   */
  minivec_tpl <drawable_t *> * windows;
  

  minivec_tpl <drawable_t *> * kill_list;

  /**
   * The underlying system window.
   * @author Hj. Malthaner
   * @supplierCardinality 1
   */
  system_window_t * syswin;


  /**
   * The tooltip to display at the top of everything
   * @author Hj. Malthaner
   */
  drawable_t * tooltip;

public:

    static window_manager_t * get_instance() {return single_instance;};


    /**
     * Basic Constructor. Usually you need only one window manager, so
     * this should only be called once, and all components which need to
     * access the window manager should call get_instance(). But if you need
     * for some reason multiple window managers you can create them with this
     * constructor. get_instance() always return the last constructed instance.
     * @author Hj. Malthaner
     */
    window_manager_t(system_window_t *sys);


    ~window_manager_t();


    /**
     * Redraws all windows in specified area..
     * @author Hj. Malthaner
     */
    void redraw(koord pos, koord size) {
      redraw(pos.x, pos.y, size.x, size.y);
    };


    /**
     * Redraws all windows in specified area..
     * @author Hj. Malthaner
     */
    void redraw(int x, int y, int w, int h);


    /**
     * Draws all windows.
     * @author Hj. Malthaner
     */
    void redraw();


    /**
     * Gets the width of the system window.
     * @author Hj. Malthaner
     */
    int get_width();


    /**
     * Gets the height of the system window.
     * @author Hj. Malthaner
     */
    int get_height();



    bool is_window_managed(drawable_t *drable) const;


    /**
     * Checks if the given window is the top level window that
     * consumes events
     * @author Hj. Malthaner
     */
    bool is_top_event_window(drawable_t *drable) const;


    /**
     * Adds a drawable (window) to the list.
     * @author Hj. Malthaner
     */
    void add_window(drawable_t *drawable);


    /**
     * Adds a drawable (window) to the list.
     * @author Hj. Malthaner
     */
    void remove_window(drawable_t *drawable);


    /**
     * Brings a drawable (window) to front.
     * @author Hj. Malthaner
     */
    void top_window(drawable_t *drawable);


    /**
     * Closes all windows.
     * @author Hj. Malthaner
     */
    void remove_all_windows();


    /**
     * Checks system window for new events and hands them to the 
     * drawables.
     * @author Hj. Malthaner
     */
    event_t & poll_and_process_events();

    event_t & get_and_process_events();


    /**
     * Cleans up list of removed windows. Only needed to be called if
     * windows are removed outside of the event handling!
     * @author Hj. Malthaner
     */
    void process_kill_list();


    /**
     * Allows program code to insert event into the window system
     * @author Hj. Malthaner
     */
    bool process_event(event_t &ev);


    /**
     * Show a tool tip like text
     * @author Hj. Malthaner
     */
    void show_tool_tip(drawable_t * tt, koord pos);

    
    /**
     * Hide the tool tip text
     * @author Hj. Malthaner
     */
    void unshow_tool_tip();
};
#endif //WINDOW_MANAGER_T_H
