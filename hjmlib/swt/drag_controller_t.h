/* Copyright by Hj. Malthaner */

#ifndef DRAG_CONTROLLER_T_H
#define DRAG_CONTROLLER_T_H

#include "event_listener_t.h"

class drawable_t;
class gui_frame_t;
class drop_source_t;
class drop_target_t;

template <class T> class slist_tpl;

/**
 * A class which controls drag gestures. Supports dragging within a frame.
 * Drop targets must be registered to be used as targets. Can only drag drawables.
 * @author Hj. Malthaner
 */
class drag_controller_t  : public event_listener_t
{
private:
   /**
    * the thing to drag around
    * @author Hj. Malthaner
    */
   drawable_t *item;
   
   gui_frame_t *frame;

   slist_tpl <drop_target_t *> * targets;

   /**
    * Needed to put back items if dragging is stopped
    * @author Hj. Malthaner
    */
   drop_source_t *source;

public:

   drag_controller_t();
   virtual ~drag_controller_t();

   void set_frame(gui_frame_t *frame);

   void add_drop_target(drop_target_t *target);
   void remove_drop_target(drop_target_t *target);
   
   void start_dragging(drop_source_t *source, drawable_t *drawable);

   drawable_t * get_drag() const {return item;};


   /**
    * Tells this controller to abort the current dragging action
    * @author Hj. Malthaner
    */
   void stop_dragging();


   /**
    * Process an event.
    * @return true if event was swallowed, false otherwise
    * @author Hj. Malthaner
    */
   bool process_event(const event_t &);

};

#endif
