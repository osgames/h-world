#! /bin/bash

export CROSS=true

make clean
make dep
make

export CROSS=false

make clean
make dep
make
