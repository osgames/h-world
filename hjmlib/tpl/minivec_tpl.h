/* 
 * minivec_tpl.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 * This file is part of the Simutrans project and may not be used
 * in other projects without written permission of the author.
 */
 

#ifndef tpl_minivec_h                 
#define tpl_minivec_h


#include "vector_tpl.h"


template<class T> class minivec_iterator_tpl;


/**
 * A template class for a simple vector type. 
 * It can only store pointer types.
 * It is optimized to use as little memory as possible. It can only
 * hold up to 255 elements. Beware, this vector never shrinks.
 *
 * @date 24-Nov 2001
 * @author Hansj�rg Malthaner
 */
template<class T> class minivec_tpl : public vector_tpl <T, unsigned char>
{                     
  friend class minivec_iterator_tpl<T>;

 public:

  /**
   * Constructs an empty vector with a capacity of 16 elements.
   *
   * @param initial_capacity initial capacity
   * @author Hj. Malthaner
   */
  minivec_tpl() : vector_tpl <T, unsigned char> () {};


  /**
   * Constructs an empty vector with initial_capacity elements.
   *
   * @param initial_capacity initial capacity
   * @author Hj. Malthaner
   */
  minivec_tpl(int initial_capacity) : vector_tpl <T, unsigned char> (initial_capacity) {};


  minivec_tpl(const minivec_tpl <T> &other) : vector_tpl <T, unsigned char> (other) {}; 


  void operator= (const minivec_tpl <T> & other)
  {
    copy_from(other);
  };

};


/**
 * Iterator class for minivecs
 * @author Hj. Malthaner
 */
template<class T> class minivec_iterator_tpl : public vector_iterator_tpl<T, unsigned char>
{
 public:

  minivec_iterator_tpl(const minivec_tpl<T> *m) : vector_iterator_tpl <T, unsigned char> (m) {};
  
  minivec_iterator_tpl(const minivec_tpl<T> &m) : vector_iterator_tpl <T, unsigned char> (m) {};

};


#endif


