#ifndef tpl_debug_helper_h
#define tpl_debug_helper_h

#ifdef simdebug_h

#define ERROR dbg->error

#else

#include "stdio.h"
#include "stdarg.h"

static void output(const char *who, const char *format, ...)
{                                 
    va_list argptr;
    va_start(argptr, format);

    fprintf(stderr, "Error: %s:\t",who);
    vfprintf(stderr, format, argptr);                 
    fprintf(stderr,"\n");
}

#define ERROR output

#endif 

#endif
