/*
 * inthashtable_tpl.h
 *
 * a template class which implements a hashtable with int keys
 *
 * Copyright Hj.Malthaner, 2001
 */

#ifndef tpl_inthashtable_tpl_h
#define tpl_inthashtable_tpl_h

#include <stdio.h>

#ifndef tpl_slist_tpl_h
#include "slist_tpl.h"
#endif


/**
 * A template class which implements a hashtable with int keys.
 * It stores only pointer types, no value types!
 *
 * @date March 2001
 * @author Hj. Malthaner
 */
template<class KEY, class T> class inthashtable_tpl
{
private:
    class node_t {
    public:
        KEY key;
        T object;

	bool operator== (const node_t &other) const {
	  return key == other.key && object == other.object;
	};

	bool operator!= (const node_t &other) const {
	  return key != other.key || object != other.object;
	};
    };

    int bag_count;

    slist_tpl <node_t> *bags;


    unsigned long hash_KEY(const KEY key) const
    {                               
      return ((unsigned long)key) % bag_count;
    }                                           
 

public:

    inthashtable_tpl() {
      bag_count = 101;
      bags = new slist_tpl <node_t> [bag_count];
    }


    inthashtable_tpl(int count) {
      bag_count = count;
      bags = new slist_tpl <node_t> [bag_count];
    }


    /**
     * Destructor. Does not free the stored objects.
     *
     * @date Juli 2001
     * @author Hj. Malthaner
     */
    ~inthashtable_tpl()
    {
      delete [] bags;
      bags = 0;
    }


    T get(const KEY key) const
    {
	const unsigned long code = hash_KEY(key);

	slist_iterator_tpl<node_t> iter(bags[code]);

	while(iter.next()) {
	    node_t node = iter.get_current();
                     
	    if(node.key == key) {
		return node.object;
	    }
	}
                               
	return 0;
    }


    /**
     * Returns the old value
     * @author Hj. Malthaner
     */
    T put(const KEY key, const T & object)
    {                                                    
	const unsigned long code = hash_KEY(key);

	slist_iterator_tpl<node_t> iter(bags[code]);

	while(iter.next()) {
	    node_t &node = iter.access_current();
                     
	    if(node.key == key) {
	        T tmp = node.object;
		node.object = object;
		return tmp;
	    }
	}

	node_t node;

	node.key = key;
	node.object = object;

	bags[code].insert( node );

	return 0;
    }


    bool remove(const KEY key)
    {                                                    
	const unsigned long code = hash_KEY(key);

	slist_iterator_tpl<node_t> iter(bags[code]);

	while(iter.next()) {
	    node_t node = iter.get_current();
                     
	    if(node.key == key) {
	        bags[code].remove(node);
		return true;
	    }
	}

	return false;
    }


    void dump_stats()
    {
	for(KEY i=0; i<bag_count; i++) {
	    int count = bags[i].count();

	    printf("Bag %d contains %d elements\n", i, count);

	    slist_iterator_tpl<node_t> iter ( bags[i] );
	
	    while(iter.next()) {
		node_t node = iter.get_current();
            
		printf(" %s\n", node.key);
	    } 
	}
    }


    slist_tpl<const KEY> *get_keys() const
    {        
	slist_tpl<const KEY> *keys = new slist_tpl<const KEY>;

	for(int i=0; i<bag_count; i++) {
	    slist_iterator_tpl<node_t> iter ( bags[i] );
	
	    while(iter.next()) {
		node_t node = iter.get_current();
            
		keys->insert(node.key);
	    } 
	}
	return keys;
    }


    slist_tpl<T> *get_elements()
    {                                                    
	slist_tpl<T>* result = new slist_tpl<T>();

	for(int i=0; i<bag_count; i++) {

	    slist_iterator_tpl<node_t> iter(bags[i]);

	    while(iter.next()) {
		node_t node = iter.get_current();
		result->insert(node.object);
	    }
	}

	return result;
    }
};

#endif
