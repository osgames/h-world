/*
 * stringhashtable.h
 *
 * a template class which implements a hashtable with string keys
 *
 * Copyright Hj.Malthaner, 2001
 */

#ifndef tpl_stringhashtable_tpl_h
#define tpl_stringhashtable_tpl_h

#include <string.h>
#include <stdio.h>

#include "slist_tpl.h"


#define STHT_BAGSIZE 101

/**
 * A template class which implements a hashtable with string keys.
 * It stores only pointer types, no value types!
 *
 * @date March 2001
 * @author Hj. Malthaner
 */
template<class T> class stringhashtable_tpl
{
private:
    class node_t {
    public:
        const char * key;
        T object;
    };

    slist_tpl <node_t> bags[STHT_BAGSIZE];


    unsigned int hash_string(const char * key) const
    {                               
      unsigned int hash = 0;

      while(*key) {
	hash += (hash << 5) + *((const unsigned char*)key)++;
      }

      return hash % STHT_BAGSIZE;
    }                                           
 

public:

    /**
     * Destructor. Does not free the stored objects.
     *
     * @date Juli 2001
     * @author Hj. Malthaner
     */
    ~stringhashtable_tpl()
    {

    }


    T* access(const char *key) const
    {
	const unsigned int code = hash_string(key);

	slist_iterator_tpl<node_t> iter(bags[code]);

	while(iter.next()) {
	    node_t node = iter.get_current();
                     
	    if(strcmp(node.key, key) == 0) {
		return &(node.object);
	    }
	}
                               
	return 0;
    }


    T get(const char *key) const
    {
	const unsigned int code = hash_string(key);

	slist_iterator_tpl<node_t> iter(bags[code]);

	while(iter.next()) {
	    node_t node = iter.get_current();
                     
	    if(strcmp(node.key, key) == 0) {
		return node.object;
	    }
	}
                               
	return 0;
    }

    
    /**
     * Returns the old value
     * @author Hj. Malthaner
     */
    T put(const char * key, T object)
    {                                                    
	const unsigned int code = hash_string(key);

	slist_iterator_tpl<node_t> iter(bags[code]);

	while(iter.next()) {
	    node_t &node = iter.access_current();
                     
	    if(strcmp(node.key, key) == 0) {
	        T tmp = node.object;
		node.object = object;
		return tmp;
	    }
	}

	node_t node;

	node.key = key;
	node.object = object;

	// new entry
	bags[code].insert( node );

	return 0;
    }


    void dump_stats()
    {
	for(int i=0; i<STHT_BAGSIZE; i++) {
	    int count = bags[i].count();

	    printf("Bag %d contains %d elements\n", i, count);

	    slist_iterator_tpl<node_t> iter ( bags[i] );
	
	    while(iter.next()) {
		node_t node = iter.get_current();
            
		printf(" %s\n", node.key);
	    } 
	}
    }


    slist_tpl<const char *> *get_keys() const
    {        
	slist_tpl<const char *> *keys = new slist_tpl<const char *>;

	for(int i=0; i<STHT_BAGSIZE; i++) {
	    slist_iterator_tpl<node_t> iter ( bags[i] );
	
	    while(iter.next()) {
		node_t node = iter.get_current();
            
		keys->insert(node.key);
	    } 
	}
	return keys;
    }


    slist_tpl<T> *get_elements()
    {                                                    
	slist_tpl<T>* result = new slist_tpl<T>();

	for(int i=0; i<STHT_BAGSIZE; i++) {

	    slist_iterator_tpl<node_t> iter(bags[i]);

	    while(iter.next()) {
		node_t node = iter.get_current();
		result->insert(node.object);
	    }
	}

	return result;
    }
};

#endif
