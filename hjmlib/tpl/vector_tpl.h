/* 
 * vector_tpl.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 * This file is part of the Simutrans project and may not be used
 * in other projects without written permission of the author.
 */
 

#ifndef tpl_vector_h                 
#define tpl_vector_h


#include "no_such_element_exception.h"


template<class T, class IT> class vector_iterator_tpl;


/**
 * A template class for a simple vector type. 
 * It can only store pointer types.
 * 
 * @param T  data type
 * @param IT index type, should be unsigned type always
 *
 * @date 24-Nov 2001
 * @author Hansj�rg Malthaner
 */
template<class T, class IT> class vector_tpl
{                     
private:

    friend class vector_iterator_tpl <T, IT>;

    /**
     * Pointer to our data
     * @author Hj. Malthaner
     */
    T* data;


    /**
     * Current size (number of elements)
     * @author Hj. Malthaner
     */
    IT size;


    /**
     * Capacity.
     * @author Hj. Malthaner
     */
    IT capacity;


    void destroy() {
      if(size == 0) {
	delete [] data;
	data = 0;
	capacity = 0;
      }
    } 

public:
    
    /**
     * Constructs an empty vector with a capacity of 16 elements.
     *
     * @param initial_capacity initial capacity
     * @author Hj. Malthaner
     */
    vector_tpl() {
      data = 0;
      size = 0;                      
      capacity = 0;
    };


    /**
     * Constructs an empty vector with initial_capacity elements.
     *
     * @param initial_capacity initial capacity
     * @author Hj. Malthaner
     */
    vector_tpl(IT initial_capacity) {
	data = new T[initial_capacity];
	size = 0;                      
	capacity = initial_capacity;
    };


    vector_tpl(const vector_tpl <T, IT> &other) {
      data = 0;
      size = 0;                      
      copy_from(other);
    };


    ~vector_tpl() {
      delete [] data;
      data = 0;
      size = 0;                      
      capacity = 0;
    }


    void copy_from(const vector_tpl <T, IT> & other) {
      capacity = other.capacity;
      size = other.size;

      delete [] data;

      data = new T[capacity];

      for(int i=0; i<capacity; i++) {
        data[i] = other.data[i];
      }
    };


    /**
     * Access element i. Throw no_such_element_exception if the 
     * element does not exist.
     * @throw no_such_element_exception
     * @author Hj. Malthaner
     */
    T& at(IT i) {
	if(i<size) {
	    return data[i];
	} else {
	    throw new no_such_element_exception();
	}
    };


    /**
     * Read element i.
     * @throw no_such_element_exception
     * @author Hj. Malthaner
     */
    const T & get(IT i) const {
	if(i<size) {
	    return data[i];
	} else {
	    throw new no_such_element_exception();
	}
    };


    /**
     * Appends an element
     * @author Hj. Malthaner
     */
    void append(const T &v) {
	if(size < capacity) {
	    // printf("vector_tpl::append(): Using unused element\n");
	    data[size++] = v;
	} else {
	    // printf("vector_tpl::append(): Growing, old cap %d, old size %d\n", capacity, size);
	    T* old = data;
	    data = new T[capacity+1];
                
	    for(unsigned int i=0; i<capacity; i++) {
		data[i] = old[i];
	    }
	    data[capacity++] = v;
	    size++;                                                          
	    delete [] old;
	    // printf("vector_tpl::append(): Growing, new cap %d, new size %d\n", capacity, size);
	}
    };
     

    /**
     * Sets vector to emtpy
     * @author Hj. Malthaner
     */
    void clear() {
      size = 0;
    };


    /**
     * Removes element at index i
     * @author Hj. Malthaner
     */
    void remove_at(IT i) {
      if(i < size) {
	size --;
	for(int j=i; j<size; j++) {
	  data[j] = data[j+1];
	}
	data[size] = T();
      }
    }


    /**
     * Removes an element
     * @author Hj. Malthaner
     */
    bool remove(const T &v) {
	for(int i=0; i<size; i++) {
	    if(data[i] == v) {
	        remove_at(i);
		return true;
	    }
	}
	return false;
    };


    /**
     * Checks if an element is already contained
     * @author Hj. Malthaner
     */
    bool contains(const T &v) const { 
	for(int i=0; i<size; i++) {
	    if(data[i] == v) {
	      return true;
	    }
	}
	return false;
    }


    /**
     * Returns the number of elements in this vector
     * @author Hj. Malthaner
     */
    IT count() const {
	return size;
    };


    void operator= (const vector_tpl <T, IT> & other)
    {
      copy_from(other);
    };
};



/**
 * Iterator class for vectors
 * @author Hj. Malthaner
 */
template<class T, class IT> class vector_iterator_tpl
{                             
private:
  const vector_tpl <T, IT> *model;
                             
  IT curr_i;

public:

  vector_iterator_tpl(const vector_tpl<T, IT> *m)
  {
    curr_i = -1;
    model = m;
  }
  
  vector_iterator_tpl(const vector_tpl<T, IT> &m)
  {
    curr_i = -1;
    model = &m;
  }

  /**
   * start iteration
   * @author Hj. Malthaner
   */
  void begin()
  {
    curr_i = -1;
  }

  /**
   * iterate next element
   * @return false, if no more elements
   * @author Hj. Malthaner
   */
  bool next()
  {
    curr_i ++;
    return curr_i < model->size;
  }

  /**
   * @return the current element (as const reference)
   * @author Hj. Malthaner
   */
  const T& get_current() const
  {
    return model->data[curr_i];
  }

  /**
   * @return the current element (as reference)
   * @author Hj. Malthaner
   */
  T& access_current()
  {
    return model->data[curr_i];
  }
};


#endif


