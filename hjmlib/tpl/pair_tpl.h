/* 
 * pair_tpl.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 */

#ifndef tpl_pair_tpl_h
#define tpl_pair_tpl_h


/**
 * Holds two elements
 *
 * @date March 2001
 * @author Hj. Malthaner
 */
template<class T1, class T2> class pair_tpl
{
 public:
  T1 e1;
  T2 e2;

  pair_tpl() {};

  pair_tpl(const T1 &a1, const T2 &a2) {e1=a1; e2=a2;};

};

#endif
