#ifndef tpl_no_such_element_exception_h
#define tpl_no_such_element_exception_h
      

/**
 * Container throw this if the requested element doesn't exist
 * and there is no way to return a value like NULL to tell the
 * caller about the problem.
 *
 * @author Hj. Malthaner
 */
class no_such_element_exception
{
  public:

  // a message line or something should be included
  char reason[1024];
};

#endif
