/* 
 * handle_tpl.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 * This file is part of the Simutrans project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef tpl_handle_tpl_h
#define tpl_handle_tpl_h

#include <typeinfo>

#ifndef tpl_inthashtable_tpl_h
#include "inthashtable_tpl.h"
#endif

// Notify about all counting
// #define HTALK_DEBUG

// Notify about destrcution of objects
// #define HTALK_DEBUG2


// define this to use key_lock error checking
// may only be used with guarded_alloc and guarded_free
// #define USE_KEYLOCK


template <class REP> class handle_node_t {
 public:
  
  /**
   * The object this handle represents 
   * @author Hj. Malthaner
   */
  REP * rep;
  int count;
  
  handle_node_t(REP * o, int i) {
    rep = o;
    count = i;
  };
};


/**
 * a template class for reference counting. Supports automatic
 * deallocation of objects if all references are removed.
 *
 * @date March 2001
 * @author Hj. Malthaner
 */
template<class T> class handle_tpl
{
private:

  static inthashtable_tpl <unsigned long, handle_node_t <T> *> objects;

  handle_node_t <T> * the_node;
  

#ifdef USE_KEYLOCK
  int check;
#endif 
  


  /**
   * Unbinds this handle from the object, deletes the object if count == 0 
   *
   * @author Hj. Malthaner
   */
  void unbind_free()
  {
#ifdef HTALK_DEBUG  
      fprintf(stderr, "unbind_free: %d refs to %s %p\n", 
	     the_node->count, typeid(T).name(), the_node->rep);
#endif

    if(--(the_node->count) == 0) {

      // remove from table of objects
      const bool ok = objects.remove((unsigned long)(the_node->rep));

      if (the_node->rep && ok) {
#ifdef HTALK_DEBUG2
	fprintf(stderr, "unbind_free(): last ref destructed, deleting %s %p\n", 
	       typeid(T).name(), the_node->rep);
#endif

	delete the_node->rep;
      }
      
#ifdef HTALK_DEBUG2
      fprintf(stderr, "unbind_free(): last ref desctructed, deleting the node\n");
#endif

      delete the_node;
      the_node = 0;
    }
  }


public:

  int get_count() const {return the_node->count;};


  bool is_bound() const {
    return the_node->rep != 0;
  }

  
  /**
   * Overloaded dereference operator. With this, handles can used as if
   * they were pointers.
   *
   * @author Hj. Malthaner
   */
  T* operator->() const {
#ifdef USE_KEYLOCK
    if(check != *( ((int *)rep)-1 )) {
      fprintf(stderr, "Error: handle_tpl::operator->(): check != sig, check=%d, sig=%d\n", check, *( ((int *)rep)-1 ));
    }
#endif
    
    return the_node->rep;
  }


  /**
   * Get pointer to data - use with care! May be neccesary to hand
   * data to external libraries which do not use reference counting.
   * Inherently dangerous, use with care!
   *
   * @author Hj. Malthaner
   */
  T * get_rep() const {return the_node->rep;};


  handle_tpl()
  {
    the_node = objects.get(0);
    if(the_node) {
      the_node->count ++;
    } else {
      the_node = new handle_node_t <T> (0, 1);
      objects.put((unsigned long)(the_node->rep), the_node);
    }
  }

  
  /**
   * Basic constructor. Constructs a new handle.
   *
   * @author Hj. Malthaner
   */
  handle_tpl(T * p)
  {
#ifdef USE_KEYLOCK
    check = *( ((int *)p)-1 );
#endif
    
    // check if we already know this object
    the_node = objects.get((unsigned long)p);

    if(the_node) {
#ifdef HTALK_DEBUG
      fprintf(stderr, "There are already %d references to %s %p\n", 
	     the_node->count, typeid(T).name(), p);
#endif

      the_node->count ++;
    } else {
      // new object
      the_node = new handle_node_t <T> (p, 1);
      objects.put((unsigned long)(the_node->rep), the_node);
    }

#ifdef HTALK_DEBUG  
    fprintf(stderr, "ctor 1 ref to %s %p\n", typeid(T).name(), p);
#endif
  }


  /**
   * Copy constructor. Constructs a new handle from another one.
   *
   * @author Hj. Malthaner
   */
  handle_tpl(const handle_tpl& r)
  {
    the_node = r.the_node;
    the_node->count ++;

#ifdef USE_KEYLOCK
    check = r.check;
#endif
    
#ifdef HTALK_DEBUG  
    fprintf(stderr, "copy %d refs to %s %p\n", 
	   the_node->count, typeid(T).name(), the_node->rep);
#endif
  }


  /**
   * Assignment operator. Adjusts counters if one handle is 
   * assigned ot another one.
   *
   * @author Hj. Malthaner
   */
  void operator=(const handle_tpl &r)
  {
    // same object?
    if(the_node != r.the_node) {

      // different Object, one ref less
      unbind_free();

      // add ref
      the_node = r.the_node;
      the_node->count ++;
      
#ifdef HTALK_DEBUG  
      fprintf(stderr, "operator=(): %d refs to %s %p\n", 
	      the_node->count, typeid(T).name(), the_node->rep);
#endif
    }
  }


  /**
   * Destructor. Deletes object if this was the last handle.
   *
   * @author Hj. Malthaner
   */
  ~handle_tpl()
  {
#ifdef HTALK_DEBUG  
    fprintf(stderr, "~handle_tpl: %d refs to %s %p\n", 
	    the_node->count, typeid(T).name(), the_node->rep);
#endif

    unbind_free();
  }


  /**
   * Very hacky. If the handled object must be given to a library via pointer
   * and the last handle in our code will be destroyed after the call,
   * the refcount would fall to 0 and the object be deleted. To work
   * around this, the handled object can be unbound, iff only one handle 
   * remains.
   *
   * @author Hj. Malthaner
   */
  T * unbind() {
    T * tmp = the_node->rep;
    
    objects.remove((unsigned long)(the_node->rep));

    return tmp;
  }


  /**
   * Very hacky. Replace bound object with a new object. Does not delete
   * the old object.
   * @return pointer to the old object
   * @author Hj. Malthaner
   */
  T * rebind(T * o) {
    T * tmp = the_node->rep;

    objects.remove((unsigned long)(the_node->rep));
    the_node->rep = o;
    objects.put((unsigned long)(the_node->rep), the_node);

    return tmp;
  }

  

  /**
   * Conversion operator. With this handles can be converted to superclass
   * handles automatically.
   *
   * @author Hj. Malthaner
   */
  template<class T2> handle_tpl<T>::operator handle_tpl<T2> ()
  {
    return handle_tpl<T2> (rep);
  }


  bool operator== (const handle_tpl<T> &other) const {
    return the_node == other.the_node;
  }

  bool operator!= (const handle_tpl<T> &other) const {
    return the_node != other.the_node;
  }

};

// Hajo: a prime number around 1000 ???
template <class T> inthashtable_tpl <unsigned long,  handle_node_t <T> *> handle_tpl<T>::objects (1023);

#endif
