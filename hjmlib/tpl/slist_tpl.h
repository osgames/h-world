/* 
 * slist_tpl.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 * This file is part of the Simutrans project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef tpl_slist_tpl_h
#define tpl_slist_tpl_h

#include <stdlib.h>

#include <typeinfo>

#ifndef tpl_debug_helper_h
#include "debug_helper.h"
#endif

template<class T> class slist_iterator_tpl;

/**
 * A template class for a single linked list. Insert() and append()
 * work in fixed time. Maintains a list of free nodes to reduce calls
 * to new and delete.
 * 
 * @date November 2000
 * @author Hj. Malthaner
 */                    

template<class T>
class slist_tpl
{
private:
    class node_t
    {
        public:
	node_t *next;
	T data;
    };

    node_t * head;
    node_t * tail;
    node_t * freelist;

    int node_count;

    friend class slist_iterator_tpl<T>;


    node_t * gimme_node()
    {
    	if(freelist) {
	    node_t * tmp = freelist;
	    freelist = freelist->next;
	    return tmp;
	} else {
	    return new node_t();
	}
    }

    void putback_node(node_t *tmp)
    {    
      tmp->data = T();
      tmp->next = freelist;
      freelist = tmp;
    }


    void copy_from(const slist_tpl<T> &other)
    {
      slist_iterator_tpl<T> iter (other);

      while(iter.next()) {
	append(iter.get_current());
      }
    }

public:
          
    slist_tpl()
    {
	head = 0;             // leere liste
        tail = 0;
	freelist = 0;
	node_count = 0;
    }


    /**
     * Copy constructor - node deep copy. Beware in case of pointer
     * types, the pointer is copied, not the object!
     *
     * @author Hj. Malthaner
     */
    slist_tpl(const slist_tpl<T> &other)
    {
      copy_from(other);
    }


    ~slist_tpl()
    {
	destroy();
    }               


    /**
     * Inserts an element at the beginning of the list.
     *
     * @author Hj. Malthaner
     */                    
    void insert(T data)
    {   
	node_t *tmp = gimme_node();

	// vorne einfuegen

	tmp->next = head;
	head = tmp;
	head->data = data;

        if (tail == 0) tail = tmp;
        node_count ++;
    }


    void insert(T data, int pos)
    { // insert data at pos
      if (head == 0) { return; }
      if (pos >= node_count) {
	ERROR("slist_tpl<T>::insert()", 
	      "T=%s, index %d too large!", typeid(T).name(), pos);
	abort();
      }
      
      if (pos == 0) { // insert at front
	insert(data);
	return;
      }

      node_t *p = head;
      while(--pos) { p = p->next; }
      // insert between p and p->next
      node_t *tmp = gimme_node();
      tmp->data = data;
      tmp->next = p->next;
      p->next = tmp;
      
      if (tail == p) tail = tmp;
      node_count ++;
      return;
      
    }
    
    /**
     * Appends an element to the end of the list.
     *
     * @author Hj. Malthaner
     */                    
    void append(T data)
    {
      if (tail == 0) {
	insert(data);
      } else {
	node_t *tmp = gimme_node();
	tmp->data = data;
	tmp->next = 0;
	
	tail->next = tmp;
	tail = tmp;
	node_count ++;
      }
    }


    /**
     * Checks if the given element is already contained in the list.
     *
     * @author Hj. Malthaner
     */                    
    bool contains(const T data) const
    {
	node_t *p = head;

	while(p != 0 && !(p->data == data)) {
	    p = p->next;
	}

	return (p != 0);         // ist NULL wenn nicht gefunden
    }

    /**
     * Removes an element from the list
     *
     * @author Hj. Malthaner
     */                    
    bool remove(const T data)
    {
	bool removed = false;

	if(head != 0) {  // Liste nicht leer

	    if(head->data == data) {
		node_t *tmp = head->next;
		putback_node( head );
		head = tmp;  

		removed = true;
	        if (head == 0) {
		    tail = 0;
		}

		node_count --;
	    } else {
		node_t *p = head;

		while(p->next != 0 && !(p->next->data == data)) {
		    p = p->next;                                 
		}

		if(p->next != 0) {
		    node_t *tmp = p->next->next;
		    putback_node( p->next );
		    p->next = tmp;

		    removed = true;
		    if (tmp == 0) {
			tail = p;
		    }
		    node_count --;
		}
	    }
	}
	return removed;
    }

  
    bool remove_at(int pos)
    {    
      if (head == 0) { return false; }
      if (pos >= node_count) {
	ERROR("slist_tpl<T>::remove()", 
	      "T=%s, index %d too large!", typeid(T).name(), pos);
	abort();
      }
      
      if (pos == 0) { // remove first element
	node_t *tmp = head->next; 

        putback_node( head ); 

        head = tmp;
	node_count --;
	if (head == 0) {
          tail = 0;
        }
	return true;
      }

      node_t *p = head;
      while(--pos) { p = p->next; }
      // remove p->next
      node_t *tmp = p->next->next; 
      putback_node( p->next ); 
      p->next = tmp;
      if (tmp == 0) {
        tail = p;
      }
      node_count --;
      return true;
    }

    /**
     * Retrieves the first element from the list. This element is
     * deleted from the list. Useful for some queueing tasks.
     * @author Hj. Malthaner
     */
    T remove_first() {
	if(head) {
	    T tmp = head->data;
	    node_t *p = head;

            head = head->next;
	    putback_node(p);

	    node_count --;

	    if(head == 0) {
		// list is empty now
		tail = 0;
	    }

	    return tmp;
	} else {
  	    ERROR("slist_tpl<T>::remove_first()", 
	          "T=%s, called on empty list!", typeid(T).name()); 
	    abort();
	}
    }
    
  
    /**
     * Recycles all nodes. Doesn't delete the objects.
     * Leaves the list empty.
     * @author Hj. Malthaner
     */
    void clear()
    {                                                                   
	while(head) {
	    node_t * tmp = head->next;
	    putback_node( head );
	    head = tmp;
	}

        tail = 0;
	node_count = 0;
    }


    /**
     * Deletes all nodes and the freelist. Doesn't delete the objects.
     * Leaves the list empty.
     * @author Hj. Malthaner
     */
    void destroy()
    {                                                                   
	while(head) {
	    node_t * tmp = head->next;
	    delete head;
	    head = tmp;
	}   


	while(freelist != 0) {
	    node_t *tmp = freelist->next;
	    delete freelist;
	    // puts("plopp2");
	    freelist = tmp;
	}

        tail = 0;
	node_count = 0;
    }


    int count() const
    {                                                                   
	return node_count;
    }


    bool is_empty() const 
    {
	return head == 0;
    }


    T& at(int pos) const
    {    
	int i = 0;
	for(node_t *p = head; p != 0; p = p->next) {
	    if(i == pos) {
		return p->data;
	    }
	    i ++;
	}

	ERROR("slist_tpl<T>::at()", 
	      "T=%s, index %d out of bounds!", typeid(T).name(), pos);
	abort();
    }


    int index_of(T data) const
    {
	if(head) {
	    node_t *t = head;
	    int index = 0;

	    while(t && t->data != data) {
		t = t->next;
		index ++;
	    }

	    if(t && t->data == data) {
		return index;
	    }

	}

	return -1;
    }


    /**
     * Assignment operator - node deep copy. Beware in case of pointer
     * types, the pointer is copied, not the object!
     *
     * @author Hj. Malthaner
     */
    void operator= (const slist_tpl &other)
    {
      copy_from(other);
    };
};


/**
 * Iterator class for single linked lists.
 * @author Hj. Malthaner
 */
template<class T>
class slist_iterator_tpl
{                             
private:
                                 
    typename slist_tpl< T >::node_t *current_node;
    typename slist_tpl< T >::node_t lead;

public:

    slist_iterator_tpl(const slist_tpl<T> *list)
    {
	current_node = &lead; 
	lead.next = list->head;
    }


    slist_iterator_tpl(const slist_tpl<T> &list)
    {
	current_node = &lead; 
	lead.next = list.head;
    }


    /**
     * start iteration
     * @author Hj. Malthaner
     */
    void begin()
    {
	current_node = &lead;
    }


    /**
     * iterate next element
     * @return false, if no more elements
     * @author Hj. Malthaner
     */
    bool next()
    {
	return (current_node = current_node->next) != 0;
    }


    /**
     * @return the current element (as const reference)
     * @author Hj. Malthaner
     */
    const T& get_current() const
    {
	return current_node->data;
    }


    /**
     * @return the current element (as reference)
     * @author Hj. Malthaner
     */
    T& access_current()
    {
	return current_node->data;
    }
};

#endif
