/* Copyright by Hansj�rg Malthaner */

#ifndef IOFILE_T_H
#define IOFILE_T_H

#ifndef PTYPES_T_H
#include "ptypes.h"
#endif

#ifdef __linux__ 
struct _IO_FILE;
#define SYS_FILE _IO_FILE
#else 
struct _iobuf;
#define SYS_FILE struct _iobuf
#endif

/* Base class for persistent objects
 *
 * @author Hj. Malthaner
 */
class iofile_t
{
 private:

  SYS_FILE *file;

  bool saving;

  /**
   * Version information. Must be set by the application. Is not
   * modified or accessed by method of this class. Will be 
   * initialized to 0 in constructor.
   * @author Hj. Malthaner
   */
  int version;

 protected:
  void set_saving(bool s) {saving = s;};

 public:

  int get_version() const {return version;};
  void set_version(int v);

  const bool & is_saving() const {return saving;};

  iofile_t();
  virtual ~iofile_t();

  /**
   * @return true on success, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool open(const char *name, bool save);

  virtual void close();

  virtual bool eof();


  /**
   * Stores/restores a id.
   * @author Hj. Malthaner
   */
  virtual void rdwr_id(perid_t &id);


  /**
   * Stores/restores a signed char.
   * @author Hj. Malthaner
   */
  virtual void rdwr_char(signed char &i);


  /**
   * Stores/restores an unsigned char.
   * @author Hj. Malthaner
   */
  virtual void rdwr_uchar(unsigned char &i);


  /**
   * Stores/restores a short.
   * @author Hj. Malthaner
   */
  virtual void rdwr_short(short &i);


  /**
   * Stores/restores a unsigned short.
   * @author Hj. Malthaner
   */
  virtual void rdwr_ushort(unsigned short &i);


  /**
   * Stores/restores a int.
   * @author Hj. Malthaner
   */
  virtual void rdwr_int(int &i);


  /**
   * Stores/restores an unsigned int.
   * @author Hj. Malthaner
   */
  virtual void rdwr_uint(unsigned int &i);


  /**
   * Stores/restores an unsigned long.
   * @author Hj. Malthaner
   */
  virtual void rdwr_ulong(unsigned long &i);


  /**
   * Stores/restores a string in a character array.
   * @author Hj. Malthaner
   */
  virtual void rdwr_carr(char * str);


  /**
   * Stores/restores a string.
   * @author Hj. Malthaner
   */
  virtual void rdwr_string(char * &str);

};

#endif
