/* Copyright by Hansj�rg Malthaner */


#ifndef PERSISTENT_T_H
#define PERSISTENT_T_H

#ifndef PTYPES_T_H
#include "ptypes.h"
#endif

class storage_t;
class iofile_t;

/* Base class for persistent objects
 *
 * @author Hj. Malthaner
 */

class persistent_t
{
 public:


  virtual ~persistent_t() {};


  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file) = 0;


  /**
   * Gets a unique ID for this objects class, must be overriden by subclass.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid() = 0;


private:    

  /** @link dependency 
   * @stereotype uses*/
  /*#  iofile_t lnkiofile_t; */
};


#endif
