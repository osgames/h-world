/* Copyright by Hansj�rg Malthaner */

#ifndef GZIOFILE_T_H
#define GZIOFILE_T_H

#include "iofile_t.h"


/* Base class for persistent objects
 *
 * @author Hj. Malthaner
 */
class gziofile_t : public iofile_t
{
 private:

  void *file;

 public:

  gziofile_t();
  virtual ~gziofile_t();

  /**
   * @return true on success, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool open(const char *name, bool save);

  virtual void close();

  virtual bool eof();


  /**
   * Stores/restores a id.
   * @author Hj. Malthaner
   */
  virtual void rdwr_id(perid_t &id);


  /**
   * Stores/restores a signed char.
   * @author Hj. Malthaner
   */
  virtual void rdwr_char(signed char &i);


  /**
   * Stores/restores an unsigned char.
   * @author Hj. Malthaner
   */
  virtual void rdwr_uchar(unsigned char &i);


  /**
   * Stores/restores a short.
   * @author Hj. Malthaner
   */
  virtual void rdwr_short(short &i);


  /**
   * Stores/restores a unsigned short.
   * @author Hj. Malthaner
   */
  virtual void rdwr_ushort(unsigned short &i);


  /**
   * Stores/restores a int.
   * @author Hj. Malthaner
   */
  virtual void rdwr_int(int &i);


  /**
   * Stores/restores an unsigned int.
   * @author Hj. Malthaner
   */
  void rdwr_uint(unsigned int &i);


  /**
   * Stores/restores an unsigned long.
   * @author Hj. Malthaner
   */
  virtual void rdwr_ulong(unsigned long &i);


  /**
   * Stores/restores a string in a character array.
   * @author Hj. Malthaner
   */
  virtual void rdwr_carr(char * str);

  /**
   * Stores/restores a string.
   * @author Hj. Malthaner
   */
  virtual void rdwr_string(char * &str);

};

#endif
