/* Copyright by Hansj�rg Malthaner */

#include <stdio.h>
#include <string.h>

#include <zlib.h>

#include "gziofile_t.h"



gziofile_t::gziofile_t() : iofile_t()
{
  file = 0;
}


gziofile_t::~gziofile_t() 
{
  close();
}


bool gziofile_t::open(const char *name, bool save) {

  file = gzopen(name, save ? "wb6" : "rb");
  
  set_saving(save);

  return file != 0;
}


void gziofile_t::close() {
  // int dummy;
  // printf("close() '%s'\n", gzerror(file, &dummy));

  if(file) {
    gzclose(file);
    file = 0;
  }

  set_saving(false);
}


bool gziofile_t::eof() 
{
  return gzeof(file);
}


/**
 * Stores/restores a id.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_id(perid_t &id) 
{
  if(is_saving()) {
    gzwrite(file, &id, sizeof(perid_t));
  } else {
    gzread(file, &id, sizeof(perid_t));
  }
}


/**
 * Stores/restores a char.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_char(signed char &i)
{
  if(is_saving()) {
    gzwrite(file, &i, sizeof(signed char));
  } else {
    gzread(file, &i, sizeof(signed char));
  }
}


/**
 * Stores/restores an unsigned char.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_uchar(unsigned char &i)
{
  if(is_saving()) {
    gzwrite(file, &i, sizeof(unsigned char));
  } else {
    gzread(file, &i, sizeof(unsigned char));
  }
}


/**
 * Stores/restores a short.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_short(short &i)
{
  if(is_saving()) {
    gzwrite(file, &i, sizeof(short));
  } else {
    gzread(file, &i, sizeof(short));
  }
}


/**
 * Stores/restores a unsigned short.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_ushort(unsigned short &i)
{
  if(is_saving()) {
    gzwrite(file, &i, sizeof(unsigned short));
  } else {
    gzread(file, &i, sizeof(unsigned short));
  }
}


/**
 * Stores/restores a int.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_int(int &i)
{
  if(is_saving()) {
    gzwrite(file, &i, sizeof(int));
  } else {
    gzread(file, &i, sizeof(int));
  }
}


/**
 * Stores/restores an unsigned int.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_uint(unsigned int &i)
{
  if(is_saving()) {
    gzwrite(file, &i, sizeof(unsigned int));
  } else {
    gzread(file, &i, sizeof(unsigned int));
  }
}


/**
 * Stores/restores an unsigned long.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_ulong(unsigned long &i)
{
  if(is_saving()) {
    gzwrite(file, &i, sizeof(unsigned long));
  } else {
    gzread(file, &i, sizeof(unsigned long));
  }
}


/**
 * Stores/restores a string in a character array.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_carr(char * str)
{
  int len;

  if(is_saving()) {
    len = strlen(str)+1;          // trailing 0!
    rdwr_int(len);
    gzwrite(file, str, len);
  } else {
    rdwr_int(len);
    gzread(file, str, len);
  }
}


/**
 * Stores/restores a string.
 * @author Hj. Malthaner
 */
void gziofile_t::rdwr_string(char * &str) 
{
  int len;

  if(is_saving()) {
    if(str) {
      len = strlen(str)+1;          // trailing 0!
      rdwr_int(len);
      gzwrite(file, str, len);
    } else {
      len = -1;
      rdwr_int(len);      
    }
  } else {
    rdwr_int(len);
    if(len != -1) {
      str = new char[len];
      gzread(file, str, len);
    } else {
      str = 0;
    }
  }
}
