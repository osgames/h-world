/* Copyright by Hansj�rg Malthaner */


#include "storage_t.h"
#include "iofile_t.h"
#include "persistent_t.h"
#include "persistence_exception_t.h"
#include "tpl/inthashtable_tpl.h"


storage_t::storage_t()
{
  // Hajo: a prime number around 8192 ???
  table = new inthashtable_tpl <perid_t, void *> (8191);
}


storage_t::~storage_t()
{
  delete(table);
  table = 0;
}


inline void * storage_t::get(perid_t oid) const
{
  return table->get(oid);
}


inline void storage_t::put(perid_t oid, void *pobj)
{
  table->put(oid, pobj);
}


/**
 * Stores a persistent object into a iofile_t object
 * @author Hj. Malthaner
 */
void storage_t::store(persistent_t *pobj, iofile_t *file)
{
  perid_t cid = t_null;
  perid_t oid = 0;

  if(pobj) {
    cid = pobj->get_cid();
    oid = ptr2oid(pobj);
  }

  // write header
  file->rdwr_id(cid);
  file->rdwr_id(oid);

  if(pobj) {
    // cached ?
    if(get(oid) == 0) {  // no -> store
      
      put(oid, pobj);

      // write body
      pobj->read_write(this, file);
    }
  }
}


/**
 * Stores a string into a iofile_t object
 * @author Hj. Malthaner
 */
void storage_t::store(const char *pobj, iofile_t *file)
{
  perid_t cid = t_null;
  perid_t oid = 0;

  if(pobj) {
    cid = t_string;
    oid = ptr2oid(pobj);
  }

  // write header
  file->rdwr_id(cid);
  file->rdwr_id(oid);
  
  if(pobj) {
    // cached ?
    if(get(oid) == 0) {  // no -> store

      put(oid, const_cast <char *> (pobj));

      // write body
      file->rdwr_string(const_cast<char *>(pobj));
    }
  }
}


/**
 * Restores a object from a iofile_t object, includes error 
 * checking for expected type.
 * @author Hj. Malthaner
 */
void * storage_t::restore(iofile_t *file, perid_t check)
{
  // for debugging we trace the type of the last loaded object,
  // often this is the real culprit

  static perid_t last_loaded = 0;


  void *pobj = 0;
  perid_t cid;
  perid_t oid;


  if(file->eof()) {
    persistence_exception_t pe;
    sprintf(pe.msg, "storage_t::restore(): unexpected end of file");
    throw pe;
  }


  // read header
  file->rdwr_id(cid);
  file->rdwr_id(oid);

  // printf("restoring cid=%ld oid=%ld\n", cid, oid); 


  if(check != t_any && cid != t_null && check != cid) {
    persistence_exception_t pe;
    sprintf(pe.msg, "storage_t::restore(): wrong object type, expected %ld, found %ld (%lx), last type was %ld", check, cid, cid, last_loaded);

    throw pe;
  }

  last_loaded = cid;

  if(cid != t_null) {
    // cached ?
    pobj = get(oid);

    // printf("restoring get() -> pobj=%p\n", pobj); 
    
    
    if(pobj == 0) {  // no -> create
      pobj = create_intern(cid, oid, file);

      // printf("restoring create() -> pobj=%p\n", pobj); 
    }

    if(pobj == 0) {
      persistence_exception_t pe;
      sprintf(pe.msg, "storage_t::restore(): unexpected null object");
      throw pe;
    }
  }


  // printf("restoring done cid=%ld oid=%ld pobj=%p\n", cid, oid, pobj); 

  return pobj;
}


/**
 * Instantiates well known types, calls create() for other types
 * @param cid class id of the object to create
 * @author Hj. Malthaner
 */
void * storage_t::create_intern(perid_t cid, perid_t oid, iofile_t *file)
{
  void *result = 0;

  switch(cid) {
  case t_string:
    char * str;

    file->rdwr_string(str);

    result = str;

    // put into cache
    put(oid, result);

    break;
  default:
    persistent_t *pobj = create(cid);

    // put into cache
    put(oid, pobj);

    // read body
    pobj->read_write(this, file);

    result = pobj;
    break;
  }
  
  return result;
}
