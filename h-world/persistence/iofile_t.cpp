/* Copyright by Hansj�rg Malthaner */

#include <stdio.h>
#include <string.h>
#include "iofile_t.h"

void iofile_t::set_version(int v)
{
  version = v;
}


iofile_t::iofile_t()
{
  file = 0;
  saving = false;
  version = 0;
}


iofile_t::~iofile_t() 
{
  close();
}


bool iofile_t::open(const char *name, bool save) {

  file = fopen(name, save ? "wb" : "rb");
  
  saving = save;

  return file != 0;
}


void iofile_t::close() {
  if(file) {
    fclose(file);
    file = 0;
    saving = false;
    version = 0;
  }
}


bool iofile_t::eof() {
  return feof(file);
}


/**
 * Stores/restores a id.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_id(perid_t &id) 
{
  if(saving) {
    fwrite(&id, sizeof(perid_t), 1, file);
  } else {
    fread(&id, sizeof(perid_t), 1, file);
  }
}


/**
 * Stores/restores a char.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_char(signed char &i)
{
  if(saving) {
    fwrite(&i, sizeof(signed char), 1, file);
  } else {
    fread(&i, sizeof(signed char), 1, file);
  }
}


/**
 * Stores/restores an unsigned char.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_uchar(unsigned char &i)
{
  if(saving) {
    fwrite(&i, sizeof(unsigned char), 1, file);
  } else {
    fread(&i, sizeof(unsigned char), 1, file);
  }
}


/**
 * Stores/restores a short.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_short(short &i)
{
  if(saving) {
    fwrite(&i, sizeof(short), 1, file);
  } else {
    fread(&i, sizeof(short), 1, file);
  }
}


/**
 * Stores/restores a unsigned short.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_ushort(unsigned short &i)
{
  if(saving) {
    fwrite(&i, sizeof(unsigned short), 1, file);
  } else {
    fread(&i, sizeof(unsigned short), 1, file);
  }
}


/**
 * Stores/restores a int.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_int(int &i)
{
  if(saving) {
    fwrite(&i, sizeof(int), 1, file);
  } else {
    fread(&i, sizeof(int), 1, file);
  }
}


/**
 * Stores/restores an unsigned int.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_uint(unsigned int &i)
{
  if(saving) {
    fwrite(&i, sizeof(unsigned int), 1, file);
  } else {
    fread(&i, sizeof(unsigned int), 1, file);
  }
}


/**
 * Stores/restores an unsigned long.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_ulong(unsigned long &i)
{
  if(saving) {
    fwrite(&i, sizeof(unsigned long), 1, file);
  } else {
    fread(&i, sizeof(unsigned long), 1, file);
  }
}


/**
 * Stores/restores a string in a character array.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_carr(char * str)
{
  int len;

  if(saving) {
    len = strlen(str)+1;          // trailing 0!
    rdwr_int(len);
    fwrite(str, len, 1, file);
  } else {
    rdwr_int(len);
    fread(str, len, 1, file);
  }
}


/**
 * Stores/restores a string.
 * @author Hj. Malthaner
 */
void iofile_t::rdwr_string(char * &str) 
{
  int len;

  if(saving) {
    len = strlen(str)+1;          // trailing 0!
    rdwr_int(len);
    fwrite(str, len, 1, file);
  } else {
    rdwr_int(len);
    str = new char[len];
    fread(str, len, 1, file);
  }
}
