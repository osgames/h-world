/* Copyright by Hansj�rg Malthaner */


#ifndef STORAGE_T_H
#define STORAGE_T_H

#ifndef PTYPES_T_H
#include "ptypes.h"
#endif

#ifndef IOFILE_T_H
#include "iofile_t.h"
#endif

class persistent_t;

template <class KEY, class T> class inthashtable_tpl;

/** 
 * Persistent storage system main class. It contains static methods
 * to store and restore objects from a iofile_t object (usually a disk
 * file). Needs to be subclassed for application specific persistent 
 * objects.
 *
 * @author Hj. Malthaner
 */
class storage_t
{
 public:

  /**
   * Constants for well known pointer types, allocated from top of range
   * @author Hj. Malthaner
   */
  enum ctypes {
    t_null=0xFFFFFFFF, 
    t_string=0xFFFFFFFE, 
    t_any=0xFFFFFFFD 
  };

 private:

  inthashtable_tpl<perid_t, void *> *table;

  /** @link dependency 
   * @stereotype uses*/
  /*#  iofile_t lnkiofile_t; */

  /**
   * Gets a unique ID for the object.
   * @author Hj. Malthaner
   */
  perid_t ptr2oid(const void * const p) const {return (perid_t)p;};

  
  void put(perid_t oid, void *pobj);


  /**
   * Instantiates well known types, calls create() for other types
   * @param cid class id of the object to create
   * @author Hj. Malthaner
   */
  void * create_intern(perid_t cid, perid_t oid, iofile_t *file);
 
 public:

  storage_t();

  virtual ~storage_t();


  /**
   * Gets a already restored object from the object cache. Needed
   * to restore pointers.
   * @author Hj. Malthaner
   */
  void * get(perid_t oid) const;


  /**
   * Stores a persistent object into a iofile_t object
   * @author Hj. Malthaner
   */
  void store(persistent_t *pobj, iofile_t *file);


  /**
   * Stores a string into a iofile_t object
   * @author Hj. Malthaner
   */
  void storage_t::store(const char *pobj, iofile_t *file);


  /**
   * Restores a object from a iofile_t object, includes error 
   * checking for expected type.
   * @author Hj. Malthaner
   */
  void * restore(iofile_t *file, perid_t check=t_any); 


  /**
   * Must be overriden by application specific subclass to
   * instantiate the appropriate object types
   * @param cid class id of the object to create
   * @author Hj. Malthaner
   */
  virtual persistent_t * create(perid_t cid) = 0;

};

#endif
