/* Copyright by Hansj�rg Malthaner */

#ifndef PTYPES_T_H
#define PTYPES_T_H

/**
 * Id type for persistence layer. Used for object ids and class ids.
 * Must be big enough to store a address, but at least 32 bit!
 * @author Hj. Malthaner
 */
typedef unsigned long perid_t;

#endif
