/* 
 * world_3d_view_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef world_3d_view_t_h
#define world_3d_view_t_h

#ifndef world_view_t_h
#include "world_view_t.h"
#endif

struct WandInfo;
class bitmap_t;

/**
 * 3D view for H-World maps
 *
 * @author Hj. Malthaner
 */
class world_3d_view_t : public world_view_t
{
 private:

  /**
   * Tracks displayed squares
   * @author Hj. Malthaner
   */
  bitmap_t * displayed;

  void draw_boden(graphics_t *gr, double links, double rechts);

  int hscan(int *lx, int *ly, const double winkel);
  int vscan(int *lx, int *ly, const double winkel);

  bool world_3d_view_t::cast_one_ray(const double winkel, 
				     const double blick,
				     struct WandInfo *treffer);

 public:

  world_3d_view_t();
  ~world_3d_view_t();


  /**
   * Clears the client level data, and prepares for a new level
   * @author Hj. Malthaner
   */
  virtual void prepare_level(koord size, 
			     const char * level_title,
			     koord level_offset);


  virtual void draw(graphics_t * gr, koord pos);

};

#endif // world_3d_view_t_h
