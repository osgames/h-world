/* 
 * world_2d_view_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef world_2d_view_t_h
#define world_2d_view_t_h


#ifndef world_view_t_h
#include "world_view_t.h"
#endif


/**
 * 2D view for H-World maps
 *
 * @author Hj. Malthaner
 */
class world_2d_view_t : public world_view_t
{
 private:

  int tile_width;
  int tile_height;

  /**
   * Grund von i,j an Bildschirmkoordinate xpos,ypos zeichnen.
   * @author Hj. Malthaner
   */
  void display_ground(graphics_t * gr, int i, int j, int xpos, int ypos);


  /**
   * Dinge von i,j an Bildschirmkoordinate xpos,ypos zeichnen.
   * @author Hj. Malthaner
   */
  void display_things(graphics_t *gr, int i, int j, int xpos, int ypos);


 public:


  /**
   * Calculates world coordinates from screen coordinates
   * @author Hansj�rg Malthaner
   */
  virtual koord screen_to_world(koord mpos) const;


  world_2d_view_t(const properties_t * game_props);
  virtual ~world_2d_view_t();



  virtual void draw(graphics_t * gr, koord pos);

};

#endif // world_2d_view_t_h
