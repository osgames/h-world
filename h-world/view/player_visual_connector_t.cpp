/* 
 * player_visual_connector_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "player_visual_connector_t.h"
#include "model/thing_t.h"
#include "model/thing_traversor_t.h"
#include "view/world_view_t.h"

#include "util/debug_t.h"
#include "util/properties_t.h"


void player_visual_connector_t::calc_overlays(const handle_tpl <thing_t> & thing)
{
  // if this monster has equipment, it needs a visual connector
  if(thing->get_int("things", 0)) {

    dbg->debug("calc_overlays()", 
	       "thing '%s'.", thing->get_string("ident", "unnamed"));
    
    player_visual_connector_t conn (thing, thing->visual);
    thing->call_listeners();
    thing->remove_listener(&conn);
  }
}


static image_meta_t get_image(const properties_t *props, 
			      const char * key,
			      const int def)
{
  image_meta_t meta;

  meta.img = def;
  meta.xoff = 0;
  meta.yoff = 0;
  meta.tileset = 0;          
  meta.colorset = props->get_int("colorset", 0);
  meta.trans = 0;

  // Hajo: get display image
  if(props->get_int(key)) {
    meta.img = *props->get_int(key);
    
  } else if(props->get_string(key)) {
    
    meta.tileset = 2;
    meta.img = 
      world_view_t::get_instance()
      ->load_tile_from_image(props->get_string(key));
      

  } else {
    
    dbg->message("get_image()", 
		 "thing '%s' without '%s'!", 
		 props->get_string("name"),
		 key);
  }

  return meta;
}



/**
 * Loads all required images for a thing.
 *
 * @param thing the thing to prepare (must not be NULL)
 * @param props the properties to read the values from
 *
 * @author Hj. Malthaner
 */
void player_visual_connector_t::prepare_images(thing_t *thing, 
					       const properties_t *props)
{
  // Hajo: determine image number from property values
  image_meta_t meta = get_image(props, "image", 1);

  thing->visual.set_image(0, meta);


  int inventory_tileset = 0;
  if(props->get_int("image.inventory.set")) {
    inventory_tileset = *props->get_int("image.inventory.set");
  }

  // animated ?
  if(props->get_int("frames")) {
    thing->visual.set_frames(*props->get_int("frames"));

    if(props->get_string("image")) {
      // load additional tiles
      char buf [256];
      const int l = sprintf(buf, props->get_string("image"));

      for(int i=1; i<thing->visual.get_frames(); i++) { 
	char number [8];
	sprintf(number, "%02d", i);
	buf[l-6] = number[0];
	buf[l-5] = number[1];

	world_view_t::get_instance()->load_tile_from_image(buf);
      }
    }
  }

  if(props->get_int("ftime")) {
    thing->visual.set_ftime(*props->get_int("ftime"));
  }

  if(props->get_int("ptime")) {
    thing->visual.set_ptime(*props->get_int("ptime"));
  }


  if(props->get_int("image.inventory")) {
    thing->inv_visual.set_image(0,
				*props->get_int("image.inventory"),
				inventory_tileset,
				0, 0, meta.colorset, false);

  } else if(props->get_string("image.inventory")) {
    

    thing->inv_visual
      .set_image(0,
		 world_view_t::get_instance()
		 ->load_tile_from_image(props->get_string("image.inventory")),
		 2,
		 0, 0, meta.colorset, false);

  } else {
    // give it a 'default' look ... 
    thing->inv_visual.set_image(0, 1, 0, 0, 0, 0, false);

    dbg->warning("prepare_images()",
		 "image.inventory entry is missing in thing");
  }
}


player_visual_connector_t::player_visual_connector_t(const thinghandle_t &pl, visual_t &v) : visual(v), player(pl)
{
  overlay_set = 0;
  player->add_listener(this);
}


player_visual_connector_t::~player_visual_connector_t()
{
  // nothing to do
}


void player_visual_connector_t::calc_overlay_images(const handle_tpl <thing_t> &limb,
						    const handle_tpl <thing_t> &thing)
{
  int tileset = 0;
  int overlay = -1;
  const char * overlay_set = thing->get_string("overlay.set", 0);
  char buf[64];

  // First, check if there is an explicit overlay/priority pair.
  if(overlay_set) {

    sprintf(buf, "overlay.%s_%c", overlay_set, sex); 

    const char * filename = thing->get_string(buf, 0);
    
    if(filename) {
      
      overlay = world_view_t::get_instance()
	->load_tile_from_image(filename);
      tileset = 2; // User defined tiles
      
    } else {
      overlay = thing->get_int(buf, -1);
    }
  }

  // Second, check if there is an default overlay/priority pair.
  if(overlay == -1) {
    
    sprintf(buf, "overlay.default_%c", sex); 

    const char * filename = thing->get_string(buf, 0);
    
    if(filename) {
      
      overlay = world_view_t::get_instance()
	->load_tile_from_image(filename);
      tileset = 2; // User defined tiles

    
    } else {
      overlay = thing->get_int(buf, -1);
    }
  }

  if(overlay >= 0) {
    const int priority = 
      thing->get_int("overlay.priority", 0) + 
      limb->get_int("item_offset.priority", 0) +
      16;

    const bool trans = thing->get_string("overlay.transparent", 0) != 0;
    
    images[priority].img = overlay;
    images[priority].tileset = tileset;
    images[priority].trans = trans;
    images[priority].colorset = thing->get_int("colorset", 0);
    
    images[priority].xoff = limb->get_int("item_offset.x", 0);
    images[priority].yoff = limb->get_int("item_offset.y", 0);

    // printf("  img=%x, x=%d, y=%d, pri=%d, file=%s\n", images[priority], offset_x[priority], offset_y[priority], priority, filename);
    
    // thing->dump();
  }
}


void player_visual_connector_t::visit_limb(const thinghandle_t &limb)
{
  calc_overlay_images(limb, limb);

  const thinghandle_t & thing = limb->get_item();

  if(thing.is_bound()) {
    calc_overlay_images(limb, thing);
  }
}


void player_visual_connector_t::update(const thinghandle_t &thing)
{
  dbg->debug("player_visual_connector_t::update()", "updateing %s", 
	     thing->get_string("name", "unnamed")); 

  thing_traversor_t trav;
  trav.set_model(thing);
  trav.set_limb_visitor(this);
  trav.set_thing_visitor(0);

  sex = *thing->get_string("sex", "m");
  overlay_set = thing->get_string("overlay_set", "default");

  visual.clear();

  for(int i=0; i<max_pri; i++) { 
    images[i].img  = 0x3FFF;
    images[i].xoff = 0;
    images[i].yoff = 0;
    images[i].tileset =  0;
    images[i].colorset =  0;
    images[i].trans = 0;
  }

  // images[15] = get_image(&thing->get_properties(), "image.shadow", -1);
  images[16] = get_image(thing->get_properties(), "image", 1);


  // build new visual
  trav.traverse();

  for(int i=0; i<max_pri; i++) { 
    if(images[i].img != 0x3FFF) {

      // printf("image=%d set=%d\n", images[i].img, images[i].tileset);

      visual.add_image(images[i]);
    }
  }


  world_view_t::get_instance()->set_dirty(true);
  // world_view_t::get_instance()->redraw();

  dbg->debug("player_visual_connector_t::update()", "done");
}
