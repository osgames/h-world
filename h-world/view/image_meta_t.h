/* 
 * image_meta_t.h
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef image_meta_t_h
#define image_meta_t_h


/**
 * Image meta data.
 * @author Hj. Malthaner 
 */
class image_meta_t
{
 public:
  // Image number
  unsigned short img;

  // x-offset
  signed char xoff;
  
  // y-offset
  signed char yoff;

  // Tileset number
  unsigned char tileset;

  // Colorset number
  unsigned char colorset;
  
  // Is this image transparent ?
  unsigned char trans;
  
  
  bool operator== (const image_meta_t other) const {
    return
      img == other.img &&
      xoff == other.xoff &&
      yoff == other.yoff &&
      tileset == other.tileset &&
      colorset == other.colorset &&
      trans == other.trans;
  };
};

#endif
