/* world_view_t.h
 *
 * Copyright (c) 2001 Hansj�rg Malthaner
 */

#ifndef world_view_t_h
#define world_view_t_h


#ifndef VISUAL_T_H
#include "visual_t.h"
#endif

#ifndef abs_view_t_h
#include "abs_view_t.h"
#endif

#ifndef DRAWABLE_T_H
#include "swt/drawable_t.h"
#endif

#ifndef tpl_minivec_h
#include "tpl/minivec_tpl.h"
#endif


class tileset_t;
class image_t;
class graphics_t;
class glasspane_t;

class world_view_ifc_t;

class ground_visual_t;
class font_t;

class properties_t;

template <class T> class stringhashtable_tpl; 


#define MAX_IMAGES 128

/**
 * View-Klasse f�r Weltmodell.
 *
 * @author Hj. Malthaner
 */
class world_view_t : public drawable_t, public abs_view_t
{
 public:

  static world_view_t * get_instance();
  static void create_instance(int v, properties_t * game_props);



 protected:
  
  glasspane_t *glasspane;


  /**
   * Tiles loaded from files, cached to avoid duplicates
   * @author Hj. Malthaner
   */
  stringhashtable_tpl <int *> *file_tiles;


  /**
   * Name of current level
   * @author Hj. Malthaner
   */
  const char * level_title;


  /**
   * Offset of current level
   * @author Hj. Malthaner
   */
  koord level_offset;


  /**
   * Size of current level
   */
  koord level_size;


  /**
   * Interface to get world data
   * @author Hj. Malthaner
   */
  world_view_ifc_t * world_view_ifc;



  tileset_t * tileset[4];


  /**
   * The world view can hold up to 128 images.
   * @author Hj. Malthaner
   */
  image_t * images[MAX_IMAGES];


  /**
   * Ground display floor offset
   * @author Hj. Malthaner
   */
  int x_off;

  /**
   * Ground display floor offset
   * @author Hj. Malthaner
   */
  int y_off;


  koord ij_off;


  /**
   * Redraw required.
   * @author Hj. Malthaner
   */
  bool dirty;


  /**
   * This array stores information which squares have been viewed
   * by the player
   * @author Hj. Malthaner
   */
  unsigned char * remembered;


  /**
   * This array stores information about squares feature type flags
   * @author Hj. Malthaner
   */
  unsigned int * flags;


  /**
   * Client display level data - this is not an exact copy of the 
   * server level data, but reflects the knowledge of the character 
   * about this level
   *
   * @author Hj. Malthaner
   */
  minivec_tpl <visual_t> * viewdata;


  ground_visual_t * grounds;


  /**
   * Painted outside the level
   */
  visual_t outside;


  /**
   * Show a visible grid?
   * @author Hj. Malthaner
   */
  int gridline;


  font_t *font;


  /**
   * @link
   * @shapeType PatternLink
   * @pattern Singleton
   * @supplierRole Singleton factory 
   */
  /*# world_view_t __world_view_t; */
  static world_view_t * single_instance;


  /**
   * Grund von i,j an Bildschirmkoordinate xpos,ypos zeichnen.
   * @author Hj. Malthaner
   */
  void display_ground(graphics_t * gr, int i, int j, int xpos, int ypos);


  /**
   * Dinge von i,j an Bildschirmkoordinate xpos,ypos zeichnen.
   * @author Hj. Malthaner
   */
  void display_things(graphics_t *gr, int i, int j, int xpos, int ypos);


  /**
   * Forward (world->screen) projection
   * @author Hj. Malthaner
   */
  koord forward(koord k);

  world_view_t();
  virtual ~world_view_t();

  // Debugging
  void draw_m(graphics_t * gr, koord pos);
  
 public:

  
  /**
   * Dither/blend ground tile borders?
   * @author Hj. Malthaner
   */
  bool dither_grounds;


  /**
   * Mouse cursor position -> highlite
   * @author Hj. Malthaner
   */
  koord cursor_ij;


  /**
   * Calculates world coordinates from screen coordinates
   * @author Hansj�rg Malthaner
   */
  virtual koord screen_to_world(koord mpos) const;


  /**
   * Set gridded display. Some subclasses may ignore this.
   * @return former grid state
   * @author Hansj�rg Malthaner
   */
  virtual bool use_grid(bool yesno);


  /**
   * Check all visuals if animations are there and redraw the
   * animations that need update
   * @author Hj. Malthaner
   */
  void animate(unsigned long time);


  bool is_remembered(koord k) const;

  const bool is_dirty() const {return dirty;};
  void set_dirty(bool d) {dirty = d;}; 
    
  koord get_ij_off() const {return ij_off;};

  void set_ij_off(koord off);


  void set_world_view_ifc(world_view_ifc_t *ifc);

  
  bool initialize(const char *base);


  /**
   * Read custom images and add them to the tileset.
   * @return new tile number
   * @author Hj. Malthaner
   */
  int load_tile_from_image(const char *filename);


  /**
   * Current tileset getter method.
   * @author Hj. Malthaner
   */
  const tileset_t * get_tileset(int set) const {
    return tileset[set];
  };


  image_t * get_image(int i) const {
    if(i >= 0 && i < MAX_IMAGES) return images[i]; else return 0;
  };
  

  /**
   * Clears the client level data, and prepares for a new level
   * @author Hj. Malthaner
   */
  virtual void prepare_level(koord size, 
			     const char * level_title,
			     koord level_offset);


  /**
   * After a new level was created, the images must be 
   * updated. world_t will call this to update the images
   * @author Hj. Malthaner
   */
  void prepare_image(int i, const char *filename);


  /**
   * After loading a game, some image numbers might have changed.
   * Thus we need to resync our data with the worlds data
   * @author Hj. Malthaner
   */
  void resync();


  virtual void draw(graphics_t * gr, koord pos);


  /**
   * Callback for level updates
   */
  void update_ground(koord k, ground_visual_t *data);


  /**
   * Callback for level updates
   */
  void clear_visuals(koord k);


  /**
   * Callback for level updates
   */
  virtual void add_feature_visual(koord k, const visual_t * visual, 
				  unsigned int flags);


  /**
   * Callback for level updates
   */
  void add_visual(koord k, const visual_t * visual);


  /**
   * Callback for level updates
   */
  void set_dark(koord k);
    

  /**
   * Triggers throwing/magic effects
   */
  void play_effect(koord origin, koord target,
		   bool show_1, bool show_2, bool show_3,
		   int image, int n);


  /**
   * Stores/restores the object. 
   * @author Hj. Malthaner
   */
  void read_write(storage_t *store, iofile_t * file);

};


#endif
