#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"


#include "ground_visual_t.h"


ground_visual_t::ground_visual_t() {
  shade[0] = shade[1] = shade[2] = shade[3] = 0;
  height[0] = height[1] = height[2] = height[3] = 0;

  tex = 0x8000;   // Tiled floor number 0
};


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void ground_visual_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_ushort(tex);

  file->rdwr_char(shade[0]);
  file->rdwr_char(shade[1]);
  file->rdwr_char(shade[2]);
  file->rdwr_char(shade[3]);

  file->rdwr_char(height[0]);
  file->rdwr_char(height[1]);
  file->rdwr_char(height[2]);
  file->rdwr_char(height[3]);
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t ground_visual_t::get_cid()
{
  return hstore_t::t_ground_visual;
}
