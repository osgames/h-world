/* 
 * world_view_t.cpp
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include <math.h>
#include <string.h>

#include "world_view_ifc_t.h"
#include "model/map_model_t.h"
#include "model/feature_t.h"

#include "util/debug_t.h"
#include "util/rng_t.h"
#include "util/nsow.h"
#include "util/string_helper.h"

#include "swt/font_t.h"
#include "swt/color_t.h"
#include "swt/graphics_t.h"
#include "swt/image_t.h"
#include "swt/tileset_t.h"
#include "swt/tile_descriptor_t.h"
#include "swt/system_window_t.h"

#include "primitives/cstring_t.h"
#include "primitives/bitmap_t.h"

#include "tpl/stringhashtable_tpl.h"

#include "gui/gui_utils.h"

#include "world_view_t.h"
#include "world_2d_view_t.h"
#include "world_3d_view_t.h"
#include "glasspane_t.h"
#include "visibob_t.h"

#include "ground_visual_t.h"

#include "persistence/iofile_t.h"

#include "../fontdef.h"


/* Hajo: 
 * all code should derive values from this constant
 * 03-Sep-03: Be careful some code doesn't and must be fixed!
 */
#define TILE_SIZE  64


/* Hajo:
 * nine part walls basic block size
 */
#define WALL_SIZE  24


world_view_t * world_view_t::single_instance = 0;


void world_view_t::create_instance(int v, properties_t * game_props)
{
  if(v == 0) {
    single_instance = new world_view_t();
  } else if(v == 1) {
    single_instance = new world_2d_view_t(game_props);
  } else {
    single_instance = new world_3d_view_t();    
  }
}


world_view_t * world_view_t::get_instance()
{
  return single_instance;
}



static tile_descriptor_t *scale_tile(const tile_descriptor_t *in)
{
  image_t src (128, 128, RGB555);
  image_t dst (128, 128, RGB555);
  color_t color (128,128,136);

  graphics_t *gr = src.get_graphics();

  gr->fillbox_wh(0, 0, 128, 128, color);
  gr->draw_tile(in, 0, 0);


  gr = dst.get_graphics();
  gr->fillbox_wh(0, 0, 128, 128, color);

  gr->draw_image(&src, 32, 48, 64, 64, 0xFFFF);
  // gr->draw_image(&src, 0, 0);

  tile_descriptor_t *t = dst.encode_tile();

  return t;
}



/**
 * Forward (world->screen) projection
 * @author Hj. Malthaner
 */
inline koord world_view_t::forward(koord k)
{
  /*
  return koord((k.x - k.y)*18 + 325,
	       (k.y + k.x)*9 + 205);
  */

  return koord((k.x - k.y)*32 + 325,
	       (k.y + k.x)*16 + 205) - koord(32,20);
}


/**
 * Calculates world coordinates from screen coordinates
 * @author Hansj�rg Malthaner
 */
koord world_view_t::screen_to_world(koord mpos) const
{
  mpos -= get_pos();

  const int x = 16*(mpos.x + TILE_SIZE + 16) / TILE_SIZE;
  int y = 32*(mpos.y - 24) / TILE_SIZE;

  const int i_off = ij_off.x-13;
  const int j_off = ij_off.y-0;

  int i = ((y + x) >> 4) + i_off;
  int j = ((y - x) >> 4) + j_off;

  // printf("%d, %d -> %d, %d\n", mpos.x, mpos.y, i, j);

  for(int n=0; n<3; n++) { 
    const int idx = i + j*level_size.x;

    if(i>=0 && j>=0 && i<level_size.x && j < level_size.y) {
    
      ground_visual_t *ground = grounds+idx;
      const int yoff = (ground->height[0] + ground->height[1] + ground->height[2] + ground->height[3]) >> 2; 
      int yneu = 32*(mpos.y - 24 - yoff) / TILE_SIZE;
    
      i = ((yneu + x) >> 4) + i_off;
      j = ((yneu - x) >> 4) + j_off;
    }
  }

  // printf("     -> %d, %d\n", i, j);

  return koord(i,j);
}


/**
 * Set gridded display. Some subclasses may ignore this.
 * @return former grid state
 * @author Hansj�rg Malthaner
 */
bool world_view_t::use_grid(bool yesno)
{
  bool old = (gridline != 0);

  if(yesno) {
    gridline = 1;
  } else {
    gridline = 0;
  }

  return old;
}


void world_view_t::prepare_level(koord k,
				 const char * level_title,
				 koord level_offset)
{
  dbg->message("world_view_t::prepare_level()", 
	       "preparing size %dx%d: %s", k.x, k.y, level_title);

  this->level_title = level_title;
  this->level_offset = level_offset;


  const int tiles = k.x * k.y;
  level_size = k;

  // clear level data

  delete [] viewdata;
  delete [] grounds;
  delete [] remembered;
  delete [] flags;
  
  viewdata = new minivec_tpl <visual_t> [tiles];
  grounds  = new ground_visual_t [tiles];
  remembered = new unsigned char[tiles];
  flags = new unsigned int[tiles];


  memset(remembered,
	 0,
	 // 1,   // magically know all the level
	 tiles*sizeof(unsigned char));


  for(int i=0; i<tiles; i++) { 
    grounds[i].set_image(0x8020);         // Tiled, basic outside tile
    flags[i] = 0;
  }


  map_model_t::get_instance()->init();


  redraw();
}


/**
 * After a new level was created, the images must be 
 * updated. world_t will call this to update the images
 * @author Hj. Malthaner
 */
void world_view_t::prepare_image(int i, const char *filename)
{
  if(i >= 0 && i < MAX_IMAGES) {
    delete images[i];
    images[i] = new image_t(filename, RGB555);
  }
} 


bool world_view_t::is_remembered(koord k) const
{
  return 
    k.x>=0 && k.y>=0 && k.x<level_size.x && k.y<level_size.y && 
    remembered[k.y*level_size.x+k.x];
}

void world_view_t::set_ij_off(koord off) {
  ij_off = off;
  dirty = true;

  cursor_ij = koord(-1, -1);
};



void world_view_t::set_world_view_ifc(world_view_ifc_t *ifc)
{
  world_view_ifc = ifc;
}



world_view_t::world_view_t()
{
  directions = directions_iso;

  world_view_ifc = 0;

  x_off = 0;
  y_off = 0;

  viewdata = 0;
  grounds = 0;
  remembered = 0;
  flags = 0;
  gridline = 0;

  level_title = "";

  for(int i=0; i<MAX_IMAGES; i++) { 
    images[i] = 0;
  }

  glasspane = new glasspane_t();
  glasspane->set_opaque(false);

  file_tiles = new stringhashtable_tpl <int *> ();

  dirty = true;

  font = font_t::load(FONT_SML);

  cursor_ij = koord(-1,-1);
}


world_view_t::~world_view_t()
{
  delete glasspane;
  glasspane = 0;

  delete file_tiles;
  file_tiles = 0;
}


bool world_view_t::initialize(const char *base) 
{
  cstring_t filebase (".");

  if(base) {
    filebase = base;
  }

  bool ok = false;

  tileset[0] = new tileset_t();
  tileset[1] = new tileset_t();
  tileset[2] = new tileset_t(4096);  // Hajo: User defind images
  tileset[3] = new tileset_t();

  dbg->message("world_view_t::initialize", "filebase = %s", filebase.chars());

  if(tileset[0]->load(filebase + "/daten128.pak", 128)) {
    ok = true;
  } else {
    dbg->fatal("world_view_t::initialize", "Cannot read %s/daten128.pak",
	       filebase.chars());
  }

  // Tileset 1 is unused currently
  // if(tileset[1]->load(filebase + "/daten128.pak", 128)) {
  //   ok = true;
  // }


  // load user graphics
  tileset[3]->load(filebase + "/user128.pak", 128);


  /* Hajo: for testing only
  for(int i=0; i<tileset[0]->get_count(); i++) {
    const tile_descriptor_t *t = tileset[0]->get_tile(i);

    tileset[0]->set_tile(i, scale_tile(t));
  }
  */

  viewdata = 0;
  grounds = 0;
  remembered = 0;
  flags = 0;

  // 32 is default outside look
  outside.set_image(0, 32, 0, 0, 0, 0, false);


  dbg->message("world_view_t::initialize", "ok = %d", ok);

  return ok;
}


/**
 * Read custom images and add them to the tileset.
 * @return new tile number
 * @author Hj. Malthaner
 */
int world_view_t::load_tile_from_image(const char *filename)
{
  int *ip = file_tiles->get(filename);

  if(ip == 0) {

    // dbg->debug("world_view_t::load_tile_from_image", "%s", filename);
    image_t img (filename, RGB555);
    tile_descriptor_t *t = img.encode_tile();

    ip = new int ();
    *ip = tileset[2]->append(t);

    file_tiles->put(str_duplicate(filename), ip);

    delete t;


    dbg->message("world_view_t::load_tile_from_image", "loaded %s at %d (%p)", filename, *ip, tileset[2]->get_tile(*ip));
  } else {
    // dbg->message("world_view_t::load_tile_from_image", "found %s at %d", filename, *ip);
  }

  return *ip;
}


/**
 * Check all visuals if animations are there and redraw the
 * animations that need update
 * @author Hj. Malthaner
 */
void world_view_t::animate(unsigned long time)
{
  static unsigned long last_time = 0;
  static unsigned int  total = 0;

  if(time - last_time > 64) {
    last_time = time;

    const koord size = get_size();
    
    const int dpy_width  = size.x * 2 / TILE_SIZE + 3;
    const int dpy_height = size.y * 4 / TILE_SIZE + 10;
    
    const int i_off = ij_off.x-13;
    const int j_off = ij_off.y-0;
    

    bitmap_t map (level_size.x, level_size.y);
    map.clr_map();
    
    for (int y = -6; y < dpy_height; y++) {
      for (int x = (y & 1); x <= dpy_width; x += 2) {
	
	const int i = ((y + x) >> 1) + i_off;
	const int j = ((y - x) >> 1) + j_off;
	
	if(i >= 0 && j >= 0 && i < level_size.x && j < level_size.y) {
	  const int idx = i + j*level_size.x;
	  minivec_tpl <visual_t> & look = viewdata[idx];
	  
	  // ground_visual_t *ground = grounds+idx;
	  
	  minivec_iterator_tpl <visual_t> iter (look);
	  
	  while( iter.next() ) {
	    visual_t & visual = iter.access_current();
	    const int frames = visual.get_frames();
	    if(frames > 1) {

	      if(visual.get_current() == 0 && visual.get_ptime() != 0) {
		const int pmask = ((4 << visual.get_ptime()) - 1);

		if((total & pmask) == 0) {
		  visual.inc_frame();
		  map.set_bit(i,j);

		  // printf("ptime: total=%d\n", total);
		}

	      } else {
		const int step = ((1 << visual.get_ftime()) - 1);

		if((step & total) == 0) {
		  visual.inc_frame();
		  map.set_bit(i,j);

		  // printf("ftime: total=%d\n", total);
		}
	      }
	    }
	  }
	}
      }
    }  
    
    const koord pos = get_pos();

    for (int y = -6; y < dpy_height; y++) {
      const int ypos = y * TILE_SIZE / 4;
      
      for (int x = (y & 1); x <= dpy_width; x += 2) {
	
	const int i = ((y + x) >> 1) + i_off;
	const int j = ((y - x) >> 1) + j_off;
	const int xpos = x * TILE_SIZE / 2;
	
	if(i >= 0 && j >= 0 && 
	   i < level_size.x && j < level_size.y &&
	   map.get_bit(i,j)) {
	  const int idx = i + j*level_size.x;
	  ground_visual_t *ground = grounds+idx;
	  const int yoff = (ground->height[0] + ground->height[1] + ground->height[2] + ground->height[3]) >> 2; 

	  // dbg->message("world_view_t::animate()", "redraw x=%d y=%d w=%d h=%d", xpos, ypos+yoff, 128+80, 128+80);
	  
	  redraw(koord(xpos-pos.x, ypos-pos.y+yoff), koord(128, 128)); 
	}
      }
    }

    total ++;
  }
}


/**
 * After loading a game, some image numbers might have changed.
 * Thus we need to resync our data with the worlds data
 * @author Hj. Malthaner
 */
void world_view_t::resync()
{
  dbg->message("world_view_t::resync()", "called");

  koord k;

  for(k.y=0; k.y<level_size.y; k.y++) {
    for(k.x=0; k.x<level_size.x; k.x++) {
      if(is_remembered(k)) {
	world_view_ifc->display_update(k.x, k.y, this, true);
      }
    }
  }
}


void world_view_t::draw_m(graphics_t * gr, koord pos) 
{
  for(int y=0; y<gr->get_height(); y+=2) { 
    for(int x=0; x<gr->get_width(); x+=2) { 
      koord c = screen_to_world(koord(x,y));
      color_t color (abs(c.x*32) & 255, abs(c.y*32) & 255, 128);
      gr->fillbox_wh(x, y, 2, 2, color);
    }
  }
}


void world_view_t::draw(graphics_t * gr, koord pos) 
{
  // dbg->message("world_view_t::draw()", "called, dirty=%d, i=%d, j=%d", dirty, ij_off.x, ij_off.y);

  // zuerst den boden zeichnen
  // denn der Boden kann kein Objekt verdecken
  
  const koord size = get_size();
  const int dpy_width  = size.x * 2 / TILE_SIZE + 3;
  const int dpy_height = size.y * 4 / TILE_SIZE + 10;
  const clip_dimension_t & clip = gr->get_clip();

  /*
  const int i_off = ij_off.x-15;
  const int j_off = ij_off.y-13;
  */

  const int i_off = ij_off.x-13;
  const int j_off = ij_off.y-0;

  /*
  y_off = (ij_off.y + ij_off.x) * 9;
  x_off = (ij_off.y - ij_off.x) * -18;
  */

  y_off = (ij_off.y + ij_off.x) * 16;
  x_off = (ij_off.y - ij_off.x) * -32;


  const system_window_t * syswin = system_window_t::get_instance();

  unsigned long T0 = syswin->get_time();

  if(dirty && clip.w == size.x && clip.h == size.y) {
    // get update from server
    world_view_ifc->recalculate_view(ij_off, 
				     dpy_width, dpy_height,
				     this);
    dirty = false;
  }

  unsigned long T1 = syswin->get_time();


  // Set triangle border dithering flag
  gr->set_border_dither(dither_grounds);


  // clear screen

  gr->fillbox_wh(pos.x, pos.y, size.x, size.y, color_t::BLACK);
  
  // start displaying
  // draw_m(gr, pos);

  for (int y = -6; y < dpy_height; y++) {
    const int ypos = y * TILE_SIZE / 4;
    
    for (int x = (y & 1); x <= dpy_width; x += 2) {
      
      const int i = ((y + x) >> 1) + i_off;
      const int j = ((y - x) >> 1) + j_off;
      const int xpos = x * TILE_SIZE / 2;
      
      display_ground(gr, i, j, xpos, ypos);
    }
  }
  
  // dann die Objekte Zeichnen
  
  for (int y = -6; y < dpy_height; y++) {
    const int ypos = y * TILE_SIZE / 4;
    
    for (int x = (y & 1); x <= dpy_width; x += 2) {
      
      const int i = ((y + x) >> 1) + i_off;
      const int j = ((y - x) >> 1) + j_off;
      const int xpos = x * TILE_SIZE / 2;
      
      display_things(gr, i, j, xpos, ypos);
    }
  }


  // Debug clip?
  // dbg->message("world_view_t::draw()", "clip x=%d y=%d w=%d h=%d", clip.x, clip.y, clip.w, clip.h);

  unsigned long T2 = syswin->get_time();

  glasspane->draw(gr, pos);


  koord off = get_pos() + size - koord(100, 34);

  if(level_offset.x != 0 || level_offset.y != 0) {
    char buf[256];
    sprintf(buf, "Offset %d,%d", level_offset.x, level_offset.y);

    font->draw_string(gr, buf, off.x, off.y, 0, color_t::WHITE);
  }

  off.y += 14;

  font->draw_string(gr, level_title, off.x - 100, off.y, 0, color_t::WHITE);

  tileborderframe(gr, pos, size);

  dbg->debug("world_view_t::draw","update time %lu ms, paint time %lu ms", T1-T0, T2-T1);
}



/**
 * Callback for level updates
 */
void world_view_t::update_ground(koord k, ground_visual_t *data)
{
  const int idx = k.x + k.y*level_size.x;
  const unsigned short old_tex = grounds[idx].tex;

  grounds[idx] = *data;

  // printf("ground at %d,%d has tex %d\n", k.x, k.y, grounds[idx].tex);

  if(data->tex != 0xFFFF) {
    remembered[idx] = true;
  } else {
    grounds[idx].tex = old_tex;
  }

  map_model_t::get_instance()->update(k);
}


/**
 * Callback for level updates
 */
void world_view_t::clear_visuals(koord k)
{
  const int idx = k.x + k.y*level_size.x;

  viewdata[idx].clear();
  flags[idx] = 0;
}


/**
 * Callback for level updates
 */
void world_view_t::add_feature_visual(koord k, 
				      const visual_t * visual, 
				      unsigned int fl)
{
  const int idx = k.x + k.y*level_size.x;
  viewdata[idx].append(*visual);
  flags[idx] = fl;
}


/**
 * Callback for level updates
 */
void world_view_t::add_visual(koord k, const visual_t *visual)
{
  const int idx = k.x + k.y*level_size.x;

  viewdata[idx].append(*visual);
}


/**
 * Callback for level updates
 */
void world_view_t::set_dark(koord k)
{
  if(k.x >= 0 && k.y >= 0 && k.x < level_size.x && k.y < level_size.y) {
    const int idx = k.x + k.y*level_size.x;

    if(remembered[idx]) {

      minivec_tpl <visual_t> & views = viewdata[idx];

      for(int i=views.count()-1; i>=0; i--) { 
	if(views.at(i).is_transient()) {
	  views.remove_at(i);
	} else {
	  views.at(i).set_transparent(false);
	  views.at(i).set_shade(10);
	}
      }

      signed char * shade = grounds[idx].shade;
      shade[3] = shade[2] = shade[1] = shade[0] = 6;
    }
  }
}


#define CAP(x,y) ((x) > (y) ? (y) : (x))

/**
 * Grund von i,j an Bildschirmkoordinate xpos,ypos zeichnen.
 * @author Hj. Malthaner
 */
void world_view_t::display_ground(graphics_t * gr, 
                                  const int i, const int j, 
				  int xpos, int ypos)
{
  if(i >= 0 && j >= 0 && i < level_size.x && j < level_size.y) {
    const int idx = i + j*level_size.x;
    ground_visual_t *ground = grounds+idx;

    const bool is_tiled = (ground->get_image(0) & 0x8000);
    const int  tex = (ground->get_image(0) & 0x7FFF);

    // if(!is_tiled)
    //  printf("i=%d j=%d   tiled=%d tex=%X\n", i, j, is_tiled, tex);

    const signed char highlite = cursor_ij.x == i && cursor_ij.y == j ? 8 : 0;
    const signed char cap = 32;

    if(is_tiled) {

      const tileset_t *tiles = world_view_t::get_instance()->get_tileset(0);

      gr->draw_tile_shaded(tiles->get_tile(tex),
			   xpos,
			   ypos,
			   CAP(ground->shade[0] + highlite, cap)
			   );
    } else if(images[tex]) {

      static koord points[3];
      static signed char shades[3];

      xpos += 32;

      // Old - wrong - value, needed for old wall tile positions
      ypos += 81;

      // ypos += 92;

      points[0] = koord(xpos + TILE_SIZE/2, 
			ypos + ground->height[0]);
      points[1] = koord(xpos,    
			ypos + TILE_SIZE/4 + ground->height[1] - gridline);
      points[2] = koord(xpos + TILE_SIZE/2, 
                        ypos + TILE_SIZE/2 + ground->height[2] - (gridline<<1));

      shades[0] = CAP(ground->shade[0] + highlite, cap);
      shades[1] = CAP(ground->shade[1] + highlite, cap);
      shades[2] = CAP(ground->shade[2] + highlite, cap);

      gr->draw_triangle(points, shades, x_off, y_off, images[tex]);

      // TILE_SIZE+1 -> Overlap a little
      points[1] = koord(xpos + TILE_SIZE+1,    
			ypos + TILE_SIZE/4 + ground->height[3] - gridline);
      shades[1] = CAP(ground->shade[3] + highlite, cap); 

      gr->draw_triangle(points, shades, x_off, y_off, images[tex]);
    }
  } else {
    // there was nothing to paint there, so we use the default look
    outside.paint(gr, xpos, ypos);
  }
}


/**
 * Dinge von i,j an Bildschirmkoordinate xpos,ypos zeichnen.
 * @author Hj. Malthaner
 */
void world_view_t::display_things(graphics_t *gr, 
				  const int i, const int j, 
				  const int xpos, const int ypos)
{
  if(i >= 0 && j >= 0 && i < level_size.x && j < level_size.y) {
    const int idx = i + j*level_size.x;
    minivec_tpl <visual_t> & look = viewdata[idx];
    ground_visual_t *ground = grounds+idx;

    const int yoff = (ground->height[0] + ground->height[1] + ground->height[2] + ground->height[3]) >> 2; 

    int start = 0;

    if(look.count() && flags[idx] & feature_t::F_NINEPARTWALL) {
      const int conn_flags = 
	feature_t::F_NINEPARTWALL | feature_t::F_DOOR | feature_t::F_DOORPOST;
      unsigned int nbr [8];
      const koord  ij (i, j); 

      for(int n=0; n<8; n++) { 
	koord pos = ij + neighbour_8[n]; 

	if(pos.x >= 0 && pos.x < level_size.x &&
	   pos.y >= 0 && pos.y < level_size.y &&
	   remembered[pos.x + pos.y*level_size.x]) {

	  nbr[n] = flags[pos.x + pos.y*level_size.x];
	} else {
	  // nbr[n] = feature_t::F_NINEPARTWALL; 
	  nbr[n] = feature_t::F_OTHER;
	}
      }

      const int wall_y_off = 11;

      visual_t visual = look.get(0);


      // Hajo: constants for placement
      // capitat T is tile based
      // lowercase t is wallblock based
      const int T4 = TILE_SIZE/4;
      const int t4 = 24/4;

      const int T8 = TILE_SIZE/8;
      const int t8 = WALL_SIZE/8;

      // top top
      if(nbr[0] & conn_flags &&
	 nbr[6] & conn_flags &&
	 nbr[7] & conn_flags) {
	visual.set_x_off(0);
	visual.set_y_off(0);
	visual.paint(gr, xpos, ypos+yoff + wall_y_off);
      }


      // top
      if(nbr[0] & conn_flags) {
	visual.set_x_off(T4 - t4);
	visual.set_y_off(T8 - t8);
	visual.paint(gr, xpos, ypos+yoff + wall_y_off);
      }

      // left
      if(nbr[6] & conn_flags) {
	visual.set_x_off(t4 - T4);
	visual.set_y_off(T8 - t8);
	visual.paint(gr, xpos, ypos+yoff + wall_y_off);
      }


      // left left
      if(nbr[4] & conn_flags &&
	 nbr[5] & conn_flags &&
	 nbr[6] & conn_flags) {
	visual.set_x_off(t4+t4-T4-T4);
	visual.set_y_off(T4 - t4);
	visual.paint(gr, xpos, ypos+yoff + wall_y_off);
      }


      // center
      visual.set_x_off(0);
      visual.set_y_off(T4 - t4);
      visual.paint(gr, xpos, ypos+yoff + wall_y_off);


      // right right
      if(nbr[0] & conn_flags &&
	 nbr[1] & conn_flags &&
	 nbr[2] & conn_flags) {
	visual.set_x_off(T4+T4-t4-t4);
	visual.set_y_off(T4 - t4);
	visual.paint(gr, xpos, ypos+yoff + wall_y_off);
      }


      // bottom
      if(nbr[4] & conn_flags) {
	visual.set_x_off(t4 - T4);
	visual.set_y_off(T8 + t4 + 1);  // ???
	visual.paint(gr, xpos, ypos+yoff + wall_y_off);
      }


      // right
      if(nbr[2] & conn_flags) {
	visual.set_x_off(T4 - t4);
	visual.set_y_off(T8 + t4 + 1);  // ???
	visual.paint(gr, xpos, ypos+yoff + wall_y_off);
      }


      // bot bot
      if(nbr[2] & conn_flags &&
	 nbr[3] & conn_flags &&
	 nbr[4] & conn_flags) {
	visual.set_x_off(0);
	visual.set_y_off(T4+T4-t4-t4);
	visual.paint(gr, xpos, ypos+yoff + wall_y_off);
      }

      start = 1;
    }


    if(cursor_ij.x == i && cursor_ij.y == j) {
      // Hajo: draw with highlite

      for(int i=start; i<look.count(); i++) { 
	visual_t & v = look.at(i);
	int light = v.get_shade();

	v.set_shade(CAP(light + 8, 32));
	v.paint(gr, xpos, ypos+yoff);
	v.set_shade(light);
      }

    } else {
      for(int i=start; i<look.count(); i++) { 
	look.get(i).paint(gr, xpos, ypos+yoff);
      }
    }
  }
}



/**
 * Triggers magic effects
 */
void world_view_t::play_effect(koord origin, koord target,
			       bool show_1, bool show_2, bool show_3,
			       int image, int n)
{
  rng_t &rng = rng_t::get_the_rng();
  const double si = sin(0.1)*0.93;
  const double co = cos(0.1)*0.93;

  visibob_t * bobs[n];
  double bopos[n*2];
  double bovec[n*2];

  const koord center = forward(origin-ij_off);


  if(show_1) {
    // spiral in
    

    for(int i=0; i<n; i++) { 
      
      bobs[i] = new visibob_t(tileset[0]->get_tile(image));
      
      bopos[i*2] = rng.get_int(1000) - 500;
      bopos[i*2+1] = rng.get_int(1000) - 500;
      
      glasspane->add_component(bobs[i]);
    }
  

    for(int j=0; j<50; j++) { 
      for(int i=0; i<n; i++) { 
	double *p = bopos + i*2;
	
	p[0] = (p[0] * co + p[1] * si);
	p[1] = (-p[0] * si + p[1] * co);
	
	bobs[i]->set_pos_quiet(koord((short)(p[0]+center.x),
				     (short)(p[1]*0.5 + center.y)
				     ));
      }
      redraw();
    }
  } else {
    for(int i=0; i<n; i++) { 
      
      // printf("Image=%x\n", image);

      bobs[i] = new visibob_t(tileset[0]->get_tile(image));
      bobs[i]->set_size(koord(128,128));
      bopos[i*2] = 0;
      bopos[i*2+1] = 0;
      
      glasspane->add_component(bobs[i]);
    }
  }

  if(show_2) {
    const koord dest = forward(target-ij_off);
    const int dx = dest.x - center.x;
    const int dy = dest.y - center.y;
    int steps;

    printf("cent %d, %d\n", center.x, center.y);
    printf("dest %d, %d\n", dest.x, dest.y);



    if(abs(dx) > abs(dy)) { 
      steps = abs(dx)/12;
    } else {
      steps = abs(dy)/12;
    }

    if(steps == 0) steps = 1;

    // move to target
    bovec[0] = ((double)(dx+steps-1)) / steps;
    bovec[1] = ((double)(dy+steps-1)) / steps;

    for(int j=0; j<steps; j++) { 
      for(int i=0; i<n; i++) { 
	double *p = bopos + i*2;
	
	p[0] += bovec[0];
	p[1] += bovec[1];

	// printf("screenpos %d, %d\n", (int)(p[0] + center.x), (int)(p[1] + center.y));

	bobs[i]->set_pos_quiet(koord((short)(p[0]+center.x),
				     (short)(p[1]*0.5 + center.y)
				     ));
      }
      
      redraw();

      system_window_t::get_instance()->sleep(20);
    }
  }



  if(show_3) {
    // explode

    for(int i=0; i<n; i++) { 
      
      bovec[i*2] = (rng.get_int(201)-100);
      bovec[i*2+1] = (rng.get_int(201)-100);
      
    }
    
    
    for(int j=0; j<30; j++) { 
      for(int i=0; i<n; i++) { 
	double *p = bopos + i*2;
	double *v = bovec + i*2;
	
	p[0] += v[0];
	p[1] += v[1];
	
	bobs[i]->set_pos_quiet(koord((short)(p[0]+center.x),
				     (short)(p[1]*0.5 + center.y)
				     ));
      }
      
      redraw();
    }
  }

  glasspane->remove_all();

  redraw();
}


/**
 * Stores/restores the object. 
 * @author Hj. Malthaner
 */
void world_view_t::read_write(storage_t *store, iofile_t * file)
{
  const int size = level_size.x*level_size.y;

  for(int i=0; i<size; i++) { 
    file->rdwr_uchar(remembered[i]);
    file->rdwr_uint(flags[i]);

    if(remembered[i]) {
      grounds[i].read_write(store, file);

      unsigned char count = viewdata[i].count();
      file->rdwr_uchar(count);

      for(int j=0; j<count; j++) { 
	if(file->is_saving()) {
	  viewdata[i].at(j).read_write(store, file);
	} else {
	  visual_t dummy;
	  dummy.read_write(store, file);
	  viewdata[i].append(dummy);
	}
      }
    }
  }
}
