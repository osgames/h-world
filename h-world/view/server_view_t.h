#ifndef server_view_t_h
#define server_view_t_h

#include "abs_view_t.h"


/**
 * Server side view data buffer
 * @author Hj. Malthaner
 */
class server_view_t : public abs_view_t
{
 private:

  koord ij_off;

  /**
   * Buffer write mark
   * @author Hj. Malthaner
   */
  int mark;


  /**
   * Command and data buffer
   * @author Hj. Malthaner
   */
  char buf[1 << 16];

 public:


  /**
   * Set gridded display. Some subclasses may ignore this.
   * @return former grid state
   * @author Hansj�rg Malthaner
   */
  virtual bool use_grid(bool yesno);


  enum command {update, clear, add, dark};


  void set_ij_off(koord off) {
    ij_off = off;
  };


  server_view_t();


  virtual ~server_view_t();


  void init();


  void redraw();


  void * get_buffer() {return buf;};
  int get_mark() const {return mark;};


  /**
   * Callback for level updates
   */
  virtual void update_ground(koord k, ground_visual_t *data);


  /**
   * Callback for level updates
   */
  virtual void clear_visuals(koord k);


  /**
   * Callback for level updates
   */
  virtual void add_visual(koord k, const visual_t * visual);


  /**
   * Callback for level updates
   */
  virtual void add_feature_visual(koord k, const visual_t * visual, 
				  unsigned int flags);


  /**
   * Callback for level updates
   */
  virtual void set_dark(koord k);


  void dump();
};

#endif // server_view_t_h
