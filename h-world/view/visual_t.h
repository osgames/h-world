/* 
 * visual_t.h
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef VISUAL_T_H
#define VISUAL_T_H


#ifndef tpl_minivec_h
#include "tpl/minivec_tpl.h"
#endif

#ifndef image_meta_t_h
#include "image_meta_t.h"
#endif

class storage_t;
class iofile_t;
class graphics_t;
class cstring_t;


/**
 * Paints a visual representation on a graphics object
 * @author Hansj�rg Malthaner 
 */
class visual_t {
 public:

  static void read_colorshades(const cstring_t & base_dir);
  static const int * get_colorshades();

private:

  /**
   * Image numbers and flags.
   * @author Hansj�rg Malthaner 
   */
  minivec_tpl <image_meta_t> images;


  /**
   * X-Offset, measured in pixels.
   * @author Hansj�rg Malthaner 
   */
  signed char x_off;

  /**
   * Y-Offset, measured in pixels.
   * @author Hansj�rg Malthaner 
   */
  signed char y_off;

  signed char shade;


  /**
   * Multi-value member:
   * Bit  0  : opaqueness
   * Bit  1  : transient
   * Bits 2-6: unused
   * Bit  7  : transparency
   *
   * @author Hansj�rg Malthaner 
   */
  unsigned char multiset;


  /**
   * frame count
   * @author Hansj�rg Malthaner 
   */
  unsigned char frames;


  /**
   * current frame
   * @author Hansj�rg Malthaner 
   */
  unsigned char current;


public:    

  /**
   * Calculate random x and y offsets
   * @author Hansj�rg Malthaner 
   */
  void calc_jitter(int jitter);


  unsigned char get_frames() const {return frames & 31;};
  void set_frames(int f);
  void inc_frame();

  /**
   * Frame time
   * @author Hansj�rg Malthaner 
   */
  int  get_ftime() const {return frames >> 5;};
  void set_ftime(int ftime);


  /**
   * Sets current frame number
   * @author Hansj�rg Malthaner 
   */
  void set_current(int c);

  unsigned char get_current() const {return current & 31;};

  /**
   * Pause time
   * @author Hansj�rg Malthaner 
   */
  int  get_ptime() const {return current >> 5;};
  void set_ptime(int ptime);


  /**
   * Set shade. Useful range 0 .. 32
   * @author Hansj�rg Malthaner 
   */
  void set_shade(int shade) {this->shade = shade;};

  int get_shade() const {return shade;};


  unsigned char  get_image_count() const {return images.count();};
  image_meta_t & access_image(int n) {return images.at(n);};


  /**
   * See image_meta_t for details on the parameters
   *
   * @author Hansj�rg Malthaner 
   */
  void set_image(int index, const image_meta_t meta);

  void set_image(int index, int image, unsigned char set, 
		 signed char xoff, signed char yoff,
		 unsigned char colorset,
		 unsigned char trans);

  void add_image(const image_meta_t meta);

  void add_image(int image, unsigned char set, 
		 signed char xoff, signed char yoff, 
		 unsigned char colorset,
		 unsigned char trans);

  void rem_image(int image, int tileset);

  void clear();

  void set_transparent(bool transparent);

  void set_opaque(bool yesno);


  /**
   * Can we look through this visual?
   * @author Hj. Malthaner
   */
  bool is_opaque() const {return multiset & 1;};


  /**
   * Forget (=don't display) this, if out of sight? 
   * @author Hj. Malthaner
   */
  void set_transient(bool yesno);


  bool is_transient() const {return multiset & 2;};


  /**
   * Get global x-offset.
   * @author Hansj�rg Malthaner 
   */
  int get_x_off() const {return x_off;};


  /**
   * Get global y-offset.
   * @author Hansj�rg Malthaner 
   */
  int get_y_off() const {return y_off;};


  /**
   * Global x-offset. Each image can have an additional x-offset
   * @author Hansj�rg Malthaner 
   */
  void set_x_off(int off) {x_off = off;};


  /**
   * Global y-offset. Each image can have an additional x-offset
   * @author Hansj�rg Malthaner 
   */
  void set_y_off(int off) {y_off = off;};


  /**
   * Basic constructor
   * @author Hansj�rg Malthaner 
   */
  visual_t();


  /**
   * Paints this visual at xpos, ypos onto graphics gr.
   * @author Hansj�rg Malthaner 
   */
  void paint(graphics_t *gr, int xpos, int ypos) const;


  /**
   * Prints all values to stdout - debugging only
   */
  void dump();


  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  void read_write(storage_t *store, iofile_t * file);
};

#endif //VISUAL_T_H
