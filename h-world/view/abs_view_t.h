/* 
 * abs_view_t.h
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef abs_view_t_h
#define abs_view_t_h

#include "primitives/koord.h"


class visual_t;
class ground_visual_t;


/**
 * Base interface for all view classes
 * @author Hansj�rg Malthaner
 */
class abs_view_t
{

 public:

  /**
   * Calculates world coordinates from screen coordinates
   * @author Hansj�rg Malthaner
   */
  koord screen_to_world(koord mpos) const;


  /**
   * Set gridded display. Some subclasses may ignore this.
   * @return former grid state
   * @author Hansj�rg Malthaner
   */
  virtual bool use_grid(bool yesno) = 0;


  /**
   * Callback for level updates
   */
  virtual void update_ground(koord k, ground_visual_t *data) = 0;


  /**
   * Callback for level updates
   */
  virtual void clear_visuals(koord k) = 0;


  /**
   * Callback for level updates
   */
  virtual void add_feature_visual(koord k, const visual_t * visual, 
				  unsigned int flags) = 0;


  /**
   * Callback for level updates
   */
  virtual void add_visual(koord k, const visual_t * visual) = 0;


  /**
   * Callback for level updates
   */
  virtual void set_dark(koord k) = 0;


};

#endif // abs_view_t_h
