/* 
 * player_visual_connector_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef PLAYER_VISUAL_CONNECTOR_T_H
#define PLAYER_VISUAL_CONNECTOR_T_H

#ifndef LIMB_VISITOR_T_H
#include "ifc/limb_visitor_t.h"
#endif

#ifndef THING_LISTENER_T_H
#include "ifc/thing_listener_t.h"
#endif

#ifndef image_meta_t_h
#include "image_meta_t.h"
#endif

class visual_t;
class properties_t;

/**
 * This class is responsible to update a being visual according
 * to the beings items
 *
 * @author Hj. Malthaner
 */
class player_visual_connector_t : public limb_visitor_t, public thing_listener_t
{
 public:

  static void calc_overlays(const handle_tpl <thing_t> & thing);


  /**
   * Loads all required images for a thing.
   *
   * @param thing the thing to prepare (must not be NULL)
   * @param props the properties to read the values from
   *
   * @author Hj. Malthaner
   */
  static void prepare_images(thing_t *thing, const properties_t *props);


 private:

  visual_t & visual;
  const handle_tpl <thing_t> & player;

  /**
   * item type priorities
   * @author Hj. Malthaner
   */
  enum pri {shirt=20, 
	    trousers=24, 
	    jacket=28, 
	    belt=32, 
	    shoes=36, 
	    gloves=40, 
	    helmet=44, 
	    shield=48, 
	    weapon=52, 
	    max_pri=80
  };


  /**
   * During inspection, we must build a prioritized list of images
   * @author Hj. Malthaner
   */
  image_meta_t images[max_pri];


  const char * overlay_set;


  /**
   * Sex options, should be one of 'f', 'm' or 'n'
   * @author Hj. Malthaner
   */
  char sex;


  void calc_overlay_images(const handle_tpl <thing_t> &limb,
			   const handle_tpl <thing_t> &thing);

 public:


  player_visual_connector_t(const handle_tpl <thing_t> &pl, visual_t &v);
  virtual ~player_visual_connector_t();

  /**
   * Implements thing_visitor_t
   * @author Hj. Malthaner
   */
  virtual void visit_limb(const handle_tpl <thing_t> &);
  

  /**
   * Implements thing_listener_t
   * @author Hj. Malthaner
   */
  virtual void update(const handle_tpl <thing_t> &thing);
  
};


#endif
