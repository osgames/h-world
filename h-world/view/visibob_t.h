#ifndef visibob_t_h
#define visibob_t_h


#include "swt/gui_component_t.h"

class tile_descriptor_t;

/**
 * Visible object
 * @author Hj. Malthaner
 */
class visibob_t : public gui_component_t
{
 private:

  const tile_descriptor_t *bob;

 public:

  visibob_t(const tile_descriptor_t *bob);

  
  /**
     * Draws the component
     * @author Hj. Malthaner
     */
  virtual void draw(graphics_t *gr, koord pos);
  
};

#endif // visibob_t_h
