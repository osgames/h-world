#ifndef world_view_ifc_t_h
#define world_view_ifc_t_h


#ifndef koord_h
#include "primitives/koord.h"
#endif


class abs_view_t;

/**
 * The world view uses this interface to read view data from the model
 * @author Hj. Malthaner
 */
class world_view_ifc_t
{
 public:


  /**
   * Updates the display data
   * @author Hj. Malthaner
   */
  virtual void recalculate_view(koord ij_off, int dpy_width, int dpy_height,
				abs_view_t *abs_view) = 0;
  

  /**
   * Updates view data at location i, j 
   * @author Hj. Malthaner
   */
  virtual void display_update(const int i, const int j,
			      abs_view_t *abs_view,
			      bool force_update) = 0;

};

#endif // world_view_ifc_t_h
