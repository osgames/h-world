/* 
 * world_2d_view_t.cpp
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "world_view_ifc_t.h"
#include "world_2d_view_t.h"
#include "ground_visual_t.h"
#include "glasspane_t.h"

#include "model/feature_t.h"

#include "util/debug_t.h"
#include "util/nsow.h"
#include "util/properties_t.h"

#include "swt/font_t.h"
#include "swt/color_t.h"
#include "swt/graphics_t.h"
#include "swt/tileset_t.h"
#include "swt/tile_descriptor_t.h"


/**
 * Calculates world coordinates from screen coordinates
 * @author Hansj�rg Malthaner
 */
koord world_2d_view_t::screen_to_world(koord mpos) const
{
  const int width = get_size().x;
  const int height = get_size().y;

  const int dpy_width  = (width+tile_width-1) / tile_width;
  const int dpy_height = (height+tile_height-1) / tile_height;

  const int i_off = ij_off.x - dpy_width/2;
  const int j_off = ij_off.y - dpy_height/2;

  mpos -= get_pos();

  const int x = mpos.x / tile_width;
  const int y = mpos.y / tile_height;
      
  const int i = x + i_off;
  const int j = y + j_off;

  // printf("%d, %d\n", i, j);

  return koord(i,j);
}



world_2d_view_t::world_2d_view_t(const properties_t * game_props) : world_view_t()
{
  directions = directions_2d;

  tile_width = game_props->get_int("tile_width", 64);
  tile_height = game_props->get_int("tile_height", 32);
}


world_2d_view_t::~world_2d_view_t()
{
  
}


void world_2d_view_t::draw(graphics_t * gr, koord pos)
{
  const int width = get_size().x;
  const int height = get_size().y;

  const int dpy_width  = (width+tile_width-1) / tile_width;
  const int dpy_height = (height+tile_height-1) / tile_height;

  const clip_dimension_t & clip = gr->get_clip();


  const int i_off = ij_off.x - dpy_width/2;
  const int j_off = ij_off.y - dpy_height/2;


  if(dirty && clip.w == get_size().x && clip.h == get_size().y) {
    // get update from server
    world_view_ifc->recalculate_view(ij_off, 
				     dpy_width, dpy_height,
				     this);
    dirty = false;
  }


  // clear screen

  gr->fillbox_wh(pos.x, pos.y, width, height, color_t::BLACK);
  
  // start displaying


  pos.x -= 64 - tile_width / 2;
  pos.y -= 96 - tile_height / 2;


  for (int y = 0; y < dpy_height; y++) {
    const int ypos = pos.y + y * tile_height;
    
    for (int x = 0; x < dpy_width; x++) {
      
      const int i = x + i_off;
      const int j = y + j_off;
      const int xpos = pos.x + x * tile_width;
      
      display_ground(gr, i, j, xpos, ypos);
    }
  }
  
  // dann die Objekte Zeichnen
  
  for (int y = 0; y < dpy_height; y++) {
    const int ypos = pos.y + y * tile_height;
    
    for (int x = 0; x < dpy_width; x++) {
      
      const int i = x + i_off;
      const int j = y + j_off;
      const int xpos = pos.x + x * tile_width;
      
      display_things(gr, i, j, xpos, ypos);
    }
  }


  glasspane->draw(gr, pos);


  koord off = get_pos() + get_size() - koord(100, 34);

  if(level_offset.x != 0 || level_offset.y != 0) {
    char buf[256];
    sprintf(buf, "Offset %d,%d", level_offset.x, level_offset.y);

    font->draw_string(gr, buf, off.x, off.y, 0, color_t::WHITE);
  }

  off.y += 14;

  font->draw_string(gr, level_title, off.x - 100, off.y, 0, color_t::WHITE);
}


/**
 * Grund von i,j an Bildschirmkoordinate xpos,ypos zeichnen.
 * @author Hj. Malthaner
 */
void world_2d_view_t::display_ground(graphics_t * gr, int i, int j, int xpos, int ypos)
{
  if(i >= 0 && j >= 0 && i < level_size.x && j < level_size.y) {
    const int idx = i + j*level_size.x;
    ground_visual_t *ground = grounds+idx;

    const bool is_tiled = (ground->get_image(0) & 0x8000);
    const int  tex = (ground->get_image(0) & 0x7FFF);


    if(is_tiled) {

      const tileset_t *tiles = world_view_t::get_instance()->get_tileset(0);

      gr->draw_tile_shaded(tiles->get_tile(tex),
			   xpos,
			   ypos,
			   ground->shade[0]
			   );

    } else if(images[tex]) {

      static koord points[3];
      static signed char shades[3];

      xpos += 64 - tile_width / 2;
      ypos += 96 - tile_height / 2;


      points[0] = koord(xpos,
			ypos + ground->height[0]);
      points[1] = koord(xpos,    
			ypos + tile_height + ground->height[1] - gridline);
      points[2] = koord(xpos + tile_width - gridline, 
                        ypos + tile_height + ground->height[2] - gridline);

      shades[0] = ground->shade[0];
      shades[1] = ground->shade[1];
      shades[2] = ground->shade[2];


      if(points[2].y < points[1].y) {
	koord tmp = points[1];
	points[1] = points[2];
	points[2] = tmp;

	shades[2] = ground->shade[1];
	shades[1] = ground->shade[2];
      }

      gr->draw_triangle(points, shades, x_off, y_off, images[tex]);

      // ---------

      points[1] = koord(xpos + tile_width - gridline + 1, 
			ypos + ground->height[3] - gridline);

      points[2] = koord(xpos + tile_width - gridline, 
                        ypos + tile_height + ground->height[2] - gridline);

      shades[1] = ground->shade[3];
      shades[2] = ground->shade[2];

      if(points[1].y < points[0].y) {
	koord tmp = points[0];
	points[0] = points[1];
	points[1] = tmp;

	shades[0] = ground->shade[3];
	shades[1] = ground->shade[0];
      }

      gr->draw_triangle(points, shades, x_off, y_off, images[tex]);

    }
  } else {
    // there was nothing to paint there, so we use the default look
    outside.paint(gr, xpos, ypos);
  }
}


/**
 * Dinge von i,j an Bildschirmkoordinate xpos,ypos zeichnen.
 * @author Hj. Malthaner
 */
void world_2d_view_t::display_things(graphics_t *gr, int i, int j, int xpos, int ypos)
{
  if(i >= 0 && j >= 0 && i < level_size.x && j < level_size.y) {
    const int idx = i + j*level_size.x;
    minivec_tpl <visual_t> & look = viewdata[idx];
    ground_visual_t *ground = grounds+idx;

    const int yoff = (ground->height[0] + ground->height[1] + ground->height[2] + ground->height[3]) >> 2; 

    int start = 0;

    if(look.count() && flags[idx] & feature_t::F_NINEPARTWALL) {
      const int conn_flags = 
	feature_t::F_NINEPARTWALL | feature_t::F_DOOR | feature_t::F_DOORPOST;
      unsigned int nbr [8];
      const koord  ij (i, j); 

      for(int n=0; n<8; n++) { 
	koord pos = ij + neighbour_8[n]; 

	if(pos.x >= 0 && pos.x < level_size.x &&
	   pos.y >= 0 && pos.y < level_size.y &&
	   remembered[pos.x + pos.y*level_size.x]) {

	  nbr[n] = flags[pos.x + pos.y*level_size.x];
	} else {
	  // nbr[n] = feature_t::F_NINEPARTWALL; 
	  nbr[n] = feature_t::F_OTHER;
	}
      }

      visual_t visual = look.get(0);


      const int tw2 = tile_width / 6;
      const int th2 = tile_height / 6;

      const int TW2 = tile_width / 2 - 24;
      const int TH2 = tile_height / 2;




      // top top
      if(nbr[0] & conn_flags &&
	 nbr[6] & conn_flags &&
	 nbr[7] & conn_flags) {
	visual.set_x_off(TW2 - 3*tw2);
	visual.set_y_off(0);
	visual.paint(gr, xpos, ypos+yoff);
      }



      // top
      if(nbr[0] & conn_flags) {
	visual.set_x_off(TW2 - tw2);
	visual.set_y_off(0);
	visual.paint(gr, xpos, ypos+yoff);
      }


      // left
      if(nbr[6] & conn_flags) {
	visual.set_x_off(TW2 - 3*tw2);
	visual.set_y_off(TH2 - th2);
	visual.paint(gr, xpos, ypos+yoff);
      }


      // left left
      if(nbr[4] & conn_flags &&
	 nbr[5] & conn_flags &&
	 nbr[6] & conn_flags) {
	visual.set_x_off(TW2 - 3*tw2);
	visual.set_y_off(TH2 + th2);
	visual.paint(gr, xpos, ypos+yoff);
      }


      // center
      visual.set_x_off(TW2 - tw2);
      visual.set_y_off(TH2 - th2);
      visual.paint(gr, xpos, ypos+yoff);


      // right right
      if(nbr[0] & conn_flags &&
	 nbr[1] & conn_flags &&
	 nbr[2] & conn_flags) {
	visual.set_x_off(TW2 + tw2);
	visual.set_y_off(0);
	visual.paint(gr, xpos, ypos+yoff);
      }


      // bottom
      if(nbr[4] & conn_flags) {
	visual.set_x_off(TW2 - tw2);
	visual.set_y_off(TH2 + th2);
	visual.paint(gr, xpos, ypos+yoff);
      }



      // right
      if(nbr[2] & conn_flags) {
	visual.set_x_off(TW2 + tw2);
	visual.set_y_off(TH2 - th2);
	visual.paint(gr, xpos, ypos+yoff);
      }



      // bot bot
      if(nbr[2] & conn_flags &&
	 nbr[3] & conn_flags &&
	 nbr[4] & conn_flags) {
	visual.set_x_off(TW2 + tw2);
	visual.set_y_off(TH2 + th2);
	visual.paint(gr, xpos, ypos+yoff);
      }


      start = 1;
    }

    if(cursor_ij.x == i && cursor_ij.y == j) {
      // Hajo: draw with highlite

      for(int i=start; i<look.count(); i++) { 
	visual_t & v = look.at(i);
	int light = v.get_shade();
	v.set_shade(light < 24 ? light + 8 : 32); 
	v.paint(gr, xpos, ypos+yoff);
	v.set_shade(light);
      }

    } else {
      for(int i=start; i<look.count(); i++) { 
	look.get(i).paint(gr, xpos, ypos+yoff);
      }
    }
  }
}
