#include "visibob_t.h"
#include "swt/tile_descriptor_t.h"
#include "swt/graphics_t.h"



visibob_t::visibob_t(const tile_descriptor_t *bob)
{
  this->bob = bob;
}


/**
 * Draws the component
 * @author Hj. Malthaner
 */
void visibob_t::draw(graphics_t *gr, koord pos)
{
  const koord screenpos = get_pos() + pos;

  // printf("visibob_t::draw(): at %d,%d\n", screenpos.x, screenpos.y);

  gr->draw_tile(bob, screenpos.x,  screenpos.y);
}
