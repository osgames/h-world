/* 
 * world_3d_view_t.cpp
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include <math.h>
#include <stdio.h>

#include "world_3d_view_t.h"
#include "model/ground_t.h"

#include "swt/color_t.h"
#include "swt/image_t.h"
#include "swt/graphics_t.h"
#include "swt/system_window_t.h"


#include "primitives/bitmap_t.h"


#define TEX_BITSIZE 7
#define TEX_SIZE    (1<<TEX_BITSIZE)

#define PERSPEKTIVE 80000

#define CALC_VZ_DIST(x1,y1,x2,y2) ((int)((x1)*(x2) + (y1)*(y2)))
#define TEX_SPALTE(x)             ((x) & ((TEX_SIZE) - 1))


struct WandInfo {
  int xpos;
  int ypos;
  int dist;
  int tex_spalte;
  int typ;
};



static int hoehen [2048];


static image_t * render_buffer;


static bool is_blocking(minivec_tpl <visual_t> & look)
{
  minivec_iterator_tpl <visual_t> iter (look);

  while( iter.next() ) {
    if(iter.get_current().is_opaque()) {
      return true;
    }
  }
  
  return false;
}


void world_3d_view_t::draw_boden(graphics_t *gr, 
				 double links,
				 double rechts)
{        
  const double col = cos(links); 
  const double cor = cos(rechts); 

  const double sil = sin(links); 
  const double sir = sin(rechts); 

  const int sxpos = ij_off.x * TEX_SIZE + TEX_SIZE/2;
  const int sypos = ij_off.y * TEX_SIZE + TEX_SIZE/2;

  unsigned short * boden = gr->get_data() + 140; 

  const int width  = get_size().x;
  const int height = get_size().y;

  int hoehe;


  hoehe= 12;
  boden += (gr->get_height() >> 1) * gr->get_width();    

  //decke += (gr->get_height() - 1 - hoehe -(gr->get_height() >> 1)) * gr->get_width();    

  while(hoehe < height/2) {
    int xpos,ypos;
    int i;
    int dx,dy;
    /*
      xpos = dx_links_rechts[links][hoehe];
      ypos = dy_links_rechts[links][hoehe];
    */
    xpos = (col * (PERSPEKTIVE/hoehe) * (1<<13));
    ypos = (sil * (PERSPEKTIVE/hoehe) * (1<<13));

    /*    
      dx=(dx_links_rechts[rechts][hoehe] - xpos) / gr->get_width();
      dy=(dy_links_rechts[rechts][hoehe] - ypos) / gr->get_width();       
    */
    dx=((cor * (PERSPEKTIVE/hoehe) * (1<<13)) - xpos) / width;
    dy=((sir * (PERSPEKTIVE/hoehe) * (1<<13)) - ypos) / width;

    xpos += (sxpos << 14);
    ypos += (sypos << 14);
    
    for(i=0; i<width; i+=1) {
      if(hoehe > hoehen[i]) {
	const int rx = xpos >> (TEX_BITSIZE + 14);
	const int ry = ypos >> (TEX_BITSIZE + 14);

	if(rx>=0 && ry>=0 && rx<level_size.x && ry<level_size.y) {
	
	  const int idx = rx + ry*level_size.x;		
	  ground_visual_t * ground = &grounds[idx];
	  const int  tex = ground->get_image(0) & 0x7FFF;

	  if(images[tex]) {
	    boden[i] = images[tex]->get_data() [((xpos >> 14) & 0x3F) + ((ypos>>14) & 0x3F)*64 ];
	  }
	}
      }
      xpos += dx;
      ypos += dy;
    }
    
    boden += gr->get_width();
    hoehe++;
  }
}


int world_3d_view_t::hscan(int *lx, int *ly,const double winkel)
{
  const double co = cos(winkel);
  const double si = sin(winkel);

  if(si == 0) {
    return -1;
  } else {
    const double dx = (co * TEX_SIZE) / (fabs(si));
    const int dy = si < 0.0 ? -TEX_SIZE : TEX_SIZE;
    double ix;
    int iy;

    // printf("hscan: dx=%f, dy=%d\n", dx, dy);
  
    iy = *ly;
                                                 
    if( TEX_SPALTE(iy) != 0) {
      const int zy = ((iy >> TEX_BITSIZE) + (si > 0.0)) << TEX_BITSIZE;
	
      ix = (*lx + (zy - iy) * co/si);
      iy = zy;
    } else {
      ix = *lx;
    }

    if( dy < 0 ) {
      iy -= (TEX_SIZE);
    }

    bool ok = true;
    int rx,ry;

    do {
      rx = (int)ix >> (TEX_BITSIZE);
      ry = iy >> (TEX_BITSIZE);
 
      ok = (rx>=0 && ry>=0 && rx<level_size.x && ry<level_size.y);

      if(ok) {

	displayed->set_bit(rx,ry);

	const int idx = rx + ry*level_size.x;
	minivec_tpl <visual_t> & look = viewdata[idx];

	if(is_blocking(look)) {
	  *lx = (int)ix;

	  if(dy < 0) {
	    *ly = iy + TEX_SIZE;
	  } else {
	    *ly = iy;
	  }

	  // printf("hscan: hit at rx=%d, ry=%d idx=%d count=%d\n", rx, ry, idx, look.count());
	  return 0;
	}

	ix += dx;
	iy += dy; 
      }
    } while(ok);
  }

  return -1;
}


int world_3d_view_t::vscan(int *lx, int *ly, const double winkel)
{
  const double co = cos(winkel);
  const double si = sin(winkel);

  if(co == 0) {
    return -1;
  } else {
    const int dx = co < 0.0 ? -TEX_SIZE : TEX_SIZE;
    const double dy = (si * TEX_SIZE) / (fabs(co));
    int ix;
    double iy;

    // printf("vscan: dx=%d, dy=%f\n", dx, dy);


    ix = *lx;

    if( TEX_SPALTE(ix) != 0) {
      const int zx = ((ix >> TEX_BITSIZE) + (co > 0.0)) << TEX_BITSIZE;
      
      iy = (*ly + ((zx -ix) * si/co));
      ix = zx;
    } else {                                
      iy = *ly;
    }
    
    if( dx < 0 ) {
      ix -= (TEX_SIZE);
    }

    bool ok = true;
    int rx,ry;

    do {
      rx = ix >> (TEX_BITSIZE);
      ry = (int)iy >> (TEX_BITSIZE);
      
      ok = (rx>=0 && ry>=0 && rx<level_size.x && ry<level_size.y);

      if(ok) {

	displayed->set_bit(rx,ry);
      
	const int idx = rx + ry*level_size.x;
	minivec_tpl <visual_t> & look = viewdata[idx];

	// printf("vscan: rx=%d, ry=%d idx=%d count=%d\n", rx, ry, idx, look.count());
      
	
	if(is_blocking(look)) {
	  if(dx < 0) {
	    *lx = ix + TEX_SIZE;
	  } else {
	    *lx = ix;
	  }
	  *ly = (int)iy;

	  // printf("vscan: hit at rx=%d, ry=%d idx=%d count=%d\n", rx, ry, idx, look.count());
	  return 0;
	}
	
	ix += dx; 
	iy += dy;	   
      } 
      
    } while(ok);
  }

  return -1;
}


bool world_3d_view_t::cast_one_ray(const double winkel, 
				   const double blick,
				   struct WandInfo *treffer)
{
  const double cob = cos(blick);
  const double sib = sin(blick);

  int htyp, vtyp;
  int hx, hy, vx, vy;
  int hdist, vdist;

  const int sxpos = ij_off.x * TEX_SIZE + TEX_SIZE/2;
  const int sypos = ij_off.y * TEX_SIZE + TEX_SIZE/2;

  hx = vx = sxpos;
  hy = vy = sypos;
    
  htyp = hscan(&hx, &hy, winkel);

  vtyp = vscan(&vx, &vy, winkel);

  if( htyp == -1 ) {
    if(vtyp == -1) {
      return false;                   /* Keine Wand gesehen */
    } else {

      treffer->xpos = vx;
      treffer->ypos = vy;
      treffer->dist = CALC_VZ_DIST(vx - sxpos, vy - sypos,
				   cob, sib);
      treffer->tex_spalte = TEX_SPALTE(vy);
      treffer->typ = vtyp;
      return true;
    }
  } else {
    if( vtyp==-1 ) {

      treffer->xpos = hx;
      treffer->ypos = hy;
      treffer->dist = CALC_VZ_DIST(hx - sxpos, hy - sypos,
				   cob, sib);

      treffer->tex_spalte = TEX_SPALTE(hx);
      treffer->typ = htyp;
      return true;
    } else {
      
      hdist = CALC_VZ_DIST(hx - sxpos, hy - sypos,
			   cob, sib);

      vdist = CALC_VZ_DIST(vx - sxpos, vy - sypos,
			   cob, sib);
      

      if(hdist <= vdist) {
	treffer->xpos = hx;
	treffer->ypos = hy;   
	treffer->dist = hdist;
	treffer->tex_spalte = TEX_SPALTE(hx);
	treffer->typ = htyp;
	return true;
      } else {
	treffer->xpos = vx;
	treffer->ypos = vy;
	treffer->dist = vdist;
	treffer->tex_spalte = TEX_SPALTE(vy);
	treffer->typ = vtyp;
	return true;
      }                                               
    }
  }
               
  // puts("Fehler in cast_one_ray : should never be here !");
  return false;
}


world_3d_view_t::world_3d_view_t() : world_view_t()
{
  displayed = new bitmap_t(1,1);


  const enum colortype ct = 
    system_window_t::get_instance()->get_graphics()->get_colortype();

  render_buffer = new image_t(128, 128, ct);
}


world_3d_view_t::~world_3d_view_t()
{
  delete displayed;
}


/**
 * Clears the client level data, and prepares for a new level
 * @author Hj. Malthaner
 */
void world_3d_view_t::prepare_level(koord size, 
				    const char * level_title,
				    koord level_offset)
{
  delete displayed;
  displayed = new bitmap_t(size.x, size.y);

  world_view_t::prepare_level(size, level_title, level_offset);
}


void world_3d_view_t::draw(graphics_t * gr, koord pos)
{
  world_view_t::draw(gr, pos);


  const int width = get_size().x;
  const int height =get_size().y;
 
  // clear screen
  // gr->fillbox_wh(pos.x, pos.y, width, height, color_t::BLACK);


  struct WandInfo treffer;
  const double blick = 0;

  const int view_angle = 340;

  displayed->clr_map();

  for(int x=0; x<width; x++) { 
    const double winkel = (blick + atan2((x-width/2), view_angle));

    const bool ok = cast_one_ray(winkel, blick, &treffer);
    
    if(ok) {
      const int wall_height = PERSPEKTIVE/(treffer.dist+1);

      gr->fillbox_wh(pos.x + x, pos.y + height/2 - wall_height/2,
		     1, wall_height,
		     color_t::GREY160);

      hoehen[x] = wall_height >> 1;
    } else {
      hoehen[x] = 2;
    }

    // printf("ok=%d, x=%03d, winkel=%f, dist=%d\n", ok, x, winkel, treffer.dist);

  }

  double links  = (blick + atan2((0-width/2), view_angle));
  double rechts = (blick + atan2((width-1-width/2), view_angle));

  draw_boden(gr, links, rechts);



  // Objekte

  const int sxpos = ij_off.x * TEX_SIZE + TEX_SIZE/2;
  const int sypos = ij_off.y * TEX_SIZE + TEX_SIZE/2;

  const double cob = cos(blick);
  const double sib = sin(blick);

  for(int j=0; j<level_size.y; j++) { 
    for(int i=0; i<level_size.x ; i++) { 

      if(displayed->get_bit(i,j)) {
	const int idx = i + j*level_size.x;
	minivec_tpl <visual_t> & look = viewdata[idx];

	if(look.count() > 0) {
	  
	  const int oxpos = i * TEX_SIZE + TEX_SIZE/2;
	  const int oypos = j * TEX_SIZE + TEX_SIZE/2;


	  const int odist = CALC_VZ_DIST(oxpos - sxpos, oypos - sypos,
				       cob, sib);

	
	  printf("object at %d,%d at distance %d\n", i, j, odist);
	}
      }
    }
  }
  

  // printf("ij_x=%d, ij_y=%d\n", ij_off.x, ij_off.y);
}
