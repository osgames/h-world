/* 
 * visual_t.cpp
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "visual_t.h"
#include "world_view_t.h"
#include "swt/graphics_t.h"
#include "swt/tileset_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"

#include "util/debug_t.h"
#include "util/rng_t.h"

#include "primitives/cstring_t.h"


static int colorshades[256];


void visual_t::read_colorshades(const cstring_t & base_dir)
{
  FILE *file = fopen(base_dir + "/graph/colorshades.pal", "r");

  if(file) {
    int R,G,B;
    fscanf(file, "%d\n", &R); // dummy only

    for(int i=0; i<256; i++) {
      fscanf(file, "%d %d %d\n", &R, &G, &B); // dummy only

      colorshades[i] = /* (R << 24) + (G << 16) + B; */
	  ((R & 0xF8) << 7) +
	  ((G & 0xF8) << 2) +
	  ((B & 0xF8) >> 3);

      // dbg->message("read_colorshades()", "%d %d %d", R, G, B);
    }
  } else {
    dbg->fatal("read_colorshades()", "cannot open file");
  }
}

const int * visual_t::get_colorshades()
{
  return colorshades;
}


/**
 * Calculate random x and y offsets
 * @author Hansj�rg Malthaner 
 */
void visual_t::calc_jitter(const int jitter)
{
  const int i = rng_t::get_the_rng().get_int(2 * jitter + 1) - jitter;
  const int j = rng_t::get_the_rng().get_int(2 * jitter + 1) - jitter;

  set_x_off((i-j));
  set_y_off((i+j) >> 1);
}


void visual_t::set_frames(int f) 
{
  frames &= ~31;
  frames |= f;
}


void visual_t::set_ftime(int ftime)
{
  frames &= 31;
  frames |= ftime << 5;
}


void visual_t::set_current(int c) 
{
  current &= ~31;
  current |= c;
};


void visual_t::set_ptime(int ptime)
{
  current &= 31;
  current |= ptime << 5;
}


void visual_t::set_image(int index, const image_meta_t meta)
{
  if(images.count() <= index) {
    images.append(meta);
  } else {
    images.at(index) = meta;
  }

  set_current(0);
}


void visual_t::set_image(int index, int image, unsigned char set, 
			 signed char xoff, signed char yoff,
			 unsigned char colorset,
			 unsigned char trans)
{
  image_meta_t meta;

  meta.img = image;
  meta.xoff = xoff;
  meta.yoff = yoff;
  meta.tileset = set;
  meta.colorset = colorset;
  meta.trans = trans;

  set_image(index, meta);
}


void visual_t::add_image(const image_meta_t meta)
{
  images.append(meta);
}


void visual_t::add_image(int image, unsigned char set, 
			 signed char xoff, signed char yoff,
			 unsigned char colorset,
			 unsigned char trans)
{
  image_meta_t meta;

  meta.img = image;
  meta.xoff = xoff;
  meta.yoff = yoff;
  meta.tileset = set;
  meta.colorset = colorset;
  meta.trans = trans;

  images.append(meta);
}


void visual_t::rem_image(int image, int tileset)
{
  minivec_iterator_tpl <image_meta_t> iter (images);

  while( iter.next() ) {
    if((int)(iter.get_current().img) == image &&
       (int)(iter.get_current().tileset) == tileset) {
      images.remove(iter.get_current());
      break;
    }
  }
}


void visual_t::clear()
{
  while(images.count()) {
    images.remove(images.get(0));
  }  
}


void visual_t::set_transparent(bool transparent)
{
  multiset &= 0x7F;
  multiset |= transparent << 7;
}


void visual_t::set_opaque(bool yesno)
{
  multiset &= 0xFE;
  multiset |= yesno;
}


/**
 * Forget (=don't display) this, if out of sight? 
 * @author Hj. Malthaner
 */
void visual_t::set_transient(bool yesno)
{
  multiset &= ~2;
  multiset |= yesno << 1;
}



/**
 * Basic constructor
 * @author Hansj�rg Malthaner 
 */
visual_t::visual_t() : images(1)
{
  x_off = 0;
  y_off = 0;
  multiset = 0;

  // 32 means no shading
  shade = 32;

  frames = 1;
  current = 0;
}


/**
 * Paints this visual at xpos, ypos onto graphics gr.
 * @author Hansj�rg Malthaner 
 */
void visual_t::paint(graphics_t *gr, int xpos, int ypos) const {
  const world_view_t *view = world_view_t::get_instance();

  for(int i=0; i<images.count(); i++) {
    
    // animate base image only
    const image_meta_t meta = images.get(i);

    const unsigned int img = meta.img + ((i > 0) ? 0 : get_current()); 
    const tileset_t *tiles = view->get_tileset(meta.tileset);
    const int x = xpos + x_off + meta.xoff;
    const int y = ypos + y_off + meta.yoff;
    const int colorset = meta.colorset;

    // transparent ?
    if((multiset & 0x80) || meta.trans) {
      if(colorset == 0) {

	gr->draw_tile_transparent(tiles->get_tile(img),
				  x,
				  y,
				  shade
				  );
      } else {

	// printf("colorset=%d\n", colorset);
	
	gr->draw_tile_transparent_and_recolored(tiles->get_tile(img),
						x,
						y,
						shade,
						colorshades + colorset*8);
	
      }

    } else {

      // dbg->message("visual_t::paint()", "img=%d, colorset=%d", img, colorset);

      if(colorset == 0) {

	gr->draw_tile_shaded(tiles->get_tile(img),
			     x,
			     y,
			     shade
			     );
      } else {

	gr->draw_tile_recolored(tiles->get_tile(img),
				x,
				y,
				shade,
				colorshades + colorset*8);
      }
    }
  }
}



void visual_t::inc_frame()
{
  const unsigned char next = get_current() + 1;

  if(next >= get_frames()) {
    set_current(0);
  } else {
    set_current(next);
  }

  /*
  printf("Frames: %d/%d, ftime=%d, ptime=%d\n", 
	 get_current(), get_frames(),
	 get_ftime(), get_ptime());
  */
}


/**
 * Prints all values to stdout - debugging only
 */
void visual_t::dump()
{
  dbg->message("visual_t::dump()", "image count = %d", images.count());

  for(int i=0; i<images.count() ; i++) { 
    dbg->message("visual_t::dump()", "  image[%d] = %d", i, images.at(i));
  }

  dbg->message("visual_t::dump()", "xoff=%d yoff=%d", x_off, y_off);
  dbg->message("visual_t::dump()", "shade=%d multiset=%X", 
	       shade, multiset);
}


// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void visual_t::read_write(storage_t *store, iofile_t * file)
{
  if(file->is_saving()) {
    signed char n = images.count();
    file->rdwr_char(n);

    for(int i=0; i<n; i++) {
      image_meta_t meta = images.at(i);

      unsigned int img    = meta.img;
      signed char xoff    = meta.xoff;
      signed char yoff    = meta.yoff;
      unsigned char tileset  = meta.tileset;
      unsigned char colorset = meta.colorset;
      unsigned char trans    = meta.trans;
      
      file->rdwr_uint(img);
      file->rdwr_char(xoff);
      file->rdwr_char(yoff);
      file->rdwr_uchar(tileset);
      file->rdwr_uchar(colorset);
      file->rdwr_uchar(trans);
    }

  } else {
    signed char n;
    file->rdwr_char(n);
    images.clear();

    for(int i=0; i<n; i++) {
      unsigned int img;
      signed char xoff;
      signed char yoff;
      unsigned char tileset;
      unsigned char colorset;
      unsigned char trans;
      
      file->rdwr_uint(img);
      file->rdwr_char(xoff);
      file->rdwr_char(yoff);
      file->rdwr_uchar(tileset);
      file->rdwr_uchar(colorset);
      file->rdwr_uchar(trans);

      // printf("image=%d colorset=%d\n", img, colorset);

      image_meta_t meta;

      meta.img = img;
      meta.xoff = xoff;
      meta.yoff = yoff;
      meta.tileset = tileset;
      meta.colorset = colorset;
      meta.trans = trans;


      // printf("image=%d colorset=%d\n", meta.img, meta.colorset);

      images.append(meta);

    }
  }
 
  file->rdwr_char(x_off);
  file->rdwr_char(y_off);
  file->rdwr_char(shade);
  file->rdwr_uchar(multiset);
}

