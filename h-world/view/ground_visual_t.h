/* 
 * ground_visual_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef ground_visual_t_h
#define ground_visual_t_h


#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif


/**
 * A class that determines the look of a ground sqaure
 *
 * @author Hj. Malthaner
 */
class ground_visual_t : public persistent_t
{
 public:
  
  /**
   * Vertex shades
   * @author Hj. Malthaner
   */
  signed char shade[4];


  /**
   * Vertex heights
   * @author Hj. Malthaner
   */
  signed char height[4];


  /**
   * Texture identifier
   * @author Hj. Malthaner
   */
  unsigned short tex;


  unsigned short get_image(int i) const { return tex;};
  void set_image(unsigned short i) {tex = i;};


  ground_visual_t();
  virtual ~ground_visual_t() {};


  // ---- implements persistent_t ----

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);
  

  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};

#endif // ground_visual_t_h
