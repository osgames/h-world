#ifndef glasspane_t_h
#define glasspane_t_h


#include "swt/gui_container_t.h"

/**
 * A translucent container - in H-World used to display special effects
 * on/over the world view.
 * @author Hj. Malthaner
 */
class glasspane_t : public gui_container_t
{

};

#endif // glasspane_t_h
