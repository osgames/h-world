#include <string.h>

#include "server_view_t.h"
#include "model/world_t.h"
#include "view/visual_t.h"
#include "view/ground_visual_t.h"

#include "util/debug_t.h"


/**
 * Set gridded display. Some subclasses may ignore this.
 * @return former grid state
 * @author Hansj�rg Malthaner
 */
bool server_view_t::use_grid(bool yesno)
{
  // Hajo: ignored - to be done
  return false;
}


server_view_t::server_view_t()
{
  mark = 0;
}

server_view_t::~server_view_t()
{
  
}


void server_view_t::init()
{
  mark = 0;
}


void server_view_t::redraw()
{
  world_t::get_instance()->recalculate_view(ij_off, 
					    8, 8,
					    this);  
}


/**
 * Callback for level updates
 */
void server_view_t::update_ground(koord k, ground_visual_t *data)
{
  // Store type
  buf[mark] = update;
  mark += 1;


  // Store koord
  memcpy(&buf[mark], &k, sizeof(koord));
  mark += sizeof(koord);

  
  // Store data
  memcpy(&buf[mark], data,sizeof(ground_visual_t));
  mark += sizeof(ground_visual_t);
  

}


/**
 * Callback for level updates
 */
void server_view_t::clear_visuals(koord k)
{
  // Store type
  buf[mark] = clear;
  mark += 1;


  // Store koord
  memcpy(&buf[mark], &k, sizeof(koord));
  mark += sizeof(koord);
  
}


/**
 * Callback for level updates
 */
void server_view_t::add_visual(koord k, const visual_t * visual)
{
   // Store type
  buf[mark] = add;
  mark += 1;


  // Store koord
  memcpy(&buf[mark], &k, sizeof(koord));
  mark += sizeof(koord);

  
  // Store data
  memcpy(&buf[mark], visual, sizeof(visual_t));
  mark += sizeof(visual_t);
   
}


/**
 * Callback for level updates
 */
void server_view_t::add_feature_visual(koord k, const visual_t * visual, 
				       unsigned int /*flags*/)
{
  add_visual(k, visual);
}


/**
 * Callback for level updates
 */
void server_view_t::set_dark(koord k)
{
  // Store type
  buf[mark] = dark;
  mark += 1;


  // Store koord
  memcpy(&buf[mark], &k, sizeof(koord));
  mark += sizeof(koord);

  
}

void server_view_t::dump()
{
  dbg->message("server_view_t::dump", "buffer size = %d", mark);
}
