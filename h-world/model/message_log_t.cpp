/* Copyright by Hj. Malthaner */

#include <string.h>
#include <stdlib.h>
#include "../gui/message_log_view_t.h"
#include "message_log_t.h"

message_log_t * message_log_t::single_instance= 0;


message_log_t * message_log_t::get_instance()
{
  if (single_instance == 0) {
    single_instance = new message_log_t();
  }
  return single_instance;
}


message_log_t::message_log_t()
{
  view = 0;

  for(int i=0; i<LOG_LENGTH; i++) {
    messages[i] = strdup("");
  }
}


const char * message_log_t::get_tail(int n)
{
  if(n >= 0 && n<LOG_LENGTH) {
    return messages[n];
  } else {
    return "";
  }
}


void message_log_t::add_message(const char *message)
{
  if(messages[LOG_LENGTH-1]) {
    free((void*)messages[LOG_LENGTH-1]);
    messages[LOG_LENGTH-1] = NULL;
  }

  for(int i=LOG_LENGTH-1; i>0; i--) {
    messages[i] = messages[i-1]; 
  }

  messages[0] = strdup(message);

  if(view) {
    view->redraw();
  }
}


void message_log_t::clear()
{
  for(int i=LOG_LENGTH-1; i>=0; i--) {
    messages[i][0] = '\0'; 
  }

  if(view) {
    view->redraw();
  }
}
