/* Copyright by Hj. Malthaner */


#include "ground_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"


/**
 * Basic constructor
 */
ground_t::ground_t() {
  type = LAND;
  height = 0;
  visual.set_image(0);
}

// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void ground_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_short(location.x);
  file->rdwr_short(location.y);

  file->rdwr_uchar(height);
  file->rdwr_uchar(type);

  visual.read_write(store, file);
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t ground_t::get_cid()
{
  return hstore_t::t_ground;
}
