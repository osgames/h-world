/* 
 * thing_tree_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "thing_traversor_t.h"
#include "thing_tree_t.h"
#include "thing_t.h"
#include "model/inventory_t.h"
#include "tpl/slist_tpl.h"


static bool matches(const thinghandle_t & thing,
		    const char *ident,
		    const char *type,
		    const char *subtype)
{
  const bool ok = 
    (ident == 0 || strcmp(ident, thing->get_string("ident", "")) == 0) &&
    (type == 0 || strcmp(type, thing->get_string("type", "")) == 0) &&
    (subtype == 0 || strcmp(subtype, thing->get_string("subtype", "")) == 0);

  /*
  dbg->warning("thing_chooser_t::matches()",
	       "ident=%s type=%s subtype=%s ok=%d", 
	       ident ? ident : "<null>", 
	       type ? type : "<null>", 
	       subtype ? subtype : "<null>",
	       ok);    
  */

  return ok;
}



/**
 * Fills the list with handles to all things that have inventories.
 * Things inside inventories are not checked again.
 * 
 * @param list the list to fill
 * @param deep if true also list contents of inventories (non-recursive)
 * @author Hj. Malthaner
 */
void thing_tree_t::list_inventories(thinghandle_t tree,
				    slist_tpl <thinghandle_t> * l,
				    bool deep) 
{
  if(l) {
    thing_traversor_t trav (tree);

    list = l;

    if(deep) {
      operation = op_inventories_deep;
    } else {
      operation = op_inventories;
    }

    trav.set_thing_visitor(this);
    trav.traverse();

    list = 0;

    if(tree->get_inventory()) {
      l->append(tree);
    }
  }
}


/**
 * Fills the list with handles to all things that are held by limbs
 * 
 * @param tree the thing tree to search
 * @param list the list to fill
 * @author Hj. Malthaner
 */
void thing_tree_t::list_items(thinghandle_t tree,
			      slist_tpl <thinghandle_t> * l)
{
  if(l) {
    thing_traversor_t trav (tree);

    list = l;

    operation = op_items;

    trav.set_thing_visitor(this);
    trav.traverse();

    list = 0;
  }
}


/**
 * Cretaes a new list of items from old list and given filter
 * 
 * @author Hj. Malthaner
 */
slist_tpl <thinghandle_t> * 
thing_tree_t::filter_list(slist_tpl <thinghandle_t> * list,
			  const char *ident,
			  const char *type,
			  const char *subtype)
{
  slist_tpl <thinghandle_t> * result = new slist_tpl <thinghandle_t>;


  slist_iterator_tpl <thinghandle_t> iter (list);

  while( iter.next() ) {
    thinghandle_t & thing = iter.access_current();
  
    if(matches(thing, ident, type, subtype)){
      result->append(thing);
    }
  }

  return result;
}



/**
 * Implements thing visitor
 *
 * @author Hj. Malthaner
 */
void thing_tree_t::visit_thing(const handle_tpl <thing_t> &thing) 
{

  switch(operation) {
  case op_inventories:
  case op_inventories_deep:
    if(thing->get_inventory()) {
      list->insert(thing);

      if(operation == op_inventories_deep) {
	thing->get_inventory()->list_things(list);
      }
    }
    break;
  case op_items:
    list->insert(thing);
    break;
  }
}
