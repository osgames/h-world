/* 
 * ground_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef GROUND_T_H
#define GROUND_T_H

#ifndef ground_visual_t_h
#include "view/ground_visual_t.h"
#endif

#ifndef koord_h
#include "primitives/koord.h"
#endif

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

/**
 * Floors and other grounds are represented by objects of this class
 * @author Hansj�rg Malthaner
 */
class ground_t : public persistent_t {
private:


  koord location;

public:

  /**
   * The ground types are used to distinguish the semantics of this ground
   * @author Hansj�rg Malthaner
   */
  enum gtypes {
    LAND, WATER, ROAD 
  };


  /**
   * The visual representation of the ground.
   * @link aggregation
   * @author Hansj�rg Malthaner
   */
  ground_visual_t visual;


  /**
   * Type of this ground. Initialized to land
   * @author Hansj�rg Malthaner
   */
  unsigned char type;


  /**
   * Height of this ground. Initialized to 0
   * @author Hansj�rg Malthaner
   */
  unsigned char height;


  /**
   * Basic constructor
   */
  ground_t();


  // ---- implements persistent_t ----

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);
  

  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};
#endif //GROUND_T_H
