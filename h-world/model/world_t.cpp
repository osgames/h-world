/* 
 * world_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <unistd.h>

#include "world_t.h"
#include "ground_t.h"
#include "thing_t.h"
#include "thinghandle_t.h"
#include "thing_traversor_t.h"
#include "feature_t.h"
#include "square_t.h"
#include "level_t.h"
#include "thing_constants.h"

#include "control/trigger_t.h"
#include "control/user_input_t.h"
#include "control/remote_user_t.h"
#include "control/scheduler_t.h"
#include "control/lua_call_t.h"

#include "view/world_view_t.h"
#include "view/player_visual_connector_t.h"

#include "gui/player_gui_t.h"
#include "factories/level_factory_t.h"
#include "ifc/limb_visitor_t.h"
#include "util/rng_t.h"
#include "util/debug_t.h"
#include "util/searchfolder_t.h"
#include "language/linguist_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"
#include "factories/feature_factory_t.h"
#include "factories/level_factory_t.h"
#include "factories/thing_factory_t.h"

#include "primitives/cstring_t.h"

#ifndef PPROPS_T_H
#include "pprops_t.h"
#endif

static char levelname[256];


static void prepare_level(world_view_t * view, level_t * level)
{
  properties_t *props = level->get_properties();

  const char *title = props->get_string("title");

  const int *xoff = props->get_int("offset.x");
  const int *yoff = props->get_int("offset.y");

  koord offset;

  if(xoff && yoff) {
    offset = koord(*xoff, *yoff);
  }


  sprintf(levelname, 
	  "%s (%s)", 
	  title ? title : "Unnamed level",
	  props->get_string("persistence_file") ? "persistent" : "transient" 
	  );



  view->prepare_level(level->get_size(), 
		      levelname,
		      offset);
}


class drop_item_t : public limb_visitor_t
{
  square_t *square;
  koord pos;

public:

  virtual void visit_limb(const handle_tpl <thing_t> &thing) {
    // printf("visit\n");
    if(thing->get_item().is_bound()) {
      // printf("Dropping %s\n", thing->get_item()->get_string("ident", "unnamed"));
      thing->get_item()->set("curse_known", "false");

      square->add_thing(thing->get_item(), pos);
      // thing->set_item(0);
    }
  }
  
  drop_item_t(square_t *s, koord k) {
    square = s;
    pos = k;
  }
};


world_t * world_t::get_instance()
{
  if (single_instance == 0) {
    single_instance = new world_t();
  }
  return single_instance;
}


world_t * world_t::single_instance= 0;


world_t::world_t() {
  player_visual = 0;
  level = 0;
  user_input = 0;

  keep_player = false;

  level_fab = new level_factory_t();
  thing_fab = new thing_factory_t(); 


  user_input = new user_input_t(0);
  // user_input = new remote_user_t(player);
}


world_t::~world_t() {
  delete level_fab;
  level_fab = 0;

  delete thing_fab;
  thing_fab = 0;

  // delete old level
  delete level;
  level = 0;
}


void world_t::init(world_view_t * view, properties_t * game_props)
{
  dbg->message("world_t::init()", "called"); 
  this->view = view;
  this->game_props = game_props;


  // Hajo: randomize flavors
  // must be done before anything else

  const int num_flavours = game_props->get_int("item_flavors", 0);

  rng_t &rng = rng_t::get_the_rng();
  slist_tpl <unsigned char> list;
  for(int i=1; i<num_flavours; i++) {
    list.append(i);
  }

  // first entry is fixed, but unused
  flavors[0] = 0;

  int n = 1;
  while(list.count()) {
    const int i = rng.get_int(list.count());
    const int v = list.at(i);
    flavors[n++] = v;
    list.remove(v);
  }


  // deregister old player
  if(player.is_bound()) {
      scheduler_t::get_instance()->remove_actor(&(player->get_actor()));
  }

  // Play animations ?
  if(game_props->get_int("frametime")) {
    user_input->set_frametime(*game_props->get_int("frametime"));
  } else {
    user_input->set_frametime(-1);
  }

  dbg->message("world_t::init()", "done"); 
}


void world_t::set_player(thinghandle_t thing)
{
  player = thing;

  // set property TH_IS_PLAYER to "true"
  // always use TH_IS_PLAYER to check if a thing is the player!!!
  player->set(TH_IS_PLAYER, "true");


  user_input->set_thing(player);
  

  player->get_actor().delete_all_actions_but(user_input);
  player->get_actor().add_action(user_input);

  player_visual = new player_visual_connector_t(player, player->visual);
  player_visual->update(player);

  dbg->message("world_t::init()", "player created"); 
}


void world_t::destroy(minivec_tpl <thinghandle_t> & things_in_transit)
{ 
  // delete all actions (actors deregister during actor destruction)

  if(level) {

    koord size = level->get_size();
    koord ij;

    for(ij.y=0; ij.y<size.y; ij.y++) {
      for(ij.x=0; ij.x<size.x; ij.x++) {
	const square_t *square = level->at(ij);
	
	if(square->get_things().count()) {
	  minivec_iterator_tpl <thinghandle_t> iter (square->get_things());
	  
	  while(iter.next()) {
	    const thinghandle_t & thing = iter.get_current();
	    
	    
	    if(*thing->get_string("status", "enemy") == 'f') {
	      // keep all friends
	      things_in_transit.append(thing);

	    } else {
	      thing->get_actor().delete_all_actions_but(user_input);
	      thing->destroy();
	    }
	  }
	}
      }
    }
  
    // delete old level
    delete level;
    level = 0;

    //  view->clear_level();

    dbg->message("world_t::destroy()", "done, %d things in transit",
		 things_in_transit.count()); 
  } else {
    dbg->message("world_t::destroy()", "nothing to do"); 
  }
}


/**
 * After a new level is set, some initialization of the view
 * must be done. This is triggered from here.
 * @author Hj. Malthaner
 */
void world_t::init_level_view()
{
  properties_t * props = level->get_properties();
  const int images = *props->get_int("images");

  for(int i=0; i<images; i++) {
    char buf[256];
      
    sprintf(buf, "image[%d]", i);
    
    world_view_t::get_instance()->prepare_image(i, props->get_string(buf));
  }
}


/**
 * Prepares a new game. Deletes old saved levels
 * @author Hj. Malthaner
 */
void world_t::new_game(const cstring_t & base_path)
{
  dbg->message("world_t::new_game()", "called"); 

  // Hajo: delete old saved levels

  searchfolder_t dir;
  unsigned int n;

  n = dir.search(base_path + "/" + "save/", "plvl");

  for(unsigned int i=0; i<n; i++) { 
    dbg->message("world_t::new_game()", "removing: %s\n", dir.at(i).chars());
    int success = unlink(dir.at(i));
    if(success == -1) {
      perror("Error");
    }
  }


  n = dir.search(base_path + "/" + "save/", "save");

  for(unsigned int i=0; i<n; i++) { 
    dbg->message("world_t::new_game()", "removing: %s\n", dir.at(i).chars());
    int success = unlink(dir.at(i));
    if(success == -1) {
      perror("Error");
    }
  }
}




/**
 * Creates a new level. Treats persistent levels.
 * @param create_new set to true if persistent levels shall
 *                   be recreated from level template file
 * @param save_old set to false if persistent levels shall not
 *                 be saved on exiting
 * @author Hj. Malthaner
 */
void world_t::new_level(pprops_t *props, 
			bool create_new,
			bool save_old)
{
  // need to destroy old level?

  minivec_tpl <thinghandle_t> things_in_transit (128);

  if(level) {

    // persistent level must be saved

    const char * name = 
      level->get_properties()->get_string("persistence_file");

    if(save_old && name) {
      save_file(name);
    }

    // Hajo: fill vector with things to keep in new level
    destroy(things_in_transit);

  } // if(level)



  // persistent level must be loaded

  const char * name = 0;

  if(!create_new) {
    name = props->get_string("persistence_file");
  }


  dbg->message("world_t::new_level()", 
	       "%d things_in_transit, persistence_file=%s",
	       things_in_transit.count(), name);


  int result = 0;

  if(name) {
    keep_player = true;
    result = load_file(name);
    keep_player = false;
  }

  // -1 means that level was not saved before
  if(name == 0 || result == -1) {

    // create new level
    level = level_fab->create_level(props, player);

    dbg->message("world_t::new_level()", "level created"); 

    init_level_view();
    view->set_ij_off(player->get_pos());

    prepare_level(view, level);

    dbg->message("world_t::new_level()", "world view set, done"); 

  } // if(result == -1)


  // Hajo: add things that come with player 
  const koord pos = player->get_pos();


  dbg->message("world_t::new_level()", 
	       "%d things_in_transit, adding near %d,%d",
	       things_in_transit.count(),
	       pos.x, pos.y); 

  minivec_iterator_tpl <thinghandle_t> iter (things_in_transit);
  while( iter.next() ) {
    thinghandle_t thing (iter.get_current());
    if(thing != player) {
      level->add_thing_near(pos, thing);
    }
  }


  view->set_ij_off(player->get_pos());
  view->set_dirty(true);
  view->redraw();
}


/**
 * Loads a new level from template file.
 * @param force_load set to true if persitent levels shall
 *                   be recreated from level templeate file
 * @author Hj. Malthaner
 */
void world_t::load_level(const char *name, bool force_load) 
{
  dbg->message("world_t::load_level()", "loading '%s'", name); 


  // read level description

  pprops_t *props = new pprops_t();
  props->load(name);
  
  dbg->message("world_t::load_level()", "props loaded"); 

  // Hajo: true = always saves the old lavel, if that level is
  // a persistent level
  new_level(props, force_load, true);
}


/**
 * removes the thing from the world and does some cleanup
 * @param thing thing to kill
 * @silent if true, suppress messages
 * @author Hj. Malthaner
 */
void world_t::kill(thinghandle_t thing, bool silent)
{
  dbg->message("world_t::kill()", "entered, killing %p", thing.get_rep()); 

  if(player == thing) {

    /*
    linguist_t::translate("The gods of debugging code resurrect you.");
    thing->set("HPcurr", 9999);
    */

    scheduler_t::get_instance()->set_state(scheduler_t::GAME_OVER);

  } else {

    lua_call_t call("on_kill");
    call.activate(thing, silent);

    printf("kill: %d refs\n", thing.get_count());

    drop_item_t dropper (level->at(thing->get_pos()), thing->get_pos());
    thing_traversor_t trav (thing);

    trav.set_limb_visitor(&dropper);
    trav.traverse();
    trav.set_model(0);

    scheduler_t::get_instance()->remove_actor(&thing->get_actor());

    square_t * square = level->at(thing->get_pos());
    if(square) {
      square->remove_thing(thing);
    } else {
      dbg->error("world_t::kill()",
		 "Can't kill a thing at a position outside of the level!");
    }

    printf("kill: (level) %d refs left\n", thing.get_count());

    thing->get_actor().delete_all_actions_but(0);

    printf("kill: (actor) %d refs left\n", thing.get_count());

    thing->destroy();

    printf("kill: %d refs left\n", thing.get_count());
    
    view->set_dirty(true);
    view->redraw();
  } 

  dbg->message("world_t::kill()", "left"); 
}


/**
 * Updates the display data
 */
void world_t::recalculate_view(const koord view_pos, 
			       const int dpy_width, 
			       const int dpy_height,
			       abs_view_t *abs_view)
{
  // dbg->message("world_t::recalculate_view()", "called, level=%p", level);

  if(level && 
     (player.is_bound() == false || player->get_int("effect.blind", 0) <= 0)) {

    // zuerst den boden zeichnen
    // denn der Boden kann kein Objekt verdecken
    ij_off = view_pos;
    const int i_off = ij_off.x;
    const int j_off = ij_off.y;

    // field of view calculation

    if(level->is_lit()) {
      light_rad = 24;
      level->recalculate_fov(ij_off, 24);
    } else {
      light_rad = player->get_int("light_rad", 4);
      level->recalculate_fov(ij_off, 16);
    }

    // start displaying
    
    for (int y = -dpy_height; y < dpy_height; y++) {
      for (int x = -dpy_width; x < dpy_width; x++) {
      
	const int i = x + i_off;
	const int j = y + j_off;
      
	display_update(i, j, abs_view, false);
      }
    }
  }
}


#define SQR(x) ((x)*(x))


#define MAX_LIGHT 1900000
#define DEC_LIGHT 180000


/**
 * Updates view data at location i, j 
 * @author Hj. Malthaner
 */
void world_t::display_update(const int i, const int j,
			     abs_view_t *abs_view,
			     bool force_update)
{
  const koord k (i,j);
  square_t *square = level->at(k);


  if(square /* && square->get_flag(square_t::dirty) */) {
    // square->clr_flag(square_t::dirty);

    const int  dist = ij_off.apx_distance_to(k);
    const bool lit = dist < light_rad || level->is_lit(); 
    const groundhandle_t & ground = square->get_ground();
    const int env_light = square->lookup_env().lightness;

    static ground_visual_t grv;       // static to keep cache-friendly

    if((level->is_viewable(k) && (lit || env_light > 0)) ||
       force_update) {

      // ground display
    
      if(ground.is_bound()) {

	grv = ground->visual;

	const koord center (ij_off*2 +koord(1,1));
	const koord k2 (k*2);


	if(level->is_lit()) {
	  // grv.shade[0]=grv.shade[1]=grv.shade[2]=grv.shade[3] = 20; 
	} else {
	  grv.shade[0] += 
	    ((MAX_LIGHT*light_rad / (center.apx_distance_to(k2)+light_rad)) >> 16) + level->get_light(k);
	  grv.shade[1] += 
	    ((MAX_LIGHT*light_rad / (center.apx_distance_to(k2+koord(0, 2))+light_rad)) >> 16) + level->get_light(k + koord(0,1));
	  grv.shade[2] += 
	    ((MAX_LIGHT*light_rad / (center.apx_distance_to(k2+koord(2, 2))+light_rad)) >> 16) + level->get_light(k + koord(1,1));
	  grv.shade[3] += 
	    ((MAX_LIGHT*light_rad / (center.apx_distance_to(k2+koord(2, 0))+light_rad)) >> 16) + level->get_light(k + koord(1,0));
	}

	// grv.tex = ground->visual.get_image(0);

	abs_view->update_ground(k, &grv);
      }

    
      // feature and things display

      const int shade = 
	level->is_lit() ? 28 : (MAX_LIGHT*light_rad / (dist+light_rad)) >> 16;

      abs_view->clear_visuals(k);

      // paint feature
      if( square->get_feature().is_bound() ) {
	/*	  
	bool trans = false;

	if(square->get_feature()->is_opaque()) {
	  trans = 
	    dist < 6 &&
	    (is_viewable(i-1,j) ||
	     is_viewable(i-1,j-1) ||
	     is_viewable(i,j-1));
	}
	*/

	// square->get_feature()->paint(gr, xpos, ypos, shade>>16, trans);

	featurehandle_t feature = square->get_feature(); 
	visual_t & visual = feature->visual;
	visual.set_shade(shade + env_light < 32 ? shade+env_light : 32);
	// visual.set_transparent(trans);

	abs_view->add_feature_visual(k, &visual, feature->get_flags());

	// visual.inc_frame();
      }
	

      // paint traps
      trigger_t * trigger = square->get_trigger();
      if(trigger && trigger->is_discovered() && trigger->is_active()) { 
	visual_t visual;
	visual.set_image(0, 100, 0, 0, 0, 0, false);
	abs_view->add_visual(k, &visual);
      }


      // paint things
      minivec_iterator_tpl <thinghandle_t> iter (square->get_things());
	
      while(iter.next()) {
	visual_t & visual = iter.get_current()->visual;
	visual.set_shade(shade + env_light < 32 ? shade+env_light : 32);

	abs_view->add_visual(k, &visual);
      }
    } else {
      // Update ground height only
      if(ground.is_bound()) {

	grv = ground->visual;
	grv.tex = 0xFFFF;     // Hack: these grounds are not remembered!
	abs_view->update_ground(k, &grv);
      }

      // not viewable or not lit
      abs_view->set_dark(k);
    }
  }
}
  

// ---- implements persistent_t ----

/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void world_t::read_write(storage_t *store, iofile_t * file)
{
  for(int i=0; i<16; i++) {
    file->rdwr_uchar(flavors[i]);    
  }


  if(file->is_saving()) {

    store->store(level, file);
    store->store(player.get_rep(), file);

    int time = scheduler_t::get_instance()->get_time();
    file->rdwr_int(time);

    view->read_write(store, file);

  } else {
      dbg->message("world_t::read_write()", "keep_player=%d", keep_player);


    /* 
     * If loading a saved game, we want to load a new player
     * (keep_player == false). If loading a persistent level
     * we want to keep the current player (keep_player == true)
     */

    if(keep_player) {

      level = (level_t *)store->restore(file, hstore_t::t_level);

      // This is the saved player
      thinghandle_t game_player = 
	thinghandle_t((thing_t *)store->restore(file, hstore_t::t_thing));



      // Hajo: friends come with player - remove old friends from
      // saved data
      koord size = level->get_size();
      koord ij;

      for(ij.y=0; ij.y<size.y; ij.y++) {
	for(ij.x=0; ij.x<size.x; ij.x++) {
	  square_t *square = level->at(ij);
	  const minivec_tpl <thinghandle_t> & things = square->get_things();
	
	  if(things.count()) {
	    
	    // Hajo: loop backwards, we remove things!!!
	    for(int i=things.count()-1; i>=0; i--) {
	      const thinghandle_t & thing = things.get(i);
	    
	      if(*thing->get_string("status", "enemy") == 'f') {
		// Hajo: remove all friends

		thing->get_actor().delete_all_actions_but(user_input);
		thing->destroy();

		square->remove_thing(thing);
	      }
	    }
	  }
	}
      }


      // Hajo: treat saved player

      // calculate position

      koord pos = game_player->get_pos();

      // special case for preset position

      
      if(*player->get_string("keep_position", "false") == 't') {
	pos = player->get_pos();
      }

      // replace saved player with current player
      level->at(pos)->remove_thing(game_player);
      level->at(pos)->add_thing(player, pos);


      dbg->message("world_t::read_write()", "adding player at %d %d\n", 
		   pos.x, pos.y); 

      game_player->destroy();


      dbg->message("world_t::read_write()", "player is %d %p, game player refs %d\n", 
		   player.is_bound(), player.get_rep(), 
		   game_player.get_count());


    } else {
    
      delete player_visual;
      player_visual = 0;

      if(player.is_bound()) {
	player->get_actor().delete_all_actions_but(user_input);
	player->get_actor().remove_action(user_input);
	player->destroy();
      }

      user_input->set_thing(0);

      dbg->message("world_t::read_write()", "player: %d refs left\n", player.get_count());

      player = 0;


      // check that all actors have been removed
      scheduler_t::get_instance()->dump();



      level = (level_t *)store->restore(file, hstore_t::t_level);

      player = 
	thinghandle_t((thing_t *)store->restore(file, hstore_t::t_thing));

      // restoring transient links
      player_visual = new player_visual_connector_t(player, player->visual);
    }


    // XXX Hack to restore the one and only user input
    user_input->set_thing(player);
    
    player->get_actor().delete_all_actions_but(user_input);
    player->get_actor().add_action(user_input);


    // check that player actor is added again
    scheduler_t::get_instance()->dump();


    // restore time
    int time;
    file->rdwr_int(time);
    scheduler_t::get_instance()->set_time(time);

    init_level_view();
    view->set_ij_off(player->get_pos());

    prepare_level(view, level);


    // load old 'remembered' map
    view->read_write(store, file);


    // re-link player UI
    player_gui_t::get_instance()->link(player);


    player_visual->update(player);
  }

  dbg->message("world_t::read_write()", "done");
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t world_t::get_cid()
{
  return hstore_t::t_world;
}


