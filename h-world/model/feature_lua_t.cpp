/* 
 * feature_lua.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "feature_lua_t.h"
#include "thing_t.h"

#include "model/thinghandle_t.h"
#include "control/lua_call_t.h"

#include "factories/hstore_t.h"

#include "util/debug_t.h"


feature_lua_t::feature_lua_t()
{
  call = new lua_call_t("unset");
}


feature_lua_t::feature_lua_t(const char *v)
{
  call = new lua_call_t(v);
}
  

feature_lua_t::~feature_lua_t()
{
  delete call;
  call = 0;
}


/**
 * Fountains fill bottles with water
 * @author Hj. Malthaner
 */
bool feature_lua_t::activate(thinghandle_t thing)
{
  // dbg->message("feature_lua_t::activate()", "calling %s", call->get_call()); 

  const int r = call->activate(thing, 0);
  return r != 0;
}


// ---- implements persistent_t ----


/**
 * Stores/restores the object.
 * @author Hj. Malthaner
 */
void feature_lua_t::read_write(storage_t *store, iofile_t * file)
{
  call->read_write(store, file);
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t feature_lua_t::get_cid()
{
  return hstore_t::t_feature_lua;
}
