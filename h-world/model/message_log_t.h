/* Copyright by Hj. Malthaner */

#ifndef MESSAGE_LOG_T_H
#define MESSAGE_LOG_T_H

#define LOG_LENGTH 10

class message_log_view_t;

class message_log_t {
private:
    /**
     * @link
     * @shapeType PatternLink
     * @pattern Singleton
     * @supplierRole Singleton factory 
     */
    /*# message_log_t __message_log_t; */
    static message_log_t * single_instance;


    message_log_view_t *view;

    message_log_t();

    char * messages[LOG_LENGTH];

public:    

    void set_view(message_log_view_t *view) {this->view = view;};
    
    const char * get_tail(int n);

    void add_message(const char *);

    void clear();

    static message_log_t * get_instance();
};

#endif //MESSAGE_LOG_T_H
