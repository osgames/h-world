#ifndef thinghandle_t_h
#define thinghandle_t_h
                
// #include "tpl/handle_tpl.h"

class thing_t;
template <class T> class handle_tpl;

typedef handle_tpl<thing_t> thinghandle_t;

#endif
