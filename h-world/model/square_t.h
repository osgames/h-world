/* 
 * square_t.h
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef SQUARE_T_H
#define SQUARE_T_H

#ifndef ENVIRONMENT_T_H
#include "environment_t.h"
#endif

#ifndef featurehandle_t_h
#include "featurehandle_t.h"
#endif

#ifndef groundhandle_t_h
#include "groundhandle_t.h"
#endif

#ifndef thinghandle_t_h
#include "thinghandle_t.h"
#endif

#ifndef tpl_minivec_h
#include "tpl/minivec_tpl.h"
#endif

#ifndef tpl_handle_tpl_h
#include "tpl/handle_tpl.h"
#endif

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif


class square_constraint_t;
class square_listener_t;
class trigger_t;
class koord;


class square_t : public persistent_t {
public:

  enum flag_values {none=0, dirty=1};
  enum room_types {undefined=0, corridor=1};

private:    

    /** @link aggregation */
    environment_t env;

    /**
     * @supplierCardinality 0..* 
     */
    minivec_tpl <thinghandle_t> things;

    minivec_tpl <square_constraint_t *> constraints;


    /**
     * @supplierCardinality 1 
     */
    groundhandle_t ground;

    featurehandle_t feature;


    /**
     * Triggers are owned by the square. Upon destrction a square
     * destroys the trigger if one is set.
     * @author Hj. Malthaner
     */     
    trigger_t * trigger;

    unsigned char flags;

public:

    /**
     * Rooms may have types. In this case all sqaures of a room are
     * set to the appropriate type
     * @author Hj. Malthaner
     */
    unsigned char room_type;

    /**
     * Hotspots to help the AI
     * @author Hj. Malthaner
     */
    unsigned char hots_type;


    bool get_flag(int f) const {return flags & f;};
    void set_flag(int f) {flags |= f;};
    void clr_flag(int f) {flags &= ~f;};


    void add_constraint(square_constraint_t *c);


    /**
     * Sets a trigger on this square
     * @author Hj. Malthaner
     */     
    void set_trigger(trigger_t *trigger);


    /**
     * Removes the trigger on this square
     * @return the old trigger
     * @author Hj. Malthaner
     */     
    trigger_t * remove_trigger();


    /**
     * @return the current trigger
     * @author Hj. Malthaner
     */     
    trigger_t * get_trigger() const {return trigger;};


    /**
     * checks if that thing can enter this square (constraints etc)
     * @author Hj. Malthaner
     */     
    bool can_enter(const thinghandle_t & thing) const;


    /**
     * checks if that thing can traverse this square
     * @author Hj. Malthaner
     */     
    bool can_traverse(const thinghandle_t &thing) const;


    /**
     * Adds a thing to this sqaure. Sets thing position to k.
     * @param k new position of the thing
     * @author Hj. Malthaner
     */
    void add_thing(thinghandle_t thing, const koord & k);


    bool remove_thing(thinghandle_t thing);

    const minivec_tpl <thinghandle_t> & get_things() const {return things;};

    /**
     * Allows to read the environment directly.
     * @author Hj. Malthaner
     */     
    const environment_t & lookup_env() const {return env;};

    /**
     * Allows to access (modify) the environment directly.
     * @author Hj. Malthaner
     */     
    environment_t & access_env() {return env;};


    void set_feature(const featurehandle_t &feature);
    const featurehandle_t & get_feature() const {return feature;};

    void set_ground(const groundhandle_t &ground);
    const groundhandle_t & get_ground() const {return ground;};

    /**
     * Basic constructor.
     * @author Hj. Malthaner
     */
    square_t();

    virtual ~square_t();


    // ---- implements persistent_t ----

    /**
     * Stores/restores the object. Must be overridden by subclass.
     * This method is supposed to call store->store() on all associated
     * objects!
     * @author Hj. Malthaner
     */
    virtual void read_write(storage_t *store, iofile_t * file);


    /**
     * Gets a unique ID for this objects class.
     * @author Hj. Malthaner
     */
    virtual perid_t get_cid();
};
#endif //SQUARE_T_H
