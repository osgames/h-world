/* 
 * feature_lua_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef feature_lua_t_h
#define feature_lua_t_h

#include "feature_peer_t.h"

class lua_call_t;

/**
 * Calls a lua function on activation
 * @author Hj. Malthaner
 */
class feature_lua_t : public feature_peer_t
{
 private:

  lua_call_t * call;

 public:

  feature_lua_t();
  feature_lua_t(const char *v);
  ~feature_lua_t();


  /**
   * Fountains fill bottles with water
   * @author Hj. Malthaner
   */
  virtual bool activate(handle_tpl <thing_t> thing);


  // ---- implements persistent_t ----
  
  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);

  
  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};

#endif // feature_lua_t_h
