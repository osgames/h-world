#include <string.h>
#include <stdlib.h>

#include "dialog_t.h"
#include "util/config_file_t.h"


dialog_t * dialog_t::read(config_file_t *file)
{
  char buf[8192];
  
  file->read_line(buf, 8190);


  if(strstr(buf, "{}")) {
    // empty dialog
    return 0;
  } else {
    
    dialog_t *dia = new dialog_t();

    file->read_line(buf, 8190);
    
    dia->speech = strdup(buf);

    int i = 0;
    bool ok;
    do {
      file->read_line(buf, 8190);
      ok = (strstr(buf, "}") == 0) && (!file->eof());

      if(ok) {
	dia->reply[i] = strdup(buf);
	dia->dialog[i] = read(file);

	i++;
      }

    } while (ok && i<MAX_REPLY);

    return dia;
  }
}


dialog_t::dialog_t()
{
  speech = 0;

  for(int i=0; i<MAX_REPLY; i++) { 
    reply[i] = 0;
    dialog[i] = 0;
  }
}


dialog_t::~dialog_t()
{
  free(speech);
  speech = 0;

  for(int i=0; i<MAX_REPLY; i++) { 

    free(reply[i]);
    reply[i] = 0;

    delete(dialog[i]);
    dialog[i] = 0;
  }
}
