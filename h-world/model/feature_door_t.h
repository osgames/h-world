/* 
 * feature_door_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef FEATURE_DOOR_T_H
#define FEATURE_DOOR_T_H

#include "primitives/koord.h"
#include "feature_peer_t.h"

/**
 * Door feature.
 * @author Hj. Malthaner
 */
class feature_door_t : public feature_peer_t 
{
private:

  /**
   * Position of the door
   * @author Hj. Malthaner
   */
  koord door_pos;


  /**
   * State of the door. True means open.
   * @author Hj. Malthaner
   */
  signed char is_open;

  unsigned char open_set;
  unsigned char closed_set;

  unsigned short open_image;
  unsigned short closed_image;

  short open_x_off;
  short open_y_off;

public:

  void set_door_pos(koord k) {door_pos = k;};

  feature_door_t();
  feature_door_t(koord pos, 
		 int open_set, int open, 
		 int closed_set, int closed, 
		 int open_x, int open_y);

  feature_door_t(koord pos, const char * open, const char * closed, int open_x, int open_y);


  /**
   * Opens/closes this door.
   * @author Hj. Malthaner
   */
  virtual bool activate(handle_tpl <thing_t> thing);


  // ---- implements persistent_t ----
  
  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);

  
  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();
};                  

#endif
