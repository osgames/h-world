/* 
 * filename
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef THING_T_H
#define THING_T_H

#ifndef koord_h
#include "primitives/koord.h"
#endif

#ifndef tpl_slist_tpl_h
#include "tpl/slist_tpl.h"
#endif

#ifndef tpl_handle_tpl_h
#include "tpl/handle_tpl.h"
#endif

#ifndef ACTOR_T_H
#include "control/actor_t.h"
#endif

#ifndef ENVIRONMENT_CONNECTOR_T_H
#include "control/environment_connector_t.h"
#endif

#ifndef VISUAL_T_H
#include "view/visual_t.h"
#endif

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

class usage_t;
class inventory_t;
class thing_listener_t;
class memory_t;

class property_t;
class properties_t;
class pprops_t;
class property_listener_t;

/**
 * Dungeon Objects. Instead of using inheritance to model
 * the different objects, the behavior of the objects is
 * delegated to associated objects. This class is a mere
 * skeleton and holds the basic data of dungeon objects,
 * all the behaviour is delegated.
 *
 * @author Hansj�rg Malthaner
 */
class thing_t : public persistent_t {
public:

    /**
     * Finds the root of a thing tree. This is a class method becuase
     * the reference counting does not allow a "self" handle for things
     *
     * @author Hj. Malthaner
     */
    static handle_tpl<thing_t> find_root(handle_tpl <thing_t> body);


    /**
     * Find a limb recursively by type and subtype
     *
     * @author Hj. Malthaner
     */
    static handle_tpl<thing_t> find_limb(handle_tpl <thing_t> body, 
			       const char *type, 
			       const char *subtype);

    /**
     * Find a limb that is holding the given item recursively
     *
     * @author Hj. Malthaner
     */
    static handle_tpl<thing_t> find_limb(handle_tpl <thing_t> body, 
					 handle_tpl <thing_t> item); 


    /**
     * Find a limb recursively by item constraints, that is find
     * a limb that can hold the item
     *
     * @author Hj. Malthaner
     */
    static handle_tpl <thing_t> find_limb_to_hold(handle_tpl <thing_t> body,
						  handle_tpl <thing_t> item);


    /**
     * Remove a thing, checks held items and inventories
     * @return true on success
     * @author Hj. Malthaner
     */
    static bool replace_thing(handle_tpl <thing_t> body,
			      handle_tpl <thing_t> old_item,
			      handle_tpl <thing_t> new_item);


    /**
     * Determines if this is a known item or not
     * @return true if unknown
     * @author Hj. Malthaner
     */
    static bool is_unknown(const properties_t * props);


private:    

    /**
     * The objects actor. Determines behavior.
     * @author Hansj�rg Malthaner
     */
    actor_t actor;


    /**
     * This is the physical representation of the thing. The body may be
     * emptry or consists of a list of limbs.
     *
     * @see parent
     * @author Hansj�rg Malthaner
     * @supplierRole limb
     */
    slist_tpl < handle_tpl<thing_t> > limbs;


    /**
     * The thing to which body this limb belongs.
     *
     * @see limbs
     * @author Hj. Malthaner
     */
    handle_tpl <thing_t> parent;


    /**
     * If this thing is held by another thing, the owner
     * variable points to the owner 
     *
     * @see item
     * @author Hj. Malthaner
     */
    handle_tpl <thing_t> owner;


    /**
     * The thing which is held by this limb.
     *
     * @see owner
     * @author Hj. Malthaner
     * @supplierRole item
     */
    handle_tpl <thing_t> item;


    /**
     * The objects environment connector. Determines interaction
     * with the environment
     * @author Hansj�rg Malthaner
     */
    environment_connector_t envi_conn;


    /**
     * Some things may contain other things. In this case they are
     * associated with an inventory
     * @author Hansj�rg Malthaner
     */
    inventory_t * inventory;


    /**
     * Things may be contained in inventories.
     * @author Hansj�rg Malthaner
     */
    inventory_t * container;


    /**
     * Some things can memorize information
     * @author Hansj�rg Malthaner
     */
    memory_t * memory;


    /** The location of the object.
     * @link aggregation 
     * @label location
     * @author Hansj�rg Malthaner
     */
    koord pos;


    /**
     * usually things have lots of properties. This are type-value pairs.
     * Lifetime is object lifetime.
     * @author Hansj�rg Malthaner
     */
    pprops_t * properties;


    /**
     * Dungeon level number.
     * @author Hansj�rg Malthaner
     */
    int level;


    /**
     * the space this thing needs to be stored
     */
    koord inv_size;


    /**
     * some things might have usages
     * @author Hansj�rg Malthaner
     */
    slist_tpl <usage_t *> usages;



    /**
     * If this thing changed, we need to inform the listeners
     * @author Hansj�rg Malthaner
     */
    slist_tpl <thing_listener_t *> listeners;


    thing_t(const thing_t &t) {};

public:


    /**
     * Counts number of special attributes
     * @author Hj. Malthaner
     */
    int count_attributes() const;


    /**
     * Fills buffer with an identifier (no magic attributes of curses)
     * for this thing (aka "short name")
     * @return number of characters written into buf
     * @author Hj. Malthaner
     */
    int get_ident_plain(char *buf);


    /**
     * Fills buffer with an identifier (prefix+ident) for
     * this thing (aka "long name")
     * @return number of characters written into buf
     * @author Hj. Malthaner
     */
    int get_ident(char *buf);


    /**
     * Calls all listenrs
     * @author Hansj�rg Malthaner
     */
    void call_listeners();


    /**
     * Deep copy
     * - listeners are not copied!
     *
     * @author Hj. Malthaner
     */
    const handle_tpl <thing_t> clone() const;


    /**
     * Gets the thing to which body this limb belongs.
     *
     * @author Hj. Malthaner
     */
    const handle_tpl <thing_t> & get_parent() const {return parent;};


    /**
     * Gets the thing that is holding this item
     *
     * @author Hj. Malthaner
     */
    const handle_tpl <thing_t> & get_owner() const {return owner;};
    

    /**
     * The visual representation of the thing.
     * @link aggregation
     * @author Hansj�rg Malthaner
     */
    visual_t visual;

    /**
     * Image to display in inventory view
     */
    visual_t inv_visual;


    void set_inv_size(koord s) {inv_size = s;};
    koord get_inv_size() {return inv_size;};


    /**
     * Gets position on level
     * @author Hansj�rg Malthaner
     */     
    koord get_pos() const {return pos;};


    /**
     * Sets position on level
     * @author Hansj�rg Malthaner
     */     
    void set_pos(koord pos) {this->pos = pos;};


    /**
     * Actor accessor.
     * @author Hansj�rg Malthaner
     */     
    actor_t & get_actor();


    /**
     * Inventory accessor.
     * @author Hansj�rg Malthaner
     */     
    inventory_t * get_inventory() const {return inventory;};


    /**
     * Sets a new inventory. Thing is responsoble to free inventories.
     * @author Hansj�rg Malthaner
     */     
    void set_inventory(inventory_t *inv);



    /**
     * Container accessor.
     * @author Hansj�rg Malthaner
     */     
    inventory_t * get_container() const {return container;};


    /**
     * Sets containing container
     * @author Hansj�rg Malthaner
     */     
    void set_container(inventory_t *con);


    /**
     * Sets a the memory. Thing is responsoble to free memories.
     * @author Hansj�rg Malthaner
     */     
    void set_memory(memory_t *mem);


    /**
     * Memorize item 'ident'
     * @author Hansj�rg Malthaner
     */     
    void set_item_known(const char * ident);


    /**
     * Memorize a message
     * @author Hansj�rg Malthaner
     */     
    void memorize(const char * catg, const char * msg);


    /**
     * Recalls the nth message
     * @return NULL if no such message exists
     * @author Hansj�rg Malthaner
     */     
    const char * recall(const char * catg, int n);


    /**
     * Check if this item can be held
     * @author Hansj�rg Malthaner
     */     
    bool check_item_constraints(handle_tpl <thing_t> thing);


    /**
     * Set item to be held
     * @param update if true, listeners are called
     * @author Hansj�rg Malthaner
     */     
    void set_item(handle_tpl <thing_t> thing, bool update);


    /**
     * Set one of a multi-limb item to be held
     * Does not call lua scripts or listeners!
     * @author Hansj�rg Malthaner
     */     
    void set_multi_item(handle_tpl <thing_t> thing);


    /**
     * Put multi-limb item on all limbs. Be careful, none of
     * the limbs may hold other items, if items are held the
     * behaviour is undefined.
     *
     * @param item the item to set, can be null
     * @author Hj. Malthaner
     */
    void put_multi(handle_tpl <thing_t> item);


    /**
     * Removes a multi limb item from all required limbs
     * @param item the item to remove, can be null
     * @authot Hj. Malthaner
     */
    void remove_multi(handle_tpl <thing_t> item);


    /**
     * Get held item
     * @author Hansj�rg Malthaner
     */     
    const handle_tpl <thing_t> & get_item();


    /**
     * Adds a usage
     * @author Hansj�rg Malthaner
     */     
    void add_usage(usage_t * usage);


    /**
     * Adds a limb to the body
     * @author Hansj�rg Malthaner
     */
    void add_limb(handle_tpl <thing_t> limb);


    /**
     * Gets the usage list
     * @author Hansj�rg Malthaner
     */     
    slist_tpl <usage_t *> & get_usages() {return usages;};


    /**
     * Adds a listener
     * @author Hansj�rg Malthaner
     */     
    void add_listener(thing_listener_t * listener);


    /**
     * Adds a property listener
     * @author Hansj�rg Malthaner
     */     
    void add_property_listener(property_listener_t * l);


    /**
     * Removes a listener
     * @author Hansj�rg Malthaner
     */     
    void remove_listener(thing_listener_t * listener);


    /**
     * Limbs accessor
     * @author Hansj�rg Malthaner
     */     
    const slist_tpl < handle_tpl <thing_t> > & get_limbs() const {return limbs;};


    /**
     * Basic constructor.
     * @author Hansj�rg Malthaner
     */     
    thing_t();


    /**
     * Creates a things as a part of another thing, i.e. a limb
     * @author Hansj�rg Malthaner
     */     
    thing_t(handle_tpl <thing_t> t);


    /**
     * Destructor. Frees the inventory.
     * @author Hansj�rg Malthaner
     */         
    ~thing_t();


    /**
     * sets property 'name' to a string value
     * @author Hansj�rg Malthaner
     */
    void set(const char * name, const char* value);


    /**
     * sets property 'name' to an int value
     * @author Hansj�rg Malthaner
     */
    void set(const char * name, int value);


    const properties_t * get_properties() const;


    /**
     * @return pointer to requested property or NULL if property does not exist
     * @author Hansj�rg Malthaner
     */
    const property_t * get(const char * name) const;


    /**
     * Get a named int property.
     * @param def default vlaue to return if the property does no exists
     * @author Hansj�rg Malthaner
     */
    int get_int(const char *name, int def) const;


    /**
     * Get a named string property.
     * @param def default vlaue to return if the property does no exists
     * @author Hansj�rg Malthaner
     */
    const char * get_string(const char *name, const char *def) const;


    /**
     * returns -1,-1 on failure!
     */
    koord get_koord(const char * name); 


    /**
     * Checks if the hitpoints dropped below 0 
     * @author Hj. Malthaner
     */
    bool check_alive();


    /**
     * Destroy limb tree, needed to clean this thing from memory.
     */
    void destroy();


    /**
     * Limbs will call this if a change has happened
     * @author Hansj�rg Malthaner
     */
    void body_changed();


    /**
     * Determines the current speed of this thing
     * @return modified delay
     * @author Hansj�rg Malthaner
     */
    int calc_speed(int delay);


    /**
     * Debugging, dump contents
     * @author Hansj�rg Malthaner
     */
    void dump();


    // ---- implements persistent_t ----

    /**
     * Stores/restores the object. Must be overridden by subclass.
     * This method is supposed to call store->store() on all associated
     * objects!
     * @author Hj. Malthaner
     */
    virtual void read_write(storage_t *store, iofile_t * file);


    /**
     * Gets a unique ID for this objects class.
     * @author Hj. Malthaner
     */
    virtual perid_t get_cid();

};

#endif //THING_T_H
