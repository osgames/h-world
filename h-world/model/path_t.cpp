/* 
 * path_t.cpp
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "path_t.h"
#include "square_t.h"
#include "level_t.h"

#include "util/debug_t.h"
#include "persistence/iofile_t.h"

#include "primitives/bitmap_t.h"
#include "tpl/prioqueue_tpl.h"



static const koord path_8[8] = {
  koord( 1, 1),
  koord( 1,-1),
  koord(-1, 1),
  koord(-1, -1),

  koord( 0,-1),
  koord( 1, 0),
  koord( 0, 1),
  koord(-1, 0),
};


struct KNode {
  koord  pos;    
  int    dist;
  int    total;
  KNode * link;     
  
  inline bool operator < (const KNode &k) const {
    return dist+total <= k.dist+k.total;
  };
};



look_for_room_t::look_for_room_t(level_t * level, int room_type)
{
  this->level = level;
  this->room_type = room_type;
}

bool look_for_room_t::am_i_there(koord k) const
{
  square_t * square = level->at(k);
  return square && square->hots_type == room_type;
}


look_for_pos_t::look_for_pos_t(level_t * level, koord dest)
{
  this->level = level;
  this->dest = dest;
}
  
bool look_for_pos_t::am_i_there(koord k) const
{
  return k == dest;
}



path_t::path_t()
{
  n = -1;
  size = 0;
  dont_check_last = false;
}


/**
 * Finds shortest path from start to dest. 
 * Paths cannot be longer that 255 sqaures. Caller is responsible to delete
 * the path after usage.
 *
 * @author Hansj�rg Malthaner
 */
bool path_t::calculate(level_t * level, const thinghandle_t & thing,
		       const koord start, dest_cond_t * dest)
{
  const int MAX_STEP = 20000;
  static KNode nodes[MAX_STEP+8+1];
  int step = 0;

  static prioqueue_tpl <KNode *> queue;
  const koord level_size = level->get_size();
  bitmap_t map (level_size.x, level_size.y);


  dbg->debug("path_t::calculate()", "start:\t%d,%d", start.x, start.y);


  queue.clear();
  map.clr_map();

  // Hajo: init first node

  KNode *tmp = &nodes[step++];

  tmp->pos =   start;
  tmp->dist =  0;
  tmp->total = 0;
  tmp->link =  0;
    

  // Hajo: init queue with first node
  queue.insert( tmp );


  do {
    tmp = queue.pop();

    // Hajo: is destination reached?
    if(dest->am_i_there(tmp->pos) == false &&
       tmp->total < 250 &&
       step < MAX_STEP) {
      
      for(int r=0; r<8; r++) {
	const koord pos = tmp->pos + path_8[r];

	square_t * square = level->at(pos);

	if(square && 
	   map.get_bit(pos.x, pos.y) == 0 &&
	   (square->can_traverse(thing) ||
	    (dont_check_last && dest->am_i_there(pos)))) {

	  KNode *k = &nodes[step++];

	  k->pos   = pos;
	  k->dist  = 0;
	  k->total = tmp->total+1;
	  k->link  = tmp;
	  
	  queue.insert( k );
	  
	  // Hajo: mark entered squares
	  map.set_bit(pos.x, pos.y);
	}
      }
    }
  } while(queue.is_empty() == false 
	  && dest->am_i_there(tmp->pos) == false);


  bool ok = false;

  if(dest->am_i_there(tmp->pos)) {
    ok = true;
    size = tmp->total+1;

    do {
      steps[tmp->total] = tmp->pos;

      dbg->debug("path_t::calculate()",
		 "step %d:  %d,%d", tmp->total, tmp->pos.x, tmp->pos.y);

    } while((tmp = tmp->link));

  } else {
    size = 0;
    ok = false;
  }

  return ok;
}


/**
 * Stores/restores the object.
 * @author Hj. Malthaner
 */
void path_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_uint(size);
  file->rdwr_int(n);
  file->rdwr_uchar(dont_check_last);

  for(unsigned int i=0; i<size; i++) { 
    file->rdwr_short(steps[i].x);
    file->rdwr_short(steps[i].y);
  }
}
