#ifndef map_model_t_h
#define map_model_t_h

#include "primitives/koord.h"


class image_t;

/**
 * Client side model for the minimap view. Singleton (one per client).
 * @author Hj. Malthaner
 */
class map_model_t
{
  static map_model_t * single_instance;

  map_model_t();

  image_t *img;

 public:

  static map_model_t * get_instance();

  ~map_model_t();

  image_t * get_image() const {return img;};


  void update(koord k);


  void init();

};

#endif // map_model_t_h
