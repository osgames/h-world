/* 
 * feature_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "feature_t.h"
#include "feature_lua_t.h"
#include "feature_peer_t.h"
#include "thing_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"
#include "factories/feature_factory_t.h"

#include "util/properties_t.h"
#include "util/debug_t.h"

#ifndef thinghandle_t_h
#include "thinghandle_t.h"
#endif


/**
 * Sets the peer for this feature.
 * @author Hj. Malthaner
 */
void feature_t::set_peer(featurepeerhandle_t peer)
{
  this->peer = peer;
}


/**
 * Gets the peer of this feature.
 * @author Hj. Malthaner
 */
const featurepeerhandle_t & feature_t::get_peer() const
{
  return peer;
}


/**
 * Sets this features properties.
 * @author Hj. Malthaner
 */
void feature_t::set_props(const properties_t * props)
{
  this->props = props;

  image_meta_t meta = feature_factory_t::load(props->get_string("sct"));
  visual.set_image(0, meta);

  if(props->get_string("9pw")) {
    set_flag(feature_t::F_NINEPARTWALL);
  }

  if(props->get_string("9pc")) {
    set_flag(feature_t::F_DOORPOST);
  }

  visual.set_transparent(*props->get_string("trans", "f") == 't');
  visual.set_opaque(*props->get_string("opaque", "t") == 't');

  const int jitter = props->get_int("jitter", 0);
  visual.calc_jitter(jitter);

  const int *ip;

  ip = props->get_int("offset.x");
  if(ip) {
    visual.set_x_off(*ip);
  }

  ip = props->get_int("offset.y");
  if(ip) {
    visual.set_y_off(*ip);
  }


  set_blocking(*props->get_string("blocking", "t") == 't');

  if(peer.is_bound() == false &&  props->get_string("lua")) {
    set_peer(new feature_lua_t(props->get_string("lua")));
  }

  light_rad = props->get_int("light_rad", 0);
}


/**
 * Get a named int property.
 * @param def default vlaue to return if the property does no exists
 * @author Hansj�rg Malthaner
 */
int feature_t::get_int(const char *name, int def) const
{
  return props->get_int(name, def);
}


/**
 * Get a named string property.
 * @param def default vlaue to return if the property does no exists
 * @author Hansj�rg Malthaner
 */
const char * feature_t::get_string(const char *name, const char *def) const
{
  return props->get_string(name, def);
}


void feature_t::set_flag(enum flags flag)
{
  flags |= flag;
}


void feature_t::clr_flag(enum flags flag)
{
  flags &= ~flag;
}


void feature_t::set_blocking(bool yesno) {
  if(yesno) {
    set_flag(BLOCKING);
  } else {
    clr_flag(BLOCKING);    
  }
}


/**
 * Gets the name of the feature
 * @param def default return in case of unnamed feature
 * @author Hj. Malthaner
 */
const char * feature_t::get_ident(const char * def) const
{
  const char * result = props->get_string("ident");

  if(result == 0) {
    result = def;
  }

  return result;
}


/**
 * Basic construcor, initializes a blocking feature.
 * @author Hj. Malthaner
 */
feature_t::feature_t()
{
  flags = F_OTHER | BLOCKING;
  visual.set_opaque(true);
  tunnel = 100;
  light_rad = 0;
}


/**
 * Copy constructor.
 * @author Hj. Malthaner
 */
feature_t::feature_t(feature_t *other)
{
  peer   = other->peer;
  flags  = other->flags;
  visual = other->visual;
  tunnel = other->tunnel;
  props  = other->props;
  light_rad = other->light_rad;
}


feature_t::~feature_t()
{
}


void feature_t::paint(graphics_t *gr, int xpos, int ypos, int shade, bool trans)
{
  visual.set_shade(shade);
  visual.set_transparent(trans);
  visual.paint(gr, xpos, ypos);
}


/**
 * Activate this feature. Delegated to the peer if a peer is
 * available.
 * @author Hj. Malthaner
 */
void feature_t::activate(const thinghandle_t &thing) const
{
  if(peer.is_bound()) {
    peer->activate(thing);
  }
}


// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void feature_t::read_write(storage_t *store, iofile_t * file)
{
  if(file->is_saving()) {
    store->store(peer.get_rep(), file);

    char * buf = (char *)props->get_string("sct");
    file->rdwr_string(buf);

  } else {
    peer = featurepeerhandle_t((feature_peer_t *) store->restore(file));

    char buf [128];
    file->rdwr_carr(buf);

    properties_t * fprops = feature_factory_t::get_props(buf);

    if(fprops == 0) {
      dbg->fatal("feature_t::read_write()",
		 "Can't load properties for '%s'\n", buf);
    }

    set_props(fprops);
  }

  file->rdwr_uchar(flags);
  file->rdwr_uchar(tunnel);

  visual.read_write(store, file);
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t feature_t::get_cid()
{
  return hstore_t::t_feature;
}
