/* Copyright by Hj. Malthaner */

#ifndef FEATURE_STAIRS_T_H
#define FEATURE_STAIRS_T_H


#include "feature_peer_t.h"

#ifndef CSTRING_T_H
#include "primitives/cstring_t.h"
#endif


/**
 * Stairs feature.
 * @author Hj. Malthaner
 */
class feature_stairs_t : public feature_peer_t 
{
private:

  /**
   * Destination of the stairs
   * @author Hj. Malthaner
   */
  cstring_t dest;

public:

  feature_stairs_t();
  feature_stairs_t(const cstring_t & d);


  /**
   * Uses the stairs.
   * @author Hj. Malthaner
   */
  virtual bool activate(handle_tpl <thing_t> thing);


  // ---- implements persistent_t ----
  
  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);

  
  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();
};                  

#endif
