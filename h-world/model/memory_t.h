/* 
 * memory_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef memory_t_h
#define memory_t_h

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

class pprops_t;
class storage_t;
class iofile_t;

/**
 * Player memory. Contains information about memoried items.
 * @author Hj. Malthaner
 */
class memory_t  : public persistent_t
{
 private:

  /**
   * Lifetime is same as memories lifetime.
   * @author Hj. Malthaner
   */
  pprops_t * known_items;

 public:


  memory_t();

  ~memory_t();


  bool is_item_known(const char * ident) const;


  /**
   * Memorize item 'ident'
   * @author Hansj�rg Malthaner
   */     
  void set_item_known(const char * ident);


  /**
   * Memorize a message
   * @author Hansj�rg Malthaner
   */     
  void memorize(const char * catg, const char * msg);


  /**
   * Recalls the nth message
   * @return NULL if no such message exists
   * @author Hansj�rg Malthaner
   */     
  const char * recall(const char * catg, int n);


  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  void read_write(storage_t *store, iofile_t * file);

  /**
   * Gets a unique ID for this objects class, must be overriden by subclass.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};

#endif // memory_t_h
