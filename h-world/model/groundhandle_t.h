#ifndef groundhandle_t_h
#define groundhandle_t_h
                
// #include "tpl/handle_tpl.h"

class ground_t;
template <class T> class handle_tpl;

typedef handle_tpl<ground_t> groundhandle_t;

#endif
