/* 
 * dice_t.h
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef dice_t_h
#define dice_t_h


/**
 * Data model of a group of dice plus a constant "2d3+10"
 * @author Hj. Malthaner
 */
class dice_t
{
 public:

  dice_t() {dice = 0; sides = 0; offset=0;};

  dice_t(unsigned char d, unsigned char s, unsigned char o) {
    dice = d; sides = s; offset = o;
  };


  /**
   * Parses XdY+C format
   * @author Hj. Malthaner
   */
  dice_t(const char * desc);


  /**
   * Number of dice
   * @author Hj. Malthaner
   */
  unsigned char dice;

  /**
   * Number of sides
   * @author Hj. Malthaner
   */
  unsigned char sides;


  /**
   * Constant offset
   * @author Hj. Malthaner
   */
  unsigned char offset;


  /**
   * Roll the dice and return the sum
   * @author Hj. Malthaner
   */
  int roll();
};

#endif // dice_t_h
