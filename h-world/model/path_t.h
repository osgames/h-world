/* 
 * path_t.h
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef path_t_h
#define path_t_h

#ifndef koord_h
#include "primitives/koord.h"
#endif

class level_t;
class thing_t;
class storage_t;
class iofile_t;
template <class T> class handle_tpl;

/**
 * Checks if we've reached the destination spot
 * @author Hj. Malthaner
 */
class dest_cond_t
{
 public:

  virtual bool am_i_there(koord k) const = 0;
};


class look_for_room_t : public dest_cond_t
{
  level_t * level;
  int room_type;
 public:

  look_for_room_t(level_t * level, int room_type);
  virtual bool am_i_there(koord k) const;
};


class look_for_pos_t : public dest_cond_t
{
  level_t * level;
  koord dest;
 public:

  look_for_pos_t(level_t * level, koord dest);
  virtual bool am_i_there(koord k) const;
};


/**
 * Paths - routes between two point of a level
 * @author Hj. Malthaner
 */
class path_t
{
 private:

  koord steps [256];
  unsigned int size;

 public:

  /**
   * Current position on path
   * @author Hansj�rg Malthaner
   */
  int n;


  /**
   * Players may click on occupied sqaures. They still want to
   * go there and trigger an action. To allow this, set this
   * flag to true
   * @author Hansj�rg Malthaner
   */
  unsigned char dont_check_last;


  koord get_step(unsigned n) const {
    return n < size ? steps[n] : koord(-1,-1);
  };

  unsigned int get_size() const {return size;};

  path_t();


  /**
   * Finds shortest path from start to dest. 
   * Paths cannot be longer that 255 sqaures. Caller is responsible to delete
   * the path after usage.
   *
   * @author Hansj�rg Malthaner
   */
  bool calculate(level_t * level, const handle_tpl <thing_t> & thing,
		      const koord start, dest_cond_t * dest);



  /**
   * Stores/restores the object.
   * @author Hj. Malthaner
   */
  void read_write(storage_t *store, iofile_t * file);

};

#endif // path_t_h
