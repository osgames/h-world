


/*
 * Constants to access properties of things. Centralized to have one place to
 * see them all and only one place to modify.
 *
 * Hj. Malthaner
 */


#define TH_NAME          "name"

#define TH_TYPE          "type"
#define TH_SUBTYPE       "subtype"

#define TH_IS_PLAYER     "is_player"

#define TH_FLAVOR        "flavor"
#define TH_KIND          "kind"

// Sum of all bulk the player is wearing
#define TH_HINDERANCE    "hinderance"

#define TH_HP_MAX        "HPmax"
#define TH_HP_CURR       "HPcurr"

#define TH_SCORE         "score"

#define TH_BASE_SPEED    "base_speed"

#define TH_QUICK_INV     "inventory.quick"

// cut damage modificator
#define TH_C_DAMAGE      "Cdamage"

// blunt damage modificator
#define TH_B_DAMAGE      "Bdamage"

// piercing ability
#define TH_PIERCE        "pierce"

#define TH_C_PROTECTION  "Cprotection"

#define TH_B_PROTECTION  "Bprotection"

#define TH_BLOCK         "block"

#define TH_LIGHT_RAD     "light_rad"

#define TH_LOCKED        "locked"

