/* 
 * memory_t.cpp
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "memory_t.h"
#include "model/pprops_t.h"
#include "util/debug_t.h"
#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"

memory_t::memory_t()
{
  known_items = new pprops_t();
}

memory_t::~memory_t()
{
  delete known_items;
  known_items = 0;
}


bool memory_t::is_item_known(const char * ident) const
{
  /*
  const bool ok = (known_items->get_int(ident) != 0);

  dbg->message("memory_t::is_item_known()", "remembering '%s' -> %d", 
	       ident, ok);

  return ok;
  */

  return (known_items->get_int(ident) != 0);
}


/**
 * Memorize item 'ident'
 * @author Hansj�rg Malthaner
 */     
void memory_t::set_item_known(const char * ident)
{
  dbg->message("memory_t::set_item_known()", "memorizing '%s'", ident);

  known_items->set(ident, 1);
}


/**
 * Memorize a message
 * @author Hansj�rg Malthaner
 */     
void memory_t::memorize(const char * catg, const char * msg)
{
  char buf [256];

  int i = 0;

  // Hajo: find free slot
  while(recall(catg, i)) {
    i++;
  }

  sprintf(buf, "_%s-%d", catg, i);
 
  known_items->set(buf, msg);
}


/**
 * Recalls the nth message
 * @return NULL if no such message exists
 * @author Hansj�rg Malthaner
 */     
const char * memory_t::recall(const char * catg, int n)
{
  char buf [256];
  sprintf(buf, "_%s-%d", catg, n);

  return known_items->get_string(buf);
}


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void memory_t::read_write(storage_t *store, iofile_t * file)
{
  if(file->is_saving()) {
    store->store(known_items, file);
  } else {
    known_items = (pprops_t *) store->restore(file, hstore_t::t_pprops);
    // known_items->dump();
  }
}


/**
 * Gets a unique ID for this objects class, must be overriden by subclass.
 * @author Hj. Malthaner
 */
perid_t memory_t::get_cid()
{
  return hstore_t::t_memory;
}
