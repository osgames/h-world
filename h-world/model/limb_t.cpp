/* Copyright by Hj. Malthaner */

#include <string.h>

#include "thing_t.h"
#include "limb_t.h"



void limb_t::add_limb(limb_t *limb)
{
  if(limb) {
    limbs.insert(limb);
  }
}

void limb_t::set_type(const char *type)
{
  if(type) {
    this->type = strdup(type);
  } else {
    this->type = NULL;
  }
}


void limb_t::set_subtype(const char *type)
{
  if(subtype) {
    this->subtype = strdup(subtype);
  } else {
    this->subtype = NULL;
  }
}


void limb_t::set_name(const char * name) {
  this->name = strdup(name);
}

thing_t * limb_t::get_thing() {
    return thing;
}


void limb_t::set_thing(thing_t * thing) {
  this->thing = thing;

  // tell our thing that we have changed
  root->body_changed();
}


limb_t::limb_t(thing_t *root)
{
  name = NULL;
  thing = NULL;

  type = NULL;
  subtype = NULL;

  this->root = root;
}


limb_t::~limb_t()
{
  free((void *)name);
  free((void *)type);
  free((void *)subtype);
}
