/* 
 * square_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "square_t.h"
#include "ground_t.h"
#include "thing_t.h"
#include "feature_t.h"
#include "feature_peer_t.h"

#include "control/trigger_t.h"

#include "util/debug_t.h"
#include "util/rng_t.h"

#include "view/player_visual_connector_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"



/**
 * Sets a trigger on this square
 * @author Hj. Malthaner
 */     
void square_t::set_trigger(trigger_t *t)
{
  if(trigger) {
    delete trigger;
  }

  trigger = t;
}


/**
 * Removes the trigger on this square
 * @return the old trigger
 * @author Hj. Malthaner
 */     
trigger_t * square_t::remove_trigger()
{
  trigger_t * result = trigger;
  trigger = 0;

  return result;
}


void square_t::set_feature(const featurehandle_t & feature)
{
  this->feature = feature;

  set_flag(dirty);
}	


void square_t::set_ground(const groundhandle_t &ground) {
  this->ground=ground;

  set_flag(dirty);
}


void square_t::add_constraint(square_constraint_t *c) 
{
  // constraints.insert(c);
  constraints.append(c);
}


/**
 * checks if that thing can enter this sqaure (constraints etc)
 * @author Hj. Malthaner
 */     
bool square_t::can_enter(const thinghandle_t &thing) const
{
  // check for obstacles

  bool ok = 
    (!get_feature().is_bound() || 
     !get_feature()->is_blocking()); 


  // check for enemies
  if(ok) {
    // count animated things
    if(things.count() > 0) {
      int anim = 0;

      minivec_iterator_tpl <thinghandle_t> iter ( things );

      while(ok && iter.next()) {
	anim += iter.get_current()->get_actor().is_animated();

	ok &= *iter.get_current()->get_string("block_move", "f") == 'f';
      }

      anim += thing->get_actor().is_animated();

      // only one animated being allowed per sqaure
      ok &= (anim <= 1);
    }    
  }


  return ok;
}


/**
 * checks if that thing can traverse this square
 * @author Hj. Malthaner
 */     
bool square_t::can_traverse(const thinghandle_t &thing) const
{
  // Hajo: check for obstacles (features)

  bool ok = true;

  if(feature.is_bound()) {
    if(feature->is_blocking() && 
       ((feature->get_flags() & feature_t::F_DOOR) == 0)) {
      ok = false;
    }
  }


  // Hajo: check for obstacles (things)
  if(ok) {
    if(things.count() > 0) {
      minivec_iterator_tpl <thinghandle_t> iter ( things );
      while(ok && iter.next()) {
	ok &= *iter.get_current()->get_string("block_move", "f") == 'f';
	ok &= *iter.get_current()->get_string("immobile", "f") == 'f';
      }
    }    
  }
  
  return ok;
}


/**
 * Adds a thing to this sqaure. Sets thing position to k.
 * @param k new position of the thing
 * @author Hj. Malthaner
 */
void square_t::add_thing(thinghandle_t thing, const koord & k)
{
  if(thing.is_bound()) {
    thing->set_pos(k);

    if(things.contains(thing) == false) {

      things.append(thing);

      if(trigger && trigger->is_active()) {

	if(trigger->get_type() & 128) {
	  // One-time trigger, player only

	  if(thing.is_bound() && *thing->get_string("is_player", "f") == 't') {
	    trigger->activate(thing);
	    set_trigger(0);
	  }
	}
	else {
	  trigger->activate(thing);
	}
      }

      set_flag(dirty);
    } else {
      dbg->warning("square_t::add_thing()", "thing is added twice!");     
    }

  } else {
    dbg->error("square_t::add_thing()", "thinghandle isn't bound!");     
  }
}


bool square_t::remove_thing(thinghandle_t thing)
{
  set_flag(dirty);
  return things.remove(thing);
}


/**
 * Basic constructor.
 * @author Hj. Malthaner
 */
square_t::square_t()
{
  flags = dirty;
  trigger = 0;
  room_type = undefined;
  hots_type = 0;
}


square_t::~square_t()
{
  delete trigger;
  trigger = 0;
}


// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void square_t::read_write(storage_t *store, iofile_t * file)
{
  env.read_write(store, file);
  file->rdwr_uchar(room_type);
  file->rdwr_uchar(hots_type);

  if(file->is_saving()) {
    minivec_iterator_tpl <thinghandle_t> thing_iter (things);
    unsigned char n = things.count();
    file->rdwr_uchar(n);

    while(thing_iter.next()) {
      store->store(thing_iter.get_current().get_rep(), file);
    }

    store->store(ground.get_rep(), file);

    minivec_iterator_tpl <square_constraint_t *> c_iter (constraints);
    n = constraints.count();
    file->rdwr_uchar(n);

    while(c_iter.next()) {
      // XXX todo
      // store->store(c_iter.get_current(), file);
    }

    store->store(feature.get_rep(), file);

  } else {
    // read from file

    unsigned char i,n;
    file->rdwr_uchar(n);

    for(i=0; i<n; i++) {
      thinghandle_t thing ((thing_t *)store->restore(file, hstore_t::t_thing));
      things.append(thing);
      player_visual_connector_t::calc_overlays(thing);
    }

    ground = groundhandle_t((ground_t *)store->restore(file, hstore_t::t_ground));

    file->rdwr_uchar(n);

    for(i=0; i<n; i++) {
      // XXX todo
      // constraints.append((square_constraint_t *)store->restore(file)));
    }

    feature_t * f = (feature_t *)store->restore(file, hstore_t::t_feature);
    if(f) {
      feature = featurehandle_t(f);
    }
  }
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t square_t::get_cid()
{
  return hstore_t::t_square;
}
