#include "height_level_t.h"

#include "model/level_t.h"
#include "model/ground_t.h"
#include "view/ground_visual_t.h"
#include "util/perlin_t.h"

#ifndef MIN 
#define MIN(a,b)  (a) < (b) ? (a) : (b)
#endif



unsigned char height_level_t::get_height(koord k) const
{
  // printf("w=%d h=%d x=%d y=%d\n", size.x, size.y, k.x, k.y);

  if(k.x >= 0 && k.y >= 0 && k.x <= size.x && k.y <= size.y) {
    return height[k.x + (size.x+1)*k.y];
  } else {
    return 128;
  }
}


void height_level_t::set_height(koord k, unsigned char h)
{
  // printf("Setting height at %d %d to %d, w=%d, h=%d\n", k.x, k.y, h, size.x, size.y);


  if(k.x >= 0 && k.y >= 0 && k.x <= size.x && k.y <= size.y) {

    height[k.x + (size.x+1)*k.y] = h;
  }
}


int height_level_t::calc_slope(koord k) const
{
  if(k.x >= 0 && k.y >= 0 && k.x < size.x && k.y < size.y) {

    const int h1 = get_height(k);
    const int h2 = get_height(k+koord(1, 0));
    const int h3 = get_height(k+koord(1, 1));
    const int h4 = get_height(k+koord(0, 1));
    
    const int mini = MIN(MIN(h1,h2), MIN(h3,h4));
    
    return 
      ((h1>mini)<<3) +
      ((h2>mini)<<2) +
      ((h3>mini)<<1) +
      ((h4>mini));
  } else {
    return 0;	// Annahme: ausserhalb der Karte ist alles Flach
  }
}


int height_level_t::calc_shade(koord k)
{
  unsigned char href = get_height(k);

  int bright = bright_base;

  bright += (href - get_height(k+koord( 1,0))) * -strength;
  bright += (href - get_height(k+koord(-1,0))) * +strength;

  bright += (href - get_height(k+koord( 0, 1))) * -strength;
  bright += (href - get_height(k+koord( 0,-1))) * +strength;

  
  return bright;
}


void height_level_t::raise_clean(koord k, unsigned char h)
{
  if(k.x >= 0 && k.y >= 0 && k.x <= size.x && k.y <= size.y) {
    
    if(get_height(k) < h) {
      set_height(k, h);
      
      raise_clean(koord(k.x-1, k.y-1), h-1);
      raise_clean(koord(k.x  , k.y-1), h-1);
      raise_clean(koord(k.x+1, k.y-1), h-1);
      raise_clean(koord(k.x-1, k.y ) , h-1);
      // Punkt selbst hat schon die neue Hoehe
      raise_clean(koord(k.x+1, k.y ) , h-1);
      raise_clean(koord(k.x-1, k.y+1), h-1);
      raise_clean(koord(k.x  , k.y+1), h-1);
      raise_clean(koord(k.x+1, k.y+1), h-1);
    }
  }
}


/**
 * Ensures height differences:  d <= 1
 * @author Hj. Malthaner 
 */
void height_level_t::cleanup_karte()
{
  unsigned char *hgts = new unsigned char [(size.x+1)*(size.y+1)];
  unsigned char *p = hgts;
  koord k;

  // printf("cleanup karte called\n");

  for(k.y=0; k.y<=size.y; k.y++) {
    for(k.x=0; k.x<=size.x; k.x++) {
      *p++ = get_height(k);
    }
  }

  p=hgts;

  for(k.y=0; k.y<=size.y; k.y++) {
    for(k.x=0; k.x<=size.x; k.x++) {
      raise_clean(k, (*p++)+1);
    }
  }

  delete[] hgts;

  // printf("cleanup karte done\n");
}



height_level_t::height_level_t(int bb, 
			       int s, 
			       koord size,
			       double persistence,
			       double amplitude) : size(size)
{
  height = new unsigned char [(size.x+1)*(size.y+1)];

  ground_base = 240;
  bright_base = bb;
  strength = s;

  perlin_t p;
  koord k;

  // printf("w=%d h=%d p=%f a=%f\n", size.x, size.y, persistence, amplitude);

  double mini = +999;
  double maxi = -999;

  for(k.y=0; k.y<=size.y; k.y++) {
    for(k.x=0; k.x<=size.x; k.x++) {
      const double n = 128.0 + p.noise_2D(k.x, k.y, persistence)*amplitude;

      if(n < mini) mini=n;
      if(n > maxi) maxi=n;

      set_height(k, n < 0 ? 0 : n > 255 ? 255 : (unsigned char)n);
    }
  }

  // printf("mini=%f maxi=%f\n", mini, maxi);

  // cleanup_karte();
}


height_level_t::~height_level_t()
{
  delete [] height;
  height = 0;

  // printf("height level destroyed\n");
}


/**
 * Sets border height to 128. Smoothes adjacent tiles
 * @author Hj. Malthaner 
 */
void height_level_t::equalize_borders()
{
  const int M = 8;

  for(int n=0; n<M; n++) {


    for(int i = n; i<size.x-n; i++) {
      const int h = (128*(M-n) + ((int)get_height(koord(i, n))) * n) / M;

      //  printf("n=%d i=%d h=%d\n", n, i, h);

      set_height(koord(i, n), h);

      set_height(koord(n, i), 
		 (128*(M-n) + ((int)get_height(koord(n, i))) * n) / M);

      
      set_height(koord(i, size.x-1-n), 
		 (128*(M-n) + get_height(koord(i, size.x-1-n)) * n) / M);

      set_height(koord(size.x-1-n, i), 
		 (128*(M-n) + get_height(koord(size.x-1-n, i)) * n) / M);
    }
  }
}
