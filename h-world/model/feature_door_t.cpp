/* 
 * feature_door_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "featurehandle_t.h"
#include "feature_door_t.h"
#include "world_t.h"
#include "square_t.h"
#include "level_t.h"
#include "thing_t.h"
#include "feature_t.h"

#include "language/linguist_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"


feature_door_t::feature_door_t()
{
  is_open = false;
  open_set = 0;
  closed_set = 0;
  open_image = 0;
  closed_image = 0;
}


feature_door_t::feature_door_t(koord pos, 
			       int oset, int open, 
			       int cset, int closed,
			       int open_x, int open_y)
{
  is_open = false;

  door_pos = pos;

  open_set = oset;
  closed_set = cset;

  open_image = open;
  closed_image = closed;

  open_x_off = open_x;
  open_y_off = open_y;
}

feature_door_t::feature_door_t(koord pos, 
			       const char * open, const char * closed, 
			       int open_x, int open_y)
{
  
}


/**
 * Opens/closes this door.
 * @author Hj. Malthaner
 */
bool feature_door_t::activate(thinghandle_t thing)
{
  is_open = !is_open;

  level_t *level = world_t::get_instance()->get_level();
  square_t *square = level->at(door_pos);
  featurehandle_t feature = square->get_feature();

  visual_t & visual = feature->visual;

  bool ok = true;

  if(is_open) {
    visual.set_image(0, open_image, open_set, 0, 0, 0, false);
    visual.set_x_off(open_x_off);
    visual.set_y_off(open_y_off);
    
    feature->set_blocking(false);
    feature->visual.set_opaque(false);
  }
  else {
    if(square->get_things().count() == 0) {
      visual.set_image(0, closed_image, closed_set, 0, 0, 0, false);
      visual.set_x_off(0);
      visual.set_y_off(0);

      feature->set_blocking(true);
      feature->visual.set_opaque(true);
    } else {
      if(thing->get_string("is_player", 0)) {
	linguist_t::translate("Something is blocking the door.");
      }
      ok = false;
    }
  }

  return ok;
}


// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void feature_door_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_short(door_pos.x);
  file->rdwr_short(door_pos.y);

  file->rdwr_char(is_open);
  
  file->rdwr_uchar(open_set);
  file->rdwr_uchar(closed_set);

  file->rdwr_ushort(open_image);
  file->rdwr_ushort(closed_image);
  
  file->rdwr_short(open_x_off);
  file->rdwr_short(open_y_off);
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t feature_door_t::get_cid()
{
  return hstore_t::t_feature_door;
}
