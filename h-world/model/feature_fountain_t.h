#ifndef feature_fountain_t_h
#define feature_fountain_t_h

#include "feature_peer_t.h"

/**
 * Fountains fill bottles with water
 * @author Hj. Malthaner
 */
class feature_fountain_t : public feature_peer_t
{

 public:


  /**
   * Fountains fill bottles with water
   * @author Hj. Malthaner
   */
  virtual bool activate(handle_tpl <thing_t> thing);


  // ---- implements persistent_t ----
  
  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);

  
  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};

#endif // feature_fountain_t_h
