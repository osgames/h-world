/* 
 * level_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef LEVEL_T_H
#define LEVEL_T_H

#include "util/fov_source_t.h"
#include "util/fov_destination_t.h"


#ifndef koord_h
#include "primitives/koord.h"
#endif


#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

class visual_t;
class square_t;
class thing_t;
class pprops_t;
template <class T> class minivec_tpl;
template <class T> class slist_tpl;
template <class T> class handle_tpl;

/**
 * Data model of a level.
 * @author Hansj�rg Malthaner
 */
class level_t : public persistent_t, public fov_source_t, fov_destination_t
{
private:

  /**
   * Size of this level.
   * @author Hansj�rg Malthaner
   */
  koord size;


  /**
   * The sqaures which make up this level
   * @supplierCardinality 0..* 
   */
  square_t *squares;


  /**
   * Viewable grids
   * @author Hansj�rg Malthaner
   */
  unsigned char *viewable;


  /**
   * Histogramm of partition sizes
   * @author Hansj�rg Malthaner
   */
  slist_tpl <int> * partition_sizes;


  /**
   * Fake 2D array of partition markers (numbers)
   * @author Hansj�rg Malthaner
   */
  short *partition_marker;


  signed char * lights;


  /**
   * Partitioning helper function. Called recursively.
   * @return number of connected sqaures.
   * @author Hansj�rg Malthaner
   */
  int fill(koord k);



  /**
   * Level properties. Aggregated object, gets deleted if level
   * is deleted.
   * @author Hansj�rg Malthaner
   */
  pprops_t *props;


  /**
   * Is this level lit?
   * @author Hansj�rg Malthaner
   */
  bool lit;


public:

  void set_light(koord k, int l);
  int get_light(koord k) const {return lights[k.y*(size.x+1) + k.x];};


  const koord & get_size() const {return size;};

  bool is_lit() const {return lit;};

  unsigned char is_viewable(const koord k) const {
    return k.x >= 0 && k.x < size.x && 
           k.y >= 0 && k.y < size.y && 
           viewable[k.x+k.y*size.x];
  };


  void set_properties(pprops_t *p);
  pprops_t * get_properties() {return props;};


  /**
   * Level square accessor.
   * @author Hansj�rg Malthaner
   */
  square_t *at(const koord pos) const;


  /**
   * Tries to add thing near the given location. If not posible,
   * adds the thing at location pos.
   * @author Hansj�rg Malthaner
   */
  void add_thing_near(koord pos, handle_tpl < thing_t > thing);


  level_t();

  level_t(koord size);
  
  ~level_t();


  /**
   * Finds connected areas in thelevel. Fills the partition_sizes list
   * and partition_count value.
   * @return number of partitions
   * @author Hansj�rg Malthaner
   */
  int create_partitions();


  int get_largest_partition();


  int partition_at(const koord pos) const {
    if(pos.x>=0 && pos.x<size.x && pos.y>=0 && pos.y<size.y) {
      return partition_marker[pos.y*size.x+pos.x];
    } else {
      return -1;
    }
  }


  /**
   * Finds a list of empty areas of size in given partition. The areas
   * may overlap! At most 255 areas are returned.
   * @author Hansj�rg Malthaner
   */
  minivec_tpl <koord> * get_empty_area(koord tl,
				       koord br,
				       koord area_size,
				       bool noitems,
				       int partition, 
				       int max) const;


  /**
   * Scans a quadrant. Fills list with all things found.
   *
   * @param list the list to fill
   * @param k_base orgin of scan
   * @param d distance to scan
   * @param dir direction (0=north, 1=east, 2=south, 3=west)
   * @author Hansj�rg Malthaner
   */
  void scan_quadrant(slist_tpl < handle_tpl < thing_t > > *list, 
		     koord k_base,
		     int d, int dir);

  /**
   * Moves a thing along a line from src to dst, until an obstacle is hit
   * or dst is reached.
   *
   * @author Hansj�rg Malthaner
   */
  void project(koord src, koord dst, handle_tpl < thing_t > what);


  /**
   * Calculate visibility information
   * @author Hj. Malthaner
   */
  void recalculate_fov(koord ij_off, int light_rad);


  // ---- implements fov_source_t ----

  /**
   * Implements fov_source_t
   * @author Hj. Malthaner
   */
  virtual bool is_blocking(koord pos);


  /**
   * Implements fov_destination_t
   * @author Hj. Malthaner
   */
  virtual void set_viewable(koord pos);   


  // ---- implements persistent_t ----

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);
  

  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};

#endif //LEVEL_T_H
