/* Copyright by Hj. Malthaner */

#ifndef LIMB_T_H
#define LIMB_T_H

#include "slist_tpl.h"
#include "swt/koord.h"

class thing_t;

/**
 * This are body parts. A limb 
 * may hold up to one thing.
 *
 * @author Hj. Malthaner
 */
class limb_t {
private:

    /**
     * The name will be used to determine which things fit here.
     *
     * @author Hj. Malthaner
     */
    const char * name;


    /**
     * The thing which is held by this bodfy part.
     *
     * @author Hj. Malthaner
     * @supplierRole item
     */
    thing_t *thing;


    /**
     * The thing to which body this limb belongs.
     *
     * @author Hj. Malthaner
     * @supplierRole item
     */
    thing_t *root;


    slist_tpl <limb_t *> limbs;

    koord display_position;

    int direction;
    int distance;

    const char *type;
    const char *subtype;


    /**
     * Max size of the thing this limb can hold.
     * @author Hj. Malthaner
     */
    koord hold_size;

public:


    void set_type(const char *type);
    void set_subtype(const char *subtype);


    int get_direction() const {return direction;};
    void set_direction(int d) {direction = d;};

    int get_distance() const {return distance;};
    void set_distance(int d) {distance = d;};


    koord get_hold_size() const {return hold_size;};
    void set_hold_size(koord size) {hold_size = size;};


    const slist_tpl <limb_t *> & get_limbs() const {return limbs;};

    koord get_display_position() const {return display_position;};
    void set_display_position(koord pos) {display_position = pos;};
    

    void add_limb(limb_t *);

    void set_name(const char * name);
    const char * get_name() const {return name;};

    void set_thing(thing_t * thing);
    thing_t * get_thing();


    limb_t(thing_t *root);
    ~limb_t();


};
#endif //LIMB_T_H
