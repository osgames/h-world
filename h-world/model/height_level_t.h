#ifndef height_level_t_h
#define height_level_t_h

#include "primitives/koord.h"


class height_level_t
{
  unsigned char *height;
  int ground_base;
  int bright_base;
  int strength;

  void raise_clean(koord k, unsigned char h);


  /**
   * Ensures height differences:  d <= 1
   * @author Hj. Malthaner 
   */
  void cleanup_karte();


 public:


  int calc_shade(koord k);


  /**
   * Reference to parents size
   */
  const koord size;

  height_level_t(int bright_base, 
		 int strength, 
		 koord size,
		 double persistence,
		 double amplitude);

  ~height_level_t();


  unsigned char get_height(koord k) const;
  void set_height(koord k, unsigned char h);

  
  /**
   * Sets border height to 128. Smoothes adjacent tiles
   * @author Hj. Malthaner 
   */
  void equalize_borders();



  int calc_slope(koord k) const;
};

#endif // height_level_t_h
