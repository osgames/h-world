/* 
 * inventory_t.h
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef INVENTORY_T_H
#define INVENTORY_T_H

#ifndef koord_h
#include "primitives/koord.h"
#endif

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

#include "model/thinghandle_t.h"


class inventory_listener_t;
template <class T> class slist_tpl;


/**
 * An inventory is a 2D storage area.
 * @author Hj. Malthaner
 */
class inventory_t : public persistent_t {
private:

    /**
     * Checks if pos is inside inventory area. 
     * @author Hj. Malthaner
     */
    bool is_valid(koord pos) const;


    /**
     * Tries to add the thing at a specific location into the inventory.
     * @return true if successful, false otherwise
     * @author Hj. Malthaner
     */
    bool add_at_with_size(thinghandle_t thing, koord pos, koord size);


    /**
     * @supplierCardinality 0..* 
     * @author Hj. Malthaner
     */
    thinghandle_t *things;


    thing_t *owner;


    /**
     * Top-left entry is root of thing
     * @author Hj. Malthaner
     */     
    unsigned char *roots;


    /**
     * the size of this inventory
     * @author Hj. Malthaner
     */
    koord size;


    /**
     * Flag, telling if the player can use items directly from
     * this inventory
     * @author Hj. Malthaner
     */
    unsigned char usage_allowed;


    slist_tpl <inventory_listener_t *> * listeners;

    void fire_thing_added(thinghandle_t &t);
    void fire_thing_removed(thinghandle_t &t);

public:

    unsigned char is_root(koord pos) const {return roots[pos.x + size.x*pos.y];};

    void set_usage_allowed(bool yesno);
    bool is_usage_allowed() const;


    koord get_size() const {return size;};
    thing_t * get_owner() const {return owner;};


    /**
     * Adds a listener (remove, add events). Double calls are detected
     * and listeners only added once.
     * @author Hj. Malthaner
     */
    void add_listener(inventory_listener_t *l);


    /**
     * Constructor. Creates an inventory of specified size.
     * @author Hj. Malthaner
     */
    inventory_t(koord size, thinghandle_t & owner);


    /**
     * Destructor. Deletes the thing array, but not the things.
     * @author Hj. Malthaner
     */
    ~inventory_t();


    /**
     * Calculates the total weight
     * @author Hj. Malthaner
     */
    int calc_weight();


    /**
     * Check if given inventory area is empty
     * @author Hj. Malthaner
     */
    bool is_empty(koord pos, koord tsize) const;


    /**
     * Tries to add the thing anywhere into the inventory.
     * @return true if successful, false otherwise
     * @author Hj. Malthaner
     */
    bool add(thinghandle_t thing);


    /**
     * Tries to add the thing anywhere into the inventory.
     * If it doesn't fit, drop to ground at the location of
     * the inventories owner
     * @return true if added, false if overflow
     * @author Hj. Malthaner
     */
    bool add_or_overflow(thinghandle_t thing);


    /**
     * Tries to add the thing at a specific location into the inventory.
     * @return true if successful, false otherwise
     * @author Hj. Malthaner
     */
    bool add_at(thinghandle_t thing, koord pos);


    /**
     * @return a pointer to the thing located at pos, or NULL if there is
     * no thing at pos
     * @author Hj. Malthaner
     */
    thinghandle_t get_from(koord pos);


    /**
     * same as get_from() but automatically removes the thing from
     * the inventory
     * @author Hj. Malthaner
     */
    thinghandle_t  take_from(koord pos);


    /**
     * removes the thing from the inventory
     * @author Hj. Malthaner
     */
    bool remove(thinghandle_t thing);


    /**
     * Fill list with things from this inventory
     * @author Hj. Malthaner
     */
    void list_things(slist_tpl < thinghandle_t > *list);


    // ---- implements persistent_t ----

    inventory_t() {};

    /**
     * Stores/restores the object. Must be overridden by subclass.
     * This method is supposed to call store->store() on all associated
     * objects!
     * @author Hj. Malthaner
     */
    virtual void read_write(storage_t *store, iofile_t * file);


    /**
     * Gets a unique ID for this objects class.
     * @author Hj. Malthaner
     */
    virtual perid_t get_cid();

};
#endif //INVENTORY_T_H
