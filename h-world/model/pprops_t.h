#ifndef PPROPS_T_H
#define PPROPS_T_H

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

#ifndef PROPERTIES_T_H
#include "util/properties_t.h"
#endif

/**
 * Persistent properties implementation
 * @author Hj. Malthaner
 */     
class pprops_t : public persistent_t, public properties_t
{

 public:

  pprops_t();


  // ---- implements persistent_t ----

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);

  
  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();


  /**
   * Debugging, dump contents
   */
  void dump();
};

#endif // pprops_t_h
