#include "map_model_t.h"

#include "swt/graphics_t.h"
#include "swt/image_t.h"
#include "swt/color_t.h"
#include "swt/system_window_t.h"

#include "view/world_view_t.h"

#include "model/world_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/feature_t.h"
#include "model/thing_t.h"

#include "model/feature_stairs_t.h"
#include "model/feature_fountain_t.h"


map_model_t * map_model_t::single_instance = 0;


map_model_t * map_model_t::get_instance()
{
  if(single_instance == 0) {
    single_instance = new map_model_t();
  }
  
  return single_instance;
}


map_model_t::map_model_t()
{
  img = 0;  
}


map_model_t::~map_model_t()
{
  delete img;
  img = 0;
}



void map_model_t::update(koord k)
{
  static color_t color_orange (255, 160, 32);
  static color_t color_red (160, 64, 32);
  static color_t color_green (64, 128, 32);
  static color_t color_blue (64, 128, 255);
  static color_t color_brown (128, 96, 32);

  if(world_t::get_instance()) {

    level_t * level = world_t::get_instance()->get_level();

    if(img && level) {

      world_view_t * view = world_view_t::get_instance();
      graphics_t * gr = img->get_graphics();
      const koord p = k*2;
      
      
      if(view->is_remembered(k)) {
	
	square_t *square = level->at(k);

	featurehandle_t feat = square->get_feature();
	
	if(feat.is_bound()) {
	  unsigned int flags = feat->get_flags();

	  if((flags & feature_t::F_DOOR) ||
	     (flags & feature_t::F_DOORPOST)) {
	    gr->fillbox_wh(p.x, p.y, 2, 2, color_brown);
	  } else if(feat->get_peer().is_bound() && 
		    dynamic_cast <feature_fountain_t *> (feat->get_peer().get_rep())) {
	    gr->fillbox_wh(p.x, p.y, 2, 2, color_blue);
	  } else if(feat->get_peer().is_bound() && 
		    dynamic_cast <feature_stairs_t *> (feat->get_peer().get_rep())) {
	    gr->fillbox_wh(p.x, p.y, 2, 2, color_orange);
	  } else if(feat->is_blocking() == false) {
	    gr->fillbox_wh(p.x, p.y, 2, 2, color_t::GREY96);
	  } else {
	    gr->fillbox_wh(p.x, p.y, 2, 2, color_t::BLACK);
	  }

	}
	else {
	  gr->fillbox_wh(p.x, p.y, 2, 2, color_t::GREY160);
	}
	
	const minivec_tpl <thinghandle_t> & things = square->get_things();
	int n = things.count();

	if(n > 0) {
	  // Max 4 things can be shown
	  if(n > 4) {
	    n = 4;
	  }

	  for(int i=0; i<n; i++) { 
	    if(things.get(i)->get_actor().is_animated()) {
	      gr->draw_pixel(p.x + (i & 1), p.y + (i>>1), color_red);
	    } else {
	      gr->draw_pixel(p.x + (i & 1), p.y + (i>>1), color_green);	      
	    }
	  }
	}
      } else {
	// unknown sqaure
	gr->fillbox_wh(p.x, p.y, 2, 2, color_t::GREY96);
      }
    }
  }
}


void map_model_t::init()
{
  if(world_t::get_instance()) {

    level_t * level = world_t::get_instance()->get_level();
    
    if(level) {
      const koord size = level->get_size();
      
      delete img;
      img = new image_t(size.x*2, size.y*2, 
			system_window_t::get_instance()->get_graphics()->get_colortype());
      
      koord k;
      
      for(k.y=0; k.y<size.y; k.y++) { 
	for(k.x=0; k.x<size.x; k.x++) {
	  update(k);
	}
      }
    }
  }
}

