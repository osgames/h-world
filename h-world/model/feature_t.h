/* Copyright by Hj. Malthaner */

#ifndef FEATURE_T_H
#define FEATURE_T_H

class feature_t;

#ifndef tpl_handle_tpl_h
#include "tpl/handle_tpl.h"
#endif

#ifndef featurepeerhandle_t_h
#include "featurepeerhandle_t.h"
#endif

#ifndef VISUAL_T_H
#include "view/visual_t.h"
#endif

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

class graphics_t;
class properties_t;

class thing_t;
template <class T> class handle_tpl;

class feature_t : public persistent_t {
public:

  enum flags {
    BLOCKING=1, 
    F_OTHER=2, 
    F_NINEPARTWALL=4, 
    F_DOORPOST=8, 
    F_DOOR=16
  };

private:

  featurepeerhandle_t peer;

  const properties_t * props;

  unsigned char flags;

public:    

  /**
   * Player digging counter. Special value: 255 = can't be dug away
   * @author Hj. Malthaner
   */
  unsigned char tunnel;

  
  /**
   * Light radius, 0 means does not emit light
   * Read-only!
   * @author Hj. Malthaner
   */
  unsigned char light_rad;


  visual_t visual;


  void set_flag(enum flags flag);
  void clr_flag(enum flags flag);

  unsigned int get_flags() const {return flags;};
  bool get_flag(enum flags flag) const { return flags & flag;};


  bool is_tunnelable() const {return tunnel < 255;};


  /**
   * Sets the peer for this feature.
   * @author Hj. Malthaner
   */
  void set_peer(featurepeerhandle_t peer);


  /**
   * Sets this features properties.
   * @author Hj. Malthaner
   */
  void set_props(const properties_t * props);


  /**
   * Get a named int property.
   * @param def default vlaue to return if the property does no exists
   * @author Hansj�rg Malthaner
   */
  int get_int(const char *name, int def) const;


  /**
   * Get a named string property.
   * @param def default vlaue to return if the property does no exists
   * @author Hansj�rg Malthaner
   */
  const char * get_string(const char *name, const char *def) const;


  /**
   * Gets the peer of this feature.
   * @author Hj. Malthaner
   */
  const featurepeerhandle_t & get_peer() const;


  /**
   * Can we move through this feature?
   * @author Hj. Malthaner
   */
  inline bool is_blocking() const {return (flags & BLOCKING);};

  
  void set_blocking(bool yesno);

  
  /**
   * Gets the name of the feature
   * @param def default return in case of unnamed feature
   * @author Hj. Malthaner
   */
  const char * get_ident(const char * def) const;


  /**
   * Basic construcor, initializes a blocking feature.
   * @author Hj. Malthaner
   */
  feature_t();


  /**
   * Copy constructor.
   * @author Hj. Malthaner
   */
  feature_t(feature_t *other);


  ~feature_t();



  void paint(graphics_t *gr, int xpos, int ypos, int shade, bool trans);


  /**
   * Activate this feature. Delegated to the peer if a peer is
   * available.
   * @author Hj. Malthaner
   */
  void activate(const handle_tpl <thing_t> & thing) const;


  // ---- implements persistent_t ----

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);
  

  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};                  

#endif //FEATURE_T_H
