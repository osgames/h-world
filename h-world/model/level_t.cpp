/* 
 * level_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#ifndef SQUARE_T_H
#include "square_t.h"
#endif

#include "level_t.h"
#include "ground_t.h"
#include "feature_t.h"
#include "feature_peer_t.h"
#include "thing_t.h"
#include "pprops_t.h"
#include "primitives/area_t.h"
#include "util/fov_t.h"
#include "util/debug_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"

#ifndef VISUAL_T_H
#include "view/visual_t.h"
#endif

#ifndef tpl_minivec_h
#include "tpl/minivec_tpl.h"
#endif

#ifndef tpl_slist_tpl_h
#include "tpl/slist_tpl.h"
#endif


void level_t::set_light(koord k, int l) 
{
  lights[k.y*(size.x+1) + k.x] = l > 127 ? 127 : l;
}

void level_t::set_properties(pprops_t *p) 
{
  props = p;
  lit = props->get("lit");
}


/**
 * Level square accessor.
 * @author Hansj�rg Malthaner
 */
square_t * level_t::at(const koord pos) const {
  if(pos.x>=0 && pos.y>=0 && pos.x<size.x && pos.y<size.y) {
    return &squares[pos.y*size.x+pos.x];
  } else {
    return 0;
  }
}


/**
 * Tries to add thing near the given location. If not posible,
 * adds the thing at location pos.
 * @author Hansj�rg Malthaner
 */
void level_t::add_thing_near(const koord pos, handle_tpl < thing_t > thing)
{
  for(int j=pos.y-1; j<=pos.y+1; j++) { 
    for(int i=pos.x-1; i<=pos.x+1; i++) { 
      const koord k(i,j);
      square_t * square = at(k);

      if(square && square->can_enter(thing)) {	
	square->add_thing(thing, k);

	return;
      }
    }
  }

  square_t * square = at(pos);

  if(square) {	
    square->add_thing(thing, pos);
  }
}


level_t::level_t()
{
  squares = 0;
  partition_marker = 0;
  props = 0;
  viewable = 0;
  lights = 0;

  partition_sizes = new slist_tpl <int>; 
}


level_t::level_t(koord size)
{
  this->size = size;

  partition_sizes = new slist_tpl <int>; 

  squares = new square_t[size.x*size.y];
  partition_marker = new short[size.x*size.y];

  viewable = new unsigned char[size.x*size.y];

  memset(viewable,
	 0,
	 size.x*size.y*sizeof(unsigned char));


  lights = new signed char[(size.x+1)*(size.y+1)];

  memset(lights,
	 0,
	 (size.x+1)*(size.y+1)*sizeof(unsigned char));

  dbg->message("level_t::level_t()", "Size=%d,%d", this->size.x, this->size.y); 
}


level_t::~level_t()
{
  delete [] squares;
  squares = 0;

  delete [] partition_marker;
  partition_marker = 0;

  delete [] viewable;
  viewable = 0;

  //  delete props;
  // props = 0;

  delete partition_sizes;
  partition_sizes = 0;
}


/**
 * Finds connected areas in the level. Fills the partition_sizes list
 * 
 * @author Hansj�rg Malthaner
 */
int level_t::create_partitions()
{
  for(int i=0; i<size.x*size.y; i++) {
    partition_marker[i] = -1;
  }

  area_t area;

  area.tl = koord(0, 0);
  area.br = size - koord(1, 1);

  area_iterator_t iter (area);

  while(iter.next()) {
    const koord k = iter.get_current();
    int n = 0;

    if(partition_marker[k.y*size.x+k.x] == -1) {
      const featurehandle_t &feature = at(k)->get_feature();
      if(!feature.is_bound() || !feature->is_blocking() ) {
	n = fill(k);
      }
    }


    if(n) {

      dbg->message("level_t::create_partitions()",
		   "Partition #%d has %d connected squares",
		   partition_sizes->count(), n);

      partition_sizes->append(n);
    }
  }

  return partition_sizes->count();
}


int level_t::get_largest_partition()
{
  slist_iterator_tpl <int> iter (partition_sizes);

  int max = -1;
  int best = -1;
  int i=0;

  while(iter.next()) {
    if(iter.get_current() > max) {
      max = iter.get_current();
      best = i;
    }
    i++;
  }

  printf("Largest partition is %d\n", best);

  return best;
}


/**
 * Finds a list of empty areas of size in given partition. The areas
 * may overlap! At most 255 areas are returned.
 * @author Hansj�rg Malthaner
 */
minivec_tpl <koord> * level_t::get_empty_area(koord tl,
					      koord br,
					      koord area_size,
					      bool noitems,
					      int partition, 
					      int max) const
{
  if(max > 255) max = 255;


  minivec_tpl <koord> * result = new minivec_tpl <koord> (max);

  for(int j=tl.y; j<br.y-area_size.y; j++) {
    for(int i=tl.x; i<br.x-area_size.x; i++) {

      koord k;

      for(k.y = j; k.y < j+area_size.y; k.y++) {
	for(k.x = i; k.x < i+area_size.x; k.x++) {

	  square_t * square = at(k);

	  if((square->get_feature().is_bound() && 
	      square->get_feature()->is_blocking()) ||
	     partition_marker[(int)k.y*(int)size.x+(int)k.x] != partition || 
	     (noitems && square->get_things().count()) ) {
	    goto loop_on;
	  }
	}
      }
	
      result->append(koord(i,j));

      i += area_size.x;

      // Check bounds, break if full
      if(result->count() >= max) {
	return result;
      }

    loop_on:;
    }
  }

  dbg->message("level_t::get_empty_area()", 
	       "tl=%d,%d  br=%d,%d, asize=%d,%d results=%d",
	       tl.x, tl.y, br.x, br.y, area_size.x, area_size.y, 
	       result->count());

  return result;
}



/**
 * Scans a quadrant. Fills list with all things found.
 *
 * @param list the list to fill
 * @param d distance to scan
 * @author Hansj�rg Malthaner
 */
void level_t::scan_quadrant(slist_tpl < handle_tpl < thing_t > > *list, 
			    koord k_base,
			    int d,
			    int dir)
{
  // ordered NESW

  /*
   * Scan border vectors
   */
  static const koord s_tab [8] = {
    koord(-1, -1),
      koord(+1, -1),
      koord(+1, +1),
      koord(-1, +1),

      koord( 0, -1),
      koord(+1,  0),
      koord( 0, +1),
      koord(-1,  0),
  };

  /*
   * Scan traversal vectors
   */
  static koord t_tab [8] = {
    koord(+1,  0),
      koord( 0, +1),
      koord(-1,  0),
      koord( 0, -1),

      koord(+1, +1),
      koord(-1, +1),
      koord(-1, -1),
      koord(+1, -1),
  };

  // Major quadrants need 2 steps, minor only 1
  const int n = dir <= 3 ? 2 : 1;

  koord s = s_tab[dir];
  koord t = t_tab[dir];

  for(int i1=0; i1<d; i1++) { 

    k_base += s;
    
    koord k = k_base;

    for(int i2=0; i2<=(i1+1)*n; i2++) { 

      // printf("scanning %d,%d\n", k.x, k.y);
      
      square_t *s = at(k);
      
      if(s && is_viewable(k)) {
	// copy things
	minivec_iterator_tpl < handle_tpl < thing_t > > iter (s->get_things());

	while( iter.next() ) {
	  list->insert( iter.get_current() );
	}

	// visual debug
	// s->get_ground()->visual.set_image(3);
      }

      k += t;
    }
  }
}


/**
 * Moves a thing along a line from src to dst, until an obstacle is hit
 * or dst is reached.
 *
 * @author Hansj�rg Malthaner
 */
void level_t::project(koord src, koord dst, handle_tpl < thing_t > what)
{
  int steps;

  const int dx = dst.x - src.x;
  const int dy = dst.y - src.y;

  int xp = (src.x << 16) + (1 << 15);
  int yp = (src.y << 16) + (1 << 15);

  if(abs(dx) > abs(dy)) { 
    steps = abs(dx);
  } else {
    steps = abs(dy);
  }

  if(steps == 0) steps = 1;

  const int xs = (dx << 16) / steps;
  const int ys = (dy << 16) / steps;

  koord pos = src;
  koord new_pos;

  for(int i=0;i<=steps;i++) {
    new_pos.x = xp >> 16;
    new_pos.y = yp >> 16;
    square_t *s = at(new_pos);

    if(s) {

      if(s->get_feature().is_bound()) {
	// obstacle
	return;
      }

      at(pos)->remove_thing(what);

      xp+=xs;
      yp+=ys;

      s->add_thing(what, new_pos);
      pos=new_pos;
    }
  }
}


/**
 * Calculate visibility information
 * @author Hj. Malthaner
 */
void level_t::recalculate_fov(const koord ij_off, const int light_rad)
{
  fov_t fov;

  memset(viewable,
	 0,
	 size.x*size.y*sizeof(unsigned char));

  fov.set_source(this);
  fov.set_destination(this);
  fov.calc_field_of_view(ij_off, light_rad);
}


static const koord fill_off[8] = {
  koord(1,0),
    koord(-1,0),
    koord(0,1),
    koord(0,-1),
    koord(1,1),
    koord(-1,-1),
    koord(-1,1),
    koord(1,-1),
    };


/**
 * Partitioning helper function.
 * @author Hansj�rg Malthaner
 */
int level_t::fill(koord k)
{
  int n = 0;


  if(at(k) && partition_marker[k.y*size.x+k.x] == -1) {
    koord * queue = new koord[size.x*size.y];
    
    int head = 0;
    int tail = 0;

    queue[tail ++] = k;
    partition_marker[k.y*size.x+k.x] = partition_sizes->count();


    while(tail > head) {
      k = queue[head++];

      const featurehandle_t &feature = at(k)->get_feature();

      if(!feature.is_bound() || !feature->is_blocking() ) {

	n += 1;

	for(unsigned char i=0; i<8; i++) {
	  koord pos = k+fill_off[i];

	  if(at(pos) && partition_marker[pos.y*size.x+pos.x] == -1) {
	    queue[tail ++] = pos;
	    partition_marker[pos.y*size.x+pos.x] = partition_sizes->count();

	    // printf("filling, tail=%d\n", tail);
	  }
	}
      }
    }

    delete [] queue;
  }

  return n;
}


/**
 * Implements fov_source_t
 * @author Hj. Malthaner
 */
bool level_t::is_blocking(koord pos)
{
  const square_t * square = at(pos);

  return (square && 
	  square->get_feature().is_bound() &&
	  square->get_feature()->visual.is_opaque());
}


/**
 * Implements fov_destination_t
 * @author Hj. Malthaner
 */
void level_t::set_viewable(koord pos)
{
  if(pos.x >= 0 && pos.y >= 0 && 
     pos.x < size.x && pos.y < size.y) {

    viewable[pos.x + pos.y*size.x] = true;
  }
}



// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void level_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_short(size.x);
  file->rdwr_short(size.y);

  if(file->is_saving()) {

    for(int i=0; i<(size.x+1)*(size.y+1); i++) {
      file->rdwr_char(lights[i]);
    }

    for(int i=0; i<size.x*size.y; i++) {
      squares[i].read_write(store, file);
      file->rdwr_short(partition_marker[i]);
    }

    int n = partition_sizes->count();
    file->rdwr_int(n);
  
    slist_iterator_tpl<int> iter (partition_sizes);
    
    while(iter.next()) {
      n = iter.get_current();
      file->rdwr_int(n);
    }


    store->store(props, file);

  } else {
    int i,n;

    delete [] lights;
    delete [] squares;
    delete [] partition_marker;
    delete props;

    props = 0;  // re-init below
    squares = new square_t[size.x*size.y];
    partition_marker = new short[size.x*size.y];
    viewable = new unsigned char [size.x*size.y];
    lights = new signed char[(size.x+1)*(size.y+1)];
  

    memset(viewable,
	   0,
	   size.x*size.y*sizeof(unsigned char));


    for(int i=0; i<(size.x+1)*(size.y+1); i++) {
      file->rdwr_char(lights[i]);
    }

    for(i=0; i<size.x*size.y; i++) {
      squares[i].read_write(store, file);
      file->rdwr_short(partition_marker[i]);
    }

    file->rdwr_int(n);       // partition_sizes->count()

    for(i=0; i<n; i++) {
      int v;
      file->rdwr_int(v);
      partition_sizes->append(v);
    }

    set_properties((pprops_t *) store->restore(file, hstore_t::t_pprops));

    // printf("level_t:: pprops = %p\n", props);

    // props->dump();
  }
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t level_t::get_cid()
{
  return hstore_t::t_level;
}
