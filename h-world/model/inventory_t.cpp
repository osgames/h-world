/* 
 * inventory_t.cpp
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "inventory_t.h"
#include "ifc/inventory_listener_t.h"

#include "thinghandle_t.h"
#include "thing_t.h"
#include "level_t.h"
#include "square_t.h"
#include "world_t.h"

#include "util/property_t.h"
#include "util/debug_t.h"
#include "control/lua_call_t.h"

#include "factories/hstore_t.h"


bool inventory_t::is_valid(koord pos) const 
{
  return pos.x >= 0 && pos.x < size.x && pos.y >= 0 && pos.y < size.y;
}


/**
 * Check if given inventory area is empty
 * @author Hj. Malthaner
 */
bool inventory_t::is_empty(koord pos, koord tsize) const
{
  const koord min = pos;
  const koord max = pos+tsize-koord(1,1);

  bool ok = is_valid(min) && is_valid(max);

  // check if area empty
  if(ok) {
    for(pos.y=min.y; pos.y<=max.y && ok; pos.y++) {
      for(pos.x=min.x; pos.x<=max.x && ok; pos.x++) {
	ok &= !things[pos.x + pos.y*size.x].is_bound();
      }
    }
  }

  return ok;
}


/**
 * Adds a listener (remove, add events). Double calls are detected
 * and listeners only added once.
 * @author Hj. Malthaner
 */
void inventory_t::add_listener(inventory_listener_t *l)
{
  if(!listeners->contains(l)) {
    listeners->insert(l);
  }
}


void inventory_t::set_usage_allowed(bool yesno)
{
  usage_allowed = yesno;
}


bool inventory_t::is_usage_allowed() const
{
  return usage_allowed;
}


/**
 * Constructor. Creates an inventory of specified size.
 * @author Hj. Malthaner
 */
inventory_t::inventory_t(koord size, thinghandle_t & owner) 
{
  this->size = size;
  this->owner = owner.get_rep();

  things = new thinghandle_t [size.x * size.y];
  roots = new unsigned char [size.x * size.y];
  
  for(int i=0; i<size.x*size.y; i++) {
    things[i] = 0;
    roots[i] = false;
  }

  // dbg->message("inventory_t::inventory_t()", 
  //	       "inventory has size %d,%d", get_size().x, get_size().y);
  

  listeners = new slist_tpl <inventory_listener_t *> ();

  usage_allowed = true;
}


/**
 * Destructor. Deletes the thing array, but not the things.
 * @author Hj. Malthaner
 */
inventory_t::~inventory_t() 
{
  delete [] things;
  things = 0;
  delete [] roots;
  roots = 0;

  delete listeners;
  listeners = 0;

  usage_allowed = false;
  owner = 0;
}


/**
 * Calculates the total weight
 * @author Hj. Malthaner
 */
int inventory_t::calc_weight() 
{
  int sum = 0;

  for(int i=0; i<size.x*size.y; i++) {
    if(roots[i]) {
      sum += things[i]->get_int("weight", 0);
    }
  }

  if(owner) {
    thinghandle_t limb = owner->get_owner();

    thinghandle_t cont;

    // Trigger unwield, no update of listeners
    if(limb.is_bound()) {
      cont = limb->get_item();
      limb->set_item(0, false);
    }

    int base = owner->get_int("base_weight", 0);
    owner->set("weight", base+sum);

    base = owner->get_int("base_bulk", 0);
    owner->set("bulk", base+(sum>>10));

    // Trigger wield
    if(limb.is_bound()) {
      limb->set_item(cont, true);
    }
  }

  return sum;
}


/**
 * Tries to add the thing anywhere into the inventory.
 * @return true if successful, false otherwise
 * @author Hj. Malthaner
 */
bool inventory_t::add(thinghandle_t thing) 
{
  if(thing.is_bound()) {
    koord pos;
    koord thing_size = thing->get_inv_size();

    for(pos.y=0; pos.y<=size.y-thing_size.y; pos.y++) {
      for(pos.x=0; pos.x<=size.x-thing_size.x; pos.x++) {
	if(is_empty(pos, thing_size)) {
	  add_at_with_size(thing, pos, thing_size);
	  return true;
	}
      }
    }
  }
  return false;
}


/**
 * Tries to add the thing anywhere into the inventory.
 * If it doesn't fit, drop to ground at the location of
 * the inventories owner
 * @author Hj. Malthaner
 */
bool inventory_t::add_or_overflow(thinghandle_t thing)
{
  const bool ok = add(thing);

  if(!ok) {
    // flow over

    // Hajo: cases to consider: 
    // 1) inventory of a thing on the map
    // 2) inventory of a thing worn by another thing

    thinghandle_t root = thing_t::find_root(owner);

    // Now see if this is worn by aynthing else?

    if(root->get_owner().is_bound()) {
      dbg->message("inventory_t::add_or_overflow()", 
		   "item is weld/held, using owner for position"); 


      // ok, one more indirection
      root = thing_t::find_root(root->get_owner());
    }

    const koord pos = root->get_pos();
    level_t * level = world_t::get_instance()->get_level();

    if(level->at(pos)) {
      level->at(pos)->add_thing(thing, pos);
    } else {
      dbg->error("inventory_t::add_or_overflow()", 
		 "overflow, but invalid square (%d,%d)", pos.x, pos.y); 
    }
  }

  return ok;
}


/**
 * Tries to add the thing at a specific location into the inventory.
 * @return true if successful, false otherwise
 * @author Hj. Malthaner
 */
bool inventory_t::add_at(thinghandle_t thing, koord pos) 
{
  return add_at_with_size(thing, pos, thing->get_inv_size());
}


thinghandle_t inventory_t::get_from(koord pos) 
{
  thinghandle_t result;

  if(is_valid(pos)) {
    result = things[pos.x + pos.y*size.x];
  }

  return result;
}


thinghandle_t inventory_t::take_from(koord pos) 
{
  thinghandle_t thing = get_from(pos);
  
  if(thing.is_bound()) {
    remove(thing);

    dbg->message("inventory_t::take_from()", 
		 "took thing %s from %d,%d", 
		 thing->get_string("name", "unnamed"), pos.x, pos.y); 
    
  } else {
    dbg->message("inventory_t::take_from()", 
		 "nothing to take from %d,%d", pos.x, pos.y); 
  }
  
  return thing;
}


bool inventory_t::remove(thinghandle_t thing) {
  bool had_it = false;

  for(int i=0; i<size.x*size.y; i++) {
    if(things[i] == thing) {
      things[i] = 0;
      roots[i] = false;
      had_it = true;
    }
  }

  if(had_it) {
    // Hajo: set new total weight and bulk

    if(owner) {
      calc_weight();
    }

    fire_thing_removed(thing);
    thing->set_container(0);
  }

  return had_it;
}


/**
 * Fill list with things from this inventory
 * @author Hj. Malthaner
 */
void inventory_t::list_things(slist_tpl < thinghandle_t > *list)
{
  for(int i=0; i<size.x*size.y; i++) {
    if(roots[i]) {
      list->insert(things[i]);
    }
  }
}


/**
 * Tries to add the thing at a specific location into the inventory.
 * @return true if successful, false otherwise
 */
bool inventory_t::add_at_with_size(thinghandle_t thing, koord pos, koord tsize) 
{
  const koord min = pos;
  const koord max = pos+tsize-koord(1,1);

  /*  
  dbg->message("inventory_t::add_at_with_size()", 
	       "thing %s at %d,%d, thing size is %d,%d", 
	       thing->get_string("name", "unnamed"),
	       pos.x, pos.y, tsize.x, tsize.y);
  */


  bool ok = is_valid(min) && is_valid(max);

  // check if area empty
  if(ok) {
    for(pos.y=min.y; pos.y<=max.y && ok; pos.y++) {
      for(pos.x=min.x; pos.x<=max.x && ok; pos.x++) {

	// if slot is empty, it's ok to add, otherwise we 
	// need to check if the things can stack
	if(things[pos.x + pos.y*size.x].is_bound()) {
	  thinghandle_t &ti = things[pos.x + pos.y*size.x];

	  // must be stackable, and less than max stack

	  const int max = ti->get_int("max_stack", 1);
	  const int cur = ti->get_int("cur_stack", 1);

	  ok &= (cur < max) 

	    &&

	  // type must match

	    (0 == strcmp(ti->get_string("type", "XY1"),
			 thing->get_string("type", "AB2")))

	    &&

	  // subtye must match

	    (0 == strcmp(ti->get_string("subtype", "XY1"),
			 thing->get_string("subtype", "AB2")));



	  if(!ok) {
	    // Hajo: the items didn't stack, so check if they might be
	    // combined


	    lua_call_t call("on_drop_item_on_item");

	    const int result = 
	      call.activate(world_t::get_instance()->get_player(), 
			    this, ti, thing);

	    if(result) {
	      // did combine or activate
	      calc_weight();
	      return true;
	    }
	  }
	}
      }
    }

    // if ok, add thing
    
    if(ok) {
      thinghandle_t &ti = things[min.x + min.y*size.x];


      // drop on emtpy space? 

      if(!ti.is_bound()) {

	thing->set_container(this);

	for(pos.y=min.y; pos.y<=max.y ; pos.y++) {
	  for(pos.x=min.x; pos.x<=max.x ; pos.x++) {
	    things[pos.x + pos.y*size.x] = thing;
	    roots[pos.x + pos.y*size.x] = false;
	  }
	}
	roots[min.x + min.y*size.x] = true;
	
      } else {

	// combine items

	const int max = ti->get_int("max_stack", 1);
	const int cur = ti->get_int("cur_stack", 1);

	const int num = thing->get_int("cur_stack", 1);
	int drop = num;

	if(max - cur < num) {
	  // not all fit
	  ok = false;      // can't drop all
	  drop = max - cur; // can only drop so much
	}

	thing->set("cur_stack", num-drop);
	ti->set("cur_stack", cur+drop);

      }

      calc_weight();
      fire_thing_added(thing);
    }
  }
  return ok;
}


void inventory_t::fire_thing_added(thinghandle_t &t)
{
  slist_iterator_tpl <inventory_listener_t *> iter (listeners);

  while( iter.next() ) {
    iter.get_current()->thing_added(t);
  }
}


void inventory_t::fire_thing_removed(thinghandle_t &t)
{
  slist_iterator_tpl <inventory_listener_t *> iter (listeners);

  while( iter.next() ) {
    iter.get_current()->thing_removed(t);
  }
}


// ---- implements persistent_t ----

/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void inventory_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_short(size.x);
  file->rdwr_short(size.y);

  file->rdwr_uchar(usage_allowed);

  if(file->is_saving()) {
    for(int i=0; i<size.x*size.y; i++) {
      file->rdwr_uchar(roots[i]);
    }

    for(int i=0; i<size.x*size.y; i++) {
      store->store(things[i].get_rep(), file);
    }

    store->store(owner, file);
  } else {

    things = new thinghandle_t [size.x * size.y];
    roots = new unsigned char [size.x * size.y];

    for(int i=0; i<size.x*size.y; i++) {
      file->rdwr_uchar(roots[i]);
    }

    for(int i=0; i<size.x*size.y; i++) {
      things[i] = (thing_t *)store->restore(file, hstore_t::t_thing);

      if(things[i].is_bound()) {
	things[i]->set_container(this);
      }
    }

    owner = (thing_t *)store->restore(file, hstore_t::t_thing);

    listeners = new slist_tpl <inventory_listener_t *> ();
  }
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t inventory_t::get_cid()
{
  return hstore_t::t_inventory;
}
