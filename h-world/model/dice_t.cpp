/* 
 * dice_t.cpp
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <stdio.h>

#include "dice_t.h"
#include "util/rng_t.h"
#include "util/debug_t.h"


/**
 * Parses XdY+C format
 * @author Hj. Malthaner
 */
dice_t::dice_t(const char * desc)
{
  int d = 0;
  int s = 0;
  int o = 0;

  sscanf(desc, "%dd%d+%d", &d, &s, &o);

  dice = (unsigned char) d;
  sides = (unsigned char) s;
  offset = (unsigned char) o;

  dbg->debug("dice_t::dice_t()", "Parsing %s as %dd%d+%d", desc, dice, sides, offset);
}


/**
 * Roll the dice and return the sum
 * @author Hj. Malthaner
 */
int dice_t::roll()
{
  return rng_t::get_the_rng().roll(dice, sides) + offset;
}

