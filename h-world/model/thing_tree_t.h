/* 
 * thing_tree_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef THING_TREE_T_H
#define THING_TREE_T_H

#include "thinghandle_t.h"
#include "ifc/thing_visitor_t.h"


template<class T> class slist_tpl;

/**
 * A class to do summary operations on a thing tree. This class is not
 * very OO, but a quick and simple solution.
 *
 * @author Hj. Malthaner
 */
class thing_tree_t : public thing_visitor_t
{
private:

  /**
   * During a list_inventory operation this holds the list
   * to fill.
   * @author Hj. Malthaner
   */
  slist_tpl <thinghandle_t> * list;

  enum op {op_inventories, op_inventories_deep, op_items};

  enum op operation;

public:

  thing_tree_t() {};

  virtual ~thing_tree_t() {list=0;};

  /**
   * Fills the list with handles to all things that have inventories.
   * Things inside inventories are not checked again.
   * 
   * @param tree the thing tree to search
   * @param list the list to fill
   * @param deep if true also list contents of inventories (non-recursive)
   * @author Hj. Malthaner
   */
  void list_inventories(thinghandle_t tree,
			slist_tpl <thinghandle_t> * list,
			bool deep);


  /**
   * Fills the list with handles to all things that are held by limbs
   * 
   * @param tree the thing tree to search
   * @param list the list to fill
   * @author Hj. Malthaner
   */
  void list_items(thinghandle_t tree,
		  slist_tpl <thinghandle_t> * list);


  /**
   * Cretaes a new list of items from old list and given filter
   * 
   * @author Hj. Malthaner
   */
  slist_tpl <thinghandle_t> * filter_list(slist_tpl <thinghandle_t> * list,
					  const char *ident,
					  const char *type,
					  const char *subtype);


  /**
   * Implements thing visitor
   *
   * @author Hj. Malthaner
   */
  void visit_thing(const handle_tpl <thing_t> &thing);
};



#endif


