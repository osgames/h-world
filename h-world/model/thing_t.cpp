/* 
 * thing_t.cpp
 *
 * Copyright (c) 2001 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "thing_t.h"
#include "world_t.h"
#include "inventory_t.h"
#include "memory_t.h"
#include "thing_constants.h"

#include "control/usage_t.h"
#include "control/lua_call_t.h"

#include "view/player_visual_connector_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"
#include "factories/thing_factory_t.h"

#include "ifc/thing_listener_t.h"

#include "util/debug_t.h"

#ifndef PPROPS_T_H
#include "pprops_t.h"
#endif


/**
 * Finds the root of a thing tree. This is a class method becuase
 * the reference counting does not allow a "self" handle for things
 *
 * @author Hj. Malthaner
 */
handle_tpl<thing_t> thing_t::find_root(handle_tpl <thing_t> body)
{
  if(body.is_bound() == false) {
    return body;
  } else if(!body->parent.is_bound()) {
    return body;
  } else {
    return find_root(body->parent);
  }
}



handle_tpl <thing_t> thing_t::find_limb(handle_tpl <thing_t> body, 
			     const char *type, 
			     const char *subtype)
{
  handle_tpl <thing_t> found;

  // printf("comparing body type '%s' and type '%s'\n", body->type, type);
  
  // can only compare if both attributes exists
  if(type && body->get_string(TH_TYPE, 0)) {
    
    // compare
    if(strcmp(body->get_string(TH_TYPE, 0), type) == 0) {
      
      // if a subtype is given we need to compare
      if(subtype) {
	if(body->get_string(TH_SUBTYPE, 0) && 
	   strcmp(body->get_string(TH_SUBTYPE, 0), subtype) == 0) {
	  // found
	  found = body;
	}
      } else {
	// found
	found = body;
      }
    }
  }
  
  if(!found.is_bound()) {
    if(body->get_limbs().count()) {
      slist_iterator_tpl <handle_tpl <thing_t> > iter (body->get_limbs());
      
      while(!found.is_bound() && iter.next()) {
	found = find_limb(iter.get_current(), type, subtype);
      }
    }
  }

  return found;
}


/**
 * Find a limb that is holding the given item recursively
 *
 * @author Hj. Malthaner
 */
handle_tpl<thing_t> thing_t::find_limb(handle_tpl <thing_t> body, 
				       handle_tpl <thing_t> item)
{
  handle_tpl <thing_t> found;

  // printf("comparing body type '%s' and type '%s'\n", body->type, type);
  
  // can only compare if both attributes exists
  if(body->get_item() == item) {
    // found
    found = body;
  } else {
    if(body->get_limbs().count()) {
      slist_iterator_tpl <handle_tpl <thing_t> > iter (body->get_limbs());
      
      while(!found.is_bound() && iter.next()) {
	found = find_limb(iter.get_current(), item);
      }
    }
  }

  return found;
}   


/**
 * Find a limb recursively by item constraints
 *
 * @author Hj. Malthaner
 */
handle_tpl <thing_t> thing_t::find_limb_to_hold(handle_tpl <thing_t> body,
						handle_tpl <thing_t> item) 
{
  handle_tpl <thing_t> found;

  if(!body->get_item().is_bound() && body->check_item_constraints(item)) {
    found = body;
  }
  
  if(!found.is_bound()) {
    if(body->get_limbs().count()) {
      slist_iterator_tpl <handle_tpl <thing_t> > iter (body->get_limbs());
      
      while(!found.is_bound() && iter.next()) {
	found = find_limb_to_hold(iter.get_current(), item);
      }
    }
  }

  return found;
}


/**
 * Remove a thing, checks held items and inventories
 * @return true on success
 * @author Hj. Malthaner
 */
bool thing_t::replace_thing(handle_tpl <thing_t> body,
			    handle_tpl <thing_t> old_item,
			    handle_tpl <thing_t> new_item) 
{
  bool found = false;

  if(body->get_item().is_bound()) {
    if(body->get_item() == old_item) {
      body->set_item(new_item, true);
      found = true;
    } else {
      // check inventory contents
      inventory_t *inv = body->get_item()->get_inventory();
      found = inv && inv->remove(old_item);

      if(found) {
	inv->add(new_item);
      }
    } 
  }

  if(!found) {
    if(body->get_limbs().count()) {
      slist_iterator_tpl <handle_tpl <thing_t> > iter (body->get_limbs());
      
      while(!found && iter.next()) {
	found = replace_thing(iter.get_current(), old_item, new_item);
      }
    }
  }

  return found;
}


/**
 * Determines if this is a known item or not
 * @return true if unknown
 * @author Hj. Malthaner
 */
bool thing_t::is_unknown(const properties_t * props)
{
  const char *pre_str = props->get_string("att-0.prefix");
  const char *uk_str = props->get_string("is_unknown");
  const char *id_str = props->get_string("identified_ident");

  // -> already memorized ?

  memory_t * mem = world_t::get_instance()->get_player()->memory;
  if(id_str && mem->is_item_known(id_str)) {
    // then set to known
    uk_str = "f";
  }


  return 
    (uk_str && uk_str[0] == 't') || 
    (pre_str && uk_str == 0) ||
    (pre_str && uk_str && uk_str[0] == 't');
}


/**
 * Counts number of special attributes
 * @author Hj. Malthaner
 */
int thing_t::count_attributes() const
{
  int n = 0;
  int result = 0;
  bool ok;
  char buf[128];
  
  do {
    sprintf(buf, "att-%d.effect", n);
    ok = (get_string(buf, 0) != 0);
    result += ok ? 1 : 0;
    n ++;
  } while(ok);

  return result;
}


/**
 * Fills buffer with an identifier (no magic attributes or curses)
 * for this thing (aka "short name")
 * @return number of characters written into buf
 * @author Hj. Malthaner
 */
int thing_t::get_ident_plain(char *buf)
{
  // already memorized ?
  const memory_t * mem = world_t::get_instance()->get_player()->memory;

  // then set to known
  const bool memorized = mem->is_item_known(get_string("identified_ident", ""));  

  lua_call_t call("calculate_plain_ident_string");
  call.activate(buf, this, memorized ? this : 0);

  const int n = strlen(buf);
  return n;
}


/**
 * Fills buffer with an identifier (prefix+ident) for
 * this thing
 * @return number of characters written into buf
 * @author Hj. Malthaner
 */
int thing_t::get_ident(char *buf) 
{
  // already memorized ? -> then set to known
  bool memorized = false;

  // before birth we have no player yet

  if(world_t::get_instance()->get_player().is_bound()) {
    const memory_t * mem = world_t::get_instance()->get_player()->memory;

    if(mem) {
      memorized = mem->is_item_known(get_string("identified_ident", ""));
    }
  }

  dbg->debug("thing_t::get_ident()", "memorized='%d'", memorized); 

  lua_call_t call("calculate_ident_string");
  call.activate(buf, this, memorized ? this : 0);

  const int n = strlen(buf);
  return n;



  // old code, before Lua callback was used

  /*
  int n = 0;

  // Hajo: check for known curses. Tell if player knows about a curse
  // - only check items, cursed monsters are irrelevant here

  if(get_string("alive",0) == 0) {
    const char * curse_known = properties.get_string("curse_known");
    if(curse_known) {
      const char * cursed = properties.get_string("cursed");
      
      if(cursed == 0) {
	// plain item
	n = sprintf(buf, "uncursed ");
      } else if(*cursed == 'f') {
	n = sprintf(buf, "uncursed ");
      } else {
	n = sprintf(buf, "cursed ");
      }
    } else {
      // player doesn't know ...
    }
  }    

  // check for magic attributes

  const char * prefix = properties.get_string("att-0.prefix");
  const char * postfix = 0;
  const char * rare = 0;
  const char * unknown = properties.get_string("is_unknown");
  const char * ident = 0;


  // -> already memorized ?

  memory_t * mem = world_t::get_instance()->get_player()->memory;
  if(mem->is_item_known(get_string("identified_ident", ""))) {
    // then set to known
    unknown = "f";
  }



  if(unknown == 0 && prefix == 0) {
    // Unidentified easy known item
    ident = properties.get_string("ident");

  } else if(unknown != 0 && *unknown != 't') {
    // Identified item

    ident = properties.get_string("identified_ident");
    if(ident == 0) {
      // Identified easy known item
      ident = properties.get_string("ident");
    }

    // Check number of magic attributes

    // rare item ?
    rare = properties.get_string("att-2.prefix");
    
    if(rare) {
      rare = "special, ";
    }

    postfix = properties.get_string("att-1.postfix");

  } else {
    // Items that must be identifed first


    ident = properties.get_string("ident");

    if(prefix != 0) {
      // Don't show real prefix until identified
      prefix = "magic";
    }
  }

  // dbg->message("thing_t::get_ident()", "prefix=%s unknown=%s ident=%s", prefix, unknown, ident); 

  if(prefix) {

    if(rare) {
      n += sprintf(buf+n, "%s", rare);
    }

    n += sprintf(buf+n, "%s %s", prefix, ident);

    if(postfix) {
      n += sprintf(buf+n, " %s", postfix);
    }

  } else {
    n += sprintf(buf+n, "%s",  ident);
  }


  return n;
  */
}


/**
 * Calls all listeners
 * @author Hansj�rg Malthaner
 */
void thing_t::call_listeners()
{
  if(parent.is_bound()) {
    parent->call_listeners();
  } else {
    // dbg->message("thing_t::call_listeners()", get_string("name", "unnamed")); 

    handle_tpl <thing_t> self (this);

    slist_iterator_tpl <thing_listener_t *> iter ( listeners );
    
    while(iter.next()) {
      dbg->message("thing_t::call_listeners()", "%s is calling %p", 
		   get_string("ident", "unnamed"),
		   iter.get_current()); 

      iter.get_current()->update(self);
    }
  }
}


/**
 * Semi-Deep copy.
 * - actor is not copied
 * - inventory is not copied
 * - listeners are not copied!
 *
 * @author Hj. Malthaner
 */
const handle_tpl <thing_t> thing_t::clone() const
{
  handle_tpl <thing_t> other (new thing_t());

  // XXX unfinished


  // No copy
  // other->actor = 0;

  // XXX deep ?
  //  slist_tpl < handle_tpl<thing_t> > limbs;


  // XXX deep ?
  //  handle_tpl <thing_t> parent;

  if(item.is_bound()) {
    other->item = item->clone();
  }

  other->envi_conn = envi_conn;


  // No copy
  other->inventory = 0;


  other->memory = 0;
  
  // Copy
  other->pos = pos;


  // Copy
  properties->copy_into(other->properties);


  other->inv_size = inv_size;
   

  // Create new usages
  thing_factory_t::add_usages(other, other->properties);


  other->visual = visual;
  other->inv_visual = inv_visual;


  // IMO the listeners should not be copied.
  // slist_tpl <thing_listener_t *> listeners;

  return other;
}


void thing_t::add_usage(usage_t * usage) 
{
  usages.insert(usage);
}


void thing_t::add_limb(handle_tpl <thing_t>limb)
{
  if(limb.is_bound()) {
    //puts(limb->get_string("name", "XXX"));

    limbs.insert(limb);
  }
}


/**
 * Adds a listener
 * @author Hansj�rg Malthaner
 */     
void thing_t::add_listener(thing_listener_t * listener)
{
  // dbg->message("thing_t::add_listener()", "%p", listener); 

  if(!listeners.contains(listener)) {
    listeners.insert(listener);
  } else {
    dbg->warning("thing_t::add_listener()", "%p is already listening", listener); 
  }
}


/**
 * Removes a listener
 * @author Hansj�rg Malthaner
 */     
void thing_t::remove_listener(thing_listener_t * listener)
{
  // dbg->message("thing_t::remove_listener()", "%p", listener); 

  listeners.remove(listener);
}


/**
 * Adds a property listener
 * @author Hansj�rg Malthaner
 */     
void thing_t::add_property_listener(property_listener_t * l) {
  properties->add_listener(l);
}


void thing_t::set_inventory(inventory_t *inv)
{
  delete inventory;

  inventory = inv;
}


/**
 * Sets containing container
 * @author Hansj�rg Malthaner
 */     
void thing_t::set_container(inventory_t *con)
{
  container = con;
}


/**
 * Sets a the memory. Thing is responsoble to free memories.
 * @author Hansj�rg Malthaner
 */     
void thing_t::set_memory(memory_t *mem)
{
  memory = mem;
}


/**
 * Memorize item 'ident'
 * @author Hansj�rg Malthaner
 */     
void thing_t::set_item_known(const char * ident)
{
  memory->set_item_known(ident);
}


/**
 * Memorize a message
 * @author Hansj�rg Malthaner
 */     
void thing_t::memorize(const char * catg, const char * msg)
{
  if(memory) {
    memory->memorize(catg, msg);
  }
}


/**
 * Recalls the nth message
 * @return NULL if no such message exists
 * @author Hansj�rg Malthaner
 */     
const char * thing_t::recall(const char * catg, int n)
{
  const char * result = NULL;

  if(memory) {
    result = memory->recall(catg, n);
  }

  return result;
}



/**
 * Check if this item can be held
 * @author Hansj�rg Malthaner
 */     
bool thing_t::check_item_constraints(handle_tpl <thing_t> thing)
{
  lua_call_t call("check_item_constraints");
  const int n= call.activate(this, thing);

  return (n == 1);
}


/**
 * Set item to be held
 * @param update if true, listeners are called
 * @author Hansj�rg Malthaner
 */     
void thing_t::set_item(handle_tpl <thing_t> thing, bool update)
{
  if(thing == item) {
    return;
  }

  handle_tpl <thing_t> self (this);

  if(item.is_bound()) {
    // base call for unwielding an item
    
    lua_call_t call("on_unwield");
    call.activate(thing_t::find_root(self), self, item);
    
    // No longer held by us
    item->owner = 0;
  }

  item = thing;


  if(item.is_bound()) {

    // base call for wielding an item
    
    lua_call_t call("on_wield");
    call.activate(thing_t::find_root(self), self, item);


    // Now held by us
    item->owner = self;
  }

  if(update) {
    call_listeners();
  }
}


/**
 * Set one of a multi-limb item to be held
 * Does not call lua scripts or listeners!
 * @author Hansj�rg Malthaner
 */     
void thing_t::set_multi_item(handle_tpl <thing_t> thing)
{
  item = thing;

  if(item.is_bound()) {  
    handle_tpl <thing_t> self (this);
    item->owner = self;
  }
}


/**
 * Put multi-limb item on all limbs. Be careful, none of
 * the limbs may hold other items, if items are held the
 * behaviour is undefined.
 *
 * @param item the item to set, can be null
 * @author Hj. Malthaner
 */
void thing_t::put_multi(handle_tpl <thing_t> item)
{
  if(item.is_bound()) {
    char key[256];
    const int count = item->get_int("limbs_required", 0);

    // Hajo: always use root to find limbs
    handle_tpl <thing_t> limb = find_root(this);
    
    for(int i=0; i<count; i++) { 
      sprintf(key, "limbs_required[%d].type", i);
      const char * type = item->get_string(key, 0);
      
      sprintf(key, "limbs_required[%d].subtype", i);
      const char * subtype = item->get_string(key, 0);
      
      thinghandle_t req = thing_t::find_limb(limb, type, subtype);
    
      req->set_multi_item(item);
    }

    // Hajo: prepare to trigger wield function
    set_multi_item(0);

    // Hajo: now trigger wield function for item
    set_item(item, true);
  }

  dbg->message("put_multi()", "done");
}


/**
 * remove item from all limbs
 * @param item the item to remove, can be null
 * @author Hj. Malthaner
 */
static void remove_multi_aux(handle_tpl <thing_t> body,
			     handle_tpl <thing_t> item)
{
  if(body->get_item() == item) {
    body->set_multi_item(0);    
  }

  slist_iterator_tpl <handle_tpl <thing_t> > iter (body->get_limbs());

  while(iter.next()) {
    remove_multi_aux(iter.get_current(), item);
  }
}


/**
 * Removes a multi limb item from all limbs
 * @param item the item to remove, can be null
 * @authot Hj. Malthaner
 */
void thing_t::remove_multi(handle_tpl <thing_t> item)
{
  if(item.is_bound()) {

    // Hajo: trigger unwield function once
    set_item(0, true);

    // Hajo: always use root to find limbs
    handle_tpl <thing_t> root = find_root(this);

    // Hajo: remove item from all limbs
    remove_multi_aux(root, item);
  }
}



/**
 * Get held item
 * @author Hansj�rg Malthaner
 */     
const handle_tpl <thing_t> & thing_t::get_item() 
{
  return item;
}


actor_t & thing_t::get_actor()
{
  return actor;
}


/**
 * Basic constructor.
 * @author Hansj�rg Malthaner
 */     
thing_t::thing_t() : pos(-1,-1)
{
  inventory = 0;
  container = 0;
  memory = 0;

  properties = new pprops_t();
}


/**
 * Creates a things as a part of another thing, i.e. a limb
 * @author Hansj�rg Malthaner
 */     
thing_t::thing_t(handle_tpl <thing_t> t) : pos(-1,-1)
{
  inventory = 0;
  container = 0;
  memory = 0;
  parent = t;

  properties = new pprops_t();
}


/**
 * Destructor. Frees the inventory.
 * @author Hansj�rg Malthaner
 */     
thing_t::~thing_t()
{
  dbg->debug("thing_t::~thing_t()", "deleting thing '%s/%s'", 
	     get_string("ident", "?"), get_string("name", "?"));

  slist_iterator_tpl < usage_t * > usgi (usages);

  while( usgi.next() ) {
    delete usgi.get_current();
  }

  usages.clear();

  delete inventory;
  inventory = 0;

  delete memory;
  memory = 0;

  delete properties;
  properties = 0;

  container = 0;
  parent = 0;
  item = 0;
  owner = 0;
}


/**
 * sets property 'name' to a string value
 * @author Hansj�rg Malthaner
 */
void thing_t::set(const char * name, const char* value) {
  properties->set(name, value);
}


/**
 * sets property 'name' to an int value
 * @author Hansj�rg Malthaner
 */
void thing_t::set(const char * name, int value) {
  properties->set(name, value);
}


const properties_t * thing_t::get_properties() const
{
  return properties;
}


/**
 * @return pointer to requested property or NULL if property does not exist
 * @author Hansj�rg Malthaner
 */
const property_t * thing_t::get(const char * name) const {
  return properties->get(name);
};


/**
 * Get a named int property.
 * @param def default vlaue to return if the property does no exists
 * @author Hansj�rg Malthaner
 */
int thing_t::get_int(const char *name, int def) const
{
  const int *p = properties->get_int(name);
  return p ? *p : def;
}


/**
 * Get a named string property.
 * @param def default vlaue to return if the property does no exists
 * @author Hansj�rg Malthaner
 */
const char * thing_t::get_string(const char *name, const char *def) const
{
  const char *p = properties->get_string(name);
  return p ? p : def;
}


/**
 * returns -1,-1 on failure!
 */
koord thing_t::get_koord(const char * name)
{
  char buf [128];

  sprintf(buf, "%s.x", name);
  int x = -1;
  if(properties->get_int(buf)) {
    x = *properties->get_int(buf);
  }

  sprintf(buf, "%s.y", name);
  int y = -1;
  if(properties->get_int(buf)) {
    y = *properties->get_int(buf);
  }


  return koord(x,y);
}


/**
 * Checks if the hitpoints dropped below 0 
 * @author Hj. Malthaner
 */
bool thing_t::check_alive()
{
  const int currHP = get_int("HPcurr", 0);

  if(currHP < 0) {
    // died

    thinghandle_t thing (this);
    world_t::get_instance()->kill(thing, false);
  }

  return currHP >= 0;
}



/**
 * Destroy limb tree, needed to clean this thing from memory.
 */
void thing_t::destroy()
{
  slist_iterator_tpl <thinghandle_t> iter (limbs);

  while(iter.next()) {
    iter.get_current()->destroy();
  }


  slist_iterator_tpl < usage_t * > usgi (usages);

  while( usgi.next() ) {
    delete usgi.get_current();
  }

  usages.clear();

  if(item.is_bound()) {
    item->owner = 0;
  }
  item = 0;
  owner = 0;
  parent = 0;
  limbs.destroy();
}



/**
 * Limbs will call this if a change has happened
 * @author Hansj�rg Malthaner
 */
void thing_t::body_changed()
{
  call_listeners();
}


/**
 * Determines the current speed of this thing
 * @return modified delay
 * @author Hansj�rg Malthaner
 */
int thing_t::calc_speed(int delay)
{
  const int base = get_int(TH_BASE_SPEED, 10);
  const int bulk = get_int(TH_HINDERANCE, 0);


  return ((delay*10) + bulk) / base;
}


/**
 * Debugging, dump contents
 * @author Hansj�rg Malthaner
 */
void thing_t::dump()
{
  properties->dump();
}


// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void thing_t::read_write(storage_t *store, iofile_t * file)
{

  visual.read_write(store, file);
  inv_visual.read_write(store, file);

  properties->read_write(store, file);

  // environment_connector_t envi_conn;

  file->rdwr_short(inv_size.x);
  file->rdwr_short(inv_size.y);

  file->rdwr_short(pos.x);
  file->rdwr_short(pos.y);

  int limb_count = limbs.count();
  int usage_count = usages.count();

  file->rdwr_int(limb_count);
  file->rdwr_int(usage_count);

  if(file->is_saving()) {

    store->store(memory, file);

    slist_iterator_tpl < handle_tpl<thing_t> > iter (limbs);

    while( iter.next() ) {
      store->store(iter.get_current().get_rep(), file);
    }

    store->store(parent.get_rep(), file);
    store->store(item.get_rep(), file);
    store->store(owner.get_rep(), file);


    slist_iterator_tpl < usage_t * > usgi (usages);

    while( usgi.next() ) {
      store->store(usgi.get_current(), file);
    }


    store->store(inventory, file);

    /*
    slist_tpl <thing_listener_t *> listeners;
    */

  } else {

    memory = (memory_t *)store->restore(file, hstore_t::t_memory);
    // memory = 0;

    limbs.clear();
    for(int i=0; i<limb_count; i++) { 
      limbs.append( thinghandle_t( (thing_t *) store->restore(file, hstore_t::t_thing) ));
    }


    parent = thinghandle_t( (thing_t *) store->restore(file, hstore_t::t_thing));
    item = thinghandle_t( (thing_t *) store->restore(file, hstore_t::t_thing));

    owner = thinghandle_t( (thing_t *) store->restore(file, hstore_t::t_thing));

    usages.clear();
    for(int i=0; i<usage_count; i++) { 
      usages.append( (usage_t *) store->restore(file));
    }


    inventory = (inventory_t *) store->restore(file, hstore_t::t_inventory);


    player_visual_connector_t::prepare_images(this, properties);
  }

  // actor/actions reference the thing, so it must be complete
  // before restoring the actor -> laod actor last
  actor.read_write(store, file);
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t thing_t::get_cid()
{
  return hstore_t::t_thing;
}
