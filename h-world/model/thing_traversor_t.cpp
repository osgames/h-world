/* 
 * thing_traversor_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "thing_traversor_t.h"

#include "model/thing_t.h"

#include "ifc/limb_visitor_t.h"
#include "ifc/thing_visitor_t.h"


void thing_traversor_t::set_limb_visitor(limb_visitor_t * lv)
{
  this->lv = lv;
}


void thing_traversor_t::set_thing_visitor(thing_visitor_t * tv)
{
  this->tv = tv;
}


void thing_traversor_t::set_model(thinghandle_t model)
{
  this->model = model;
}


void thing_traversor_t::traverse_limb(thinghandle_t limb)
{
  // visit limb
  if(lv) lv->visit_limb(limb);

  // if something held, visit thing
  if(tv && limb->get_item().is_bound()) tv->visit_thing(limb->get_item());

  // recurse
  if(limb->get_limbs().count()) {
    slist_iterator_tpl <thinghandle_t> iter (limb->get_limbs());

    while(iter.next()) {
      traverse_limb(iter.get_current());
    }
  }
}

thing_traversor_t::thing_traversor_t()
{
  lv = 0;
  tv = 0;
}


thing_traversor_t::thing_traversor_t(thinghandle_t model)
{
  lv = 0;
  tv = 0;
  this->model = model;
}


void thing_traversor_t::traverse()
{
  traverse_limb(model);
}
