/* Copyright by Hj. Malthaner */

#ifndef ENVIRONMENT_T_H
#define ENVIRONMENT_T_H

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

/**
 * Class for environements, each square has a environment. This is 
 * a set of values that describe the envrionment.
 *
 * The values have a default range of -100 ... 100.
 * The values have a default value of 0.
 *
 * @author Hj. Malthaner
 */

class environment_t : public persistent_t {
public:
  signed char gravity;
  signed char humidity;
  signed char lightness;
  signed char loudness;
  signed char temperature;
  
  // default values
  
  signed char def_gravity;
  signed char def_humidity;
  signed char def_lightness;
  signed char def_loudness;
  signed char def_temperature;
  
  
  environment_t();


  // ---- implements persistent_t ----

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);
  

  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};
#endif //ENVIRONMENT_T_H
