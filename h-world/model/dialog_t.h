#ifndef dialog_t_h
#define dialog_t_h

#define MAX_REPLY 16

class config_file_t;

/**
 * Class for player/NPC dialogs
 *
 * @author Hj. Malthaner
 */
class dialog_t
{
 public:

  char *speech;

  char *reply[MAX_REPLY];

  dialog_t * dialog[MAX_REPLY];



  static dialog_t * read(config_file_t *file);

  dialog_t();

  ~dialog_t();

};

#endif // dialog_t_h
