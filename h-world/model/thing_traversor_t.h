/* 
 * thing_traversor_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef THING_TRAVERSOR_T_H
#define THING_TRAVERSOR_T_H

#include "tpl/handle_tpl.h"
#include "thinghandle_t.h"

class limb_visitor_t;
class thing_visitor_t;


/**
 * A class to inspect all limbs and sub-things of a thing.
 * Does not inspect inventories.
 * @author Hj. Malthaner
 */
class thing_traversor_t 
{
private:

  thinghandle_t model;

  limb_visitor_t * lv; 
  thing_visitor_t * tv;
  

  void traverse_limb(thinghandle_t limb);

public:

  void set_limb_visitor(limb_visitor_t * lv);
  void set_thing_visitor(thing_visitor_t * tv);

  void set_model(thinghandle_t model);

  thing_traversor_t();
  thing_traversor_t(thinghandle_t model);

  void traverse();
};



#endif


