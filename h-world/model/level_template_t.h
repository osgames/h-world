/* Copyright by Hj. Malthaner */

#ifndef LEVEL_TEMPLATE_T_H
#define LEVEL_TEMPLATE_T_H

#include "tpl/slist_tpl.h"

class level_template_t {

public:
    slist_tpl <const char *> clutter;

    int preferred_item_level;

    /**
     * Monsters per 1000 squares.
     * @author Hj. Malthaner
     */
    int preferred_monster_density;

    /**
     * Items per 1000 squares.
     * @author Hj. Malthaner
     */
    int preferred_item_density;


    /**
     * Loads a level templet from a text file.
     * @author Hj. Malthaner
     */
    void load(FILE *file);
};
#endif //LEVEL_TEMPLATE_T_H
