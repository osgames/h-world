/* 
 * feature_peer_t.h
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef FEATURE_PEER_T_H
#define FEATURE_PEER_T_H


#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif


class thing_t;
template <class T> class handle_tpl;

/**
 * Abstract base class for actions bound to features.
 * @author Hj. Malthaner
 */
class feature_peer_t : public persistent_t {
private:

public:    

  virtual ~feature_peer_t() {};

  /**
   * Activate feature.
   * @author Hj. Malthaner
   * @return true if activation was successful, false otherwise
   */
  virtual bool activate(handle_tpl <thing_t> thing) = 0;

};                  

#endif
