#include "pprops_t.h"

#include "factories/hstore_t.h"

#include "util/debug_t.h"
#include "util/property_t.h"

#include "tpl/slist_tpl.h"


pprops_t::pprops_t()
{
  // printf("pprops_t: %p\n", this);
}


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void pprops_t::read_write(storage_t *store, iofile_t * file)
{
  if(file->is_saving()) {

    const slist_tpl<const char *> * keys = get_keys();

    int count = keys->count();
    file->rdwr_int(count);

    slist_iterator_tpl <const char *> iter (keys);
    
    while( iter.next() ) {
    
      char * key = const_cast <char *> (iter.get_current());

      const property_t *prop = get(key);
      signed char type;

      // dbg->message("pprops_t::read_write()", "writing property '%s'", key);

      file->rdwr_string(key);

      if(prop == 0) {
	dbg->message("pprops_t::read_write()", "property '%s' was null", key);
	type = '0';
	file->rdwr_char(type);
      }
      else if(prop->get_int()) {
	type = 'i';
	file->rdwr_char(type);
	int n = *prop->get_int();
	file->rdwr_int(n);
      }
      else if(prop->get_string()) {
	type = 'c';
	file->rdwr_char(type);
	char * val = const_cast <char *> (prop->get_string());
	file->rdwr_string(val);
      } else {
	type = '0';
	file->rdwr_char(type);
      }
    }

    delete keys;

  } else {
    // loading

    int count;
    file->rdwr_int(count);


    for(int i=0; i<count; i++) { 

      char * key;
      file->rdwr_string(key);

      signed char type;
      file->rdwr_char(type);

      dbg->debug("pprops_t::read_write()", "reading property '%s', type '%c' (%d)", key, type, type);


      if(type == 'i') {
	int n;
	file->rdwr_int(n);
	set(key, n);
      } else if(type == 'c') {
	char * val;
	file->rdwr_string(val);
	set(key, val);
	delete [] val;
      } else {
	// ignore null props
      }

      delete [] key;
    }
    
    // Debug
    // dump();
  }
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t pprops_t::get_cid() 
{
  return hstore_t::t_pprops;
}



void pprops_t::dump()
{
  const slist_tpl <const char *> *keys = get_keys();

  slist_iterator_tpl <const char *> iter (keys);

  while( iter.next() ) {
    const char * key = iter.get_current();
    const int * i = get_int(key);
    const char * p =  get_string(key);

    dbg->message("pprops_t::dump()", "'%s'\t%p %p\t(%d) (%s)", 
		 key, i, p,
		 i ? *i : -1,
		 p ? p : "null");
  }
  



  delete keys;
}
