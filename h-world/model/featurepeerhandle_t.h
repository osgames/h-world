#ifndef featurepeerhandle_t_h
#define featurepeerhandle_t_h
                
// #include "tpl/handle_tpl.h"

class feature_peer_t;
template <class T> class handle_tpl;

typedef handle_tpl<feature_peer_t> featurepeerhandle_t;

#endif
