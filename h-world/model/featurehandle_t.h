#ifndef featurehandle_t_h
#define featurehandle_t_h
                
// #include "tpl/handle_tpl.h"

class feature_t;
template <class T> class handle_tpl;

typedef handle_tpl<feature_t> featurehandle_t;

#endif
