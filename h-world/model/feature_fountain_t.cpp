/* 
 * feature_fountain.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "feature_fountain_t.h"
#include "thing_t.h"
#include "thing_constants.h"

#include "factories/thing_factory_t.h"
#include "factories/hstore_t.h"

#include "gui/thing_chooser_t.h"

#include "util/debug_t.h"

#include "language/linguist_t.h"

/**
 * Fountains fill bottles with water
 * @author Hj. Malthaner
 */
bool feature_fountain_t::activate(thinghandle_t thing)
{
  dbg->message("feature_fountain_t::activate()", "called"); 

  bool filled = false;
  thing_factory_t factory;
  thinghandle_t root ( thing_t::find_root(thing) );
  thing_chooser_t chooser ("Fill which bottle?",
			   root, 0, "potion", "nothing");
  chooser.center();
  chooser.hook();

  thinghandle_t bottle = chooser.get_choice();

  if(bottle.is_bound()) {
    const char * type = bottle->get_string(TH_TYPE, "");
    const char * subtype = bottle->get_string(TH_SUBTYPE, "");

    dbg->message("feature_fountain_t::activate()", "type='%s' subtype='%s'", type, subtype); 


    if(strcmp(type, "potion") == 0 && strcmp(subtype, "nothing") == 0) {
	  
      thing_t::replace_thing(root, 
			     bottle, 
			     factory.create("water_bottle", 0, 0));

      filled = true;
    }
  }

  if(!filled) {
    linguist_t::translate("You need an empty bottle to fill it with water.");
  } else {
    linguist_t::translate("You fill the empty bottle with water.");
  }

  return filled;
}


// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void feature_fountain_t::read_write(storage_t *store, iofile_t * file)
{
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t feature_fountain_t::get_cid()
{
  return hstore_t::t_feature_fountain;
}
