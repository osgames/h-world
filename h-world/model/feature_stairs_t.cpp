/* 
 * feature_stairs_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "feature_stairs_t.h"
#include "world_t.h"
#include "level_t.h"
#include "thing_t.h"

#include "util/debug_t.h"
#include "language/linguist_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"

#ifndef PPROPS_T_H
#include "pprops_t.h"
#endif


feature_stairs_t::feature_stairs_t()
{
}


feature_stairs_t::feature_stairs_t(const cstring_t & d)
{
  dest = d;
}


/**
 * Uses the stairs.
 * @author Hj. Malthaner
 */
bool feature_stairs_t::activate(thinghandle_t thing)
{
  dbg->message("feature_stairs_t::activate()", 
	       "destination is '%s'", dest.chars());

  linguist_t::translate("Loading/creating new level, please wait ...");

  // check for level indirection

  if(dest[0] == '$') {
    // indirect level name
    level_t *level = world_t::get_instance()->get_level();
    const char * name = level->get_properties()->get_string(dest);
    if(name) {
      world_t::get_instance()->load_level(name, false);
    } else {
      dbg->warning("feature_stairs_t::activate()", 
		   "cannot get level name for exit '%s'", dest.chars());       
    }

  } else {
    // direct level name
    world_t::get_instance()->load_level(dest, false);
  }

  return true;
}


// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void feature_stairs_t::read_write(storage_t *store, iofile_t * file)
{
  if(file->is_saving()) {
    store->store(dest, file);
  } else {
    char * str = (char *)store->restore(file);
    dest = cstring_t(str);
    delete [] str;
  }
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t feature_stairs_t::get_cid()
{
  return hstore_t::t_feature_stairs;
}
