/* Copyright by Hj. Malthaner */

#include "environment_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"


environment_t::environment_t()
{
  gravity = 0;
  humidity = 0;
  lightness = 0;
  loudness = 0;
  temperature = 0;
  def_gravity = 0;
  def_humidity = 0;
  def_lightness = 0;
  def_loudness = 0;
  def_temperature = 0;
}


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void environment_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_char(gravity);
  file->rdwr_char(humidity);
  file->rdwr_char(lightness);
  file->rdwr_char(loudness);
  file->rdwr_char(temperature);
  file->rdwr_char(def_gravity);
  file->rdwr_char(def_humidity);
  file->rdwr_char(def_lightness);
  file->rdwr_char(def_loudness);
  file->rdwr_char(def_temperature);
}
  

/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t environment_t::get_cid()
{
  return hstore_t::t_environment;
}
