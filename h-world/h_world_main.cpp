/* 
 * h_world_main.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <time.h>
#include <string.h>

#include "h_world_main.h"

#include "util/debug_t.h"
#include "util/properties_t.h"
#include "util/rng_t.h"

#include "model/level_t.h"
#include "model/world_t.h"
#include "model/thing_t.h"
#include "model/message_log_t.h"

#include "swt/sdl_window_t.h"
//#include "swt/x11_window_t.h"
#include "swt/system_window_t.h"
#include "swt/window_manager_t.h"
#include "swt/gui_window_t.h"
#include "swt/gui_container_t.h"
#include "swt/tileset_t.h"
#include "view/world_view_t.h"

#include "gui/message_log_view_t.h"
#include "gui/usage_panel_t.h"
#include "gui/stats_panel_t.h"
#include "gui/player_gui_t.h"
#include "gui/help_frame_t.h"

#include "gui/birth_frame_t.h"
#include "gui/multi_choice_t.h"

#include "control/scheduler_t.h"
#include "control/user_input_t.h"
#include "control/lua_call_t.h"

#include "primitives/cstring_t.h"
#include "primitives/invalid_argument_exception_t.h"

// scripting system interface
#include "lua_system.h"

// for the sizes dump
#include "model/square_t.h"
#include "model/ground_t.h"
#include "model/feature_t.h"

#include "guidef.h"

#include "factories/thing_factory_t.h"
#include "factories/feature_factory_t.h"


// Hajo: font file names, see also: fontdef.h
char font_small [128];
char font_standard [128];


extern void h_world_init();


static void dump_sizes()
{
  printf("square_t\t%d\n", sizeof(square_t));
  printf("ground_t\t%d\n", sizeof(ground_t));
  printf("feature_t\t%d\n", sizeof(feature_t));

  printf("visual_t\t%d\n", sizeof(visual_t));
  
  
  printf("slist_tpl<void *>\t%d\n", sizeof(slist_tpl<void *>));
  printf("minivec_tpl<void *>\t%d\n", sizeof(minivec_tpl<void *>));
}


static thinghandle_t birth(properties_t * game_props)
{
  const int width = game_props->get_int("screen_width", 800);
  const int height = game_props->get_int("screen_height", 600);

  // get birth options
  
  birth_frame_t *frame = new birth_frame_t();

  const koord size = frame->get_size();
  
  frame->set_pos(koord(width-size.x+100, height-size.y)/2);
  frame->hook();
  
  thinghandle_t result = frame->choice;
  
  delete frame;  
  frame = 0;


  lua_call_t call ("on_birth");
  call.activate(result, 0);


  result->memorize("quest", game_props->get_string("starting_quest"));

  return result;
}


static thinghandle_t load_or_birth(cstring_t base_path, 
				   cstring_t data_dir,
				   properties_t * game_props)
{
  const int width = game_props->get_int("screen_width", 800);
  const int height = game_props->get_int("screen_height", 600);

  // init to unbound handle = no result;
  thinghandle_t result;

  // Hajo: init features -> preloades feature images
  // it is important that feature factory is initialized first!
  feature_factory_t::init(base_path);

  // init things
  thing_factory_t::init(data_dir);


  window_manager_t *winman = window_manager_t::get_instance();

  // backdrop drawable

  gui_window_t * back = new gui_window_t();
  back->set_pos(koord(0,0));
  back->set_size(koord(width, height));
  back->set_opaque(true);
  back->set_color(color_t::GREY32);
  winman->add_window(back);
  back->set_visible(true);


  // show intro message

  help_frame_t *intro_frame = new help_frame_t(base_path + "/" + "help", 
					       "intro.html", false);
  intro_frame->set_visible(true);
  winman->add_window(intro_frame);

  // ask for new game

  multi_choice_t c_new_game("Start a new game?", "Start a new game?", "y: yes|n: no");

  // Check if there is an old saved game
  cstring_t filename = base_path + "/" + "save/game.save";

  dbg->message("birth()", "Checking for saved game '%s'", filename.chars());
 

  FILE * file = fopen(base_path + "/" + "save/game.save", "rb");
  if(file != 0) {
    fclose(file);

    // Ok, ask if continue

    c_new_game.center();
    c_new_game.hook();
  }


  if(file == 0 || c_new_game.get_choice() == 'y') {
    world_t::get_instance()->new_game(base_path);
    result = birth(game_props);
  }


  // Shut down intro message
  winman->remove_window(intro_frame);
  delete intro_frame;

  winman->remove_window(back);
  delete back;
  back = 0;

  // Hajo: enforce "kill_list" cleanup
  winman->process_kill_list();

  return result;
}



extern int h_world_main(int argc, char ** argv)
{
  cstring_t path (gimme_arg(argc, argv, "-game", 1));

  if(path.len() == 0) {
    path = "./default";
  }

  // dump_sizes();
  
  try {

    // randomize with new seed every program start
    rng_t::get_the_rng().set_seed(time(0));
    

    // init recoloring feature
    visual_t::read_colorshades(path);

    
    // then prepare system window  

    unsigned int sdl_flags = sdl_window_t::F_NONE;
    if(gimme_arg(argc, argv, "-OPENGL", 0)) {
      sdl_flags |= sdl_window_t::F_OPENGL;
    }
    if(gimme_arg(argc, argv, "-fullscreen", 0)) {
      sdl_flags |= sdl_window_t::F_FULLSCREEN;
    }

    system_window_t *syswin = new sdl_window_t(sdl_flags);
    // system_window_t *syswin = new x11_window_t();


    // Hajo: load global configuration

    properties_t config;
    if(config.load("config.txt") == false) {
      dbg->fatal("h_world_main()", "Cannot read 'config.txt'"); 
    }


    // Hajo: load global game settings

    properties_t game_props;
    cstring_t game_props_filename = path + "/data/game.props";
    if(game_props.load(game_props_filename) == false) {
      dbg->fatal("h_world_main()", "Cannot read '%s'", 
		 game_props_filename.chars()); 
    }

    koord screensize (game_props.get_int("screen_width", 800),
		      game_props.get_int("screen_height", 600));
    

    if(gimme_arg(argc, argv, "-screensize", 0) != NULL) {
	const char * res_str = gimme_arg(argc, argv, "-screensize", 1);

	int n = 0;

	if(res_str) {
	    n = sscanf(res_str, "%hdx%hd", &screensize.x, &screensize.y);
	}

	if(n != 2) {
	    printf("invalid argument for -screensize option\n");
	    printf("argument must be of format like -screensize 800x600\n");
	    return 0;
	}
    }


    cstring_t window_title ("H-World CRPG Engine 0.4.3.0 - 22-Jan-05 - Running: ");
    window_title += game_props.get_string("title");

    strcpy(font_small, path);
    strcat(font_small, "/font/hajo_6x11p.bdf");

    strcpy(font_standard, path);
    strcat(font_standard, "/font/hajow_7x13p.bdf");

    dbg->message("main()", "font_standard=%s", font_standard);


    if(syswin->initialize(screensize.x, screensize.y, 
			  window_title,
			  font_small)) {

      h_world_init();

      // set up window manager
      window_manager_t *winman = new window_manager_t(syswin);

      int viewtype = game_props.get_int("view_type", 0);

      if(gimme_arg(argc, argv, "-ISO", 0) != NULL) {
	viewtype = 0;
      }
      if(gimme_arg(argc, argv, "-2D", 0) != NULL) {
	viewtype = 1;
      }
      if(gimme_arg(argc, argv, "-3D", 0) != NULL) {
	viewtype = 2;
      }

      world_view_t::create_instance(viewtype, &game_props);

      world_view_t *view = world_view_t::get_instance();
      if(view->initialize(path)) {

	view->dither_grounds = 
	  *config.get_string("dither_grounds", "t") == 't';


	// init lua system
	lua_init(path);

	props_display_t::set_path(path);
	user_input_t::set_path(path);

	message_log_t * log = message_log_t::get_instance();
	
	log->add_message(game_props.get_string("intro_line_1"));
	log->add_message(game_props.get_string("intro_line_2"));
	log->add_message(game_props.get_string("intro_line_3"));
	log->add_message(game_props.get_string("intro_line_4"));
	log->add_message(game_props.get_string("intro_line_5"));


	view->set_world_view_ifc(world_t::get_instance());

	
	// set up world view
	view->set_pos(koord(140,60));
	view->set_size(screensize - koord(140, 60+80));
	winman->add_window(view);
	view->set_visible(true);
	

	// create world
	cstring_t dirname ( path + "/data/" );
	cstring_t filename ( "level001.props" );
	  

	// init world
	world_t::get_instance()->init(view, &game_props);

	// create new player
	thinghandle_t player = load_or_birth(path, dirname, &game_props);

	// new character? -> new game
	if(player.is_bound()) {

	  if(gimme_arg(argc, argv, "-level", 0)) {
	    filename = gimme_arg(argc, argv, "-level", 1);
	  } else {
	    filename = player->get_string("start_level", "level001.props");
	  }

	  world_t::get_instance()->set_player(player);
	  world_t::get_instance()->load_level(dirname+filename, true);


	} else {
	  // no character? -> load old game
	  // must be done after UI is initialized!
	  // see below
	}

	gui_window_t *win;	

	// set up usage view
	win = new gui_window_t();
	win->set_pos(koord(0, screensize.y-80));
	win->set_size(koord(screensize.x, 80));
	win->set_opaque(true);
	win->set_background(view->get_tileset(2)->get_tile(BACK_TILE));
	usage_panel_t *upan = new usage_panel_t();
	// upan->set_model(world_t::get_instance()->get_player());
	world_t::get_instance()->get_user_input()->set_usage_panel(upan);
	upan->set_size(win->get_size());
	win->add_component(upan);
	win->set_visible(true);
	winman->add_window(win);
	
	
	// set up stats view
	win = new gui_window_t();
	win->set_pos(koord(0,0));
	win->set_size(koord(140, screensize.y - 80));
	win->set_opaque(true);
	win->set_background(view->get_tileset(2)->get_tile(BACK_TILE));
	stats_panel_t *pan = new stats_panel_t();
	
	pan->set_pos(koord(0,0));
	pan->set_size(win->get_size());
	pan->set_opaque(false);
	win->add_component(pan);
	winman->add_window(win);
	win->set_visible(true);


	// set up message view
	win = new gui_window_t();
	win->set_pos(koord(140, 0));
	win->set_size(koord(screensize.x - 140, 60));
	win->set_opaque(true);
	win->set_background(view->get_tileset(2)->get_tile(BACK_TILE));
	message_log_view_t *mview = new message_log_view_t();
	mview->set_model(message_log_t::get_instance());
	mview->set_size(win->get_size());
	win->add_component(mview);
	winman->add_window(win);
	win->set_visible(true);

	
	// init player UI
	player_gui_t::get_instance()->init(pan, upan);


	// no character? -> load old game
	if(!player.is_bound()) {
	  load_file("save/game.save");
	} else {
	  // make sure to kill reference
	  player = 0;
	}
	

	// link player UI
	player_gui_t::get_instance()->link(world_t::get_instance()->get_player());

	// Make sure UI is now redrawn
	// winman->redraw();
	
	int state;
	
	do {
	  // make world alive
	  state = scheduler_t::get_instance()->process(syswin, winman);

	  if(state == scheduler_t::GAME_OVER) {
	    help_frame_t *frame = new help_frame_t("html/game_over.html", true);
	    frame->set_pos(koord(350,170));
	    frame->hook();	  

	    minivec_tpl <thinghandle_t> things_in_transit (128);
	    world_t::get_instance()->destroy(things_in_transit);

	    world_t::get_instance()->init(view, &game_props);
	    world_t::get_instance()->set_player(birth(&game_props));
	    world_t::get_instance()->load_level(dirname+filename, true);


	    player_gui_t::get_instance()->link(world_t::get_instance()->get_player());

	    view->set_dirty(true);
	    view->redraw();
	    winman->get_and_process_events();
	  }
	  
	  
	} while(state != scheduler_t::QUIT);
	
	// shut down
	
	winman->remove_all_windows();
	delete winman;
	winman = NULL;
	
	syswin->close();
	delete syswin;
	syswin = NULL;
	
	// tear down lua system
	lua_exit();
	
	
      } else {
	dbg->fatal("main()", "Cannot initialize world view.");
      }
      
    } else {
      dbg->fatal("main()", "Cannot initialize system window.");
    }
    
  } catch(invalid_argument_exception_t iax) {
    dbg->fatal("main()", iax.reason);
  } catch(no_such_element_exception nse) {
    dbg->fatal("main()", "No such element exception, no further info available");
  }
  
  return 0;
}
