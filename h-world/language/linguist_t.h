/* Copyright by Hj. Malthaner */

#ifndef LINGUIST_T_H
#define LINGUIST_T_H


/**
 * This class is responible for generating reasonable sentences in the 
 * current output language.
 * @author Hj. Malthaner
 */
class linguist_t
{


 public:
  
  /**
   * Output a sentance of "sub verb obj" i.e. "The player hits the jackal"
   * XXX unfinished!
   * @author Hj. Malthaner
   */
  void static svo(const char * sub, const char * verb, const char * obj);

  void static svp(const char * sub, const char * verb);


  void static svpo(const char * sub, const char * verb, 
		   const char *pers, const char * obj);

  void static svso(const char * sub, const char * verb, 
		   const char *pers, const char * obj);

  void static pvso(const char * sub, const char * verb, 
		   const char *pers, const char * obj);
  
  /**
   * Output a sentance of "pronoun verb obj" i.e. "You hit the jackal"
   * XXX unfinished!
   * @param definite use a definite or indefinite articel (true=the, false=a)
   * @author Hj. Malthaner
   */
  void static pvo(const char * sub, const char * verb, 
		  bool definite, const char * obj);


  /**
   * Output a sentance of "pronoun verb adjective obj" i.e. 
   * "You hit the wounded jackal"
   * 
   * @param definite use a definite or indefinite article (true=the, false=a)
   * @author Hj. Malthaner
   */
  void static pvao(const char * sub, const char * verb, 
		   bool definite, const char *adj, const char * obj);


  /**
   * Output a sentance of "sub verb" i.e. "The jackal dies"
   * XXX unfinished!
   * @author Hj. Malthaner
   */
  void static sv(const char * sub, const char * verb);

  /**
   * Translates and output a fixed message.
   * XXX unfinished!
   * @author Hj. Malthaner
   */
  void static translate(const char * msg);

};


#endif
