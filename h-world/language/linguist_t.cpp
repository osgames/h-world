/* linguist_t.cpp
 *
 * English sentance-creator
 *
 * Copyright by Hj. Malthaner 
 */

#include "linguist_t.h"
#include "model/message_log_t.h"

#include <stdio.h>
#include <string.h>

const char vowels[6] = "aeiou"; 


/**
 * Checks if a character is a vowel
 * @author Hj. Malthaner
 */
static inline bool is_vowel(char c) {
  bool ok = false;

  for(int i=0; i<5; i++) {
    if(c == vowels[i]) {
      ok = true;
      break;
    }
  }

  return ok;
}


/**
 * Tries to guess 3rd person ending of a verb
 * @author Hj. Malthaner
 */
static const char * verb_s(const char * verb) 
{
  const char verb_end = verb[strlen(verb)-1];

  const char *vs = "s";

  if(verb_end == 'h' || verb_end == 's') {
    vs = "es";
  }

  return vs;
}


/**
 * Determines whether a possesiv s or a ' is needed
 * @author Hj. Malthaner
 */
static const char * poss_s(const char * noun) 
{
  const char noun_end = noun[strlen(noun)-1];

  const char *ps = (noun_end == 's') ? "'" : "s";

  return ps;
}


/**
 * Output a sentance of "sub verb obj" i.e. "The arrow hits the jackal"
 * XXX unfinished!
 * @author Hj. Malthaner
 */
void linguist_t::svo(const char * sub, const char * verb, const char * obj)
{
  char buf[256];

  const char *vs = verb_s(verb);

  sprintf(buf, "The %s %s%s the %s.", sub, verb, vs, obj);

  message_log_t::get_instance()->add_message(buf);
}


void linguist_t::svp(const char * sub, const char * verb)
{
  char buf[256];

  const char *vs = verb_s(verb);

  sprintf(buf, "The %s %s%s you.", sub, verb, vs);

  message_log_t::get_instance()->add_message(buf);
}


void linguist_t::pvso(const char * sub, const char * verb, 
		 const char *pers, const char * obj)
{
  char buf[256];
  const char * ps = poss_s(pers); 

  sprintf(buf, "%s %s the %s%s %s.", sub, verb, pers, ps, obj);
  
  message_log_t::get_instance()->add_message(buf);
}


void linguist_t::svso(const char * sub, const char * verb, 
		 const char *pers, const char * obj)
{
  char buf[256];
  const char * vs = verb_s(verb);
  const char * ps = poss_s(pers); 

  sprintf(buf, "The %s %s%s the %s%s %s.", sub, verb, vs, pers, ps, obj);

  message_log_t::get_instance()->add_message(buf);
}


void linguist_t::svpo(const char * sub, const char * verb, 
		 const char *pers, const char * obj)
{
  char buf[256];
  const char * vs = verb_s(verb);

  sprintf(buf, "The %s %s%s %s %s.", sub, verb, vs, pers, obj);

  message_log_t::get_instance()->add_message(buf);
}


/**
 * Output a sentance of "pronoun verb obj" i.e. "You hit the jackal"
 * XXX unfinished!
 * @param definite use a definite or indefinite articel (true=the, false=a)
 * @author Hj. Malthaner
 */
void linguist_t::pvo(const char * sub, const char * verb, 
		     bool definite, const char * obj)
{
  char buf[256];

  const char * article = "";

  // plural needs no article

  if(obj[strlen(obj)-1] != 's') {
    article = definite ? "the " : is_vowel(*obj) ? "an " : "a ";
  }


  sprintf(buf, "%s %s %s%s.", 
	  sub, verb, article, obj);

  message_log_t::get_instance()->add_message(buf);
}


/**
 * Output a sentance of "pronoun verb adjective obj" i.e. 
 * "You hit the wounded jackal"
 * 
 * @param definite use a definite or indefinite article (true=the, false=a)
 * @author Hj. Malthaner
 */
void linguist_t::pvao(const char * sub, const char * verb, 
		      bool definite, const char *adj, const char * obj)
{
  char buf[256];

  if(*adj) {
    sprintf(buf, "%s %s", adj, obj);
    pvo(sub, verb, definite, buf);
  } else {
    pvo(sub, verb, definite, obj);
  }
}



/**
 * Output a sentance of "sub verb" i.e. "The jackal dies"
 * XXX unfinished!
 * @author Hj. Malthaner
 */
void linguist_t::sv(const char * sub, const char * verb)
{
  char buf[256];
  const char *vs = verb_s(verb);

  sprintf(buf, "The %s %s%s.", sub, verb, vs);

  message_log_t::get_instance()->add_message(buf);
}


/**
 * Translates and output a fixed message.
 * XXX unfinished!
 * @author Hj. Malthaner
 */
void linguist_t::translate(const char * msg)
{
  message_log_t::get_instance()->add_message(msg);
}
