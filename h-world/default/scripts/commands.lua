----------------------------------------------------------------
-- commands.lua
--
-- H-World user commands script. Called if the user presses a key
-- this scrip can handle all user input that is not mapped to a
-- default action
--
-- Author: Hj. Malthaner
-- Date:   11-May-2003
-- Update: 01-Nov-2004 - Hj. Malthaner: added "on_thing_clicked"
----------------------------------------------------------------


------------------------------------------------------------------
-- Called if the user presses a key
--
-- author: Hj. Malthaner
-- date:   11-May-2003
-- update: 11-May-2003
--
-- param user:   type thing_t, the player character
-- param code:   key code
-- return:       delay (duration of action)
------------------------------------------------------------------


function on_key_pressed(user, code)
	local delay = 0       -- 0 means not to quit the input loop yet

	-- just an example how to use commands
	-- code 32 is SPACE

	if(code == 32) then
		translate("Example user defined command script for key SPACE was triggered.")
                -- ten is the standard duration, it equals
		-- roughly the time to make one step while 
		-- walking

		local x, y
		x,y = thing_get_location(user)

		translate("You are at location " .. x .. ", " .. y .. ".");

		delay = 10
	end

	-- code 'c': command followers
	if code == 99 then
		delay = command_followers(user);
	end

	-- code '^', place a trap
	if code == 94 then
		translate("Which type of trap do you want to place [0,1]?")
		local type = user_ask_key() - 48

		local x, y
		x,y = thing_get_location(user)

		square_set_trigger(x, y, type)

		translate("Placing a trap of type " .. type .. ".")

		delay = 10
	end


	return delay
end


------------------------------------------------------------------
-- Called if the user clicks a thing
--
-- author: Hj. Malthaner
-- date:   01-Nov-2004
-- update: 01-Nov-2004
--
-- param user:   type thing_t, the player character
-- param thing:  type thing_t, the thing that was clicked
-- return:       delay (duration of action), pass -1 for "not done"
------------------------------------------------------------------

function on_thing_clicked(user, thing)

	local ident = thing_get_ident(thing)
	local status = thing_get_value(thing, "status")
	local alive = thing_get_value(thing, "alive") or "false"

	local message = "You see a " .. ident .. "." 
	local buttons = nil
	local delay = -1

	local px, py = thing_get_location(user)
	local tx, ty = thing_get_location(thing)

	local dx = abs(px-tx)
	local dy = abs(py-ty)

	_ALERT("dx=".. dx .. " dy=" .. dy .. "\n")

	if (status == nil) then

		if (alive ~= "true") then

			if(dx <= 1 and dy <= 1) then
				if (thing_get_value(thing, "inventory")) then
					buttons = "g: get it|o: open it|l: leave it";
				else
	  				buttons = "g: get it|l: leave it";
				end
			end
		else 
			-- dont know
		end

	elseif (status == "neutral") then
		local diarad = thing_get_value(thing, "dialog.radius") or 0

		_ALERT("dialof.radius=" .. diarad .. "\n")

		if(diarad >= dx+dy) then
			buttons = "t: talk|l: leave it";
		end

	elseif (status == "friend") then
		buttons = "c: give command|i: inpect it|T: trade|l: leave it";
	end


	if(buttons) then
		local choice = show_multi_choice("Please select",
			                         message,
			                         buttons)

		if (choice == 84) then
			-- 'T'rading
			delay = user_cmd_trade(user, thing)
		elseif (choice == 103) then
			-- 'g'et it
			delay = user_cmd_get(user, thing)
		elseif(choice == 105) then
			-- 'i'nspect it
			delay = user_cmd_inspect(user, thing)

		elseif (choice == 111) then
			-- 'o'pen it
			delay = user_cmd_open(user, thing)

		elseif (choice == 116) then
			-- 't'alk to it
			delay = user_cmd_talk(user, thing)

		elseif (choice == 99) then
			-- 'c'ommand it
			delay = command_followers_aux(user, thing)
		end

	end

	return delay
end



------------------------------------------------------------------
-- Ask the user for a command, and executes the command on a 
-- follower
--
-- author: Hj. Malthaner
-- date:   17-Oct-2004
-- update: 01-Nov-2004 - Hj. Malthaner: now uses command_followers_aux()
--
-- param user:   type thing_t, the player character
-- return:       delay (duration of action)
------------------------------------------------------------------


function command_followers(user)

	local x, y, xd, yd
	local delay = 0

	x, y = thing_get_location(user)

	xd, yd = user_ask_direction(10)    -- up to 10 squares away is ok

	x = x + xd
	y = y + yd


	-- find thing there

	local n = 0
	local thing = square_get_thing(x, y, n)

	if(thing ~= nil and 
	   thing_get_value(thing, "status") == "friend") then

		delay = command_followers_aux(user, thing)
	end

	return delay
end


------------------------------------------------------------------
-- Ask the user for a command, and executes the command on a 
-- follower
--
-- author: Hj. Malthaner
-- date:   01-Nov-2004
-- update: 01-Nov-2004
--
-- param user:   type thing_t, the player character
-- param thing:  type thing_t, the follower
-- return:       delay (duration of action)
------------------------------------------------------------------


function command_followers_aux(user, thing)

	local delay = 0

	if(thing ~= nil and 
	   thing_get_value(thing, "status") == "friend") then

		local choice = 
			show_multi_choice("Command a follower",
			                  "Which order do you want to give?",
			                  "d: disband|w: wait|f: follow me|n: no orders")


		if(thing == user) then
			translate("You give orders to yourself.")
		else
			if(choice == 100) then
				translate("You disband the " .. thing_get_ident(thing) .. ".")
				thing_set_value(thing, "status", "neutral")
			end
			if(choice == 119) then
				translate("You order the " .. thing_get_ident(thing) .. " to wait.")
				thing_set_value(thing, "immobile", "true")
			end
			if(choice == 102) then
				translate("You order the " .. thing_get_ident(thing) .. " to follow you.")
				thing_set_value(thing, "immobile", "false")
			end
		end
	end

	return delay
end