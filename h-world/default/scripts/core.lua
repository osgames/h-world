----------------------------------------------------------------
-- core.lua
--
-- This file contains Lua functions that are called by the H-World
-- engine core. They are meant to allow further extensibility and
-- configurability.
-- 
-- Author:    Hj. Malthaner
-- Creation:  28-Apr-04
-- Update:    02-May-04
-- Update:    15-May-04 - Hj. Malthaner: added "on_kill" function
-- Update:    02-Jul-04 - Hj. Malthaner: added "on_creation" function
-- Update:    07-Jul-04 - Hj. Malthaner: added item socket creation
-- Update:    09-Nov-04 - Hj. Malthaner: added "on_birth" function
----------------------------------------------------------------


------------------------------------------------------------------
-- Helper table to transform item colors to names
-- author: Hj. Malthaner
------------------------------------------------------------------

item_color_table =
{
	"",		-- 0
	"red ",		-- 1
	"orange ",	-- 2
	"ochre ",	-- 3
	"yellow ",	-- 4
	"lime green ",	-- 5
	"green ",	-- 6
	"turquoise ",	-- 7
	"cyan ",	-- 8
	"blue ",	-- 9
	"aquamarine ",	-- 10
	"violet ",	-- 11
	"purple ",	-- 12
	"pink ",	-- 13
	"grey ",	-- 14
	"silver ",	-- 15
	"dark ",	-- 16
	"",
	"",
	"",
	"",
}


------------------------------------------------------------------
-- this function is called by the engine to determine an item
-- identifier string. Different games will need different ident
-- strings, thus this functionality was moved into this script.
--
-- author: Hj. Malthaner
-- date:   28-Apr-04
-- update: 28-Apr-04
--
-- param thing:     type thing_t, the thing to check
-- param memorized  nil if not memorized, non-nil if memorized
-- return:          ident string
------------------------------------------------------------------

function calculate_ident_string(thing, memorized)
	-- _ALERT("lua: item ident called\n")

	local item_ident = "";

	-- Hajo: check for known curses. Tell if player knows about a curse
	-- only check items, cursed monsters are irrelevant here

	if(thing_get_value(thing, "alive") == nil) then
		local curse_known = thing_get_value(thing, "curse_known") or "false"

		if(curse_known == "true") then
			local cursed = thing_get_value(thing, "cursed")
      
			if(cursed == nil) then
				-- plain item
				item_ident = "uncursed "
	      		else 
				if(cursed == "false") then
					item_ident = "uncursed "
      				else
					item_ident = "cursed "
    	  			end
			end
	    	else
    			-- player doesn't know ...
    		end
	end

	-- check for magic attributes

	local prefix = thing_get_value(thing, "att-0.prefix")
	local postfix = nil
	local rare = nil
  	local unknown = thing_get_value(thing, "is_unknown")
	local ident = nil


  	-- already memorized ?

	if(memorized) then
    		-- set to known
    		unknown = "false";
  	end


  	if(unknown == nil and prefix == nil) then
    	-- Unidentified easy known item
    	ident = thing_get_value(thing, "ident")

  	else 
		if(unknown and unknown ~= "true") then

    			-- Identified item

    			ident = thing_get_value(thing, "identified_ident")

			if(ident == nil) then
      				-- Identified easy known item
		    		ident = thing_get_value(thing, "ident")
    			end


    			-- Check number of magic attributes

    			-- rare item ?
    			rare = thing_get_value(thing, "att-2.prefix")
    
    			if(rare) then
      				rare = "special "
    			end

    			postfix = thing_get_value(thing, "att-1.postfix")

  		else
    			-- Items that must be identifed first

    			ident = thing_get_value(thing, "ident")

			-- Get colors for flavored items
			local colorset = thing_get_value(thing, "colorset")

			if(colorset) then
				ident = item_color_table[(colorset+1)] .. ident
			end


		    	if(prefix) then
      				-- don't show real prefix until identified
      				prefix = "magic"
    			end
  		end
	end

  	-- ALERT("thing_t::get_ident()", "prefix=%s unknown=%s ident=%s", prefix, unknown, ident); 

  	if(prefix) then

		if(rare) then
      		item_ident = item_ident .. rare
    	end

    	item_ident = item_ident .. prefix .. " " .. ident

    	if(postfix) then
      		item_ident = item_ident .. " " .. postfix
    	end

  	else
	    item_ident = item_ident .. ident
  	end

	-- _ALERT("lua: item ident is '" .. item_ident .. "'\n")

  	return item_ident
end


------------------------------------------------------------------
-- this function is similar to calculate_ident_string() except
-- that it doesn't include curses, prefixes and postfixes
--
-- author: Hj. Malthaner
-- date:   02-May-04
-- update: 02-May-04
--
-- param thing:     type thing_t, the thing to check
-- param memorized  nil if not memorized, non-nil if memorized
-- return:          ident string
------------------------------------------------------------------

function calculate_plain_ident_string(thing, memorized)
	-- _ALERT("lua: plain item ident called\n")

	local item_ident = "";

	-- check if this item is known

  	local unknown = thing_get_value(thing, "is_unknown")

  	-- or if the type is already memorized ?

	if(memorized) then
    		-- set to known
    		unknown = "false";
  	end


  	if(unknown == nil) then
    		-- Unidentified easy known item
    		ident = thing_get_value(thing, "ident")

  	else 
		if(unknown and unknown ~= "true") then

    			-- Identified item

    			ident = thing_get_value(thing, "identified_ident")

			if(ident == nil) then
      				-- Identified easy known item
		    		ident = thing_get_value(thing, "ident")
    			end
  		else
    			-- Items that must be identifed first
    			ident = thing_get_value(thing, "ident")
  		end
	end

    	item_ident = ident

	-- _ALERT("lua: plain item ident is '" .. item_ident .. "'\n")

  	return item_ident
end


------------------------------------------------------------------
-- Check if the given item can be held by the limb (i.e. check
-- size, type ...)
--
-- author: Hj. Malthaner
-- date:   06-May-04
-- update: 06-May-04
--
-- param limb:      type thing_t, the limb to hold the item
-- param thing:     type thing_t, the thing to check
-- return:          1 if ok, 0 otherwise
------------------------------------------------------------------


function check_item_constraints(limb, thing)

	-- check size

	local width  = thing_get_value(thing, "width") or 0
	local height = thing_get_value(thing, "height") or 0

	local max_w = thing_get_value(limb, "hold_size.x") or 0
	local max_h = thing_get_value(limb, "hold_size.y") or 0


	if(width > max_w or height > max_h) then
      		_ALERT("check_item_constraints: too big\n");
		return 0
	end


    	-- Hajo: clothes must fit. 
    	-- Default is always fit.

    	local fwidth  = thing_get_value(thing, "fit_width") or max_w 
    	local fheight = thing_get_value(thing, "fit_height") or max_h 
  
	local wdiff = fwidth - max_w
	local hdiff = fheight - max_h

    	if(wdiff > 1 or wdiff < -1 or hdiff > 1 or hdiff < -1) then
      		_ALERT("check_item_constraints: doesn't fit\n");
      		return 0
    	end


  	-- Hajo: constraints are checked here
    
    	local count = thing_get_value(limb, "constraints") or 0
    	local type = thing_get_value(thing, "type") or "X"

	if(count == 0) then
		return 1
	end

    	for i = 0, count, 1 do 
		local constraint = "constraint[" .. i .. "]"
		local con_type = thing_get_value(limb, constraint) or "Y"

		-- _ALERT("check_item_constraints: " .. i .. " con_type=" .. con_type .. " type=" .. type .. "\n");


		if(type == thing_get_value(limb, "constraint["..i.."]")) then
			return 1
		end
    	end

  	return 0
end


------------------------------------------------------------------
-- helper table to pass paramaters to birth callback
------------------------------------------------------------------
birth_globals = 
{
	skillpoints = 3,
	fighterskill = 0,
	thiefskill = 0,
	wizardskill = 0
}


------------------------------------------------------------------
-- This is called after the basic PC type is chosen
--
-- author: Hj. Malthaner
-- date:   09-Nov-04
-- update: 20-Nov-04: Hj. Malthaner - added name evaluation
--
-- param thing:     type thing_t, the thing to be destroyed
-- param dummy:     type number, currently always 0
-- return:          always 0 so far
------------------------------------------------------------------

function on_birth(thing, dummy)
	_ALERT("lua: on_birth() called\n")	

	local frame = frame_create("Birth options", "birth_callback",
                                   250, 130, 300, 210)

	birth_globals["thing"] = thing
	birth_globals["frame"] = frame


	birth_globals["ok"] =
		frame_add_buttonlabel(frame, "o: ok", -1, 130, 160, 40, 16)


	birth_globals["name_label"] =
		frame_add_buttonlabel(frame, 
				      "Please enter your name:",
                                      -1, 20, 10, 200, 16)

	birth_globals["name"] =
		frame_add_buttonlabel(frame, 
				      "Unnamed",
                                      -1, 40, 30, 140, 16)
		


	birth_globals["label"] =
		frame_add_buttonlabel(frame, 
				      "You have " ..
				      birth_globals["skillpoints"] ..
				      " skill points to distribute:",
                                      -1, 20, 60, 200, 16)

	birth_globals["fighter"] =
		frame_add_buttonlabel(frame, 
				      "f: Fighter skills: 0", 
				      -1, 40, 80, 130, 16)

	birth_globals["thief"] =
		frame_add_buttonlabel(frame, 
				      "t: Thief skills: 0", 
				      -1, 40, 100, 130, 16)

	birth_globals["wizard"] =
		frame_add_buttonlabel(frame, 
				      "w: Wizard skills: 0", 
				      -1, 40, 120, 130, 16)


	buttonlabel_set_text_offset(birth_globals["name_label"], 8, 2);
	buttonlabel_set_text_offset(birth_globals["label"], 8, 2);

	buttonlabel_set_text_offset(birth_globals["name"], 8, 2);
	buttonlabel_set_editable(birth_globals["name"], 1);


	buttonlabel_set_text_offset(birth_globals["fighter"], 8, 2);
	buttonlabel_set_text_offset(birth_globals["thief"], 8, 2);
	buttonlabel_set_text_offset(birth_globals["wizard"], 8, 2);

	frame_show(frame)

	return 0
end



------------------------------------------------------------------
-- Helper function to handle button presses in birth UI
--
-- author: Hj. Malthaner
-- date:   09-Nov-04
-- update: 20-Nov-04: Hj. Malthaner - added name evaluation
------------------------------------------------------------------

function birth_callback(buttonlabel)
	_ALERT("lua: birth_callback() called\n")	


	if(birth_globals["fighter"] == buttonlabel and
           birth_globals["skillpoints"] > 0) then
		birth_globals["skillpoints"] = birth_globals["skillpoints"] - 1
		birth_globals["fighterskill"] = birth_globals["fighterskill"] + 1
		buttonlabel_set_text(buttonlabel,
				     "f: Fighter skills: " ..
				     birth_globals["fighterskill"])
	end

	if(birth_globals["thief"] == buttonlabel and
           birth_globals["skillpoints"] > 0) then
		birth_globals["skillpoints"] = birth_globals["skillpoints"] - 1
		birth_globals["thiefskill"] = birth_globals["thiefskill"] + 1
		buttonlabel_set_text(buttonlabel,
				     "t: Thief skills: " ..
				     birth_globals["thiefskill"])
	end

	if(birth_globals["wizard"] == buttonlabel and
           birth_globals["skillpoints"] > 0) then
		birth_globals["skillpoints"] = birth_globals["skillpoints"] - 1
		birth_globals["wizardskill"] = birth_globals["wizardskill"] + 1
		buttonlabel_set_text(buttonlabel,
				     "f: Fighter skills: " ..
				     birth_globals["wizardskill"])

	end


	buttonlabel_set_text(birth_globals["label"],
		  	     "You have " ..
			     birth_globals["skillpoints"] ..
			     " skill points to distribute:")


	if(birth_globals["ok"] == buttonlabel) then
		thing_set_value(birth_globals["thing"],
				"birth_name",
                                buttonlabel_get_text(birth_globals["name"]))

		frame_destroy(birth_globals["frame"])
	end
end


------------------------------------------------------------------
-- This is called after a thing is created by the thing factory
-- This function is the place to do game-specific customizations
-- of items and monsters
--
-- author: Hj. Malthaner
-- date:   02-Jul-04
-- update: 02-Jul-04
--
-- param thing:     type thing_t, the thing that just was created
-- param n:         type number, always 0 so far
-- return:          always 0 so far
------------------------------------------------------------------

function on_creation(thing, n)

	-- place game specific item/monster customizations code here


	-- "The Jungle" game module has items with sockets
        -- add sockets to some items only, by random

	if(rng_get_int(10) <= 0) then

		local sock_max = thing_get_value(thing, "sock_max") or 0
		local sock_act = 0
	
		if(sock_max > 0) then
			sock_act = rng_get_int(sock_max) + 1
		end

		-- set actual number of sockets
		thing_set_value(thing, "sock_act", sock_act)

		-- add socket images
    		for i = 0, sock_act-1, 1 do 
			add_socket_or_gem(thing, i, sym_sock_inv, 0);
		end
	end


	return 0
end


------------------------------------------------------------------
-- This is called if a things is about to be destroyed/killed. It is called
-- immediately before the data structures are cleaned up
--
-- author: Hj. Malthaner
-- date:   15-May-04
-- update: 30-Oct-04: Hj. Malthaner - added handling of leftovers
--
-- param thing:     type thing_t, the thing to be destroyed
-- param noisy:     type number, 0 if thing dies noisy
-- return:          always 0 so far
------------------------------------------------------------------

function on_kill(thing, noisy)

	-- treasure drops and magix effects bound to a death
	-- can be handeld here.

	local leftovers = thing_get_value(thing, "leftovers") or 0
	local n = 0
	local x,y

	x,y = thing_get_location(thing)
	
	while(n < leftovers) do
		local key = "leftover[" .. n .. "]"
		local value = thing_get_value(thing, key)

		if(value) then
			local item = thing_create(value, 0, 0)

			if(item) then
				square_put_thing(x, y, item)
			end
		end

		n = n + 1
	end

	if(noisy == 0) then
		translate_sv(calculate_plain_ident_string(thing, 1), "die")
	end

	return 0
end


