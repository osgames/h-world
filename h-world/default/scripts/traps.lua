----------------------------------------------------------------
-- traps.lua
--
-- H-World traps script. All trap efffects are scripted here.
-- Traps may include switches and non-harmful effects
--
--
-- Author:  Hj. Malthaner
-- Created: 08-Nov-2003
-- Update:  08-Nov-2003
-- Update:  12-May-2004: Hj. Malthaner - added feature activations
----------------------------------------------------------------




------------------------------------------------------------------
-- Called if a thing triggers a trap
--
-- author: Hj. Malthaner
-- date:   08-Nov-2003
-- update: 08-Nov-2003
--
-- param thing:  type thing_t, the thing that triggered the trap
-- param x:      x position of trap
-- param y:      y position of trap
-- param type:   type of trap
------------------------------------------------------------------



function trap_triggered (thing, x, y, type)
	-- translate("Trap triggered x=" .. x .. " y=" .. y .. " type=" .. type)

	-- damage trap
	if type == 0 then
		local damage = 5 + rng_roll(2, 6)

    		thing_set_value(thing, 
                	        "HPcurr", 
                    		(thing_get_value(thing, "HPcurr") or 0) - damage)
		
		-- Let player and only player know what happened
		if(thing_get_value(thing, "is_player")) then
			translate("Something snaps your feet!")

			if(damage > 12) then
				translate("It really hurts!")
			elseif(damage > 8) then
				translate("It hurts!")
			else 
				translate("It hurts somewhat.")
			end
		end
	end


	-- poison trap
	if type == 1 then
		local damage = 5 + rng_roll(2, 3)
		thing_set_value(thing, "effect.poison", damage)

		-- Let player and only player know what happened
		if(thing_get_value(thing, "is_player")) then
			translate("A small dart hits you. You feel sick.")
		end
	end


	-- confusion trap
	if type == 2 then

	end


	-- introduction story
	if type == 128 then
		-- stop player automoving
		thing_set_value(thing, "AI.state", "nothing")

		show_html(200, 120, MODULE_PATH .. "help", "enter_village.html")
	end

end


------------------------------------------------------------------
-- Called if a thing runs into a tree (see data/features.sects)
--
-- author: Hj. Malthaner
-- date:   12-May-2004
--
-- param thing:  type thing_t, the thing that triggered the trap
-- param n:      type number, always 0 so far 
-- return:       always 0 so far
------------------------------------------------------------------

function bump_tree(user, n)

	translate("A tree is blocking your way!")
	return 0
end


------------------------------------------------------------------
-- Called if a thing runs into a wall (see data/features.sects)
--
-- author: Hj. Malthaner
-- date:   12-May-2004
--
-- param thing:  type thing_t, the thing that triggered the trap
-- param n:      type number, always 0 so far 
-- return:       always 0 so far
------------------------------------------------------------------

function bump_wall(user, n)

	translate("A wall is blocking your way!")
	return 0
end

------------------------------------------------------------------
-- Called if a thing runs into someting immovable (see data/features.sects)
--
-- author: Hj. Malthaner
-- date:   12-May-2004
--
-- param thing:  type thing_t, the thing that triggered the trap
-- param n:      type number, always 0 so far 
-- return:       always 0 so far
------------------------------------------------------------------

function bump_immovable(user, n)

	translate("This is either fixed or too heavy to be moved.")
	return 0
end
