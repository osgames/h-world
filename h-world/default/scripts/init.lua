----------------------------------------------------------------
-- init.lua
--
-- H-World script initialition script, purpose is to load
-- all required scripts
--
-- Author: Hj. Malthaner
-- Created: 20-Mar-2002
-- Update: 28-Apr-2004 - Hj. Malthaner, added status icons
-- Update: 30-May-2004 - Hj. Malthaner, added skin definitions
-- Update: 10-Jun-2004 - Hj. Malthaner, added skin definitions
-- Update: 17-Oct-2004 - Hj. Malthaner, introduced global MODULE_PATH
----------------------------------------------------------------



-- global values


-- skin definitions, used by engine -> order is fixed!

load_tile_from_image(MODULE_PATH .. "graph/skin/menuback.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/inveback.png")

-- window borders

load_tile_from_image(MODULE_PATH .. "graph/skin/wibo_bot.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/wibo_left.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/wibo_right.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/tiba_left.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/tiba_mid.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/tiba_right.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/wico_left.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/wico_right.png")

-- in-window borders

load_tile_from_image(MODULE_PATH .. "graph/skin/bico_topleft.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/bico_topright.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/bico_botleft.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/bico_botright.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/bibo_top.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/bibo_bot.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/bibo_left.png")
load_tile_from_image(MODULE_PATH .. "graph/skin/bibo_right.png")

load_tile_from_image(MODULE_PATH .. "graph/skin/itemback.png")

-- global overlay images, used by scripts

sym_hurt   = load_tile_from_image(MODULE_PATH .. "graph/symbols/hurt.png")
sym_hunger = load_tile_from_image(MODULE_PATH .. "graph/symbols/hunger.png")
sym_thirst = load_tile_from_image(MODULE_PATH .. "graph/symbols/thirst.png")
sym_poison = load_tile_from_image(MODULE_PATH .. "graph/symbols/poison.png")
sym_frozen = load_tile_from_image(MODULE_PATH .. "graph/symbols/frozen.png")
sym_sleep  = load_tile_from_image(MODULE_PATH .. "graph/symbols/sleep.png")


sym_go_fountain = load_tile_from_image(MODULE_PATH .. "graph/symbols/go_fountain.png")
sym_go_person   = load_tile_from_image(MODULE_PATH .. "graph/symbols/go_person.png")
sym_go_sleep    = load_tile_from_image(MODULE_PATH .. "graph/symbols/go_sleep.png")
sym_go_wash     = load_tile_from_image(MODULE_PATH .. "graph/symbols/go_wash.png")
sym_go_work     = load_tile_from_image(MODULE_PATH .. "graph/symbols/go_work.png")


sym_gem_inv = load_tile_from_image(MODULE_PATH .. "graph/tiles/gem_inv.png")
sym_sock_inv = load_tile_from_image(MODULE_PATH .. "graph/tiles/socket.png")

-- read all functions

local fail = nil

fail = dofile(MODULE_PATH .. "scripts/core.lua")
if (fail == nil) then error("Error while reading core.lua") end

fail = dofile(MODULE_PATH .. "scripts/usages.lua")
if (fail == nil) then error("Error while reading usages.lua") end

fail = dofile(MODULE_PATH .. "scripts/attacks.lua")
if (fail == nil) then error("Error while reading attacks.lua") end

fail = dofile(MODULE_PATH .. "scripts/recipes.lua")
if (fail == nil) then error("Error while reading recipes.lua") end

fail = dofile(MODULE_PATH .. "scripts/effects.lua")
if (fail == nil) then error("Error while reading effects.lua") end

fail = dofile(MODULE_PATH .. "scripts/commands.lua")
if (fail == nil) then error("Error while reading commands.lua") end

fail = dofile(MODULE_PATH .. "scripts/traps.lua")
if (fail == nil) then error("Error while reading traps.lua") end

fail = dofile(MODULE_PATH .. "scripts/dialogs.lua")
if (fail == nil) then error("Error while reading dialogs.lua") end

fail = dofile(MODULE_PATH .. "scripts/ai.lua")
if (fail == nil) then error("Error while reading ai.lua") end

_ALERT("Lua: init.lua done\n")
 