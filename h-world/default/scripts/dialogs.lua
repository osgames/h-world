----------------------------------------------------------------
-- dialogs.lua
--
-- H-World dialog effects and callbacks.
--
-- author: Hj. Malthaner
-- date:   21-Apr-2004
-- update: 21-Apr-2004
----------------------------------------------------------------


------------------------------------------------------------------
-- NPC wants to join the party -> make him a friend
--
-- author: Hj. Malthaner
-- date:   21-Apr-2004
-- update: 21-Apr-2004
--
-- param user:   type thing_t, the player
-- param npc:    type thing_t, the npc
-- return:       int, unused
------------------------------------------------------------------

function dialog_join_party (user, npc)
	_ALERT("dialog_join_party() called\n")

	translate("The " .. thing_get_ident(npc) .. " joins you.");

	thing_set_value(npc, "status", "friend")

	-- no more talking
	thing_set_value(npc, "dialog.radius", 0)

	return 0
end


------------------------------------------------------------------
-- Make NPC hostile
--
-- author: Hj. Malthaner
-- date:   21-Apr-2004
-- update: 21-Apr-2004
--
-- param user:   type thing_t, the player
-- param npc:    type thing_t, the npc
-- return:       int, unused
------------------------------------------------------------------

function dialog_hostile_npc (user, npc)
	_ALERT("dialog_hostile_npc() called\n")

	translate("The " .. thing_get_ident(npc) .. " turns to fight!");

	thing_set_value(npc, "status", "enemy")

	-- no more talking
	thing_set_value(npc, "dialog.radius", 0)

	return 0
end


-- "The Jungle" specific dialog functions - module makers will
-- need their own stuff here, I guess


------------------------------------------------------------------
-- Memorize adventuress story
--
-- author: Hj. Malthaner
-- date:   12-Sep-2004
-- update: 12-Sep-2004
--
-- param user:   type thing_t, the player
-- param npc:    type thing_t, the npc
-- return:       int, unused
------------------------------------------------------------------

function dialog_memorize_adventuress(user,npc)

	if(thing_recall(user, "dialog_adventuress", 0) == nil) then

		translate("You memorize the traders story. (Press M to open thebook of memories.)")

		thing_memorize(user, "dialog_adventuress", "")
		thing_memorize(user, "event", "<h1>Discovery:</h1>The weapons trader knows of an adventuress who is exploring the dungeon.")
	end

end


------------------------------------------------------------------
-- Memorize cat and dog (lord) story
--
-- author: Hj. Malthaner
-- date:   12-Sep-2004
-- update: 22-Oct-2004 - adapted to necromancer story
--
-- param user:   type thing_t, the player
-- param npc:    type thing_t, the npc
-- return:       int, unused
------------------------------------------------------------------

function dialog_memorize_lord(user,npc)

	if(thing_recall(user, "dialog_old_lord", 0) == nil) then

		translate("You memorize the traders story. (Press M to open thebook of memories.)")

		thing_memorize(user, "dialog_old_lord", "")
		thing_memorize(user, "event", "<h1>Discovery:</h1>The clothes trader knows of an old fortress and an evil necromancer. He says the necromancers dog and cat are locked somewhere in the fortress basement.")
	end

end


------------------------------------------------------------------
-- Memorize fortress story
--
-- author: Hj. Malthaner
-- date:   22-Sep-2004
-- update: 
--
-- param user:   type thing_t, the player
-- param npc:    type thing_t, the npc
-- return:       int, unused
------------------------------------------------------------------

function dialog_memorize_fortress(user,npc)

	if(thing_recall(user, "dialog_fortress", 0) == nil) then

		translate("You memorize the traders story. (Press M to open thebook of memories.)")

		thing_memorize(user, "dialog_fortress", "")
		thing_memorize(user, "event", "<h1>Discovery:</h1>The weapons trader knows of an old fortress and an evil necromancer. He says the necromancer was killed during his experiments in the fortress cellars, but the undead horrors still exist in the basement.")
	end

end
