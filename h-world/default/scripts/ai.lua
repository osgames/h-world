----------------------------------------------------------------
-- ai.lua
--
-- H-World AI handling scripts
--
-- Author: Hj. Malthaner
-- Created: 10-Sep-2004
-- Update: 13-Sep-2004 - Hj. Malthaner: added pathfinding triggers
-- Update: 15-Sep-2004 - Hj. Malthaner: added functions to be executed
--                                      if a being reached a destination
----------------------------------------------------------------



------------------------------------------------------------------
-- Check if being needs to sleep
--
-- author: Hj. Malthaner
-- date:   10-Sep-04
-- update: 10-Sep-04
--
-- param being:      type thing_t, the being to be checked
-- return:           importance of sleeping
------------------------------------------------------------------
function sleep_check(being)

	local pri = -1

	local sleep = thing_get_value(being, "sleep") or 100

	-- about to toggle
	if(sleep >= -2 and sleep <= 2) then
		pri = 1
	end

	-- sleeping
	if(sleep > 0) then
		pri = 0

		-- are we observed? 
		if(square_is_visible(thing_get_location(being))) then

			-- chance to wake up
			if(rng_roll(6) == 0) then
				-- now it's important to wake up
				pri = 100
				translate("The "..thing_get_ident(being).." becomes aware of you and wakes up.")
			end
		end
	end

	return pri
end


------------------------------------------------------------------
-- Exectue sleep/wakeup toggle
--
-- author: Hj. Malthaner
-- date:   10-Sep-04
-- update: 10-Sep-04
--
-- param being:      type thing_t, the being to be checked
-- return:           delay
------------------------------------------------------------------
function sleep_execute(being)

	local sleep = thing_get_value(being, "sleep") or 0

	if(sleep ~= nil and sleep >= -2 and sleep <= 2) then
		if(sleep >= 0) then
			-- wake up
			local dice = thing_get_value(being, "sleep_wake") or "%10d10+50"

			local v = rng_roll(dice);
			thing_set_value(being, "sleep", -v)

			if(square_is_visible(thing_get_location(being))) then
				translate("The "..thing_get_ident(being).." wakes up.")
			end
		else 
			-- fall asleep ... but not if watched

			if(square_is_visible(thing_get_location(being))) then
				translate("The "..thing_get_ident(being).." is tired.")
			else 
				local dice = thing_get_value(being, "sleep_sleep") or "%10d10+50"
				local v = rng_roll(dice);
				thing_set_value(being, "sleep", v)
			
				if(square_is_visible(thing_get_location(being))) then
					translate("The "..thing_get_ident(being).." falls asleep.")
				end
			end
		end

		-- fixed delay of 1 tick
		return 1
	end


	if(sleep ~= nil and sleep > 0) then

		-- surprised wakeup
		if(square_is_visible(thing_get_location(being))) then
			thing_set_value(being, "sleep", -100)
		end

		-- sleep another 30 ticks
		return 30
	end

	-- fixed delay of 1 ticks
	return 1
end




------------------------------------------------------------------
-- Check if this being wants to go to a room
--
-- author: Hj. Malthaner
-- date:   13-Sep-04
-- update: 14-Sep-04
--
-- param being:      type thing_t, the being to be checked
-- return:           importance of walking there
------------------------------------------------------------------
function go_room_check(being)

	local pri = -1
	local sleep = thing_get_value(being, "sleep") or 0

	-- _ALERT("sleep=" .. sleep .. "\n")

	if(sleep <= 0) then
		local state = thing_get_value(being, "AI.state") or "nothing"

		-- _ALERT("state=" .. state .. "\n")

		if(state == "nothing" and rng_get_int(3) == 0) then
			pri = 101
		end
	end

	return pri
end


------------------------------------------------------------------
-- Exectue looking for a room
--
-- author: Hj. Malthaner
-- date:   13-Sep-04
-- update: 14-Sep-04
--
-- param being:      type thing_t, the being to be checked
-- return:           delay
------------------------------------------------------------------
function go_room_execute(being)

	-- look for a room
	thing_set_value(being, "AI.state", "search_room")

	-- look for a room of this type
	local type = 1 + rng_get_int(7)


	-- _ALERT("look for room of type=" .. type .. "\n")

	thing_set_value(being, "AI.room_type", type)

	return 1
end



------------------------------------------------------------------
-- Check if this being has reached its destination
--
-- author: Hj. Malthaner
-- date:   15-Sep-04
-- update: 15-Sep-04
--
-- param being:      type thing_t, the being to be checked
-- return:           importance of walking there
------------------------------------------------------------------
function am_i_there_check(being)

	local pri = -1

	local state = thing_get_value(being, "AI.state") or "nothing"

	-- _ALERT("state=" .. state)

	if(state == "destination_reached") then
		pri = 1000
	end

	return pri
end


------------------------------------------------------------------
-- Execute action after reaching a destination 
--
-- author: Hj. Malthaner
-- date:   15-Sep-04
-- update: 15-Sep-04
--
-- param being:      type thing_t, the being to be checked
-- return:           delay
------------------------------------------------------------------
function am_i_there_execute(being)

	thing_set_value(being, "AI.state", "nothing")

	-- What have we been searching?
	local type = thing_get_value(being, "AI.room_type") or 0

	if(type == 2) then
		-- sleep

		if(square_is_visible(thing_get_location(being))) then
			translate("The "..thing_get_ident(being).." is tired.")
		else 
			local dice = thing_get_value(being, "sleep_sleep") or "%10d10+50"
			local v = rng_roll(dice);
			thing_set_value(being, "sleep", v)
		end
	end

	return 10
end
