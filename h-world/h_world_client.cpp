/* 
 * h_world_client.cpp
 *
 * Copyright (c) 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "h_world_main.h"

#include "util/debug_t.h"


#include "swt/sdl_window_t.h"
//#include "swt/x11_window_t.h"
#include "swt/system_window_t.h"
#include "swt/window_manager_t.h"
#include "swt/gui_window_t.h"
#include "swt/gui_container_t.h"
#include "swt/tileset_t.h"

#include "primitives/cstring_t.h"
#include "primitives/invalid_argument_exception_t.h"

#include "view/world_view_t.h"
#include "view/server_view_t.h"
#include "view/visual_t.h"
#include "view/ground_visual_t.h"

#include "model/world_t.h"

#include "gui/stats_panel_t.h"
#include "net/client_socket_t.h"
#include "net/socketio_t.h"
#include "net/commands.h"


extern int h_world_main(int argc, char ** argv)
{
  const char * path = gimme_arg(argc, argv, "-game", 1);

  if(argc !=3 ) {
    printf("Usage: %s <host> <port>\n", argv[0]);
    exit(10);
  }


  client_socket_t socket(argv[1], atoi(argv[2]));


  try {

    // init recoloring feature
    visual_t::read_colorshades();

    
    // then prepare system window  
    system_window_t *syswin = new sdl_window_t();
    // system_window_t *syswin = new x11_window_t();

    koord screensize (800, 600);


    if(gimme_arg(argc, argv, "-screensize", 0) != NULL) {
	const char * res_str = gimme_arg(argc, argv, "-screensize", 1);

	int n = 0;

	if(res_str) {
	    n = sscanf(res_str, "%hdx%hd", &screensize.x, &screensize.y);
	}

	if(n != 2) {
	    printf("invalid argument for -screensize option\n");
	    printf("argument must be of format like -screensize 800x600\n");
	    return 0;
	}
    }

    if(syswin->initialize(screensize.x, screensize.y, 
			  "H-World CRPG Engine Client 0.1.6")) {

      // set up window manager
      window_manager_t *winman = new window_manager_t(syswin);
      
      world_view_t *view = world_view_t::get_instance();
      if(view->initialize(path)) {
	
	props_display_t::set_path(path);

	// set up world view
	view->set_pos(koord(140,60));
	view->set_size(screensize - koord(140, 60+80));
	winman->add_window(view);
	view->set_visible(true);

	cstring_t dirname ( "./data/" );
	cstring_t filename ( "level001.props" );
	
	world_t::get_instance()->init(view, 
				      dirname, 
				      filename, 
				      -1);



	bool done = false;

	socketio_t sockio (&socket);
	char buf[1<<16];
	bool ok;


	// get level size from server
	sockio.write_short(GET_LEVEL_SIZE);
	koord levelsize(sockio.read_short(), sockio.read_short());	
	view->prepare_level(levelsize);

	do {

	  // ask server for map update

	  sockio.write_short(GET_MAP);

	  short size = sockio.read_short();

	  dbg->message("main()", "getting %d bytes map data", size);
	  
	  ok = socket.read(buf, size);

	  if(!ok) {
	    dbg->warning("main()", "error while getting map data");
	  } else {
	    
	    int mark = 0;

	    do {
	      int what = buf[mark];
	      mark ++;

	      koord k = *((koord *)&buf[mark]);
	      mark += sizeof(koord);

	      dbg->warning("main()", "map op %d at %dx%d", what, k.x, k.y);

	      switch(what) {
	      case server_view_t::update:
		{
		  ground_visual_t * grv =
		    (ground_visual_t *) &buf[mark];
		  mark += sizeof(ground_visual_t);

		  view->update_ground(k, grv);
		}
		break;
	      case server_view_t::clear:
		{
		  view->clear_visuals(k);
		}
		break;
	      case server_view_t::add:
		{
		  visual_t * visual =
		    (visual_t *) &buf[mark];
		  mark += sizeof(visual_t);

		  // view->add_visual(k, visual);
		}
		break;
	      case server_view_t::dark:
		{
		  view->set_dark(k);
		}
		break;
	      }
	    } while(mark < size);

	    view->redraw();
	  }


	  



	  event_t & event = winman->poll_and_process_events();

	  while(event.type == event_t::IGNORE_EVENT ||
		event.type == event_t::NO_EVENT) {
	    
	    winman->get_and_process_events();
	  }

	  // dbg->message("main()", "type %d, code %d", event.type, event.code);
	  
	  sockio.write_short(PUT_EVENT);
	  sockio.write_short(event.type);
	  sockio.write_short(event.code);


	} while(!done);
	


      }
    }

  } catch(invalid_argument_exception_t iax) {
    dbg->fatal("main()", iax.reason);
  }
  
  return 0;


}
