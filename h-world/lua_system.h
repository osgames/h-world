/* 
 * lua_system.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

extern "C" {
#include "lua/include/lua.h"
#include "lua/include/lualib.h"
}

void lua_init(const char *path);

/**
 * quits on error
 */
void lua_diagnostics_fatal(const int fail);

/**
 * just gives error message
 */
void lua_diagnostics_error(const int fail);

void lua_exit();

lua_State * lua_state();
