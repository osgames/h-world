/* dr_rdppm.c
 *
 * Copyright (c) 2001 Hansj�rg Malthaner
 *
 * This file is part of the Simugraph graphics engine.
 *
 * This file may be copied and modified freely as long as the above
 * credits are retained.  No one who-so-ever may sell or market
 * this software in any form without the expressed written consent
 * of the author Hansj�rg Malthaner.
 *
 */

int load_block(unsigned char *block, char *filename);
