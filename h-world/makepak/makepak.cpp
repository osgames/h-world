/* makepak.cpp
 *
 * Copyright (c) 2001 Hansj�rg Malthaner
 *
 */

/* makepak.c
 * 
 * daten.pak generator fuer Simugraph
 * Hj. Malthaner, Aug. 1997
 *                                     
 *
 * 3D, isometrische Darstellung        
 *
 * ??.??.00 trennung von simgraph.c und makepak.c
 */

/*
 * 18.11.97 lineare Speicherung fuer Images -> hoehere Performance
 * 22.03.00 modifiziert f�r run-l�ngen speicherung -> hoehere Performance
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "util/config_file_t.h"

#include "swt/read_png.h"


static int IMG_SIZE = 64;
static int SCALE = 1;
static const char * DATNAME = 0;
static const char * PAKNAME = 0;


#define TRANSPARENT 0x808088


typedef unsigned char  PIXEL;
typedef unsigned short PIXVAL;
typedef unsigned int   PIXRGB;


// number of special colors
#define SPECIAL 8

static PIXRGB rgbtab[SPECIAL] =
{
  0x001C1C,
  0x003838,
  0x005555,
  0x007171,

  0x008D8D,
  0x00AAAA,
  0x00C6C6,
  0x00E2E2
};



struct dimension {
    int xmin,xmax,ymin,ymax;
};


struct imd {
    unsigned char x;
    unsigned char y;
    unsigned char w;
    unsigned char h;
    int len;
    PIXVAL * data;
}; 
                                 

/*
 * Maximal number of processable images. Enlarge if more images are needed
 */
 
#define MAX_IMAGES   1600

static struct imd images[MAX_IMAGES];


static int block_getpix(const image_data *block, int x, int y) 
{
  x *= SCALE;
  y *= SCALE;

  return 
    ((block->data[((y)*block->width*3)+(x)*3] << 16) + 
     (block->data[((y)*block->width*3)+(x)*3+1] << 8) + 
     (block->data[((y)*block->width*3)+(x)*3+2]));
}

static PIXVAL pixrgb_to_pixval(int rgb)
{                                       
  const int r = rgb >> 16;
  const int g = (rgb >> 8) & 0xFF;
  const int b = rgb & 0xFF;

  for(int i=0; i<SPECIAL; i++) {
    if(rgbtab[i] == rgb) {

      return 0x8000 + i;
    }
  }

  return ((r & 0xF8) << 7) | ((g & 0xF8) << 2) | ((b & 0xF8) >> 3);
}


void
getline(char *str, int max, FILE *f)
{
  fgets(str, max, f);
  str[strlen(str)-1]=0;
}


void
init_dim(PIXRGB *image, struct dimension *dim)
{
    int x,y;

    dim->ymin = dim->xmin = IMG_SIZE-1;
    dim->ymax = dim->xmax = 0;

    for(y=0; y<IMG_SIZE; y++) {
	for(x=0; x<IMG_SIZE; x++) {
	    if(image[x+y*IMG_SIZE] != TRANSPARENT) {
		if(x<dim->xmin)
		    dim->xmin = x;
		if(y<dim->ymin)
		    dim->ymin = y;
		if(x>dim->xmax)
		    dim->xmax = x;
		if(y>dim->ymax)
		    dim->ymax = y;
	    }
	}
    }


/*    dim[n].ymin = dim[n].xmin = 32;
    dim[n].ymax = dim[n].xmax = 63;
*/
/*    printf("Dim[%d] %d %d %d %d\n",n,dim[n].xmin,dim[n].ymin,dim[n].xmax,dim[n].ymax);
*/
}

static PIXVAL* encode_image(image_data *block,
			    int x, int y, struct dimension *dim, int *len)
{
    int line;
    PIXVAL *dest;
    PIXVAL *dest_base = (PIXVAL *)malloc(IMG_SIZE*IMG_SIZE*2*sizeof(PIXVAL));
    PIXVAL *run_counter;

    y += dim->ymin;
    dest = dest_base;

    for(line=0; line<(dim->ymax-dim->ymin+1); line++) {
	int   row = 0;
	PIXRGB pix = block_getpix(block, x, (y+line));
	unsigned char count = 0;

	do {
	    count = 0;
	    while(pix == TRANSPARENT && row < IMG_SIZE) {
		count ++;
		row ++;
		pix = block_getpix(block, (x+row), (y+line));
	    }

//	    printf("-%d ", count);

	    *dest++ = count;

	    // if(row < 64) {

		run_counter = dest++;
		count = 0;

		while(pix != TRANSPARENT && row < IMG_SIZE) {
		    *dest++ = pixrgb_to_pixval(pix);

		    count ++;
		    row ++;
		    pix = block_getpix(block, (x+row), (y+line));
		}
		*run_counter = count;

//		printf("+%d ", count);

	    // }

	} while(row < IMG_SIZE);

//	printf("\n");

	*dest++ = 0;
    }

    *len = (dest - dest_base);

    return dest_base;
}


static int init_image_block(char *filename, int start)
{
    struct dimension dim;
    PIXRGB image[IMG_SIZE*IMG_SIZE];
    int nr = start;

    int z,n;
    int x,y;

    image_data * block = read_png(filename);

    if(block == 0) {
      fprintf(stderr, "Error while reading '%s'\n", filename);
      exit(1);
    }  

    const int lines = block->height/IMG_SIZE;
    const int rows  = block->width/IMG_SIZE;

    printf("%s is %dx%d, start=%d\n", filename, rows, lines, start);

    for(z=0; z<lines; z++) {
	for(n=0; n<rows; n++) {
	    for(x=0; x<IMG_SIZE; x++) {
		for(y=0; y<IMG_SIZE; y++) {
		    image[x+y*IMG_SIZE] = block_getpix(block,
						       x+n*IMG_SIZE, 
						       y+z*IMG_SIZE);
		}                          
	    }   
	    init_dim(image, &dim);

	    images[nr].x = dim.xmin;
	    images[nr].y = dim.ymin;

	    images[nr].w = dim.xmax >= dim.xmin ? dim.xmax - dim.xmin + 1 : 0;
	    images[nr].h = dim.ymax >= dim.ymin ? dim.ymax - dim.ymin + 1 : 0;

	    if(images[nr].h > 0) {
		int len;
		images[nr].data = encode_image(block,
					       n*IMG_SIZE, 
					       z*IMG_SIZE, 
					       &dim, &len);
		images[nr].len = len;
	    } else {
		images[nr].data = NULL;
		images[nr].len = 0;
	    }


	    printf("Image %d l=%d  x=%d y=%d w=%d h=%d\n"
		   "               xmin=%d xmax=%d ymin=%d ymax=%d\n",
		   nr, 
		   images[nr].len,
		   images[nr].x,
		   images[nr].y,
		   images[nr].w,
		   images[nr].h,

		   dim.xmin,
		   dim.xmax,
		   dim.ymin,
		   dim.ymax

		   );


	    nr ++;
	}
    }

    free(block->data);
    free(block);

    return lines * rows;
}


void fwrite_int(int *p, FILE *f)
{
    const int i = *p;
    fputc(i & 255,f);
    fputc((i >> 8) & 255,f);
    fputc((i >> 16) & 255,f);
    fputc((i >> 24) & 255,f);
}


void init_images()
{    
  char buf[256];
  config_file_t file;
  int n = 0;

  if(file.open(DATNAME)) {
    
    while(! file.eof()) {
      file.read_line(buf, 250);
      if(! file.eof()) {
	n += init_image_block(buf, n);  
      }
    }
    
  } else {
    fprintf(stderr, 
	    "Error: can't open %s for reading.\n", DATNAME);
    exit(1);
  }
                                

  FILE *f = fopen(PAKNAME, "wb");

  if(f != NULL) {
    int i;

    fprintf(f, "H-World daten.pak 0.0.1");

    i = n;
    fwrite_int(&i, f);
    
    for(i=0; i<n; i++) {
      
      fputc(images[i].x, f);
      fputc(images[i].y, f);
      fputc(images[i].w, f);
      fputc(images[i].h, f);

      fwrite_int(&(images[i].len), f);
      
      if(images[i].h > 0) {
	fwrite(images[i].data, images[i].len*sizeof(PIXVAL), 1, f);
      }                                               
    }
    
    printf("-> Processed %d images\n", n);

    fclose(f);
  } else {
    printf("Can't write '%s'.\n", PAKNAME);
    exit(1);
  }
}


int
main(int argc, char *argv[])
{
  if(argc == 5) {

    sscanf(argv[1], "%d", &IMG_SIZE);
    SCALE = *argv[2] - '0';
    DATNAME = argv[3];
    PAKNAME = argv[4];

  } else {
    IMG_SIZE = 64;
    SCALE = 1;
    DATNAME = IMG_SIZE==128 ? "images/makepak128.dat" : "images/makepak.dat";
    PAKNAME = IMG_SIZE==128 ? "daten128.pak" : "daten.pak";
  }
  

  puts("  \nH-World Makepak v1.05 by Hj. Malthaner (c) 2005\n");

  puts("  This program creates a 'daten.pak' file used");
  puts("  by H-World from a bundle of png images.\n");

  puts("  If you have questions about Makepak");
  puts("  contact the author at:");
  puts("    hansjoerg.malthaner@gmx.de\n");

  puts("  usage makepak <size> <scale> <list> <output>");
  puts("    <size>:   image size");
  puts("    <scale>:  scale must be >= 1, downscales images");
  puts("    <list>:   filename of image list");
  puts("    <output>: destination file");

  printf("\ncreating %dx%d tiles, processing image list:\n", IMG_SIZE, IMG_SIZE);
  

  init_images();
  
  
  puts("-> Wrote daten.pak successfully.\n");
  
#ifdef COUNT_COLORS
  for(i=0; i<256; i++) {
    printf("%3d %8d\n", i, histogramm[i]);
  }
#endif

  return 0;
}
