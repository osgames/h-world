#ifndef inventory_view_listener_t_h
#define inventory_view_listener_t_h


class thing_t;
template <class T> class handle_tpl; 

class inventory_view_listener_t
{

 public:

  virtual void mouse_over_thing(const handle_tpl < thing_t > &thing) = 0;
};

#endif // inventory_view_listener_t_h
