/* Copyright by Hj. Malthaner */

#ifndef THING_LISTENER_T_H
#define THING_LISTENER_T_H


class thing_t;
template <class T> class handle_tpl; 


/** @interface */
class thing_listener_t {
public:    

    virtual void update(const handle_tpl<thing_t> &) = 0;
};
#endif //THING_LISTENER_T_H
