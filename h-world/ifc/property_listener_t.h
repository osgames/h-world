/* Copyright by Hj. Malthaner */

#ifndef PROPERTY_LISTENER_T_H
#define PROPERTY_LISTENER_T_H

class property_t;

/** @interface */
class property_listener_t {
public:    

    virtual void update(const property_t *) = 0;
};
#endif //PROPERTY_LISTENER_T_H
