/* Copyright by Hj. Malthaner */

#ifndef THING_VISITOR_T_H
#define THING_VISITOR_T_H

class thing_t;
template <class T> class handle_tpl; 

class thing_visitor_t {
public:    

    virtual void visit_thing(const handle_tpl <thing_t> &) = 0;
};
#endif //THING_VISITOR_T_H
