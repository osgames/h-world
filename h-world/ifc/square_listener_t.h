#ifndef square_listener_t_h
#define square_listener_t_h

class square_t;
template <class T> class handle_tpl; 


class square_listener_t
{
  public:

  /**
   * Called if item list was changed.
   *
   * @author Hj. Malthaner
   */
  virtual void item_list_changed(square_t *) = 0;


};

#endif // square_listener_t_h
