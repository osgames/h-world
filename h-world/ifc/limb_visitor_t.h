/* Copyright by Hj. Malthaner */

#ifndef LIMB_VISITOR_T_H
#define LIMB_VISITOR_T_H


class thing_t;
template <class T> class handle_tpl; 


/** @interface */
class limb_visitor_t {
public:    

    virtual void visit_limb(const handle_tpl <thing_t> &) = 0;
};
#endif //LIMB_VISITOR_T_H
