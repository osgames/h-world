#ifndef inventory_listener_t_h
#define inventory_listener_t_h


class thing_t;
template <class T> class handle_tpl; 

class inventory_listener_t
{

 public:

  virtual void thing_added(handle_tpl < thing_t > &thing) = 0;
  virtual void thing_removed(handle_tpl < thing_t > &thing) = 0;
};

#endif // inventory_listener_t_h
