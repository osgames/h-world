#include <string.h>

#include "h_world_main.h"

// just for testing so far

#include "util/debug_t.h"
#include "util/properties_t.h"

#include "model/world_t.h"
#include "model/message_log_t.h"
 
#include "swt/sdl_window_t.h"
//#include "swt/x11_window_t.h"
#include "swt/system_window_t.h"
#include "swt/window_manager_t.h"
#include "swt/gui_window_t.h"
#include "swt/gui_container_t.h"
#include "swt/tileset_t.h"
#include "view/world_view_t.h"

#include "gui/message_log_view_t.h"
#include "gui/usage_panel_t.h"
#include "gui/stats_panel_t.h"
#include "gui/player_gui_t.h"
#include "gui/birth_frame_t.h"
#include "gui/help_frame_t.h"

#include "control/scheduler_t.h"
#include "control/user_input_t.h"

#include "primitives/cstring_t.h"

// scripting system interface
#include "lua_system.h"



/*
static void check_times()
{
  unsigned long T0 = syswin->get_time();

  for(int i=0; i<200; i++) {
    winman->draw();
  }
  
  unsigned long T1 = syswin->get_time();
  
  dbg->message("main()", "200 display loops took %lu ms", (T1-T0));
  dbg->message("main()", "-> %d FPS", 200*1000/(T1-T0));
}
*/


const char *gimme_arg(int argc, char *argv[], const char *arg, int off)
{                                                     
//    printf("Searching parameter %s, ", arg);

    for(int i=1; i<argc; i++) {
	if(strcmp(argv[i], arg) == 0 && i<argc-off) {
//	    printf("found %s\n", argv[i+off]);
	    return argv[i+off];
	}
    }
//    printf("<- failed.\n");
    return NULL;
}


int main(int argc, char ** argv)

{
  // first of all initialize debugging facility
  dbg = new debug_t("stderr",
		    gimme_arg(argc, argv, "-debug", 0) ? log_t::DEBUG : log_t::MESSAGE,
		    true 
		    );

  return h_world_main(argc, argv);
}
