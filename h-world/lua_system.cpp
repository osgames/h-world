/* 
 * lua_system.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "util/debug_t.h"
#include "util/property_t.h"
#include "util/rng_t.h"
#include "language/linguist_t.h"
#include "model/world_t.h"
#include "model/level_t.h"
#include "model/square_t.h"
#include "model/thing_t.h"
#include "model/inventory_t.h"
#include "model/dice_t.h"
#include "factories/thing_factory_t.h"
#include "view/world_view_t.h"
#include "lua_system.h"

#include "control/trigger_t.h"
#include "control/user_input_t.h"
#include "gui/thing_chooser_t.h"
#include "gui/lua_frame_t.h"
#include "gui/help_frame_t.h"
#include "gui/multi_choice_t.h"
#include "gui/hw_button_t.h"

#include "swt/window_manager_t.h"
#include "swt/system_window_t.h"


// -------- prototypes ---------


extern "C" {
  static int thing_get_value(lua_State *L);
  static int thing_set_value(lua_State *L);
  static int thing_find_limb(lua_State *L);
  static int thing_get_item(lua_State *L);
  static int thing_set_item(lua_State *L);
  static int thing_remove_item(lua_State *L);
  static int thing_set_item_known(lua_State *L);
  static int thing_get_ident(lua_State *L);
  static int thing_get_location(lua_State *L);
  static int thing_set_location(lua_State *L);
  static int thing_get_inventory(lua_State *L);

  static int thing_replace_item(lua_State *L);
  static int thing_add_item_to_inv(lua_State *L);

  static int thing_choose(lua_State *L);

  static int thing_create(lua_State *L);

  static int thing_visual_set_image(lua_State *L);
  static int thing_visual_add_image(lua_State *L);
  static int thing_visual_get_image(lua_State *L);
  static int thing_visual_remove_image(lua_State *L);
  static int rng_get_int(lua_State *L);
  static int rng_roll(lua_State *L);

  static int square_get_thing(lua_State *L);
  static int square_put_thing(lua_State *L);
  static int square_remove_thing(lua_State *L);
  static int square_set_trigger(lua_State *L);
  static int square_is_visible(lua_State *L);

  static int user_ask_direction(lua_State *L);
  static int user_ask_key(lua_State *L);

  static int user_cmd_get(lua_State *L);
  static int user_cmd_open(lua_State *L);
  static int user_cmd_inspect(lua_State *L);
  static int user_cmd_talk(lua_State *L);
  static int user_cmd_trade(lua_State *L);

  static int translate(lua_State *L);
  static int translate_sv(lua_State *L);

  static int thing_throw(lua_State *L);
  static int thing_target(lua_State *L);
  static int thing_kill(lua_State *L);
  static int thing_memorize(lua_State *L);
  static int thing_recall(lua_State *L);
  static int thing_is_animated(lua_State *L);

  static int inventory_add_thing(lua_State *L);
  static int inventory_remove_thing(lua_State *L);

  static int load_tile_from_image(lua_State *L);

  static int show_html(lua_State *L);
  static int show_multi_choice(lua_State *L);

  static int frame_create(lua_State *L);
  static int frame_destroy(lua_State *L);
  static int frame_show(lua_State *L);
  static int frame_add_buttonlabel(lua_State *L);

  static int buttonlabel_set_text(lua_State *L);
  static int buttonlabel_get_text(lua_State *L);
  static int buttonlabel_set_text_offset(lua_State *L);
  static int buttonlabel_set_editable(lua_State *L);

  static int system_get_time(lua_State *L);
  static int system_sleep_until(lua_State *L);

} // extern "C"


// -------- lua setup and teardown  ---------

static lua_State *L;


lua_State * lua_state()
{
  return L;
}


void lua_init(const char *path)
{
  // set stacksize to 1024
  L = lua_open(1024);


  lua_baselibopen(L);
// lua_stringlibopen(L);
  lua_mathlibopen(L);
// lua_iolibopen(L);


  lua_register(L, "thing_get_value", thing_get_value);
  lua_register(L, "thing_set_value", thing_set_value);
  lua_register(L, "thing_find_limb", thing_find_limb);
  lua_register(L, "thing_get_item",  thing_get_item);
  lua_register(L, "thing_set_item",  thing_set_item);
  lua_register(L, "thing_remove_item",  thing_remove_item);
  lua_register(L, "thing_set_item_known",  thing_set_item_known);
  lua_register(L, "thing_get_ident",  thing_get_ident);
  lua_register(L, "thing_get_location",  thing_get_location);
  lua_register(L, "thing_set_location",  thing_set_location);
  lua_register(L, "thing_get_inventory",  thing_get_inventory);

  lua_register(L, "thing_replace_item",  thing_replace_item);
  lua_register(L, "thing_add_item_to_inv",  thing_add_item_to_inv);

  lua_register(L, "thing_create",  thing_create);

  lua_register(L, "thing_visual_set_image",  thing_visual_set_image);
  lua_register(L, "thing_visual_add_image",  thing_visual_add_image);
  lua_register(L, "thing_visual_get_image",  thing_visual_get_image);
  lua_register(L, "thing_visual_remove_image",  thing_visual_remove_image);


  lua_register(L, "thing_choose",  thing_choose);

  lua_register(L, "rng_get_int", rng_get_int);
  lua_register(L, "rng_roll", rng_roll);


  lua_register(L, "square_get_thing", square_get_thing);
  lua_register(L, "square_put_thing", square_put_thing);
  lua_register(L, "square_remove_thing", square_remove_thing);
  lua_register(L, "square_set_trigger", square_set_trigger);
  lua_register(L, "square_is_visible", square_is_visible);

  lua_register(L, "user_ask_direction", user_ask_direction);
  lua_register(L, "user_ask_key", user_ask_key);

  lua_register(L, "user_cmd_open", user_cmd_open);  
  lua_register(L, "user_cmd_get", user_cmd_get);
  lua_register(L, "user_cmd_inspect", user_cmd_inspect);
  lua_register(L, "user_cmd_talk", user_cmd_talk);
  lua_register(L, "user_cmd_trade", user_cmd_trade);

  lua_register(L, "translate", translate);
  lua_register(L, "translate_sv", translate_sv);

  lua_register(L, "thing_throw", thing_throw);
  lua_register(L, "thing_target", thing_target);
  lua_register(L, "thing_kill", thing_kill);
  lua_register(L, "thing_memorize", thing_memorize);
  lua_register(L, "thing_recall", thing_recall);
  lua_register(L, "thing_is_animated", thing_is_animated);


  lua_register(L, "inventory_add_thing", inventory_add_thing);
  lua_register(L, "inventory_remove_thing", inventory_remove_thing);

  lua_register(L, "load_tile_from_image", load_tile_from_image);
  lua_register(L, "show_html", show_html);
  lua_register(L, "show_multi_choice", show_multi_choice);

  lua_register(L, "frame_create", frame_create);
  lua_register(L, "frame_destroy", frame_destroy);
  lua_register(L, "frame_show", frame_show);
  lua_register(L, "frame_add_buttonlabel", frame_add_buttonlabel);

  lua_register(L, "buttonlabel_set_text", buttonlabel_set_text);
  lua_register(L, "buttonlabel_get_text", buttonlabel_get_text);
  lua_register(L, "buttonlabel_set_text_offset", buttonlabel_set_text_offset);
  lua_register(L, "buttonlabel_set_editable", buttonlabel_set_editable);

  lua_register(L, "system_get_time", system_get_time);
  lua_register(L, "system_sleep_until", system_sleep_until);


  char buf[1024];

  if(path) {
    strcpy(buf, path);
    strcat(buf, "/");
  } else {
    strcpy(buf, "./");
  }

  lua_pushstring(L, buf);
  lua_setglobal(L, "MODULE_PATH");


  strcat(buf, "scripts/init.lua");

  dbg->message("lua_diagnostics()", "lua system reading %s", buf);

  const int fail = lua_dofile(L, buf);

  lua_diagnostics_fatal(fail);

  if(fail == 0) {
    dbg->message("lua_diagnostics()", "lua system running with %s", buf);
  }
}


void lua_diagnostics_fatal(const int fail)
{
  switch(fail) {
  case LUA_ERRRUN:
    dbg->fatal("lua_diagnostics()", "LUA_ERRRUN - runtime error");
    break;
  case LUA_ERRSYNTAX:
    dbg->fatal("lua_diagnostics()", "LUA_ERRSYNTAX - syntax error");
    break;
  case LUA_ERRMEM:
    dbg->fatal("lua_diagnostics()", "LUA_ERRMEM - memory allocation failed");
    break;
  case LUA_ERRERR:
    dbg->fatal("lua_diagnostics()", "LUA_ERRERR - ???");
    break;
  case LUA_ERRFILE:
    dbg->fatal("lua_diagnostics()", "LUA_ERRFILE - File not found");
    break;
  case 0:
    // that's success !!
    break;
  default:
    dbg->error("lua_diagnostics()", "lua system reported error %d", fail);
  }
}


void lua_diagnostics_error(const int fail)
{
  switch(fail) {
  case LUA_ERRRUN:
    dbg->error("lua_diagnostics()", "LUA_ERRRUN - runtime error");
    break;
  case LUA_ERRSYNTAX:
    dbg->error("lua_diagnostics()", "LUA_ERRSYNTAX - syntax error");
    break;
  case LUA_ERRMEM:
    dbg->error("lua_diagnostics()", "LUA_ERRMEM - memory allocation failed");
    break;
  case LUA_ERRERR:
    dbg->error("lua_diagnostics()", "LUA_ERRERR - ???");
    break;
  case LUA_ERRFILE:
    dbg->error("lua_diagnostics()", "LUA_ERRFILE - File not found");
    break;
  case 0:
    // that's success !!
    break;
  default:
    dbg->error("lua_diagnostics()", "lua system reported error %d", fail);
  }
}

void lua_exit()
{
  lua_close(L);
  L = 0;
}



// ----- bindings -----------




extern "C" {


static int thing_get_value(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  void * data = lua_touserdata(L, -2);
  const char * name = lua_tostring(L, -1);

  if(n != 2) {
    dbg->warning("thing_get_value()", "wrong number of arguments, 2 expected, %d given", n);    
    return 0;
  }

  if(data == 0) {
    dbg->warning("thing_get_value()", "thing is nil");    
    lua_pushnil(L);
    return 1;
  }

  if (name == 0) {
    dbg->warning("thing_get_value()", "name is nil");    
    lua_pushnil(L);
    return 1;
  }

  // dbg->warning("thing_get_value()", "name is %s", name);    

  thing_t * thing = (thing_t *)data;

  // dbg->message("thing_get_value()", "value %s in thing %p", name, thing);    
  const property_t *prop = thing->get(name);

  if(prop == 0) {
    lua_pushnil(L);
  }
  else if(prop->get_int()) {
    lua_pushnumber(L, *prop->get_int());
  }
  else if(prop->get_string()) {
    lua_pushstring(L, prop->get_string());
  } 
  else {
    lua_pushnil(L);
  } 
  
  return 1;
}


static int thing_set_value(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  void * data = lua_touserdata(L, -3);
  const char * name = lua_tostring(L, -2);

  if(n != 3) {
    dbg->warning("thing_set_value()", "wrong number of arguments, 3 expected, %d given", n);    
    return 0;
  }

  if(data == 0) {
    dbg->warning("thing_set_value()", "thing is nil");    
    return 0;
  }

  if (name == 0) {
    dbg->warning("thing_set_value()", "name is nil");    
    return 0;
  }

  thing_t * thing = (thing_t *)data;

  if(lua_tag(L, -1) == LUA_TSTRING) {
    thing->set(name, lua_tostring(L, -1));
  } else {
    thing->set(name, (int)lua_tonumber(L, -1));
  }

  return 0;
}


static int thing_find_limb(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n == 2) {
    void * d1 = lua_touserdata(L, -2);
    void * d2 = lua_touserdata(L, -1);

    handle_tpl <thing_t> owner ((thing_t *)d1);
    handle_tpl <thing_t> item ((thing_t *)d2);

    handle_tpl <thing_t> limb = thing_t::find_limb(thing_t::find_root(owner),
						   item);
    if(limb.is_bound()) {
      lua_pushuserdata(L, limb.get_rep());
    }
    else {
      lua_pushnil(L);
    }


  } else if(n == 3) {

    void * data = lua_touserdata(L, -3);
    const char * type = lua_tostring(L, -2);
    const char * subtype = lua_tostring(L, -1);

    // Hold reference
    handle_tpl <thing_t> target ((thing_t *)data);

    handle_tpl <thing_t> limb = thing_t::find_limb(thing_t::find_root(target),
						   type, 
						   subtype);
    if(limb.is_bound()) {
      lua_pushuserdata(L, limb.get_rep());
    }
    else {
      lua_pushnil(L);
    }


  } else {
    dbg->warning("thing_find_limb()", "wrong number of arguments, 2 or 3 expected, %d given", n);    
    lua_pushnil(L);
  }


  return 1;
}


static int thing_get_item(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 1) {
    dbg->warning("thing_get_item()", "wrong number of arguments, 1 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * data = lua_touserdata(L, -1);

    if(data) {
      thing_t * item = ((thing_t *)data)->get_item().get_rep();
      if(item) {
	lua_pushuserdata(L, item);
      }
      else {
	// no item case
	lua_pushnil(L);
      }
    } else {
      dbg->warning("thing_get_item()", "thing is nil", n);    
      lua_pushnil(L);
    }
  }

  return 1;
}


static int thing_set_item(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("thing_set_item()", "wrong number of arguments, 2 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * d1 = lua_touserdata(L, -2);
    void * d2 = lua_touserdata(L, -1);

    if(d1) {
      thing_t * limb = (thing_t *)d1;
      thing_t * item = (thing_t *)d2;
			
      limb->put_multi(item);

    } else {
      dbg->warning("thing_set_item()", "thing is nil", n);    
    }
  }

  return 0;
}


static int thing_remove_item(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("thing_remove_item()", "wrong number of arguments, 2 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * d1 = lua_touserdata(L, -2);
    void * d2 = lua_touserdata(L, -1);

    if(d1) {
      thing_t * limb = (thing_t *)d1;
      thing_t * item = (thing_t *)d2;
			
      limb->remove_multi(item);

    } else {
      dbg->warning("thing_remove_item()", "thing is nil", n);    
    }
  }

  return 0;
}


static int thing_set_item_known(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("thing_set_item_known()", "wrong number of arguments, 2 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * d1 = lua_touserdata(L, -2);
    void * d2 = lua_touserdata(L, -1);

    if(d1) {
      thing_t * user = (thing_t *)d1;
      thing_t * item = (thing_t *)d2;
			
      if(item != 0) {
	const char * iid = item->get_string("identified_ident", 0);

	if(iid != 0) {
	  user->set_item_known(iid);
	} 
      } else {
	dbg->warning("thing_set_item_known()", "item is nil", n);    
      }

    } else {
      dbg->warning("thing_set_item_known()", "user is nil", n);    
    }
  }

  return 0;
}


static int thing_get_ident(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 1) {
    dbg->warning("thing_get_ident()", "wrong number of arguments, 1 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * d1 = lua_touserdata(L, -1);

    if(d1) {
      thing_t * item = (thing_t *)d1;
      char buf[256];
			
      item->get_ident(buf);

      dbg->debug("thing_get_ident()", "ident is '%s'", buf);    

      lua_pushstring(L, buf);

    } else {
      dbg->warning("thing_get_ident()", "thing is nil", n);    
      lua_pushnil(L);
    }
  }

  return 1;
}


static int thing_get_location(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 1) {
    dbg->warning("thing_get_location()", "wrong number of arguments, 1 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * d1 = lua_touserdata(L, -1);

    if(d1) {
      thing_t * item = (thing_t *)d1;
			
      koord k = item->get_pos();

      lua_pushnumber(L, k.x);
      lua_pushnumber(L, k.y);

    } else {
      dbg->warning("thing_get_location()", "thing is nil", n);    
    }
  }

  return 2;
}


static int thing_set_location(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("thing_set_location()", 
		 "wrong number of arguments, 3 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * d1 = lua_touserdata(L, -3);
    int x = (int)lua_tonumber(L, -2);
    int y = (int)lua_tonumber(L, -1);

    if(d1) {
      thing_t * item = (thing_t *)d1;
			
      item->set_pos(koord(x,y));

    } else {
      dbg->warning("thing_set_location()", "thing is nil", n);    
    }
  }

  return 0;
}


static int thing_get_inventory(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 1) {
    dbg->warning("thing_get_inventory()", 
		 "wrong number of arguments, 1 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * data = lua_touserdata(L, -1);

    if(data) {
      thing_t * thing = (thing_t *)data;
      inventory_t * inv = thing->get_inventory();

      if(inv) {
	lua_pushuserdata(L, inv);
      }
      else {
	// no inventory case
	lua_pushnil(L);
      }
    } else {
      dbg->warning("thing_get_inventory()", "thing is nil", n);    
      lua_pushnil(L);
    }
  }

  return 1;
}


static int thing_replace_item(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("thing_replace_item()", "wrong number of arguments, 3 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * d1 = lua_touserdata(L, -3);
    void * d2 = lua_touserdata(L, -2);
    void * d3 = lua_touserdata(L, -1);

    if(d1) {
      thing_t * limb = (thing_t *)d1;
      thing_t * old_item = (thing_t *)d2;
      thing_t * new_item = (thing_t *)d3;
			
      thing_t::replace_thing(thing_t::find_root(limb), old_item, new_item);

    } else {
      dbg->warning("thing_replace_item()", "thing is nil", n);    
    }
  }

  return 0;
}

static int thing_add_item_to_inv(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("thing_add_item_to_inv()", "wrong number of arguments, 2 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * d1 = lua_touserdata(L, -2);
    void * d2 = lua_touserdata(L, -1);

    if(d1) {
      thing_t * thing = (thing_t *)d1;
      thing_t * item = (thing_t *)d2;
	
      int ok = 0;

      if(thing->get_inventory()) {
	ok = thing->get_inventory()->add(item);
      }
		
      if(ok) {
	lua_pushnumber(L, 1);
      } else {
	lua_pushnil(L);
      }

    } else {
      dbg->warning("thing_add_item_to_inv()", "thing is nil", n);    
    }
  }

  return 1;
}



static int thing_create(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("thing_create()", 
		 "wrong number of arguments, 3 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    const char * name = lua_tostring(L, -3);
    const int magic_max = (int)lua_tonumber(L, -2);
    const int magic_chance = (int)lua_tonumber(L, -1);

    if(name) {
      thing_factory_t fab;

      thinghandle_t tmp = fab.create(name, magic_max, magic_chance);
      
      // dbg->warning("thing_create()", "count %d", tmp.get_count());    

      thing_t * item = tmp.unbind();

      if(item) {
	lua_pushuserdata(L, item);
      }
      else {
	// no item case
	lua_pushnil(L);
      }


    } else {
      dbg->warning("thing_set_item()", "thing is nil", n);    
    }
  }

  return 1;
}


static int thing_visual_get_image(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("thing_visual_get_image()", 
		 "wrong number of arguments, 3 expected, %d given", n);
  } else {

    void * data = lua_touserdata(L, -3);
    const int inv_or_map = (int)lua_tonumber(L, -2);
    const int index = (int)lua_tonumber(L, -1);

    if(data) {
      thing_t *thing = (thing_t *)data;
      const image_meta_t meta = inv_or_map == 1 ? thing->inv_visual.access_image(index) : thing->visual.access_image(index);
      const int image = meta.img + (meta.tileset << 14);

      lua_pushnumber(L, image);
      lua_pushnumber(L, meta.xoff);
      lua_pushnumber(L, meta.yoff);
      lua_pushnumber(L, meta.colorset);
      lua_pushnumber(L, meta.trans);

    } else {
      dbg->warning("thing_visual_get_image()", "thing is nil", n);    
    }
  }

  return 5;
}


static int thing_visual_set_image(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 9) {
    dbg->warning("thing_visual_set_image()", 
		 "wrong number of arguments, 9 expected, %d given", n);    
  } else {

    void * data = lua_touserdata(L, -9);
    int inv_or_map = (int)lua_tonumber(L, -8);
    int index = (int)lua_tonumber(L, -7);
    int image = (int)lua_tonumber(L, -6);
    int colorset = (int)lua_tonumber(L, -5);
    int xoff = (int)lua_tonumber(L, -4);
    int yoff = (int)lua_tonumber(L, -3);
    int trans = (int)lua_tonumber(L, -2);
    int redraw = (int)lua_tonumber(L, -1);

    // dbg->message("thing_visual_set_image()", "colorset=%d", colorset); 

    if(data) {
      image_meta_t meta;

      meta.img = image & 0x3FFF;
      meta.xoff = xoff;
      meta.yoff = yoff;
      meta.tileset = image >> 14;
      meta.colorset = colorset;
      meta.trans = trans;

      if(inv_or_map == 0) {
	((thing_t *)data)->visual.set_image(index, meta);
      } else {
	((thing_t *)data)->inv_visual.set_image(index, meta);
      }

      if(redraw) {
	world_view_t::get_instance()->set_dirty(true);
	world_view_t::get_instance()->redraw();
      }

    } else {
      dbg->warning("thing_visual_set_image()", "thing is nil", n);    
    }
  }

  return 0;
}


static int thing_visual_add_image(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("thing_visual_add_image()", "wrong number of arguments, 3 expected, %d given", n);    
  } else {
    void * data = lua_touserdata(L, -3);
    int image = (int)lua_tonumber(L, -2);
    int redraw = (int)lua_tonumber(L, -1);

    if(data) {
      ((thing_t *)data)->visual.add_image(image & 0x3FFF, image >> 14, 0, 0, 0, false);

      if(redraw) {
	world_view_t::get_instance()->set_dirty(true);
	world_view_t::get_instance()->redraw();
      }

    } else {
      dbg->warning("thing_visual_add_image()", "thing is nil", n);    
    }
  }

  return 0;
}


static int thing_visual_remove_image(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("thing_visual_remove_image()", "wrong number of arguments, 2 expected, %d given", n);    
  } else {
    void * data = lua_touserdata(L, -3);
    int image = (int)lua_tonumber(L, -2);
    int redraw = (int)lua_tonumber(L, -1);

    if(data) {
      ((thing_t *)data)->visual.rem_image(image & 0x3FFF, image >> 14);

      if(redraw) {
	world_view_t::get_instance()->set_dirty(true);
	world_view_t::get_instance()->redraw();
      }

    } else {
      dbg->warning("thing_visual_remove_image()", "thing is nil", n);    
    }
  }

  return 0;
}


static int thing_choose(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 5) {
    dbg->warning("thing_choose()", "wrong number of arguments, 5 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    const char * title   = lua_tostring(L,   -5);
    void * data          = lua_touserdata(L, -4);
    const char * ident   = lua_tostring(L,   -3);
    const char * type    = lua_tostring(L,   -2);
    const char * subtype = lua_tostring(L,   -1);

    if(data) {
      thing_t * thing = (thing_t *) data;

      thing_chooser_t chooser (title, thing, ident, type, subtype);
      chooser.hook();

      thinghandle_t item = chooser.get_choice();

      if(item.is_bound()) {
	dbg->message("thing_choose()", "item chosen");

	lua_pushuserdata(L, item.unbind());
      }
      else {
	dbg->warning("thing_choose()", "no item chosen");    
	// no item case
	lua_pushnil(L);
      }

    } else {
      dbg->warning("thing_choose()", "thing is nil");    
    }
  }

  return 1;
}


static int rng_get_int(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 1) {
    dbg->warning("rng_get_int()", "wrong number of arguments, 1 expected, %d given", n);    
    lua_pushnil(L);
  } else {

    const int r = (int)lua_tonumber(L, -1);  

    lua_pushnumber(L, r > 0 ? rng_t::get_the_rng().get_int(r) : 0);
  }

  return 1;
}


static int rng_roll(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2 && n != 1) {
    dbg->warning("rng_roll()", "wrong number of arguments, 1 or 2 expected, %d given", n);    
    lua_pushnil(L);
  }

  if(n == 1) {
    const char * d = lua_tostring(L,  -1);  

    if(d) {
      dice_t dice (d+1);
      const int v = dice.roll();
      lua_pushnumber(L, v);
    } else {
      dbg->warning("rng_roll()", "dice string is nil, returning zero.");
      lua_pushnumber(L, 0);
    }

  } else if(n == 2) {

    const int dice = (int)lua_tonumber(L,  -2);  
    const int sides = (int)lua_tonumber(L, -1);  
    
    lua_pushnumber(L, rng_t::get_the_rng().roll(dice, sides));
  }

  return 1;
}


static int square_get_thing(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("square_get_thing()", "wrong number of arguments, 3 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    const int x = (int)lua_tonumber(L, -3);
    const int y = (int)lua_tonumber(L, -2);
    const int n = (int)lua_tonumber(L, -1);

    dbg->warning("square_get_thing()", "x=%d, y=%d, n=%d", x, y, n);    

    square_t * square = world_t::get_instance()->get_level()->at(koord(x,y));

    thinghandle_t result;

    if(square) {
      const minivec_tpl <thinghandle_t> & items = square->get_things();

      if(n < items.count()) {
	result = items.get(n);
      }
    }

    if(result.is_bound()) {
      lua_pushuserdata(L, result.get_rep());
    } else {
      lua_pushnil(L);
    }
  }

  return 1;
}


static int square_put_thing(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("square_put_thing()", "wrong number of arguments, 3 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    int x = (int)lua_tonumber(L, -3);
    int y = (int)lua_tonumber(L, -2);
    thing_t * thing = (thing_t *) lua_touserdata(L, -1);
    const koord k (x,y);

    square_t * square = world_t::get_instance()->get_level()->at(k);

    if(square && thing) {
      square->add_thing(thing, k);
    }
  }

  return 0;
}


static int square_remove_thing(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("square_remove_thing()", 
		 "wrong number of arguments, 3 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    int x = (int)lua_tonumber(L, -3);
    int y = (int)lua_tonumber(L, -2);
    thing_t * thing = (thing_t *) lua_touserdata(L, -1);

    square_t * square = world_t::get_instance()->get_level()->at(koord(x,y));

    if(square && thing) {
      square->remove_thing(thing);
    }
  }

  return 0;
}


static int square_set_trigger(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("square_set_trigger()", 
		 "wrong number of arguments, 3 expected, %d given", n);    
  } else {
    int x = (int)lua_tonumber(L, -3);
    int y = (int)lua_tonumber(L, -2);
    int t = (int)lua_tonumber(L, -1);

    square_t * square = world_t::get_instance()->get_level()->at(koord(x,y));

 
    trigger_t * trigger = new trigger_t( koord(x,y) );
    trigger->set_type( t );
    square->set_trigger( trigger );
  }

  return 0;
}


static int square_is_visible(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("square_is_visible()", 
		 "wrong number of arguments, 2 expected, %d given", n);    


    lua_pushnil(L);

  } else {
    int x = (int)lua_tonumber(L, -2);
    int y = (int)lua_tonumber(L, -1);

    if(world_t::get_instance()->get_level()->is_viewable(koord(x,y))) {
      lua_pushnumber(L, 0);
    } else {
      lua_pushnil(L);
    }
  }

  return 1;
}


static int translate(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 1) {
    dbg->warning("translate()", "wrong number of arguments, 1 expected, %d given", n);    
  } else {

    const char * txt = lua_tostring(L, -1);  

    linguist_t::translate(txt);
  }

  return 0;
}


static int translate_sv(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("translate()", 
		 "wrong number of arguments, 2 expected, %d given", n);    
  } else {

    const char * s = lua_tostring(L, -2);  
    const char * v = lua_tostring(L, -1);  

    linguist_t::sv(s, v);
  }

  return 0;
}



static int thing_throw(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  int delay = 0;

  if(n != 3) {
    dbg->warning("thing_throw()", "wrong number of arguments, 3 expected (user, ammo, multiplier), %d given", n);    
  } else {
    void * d1 = lua_touserdata(L, -3);
    void * d2 = lua_touserdata(L, -2);
    int multi = (int)lua_tonumber(L, -1);

    if(d1 && d2) {
      thing_t * thing = (thing_t *)d1;
      thing_t * item = (thing_t *)d2;
  
      delay = cmd_throw(thing, item, multi);
    } else {
      dbg->warning("thing_throw()", 
		   "thing=%p item=%p should both be != (nil)", d1, d2);    
    }

    lua_pushnumber(L, delay);

  }

  return 1;
}


static int thing_target(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 1) {
    dbg->warning("thing_target()", 
		 "wrong number of arguments, 1 expected, %d given", n);    
  } else {
    void * d1 = lua_touserdata(L, -1);

    thinghandle_t target;

    if(d1) {
      thing_t * thing = (thing_t *)d1;
  
      target = cmd_target(thing);
    }

    lua_pushuserdata(L, target.get_rep());

  }

  return 1;
}


static int thing_kill(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("thing_kill()", 
		 "wrong number of arguments, 2 expected, %d given", n);    
  } else {
    void * d1 = lua_touserdata(L, -2);
    const bool silent = ((int)lua_tonumber(L, -1)) == 1;

    if(d1) {
      thing_t * thing = (thing_t *)d1;
      world_t::get_instance()->kill(thing, silent);
    }
  }

  return 0;
}


static int thing_memorize(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("thing_memorize()", 
		 "wrong number of arguments, 3 expected, %d given", n);    
  } else {
    void * d1 = lua_touserdata(L, -3);
    const char * catg = lua_tostring(L, -2);
    const char * msg  = lua_tostring(L, -1);

    if(d1) {
      thing_t * thing = (thing_t *)d1;
      thing->memorize(catg, msg);
    } else {
      dbg->warning("thing_memorize()", "thing is nil");
    }
  }

  return 0;
}


static int thing_recall(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 3) {
    dbg->warning("thing_recall()", 
		 "wrong number of arguments, 3 expected, %d given", n);    
  } else {
    void * d1 = lua_touserdata(L, -3);
    const char * catg = lua_tostring(L, -2);
    const int n = (int)lua_tonumber(L, -1);

    if(d1) {
      thing_t * thing = (thing_t *)d1;
      const char * msg = thing->recall(catg, n);

      if(msg) {
	lua_pushstring(L, msg);
      } else {
	lua_pushnil(L);	
      }
    } else {
      dbg->warning("thing_recall()", "thing is nil");
      return 0;
    }
  }

  return 1;
}


static int thing_is_animated(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 1) {
    dbg->warning("thing_is_animated()", 
		 "wrong number of arguments, 1 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    void * d1 = lua_touserdata(L, -1);

    if(d1) {
      thing_t * thing = (thing_t *)d1;
      const bool anim = thing->get_actor().is_animated();

      if(anim) {
	lua_pushnumber(L, 0);
      } else {
	lua_pushnil(L);	
      }
    } else {
      dbg->warning("thing_recall()", "thing is nil");
      return 0;
    }
  }

  return 1;
}


static int user_ask_direction(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n > 1) {
    dbg->warning("user_ask_direction()",
		 "wrong number of arguments, 0 or 1 expected, %d given", n);    
    return 0;
  } else {
    int max_dist = 1;

    if(n == 1) {
      max_dist = (int)lua_tonumber(L, -1);
    }

    const koord dir = 
      user_input_t::ask_direction(world_t::get_instance()->get_player(),
				  max_dist);

    lua_pushnumber(L, dir.x);
    lua_pushnumber(L, dir.y);
  }

  return 2;
}


static int user_ask_key(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 0) {
    dbg->warning("user_ask_key()", 
		 "wrong number of arguments, 0 expected, %d given", n);    
  } else {
    const int key = user_input_t::wait_for_key();

    lua_pushnumber(L, key);
  }

  return 1;
}


static int user_cmd_open(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("user_cmd_open()", 
		 "wrong number of arguments, 2 expected, %d given", n);    

    lua_pushnumber(L, 0);
  } else {
    thing_t * player = (thing_t *) lua_touserdata(L, -2);
    thing_t * item = (thing_t *) lua_touserdata(L, -1);

    const int delay = user_input_t::cmd_open_thing(player, item);

    lua_pushnumber(L, delay);
  }

  return 1;
}


static int user_cmd_get(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("user_cmd_get()", 
		 "wrong number of arguments, 2 expected, %d given", n);    

    lua_pushnumber(L, 0);
  } else {
    thing_t * player = (thing_t *) lua_touserdata(L, -2);
    thing_t * item = (thing_t *) lua_touserdata(L, -1);

    const int delay = user_input_t::cmd_get_thing(player, item);

    lua_pushnumber(L, delay);
  }

  return 1;
}


static int user_cmd_inspect(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("user_cmd_inspect()", 
		 "wrong number of arguments, 2 expected, %d given", n);    

    lua_pushnumber(L, 0);
  } else {
    thing_t * player = (thing_t *) lua_touserdata(L, -2);
    thing_t * item = (thing_t *) lua_touserdata(L, -1);

    const int delay = user_input_t::cmd_inspect_thing(player, item);

    lua_pushnumber(L, delay);
  }

  return 1;
}


static int user_cmd_talk(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  int delay = 0;

  if(n != 2) {
    dbg->warning("user_cmd_talk()", 
		 "wrong number of arguments, 2 expected, %d given", n);    
  } else {
    thing_t * p = (thing_t *) lua_touserdata(L, -2);
    thing_t * t = (thing_t *) lua_touserdata(L, -1);

    const koord ppos = p->get_pos();
    const koord tpos = t->get_pos();

    level_t * level = world_t::get_instance()->get_level();

    square_t * s1 = level->at(ppos);
    square_t * s2 = level->at(tpos);


    if(s1 && s2) {
      
      const minivec_tpl <thinghandle_t> & things = s1->get_things();
      minivec_iterator_tpl <thinghandle_t> iter (things);
	
      while( iter.next() ) {
	thinghandle_t & player = iter.access_current();
	if(player.get_rep() == p) {

	  const minivec_tpl <thinghandle_t> & things = s2->get_things();
	  minivec_iterator_tpl <thinghandle_t> iter (things);
	
	  while( iter.next() ) {
	    thinghandle_t & thing = iter.access_current();

	    if(thing.get_rep() == t) {
	      delay = user_input_t::cmd_talk(player, thing);
	      goto ende;
	    }
	  }
	}
      }
    }

  ende:;
  }

  lua_pushnumber(L, delay);

  return 1;
}


static int user_cmd_trade(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  int delay = 0;

  if(n != 2) {
    dbg->warning("user_cmd_trade()", 
		 "wrong number of arguments, 2 expected, %d given", n);    
  } else {
    thing_t * p = (thing_t *) lua_touserdata(L, -2);
    thing_t * t = (thing_t *) lua_touserdata(L, -1);

    delay = user_input_t::cmd_trade(p, t);
  }

  lua_pushnumber(L, delay);

  return 1;
}


static int inventory_add_thing(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("inventory_add_thing()", 
		 "wrong number of arguments, 2 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    inventory_t * inv = (inventory_t*)lua_touserdata(L, -2);
    thing_t * item = (thing_t *)lua_touserdata(L, -1);

    const bool ok = inv->add(item);

    if(ok) {
      lua_pushuserdata(L, item);
    } else {
      lua_pushnil(L);
    }
  }

  return 1;
}


static int inventory_remove_thing(lua_State *L) 
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 2) {
    dbg->warning("inventory_remove_thing()", 
		 "wrong number of arguments, 2 expected, %d given", n);    
    lua_pushnil(L);
  } else {
    inventory_t * inv = (inventory_t*)lua_touserdata(L, -2);
    thing_t * d1 = (thing_t *)lua_touserdata(L, -1);

    thinghandle_t item (d1);

    const bool ok = inv->remove(item);

    if(ok) {
      lua_pushuserdata(L, d1);
    } else {
      lua_pushnil(L);
    }
  }

  return 1;
}


static int load_tile_from_image(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 1) {
    dbg->warning("load_tile_from_image()", "wrong number of arguments, 1 expected, %d given", n);    
  } else {

    const char * filename = lua_tostring(L, -1);

    const int image = 
      world_view_t::get_instance()
      ->load_tile_from_image(filename) + (2 << 14); // User defined tiles

    lua_pushnumber(L, image);
  }

  return 1;
}


static int show_html(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  if(n != 4) {
    dbg->warning("show_html()", 
		 "wrong number of arguments, 4 expected, %d given", n);    
  } else {
    const int x = (int)lua_tonumber(L, -4);
    const int y = (int)lua_tonumber(L, -3);
    const char * path = lua_tostring(L, -2);
    const char * filename = lua_tostring(L, -1);


    help_frame_t *frame = new help_frame_t(path, filename, true);
    frame->set_pos(koord(x,y));
    frame->set_visible(true);
    window_manager_t::get_instance()->add_window(frame);

  }

  return 0;
}


static int show_multi_choice(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  int c = -1;

  if(n != 3) {
    dbg->warning("multi_choice()", 
		 "wrong number of arguments, 3 expected, %d given", n);    
  } else {
    const char * title = lua_tostring(L, -3);
    const char * mesg = lua_tostring(L, -2);
    const char * buts = lua_tostring(L, -1);

    multi_choice_t choice (title, mesg, buts);

    koord c_size = choice.get_size();
    choice.set_pos(koord(800-c_size.x,600-c_size.y)/2);
    choice.hook();
      
    c = choice.get_choice();
  }

  lua_pushnumber(L, c);

  return 1;
}


static int frame_create(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments

  gui_frame_t * frame = 0;
  
  if(n != 6) {
    dbg->warning("frame_create()", 
		 "wrong number of arguments, 6 expected, %d given", n);    
  } else {
    const char * title    = lua_tostring(L, -6);
    const char * callback = lua_tostring(L, -5);
    const int x = (int)lua_tonumber(L, -4);
    const int y = (int)lua_tonumber(L, -3);
    const int w = (int)lua_tonumber(L, -2);
    const int h = (int)lua_tonumber(L, -1);

    frame = new lua_frame_t(title, callback);
    frame->set_pos(koord(x, y));
    frame->set_size(koord(w, h));
  }

  lua_pushuserdata(L, frame);

  return 1;
}


static int frame_destroy(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  
  if(n != 1) {
    dbg->warning("frame_destroy()", 
		 "wrong number of arguments, 1 expected, %d given", n);    
  } else {

    lua_frame_t * frame = (lua_frame_t *)lua_touserdata(L, -1);
    frame->on_close();
    frame->close();

    // Hajo: enforce "kill_list" cleanup
    window_manager_t::get_instance()->process_kill_list();
  }

  return 0;
}


static int frame_show(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  
  if(n != 1) {
    dbg->warning("frame_show()", 
		 "wrong number of arguments, 1 expected, %d given", n);    
  } else {

    lua_frame_t * frame = (lua_frame_t *)lua_touserdata(L, -1);
    frame->hook();
  }

  return 0;
}


static int frame_add_buttonlabel(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  
  hw_button_t * but = 0;

  if(n != 7) {
    dbg->warning("frame_add_buttonlabel()", 
		 "wrong number of arguments, 7 expected, %d given", n);    
  } else {
    lua_frame_t * frame = (lua_frame_t *)lua_touserdata(L, -7);
    const char * label    = lua_tostring(L, -6);
    const int img = (int)lua_tonumber(L, -5);
    const int x = (int)lua_tonumber(L, -4);
    const int y = (int)lua_tonumber(L, -3);
    const int w = (int)lua_tonumber(L, -2);
    const int h = (int)lua_tonumber(L, -1);

    but = frame->add_buttonlabel(label, img, x, y, w, h);
  }

  lua_pushuserdata(L, but);

  return 1;
}


static int buttonlabel_set_text(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  
  if(n != 2) {
    dbg->warning("buttonlabel_set_text()", 
		 "wrong number of arguments, 2 expected, %d given", n);    
  } else {

    hw_button_t * but = (hw_button_t *)lua_touserdata(L, -2);
    const char * text = lua_tostring(L, -1);
    but->set_text(text);
    but->redraw();
  }

  return 0;
}


static int buttonlabel_get_text(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  const char * text = "";

  if(n != 1) {
    dbg->warning("buttonlabel_get_text()", 
		 "wrong number of arguments, 1 expected, %d given", n);    
  } else {

    hw_button_t * but = (hw_button_t *)lua_touserdata(L, -1);

    text = but->get_text();
  }

  lua_pushstring(L, text);

  return 1;
}


static int buttonlabel_set_text_offset(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  
  if(n != 3) {
    dbg->warning("buttonlabel_set_text_offset()", 
		 "wrong number of arguments, 3 expected, %d given", n);    
  } else {

    hw_button_t * but = (hw_button_t *)lua_touserdata(L, -3);
    const int x = (int)lua_tonumber(L, -2);
    const int y = (int)lua_tonumber(L, -1);

    but->text_offset = koord(x, y);
    but->redraw();
  }

  return 0;
}


static int buttonlabel_set_editable(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  
  if(n != 2) {
    dbg->warning("buttonlabel_set_editable()", 
		 "wrong number of arguments, 2 expected, %d given", n);    
  } else {

    hw_button_t * but = (hw_button_t *)lua_touserdata(L, -2);
    const int yesno = (int)lua_tonumber(L, -1);

    but->editable = (yesno != 0);
  }

  return 0;
}


static int system_get_time(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  
  
  if(n != 0) {
    dbg->warning("system_get_time()", 
		 "wrong number of arguments, 0 expected, %d given", n);    
  }

  const unsigned long time = system_window_t::get_instance()->get_time();
  lua_pushnumber(L, time);

  return 1;
}


static int system_sleep_until(lua_State *L)
{
  const int n = lua_gettop(L);  // number of arguments
  
  if(n != 1) {
    dbg->warning("system_get_time()", 
		 "wrong number of arguments, 0 expected, %d given", n);    
  } else {
    const unsigned long now = system_window_t::get_instance()->get_time();
    const unsigned long then = (unsigned long) lua_tonumber(L, -1);
    
    if(now < then) {
      const unsigned long delta = then - now;
      system_window_t::get_instance()->sleep(delta);
    }
  }

  return 0;
}


} // extern "C"
