#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>


#include "util/debug_t.h"

#include "client_socket_t.h"


struct sockdata_t {
  int sfd;
  int tfd;
};


client_socket_t::client_socket_t(char *host, int port)
{
  struct sockaddr_in sa;   // internet socket addr. structure
  struct hostent *hp;      // result of host name lookup

    
  dbg->message("client_socket_t::client_socket_t()", 
		 "connection to %s %d", host, port);

  // look up host
  
  if((hp = gethostbyname(host)) == NULL) {
    fprintf(stderr, "%s no such host?\n", host);
    exit( 1 );
  }             

  // put host address and addr type into socket struct

  memcpy(&sa.sin_addr, hp->h_addr, hp->h_length);
  sa.sin_family = hp->h_addrtype;

  // put the socket number into the socket struct

  sa.sin_port = htons(port);

  // allocate an open socket

  if((sd->sfd = socket(hp->h_addrtype, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    exit( 1 );
  } 

  // connect to remote server
    
  if(connect(sd->sfd, (struct sockaddr *)&sa, sizeof sa) < 0) {
    perror("connect");
    exit( 1 );
  }  
}


client_socket_t::~client_socket_t()
{
  
}


/**
 * Tries to read 'size' bytes into 'buffer'
 * @return true on success, false otherwise
 * @author Hj. Malthaner
 */
bool client_socket_t::read(void * buffer, int size)
{
  ssize_t n = 0;

  do{
    n = ::read(sd->sfd, buffer, size);

    if(n == -1) {
      perror("read");
    }
    
  } while(n == 0);

  return n == size;
}


/**
 * Tries to write 'size' bytes from 'buffer'
 * @return true on success, false otherwise
 * @author Hj. Malthaner
 */
bool client_socket_t::write(void * buffer, int size)
{
  ssize_t n = ::write(sd->sfd, buffer, size);

  if(n == -1) {
    perror("write");
  }

  return n == size;
}
