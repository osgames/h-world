#ifndef socket_t_h
#define socket_t_h

class socket_t
{
 public:


  /**
   * Tries to read 'size' bytes into 'buffer'
   * @return true on success, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool read(void * buffer, int size) = 0;


  /**
   * Tries to write 'size' bytes from 'buffer'
   * @return true on success, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool write(void * buffer, int size) = 0;


};

#endif // socket_t_h
