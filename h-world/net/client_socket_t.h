#ifndef client_socket_t_h
#define client_socket_t_h

#include "socket_t.h"


struct sockdata_t;


class client_socket_t : public socket_t
{
 private:

  struct sockdata_t * sd;


 public:

  client_socket_t(char *host, int port);

  virtual ~client_socket_t();


  /**
   * Tries to read 'size' bytes into 'buffer'
   * @return true on success, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool read(void * buffer, int size);


  /**
   * Tries to write 'size' bytes from 'buffer'
   * @return true on success, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool write(void * buffer, int size);


};

#endif // client_socket_t_h
