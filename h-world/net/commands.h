#ifndef commands_h
#define commands_h

enum commands
{
  GET_MAP,
  PUT_EVENT,
  GET_LEVEL_SIZE
};

#endif // commands_h
