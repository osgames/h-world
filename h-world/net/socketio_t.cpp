#include "socketio_t.h"

#include <netinet/in.h>


socketio_t::socketio_t(socket_t *socket)
{
  this->socket = socket;
}


short socketio_t::read_short()
{
  short v;

  socket->read(&v, sizeof(short));

  return ntohs(v);
}


void socketio_t::write_short(short v)
{
  v = htons(v);

  socket->write(&v, sizeof(short));
}
