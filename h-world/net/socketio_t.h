#ifndef socketio_t_h
#define socketio_t_h

#include "socket_t.h"

class socketio_t
{
 private:
  socket_t *socket;

 public:

  socketio_t(socket_t *socket);

  short read_short();
  void write_short(short v);
};

#endif // socketio_t_h
