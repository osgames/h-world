#ifndef server_socket_t_h
#define server_socket_t_h

#include "socket_t.h"

struct sockdata_t;


/**
 * Wrapper class to encapsulte system dependand socket handling
 * @author Hj. Malthaner
 */
class server_socket_t : public socket_t
{
 private:

  struct sockdata_t * sd;


 public:


  /**
   * Opens a server socket on specified port
   * @author Hj. Malthaner
   */
  server_socket_t(int port);

  virtual ~server_socket_t();


  void accept();


  /**
   * Tries to read 'size' bytes into 'buffer'
   * @return true on success, false otherwise
   * @author Hj. Malthaner
   */
  bool read(void * buffer, int size);


  /**
   * Tries to write 'size' bytes from 'buffer'
   * @return true on success, false otherwise
   * @author Hj. Malthaner
   */
  bool write(void * buffer, int size);

};

#endif // server_socket_t_h
