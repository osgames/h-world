#include "server_socket_t.h"

#include <stdio.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>


#define BACKLOG      5
#define MAXHOSTNAME 32



struct sockdata_t {
  int sfd;
  int tfd;
};



/**
 * Opens a server socket on specified port
 * @author Hj. Malthaner
 */
server_socket_t::server_socket_t(int port)
{
  struct sockaddr_in sa;		// internet socket address structs
  struct hostent *hp;			// hostname lookup result

  char localhost[MAXHOSTNAME+1];


  sd = new sockdata_t();


  // get host information


  gethostname(localhost, MAXHOSTNAME);
    
  printf("Hostname is '%s'\n", localhost);

  if((hp = gethostbyname(localhost)) == NULL) {
    perror("gethostbyname");
    printf("cannot get local host information structure.\n");
    exit(1);
  }

  // init socket

  sa.sin_port = htons(port);
  sa.sin_family = hp->h_addrtype;
  memcpy(&sa.sin_addr, hp->h_addr, hp->h_length);

  // allocate an socket for incoming connections

  if((sd->sfd = socket(hp->h_addrtype, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    exit(1);
  }

  // bind socket to service port

  if(bind(sd->sfd, (struct sockaddr *) &sa, sizeof sa) < 0) {
    perror("bind");
    exit(1);
  }           

  // get aware of connection requests

  if(listen(sd->sfd, BACKLOG) < 0) {
    perror("listen");
    exit(1);
  }
}


server_socket_t::~server_socket_t()
{
  delete sd;
  sd = 0;
}


void server_socket_t::accept()
{
  struct sockaddr_in isa;	// internet socket address structs
  socklen_t i = sizeof isa;

  sd->tfd = ::accept(sd->sfd, (struct  sockaddr *)&isa, &i);

  if(sd->tfd < 0) {
    perror("accept");
    exit(1);
  }           
}


/**
 * Tries to read 'size' bytes into 'buffer'
 * @return true on success, false otherwise
 * @author Hj. Malthaner
 */
bool server_socket_t::read(void * buffer, int size)
{
  ssize_t n = 0;

  do{
    n = ::read(sd->tfd, buffer, size);

    if(n == -1) {
      perror("read");
    }
    
  } while(n == 0);

  return n == size;
}


/**
 * Tries to write 'size' bytes from 'buffer'
 * @return true on success, false otherwise
 * @author Hj. Malthaner
 */
bool server_socket_t::write(void * buffer, int size)
{
  ssize_t n = ::write(sd->tfd, buffer, size);

  if(n == -1) {
    perror("write");
  }

  return n == size;
}
