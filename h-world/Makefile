#
# Makefile
#
# This is the makefile for the H-World project
# Copyright by Hj. Malthaner
#


INCLUDES=-I../hjmlib -I../../hjmlib -I. -I.. -I../..


# Compiler flags and include paths
# CXXFLAGS=-Wall -g $(INCLUDES)
CXXFLAGS=-Wall -march=i586 -g $(INCLUDES)


ifeq ($(SIM_OPTIMIZE),true)
CXXFLAGS=-Wall -march=i586 -O -fomit-frame-pointer $(INCLUDES)
endif


# standard compilation setup
AR=ar
RANLIB=ranlib
CXX=g++
CC=gcc
LDFLAGS=
LDLIBS= 
LN=$(CC)

# destination if building the hlib
LIBDEST=./libhworld.a


UXFLAGS= -L../hjmlib/i586-linux
UXLIBS= ../lua/lib/liblua.a ../lua/lib/liblualib.a -lprimitives -lswt /usr/lib/libpng.a -lz -lstdc++

UXLIBS_STATIC= ../lua/lib/liblua.a ../lua/lib/liblualib.a -lprimitives -lswt /usr/lib/libpng.a -lz


W32FLAGS= -L/home/hajo/src/hjmlib/i586-win32
W32LIBS = ../lua/lib-w32/liblua.a ../lua/lib-w32/liblualib.a -lmingw32 -lSDLmain -lprimitives -lswt -lpng -lz


# cross compilation setup

ifeq ($(SIM_CROSS),true)
CC=/usr/local/src/cross-tools/bin/i386-mingw32msvc-gcc
CXX=/usr/local/src/cross-tools/bin/i386-mingw32msvc-g++
LN=/usr/local/src/cross-tools/bin/i386-mingw32msvc-gcc

AR=/usr/local/src/cross-tools/bin/i386-mingw32msvc-ar
RANLIB=/usr/local/src/cross-tools/bin/i386-mingw32msvc-ranlib

LIBDEST=./libhworld-win32.a

endif


CFLAGS=$(CXXFLAGS)


export CC
export CFLAGS
export CXX
export CXXFLAGS
export INCLUDES


# Source files
SOURCES=\
h_world.cpp lua_system.cpp h_world_main.cpp

# Object files
OBJECTS=\
h_world.o lua_system.o h_world_main.o

MAIN=h_world_init.o



# Sub-package object files
SUBS=\
language/linguist_t.o\
\
persistence/storage_t.o\
persistence/iofile_t.o\
persistence/gziofile_t.o\
\
util/string_helper.o\
util/nsow.o\
util/debug_t.o\
util/log_t.o\
util/config_file_t.o\
util/properties_t.o\
util/property_t.o\
util/sections_t.o\
util/rng_t.o\
util/perlin_t.o\
util/fov_t.o\
util/searchfolder_t.o\
\
model/pprops_t.o\
model/message_log_t.o\
model/environment_t.o\
model/ground_t.o\
model/feature_t.o\
model/feature_door_t.o\
model/feature_stairs_t.o\
model/feature_fountain_t.o\
model/feature_lua_t.o\
model/memory_t.o\
model/thing_t.o\
model/square_t.o\
model/level_t.o\
model/world_t.o\
model/inventory_t.o\
model/thing_traversor_t.o\
model/thing_tree_t.o\
model/height_level_t.o\
model/map_model_t.o\
model/path_t.o\
model/dice_t.o\
\
gui/gui_utils.o\
gui/tile_font_t.o\
gui/maptooltip_t.o\
gui/escapable_frame_t.o\
gui/modal_frame_t.o\
gui/hw_button_t.o\
gui/chapter_view_t.o\
gui/book_frame_t.o\
gui/html_view_t.o\
gui/lua_frame_t.o\
gui/help_frame_t.o\
gui/dialog_view_t.o\
gui/dialog_frame_t.o\
gui/drop_location_t.o\
gui/inspector_t.o\
gui/props_display_t.o\
gui/attack_buttons_t.o\
gui/stats_panel_t.o\
gui/usage_panel_t.o\
gui/player_gui_t.o\
gui/message_log_view_t.o\
gui/map_view_t.o\
gui/map_frame_t.o\
gui/inventory_view_t.o\
gui/thing_hold_t.o\
gui/thing_chooser_t.o\
gui/thing_drag_t.o\
gui/square_item_view_t.o\
gui/body_view_t.o\
gui/multi_choice_t.o\
gui/birth_frame_t.o\
gui/skill_view_t.o\
gui/skill_tree_view_t.o\
gui/trade_view_t.o\
gui/swap_frame_t.o\
\
view/visual_t.o\
view/ground_visual_t.o\
view/visibob_t.o\
view/player_visual_connector_t.o\
view/server_view_t.o\
view/world_view_t.o\
view/world_2d_view_t.o\
view/world_3d_view_t.o\
\
control/lua_call_t.o\
control/commands.o\
control/thing_usage_selector_t.o\
control/trigger_t.o\
control/action_move_t.o\
control/action_imove_t.o\
control/action_lua_t.o\
control/action_attack_t.o\
control/user_input_t.o\
control/action_t.o\
control/action_trade_t.o\
control/action_speak_t.o\
control/action_swap_t.o\
control/skills_t.o\
control/actor_t.o\
control/scheduler_t.o\
control/usage_t.o\
control/attack_t.o\
control/pick_thing_t.o\
\
factories/hstore_t.o\
factories/town_t.o\
factories/roadtown_t.o\
factories/structure_t.o\
factories/cave_t.o\
factories/corridor_t.o\
factories/room_t.o\
factories/forest_t.o\
factories/external_t.o\
factories/feature_peer_broker_t.o\
factories/hdoor_creator_t.o\
factories/vdoor_creator_t.o\
factories/exit_creator_t.o\
factories/fountain_creator_t.o\
factories/feature_factory_t.o\
factories/level_factory_t.o\
factories/thing_factory_t.o


NETWORK_OBJECTS=\
net/client_socket_t.o\
net/server_socket_t.o\
net/socketio_t.o\
control/remote_user_t.o


SYS_SDL =  -lSDL -lpthread

SYS_WINSDL = -lSDL

SYS_X11 = -L/usr/X11/lib -lX11 -lm



all: linux

client: subs h_world_client.o
	$(LN) $(LDFLAGS) $(UXFLAGS) -o client h_world_client.o $(OBJECTS) $(SUBS) $(SYS_SDL) $(LDLIBS) $(UXLIBS) 

hlib: subs
	$(AR) -rc $(LIBDEST) $(OBJECTS) $(SUBS)
	$(RANLIB) $(LIBDEST)


linux2: subs $(MAIN)
	$(LN) $(LDFLAGS) $(UXFLAGS) -o H-World $(MAIN) $(OBJECTS) $(SUBS) /usr/lib/libstdc++-3-libc6.1-2-2.10.0.a $(SYS_SDL) $(LDLIBS) $(UXLIBS_STATIC)

linux: subs $(MAIN)
	$(LN) $(LDFLAGS) $(UXFLAGS) -o H-World $(MAIN) $(OBJECTS) $(SUBS) $(SYS_SDL) $(LDLIBS) /usr/lib/libstdc++.a $(UXLIBS_STATIC) 


win32: subs $(MAIN)
	$(LN) $(LDFLAGS) $(W32FLAGS) -o H-World $(MAIN) $(OBJECTS) $(SUBS) $(W32LIBS) $(SYS_WINSDL) $(LDLIBS)


X11: subs
	$(LN) $(LDFLAGS) -o H-World $(OBJECTS) $(SUBS) $(SYS_X11) $(LDLIBS)


linux_static: linux
	$(LN) $(LDFLAGS) $(UXFLAGS) -o H-World $(MAIN) $(OBJECTS) $(SUBS) /usr/lib/libstdc++-3-libc6.1-2-2.10.0.a $(SYS_SDL) $(LDLIBS) $(UXLIBS_STATIC)


subs: language_sub persistence_sub model_sub util_sub view_sub gui_sub control_sub factory_sub $(OBJECTS)


clean:
	rm -f *.o */*.o */*/*.o


language_sub:
	$(MAKE) -e -C language

persistence_sub:
	$(MAKE) -e -C persistence

model_sub:
	$(MAKE) -e -C model 

util_sub:
	$(MAKE) -e -C util 

net_sub:
	$(MAKE) -e -C net 

view_sub:
	$(MAKE) -e -C view 

gui_sub:
	$(MAKE) -e -C gui 

control_sub:
	$(MAKE) -e -C control 

factory_sub:
	$(MAKE) -e -C factories

# makepak_sub: makepak/makepak.o makepak/dr_rdppm.o util/config_file_t.o
#	$(CXX) -o  makepak/makepak makepak/makepak.o makepak/dr_rdppm.o util/config_file_t.o

makepak_sub:
	$(MAKE) -C makepak



dep: depdep
	cd language; make dep; cd ..
	cd persistence; make dep; cd ..
	cd model; make dep; cd ..
	cd util; make dep; cd ..
	cd view; make dep; cd ..
	cd gui; make dep; cd ..
	cd control; make dep; cd ..
	cd factories; make dep; cd ..


pak:
	makepak/makepak 2
	mv daten128.pak default


depdep: $(SOURCES)
	gcc $(INCLUDES) -M $^ > .depend


demo_old:
	mkdir -p ../h-world-demo/help
	cp help/*.html ../h-world-demo/help
	mkdir -p ../h-world-demo/data
	cp -r data/*.tab ../h-world-demo/data
	cp -r data/*.props ../h-world-demo/data
	cp -r data/*.sects ../h-world-demo/data
	cp -r data/*.hex ../h-world-demo/data
	cp -r data/*.png ../h-world-demo/data
	cp -r data/*.lvl ../h-world-demo/data
	cp -r data/*.pal ../h-world-demo/data
	mkdir -p ../h-world-demo/data/dialogs
	cp -r data/dialogs/*.sects ../h-world-demo/data/dialogs
	mkdir -p ../h-world-demo/data/custom
	cp -r data/custom/*.png ../h-world-demo/data/custom
	cp -r data/custom/*.sects ../h-world-demo/data/custom
	mkdir -p ../h-world-demo/scripts
	cp -r scripts/*.lua ../h-world-demo/scripts
	mkdir -p ../h-world-demo/save
	cp -r save/delete.me ../h-world-demo/save
	mkdir -p ../h-world-demo/help/html
	cp -r help/html/*.html ../h-world-demo/help/html
	cp -r Documentation/readme.txt ../h-world-demo/
	cp -r Documentation/credits.txt ../h-world-demo/
	cp -r Documentation/history.txt ../h-world-demo/
	cp -r Documentation/release_notes-0_3_7.txt ../h-world-demo/
	cp daten128.pak ../h-world-demo
	strip H-World
	cp H-World ../h-world-demo


demo:
	mkdir -p ../h-world-demo/help
	cp config.txt ../h-world-demo
	cp help/*.html ../h-world-demo/help
	mkdir -p ../h-world-demo/default/data
	cp -r default/data/*.tab ../h-world-demo/default/data
	cp -r default/data/*.props ../h-world-demo/default/data
	cp -r default/data/*.sects ../h-world-demo/default/data
	cp -r default/data/*.png ../h-world-demo/default/data
	cp -r default/data/*.lvl ../h-world-demo/default/data
	mkdir -p ../h-world-demo/default/data/rooms
	cp -r default/data/rooms/*.lvl ../h-world-demo/default/data/rooms
	mkdir -p ../h-world-demo/default/data/dialogs
	cp -r default/data/dialogs/*.sects ../h-world-demo/default/data/dialogs
	mkdir -p ../h-world-demo/default/data/custom
	cp -r default/data/custom/*.sects ../h-world-demo/default/data/custom
	mkdir -p ../h-world-demo/default/scripts
	cp -r default/scripts/*.lua ../h-world-demo/default/scripts
	mkdir -p ../h-world-demo/default/save
	cp -r default/save/delete.me ../h-world-demo/default/save
	mkdir -p ../h-world-demo/help/html
	cp -r help/html/*.html ../h-world-demo/help/html
	mkdir -p ../h-world-demo/default/help
	cp -r default/help/*.html ../h-world-demo/default/help

	cp -r default/font ../h-world-demo/default

	cp -r default/graph ../h-world-demo/default/

	cp -r Documentation/readme.txt ../h-world-demo/
	cp -r Documentation/credits.txt ../h-world-demo/
	cp -r Documentation/history.txt ../h-world-demo/
	cp default/daten128.pak ../h-world-demo/default



windemo:
	mkdir -p /dos/e/src/spiele/h-world-demo/help
	cp help/*.html /dos/e/src/spiele/h-world-demo/help
	mkdir -p /dos/e/src/spiele/h-world-demo/data
	cp -r data/*.tab /dos/e/src/spiele/h-world-demo/data
	cp -r data/*.props /dos/e/src/spiele/h-world-demo/data
	cp -r data/*.sects /dos/e/src/spiele/h-world-demo/data
	cp -r data/*.hex /dos/e/src/spiele/h-world-demo/data
	cp -r data/*.png /dos/e/src/spiele/h-world-demo/data
	cp -r data/*.lvl /dos/e/src/spiele/h-world-demo/data
	cp -r data/*.pal /dos/e/src/spiele/h-world-demo/data
	mkdir -p /dos/e/src/spiele/h-world-demo/dialogs
	cp -r data/dialogs/*.txt /dos/e/src/spiele/h-world-demo/dialogs
	mkdir -p /dos/e/src/spiele/h-world-demo/data/custom
	cp -r data/custom/* /dos/e/src/spiele/h-world-demo/data/custom
	mkdir -p /dos/e/src/spiele/h-world-demo/scripts
	cp -r scripts/*.lua /dos/e/src/spiele/h-world-demo/scripts
	mkdir -p /dos/e/src/spiele/h-world-demo/save
	cp save/delete.me /dos/e/src/spiele/h-world-demo/save/
	mkdir -p /dos/e/src/spiele/h-world-demo/help/html
	cp -r help/html/*.html /dos/e/src/spiele/h-world-demo/help/html
	cp -r Documentation/readme.txt /dos/e/src/spiele/h-world-demo/
	cp -r Documentation/history.txt /dos/e/src/spiele/h-world-demo/
	cp -r Documentation/release_notes-0_1_8.txt /dos/e/src/spiele/h-world-demo/
	cp daten128.pak /dos/e/src/spiele/h-world-demo
	/usr/local/src/cross-tools/bin/i386-mingw32msvc-strip H-World
	cp H-World /dos/e/src/spiele/h-world-demo/H-World.exe


ifeq (.depend,$(wildcard .depend))
    # include .depend only if it exists
include .depend
endif
