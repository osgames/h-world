/* 
 * usage_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <stdlib.h>
#include <string.h>

#include "usage_t.h"
#include "persistence/iofile_t.h"


void usage_t::set_name(const char *name)
{
  strncpy(this->name, name, 32);
  this->name[31] = '\0';
}


void usage_t::set_verb(const char *verb)
{
  strncpy(this->verb, verb, 32);
  this->verb[31] = '\0';
}


usage_t::usage_t()
{
  set_name("Unnamed usage");
  set_verb("grmbl");
}


usage_t::~usage_t()
{
}


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void usage_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_carr(verb);
  file->rdwr_carr(name);
}
