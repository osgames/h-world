/* 
 * action_move_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "action_lua_t.h"
#include "lua_system.h"
#include "util/debug_t.h"
#include "model/thing_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"


/**
 * Get type of action performed by this object
 * @author Hansj�rg Malthaner
 */
enum action_t::atype action_lua_t::get_type() const
{
  return action_t::other;
}


action_lua_t::action_lua_t() {
  // ---- implements persistent_t ----
}


action_lua_t::action_lua_t(thinghandle_t thing, const char * call)
{
  this->thing = thing;
  strncpy(this->call, call, 32);
  this->call[31] = '\0';
}


int action_lua_t::check()
{
  char buf[256];

  sprintf(buf, "%s_check", call);

  dbg->debug("action_lua_t::check()", "calling '%s'", buf);

  lua_State * L = lua_state();

  lua_getglobal(L, buf);
  lua_pushusertag(L, thing.get_rep(), LUA_ANYTAG);
  const int fail = lua_call(L, 1, 1);
  lua_diagnostics_error(fail);

  const int result = (int)lua_tonumber(L, -1);
  lua_pop(L, 1);

  return result;
}


int action_lua_t::execute()
{
  char buf[256];

  sprintf(buf, "%s_execute", call);

  dbg->debug("action_lua_t::check()", "calling '%s'", buf);

  lua_State * L = lua_state();

  lua_getglobal(L, buf);
  lua_pushusertag(L, thing.get_rep(), LUA_ANYTAG);
  const int fail = lua_call(L, 1, 1);
  lua_diagnostics_error(fail);

  const int delay = (int)lua_tonumber(L, -1);
  lua_pop(L, 1);

  return delay;
}


/**
 * Apply duration based effects
 * @author Hj. Malthaner
 */
void action_lua_t::apply_effects(int delay)
{
  action_t::apply_effects(thing, delay);
}


// ---- implements persistent_t ----


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void action_lua_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_carr(call);

  if(file->is_saving()) {
    store->store(thing.get_rep(), file);
  } else {
    thing = thinghandle_t((thing_t *)store->restore(file, hstore_t::t_thing));
  }
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t action_lua_t::get_cid()
{
  return hstore_t::t_action_lua;
}
