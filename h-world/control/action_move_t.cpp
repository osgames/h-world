/* 
 * action_move_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "action_move_t.h"
#include "util/rng_t.h"
#include "util/debug_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/world_t.h"
#include "model/thing_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"


/**
 * Get type of action performed by this object
 * @author Hansj�rg Malthaner
 */
enum action_t::atype action_move_t::get_type() const
{
  return action_t::move;
}


action_move_t::action_move_t() {
  // ---- implements persistent_t ----
}


action_move_t::action_move_t(thinghandle_t thing)
{
  this->thing = thing;


  ident = thing->get_string("ident", "XXX");
  hate = thing->get_int("hate", 0);
  love = thing->get_int("love", 0);
  fear = thing->get_int("fear", 0);

  // printf("hate=%d  love=%d  fear=%d\n", hate, love, fear);
}


int action_move_t::check()
{
  const level_t * level = world_t::get_instance()->get_level();
  const koord pos = thing->get_pos();
  const int range = 6;

  int hate_sum = 0;
  int love_sum = 0;
  int fear_sum = 0;

  koord hate_center;
  koord love_center;
  koord fear_center;

  int result;

  // Hajo: Must be able to move and on map, otherwise it cannot move
  if(*thing->get_string("immobile", "false") != 'f' ||
     thing->get_int("sleep", -1) > 0 ||
     pos == koord(-1,-1)) {
    return -1;
  }


  // printf("Status='%s'\n", thing->get_string("status", "enemy"));

  // Hajo: neutral cretaures do random movement
  if(*thing->get_string("status", "enemy") == 'n') {
    move = koord(0,0);
    result = 1;
  } else {

    // check near area
    for(int j=-range; j<=range; j++) {
      for(int i=-range; i<=range; i++) {
	
	// if not self
	if(j!=0 || i!=0) {
	  
	  const koord k (pos.x+i, pos.y+j); 
	  const square_t *square = level->at(k);
	  
	  if(square && square->get_things().count()) {
	    const int dist = k.apx_distance_to(pos);
	    minivec_iterator_tpl <thinghandle_t> iter (square->get_things());
	    
	    while(iter.next()) {
	      const thinghandle_t & othing = iter.get_current();

	      
	      if(dist < 12) {
		const int rate = 12-dist; 
		const char *other = othing->get_string("ident", "XXX");
		
		// dbg->message("action_move_t::check()", "see '%s'", other);
		
		if(strcmp(other, ident) == 0) {   // we love same ident
		  love_sum += rate;
		  love_center += (k-pos)*rate;
		} 
		else if(strcmp(other, "player") == 0) {// we hate and fear player
		  fear_sum += rate;
		  fear_center += (k-pos)*rate;
		  
		  hate_sum += rate;
		  hate_center += (k-pos)*rate;
		}
	      }
	    }
	  }
	}
      }
    }

    // now we have the weighted centers
    // calculate where to go
    
    // attraction center is sum of love and hate
    
    koord att;
    int tmp = 0;

    if(love>0 && love_sum > 0) {
      att += (love_center*love) / love_sum;
      tmp += 1;
    }
    if(hate>0 && hate_sum > 0) {
      att += (hate_center*hate) / hate_sum;
      tmp += 1;
    }
    
    if(tmp > 1) {
      att = att/tmp;
    }


    // modify fear: each near companion reduces fear by about 1
    const int act_fear = fear - love_sum/8;
    
    // repulsion center is fear center
    
    koord rep;
    
    if(act_fear>0 && fear_sum > 0) {
      rep = (fear_center*act_fear) / fear_sum;
    }

    // movement vector: away from rep towards att
    
    koord vec = att - rep;


    move = vec.sign();
    
    result = fear_sum*act_fear > hate_sum*hate + love_sum*love ? 999 : 100;

    /*
    dbg->message("action_move_t::check()", 
		 "Thing '%s' at %d,%d: att %d,%d  rep %d,%d -> move %d,%d, pri=%d", 
		 thing->get_string("ident", "unnamed"), 
		 pos.x, pos.y,
		 att.x, att.y, rep.x, rep.y, move.x, move.y, result);
    */
  }




  // need to flee?
  return result;
}


int action_move_t::execute()
{
  rng_t &rng = rng_t::get_the_rng();
  const level_t * level = world_t::get_instance()->get_level();

  
  // random movement if no aim
  if(move == koord(0,0)) {

    if(last_move == koord(0,0)) {
      // no last move, need new random direction
      move =  koord(rng.get_int(3)-1, rng.get_int(3)-1);
    } else {
      if(rng.get_int(100) < 30) {
	// random new direction
	move =  koord(rng.get_int(3)-1, rng.get_int(3)-1);
      } else {
	// continue in old direction
	move = last_move;
      }
    }
  }

  if(thing->get_int("effect.paralyzed", 0) > 0) {
    move = koord(0,0);
  }


  if(move != koord(0,0)) {
    koord new_pos;

    last_move = move;            // save old direction

    for(int i=0; i<3; i++) {     // try three strategies

      switch(i) {
      case 0:
	new_pos = thing->get_pos() + move;
	break;
      case 1:
	new_pos = thing->get_pos() + move.rotate(-1);  // left
	break;
      case 2:
	new_pos = thing->get_pos() + move.rotate(+1);  // right
	break;
      }
      
      square_t * square = level->at(new_pos);

      if(square && 
	 square->can_enter(thing))
      {
	level->at(thing->get_pos())->remove_thing(thing);
	square->add_thing(thing, new_pos);
	
	break;
      }
    }
  }



  thing->visual.set_x_off(rng.get_int(9)-4);
  thing->visual.set_y_off(rng.get_int(5)-2);


  // default is ten time quants for a move
  const int delay = thing->calc_speed(10);

  return delay;
}


/**
 * Apply duration based effects
 * @author Hj. Malthaner
 */
void action_move_t::apply_effects(int delay)
{
  action_t::apply_effects(thing, delay);
}


// ---- implements persistent_t ----

/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void action_move_t::read_write(storage_t *store, iofile_t * file)
{

  file->rdwr_int(love);
  file->rdwr_int(hate);
  file->rdwr_int(fear);


  if(file->is_saving()) {
    store->store(ident, file);
    store->store(thing.get_rep(), file);
  } else {
    ident = (char *) store->restore(file);
    thing = thinghandle_t((thing_t *)store->restore(file, hstore_t::t_thing));
  }

  // transient
  //  koord move;

}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t action_move_t::get_cid()
{
  return hstore_t::t_action_move;
}
