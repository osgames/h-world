#ifndef commands_t_h
#define commands_t_h

#ifndef thinghandle_t_h
#include "model/thinghandle_t.h"
#endif

class usage_t;
class inventory_t;

// 
// Procedural commands
//

/**
 * Uses an item.
 * @return the delay of the action
 * @author Hj. Malthaner
 */
extern int interactive_usage(thinghandle_t user,
			     inventory_t * inv,
			     thinghandle_t item,
			     usage_t * usage);


#endif // commands_t_h
