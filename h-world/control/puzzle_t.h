#ifndef puzzle_t_h
#define puzzle_t_h

#include "ifc/square_listener_t.h"

/**
 * A puuzle consists of a set of tirems which must be 
 * put onto a set of sqaures. This class checks the 
 * completion of a puzzle.
 *
 * @author Hj. Malthaner
 */
class puzzle_t : square_listener_t
{


public:

  /**
   * Called if item list was changed.
   *
   * @author Hj. Malthaner
   */
  virtual void item_list_changed(square_t *);

};

#endif // puzzle_t_h
