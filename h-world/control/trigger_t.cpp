/* 
 * trigger_t.cpp
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "lua_system.h"
#include "trigger_t.h"

#include "util/rng_t.h"
#include "util/debug_t.h"
#include "model/thing_t.h"
#include "control/skills_t.h"
#include "language/linguist_t.h"
#include "tpl/handle_tpl.h"


void trigger_t::set_discovered(bool yesno)
{
  discovered = yesno;
}


void trigger_t::set_type(unsigned char type)
{
  this->type = type;
}


int trigger_t::get_type() const
{
  return type;
}


void trigger_t::set_desc(const char * desc)
{
  this->desc = desc; 
} 


const char * trigger_t::get_desc() const
{
  return desc;
}


trigger_t::trigger_t(koord pos)
{
  pos_self = pos;

  active = true;
  discovered = false;
  type = 0;
}


/**
 * Starts the script
 * @author Hj. Malthaner 
 */
void trigger_t::activate(const handle_tpl < thing_t > &thing)
{
  if(active) {
    dbg->message("trigger_t::activate()", "called, x=%d y=%d",
		 pos_self.x, pos_self.y);

    rng_t & rng = rng_t::get_the_rng();
    const bool is_player = thing->get_string("is_player", 0) != 0;

    // Hajo: check if player discovers the trap
    const int v = thing->get_actor().skills->get_skill_sum(skills_t::perception);
    const int chance = 5 + (v-100000);

    // Hajo: check if player can avoid setting off the trap
    if(rng.get_int(100) < chance) {
      if(is_player) {
	linguist_t::translate("You avoid triggering the trap.");
	discovered = true;
      }
      thing->get_actor().skills->add_skill_value(skills_t::perception, 10);
    } else {

      if(is_player) {
	// Hajo: let the player know about the trap
	discovered = true;
      }

    
      lua_State * L = lua_state();
    
      // the lua functions name must be the usages verb
      lua_getglobal(L, "trap_triggered");
      lua_pushusertag(L, thing.get_rep(), LUA_ANYTAG);
      lua_pushnumber(L, pos_self.x);
      lua_pushnumber(L, pos_self.y);
      lua_pushnumber(L, type);

      const int fail = lua_call(L, 4, 0);

      lua_diagnostics_error(fail);
    }
  }
}
