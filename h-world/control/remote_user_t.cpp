#include <stdio.h>

#include "remote_user_t.h"
#include "model/thing_t.h"
#include "model/world_t.h"
#include "model/level_t.h"
#include "view/server_view_t.h"
#include "net/server_socket_t.h"
#include "net/socketio_t.h"
#include "net/commands.h"

#include "swt/event_t.h"


remote_user_t::remote_user_t(thinghandle_t thing) : user_input_t(thing)
{
  socket = new server_socket_t(5552);

  socket->accept();
}


remote_user_t::~remote_user_t()
{
  delete socket;
  socket = 0;
}


/**
 * This method checks if the action is good to execute 
 * regarding the surrounding situation. 
 *
 * Used values so far:
 *   -1 (no attack possible)
 *  500 (attack possible)
 *    1 (move normal)
 *  999 (flee)
 *
 * @author Hansj�rg Malthaner
 */
int remote_user_t::check()
{
  return 999;
}


/**
 * This method performs the action. 
 * @return delay time for next action, a negative delay means
 *         the actor will no more be included in scheduling
 * @author Hansj�rg Malthaner
 */
int remote_user_t::execute()
{
  int delay = 10;
  bool done = false;

  server_view_t server_view;

  socketio_t sockio(socket);


  while(!done) {

    short cmd = sockio.read_short();


    printf("Got command '%hd'\n", cmd);


    switch(cmd) {
    case GET_MAP:
      {
	server_view.init();
	server_view.set_ij_off(get_thing()->get_pos());
	server_view.redraw();
	server_view.dump();
	
	sockio.write_short(server_view.get_mark());
	socket->write(server_view.get_buffer(), server_view.get_mark());
      }
      break;

    case PUT_EVENT:
      {
      event_t event;
    

      event.type = (enum event_t::event_type_t)sockio.read_short();
      event.code = sockio.read_short();

      done = handle_event(event, delay);

      }
      break;

    case GET_LEVEL_SIZE:
      {
	koord size = world_t::get_instance()->get_level()->get_size();
	sockio.write_short(size.x);
	sockio.write_short(size.y);
      }
    }


    /*

    // Do we still live?
    get_thing()->check_alive();

    server_view.init();
    server_view.set_ij_off(get_thing()->get_pos());
    server_view.redraw();
    server_view.dump();

    // ask for client input

    
    socket->read(buffer, 1);


    printf("Read '%c'\n", buffer[0]);

    if(buffer[0] >= 32) {
      event_t event;
    

      event.type = event_t::KEY_PRESS;
      event.code = buffer[0];

      done = handle_event(event, delay);

    }

    */

  }

  return delay;
}


// ---- implements persistent_t ----

/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void remote_user_t::read_write(storage_t *store, iofile_t * file)
{
  
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t remote_user_t::get_cid()
{
  // TODO
  return  ~0;
}
