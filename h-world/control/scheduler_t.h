/* Copyright by Hj. Malthaner */

#ifndef SCHEDULER_T_H
#define SCHEDULER_T_H

#include "tpl/prioqueue_tpl.h"

class actor_t;
class system_window_t;
class window_manager_t;

class scheduler_t {
public:    

    enum state {GAME, GAME_OVER, QUIT}; 

private:

    prioqueue_tpl <actor_t *> actors;

    /**
     * @link
     * @shapeType PatternLink
     * @pattern Singleton
     * @supplierRole Singleton factory 
     */
    /*# scheduler_t __scheduler_t; */
    static scheduler_t * single_instance;

    enum state go_on;


    /**
     * Game time, quantified
     * @author Hj. Malthaner
     */
    int time;


    /**
     * Currently executed actor (needed for remove_actor)
     * @author Hj. Malthaner
     */
    actor_t *current_actor;


    scheduler_t();


 public:
    
    int get_time() const {return time;};

    actor_t * get_current_actor() const;

    /**
     * Used after loading of a world to set the time of the loaded game
     * @author Hj. Malthaner
     */
    void set_time(int t) {time = t;};

    void set_state(enum state s) {go_on = s;};

    void remove_actor(actor_t * actor);

    void add_actor(actor_t * actor);

    static scheduler_t * get_instance();


    enum state process(system_window_t *syswin,
		       window_manager_t *winman);


    /**
     * Dump a listing of all actors to stderr
     * @author Hj. Malthaner
     */
    void dump();
};
#endif //SCHEDULER_T_H
