/* 
 * user_input_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>
#include <ctype.h>

#include "user_input_t.h"
#include "action_trade_t.h"
#include "pick_thing_t.h"
#include "attack_t.h"
#include "scheduler_t.h"
#include "lua_call_t.h"
#include "commands.h"
#include "thing_usage_selector_t.h"

#include "factories/thing_factory_t.h"
#include "factories/level_factory_t.h"

#include "model/thing_t.h"
#include "model/thing_tree_t.h"
#include "model/thing_constants.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/world_t.h"
#include "model/path_t.h"
#include "model/feature_t.h"
#include "model/message_log_t.h"
#include "model/pprops_t.h"

#include "swt/event_t.h"
#include "swt/color_t.h"
#include "swt/gui_frame_t.h"
#include "swt/window_manager_t.h"
#include "swt/system_window_t.h"
#include "primitives/cstring_t.h"

#include "gui/body_view_t.h"
#include "gui/skill_view_t.h"
#include "gui/usage_panel_t.h"
#include "gui/map_frame_t.h"
#include "gui/help_frame_t.h"
#include "gui/swap_frame_t.h"
#include "gui/book_frame_t.h"
#include "gui/dialog_frame_t.h"
#include "gui/multi_choice_t.h"
#include "gui/maptooltip_t.h"

#include "view/world_view_t.h"
#include "util/debug_t.h"
#include "util/rng_t.h"
#include "util/nsow.h"
#include "language/linguist_t.h"

#include "factories/hstore_t.h"
#include "persistence/gziofile_t.h"



static maptooltip_t * tooltip = 0;


// Base path for i/o files
static cstring_t base_path(".");


// Helper variable for loading the old usage panel state again 
static int def_usg = 0; 
static int def_atk = 0;


enum the_windows {
  WIN_INVENTORY,
  WIN_MAP,
  WIN_MAX
};

static drawable_t * windows[WIN_MAX];


static void show_frame(window_manager_t *winman, drawable_t *frame)
{
  frame->set_visible(true);

  if(winman->is_window_managed(frame)) {
    winman->top_window(frame);
  } else {
    winman->add_window(frame);
  }
}


static void close_windows()
{
  for(int i=0; i<WIN_MAX; i++) {
    if(windows[i]) {
      windows[i]->close();
      delete windows[i];
      windows[i] = 0;
    }
  }
}


thinghandle_t user_input_t::get_thing() const 
{
  return thing;
}


/**
 * Wait for a keypress
 * @author Hj. Malthaner
 */
int user_input_t::wait_for_key()
{
  event_t ev;

  do {
    ev = window_manager_t::get_instance()->get_and_process_events();
  } while(ev.type != event_t::KEY_PRESS);
  
  return ev.code;
}


static int wait_for_direction_key(bool self)
{
  event_t ev;

  if(self) {
    linguist_t::translate("Direction [9,6,3,2,5,1,4,7,8,ESC]?");
  } else {
    linguist_t::translate("Direction [9,6,3,2,1,4,7,8,ESC]?");
  }

  do {
    ev = window_manager_t::get_instance()->get_and_process_events();
  } while(ev.type != event_t::KEY_PRESS);
  
  return ev.code;
}


koord user_input_t::ask_direction(thinghandle_t thing, int max_dist)
{
  linguist_t::translate("Direction [9,6,3,2,1,4,7,8,ESC or mouse click]?");

  koord result (-2, -2);
  bool done = false;
  event_t ev;

  do {
    ev = window_manager_t::get_instance()->get_and_process_events();

    if(ev.type == event_t::MOUSE_MOVE) {
      int d;
      handle_move_event(ev, d);

      if(world_view_t::get_instance()->is_dirty()) {
	world_view_t::get_instance()->redraw();
      }
    }

    if(ev.type == event_t::KEY_PRESS) {
      if(ev.code >= '1' && ev.code <= '9') {
	result = directions[ev.code - '1'];
      }
      done = true;
    }

    if(ev.type == event_t::BUTTON_PRESS) {
      const koord ij = world_view_t::get_instance()->screen_to_world(ev.mpos);
      const koord pos = thing->get_pos();

      if(abs(ij.x - pos.x) <= max_dist && abs(ij.y - pos.y) <= max_dist) { 
	result = ij - pos;
	done = true;
      } else {
	linguist_t::translate("This is too far away. Please enter direction [9,6,3,2,1,4,7,8,ESC or mouse click]?");	
      }
    }

  } while(!done);



  return result;
}


/**
 * Base path, i.e. for loading/saving games
 * @author Hj. Malthaner
 */
void user_input_t::set_path(const char *path)
{
  base_path = path;
}


static void interactive_run()
{
  int dir = wait_for_direction_key(false) - '1';

  level_t * level = world_t::get_instance()->get_level();
  thinghandle_t player = world_t::get_instance()->get_player();
}


static void interactive_scan_quadrant(slist_tpl < handle_tpl < thing_t > > *list, 
			       koord k_base,
			       int d)
{
  int dir = -1;

  do {
    int code = wait_for_direction_key(false);
    
    switch(code) {
    case '9': dir = 0; break;
    case '3': dir = 1; break;
    case '1': dir = 2; break;
    case '7': dir = 3; break;

    case '6': dir = 4; break;
    case '2': dir = 5; break;
    case '4': dir = 6; break;
    case '8': dir = 7; break;

    case 27: dir = -2; break;
    }
  } while(dir == -1);
  
  if(dir >= 0 && dir <= 8) {
    world_t::get_instance()->get_level()->scan_quadrant(list, 
							k_base,
							d,
							dir);
  }
}


static void tell_player(const handle_tpl < thing_t > & thing,
			bool verbose)
{
  char buf[1024];
  thing->get_ident(buf);
  linguist_t::pvo("You", "see", false, buf);

  if(thing->get_inventory()) {
    linguist_t::translate("It might contain something.");
  }

  if(verbose) {
    const char * p = thing->get_string("desc", 0);

    if(p) {
      sprintf(buf, "� %s", p);
      linguist_t::translate(buf);
    }

    p = thing->get_string("desc2", 0);

    if(p) {
      sprintf(buf, "� %s", p);
      linguist_t::translate(buf);
    }
  }
}


void save_file(const char * filename)
{
  hstore_t store;
  gziofile_t file;
  // iofile_t file;

  int magic;

  magic  = 'H' << 24;
  magic += 'W' << 16;
  magic += 1;

  int version = 2;


  cstring_t fullname (base_path + "/" + filename);
  
  dbg->message("save_file()", "saving file '%s'", fullname.chars());


  file.open(fullname, true);
  
  file.rdwr_int(magic);
  file.rdwr_int(version);

  world_t::get_instance()->read_write(&store, &file);
  
  file.close();
}



int load_file(const char *filename)
{
  hstore_t store;
  gziofile_t file;
  // iofile_t file;
  int magic = 0;
  int result = 0;

  if(! file.open(base_path + "/" + filename, false)) {
    // printf("Cannot open %s\n", (base_path + "/" + filename).chars());

    result = -1;
  } else {
    file.rdwr_int(magic);

    const bool ok = 
      (((magic >> 24) & 255) == 'H') &&
      (((magic >> 16) & 255) == 'W') &&
      (((magic >>  8) & 255) ==  0) &&
      ( (magic & 255) ==  1);
    
    if(ok) {
      int version;
      file.rdwr_int(version);

      dbg->message("load_file()", 
		   "loading file version %d", version);

      file.set_version(version);

      {
	minivec_tpl <thinghandle_t> things_in_transit (128);
	world_t::get_instance()->destroy(things_in_transit);

	for(int i=0; i<things_in_transit.count(); i++) { 
	  dbg->message("load_file()", 
		       "destroying %s",
		       things_in_transit.at(i)->get_string("ident", "unnamed"));

	  if(things_in_transit.at(i)->get_string("is_player", 0) == 0) {
	    things_in_transit.at(i)->get_actor().delete_all_actions_but(0);
	  }

	  things_in_transit.at(i)->destroy();
	  things_in_transit.at(i) = 0;
	}
      }

      world_t::get_instance()->read_write(&store, &file);

      file.close();
      
      message_log_t::get_instance()->clear();
      
      linguist_t::translate("Game loaded.");
      
      dbg->message("load_file()", 
		   "Game loaded, refreshing view");
      

      // Hajo: why is this line needed?
      // redraw isn't called if this line is removed!
      window_manager_t::get_instance()->poll_and_process_events();

      world_view_t::get_instance()->resync();
      world_view_t::get_instance()->set_dirty(true);
      world_view_t::get_instance()->redraw();
      
      result = 0;
    } else {
      // wrong magick number
      result = -2;
    }
  }

  return result;
}


/**
 * Get type of action performed by this object
 * @author Hansj�rg Malthaner
 */
enum action_t::atype user_input_t::get_type() const
{
  return user_input;
}


void user_input_t::set_thing(thinghandle_t player) {
  thing = player;

  close_windows();

  if(panel) {
    panel->def_usg = def_usg;
    panel->def_atk = def_atk;
  }
}


void user_input_t::set_frametime(int frametime)
{
  this->frametime = frametime;
}


int user_input_t::cmd_talk(thinghandle_t & player, 
			   thinghandle_t & other)
{
  const char * filename = other->get_string("dialog", 0);
  int delay = 1;

  if(filename) {
    gui_frame_t * window = new dialog_frame_t(filename, false, other, player);

    window->set_pos(koord(160,60));
    window->set_visible(true);
    window_manager_t::get_instance()->add_window(window);
  } else {
    // does not speak
  }

  return delay;
}



int user_input_t::cmd_inspect_thing(thinghandle_t /*player*/, thinghandle_t thing)
{
  char ident[128];
  thing->get_ident(ident);

  linguist_t::pvo("You", "inspect", true, ident);
  
  body_view_t *frame = 
    new body_view_t(thing, 
		    "Inventory and body view",
		    true);
  
  frame->center();
  frame->refresh(thing->get_pos()); 
  frame->set_visible(true);

  show_frame(window_manager_t::get_instance(), frame);
  
  return 0;
}


int user_input_t::cmd_trade(thinghandle_t player, thinghandle_t thing)
{
  char ident[128];
  thing->get_ident(ident);

  linguist_t::pvo("You", "trade with", true, ident);
  
  action_trade_t *trade = new action_trade_t(thing);
  trade->set_price_factor(0);
  trade->set_customer(player);
  trade->execute();
  
  return 0;
}


static int cmd_open_inventory(thinghandle_t player)
{
  drawable_t *frame = windows[WIN_INVENTORY];

  if(frame == 0) {	    
    frame = windows[WIN_INVENTORY] = 
      new body_view_t(player, "Player inventory and body view", false);
    
    dynamic_cast<body_view_t *>(frame)->center();
  }
  
  dynamic_cast <body_view_t *> (frame) -> refresh(player->get_pos()); 
  
  frame->set_visible(true);
  show_frame(window_manager_t::get_instance(), frame);

  return 0;
}


/**
 * Player entered a movement command. Move PC and do everything
 * that movement can trigger
 * @author Hansj�rg Malthaner
 */
int user_input_t::cmd_move_to(koord new_pos)
{
  int delay = 10; // default, is overriden in specific case

  world_t * world = world_t::get_instance();
  level_t * level = world->get_level();
	  
  square_t * old_square = level->at(thing->get_pos());
  square_t * square = level->at(new_pos);

  if(square) {
	    
    const featurehandle_t & feature = square->get_feature();

    if(square->can_enter(thing)) {

      // time for move
      delay = 10;


      // if the room type changes, tell player about that
      if(old_square->room_type != square->room_type) {
	const properties_t * game_props = world->get_game_props();
	char buf[256];

	dbg->message("cmd_move()", "Room type=%d", square->room_type);

	sprintf(buf, "room_type_name[%d]", square->room_type);
	const char * name = game_props->get_string(buf);
	if(name == 0) {
	  name = "room";
	}

	linguist_t::pvo("You", "enter", false, name); 
      }


      // if there are things here, tell player about that
      minivec_iterator_tpl <thinghandle_t>  iter (level->at(new_pos)->get_things());

      while( iter.next() ) {
	const thinghandle_t & item = iter.get_current();
	if(item != thing) {   // don't bother player with himself
	  tell_player(item, false);
	}
      }


      // move 
      level->at(thing->get_pos())->remove_thing(thing);

      thing->visual.set_x_off(rng_t::get_the_rng().get_int(7)-3);
      thing->visual.set_y_off(rng_t::get_the_rng().get_int(3)-1);
      
      level->at(new_pos)->add_thing(thing, new_pos);
      world_view_t::get_instance()->set_ij_off(thing->get_pos());


      // in endless levels we need to switch to another area 
      // if the player gets too near to the border
      if(level->get_properties()->get_string("endless")) {
	if(new_pos.x < 10 || 
	   new_pos.y < 10 ||
	   new_pos.x > level->get_size().x-10 || 
	   new_pos.y > level->get_size().y-10) {
	  
	  // need to switch to another area;
		  
	  
	  pprops_t *props = level->get_properties();
	  props->set("other_area", "true");
	  
	  int x = *props->get_int("offset.x"); 
	  int y = *props->get_int("offset.y"); 
	  koord off;
	  koord trans_pos;
	  
	  if(new_pos.y < 10) {
	    off.y -= level->get_size().y-20;
	    trans_pos = koord(new_pos.x, (short)90);
	  }
	  else if(new_pos.x < 10) {
	    off.x -= level->get_size().y-20;
	    trans_pos = koord((short)90, new_pos.y);
	  }
	  else if(new_pos.y >= 90) {
	    off.y += level->get_size().y-20;
	    trans_pos = koord(new_pos.x, (short)10);
	  }
	  else if(new_pos.x >= 90) {
	    off.x += level->get_size().y-20;
	    trans_pos = koord((short)10, new_pos.y);
	  }

	  props->set("offset.x", x+off.x); 
	  props->set("offset.y", y+off.y); 
	  
	  const bool save_old = (x == 0 && y == 0);
	  const bool create_new = (x+off.x != 0) || (y+off.y != 0); 


	  dbg->message("user_input_t::handle_event()",
		       "create_new=%d "
		       "trans_pos=(%d, %d)", 
		       create_new,
		       trans_pos.x, trans_pos.y); 

	  thing->set("keep_position", "true");

	  thing->set_pos(trans_pos);

	  world_t::get_instance()->new_level(props, 
					     create_new,
					     save_old);


	  thing->set("keep_position", "false");


	  level_t * new_level = world_t::get_instance()->get_level();
	  if(!create_new) {
	    properties_t * props = new_level->get_properties();
	    // Hajo: we entered the core tile again - need to reset
	    // wrong saved offset of level template !!!
	    props->set("offset.x", 0); 
	    props->set("offset.y", 0);
	    
	    
	    printf("new pos is (%d, %d)\n", new_pos.x, new_pos.y);
	    printf("trans pos is (%d, %d)\n", trans_pos.x, trans_pos.y);
	  }
	} 
      }
    }
    // is there a feature there
    else if(feature.is_bound() && feature->is_blocking()) {
      feature->activate(thing);
      world_view_t::get_instance()->set_dirty(true);
	      
      // activate thing needs time
      delay = 1;
    } 
    // try to attack the obstacle, but don't attack self
    else if(new_pos != thing->get_pos()) {
      if(square->get_things().count()) {
	const minivec_tpl <thinghandle_t> & things = 
	  square->get_things();
	
	minivec_iterator_tpl <thinghandle_t> iter (things);
	
	while( iter.next() ) {
	  thinghandle_t & other = iter.access_current();
	  
	  if(*other->get_string("status", "enemy") == 'f') {
	    // Hajo: swap places with friend 
	    const koord mpos = thing->get_pos();
	    const koord opos = other->get_pos();

	    thinghandle_t follower (other);

	    level->at(opos)->remove_thing(follower);
	    level->at(mpos)->remove_thing(thing);

	    level->at(mpos)->add_thing(follower, mpos);
	    level->at(opos)->add_thing(thing, opos);

	  } else if(*other->get_string("status", "enemy") == 'n') {
	    // Hajo: talk?

	    cmd_talk(thing, other);

	  } else if(other->get_actor().is_animated()) {
	    // Hajo: attack obstacle
	    panel->get_selected_attack()->activate(thing, 0, other);
	    
	    // attack delay
	    delay = panel->get_selected_attack()->get_delay();
	  }
	}
      }
    }
    
    world_view_t::get_instance()->set_dirty(true);
  }

  return thing->calc_speed(delay);
}


/**
 * Player entered a movement command. Move PC and do everything
 * that movement can trigger
 * @author Hansj�rg Malthaner
 */
int user_input_t::cmd_move(int code)
{
  const koord new_pos = thing->get_pos() + directions[code - '1'];
  return cmd_move_to(new_pos);
}


/**
 * Targetting. Called interactively.
 * @return target
 * @author Hj. Malthaner
 */
thinghandle_t cmd_target(thinghandle_t thing)
{
  thinghandle_t result;

  slist_tpl < handle_tpl <thing_t> > list;

  interactive_scan_quadrant(&list,
			    thing->get_pos(),
			    8);

  slist_iterator_tpl < handle_tpl < thing_t > > iter (list);
  
  while( iter.next() ) {
    printf("%s\n", iter.get_current()->get_string("ident", "unnamed"));
  }

  if(list.count()) {
    bool exit = false;
    int idx = 0;

    linguist_t::translate("Target [5=next, SPACE=doit, ESC=cancel]?");

    do {
      event_t ev;
      
      // highlite

      if(list.count() == 1) {
	// only one target, nothing to choose
	ev.code = ' ';
      } else {

	list.at(idx)->visual.add_image(76, 0, 0, 0, 0, false);
	
	linguist_t::pvo("You", "aim at", true, list.at(idx)->get_string("ident", "unnamed"));

	world_view_t::get_instance()->set_dirty(true);
	world_view_t::get_instance()->redraw();
	      
	do {
	  ev = window_manager_t::get_instance()->get_and_process_events();
	} while(ev.type != event_t::KEY_PRESS);
	
	list.at(idx)->visual.rem_image(76, 0);
	world_view_t::get_instance()->set_dirty(true);
	world_view_t::get_instance()->redraw();
	
      }

      switch(ev.code) {
      case 27:
	exit = true;
	break;
	
      case '5':

	idx ++;
	if(idx >= list.count()) idx = 0;
	break;

      case ' ':
	result = list.at(idx);
	exit = true;
	break;

      }
      
    } while(!exit);
    
  } else {
    linguist_t::translate("No targets.");
  }

  // unbound handle = no target
  return result;
}



/**
 * Throw an item. Used ineractively.
 * @param thing the thrower
 * @param ammo_stack thing(s) to throw
 * @param multi damage multiplier
 * @author Hj. Malthaner
 */
int cmd_throw(thinghandle_t thing, thinghandle_t ammo_stack, int multi)
{
  int delay = 1;
  thinghandle_t target = cmd_target(thing);

  if(target.is_bound()) {

    thinghandle_t ammo;
    const int stack = ammo_stack->get_int("cur_stack", 1);
	    
    if(stack == 1) {
      ammo = ammo_stack;
    } else {
      ammo = ammo_stack->clone();
      ammo->set("cur_stack", 1);
      ammo_stack->set("cur_stack", stack-1);
    }
	    
    linguist_t::pvo("You", 
		    multi <= 1 ? "throw" : "shoot", 
		    false, ammo->get_string("ident", "unnamed"));
	  
    attack_t attack ("Throw", ammo);
		    
    // determine strength of damage

    const char *p = ammo->get_string("throw.cut", "%1d1");
    dice_t cut (p+1);
    cut.dice   *= multi;
    cut.offset *= multi;
    
    attack.set_cut(cut);
    
    p = ammo->get_string("throw.blunt", "%1d1");
    dice_t blunt (p+1);
    blunt.dice   *= multi;
    blunt.offset *= multi;

    attack.set_blunt(blunt);
    
    attack.set_pierce(ammo->get_int("throw.pierce", 0) * multi);

		    
    // show throwing effect

    const koord target_pos = target->get_pos();
    level_t * level = world_t::get_instance()->get_level();
    
    world_view_t::get_instance()->play_effect(thing->get_pos(),
					      target_pos,
					      false, true, false,
					      77,
					      // ammo->visual.get_image(0),

					      1);
    attack.set_verb("hit");
    attack.activate(thing, 0, target);
	 
    
    const char * lua = ammo->get_string("throw.lua", 0);
    if(lua) {
      lua_call_t call(lua);
      call.activate(target, ammo);
    }
	  
   
    // did we keep some?
    if(stack == 1) {
      thinghandle_t limb = ammo_stack->get_owner();
      if(limb.is_bound()) {
	ammo_stack->get_owner()->set_item(0, true);
      }
    }
	    
    level->at(target_pos)->add_thing(ammo, target_pos);
    
    world_view_t::get_instance()->set_dirty(true);
    
    // XXX throwing attack delay
    delay = 10;
  }

  return delay;
}


/**
 * Get a thing from ground
 * @author Hansj�rg Malthaner
 */
int user_input_t::cmd_get_thing(thinghandle_t player, thinghandle_t item)
{
  int delay = 10;

  pick_thing_t u;
    
  if(item.is_bound()) {
    delay = u.pick_up(player, item);
  } else {
    delay = u.activate(player, player->get_pos());
  }

  world_view_t::get_instance()->set_dirty(true);

  return delay;
}


void cmd_unlock_thing(thinghandle_t player, thinghandle_t container)
{
  if(*container->get_string(TH_LOCKED, "f") == 't') {
    // try to unlock container

    // Hajo: TODO: get lock subtype (must match key subtype)
    const char * subtype = 0;


    slist_tpl <thinghandle_t> * items = new slist_tpl <thinghandle_t>;

    thing_tree_t tree;

    // true means to list inventory contents, too
    tree.list_inventories(player, items, true);
    tree.list_items(player, items);

    slist_tpl <thinghandle_t> * tmp;

    tmp = tree.filter_list(items, 0, "key", subtype);
    delete items;
    items = tmp;

    if(items->count() > 0) {
      // Hajo: got a matching key
      container->set(TH_LOCKED, "false");
    } else {
      char buf[128];
      sprintf(buf, "%s key", subtype);

      linguist_t::pvo("You", "need", false, buf);
    }
  } 
}


int user_input_t::cmd_open_thing(thinghandle_t player, thinghandle_t container)
{
  cmd_unlock_thing(player, container);

  if(*container->get_string(TH_LOCKED, "f") == 'f') {
    gui_frame_t * window = 
      new swap_frame_t(player, container, true);
    
    window->set_pos(koord(160,60));
    window->set_visible(true);
  
    window_manager_t::get_instance()->add_window(window);
  
    char ident[256];
    container->get_ident(ident);
    linguist_t::pvo("You", "open", true, ident);
  }

  return 0;
}


/**
 * Player clicked a sqaure. Trigger actions for items on square.
 * @param self if true, player clicked his own square
 * @author Hansj�rg Malthaner
 */
int user_input_t::cmd_click_square(koord pos, bool * done, bool self)
{
  int delay = 0;
  *done = false;

  const level_t * level = world_t::get_instance()->get_level();
  square_t * square = level->at(pos);
  const minivec_tpl <thinghandle_t> & things = square->get_things();

  if(square) {
    minivec_iterator_tpl <thinghandle_t> iter (things);

    while(*done == false && iter.next()) {
      if(iter.get_current() != thing) {

	lua_call_t call("on_thing_clicked");
	delay = call.activate(thing, iter.get_current());
			      
	if(delay == -1) {
	  *done = false;
	  delay = 0;
	} else {
	  *done = true;
	}
      } else {
	// Player clicked player
	if(things.count() == 1 && self) {
	  cmd_open_inventory(thing);
	}
      }
    }  
  }

  return delay;
}


user_input_t::user_input_t(thinghandle_t thing) 
{
  this->thing = thing;
  this->frametime = -1;
  this->path = new path_t();
  path->dont_check_last = true;

  for(int i=0; i<WIN_MAX; i++) {
    windows[i] = 0;
  }

  panel = 0;

  if(tooltip == 0) {
    tooltip = new maptooltip_t();
  }
};


user_input_t::~user_input_t()
{
  delete path;

  dbg->message("user_input_t::~user_input_t()", 
	       "Called (%p)", this);
}


void user_input_t::set_usage_panel(usage_panel_t *upan)
{
  panel = upan;
}


/**
 * This method checks if the action is good to execute 
 * regarding the surrounding situation.
 */
int user_input_t::check()
{
  // Adjust! XXX
  return 0;
}


/**
 * Processes user input.
 */
int user_input_t::execute()
{
  bool done = false;
  int  delay = 0;
  
  window_manager_t *winman = window_manager_t::get_instance();

  while(!done) {

    // Do we still live?
    thing->check_alive();
    
    if(world_view_t::get_instance()->is_dirty()) {
      // Hajo: full update required ? 
      world_view_t::get_instance()->redraw();
    } else if(frametime > -1) {
      // Hajo: animation update
      world_view_t::get_instance()->animate(system_window_t::get_instance()->get_time());
    }


    event_t & event = winman->poll_and_process_events();
    bool ai_idle = (strcmp(thing->get_string("AI.state", "nothing"), "nothing") == 0);

    if(frametime == -1 && ai_idle) {

      // Turn based

      while(event.type == event_t::IGNORE_EVENT ||
	    event.type == event_t::NO_EVENT) {
	
	winman->get_and_process_events();
      }
    }


    // dbg->message("user_input_t::execute()", "Time %d - Got event %d, %d", scheduler_t::get_instance()->get_time(), event.type, event.code);

    if(thing->get_int("effect.paralyzed", 0) <= 0) {
      done = handle_event(event, delay);

    } else {
      delay = 10;
      done = true;
    }

    if(frametime != -1) {
      // realtime
      system_window_t::get_instance()->sleep(frametime);
    } else if(ai_idle == false) {
      static unsigned long last_time = 0;

      system_window_t * syswin = system_window_t::get_instance();
      const unsigned long now = syswin->get_time();
      const unsigned long delta = now - last_time;
      const unsigned long delay = 150ul;
      // printf("delta=%ld, wait=%ld\n", delta, delay-delta);

      if(delta < delay) {
	system_window_t::get_instance()->sleep(delay - delta);
      }
      last_time = now;
    }
  }

  return delay;
}


/**
 * Apply duration based effects
 * @author Hj. Malthaner
 */
void user_input_t::apply_effects(int delay)
{
  action_t::apply_effects(thing, delay);
}


bool user_input_t::handle_key_event(event_t & event, int & delay)
{
  window_manager_t *winman = window_manager_t::get_instance();  
  bool done = false;

  // Hajo: cancel old action
  thing->set("AI.state", "nothing");

  switch(event.code) {
  case '#':
    {
      const bool grid = world_view_t::get_instance()->use_grid(false);
      world_view_t::get_instance()->use_grid(!grid);
      world_view_t::get_instance()->set_dirty(true);
    }
  break;
  case '?':
  case 'h':
  case event_t::KEY_F1:
    {
      // false = not modal
      help_frame_t *frame = new help_frame_t("help.html", false);
      frame->set_visible(true);
      
      winman->add_window(frame);
    }
    break;

  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    {
      // Moving closes inventory view
      if(windows[WIN_INVENTORY] && 
	 winman->is_window_managed(windows[WIN_INVENTORY])) {
	windows[WIN_INVENTORY]->close();
      }
      
      
      delay = cmd_move(event.code);
      
      // XXX Hack -> unset event
      event.type = event_t::IGNORE_EVENT;
      

      // Update mini map if it's open
      drawable_t *frame = windows[WIN_MAP];
      
      if(frame && frame->is_visible()) {
	frame->redraw();
      }
      
      
      done = true;
    }
    break;

  case '.':
    interactive_run();
    break;
    
  case 'C':
    {
      gui_frame_t *frame = new skill_view_t(thing->get_actor().skills,
					    "Skills/Abilities");
      frame->set_visible(true);
      window_manager_t::get_instance()->add_window(frame);
    }
    break;
  case 'a':
    panel->def_atk_prev();
    break;
    
    /*
      case 'b':
      {
      gui_frame_t *frame = new dialog_frame_t("./data/testdialog.dia");
      frame->set_visible(true);
      window_manager_t::get_instance()->add_window(frame);
      }
      
      break;
    */
    
  case 'd':
    {
      // dig through a feature
      const koord dir = ask_direction(thing, 1);
      
      if(dir.x != -2) {
	const koord new_pos = thing->get_pos() + dir;
	
	level_t * level = world_t::get_instance()->get_level();
	square_t * square = level->at(new_pos);
	
	if(square) {
	  featurehandle_t feature = square->get_feature();
	  
	  if(feature.is_bound()) {
	    if(feature->is_tunnelable()) {
	      if(feature->tunnel <= 10) {
		square->set_feature(0);
		world_view_t::get_instance()->set_dirty(true);
		linguist_t::pvo("You", "have cleared", true, "square");
		done = true;
	      } else {
		feature->tunnel -= 10;
		char buf[256];
		sprintf(buf, "You start digging ... %d integrity points left.", feature->tunnel);
		linguist_t::translate(buf);
	      }
	    } else {
	      linguist_t::translate("This is too hard to be dug through.");
	    }
	  } else {
	    linguist_t::translate("There is nothing to dig away.");
	  }
	}
      }
    }

    break;
  case 'g':
    // pick up an item 
    delay = cmd_get_thing(thing, thinghandle_t());
    break;

  case 'i':
    delay = cmd_open_inventory(thing);
    break;
    
  case 'l':
    {
      slist_tpl < handle_tpl <thing_t> > list;
	  
      int d;

      if(world_t::get_instance()->get_level()->get_properties()->get("lit")) {
	d = 12;
      } else {
	d = thing->get_int("light_rad", 4)-2;
      }
      
      interactive_scan_quadrant(&list, thing->get_pos(), d);
      
      if(list.count()) {
	slist_iterator_tpl < handle_tpl < thing_t > > iter (list);
	
	while( iter.next() ) {
	  tell_player(iter.get_current(), true);
	  
	  if(list.count() > 1) {
	    linguist_t::translate("<more>");
	    wait_for_key();
	  }
	}
	linguist_t::translate("You finish looking.");
	
      } else {
	linguist_t::translate("You see nothing of interest there.");
      }
    }
    break;
    

  case 'm':
    {
      drawable_t *frame = windows[WIN_MAP];
	  
      if(frame == 0) {
	frame = windows[WIN_MAP] = new map_frame_t();
      }

      show_frame(winman, frame);
    }
    break;

      case 'o':             // open something
	{
	  const koord dir = ask_direction(thing, 1);

	  if(dir.x != -2) {
	    const koord pos = thing->get_pos() + dir;

	    level_t * level = world_t::get_instance()->get_level();

	    square_t * square = level->at(pos);

	    if(square) {
	      minivec_iterator_tpl <thinghandle_t> iter (square->get_things());
	      bool ok = false;
	      while(iter.next()) {
		if(iter.get_current()->get_inventory()) {
		  cmd_open_thing(thing, iter.get_current());
		  ok = true;
		}
	      }
    
	      if(!ok) {
		linguist_t::translate("There is nothing to open.");
	      }
	    }

	  } else {
	    linguist_t::translate("You think better of it.");
	  }
	}
	break;

      case 's':
	panel->def_atk_next();
	break;

      case 't':
	{
	  thinghandle_t ammo_stack;
	  thinghandle_t limb = thing_t::find_limb(thing, "hold", "weapon");
	  
	  if(limb.is_bound()) {
	    ammo_stack = limb->get_item();
	  }
	  
	  if(ammo_stack.is_bound()) {
	    delay = cmd_throw(thing, ammo_stack, 1);
	    done = true;
	  } else {
	    linguist_t::translate("Nothing to throw.");
	  }
	}
	break;

      case 'q':
	panel->def_usg_prev();
	break;
	
      case 'u':
	{
	  handle_tpl <thing_t> item = 
	    panel->get_selected_item_for_use();

	  if(item.is_bound()) {

	    // Hajo: let player select an usage
	    thing_usage_selector_t choice;
	    delay = choice.select_and_activate(thing, 
					       item->get_container(), 
					       item);

	    dbg->message("user_input_t::handle_event()", 
			 "Usage takes %d ticks", delay);

	    done = true;
	  } else {
	    linguist_t::translate("No usage selected.");
	  }
	}
	break;

      case 'w':
	panel->def_usg_next();
	break;


      case 'M':
	{
	  modal_frame_t * book = new book_frame_t(thing);

	  book->center();
	  book->hook();

	  break;
	}

      case 'L':
	{

	  close_windows();

	  linguist_t::translate("Loading game, please wait a few seconds ...");

	  const int ok = load_file("save/game.save");

	  if(ok == 0) {

	    // return immediately after loading
	    // notify scheduler, that our actor will no longer live
	    delay = -1;
	    done = true;
      
	    dbg->message("user_input_t::execute()", "Game loaded, done");

	  } else if(ok == -1) {
	    linguist_t::translate("Savegame file not found, game not loaded!");
	    delay = 1;

	  } else {
	    /*	    
	    dbg->message("user_input_t::execute()", "MAGICK %X%X%X%X\n", 
			 ((magic >> 24) & 255),
			 ((magic >> 16) & 255),
			 ((magic >>  8) & 255),
			 (magic & 255)
			 );	    
	    */
	    linguist_t::translate("Incompatible savegame version, game not loaded!");	    
	    delay = 1;
	  }
	}
	break;
	
      case 'S':
	{
	  linguist_t::translate("Saving game, please wait ...");


	  save_file("save/game.save");


	  // XXX Hack -> unset event
	  event.type = event_t::IGNORE_EVENT;

	  linguist_t::translate("Game saved.");
	}
	break;

      case 'F':
      case 'I':
      case 'T':
	{
	  int action = event.code;
	  if(action == 'I') {
	    linguist_t::translate("Inspect:");
	  } else if(action == 'T') {
	    linguist_t::translate("Trade:");
	  }


	  const koord dir = ask_direction(thing, 1);

	  printf("%d, %d\n", dir.x, dir.y);

	  if(dir.x != -2 && !(dir.x == 0 && dir.y == 0)) {
	    
	    const koord new_pos = thing->get_pos() + dir;
	    level_t * level = world_t::get_instance()->get_level();
	    square_t * square = level->at(new_pos);

	    if(square) {
	      minivec_iterator_tpl <thinghandle_t> iter (square->get_things());

	      while(iter.next()) {

		if(action == 'T') {
		  if(*(iter.get_current()->get_string("status", "enemy")) == 'f') {

		    cmd_trade(thing, iter.get_current());
		    break;
		  }
		} else if(action == 'I') {
		  if(*(iter.get_current()->get_string("status", "enemy")) == 'f' ||
		     iter.get_current()->get_actor().is_animated() == false) {
		    
		    int k = 'y';
		    char ident[128];
		    iter.get_current()->get_ident(ident);

		    if(square->get_things().count() > 1) {
		      char msg[1024];
		      sprintf(msg, "Inspect the %s [y,n]?", ident);
		    
		      linguist_t::translate(msg);
		      k = wait_for_key();
		    }

		    if(k == 'y') {
		      cmd_inspect_thing(thing, iter.get_current());
		      break;
		    }
		  }
		} else if(action == 'F') {
		  const char *type = iter.get_current()->get_string("type", "XXX");
		  if(type && strcmp(type, "victim")==0) {
		    world_t::get_instance()->kill(iter.get_current(), true);
		    
		    thing_factory_t thing_factory;
		    thinghandle_t princess = 
		      thing_factory.create("princess", 0, 0);
		    
		    level_factory_t::place_thing(level,
						 new_pos,
						 princess,
						 rng_t::get_the_rng());
		    
		    world_view_t::get_instance()->set_dirty(true);
		    world_view_t::get_instance()->redraw();
		    
		    break;
		  }
		}
	      }
	    }
	  } else {
	    linguist_t::translate("You think better of it.");
	  }
	}
	break;

      case 'Q':
      case 'X':
	scheduler_t::get_instance()->set_state(scheduler_t::QUIT);
	done = true;
	break;
      default:
	{
	  lua_call_t call("on_key_pressed");
	  delay = call.activate(thing, event.code);

	  world_view_t::get_instance()->set_dirty(true);

	  if(delay > 0) {
	    done = true;
	  }
	}
      }


  return done;
}


bool user_input_t::handle_move_event(event_t & event, int & delay)
{
  static koord old_ij = koord(-1, -1);
  static bool old_state = false;
  bool done = false;

  // printf("mpos.x=%d mpos.y=%d\n", event.mpos.x, event.mpos.y);

  const koord ij = world_view_t::get_instance()->screen_to_world(event.mpos);

  if(ij != old_ij) {
    world_view_t::get_instance()->cursor_ij = ij;
    level_t * level = world_t::get_instance()->get_level();
    square_t * square = level->at(ij);

    world_view_t::get_instance()->set_dirty(true);

    const bool state = 
      square && 
      (square->get_things().count() > 0 || square->get_feature().is_bound());

    if(state != old_state || state) {
      old_state = state;

      if(state) {
	const int n = square->get_things().count();
	if(n > 0) {
	  char buf[128];

	  if(level->is_viewable(ij)) {
	    const thinghandle_t & thing = square->get_things().get(n-1);
	    const int magic = thing->count_attributes();

	    thing->get_ident_plain(buf);

	    color_t color = color_t::WHITE;

	    if(magic < 1) {
	      color = color_t(224, 224, 224);
	    } else if(magic < 3) {
	      color = color_t(96, 136, 255);
	    } else {
	      color = color_t(224, 224, 96);
	    }

	    // descriptive text
	    char text[1024];
	    strcpy(text, thing->get_string("desc", ""));
	    if(*text != '\0') {
	      strcat(text, " ");
	      strcat(text, thing->get_string("desc2", ""));
	      strcat(text, " ");
	      strcat(text, thing->get_string("desc3", ""));
	    }

	    tooltip->set_text(buf, text, color);

	    window_manager_t::get_instance()->unshow_tool_tip();	  
	    window_manager_t::get_instance()
	      ->show_tool_tip(tooltip, event.mpos - koord(20, 40+tooltip->get_size().y));
	  } else {
	    window_manager_t::get_instance()->unshow_tool_tip();	  
	  }
	} else if(square->get_feature().is_bound()) {
	  const char * featname = square->get_feature()->get_ident(0);
	  if(featname) {
	    if(level->is_viewable(ij)) {
	      color_t color (160, 160, 160);

	      if(square->get_feature()->get_peer().is_bound()) {
		color = color_t(248, 160, 32);
	      }

	      tooltip->set_text(featname, "", color);

	      window_manager_t::get_instance()->unshow_tool_tip();	  
	      window_manager_t::get_instance()
		->show_tool_tip(tooltip, event.mpos - koord(20, 40));
	    } else {
	      window_manager_t::get_instance()->unshow_tool_tip();	  
	    }
	  } else {
	    window_manager_t::get_instance()->unshow_tool_tip();	  
	  }
	} else {
	  window_manager_t::get_instance()->unshow_tool_tip();	  
	}
      } else {
	window_manager_t::get_instance()->unshow_tool_tip();
      }
    }

    old_ij = ij;
  }

  return done;
}


bool user_input_t::handle_click_event(event_t & event, int & delay)
{
  bool done = false;

  const koord pos = thing->get_pos();
  const koord ij = world_view_t::get_instance()->screen_to_world(event.mpos);
  level_t * level = world_t::get_instance()->get_level();
  square_t * square = level->at(ij);


  // Hajo: cancel old action
  thing->set("AI.state", "nothing");

  
  // Hajo: valid click ?
  if(square) {
    if(ij == pos) {
      // Hajo: click self -> use/pickup

      if(event.code == event_t::BUTTON_LEFT) {
	delay = cmd_click_square(ij, &done, true);
      } else {
	// Todo: use
      }

    } else if(abs(ij.x - pos.x) <= 1 && abs(ij.y - pos.y) <= 1) { 
      // Hajo: click near -> activate or walk
      if(square->get_feature().is_bound() &&
	 square->get_feature()->is_blocking()) {
	square->get_feature()->activate(thing);
	world_view_t::get_instance()->set_dirty(true);
	done = true;
      } else {
	delay = cmd_click_square(ij, &done, false);
	
	if(!done) {
	  delay = cmd_move_to(ij);
	  done = true;
	}
      }

    } else {
      // Hajo: click far -> walk

      delay = cmd_click_square(ij, &done, false);

      look_for_pos_t dest(level, ij);
      
      const bool ok = path->calculate(level, 
				      thing,
				      thing->get_pos(), 
				      &dest);
      if(ok) {
	dbg->message("user_input_t::handle_click_event()",
		     "found a %d step path to %d,%d",
		     path->get_size(), ij.x, ij.y);
	
	path->n = 0;
	thing->set("AI.state", "follow_path");
      } else {
	// Hajo: No path - do something else
	dbg->message("user_input_t::handle_click_event()",
		     "found no path to %d,%d",
		     ij.x, ij.y);
      
	path->n = -1;
	thing->set("AI.state", "nothing");
      }

      delay = 0;
    }
  }

  return done;
}


bool user_input_t::handle_event(event_t & event, int & delay)
{
  bool done = false;

  const char * state = thing->get_string("AI.state", "nothing");

  if(strcmp(state, "follow_path") == 0) {
    path->n++;
    if(path->n < (int)path->get_size()) {
      delay = cmd_move_to(path->get_step(path->n));

      if(thing->get_pos() != path->get_step(path->n)) {
	// Hajo: path blocked, stop moving
	thing->set("AI.state", "nothing");
      }
    } else {
      thing->set("AI.state", "nothing");
      delay = cmd_click_square(thing->get_pos(), &done, false);
    }
		
    done = true;
    
  } else if(event.type == event_t::KEY_PRESS) {
    window_manager_t::get_instance()->unshow_tool_tip();
    done = handle_key_event(event, delay);

  } else if(event.type == event_t::BUTTON_RELEASE) {
    window_manager_t::get_instance()->unshow_tool_tip();
    done = handle_click_event(event, delay);

  } else if(event.type == event_t::MOUSE_MOVE) {
    done = handle_move_event(event, delay);

  }

  return done;
}


// ---- implements persistent_t ----

user_input_t::user_input_t()
{
  // ---- implements persistent_t ----

  path = new path_t();
  path->dont_check_last = true;

  for(int i=0; i<WIN_MAX; i++) {
    windows[i] = 0;
  }

  panel = 0;

  if(tooltip == 0) {
    tooltip = new maptooltip_t();
  }

  dbg->message("user_input_t::user_input_t()", 
	       "Called (%p)", this);
}


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void user_input_t::read_write(storage_t *store, iofile_t * file)
{
  if(panel) {
    def_usg = panel->def_usg;
    def_atk = panel->def_atk;
  }

  file->rdwr_int(def_usg);
  file->rdwr_int(def_atk);

  dbg->message("user_input_t::read_write_t()",
	       "def_usg=%d, def_atk=%d", def_usg, def_atk);
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t user_input_t::get_cid()
{
  return hstore_t::t_user_input;
}
