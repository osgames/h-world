/* 
 * action_trade_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "action_trade_t.h"

#include "model/world_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/thing_t.h"
#include "control/skills_t.h"
#include "util/debug_t.h"

#include "view/world_view_t.h"
#include "gui/trade_view_t.h"
#include "swt/window_manager_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"


/**
 * Get type of action performed by this object
 * @author Hansj�rg Malthaner
 */
enum action_t::atype action_trade_t::get_type() const
{
  return action_t::trade;
}


void action_trade_t::set_customer(thinghandle_t thing)
{
  customer = thing;
}


/**
 * Player must pay x times the value of the goods
 * @author Hj. Malthaner
 */
void action_trade_t::set_price_factor(int f)
{
  price_factor = f;
}


action_trade_t::action_trade_t() {
  // ---- implements persistent_t ----

  window = 0;
  price_factor = 256;
}


action_trade_t::action_trade_t(thinghandle_t thing)
{
  this->thing = thing;
  window = 0;
  price_factor = 256;
}


action_trade_t::~action_trade_t() 
{
  if(window) {
    window->close();
    delete window;
    window = 0;
  }
}


/**
 * Checks if it's good to attack now.
 * @author Hj. Malthaner
 */
int action_trade_t::check()
{
  return -1;

  /*
  static const koord direction [8] = {
    koord( 1, 0),
    koord( 1, 1),
    koord( 0, 1),
    koord(-1, 1),
    koord(-1, 0),
    koord(-1,-1),
    koord( 0,-1),
    koord( 1,-1),
  };

  level_t * level = world_t::get_instance()->get_level();

  int val = -1;

  for(int i=0; i<8; i++) {
    const koord k = thing->get_pos() + direction[i];
      
    if(level->at(k)) {
      minivec_iterator_tpl <thinghandle_t> iter (level->at(k)->get_things());
	
      while(iter.next()) {
	// printf("'%s'\n", iter.get_current()->get_string("name", "XXX"));
	  
	if(iter.get_current()->get_string("is_player", 0)) {
	    
	  customer = iter.get_current(); 

	  val = 550;
	  goto done;
	}
      }
    }
  }

  done:
  return val;
  */
}


/**
 * Attacks. 
 * @author Hj. Malthaner
 */
int action_trade_t::execute()
{
  int delay = 10;

  dbg->message("action_trade_t::execute()", 
	       "Called.");


  // Hajo: redraw map, player has moved
  world_view_t::get_instance()->redraw();


  window_manager_t *winman = window_manager_t::get_instance();

  if(window == 0) {
    trade_view_t *trade_view = new trade_view_t(customer, thing);

    // Hajo: calculate price factor from players skills and
    // traders demands

    const int v = customer->get_actor().skills->get_skill_sum(skills_t::trade);

    // dbg->message("action_trade_t::execute()", "v = %d\n", v);


    trade_view->set_price_factor((price_factor*v)/100000);

    window = trade_view;

    window->set_pos(koord(160,60));

    winman->add_window(window);
    window->set_visible(true);

  } else {

    if(winman->is_window_managed(window)) {
      winman->top_window(window);
    } else {
      winman->add_window(window);
    }

    window->set_visible(true);
  }


  return delay;
}


/**
 * Apply duration based effects
 * @author Hj. Malthaner
 */
void action_trade_t::apply_effects(int delay)
{
  action_t::apply_effects(thing, trade);
}


// ---- implements persistent_t ----

/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void action_trade_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_int(price_factor);

  if(file->is_saving()) {
    store->store(thing.get_rep(), file);
  } else {
    thing = thinghandle_t((thing_t *)store->restore(file, hstore_t::t_thing));
  }
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t action_trade_t::get_cid()
{
  return hstore_t::t_action_trade;
}
