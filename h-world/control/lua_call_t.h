/* 
 * lua_call_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef LUA_CALL_T_H
#define LUA_CALL_T_H

#include "usage_t.h"

class thing_t;
class inventory_t;
template <class T> class handle_tpl;

/**
 * Usage to call a lua script
 * @author Hansj�rg Malthaner
 */
class lua_call_t : public usage_t {
 private:

  /**
   * Name of the Lua method to call
   * @author Hansj�rg Malthaner
   */
  char call[32];

 public:

  /**
   * XXX Get time delay to do this usage - FIXME
   * @author Hansj�rg Malthaner
   */
  int get_delay() const {return 10;};


  void set_call(const char * call);
  

  lua_call_t(const char * call);


  /**
   * Not implemented !!!
   * @param user the thing which activates this usage (for reflexive usages)
   * @return stack count reduction of the used thing, if the stack count drops
   *         to 0 the thing is supposed to be removed
   * @author Hansj�rg Malthaner
   */
  virtual int activate(const handle_tpl <thing_t> &user,
		       koord pos);


  /**
   * Calls a lua function, the function name is the verb set for this usage.
   * @param user the thing which activates this usage (for reflexive usages)
   * @return stack count reduction of the used thing, if the stack count drops
   *         to 0 the thing is supposed to be removed
   * @author Hansj�rg Malthaner
   */
  virtual int activate(const handle_tpl <thing_t> &user,
		       inventory_t * inv,
		       const handle_tpl <thing_t> &t);



  // non-usage API lua calls follow

  int activate(const handle_tpl <thing_t> &user,
	       const handle_tpl <thing_t> &t1);


  int activate(const handle_tpl <thing_t> &user,
	       const handle_tpl <thing_t> &t1,
	       const handle_tpl <thing_t> &t2);


  int activate(const handle_tpl <thing_t> &user,
	       inventory_t * inv,
	       const handle_tpl <thing_t> &t1,
	       const handle_tpl <thing_t> &t2);


  int activate(const handle_tpl <thing_t> &user, int value);
  void activate(char * buf, void * any1, void * any2);


  // ---- implements persistent_t ----


  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};


#endif

