/* Copyright by Hj. Malthaner */

#include <string.h>

#include "attack_t.h"
#include "model/thing_t.h"
#include "model/thing_traversor_t.h"
#include "model/thing_constants.h"
#include "control/skills_t.h"
#include "view/world_view_t.h"

#include "language/linguist_t.h"
#include "util/rng_t.h"
#include "tpl/handle_tpl.h"

// Hack, for winner check!
#include "gui/help_frame_t.h"
#include "swt/window_manager_t.h"

#include "lua_system.h"

#include "factories/hstore_t.h"


attack_t::attack_t()
{
  // only needed for persistence layer
}


/**
 * Construct an attack with a name
 * @param name name of the attack
 * @param item the thing that grants this attack
 * @author Hj. Malthaner
 */
attack_t::attack_t(const char * name, handle_tpl <thing_t> i) :
  blunt(0,0,0), cut(0,0,0), pierce(0), item(i)
{
  set_name(name);

  // default is ten time quants
  delay = 10;

  // set_policy(usage_t::attack);
}


attack_t::~attack_t()
{
  hit_limb = 0;
  item = 0;
}


/**
 * Executes the attack
 * @return stack count reduction of the used thing, if the stack count drops
 *         to 0 the thing is supposed to be removed
 * @author Hansj�rg Malthaner
 */
int attack_t::activate(const handle_tpl <thing_t> &user,
		       inventory_t * inv,
		       const handle_tpl <thing_t> &target)
{

  if(target.is_bound()) {

    // Hajo: little animation

    world_view_t * view = world_view_t::get_instance();

    const int yoff = user->visual.get_y_off();

    user->visual.set_y_off(yoff+2);

    view->set_dirty(true);
    view->redraw();


    // Hajo: actual attacking code


    thing_traversor_t trav;
    
    trav.set_limb_visitor(this);
    
    lua_State * L = lua_state();

    lua_getglobal(L, "attack1");
    lua_pushusertag(L, user.get_rep(), LUA_ANYTAG);
    lua_pushusertag(L, target.get_rep(), LUA_ANYTAG);
    lua_pushusertag(L, item.get_rep(), LUA_ANYTAG);

    const int fail = lua_call(L, 3, 1);
    
    lua_diagnostics_error(fail);

    const int r = (int)lua_tonumber(L, 1);
    
    lua_pop(L,1);

    // do some talk - only text output here

    if(r == 1) {

      if(user->get_string(TH_IS_PLAYER, 0)) {
	linguist_t::pvo("You",
			get_verb(),
			true,
			target->get_string("ident", "Unnamed"));
	

	linguist_t::svo(target->get_string ("ident", "unnamed"), 
			"block", "attack");
      }
      else {

	if(target->get_string(TH_IS_PLAYER, 0)) {
	  linguist_t::svp(user->get_string("ident", "Unnamed"),
			  get_verb());
	

	  linguist_t::pvo("You", "block", true, "attack");
	} else {
	  linguist_t::svo(user->get_string("ident", "Unnamed"),
			  get_verb(),
			  target->get_string ("ident", "unnamed"));
	

	  linguist_t::svo(target->get_string ("ident", "unnamed"), 
			  "block", "attack");
	  
	}

      }
    } else if(r == 2) {

      if(user->get_string(TH_IS_PLAYER, 0)) {
	linguist_t::pvo("You",
			"miss",
			true,
			target->get_string("ident", "Unnamed"));
      }
      else {
	if(target->get_string(TH_IS_PLAYER, 0)) {
	  linguist_t::svp(user->get_string("ident", "Unnamed"),
			  "miss");
	} else {
	  linguist_t::svo(user->get_string("ident", "Unnamed"),
			  "miss",
			  target->get_string("ident", "Unnamed"));
	}
      }

    } else {

      trav.set_model(target);
      hit_limb = 0;

      
      sum = 0;
      limbs = 0;
      summarize = true; // hidden parameter for trav 
      trav.traverse();

      if(limbs > 1) {
	// printf("  sum=%d\n", sum);

	if(sum == 0) {
	  // nothing to hit? Well just hit first possible limb.
	  hit_location = 0;
	} else {
	  // determine hit location
	  hit_location = rng_t::get_the_rng().get_int(sum);
	}

	// printf("  hit location=%d\n", hit_location);

	sum = 0;
	summarize = false; // hidden parameter for trav 
	trav.traverse();
      }

      const char * hit_string = 0;

      if(hit_limb.is_bound()) {
	hit_string = hit_limb->get_string("name", "Unnamed");
      } else {
	hit_limb = target;
      }

      if(hit_string) {
	
	if(user->get_string(TH_IS_PLAYER, 0)) {
	  linguist_t::pvso("You", 
			   get_verb(),
			   target->get_string("ident", "Unnamed"),
			   hit_string
			   );
	} else {
	  if(target->get_string(TH_IS_PLAYER, 0)) {
	    linguist_t::svpo(user->get_string("ident", "Unnamed"),
			     get_verb(),
			     "your",
			     hit_string
			     );
	  } else {
	    linguist_t::svso(user->get_string("ident", "Unnamed"),
			     get_verb(),
			     target->get_string("ident", "Unnamed"),
			     hit_string
			     );
	  }
	}
      } else {

	if(user->get_string(TH_IS_PLAYER, 0)) {
	  linguist_t::pvo("You", 
			  get_verb(),
			  true,
			  target->get_string("ident", "Unnamed")
			  );
	} else {
	  if(target->get_string(TH_IS_PLAYER, 0)) {
	    linguist_t::svp(user->get_string("ident", "Unnamed"),
			    get_verb()
			    );
	  } else {
	    linguist_t::svo(user->get_string("ident", "Unnamed"),
			    get_verb(),
			    target->get_string("ident", "Unnamed")
			    );
	  }
	}
      }


      // is there armor there?
      thinghandle_t armor = hit_limb->get_item();


      lua_State * L = lua_state();

      lua_getglobal(L, "attack2");
      lua_pushusertag(L, user.get_rep(), LUA_ANYTAG);
      lua_pushusertag(L, item.get_rep(), LUA_ANYTAG);
      lua_pushusertag(L, target.get_rep(), LUA_ANYTAG);

      if(armor.is_bound()) {
	lua_pushusertag(L, armor.get_rep(), LUA_ANYTAG);
      } else {
	lua_pushnil(L);
      }
      lua_pushusertag(L, hit_limb.get_rep(), LUA_ANYTAG);

      lua_pushnumber(L, blunt.dice);
      lua_pushnumber(L, blunt.sides);

      lua_pushnumber(L, cut.dice);
      lua_pushnumber(L, cut.sides);

      lua_pushnumber(L, pierce);
      
      const int fail = lua_call(L, 10, 0);
    
      lua_diagnostics_error(fail);


      // free ref
      hit_limb = 0;
      trav.set_model(0);

      // add some value to the skill
      user->get_actor().skills->add_skill_value(skills_t::blades, 10);
      bool died = target->get_int("HPcurr", -1) < 0;

      if(died) {
	user->set("score", 
		  user->get_int("score", 0) + target->get_int("kill_score", 0));


	if(strcmp(user->get_string("quest", ""), 
		  target->get_string("ident", "-")) == 0) {

	  // true = modal dialog
	  help_frame_t *frame = new help_frame_t("html/winner.html", true);
	  frame->set_visible(true);
	  frame->set_pos(koord(350,200));
	  window_manager_t::get_instance()->add_window(frame);	  
	}

	target->check_alive();
      }
    }

    user->visual.set_y_off(yoff);

    view->set_dirty(true);
    view->redraw();

  }
  else {
    linguist_t::translate("There is nothing to attack.");
  }

  return 0;
}


/**
 * Implements limb visitor_t
 * @author Hansj�rg Malthaner
 */
void attack_t::visit_limb(const handle_tpl <thing_t> &limb)
{
  limbs ++;

  if(summarize) {
    sum += limb->get_int("hitchance", 0);
  } else {
    const int chance =  limb->get_int("hitchance", 0);

    if(chance) {
      sum += chance;

      if(hit_location <= sum && !hit_limb.is_bound()) {
	hit_limb = limb;
      }
    }
  }
}



/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void attack_t::read_write(storage_t *store, iofile_t * file)
{
  usage_t::read_write(store, file);

  file->rdwr_uchar(blunt.dice);
  file->rdwr_uchar(blunt.sides);

  file->rdwr_uchar(cut.dice);
  file->rdwr_uchar(cut.sides);

  file->rdwr_uchar(pierce);
  file->rdwr_uchar(delay);

  if(file->is_saving()) {
    store->store(item.get_rep(), file);
  } else {
    item = thinghandle_t( (thing_t *) store->restore(file, hstore_t::t_thing));
  }

}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t attack_t::get_cid()
{
  return hstore_t::t_attack;
}
