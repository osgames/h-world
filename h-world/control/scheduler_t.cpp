/* Copyright by Hj. Malthaner */

#include <unistd.h>


#include "actor_t.h"
#include "scheduler_t.h"
#include "swt/system_window_t.h"
#include "swt/window_manager_t.h"
#include "view/world_view_t.h"
#include "util/debug_t.h"

#include "tpl/no_such_element_exception.h"
#include "primitives/invalid_argument_exception_t.h"
#include "persistence/persistence_exception_t.h"

scheduler_t * scheduler_t::single_instance= 0;

scheduler_t * scheduler_t::get_instance(){
  if (single_instance == 0) {
    single_instance = new scheduler_t();
  }
  return single_instance;
}


actor_t * scheduler_t::get_current_actor() const
{
  return current_actor;
}


scheduler_t::scheduler_t() 
{
  time = 0;
  current_actor = 0;
}


void scheduler_t::remove_actor(actor_t * actor)
{
  // dbg->message("scheduler_t::remove_actor()", "removing %p", actor);
  actors.remove(actor);

  // check if current actor is to be removed ...
  if(current_actor == actor) {
    current_actor = 0;
  }
}



void scheduler_t::add_actor(actor_t * actor) 
{
  if(!actors.contains(actor)) {
    // dbg->message("scheduler_t::add_actor()", "adding %p", actor);

    actors.insert(actor);
  }
}



enum scheduler_t::state scheduler_t::process(system_window_t *syswin,
			 window_manager_t *winman)

{
  go_on = GAME;

  do {
    int delay = 0;
    current_actor = actors.pop();

    // dbg->message("scheduler_t::process()", "Time %d, Processing actor", time);

    try {
      // try to execute an action
      delay = current_actor->execute();      
      
    } catch ( invalid_argument_exception_t oops ) {
      dbg->error("scheduler_t::process()", oops.reason);
    } catch ( no_such_element_exception ex ) {
      dbg->error("scheduler_t::process()", ex.reason);
    } catch ( persistence_exception_t pe ) {
      dbg->fatal("scheduler_t::process()", pe.msg);
    } catch ( ... ) {
      dbg->error("scheduler_t::process()", "Unhandled exception caught, trying to continue");	
    }

    // dbg->message("scheduler_t::process()", "delay is %d for %p", delay, actor);

    // Hajo: curent actor might have been removed by remove_actor()!
    if(current_actor && delay >= 0) {
      time = current_actor->get_next_time();
      current_actor->add_time(delay);

      actors.insert(current_actor);
    } else {
      dbg->message("scheduler_t::process()", 
		   "Unscheduling actor %p", current_actor);
    }

  } while(go_on == GAME);

  return go_on;
}


/**
 * Dump a listing of all actors to stderr
 * @author Hj. Malthaner
 */
void scheduler_t::dump()
{
  const int n = actors.count();
  printf("Scheduler has %d actions\n", n);
}
