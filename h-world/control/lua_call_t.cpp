/* 
 * lua_call_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "util/debug_t.h"
#include "lua_call_t.h"
#include "lua_system.h"
#include "model/thinghandle_t.h"
#include "tpl/handle_tpl.h"

#include "factories/hstore_t.h"


void lua_call_t::set_call(const char * call)
{
  strncpy(this->call, call, 32);
  this->call[31] = '\0';
}


lua_call_t::lua_call_t(const char * call)
{
  set_name("Lua_call");
  set_call(call);
}


/**
 * Picks up first thing from location
 * @param user the thing which activates this usage (for reflexive usages)
 * @return stack count reduction of the used thing, if the stack count drops
 *         to 0 the thing is supposed to be removed
 * @author Hansj�rg Malthaner
 */
int lua_call_t::activate(const handle_tpl <thing_t> &user,
			  koord pos)
{
  return 0;
}


/**
 * 
 * @param user the thing which activates this usage (for reflexive usages)
 * @return stack count reduction of the used thing, if the stack count drops
 *         to 0 the thing is supposed to be removed
 * @author Hansj�rg Malthaner
 */
int lua_call_t::activate(const handle_tpl <thing_t> &user,
			 inventory_t * inv,
			 const handle_tpl <thing_t> &t1)
{
  dbg->message("lua_call_t::activate()", "calling1 '%s'", call);

  lua_State * L = lua_state();

  // the lua function name must be the usages verb
  lua_getglobal(L, call);
  lua_pushusertag(L, user.get_rep(), LUA_ANYTAG);

  if(inv) {
    lua_pushusertag(L, inv, LUA_ANYTAG);
  } else {
    lua_pushnil(L);
  }

  lua_pushusertag(L, t1.get_rep(), LUA_ANYTAG);
  const int fail = lua_call(L, 3, 1);

  lua_diagnostics_error(fail);

  const int reduction = (int)lua_tonumber(L, -1);

  lua_pop(L,1);

  return reduction;
}


int lua_call_t::activate(const handle_tpl <thing_t> &user,
			 const handle_tpl <thing_t> &t1)
{
  dbg->message("lua_call_t::activate()", "calling2 '%s'", call);

  lua_State * L = lua_state();

  // the lua functions name must be the usages verb
  lua_getglobal(L, call);
  lua_pushusertag(L, user.get_rep(), LUA_ANYTAG);
  lua_pushusertag(L, t1.get_rep(), LUA_ANYTAG);
  const int fail = lua_call(L, 2, 1);

  lua_diagnostics_error(fail);

  const int reduction = (int)lua_tonumber(L, -1);

  lua_pop(L,1);

  return reduction;
}


int lua_call_t::activate(const handle_tpl <thing_t> &user,
			 const handle_tpl <thing_t> &t1,
			 const handle_tpl <thing_t> &t2)
{
  dbg->message("lua_call_t::activate()", "calling3 '%s'", call);

  lua_State * L = lua_state();

  // the lua functions name must be the usages verb
  lua_getglobal(L, call);
  lua_pushusertag(L, user.get_rep(), LUA_ANYTAG);
  lua_pushusertag(L, t1.get_rep(), LUA_ANYTAG);
  lua_pushusertag(L, t2.get_rep(), LUA_ANYTAG);
  const int fail = lua_call(L, 3, 1);

  lua_diagnostics_error(fail);

  const int reduction = (int)lua_tonumber(L, -1);

  lua_pop(L,1);

  return reduction;
}


int lua_call_t::activate(const handle_tpl <thing_t> &user,
			 inventory_t * inv,
			 const handle_tpl <thing_t> &t1,
			 const handle_tpl <thing_t> &t2)
{
  dbg->message("lua_call_t::activate()", "calling4 '%s'", call);

  lua_State * L = lua_state();

  // the lua functions name must be the usages verb
  lua_getglobal(L, call);
  lua_pushusertag(L, user.get_rep(), LUA_ANYTAG);
  lua_pushusertag(L, inv, LUA_ANYTAG);
  lua_pushusertag(L, t1.get_rep(), LUA_ANYTAG);
  lua_pushusertag(L, t2.get_rep(), LUA_ANYTAG);
  const int fail = lua_call(L, 4, 1);

  lua_diagnostics_error(fail);

  const int reduction = (int)lua_tonumber(L, -1);

  lua_pop(L,1);

  return reduction;
}


int lua_call_t::activate(const handle_tpl <thing_t> &user, int value)

{
  dbg->debug("lua_call_t::activate()", "calling5 '%s'", call);

  lua_State * L = lua_state();

  // the lua functions name must be the usages verb
  lua_getglobal(L, call);
  lua_pushusertag(L, user.get_rep(), LUA_ANYTAG);
  lua_pushnumber(L, value);
  const int fail = lua_call(L, 2, 1);

  lua_diagnostics_error(fail);

  const int result = (int)lua_tonumber(L, -1);

  lua_pop(L,1);

  return result;
}


void lua_call_t::activate(char * buf, void * any1, void * any2)
{
  dbg->debug("lua_call_t::activate()", "calling6 '%s'", call);

  lua_State * L = lua_state();

  // the lua functions name must be the usages verb
  lua_getglobal(L, call);

  if(any1) {
    lua_pushusertag(L, any1, LUA_ANYTAG);
  } else {
    lua_pushnil(L);    
  }

  if(any2) {
    lua_pushusertag(L, any2, LUA_ANYTAG);
  } else {
    lua_pushnil(L);    
  }
 
  const int fail = lua_call(L, 2, 1);

  lua_diagnostics_error(fail);

  const char *result = lua_tostring(L, -1);

  // dbg->message("lua_call_t::activate()", "result is '%s'", result);

  if(result == 0) {
    result = "";
  }

  sprintf(buf, result);

  lua_pop(L,1);
}


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void lua_call_t::read_write(storage_t *store, iofile_t * file)
{
  usage_t::read_write(store, file);
  file->rdwr_carr(call);  
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t lua_call_t::get_cid()
{
  return hstore_t::t_lua_call;
}
