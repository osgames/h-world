/* 
 * action_attack_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef ACTION_ATTACK_T_H
#define ACTION_ATTACK_T_H


#include "action_t.h"
#include "model/thinghandle_t.h"
#include "ifc/thing_listener_t.h"
#include "ifc/thing_visitor_t.h"
#include "ifc/limb_visitor_t.h"

#include "tpl/handle_tpl.h"
#include "tpl/slist_tpl.h"

class attack_t;
class square_t;

class action_attack_t : public action_t, thing_listener_t, thing_visitor_t, limb_visitor_t  {
private:
  thinghandle_t thing;

  slist_tpl <attack_t*> attacks;

  attack_t *chosen_attack;

  handle_tpl <thing_t> attackee;

  /**
   * Check if there is something attackeable there
   * @author Hansj�rg Malthaner
   */
  int attack_square(square_t * square,
		    const char * const is_friend);

public:


  /**
   * Get type of action performed by this object
   * @author Hansj�rg Malthaner
   */
  virtual enum atype get_type() const;


  action_attack_t(thinghandle_t thing);

  ~action_attack_t();


  /**
   * Checks if it's good to attack now.
   * @author Hj. Malthaner
   */
  int check();


  /**
   * Attacks. 
   * @return delay time for next action
   * @author Hj. Malthaner
   */
  int execute();


  /**
   * Apply duration based effects
   * @author Hj. Malthaner
   */
  void apply_effects(int delay);


  /**
   * Implements thing listener
   * @author Hj. Malthaner
   */
  virtual void update(const handle_tpl<thing_t> &);


  /**
   * Implements thing visitor
   * @author Hj. Malthaner
   */
  virtual void visit_thing(const handle_tpl<thing_t> &);


  /**
   * Implements limb visitor
   * @author Hj. Malthaner
   */
  virtual void visit_limb(const handle_tpl<thing_t> &);


  // ---- implements persistent_t ----

  action_attack_t();

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();


};
#endif //ACTION_ATTACK_T_H
