/* Copyright by Hj. Malthaner */

#ifndef USER_INPUT_T_H
#define USER_INPUT_T_H

#ifndef ACTION_T_H
#include "action_t.h"
#endif

#ifndef koord_h
#include "primitives/koord.h"
#endif

class usage_panel_t;
class event_t;
class path_t;


template <class T> class minivec_tpl;

extern int load_file(const char * filename);
extern void save_file(const char * filename);


/**
 * Targetting. Called interactively.
 * @return target
 * @author Hj. Malthaner
 */
thinghandle_t cmd_target(thinghandle_t thing);


/**
 * Throw an item. Used ineractively.
 * @param thing the thrower
 * @param ammo_stack thing(s) to throw
 * @param multi damage multiplier
 * @author Hj. Malthaner
 */
extern int cmd_throw(thinghandle_t thing, thinghandle_t ammo_stack, int multi);


/**
 * This action processes user input.
 * @author Hj. Malthaner
 */
class user_input_t : public action_t {
 public:


  static koord ask_direction(thinghandle_t thing, int max_dist);


  /**
   * Get a thing from ground
   * @author Hansj�rg Malthaner
   */
  static int cmd_get_thing(thinghandle_t player, thinghandle_t thing);

  static int cmd_open_thing(thinghandle_t player, thinghandle_t container);

  static int cmd_inspect_thing(thinghandle_t player, thinghandle_t thing);

  static int cmd_trade(thinghandle_t player, thinghandle_t thing);

  static int cmd_talk(thinghandle_t & player, thinghandle_t & thing);


  /**
   * Wait for a keypress
   * @author Hj. Malthaner
   */
  static int wait_for_key();


  /**
   * Base path, i.e. for loading/saving games
   * @author Hj. Malthaner
   */
  static void set_path(const char *path);

private:
  thinghandle_t thing;


  usage_panel_t *panel;


  int frametime;

  /**
   * Lifetime = object lifetime
   * @author Hansj�rg Malthaner
   */
  path_t * path;


  /**
   * Player entered a movement command. Move PC and do everything
   * that movement can trigger
   * @author Hansj�rg Malthaner
   */
  int cmd_move_to(koord new_pos);


  /**
   * Player entered a movement command. Move PC and do everything
   * that movement can trigger
   * @author Hansj�rg Malthaner
   */
  int cmd_move(int code);



  /**
   * Player clicked a sqaure. Trigger actions for items on square.
   * @param self if true, player clicked his own square
   * @author Hansj�rg Malthaner
   */
  int cmd_click_square(koord pos, bool * done, bool self);


  bool handle_key_event(event_t & event, int & delay);
  bool handle_click_event(event_t & event, int & delay);
  static bool handle_move_event(event_t & event, int & delay);

 protected:

  thinghandle_t get_thing() const;

  bool handle_event(event_t & ev, int & delay);


 public:    


  /**
   * Get type of action performed by this object
   * @author Hansj�rg Malthaner
   */
  virtual enum atype get_type() const;


  void set_thing(thinghandle_t player);

  void set_frametime(int frametime);

  user_input_t(thinghandle_t thing);

  virtual ~user_input_t();


  void set_usage_panel(usage_panel_t *upan);


  /**
   * This method checks if the action is good to execute regarding 
   * the surrounding situation.
   * @author Hj. Malthaner
   */
  int check();


  /**
   * Processes user input.
   * @return delay time for next action
   * @author Hj. Malthaner
   */
  int execute();


  /**
   * Apply duration based effects
   * @author Hj. Malthaner
   */
  void apply_effects(int delay);


  // ---- implements persistent_t ----

  user_input_t();

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};
#endif //USER_INPUT_T_H
