/* 
 * action_imove_t.cpp
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "action_imove_t.h"

#include "model/path_t.h"
#include "model/thing_t.h"
#include "model/feature_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/world_t.h"
#include "util/debug_t.h"
#include "factories/hstore_t.h"
#include "persistence/storage_t.h"
#include "persistence/iofile_t.h"

/**
 * Get type of action performed by this object
 * @author Hansj�rg Malthaner
 */
enum action_t::atype action_imove_t::get_type() const
{
  return action_t::other;
}


action_imove_t::action_imove_t(thinghandle_t thing)
{
  this->thing = thing;

  path = new path_t();
}


action_imove_t::~action_imove_t()
{
  delete path;
  path = 0;
}


/**
 * Checks if it's good to intelligently move now.
 * @author Hj. Malthaner
 */
int action_imove_t::check()
{
  int result = -1;  // do not execute by default

  // must be on map to move!
  if(thing->get_pos() != koord(-1,-1)) {
    const char * ai_state = thing->get_string("AI.state", 0);

    if(ai_state && strcmp("search_room", ai_state) == 0) {
      result = 10;
      // result = 900;
    }

    if(ai_state && strcmp("follow_path", ai_state) == 0) {
      result = 10;
      // result = 900;
    }
  }

  return result;
}


/**
 * Imoves the thing. 
 * @return delay time for next action
 * @author Hj. Malthaner     
 */
int action_imove_t::execute()
{
  const char * ai_state = thing->get_string("AI.state", 0);

  int delay = 10;

  if(ai_state && strcmp("search_room", ai_state) == 0) {
    search_room();

  } else if(ai_state && strcmp("follow_path", ai_state) == 0) {

    delay = follow_path();
  }  

  return delay;
}



void action_imove_t::search_room()
{
  const int room_type = thing->get_int("AI.room_type", 0);
  level_t * level = world_t::get_instance()->get_level();

  
  dest_cond_t * dest = 0;
  
  if(room_type == 2) {
    // check if AI has a lair assigned

    const int x = thing->get_int("AI.lair.x", -1);
    const int y = thing->get_int("AI.lair.y", -1);

    if(x != -1 && y != -1) {
      dbg->message("action_imove_t::search_room()",
		   "Being wants to go to its lair at %d,%d", x, y);
      dest = new look_for_pos_t (level, koord(x,y));
    } else {
      dbg->message("action_imove_t::search_room()",
		   "Being is looking for lair (%d)", room_type);
      dest = new look_for_room_t (level, room_type);
    }

  } else if(room_type == 6) {
    // check if AI has a working place assigned

    const int x = thing->get_int("AI.work.x", -1);
    const int y = thing->get_int("AI.work.y", -1);

    if(x != -1 && y != -1) {
      dbg->message("action_imove_t::search_room()",
		   "Being wants to go to its workplace at %d,%d", x, y);
      dest = new look_for_pos_t (level, koord(x,y));
    } else {
      dbg->message("action_imove_t::search_room()",
		   "Being is looking for working room (%d)", room_type);
      dest = new look_for_room_t (level, room_type);
    }

  } else if(room_type == 3) {
    // check if AI has a washing place assigned

    const int x = thing->get_int("AI.wash.x", -1);
    const int y = thing->get_int("AI.wash.y", -1);

    if(x != -1 && y != -1) {
      dbg->message("action_imove_t::search_room()",
		   "Being wants to wash itself at %d,%d", x, y);
      dest = new look_for_pos_t (level, koord(x,y));
    } else {
      dbg->message("action_imove_t::search_room()",
		   "Being is looking for rest room (%d)", room_type);
      dest = new look_for_room_t (level, room_type);
    }

  } else {
    dbg->message("action_imove_t::search_room()",
		 "Being is looking for room of type %d", room_type);

    dest = new look_for_room_t (level, room_type);
  }


  const bool ok = path->calculate(level, thing, thing->get_pos(), dest);
    
  if(ok) {
    path->n = 0;

    thing->set("AI.state", "follow_path");
  } else {
    path->n = -1;
    // Hajo: No path - do something else
    thing->set("AI.state", "nothing");
  }

  dbg->message("action_imove_t::execute()",
	       "... found %d path (of length %d)", ok, path->get_size());  

  delete dest;
  dest = 0;
}


int action_imove_t::follow_path()
{
  if(path->n == -1) {
    dbg->message("action_imove_t::execute()", "no path! (n == -1)");
    
    return 10;
  } else {
      
    if(path->get_size() <= 1) {
      // we are already there!
      thing->set("AI.state", "destination_reached");
      
      // dbg->message("action_imove_t::execute()",
      //	      "Being already was at destination");

    } else {
      level_t * level = world_t::get_instance()->get_level();
      const koord new_pos = path->get_step(path->n+1);
      square_t * square = level->at(new_pos);
      
      if(square) {

	if(square->can_enter(thing)) {
	  const koord old_pos = thing->get_pos();
	  level->at(old_pos)->remove_thing(thing);
	  square->add_thing(thing, new_pos);
	  
	  // dbg->message("action_imove_t::execute()",
	  //	       "Being steps to %d -> %d,%d", path->n, new_pos.x, new_pos.y);
	  
	  // Hajo: close doors if possible
	  if(level->at(old_pos)->get_feature().is_bound()) {
	    level->at(old_pos)->get_feature()->activate(thing);
	  }

	  
	  path->n ++;
	  if(path->n == (int)(path->get_size()-1)) {
	    // What do we do if we are there?
	    thing->set("AI.state", "destination_reached");
	    
	    // dbg->message("action_imove_t::execute()",
	    //		 "Being reached destination %d,%d", new_pos.x, new_pos.y);

	  }
	  
	} else {
	  dbg->message("action_imove_t::execute()",
		       "Path blocked at %d -> %d,%d", path->n, new_pos.x, new_pos.y);
	  
	  if(square->get_feature().is_bound()) {
	    dbg->message("action_imove_t::execute()",
			 "Being activates feature at %d,%d", 
			 path->n, new_pos.x, new_pos.y);
	    
	    square->get_feature()->activate(thing);
	  } else {
	    // something blocks us -> search new path!
	    // thing->set("AI.state", "search_room");

	    // try something else
	    thing->set("AI.state", "destination_reached");
	  }
	}
      } else {
	dbg->warning("action_imove_t::execute()",
		     "Path off map at %d (%d) -> %d,%d", 
		     path->n, path->get_size(), new_pos.x, new_pos.y);
      }
    }
  }


  // default is ten time quants for a move
  const int delay = thing->calc_speed(10);

  return delay;
}


/**
 * Apply duration based effects
 * @author Hj. Malthaner
 */
void action_imove_t::apply_effects(int delay)
{
  action_t::apply_effects(thing, delay);  
}


action_imove_t::action_imove_t()
{
  path = new path_t();
}


/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void action_imove_t::read_write(storage_t *store, iofile_t * file)
{
  path->read_write(store, file);

  if(file->is_saving()) {
    store->store(thing.get_rep(), file);
  } else {
    thing = thinghandle_t((thing_t *)store->restore(file, hstore_t::t_thing));
  }
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t action_imove_t::get_cid()
{
  return hstore_t::t_action_imove;
}
