/* 
 * thing_usage_selector_t.h
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef thing_usage_selector_t_h
#define thing_usage_selector_t_h

class thing_t;
class inventory_t;
template <class T> class handle_tpl;

class thing_usage_selector_t
{

 public:
  thing_usage_selector_t();


  /**
   * Select an usage and activate it
   * @param inv the inventory if the things is in an inventory, null otherwise
   * @return the delay of the action
   * @author Hj. Malthaner
   */
  int select_and_activate(const handle_tpl<thing_t> & user, 
			  inventory_t * inv,
			  const handle_tpl<thing_t> & thing);

};

#endif // thing_usage_selector_t_h
