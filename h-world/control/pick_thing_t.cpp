/* 
 * pick_thing_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "util/debug_t.h"

#include "model/inventory_t.h"
#include "model/world_t.h"
#include "model/level_t.h"
#include "model/square_t.h"
#include "model/thing_t.h"
#include "model/thinghandle_t.h"
#include "model/thing_tree_t.h"

#include "language/linguist_t.h"

#include "pick_thing_t.h"

#include "factories/hstore_t.h"


pick_thing_t::pick_thing_t()
{
  policy_t &policy = get_policy();

  policy.target = policy_t::location;
  policy.power  = 0;
  policy.range  = 0;
  policy.effect = policy_t::pick_thing;


  set_name("Pickup");
}


int pick_thing_t::pick_up(const handle_tpl <thing_t> &user,
			  const handle_tpl <thing_t> &thing)
{
  if(thing != user) {
    char ident[256];
    thing_tree_t tree;
    slist_tpl <thinghandle_t> list;
    
    // fixed to ground ?
    if(*thing->get_string("pickup", "true") == 'f') {
      char buf[256];
      thing->get_ident(ident);
      sprintf(buf, "The %s appears to be fixed on the ground!", ident);
      linguist_t::translate(buf);
      return 0;
    }
      
    // false means not to list inventory contents
    tree.list_inventories(user, &list, false);
    
    printf("  found %d inventories\n", list.count());
    
    if(list.count() > 0) {
      inventory_t *inv = list.at(0)->get_inventory();
      const bool ok = inv->add(thing);
      
      if(ok) {
	level_t *level = world_t::get_instance()->get_level();
	square_t *square = level->at(thing->get_pos());
	if(square) {
	  square->remove_thing(thing);
	  thing->get_ident(ident);
	
	  linguist_t::pvo("You", "have", false, ident); 
	}
      } else {
	linguist_t::translate("Your pack is full.");
	
      }
    } else {
      // no bag or pack
      // check if some limb can hold this
      thinghandle_t limb = thing_t::find_limb_to_hold(user, thing); 
      
      if(limb.is_bound()) {
	limb->set_item(thing, true);

	level_t *level = world_t::get_instance()->get_level();
	square_t *square = level->at(thing->get_pos());
	if(square) {
	  square->remove_thing(thing);
	}

	thing->get_ident(ident);
	
	linguist_t::pvo("You", "have", false, ident);
      }
      
    }
  } else {
    linguist_t::translate("There is nothing to pick up.");
  }  

  return 0;
}


/**
 * Picks up first thing from location
 * @param user the thing which activates this usage (for reflexive usages)
 * @return stack count reduction of the used thing, if the stack count drops
 *         to 0 the thing is supposed to be removed
 * @author Hansj�rg Malthaner
 */
int pick_thing_t::activate(const handle_tpl <thing_t> &user,
			    koord pos)
{
  dbg->message("pick_thing_t::activate()", 
	       "%s is picking from %d,%d", 
	       user->get_string("ident", "unnamed"), pos.x, pos.y);


  level_t *level = world_t::get_instance()->get_level();
  square_t *square = level->at(pos);

  const minivec_tpl <thinghandle_t> & sqlist = square->get_things();
    
  if(sqlist.count() > 0) {
    thinghandle_t thing = sqlist.get(0);

    if(thing == user && sqlist.count() > 1) {
      thing = sqlist.get(1);
    }

    pick_up(user, thing);
  } else {
    linguist_t::translate("There square is empty.");
  }

  return 0;
}


/**
 * Not implmented, pick up can only be applied to a location!
 * @param user the thing which activates this usage (for reflexive usages)
 * @return stack count reduction of the used thing, if the stack count drops
 *         to 0 the thing is supposed to be removed
 * @author Hansj�rg Malthaner
 */
int pick_thing_t::activate(const handle_tpl <thing_t> &user,
			   inventory_t * ,
			   const handle_tpl <thing_t> &t)
{
  // Hack: get users location an pick from there

  return activate(user, user->get_pos());
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t pick_thing_t::get_cid()
{
  return hstore_t::t_pick_thing;
}

