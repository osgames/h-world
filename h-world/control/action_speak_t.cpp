/* 
 * action_speak_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "action_speak_t.h"

#include "model/world_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/thing_t.h"
#include "util/debug_t.h"
#include "util/string_helper.h"

#include "view/world_view_t.h"
#include "gui/dialog_frame_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"

#include "swt/window_manager_t.h"

/**
 * Get type of action performed by this object
 * @author Hansj�rg Malthaner
 */
enum action_t::atype action_speak_t::get_type() const
{
  return action_t::speak;
}



action_speak_t::action_speak_t() {
  // ---- implements persistent_t ----

  filename = 0;
  window = 0;
  radius = 1;
}


action_speak_t::action_speak_t(thinghandle_t thing, 
			       const char *filename)
{
  this->thing = thing;
  this->filename = str_duplicate(filename);
  window = 0;
  radius = 1;
}


action_speak_t::~action_speak_t() 
{
  if(window) {
    window->close();
    delete window;
    window = 0;
  }

  if(filename) {
    delete [] filename;
    filename = 0;
  }
}


/**
 * Checks if it's good to speak now.
 * @author Hj. Malthaner
 */
int action_speak_t::check()
{
  const level_t * level = world_t::get_instance()->get_level();
  const int radius = thing->get_int("dialog.auto.radius", 1);

  int val = -1;


  for(int j=-radius; j<=radius; j++) {
    for(int i=-radius; i<=radius; i++) {

      if(i != 0 || j != 0) {
	const koord offset (i,j);
	const koord k = thing->get_pos() + offset;
      
	if(level->at(k)) {
	  minivec_iterator_tpl <thinghandle_t> iter (level->at(k)->get_things());
	
	  while(iter.next()) {
	    // printf("'%s'\n", iter.get_current()->get_string("name", "XXX"));
	    
	    if(iter.get_current()->get_string("is_player", 0)) {
	    
	      customer = iter.get_current(); 

	      val = 600;
	      goto done;
	    }
	  }
	}
      }
    }
  }

  done:
  return val;
}


/**
 * Attacks. 
 * @author Hj. Malthaner
 */
int action_speak_t::execute()
{
  int delay = 10;

  dbg->message("action_speak_t::execute()", "Called.");


  // Hajo: redraw map, player has moved
  world_view_t::get_instance()->redraw();


  if(window != 0) {
    dbg->message("action_speak_t::execute()", "Removing old window.");
    window_manager_t::get_instance()->remove_window(window);
    delete window;
    window = 0;
  }

  window = new dialog_frame_t(filename, false, thing, customer);

  window->set_pos(koord(160,60));

  window->set_visible(true);

  window_manager_t::get_instance()->add_window(window);


  return delay;
}


/**
 * Apply duration based effects
 * @author Hj. Malthaner
 */
void action_speak_t::apply_effects(int delay)
{
  action_t::apply_effects(thing, delay);
}



// ---- implements persistent_t ----

/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void action_speak_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_int(radius);
  file->rdwr_string(filename);

  if(file->is_saving()) {
    store->store(thing.get_rep(), file);
  } else {
    thing = thinghandle_t((thing_t *)store->restore(file, hstore_t::t_thing));
  }
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t action_speak_t::get_cid()
{
  return hstore_t::t_action_speak;
}

