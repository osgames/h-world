/* 
 * commands.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "commands.h"

#include "usage_t.h"
#include "model/world_t.h"
#include "model/thing_t.h"
#include "model/inventory_t.h"
#include "util/debug_t.h"


/**
 * Uses an item.
 * @return the delay of the action
 * @author Hj. Malthaner
 */
int interactive_usage(thinghandle_t user,
		      inventory_t * inv,
		      thinghandle_t item,
		      usage_t * usage)
{
  char buf[256];

  dbg->message("interactive_usage()", "usage %s %s", 
	       usage->get_name(), usage->get_verb());

  if(user.is_bound()) {
    user->get_ident(buf);
    dbg->message("interactive_usage()", "user %s", buf);
  } else {
    dbg->message("interactive_usage()", "no user given, assuming player");
    user = world_t::get_instance()->get_player();
  }

  item->get_ident(buf);
  dbg->message("interactive_usage()", "item %s", buf);


  const int delay = usage->activate(user, inv, item);
  
  // Hajo: check reduced stack count
  int stack = item->get_int("cur_stack", 1);
  if(stack == 0) {

    // Hajo: if the thing is stored in an inventory,
    // we need to remove it, otherwise we need to check
    // the owner
    if(inv) {
      inv->remove(item);
    } else {
      thing_t::replace_thing(item->get_owner(), item, 0);
    }

    // dbg->warning("user_input_t::execute()", "refcount is %d", thing.get_count());
  }

  return delay;
}
