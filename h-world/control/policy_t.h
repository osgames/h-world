#ifndef POLICY_T_H
#define POLICY_T_H


/**
 * Usages must explain their effects to the AI. This objects contain
 * all the information the AI needs to know about usages.
 * @author Hj. Malthaner
 */
class policy_t
{
public:

  /**
   * List of possible targets
   * @author Hj. Malthaner
   */
  enum target_t {self, thing, location};


  /**
   * List of possible effects, this is a bitfield!
   * @author Hj. Malthaner
   */
  enum effect_t {none           = 0x0000,
		 // damage_pierce  = 0x0001, 
		 damage_cut     = 0x0002, 
                 damage_blunt   = 0x0004,
		 // damage_scratch = 0x0008,
                 magic_heal     = 0x0010,
		 magic_light    = 0x0020,
                 pick_thing     = 0x0040
                };

   
  /**
   * apply to what?
   * @author Hj. Malthaner
   */
  enum target_t target;


  /**
   * Maximum distance, melee attacks have range 1. 0 is the own squre,
   * 1 are neighbour squares. Kings distance.
   * @author Hj. Malthaner
   */
  int           range;    
  
  /**
   * what does it do? This is a bitfield and can hold more than one effect!
   * @author Hj. Malthaner
   */
  int           effect;   

  /**
   * strength of effect, attacks must be considered separately
   * @author Hj. Malthaner
   */
  int           power;


  policy_t () {
    target = self;
    range = 0;
    effect = 0;
    power = 0;
  }
};

#endif
