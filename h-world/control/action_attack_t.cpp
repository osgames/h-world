/* 
 * action_attack_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "action_attack_t.h"
#include "attack_t.h"
#include "model/thing_traversor_t.h"
#include "model/world_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/thing_t.h"
#include "util/debug_t.h"
#include "util/rng_t.h"
#include "language/linguist_t.h"

#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"


/**
 * Get type of action performed by this object
 * @author Hansj�rg Malthaner
 */
enum action_t::atype action_attack_t::get_type() const
{
  return action_t::attack;
}


action_attack_t::action_attack_t() {
  // ---- implements persistent_t ----
};


action_attack_t::action_attack_t(thinghandle_t thing)
{
  chosen_attack = 0;

  thing->add_listener(this);
  update(thing);

  this->thing = thing;
}


action_attack_t::~action_attack_t() 
{
  chosen_attack = 0;

  thing->remove_listener(this);
}


static bool is_my_enemy(const char * const is_friend, 
			const thinghandle_t & current)
{
  // Hajo: be peaceful by default
  bool enemy = false;

  if(*is_friend == 'f') {
    if(current->get_string("alive", 0) &&
       *current->get_string("status", "enemy") == 'e' ) {
      enemy = true;
    }
    
  } else {
    if(current->get_string("alive", 0) &&
       *current->get_string("status", "enemy") == 'f' ) {
      enemy = true;
    }
  }

  return enemy;
}


int action_attack_t::attack_square(square_t * square,
				   const char * const is_friend)
{
  int val = -1;

  if(square) {

    minivec_iterator_tpl <thinghandle_t> iter (square->get_things());
    
    while(iter.next()) {
      // printf("'%s'\n", iter.get_current()->get_string("name", "XXX"));
      
      const thinghandle_t & current = iter.get_current();

      if(is_my_enemy(is_friend, current)) {
	attackee = current; 
	chosen_attack = attacks.at(rng_t::get_the_rng().get_int(attacks.count()));
	val = 500;
	    
	/*
	  dbg->message("action_attack_t::check()", 
	  "%s chooses to attack %s", 
	  thing->get_string("ident", "XXX"), 
	  attackee->get_string("ident", "XXX"));
	*/
	break;
      }
    }
  }

  return val;
}


/**
 * Checks if it's good to attack now.
 * @author Hj. Malthaner
 */
int action_attack_t::check()
{
  static const koord direction [8] = {
    koord( 1, 0),
    koord( 1, 1),
    koord( 0, 1),
    koord(-1, 1),
    koord(-1, 0),
    koord(-1,-1),
    koord( 0,-1),
    koord( 1,-1),
  };

  const char * is_friend = thing->get_string("status", "enemy");


  if(thing->get_int("effect.paralyzed", 0) > 0 ||
     thing->get_int("sleep", -1) > 0 ||
     *is_friend == 'n') {
    return -1;
  }


  level_t * level = world_t::get_instance()->get_level();

  int val = -1;
  chosen_attack = 0;

  if(attacks.count()) {

    // Hajo: check melee attacks
    for(int i=0; i<8; i++) {
      const koord k = thing->get_pos() + direction[i];
      square_t * square = level->at(k);
      val = attack_square(square, is_friend);

      if(val != -1) {
	break;
      }
    }

    // Check ranged attacks 
    if(val == -1) {
      int max_range = 0;

      slist_tpl <attack_t *> ranged_attacks;
      slist_iterator_tpl <attack_t *> iter (attacks);

      while( iter.next() ) {
	attack_t *att = iter.get_current();
	if(att->get_policy().range > 1) {
	  ranged_attacks.append(att);

	  if(att->get_policy().range > max_range) {
	    max_range = att->get_policy().range;
	  }
	}
      }
      
      if(ranged_attacks.count() > 0 &&
	 level->is_viewable(thing->get_pos())) {
	// we got ranged attacks

	// scan area

	for(int j=-max_range; j<=max_range && val == -1; j++) { 
	  for(int i=-max_range; i<=max_range && val == -1; i++) { 
	    const koord k = thing->get_pos() + koord(i,j);
	    square_t * square = level->at(k);
	    val = attack_square(square, is_friend);
	  }
	}
	
	if(val != -1) {
	  // ok, got something to attack;
	  // XXX todo -> choose proper attack
	  
	  chosen_attack = 
	    ranged_attacks.at(0);	  
	}
      }
    }

  } // if(attacks.count()) 



  return val;
}


/**
 * Attacks. 
 * @author Hj. Malthaner
 */
int action_attack_t::execute()
{
  int delay = 1;

  if(chosen_attack) {

    chosen_attack->activate(thing, 0, attackee);

    delay = chosen_attack->get_delay();
  } else {
    dbg->warning("action_attack_t::execute()", 
		 "Called but no attack was chosen!");
  }

  delay = thing->calc_speed(delay);

  return delay;
}


/**
 * Apply duration based effects
 * @author Hj. Malthaner
 */
void action_attack_t::apply_effects(int delay)
{
  action_t::apply_effects(thing, delay);
}


/**
 * Implements thing listener
 * @author Hj. Malthaner
 */
void action_attack_t::update(const handle_tpl<thing_t> &thing)
{
  thing_traversor_t trav;
  trav.set_model(thing);
  trav.set_limb_visitor(this);
  trav.set_thing_visitor(this);

  attacks.clear();

  // inspect thing
  trav.traverse();
}


/**
 * Implements thing visitor
 * @author Hj. Malthaner
 */
void action_attack_t::visit_thing(const handle_tpl<thing_t> &thing)
{
  // dbg->message("action_attack_t::visit_thing()", "called");

  slist_iterator_tpl <usage_t *> iter (thing->get_usages());

  while(iter.next()) {
    attack_t *atk = dynamic_cast <attack_t *> (iter.get_current());

    if(atk) {
      attacks.insert(atk);

      // dbg->message("action_attack_t::visit()", "Attack %s found" , atk->get_name());
    }

  }
  
}


/**
 * Implements limb visitor
 * @author Hj. Malthaner
 */
void action_attack_t::visit_limb(const handle_tpl<thing_t> &limb)
{
  // dbg->message("action_attack_t::visit_limb()", "called");

  const handle_tpl <thing_t> & thing = limb->get_item();

  // Only use attack is limb does not hold something

  if(!thing.is_bound() || 
     (limb->get_string("usages_always", 0) && 
      *thing->get_string("alive", "f") != 't')) {

    slist_iterator_tpl <usage_t *> iter (limb->get_usages());

    while(iter.next()) {
      attack_t *atk = dynamic_cast <attack_t *> (iter.get_current());
      
      if(atk) {
	attacks.insert(atk);

	// dbg->message("action_attack_t::visit_limb()", "Attack %s found" , atk->get_name());
      }
    }
  }
}

// ---- implements persistent_t ----

/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void action_attack_t::read_write(storage_t *store, iofile_t * file)
{
  /* XXX

  slist_tpl <attack_t*> attacks;

  */


  if(file->is_saving()) {
    store->store(thing.get_rep(), file);
  } else {
    thing = thinghandle_t((thing_t *)store->restore(file, hstore_t::t_thing));

    thing->add_listener(this);
    update(thing);
  }


  // transient
  // attack_t *chosen_attack;
  // handle_tpl <thing_t> attackee;

}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t action_attack_t::get_cid()
{
  return hstore_t::t_action_attack;
}
