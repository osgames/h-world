/* 
 * actor_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef ACTOR_T_H
#define ACTOR_T_H

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

class action_t;
class thing_t;
class skills_t;

template <class T> class slist_tpl;

class actor_t : public persistent_t {
private:    

    /**
     * @supplierCardinality 0..* 
     * @author Hj. Malthaner
     */
    slist_tpl <action_t *> * actions;

    /**
     * Time of next turn to be executed
     * @author Hj. Malthaner
     */
    int next_time;

    /**
     * See description of add_delay() int this class
     * @author Hj. Malthaner
     */
    int addi_delay;
    
public:

    skills_t * skills;


    const int & get_next_time() const {return next_time;};

    /**
     * Adds some time to the timestamp
     * @author Hj. Malthaner
     */
    void add_time(int delay);


    /**
     * Some events of the world may add additional delays to the currently
     * executed action - i.e. if the player is using items from the event
     * loop then there is no regular way to add the delays to the executed
     * action - but the current actor is know. Therefore the delays are
     * added to the current actor. Actually this is kind of a hack because
     * using items from the event loop was not planned in first stage but
     * added later on, and it neds this bridge to access the data.
     *
     * @author Hj. Malthaner
     */
    void add_delay(int delay);


    actor_t();
    ~actor_t();


    /**
     * Does this actor represent a animated thing? (charging does not count!)
     * @author Hj. Malthaner
     */
    bool is_animated();


    /**
     * executes the actions
     * @author Hj. Malthaner
     */
    int execute();


    /**
     * Adds an action.
     * @author Hj. Malthaner
     */
    void add_action(action_t *action);


    /**
     * Removes an action.
     * @author Hj. Malthaner
     */
    void remove_action(action_t *action);


    /**
     * Deletes all actions but the given action. Useful to preserve the
     * player input action during world destruction..
     * @author Hj. Malthaner
     */
    void delete_all_actions_but(action_t *action);


    /**
     * Finds an action for the given action type
     * @return the action or NULL if no matching action was found
     * @author Hj. Malthaner
     */
    action_t * find_action_for(int atype);


    bool operator< (const actor_t & a) const {
      return next_time < a.next_time;
    }


    // ---- implements persistent_t ----

    /**
     * Stores/restores the object. Must be overridden by subclass.
     * This method is supposed to call store->store() on all associated
     * objects!
     * @author Hj. Malthaner
     */
    virtual void read_write(storage_t *store, iofile_t * file);


    /**
     * Gets a unique ID for this objects class.
     * @author Hj. Malthaner
     */
    virtual perid_t get_cid();


};

#endif //ACTOR_T_H
