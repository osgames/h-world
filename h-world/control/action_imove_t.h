/* 
 * action_imove_t.h
 *
 * Copyright (c) 2002 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef ACTION_IMOVE_T_H
#define ACTION_IMOVE_T_H


#include "action_t.h"
#include "primitives/koord.h"
#include "model/thinghandle_t.h"
#include "tpl/handle_tpl.h"

class path_t;

/**
 * Intelligent moving -> look for room (or other destination)
 * calculate a path there, and go there.
 *
 * @author Hansj�rg Malthaner
 */
class action_imove_t : public action_t {
private:
  thinghandle_t thing;

  /**
   * Lifetime = object lifetime
   * @author Hansj�rg Malthaner
   */
  path_t * path;


  void search_room();
  int follow_path();

public:


  /**
   * Get type of action performed by this object
   * @author Hansj�rg Malthaner
   */
  virtual enum atype get_type() const;


  action_imove_t(thinghandle_t thing);

  virtual ~action_imove_t();


  /**
   * Checks if it's good to intelligently move now.
   * @author Hj. Malthaner
   */
  int check();


  /**
   * Imoves the thing. 
   * @return delay time for next action
   * @author Hj. Malthaner     
   */
  int execute();


  /**
   * Apply duration based effects
   * @author Hj. Malthaner
   */
  void apply_effects(int delay);


  // ---- implements persistent_t ----

  action_imove_t();


  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};
#endif //ACTION_IMOVE_T_H
