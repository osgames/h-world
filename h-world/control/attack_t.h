/* Copyright by Hj. Malthaner */

#ifndef ATTACK_T_H
#define ATTACK_T_H

#ifndef USAGE_T_H
#include "usage_t.h"
#endif

#ifndef dice_t_h
#include "model/dice_t.h"
#endif

#ifndef tpl_handle_tpl_h
#include "tpl/handle_tpl.h"
#endif

#ifndef LIMB_VISITOR_T_H
#include "ifc/limb_visitor_t.h"
#endif


class thing_t;


class attack_t : public usage_t, limb_visitor_t {
private:

  /**
   * blunt damage amount
   * @author Hj. Malthaner
   */
  dice_t  blunt;

  /**
   * cut damage amount
   * @author Hj. Malthaner
   */
  dice_t cut;


  /**
   * piercing ability (percent)
   * @author Hj. Malthaner
   */
  unsigned char pierce;


  /**
   * Time delay to do this attack
   * @author Hansj�rg Malthaner
   */
  unsigned char delay;


  /**
   * The item that grants this attack
   * @author Hansj�rg Malthaner
   */
  handle_tpl < thing_t > item;


  /**
   * For determining hit location of the target. In fact this
   * is a hidden parameter for the visit_limb() method.
   *   
   * @author Hansj�rg Malthaner
   */
  unsigned char summarize;

  
  /**
   * Random number to determine the hit limb
   *   
   * @author Hansj�rg Malthaner
   */
  int hit_location;

  int sum;
  int limbs;

  /**
   * The limb that was hit
   *   
   * @author Hansj�rg Malthaner
   */
  handle_tpl <thing_t> hit_limb;


public:    

  const dice_t & get_blunt() const {return blunt;};
  const dice_t & get_cut() const {return cut;};
  unsigned char get_pierce() const {return pierce;};

  void set_blunt(dice_t b) {blunt = b;};
  void set_cut(dice_t b) {cut = b;};
  void set_pierce(unsigned char b) {pierce = b;};


  /**
   * Get time delay to do this attack
   * @author Hansj�rg Malthaner
   */
  int get_delay() const {return delay;};


  /**
   * Set time delay to do this attack
   * @author Hansj�rg Malthaner
   */
  void set_delay(unsigned char d) {delay = d;};


  /**
   * Construct an attack with a name
   * @param name name of the attack
   * @param item the thing that grants this attack
   * @author Hj. Malthaner
   */
  attack_t(const char * name, handle_tpl <thing_t> item);


  virtual ~attack_t();


  /**
   * Executes the attack
   * @param user the thing which activates this usage (for reflexive usages)
   * @return stack count reduction of the used thing, if the stack count drops
   *         to 0 the thing is supposed to be removed
   * @author Hansj�rg Malthaner
   */
  int activate(const handle_tpl <thing_t> &user,koord pos) { 
    /* cannot attack a location */
    return 0;
  };


  /**
   * Executes the attack
   * @param user the thing which activates this usage (for reflexive usages)
   * @return stack count reduction of the used thing, if the stack count drops
   *         to 0 the thing is supposed to be removed
   * @author Hansj�rg Malthaner
   */
  int activate(const handle_tpl <thing_t> &user,
	       inventory_t * inv,
	       const handle_tpl <thing_t> &target);


  /**
   * Implements limb visitor_t
   * @author Hansj�rg Malthaner
   */
  virtual void visit_limb(const handle_tpl <thing_t> &);


  // ---- implements persistent_t ----

  attack_t();

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};
#endif //ATTACK_T_H
