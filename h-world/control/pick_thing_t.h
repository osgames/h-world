/* 
 * pick_thing_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef PICK_THING_T_H
#define PICK_THING_T_H

#include "usage_t.h"

class thing_t;
template <class T> class handle_tpl;

/**
 * Usage to pick up things
 * @author Hansj�rg Malthaner
 */
class pick_thing_t : public usage_t {
 private:



 public:

  /**
   * Get time delay to pick up something
   * @author Hansj�rg Malthaner
   */
  int get_delay() const {return 2;};

  
  pick_thing_t();


  int pick_up(const handle_tpl <thing_t> &user,
	      const handle_tpl <thing_t> &item);


  /**
   * Picks up first thing from location
   * @param user the thing which activates this usage (for reflexive usages)
   * @return stack count reduction of the used thing, if the stack count drops
   *         to 0 the thing is supposed to be removed
   * @author Hansj�rg Malthaner
   */
  virtual int activate(const handle_tpl <thing_t> &user,
		       koord pos);


  /**
   * Not implmented, pick up can only be applied to a location!
   * @param user the thing which activates this usage (for reflexive usages)
   * @return stack count reduction of the used thing, if the stack count drops
   *         to 0 the thing is supposed to be removed
   * @author Hansj�rg Malthaner
   */
  virtual int activate(const handle_tpl <thing_t> &user,
		       inventory_t * inv,
		       const handle_tpl <thing_t> &t);


  // ---- implements persistent_t ----


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();


};


#endif

