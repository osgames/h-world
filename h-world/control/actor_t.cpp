/* 
 * actor_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "actor_t.h"
#include "action_t.h"
#include "scheduler_t.h"
#include "util/debug_t.h"

#include "skills_t.h"
#include "tpl/slist_tpl.h"


#include "persistence/iofile_t.h"
#include "factories/hstore_t.h"


/**
 * Adds some time to the timestamp
 * @author Hj. Malthaner
 */
void actor_t::add_time(int delay) 
{
  next_time += delay;
}


/**
 * Some events of the world may add additional delays to the currently
 * executed action - i.e. if the player is using items from the event
 * loop then there is no regular way to add the delays to the executed
 * action - but the current actor is know. Therefore the delays are
 * added to the current actor. Actually this is kind of a hack because
 * using items from the event loop was not planned in first stage but
 * added later on, and it neds this bridge to access the data.
 *
 * @author Hj. Malthaner
 */
void actor_t::add_delay(int delay)
{
  addi_delay += delay;
}



actor_t::actor_t()
{
  next_time = scheduler_t::get_instance()->get_time();

  skills = new skills_t();
  actions = new slist_tpl <action_t *> (); 
}


actor_t::~actor_t()
{
  scheduler_t::get_instance()->remove_actor(this);

  delete skills;
  skills = 0;

  delete actions;
  actions = 0;
}


/**
 * Does this actor represent a animated thing? (charging does not count!)
 * @author Hj. Malthaner
 */
bool actor_t::is_animated() {

  // XXX should check for something else but just the number of actions!
  
  return actions->count() > 0;
}


/**
 * executes the actions
 * @author Hj. Malthaner
 */
int actor_t::execute()
{
  slist_iterator_tpl <action_t *> iter (actions);
  int max = -1;
  action_t * best = NULL;
  int delay = 10;

  // Hajo: This can be increased from the outside via add_delay(int)
  addi_delay = 0;
  

  //   dbg->message("actor_t::execute()", "Actor %p is processing %d actions", 
  //	       this,
  //	       actions->count());


  while(iter.next()) {
    action_t *action = iter.get_current();

    const int value = action->check();

    //    dbg->message("actor_t::execute()", "Action %p yields value %d", 
    //		 action, value);

    if(value > max) {
      max = value;
      best = action;
    }
  }

  //  dbg->message("actor_t::execute()", "Best action is %p", best);

  if(best) {
    delay = best->execute();

    // Apply duration based effects
    best->apply_effects(delay);
  }


  // Debugging
  if(addi_delay > 0) {
    dbg->message("actor_t::execute()", "Additional delay is %d", addi_delay);
  }

  // Hajo: return total delay
  return delay + addi_delay;
}


/**
 * Adds an action.
 * @author Hj. Malthaner
 */
void actor_t::add_action(action_t *action)
{
  if(actions->is_empty()) {
    scheduler_t::get_instance()->add_actor(this);
  }

  // Hajo: allow actions only once!
  if(!actions->contains(action)) {
    action->set_actor(this);
    actions->insert(action);  
  }

  /*
  dbg->message("actor_t::add_action()", 
	       "Adding action %p (actor %p), count is now %d", 
	       action,
	       this,
	       actions->count());
  */
};


/**
 * Removes an action.
 * @author Hj. Malthaner
 */
void actor_t::remove_action(action_t *action)
{
  actions->remove(action);
  action->set_actor(0);

  if(actions->is_empty()) {
    scheduler_t::get_instance()->remove_actor(this);
  }

  dbg->message("actor_t::remove_action()", 
	       "Removing action %p, count is now %d", 
	       action,
	       actions->count());
}


/**
 * Deletes all actions but the given action. Useful to preserve the
 * player input action during world destruction..
 * @author Hj. Malthaner
 */
void actor_t::delete_all_actions_but(action_t *action) 
{
  bool has_it = false;

  if(action) {
    action->set_actor(0);
  }

  slist_iterator_tpl <action_t *> iter (actions);
  while(iter.next()) {
    action_t *a = iter.get_current();

    if(a != action) {
      // dbg->message("actor_t::delete_all_actions_but()", "deleting %p", a); 
      delete a;
    } else {
      has_it = true;
    }
  }

  // remove all actions
  actions->clear();

  // if we had the special action, insert it again
  if(action && has_it) {
    action->set_actor(this);
    actions->insert(action);
  }
}


/**
 * Finds an action for the given action type
 * @return the action or NULL if no matching action was found
 * @author Hj. Malthaner
 */
action_t * actor_t::find_action_for(int atype)
{
  action_t *result = 0;
  slist_iterator_tpl <action_t *> iter (actions);

  while(iter.next()) {
    if(iter.get_current()->get_type() == atype) {
      result = iter.get_current();
      break;
    }
  }  

  dbg->message("actor_t::find_action_for()", 
	       "Searching atype %d, found action %p", 
	       atype,
	       result);

  return result;
}


// ---- implements persistent_t ----

/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void actor_t::read_write(storage_t *store, iofile_t * file)
{
  file->rdwr_int(next_time);

  int n = actions->count();

  file->rdwr_int(n);

  if(file->is_saving()) {

    slist_iterator_tpl <action_t *> iter (actions);

    while(iter.next()) {
      store->store(iter.get_current(), file);
    }

  } else {

    actions->clear();

    for(int i=0; i<n; i++) { 

      // There seem to be buggy savegames, that have actions
      // more than once, which subsequently crashes
      // delete_all_actions_but() because of a double delete

      action_t * action = (action_t *)store->restore(file);
      if(!actions->contains(action)) {
	actions->insert(action);
      }
    }


    // loading does not add actor to the scheduler automatically
    // so this is done here
    if(n) {
      scheduler_t::get_instance()->add_actor(this);

      // Hajo: avoid repeated actions if a saved game with an old
      // time stamp is loaded.
      if(next_time < scheduler_t::get_instance()->get_time()) {
	next_time = scheduler_t::get_instance()->get_time();
      }
    }
  }

  skills->read_write(store, file);
}


/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t actor_t::get_cid()
{
  return hstore_t::t_actor;
}

