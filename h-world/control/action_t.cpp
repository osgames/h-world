#include "action_t.h"
#include "lua_call_t.h"
#include "model/thing_t.h"

static lua_call_t call("per_turn_effects");


/**
 * Apply duration based effects
 * @author Hj. Malthaner
 */
void action_t::apply_effects(thinghandle_t thing, int duration)
{
  call.activate(thing, duration);
}


void action_t::set_actor(actor_t *a) 
{
  actor = a;
}


action_t::~action_t()
{
}
