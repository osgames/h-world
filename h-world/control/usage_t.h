/* Copyright by Hj. Malthaner */

#ifndef USAGE_T_H
#define USAGE_T_H

#include "persistence/persistent_t.h"

#include "primitives/koord.h"
#include "policy_t.h"

class thing_t;
class inventory_t;

template <class T> class handle_tpl;

class usage_t : public persistent_t {
private:

  /**
   * Usages usually have a name to display to the user
   * @author Hansj�rg Malthaner
   */
  char name[32];


  /**
   * The verb to output if the usage is executed
   * @author Hansj�rg Malthaner
   */
  char verb[32];


  /**
   * The AI doesn't understand names, so usages must have
   * a name like identifer for the AI, too
   * @author Hansj�rg Malthaner
   */
  policy_t policy;


protected:

  void set_name(const char *name);
  void set_policy(policy_t t) {policy = t;};

public:    

  const char * get_name() const {return name;};

  void set_verb(const char *verb);
  const char * get_verb() const {return verb;};



  /**
   * Time delay to do this usage
   * @author Hansj�rg Malthaner
   */
  virtual int get_delay() const = 0;


  /**
   * Policy accessor, grants read/write access!
   * @author Hansj�rg Malthaner
   */
  policy_t & get_policy() {return policy;};


  usage_t();

  /**
   * Frees aggregated members, like the name
   * @author Hansj�rg Malthaner
   */
  virtual ~usage_t();


  /**
   * Executes the usage
   * @param user the thing which activates this usage (for reflexive usages)
   * @return stack count reduction of the used thing, if the stack count drops
   *         to 0 the thing is supposed to be removed
   * @author Hansj�rg Malthaner
   */
  virtual int activate(const handle_tpl <thing_t> &user,
		       koord pos) = 0;


  /**
   * Executes the usage
   * @param user the thing which activates this usage (for reflexive usages)
   * @return stack count reduction of the used thing, if the stack count drops
   *         to 0 the thing is supposed to be removed
   * @author Hansj�rg Malthaner
   */
  virtual int activate(const handle_tpl <thing_t> &user,
		       inventory_t * inv,
		       const handle_tpl <thing_t> &target) = 0;


  // ---- implements persistent_t ----


  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid() = 0;


};
#endif //USAGE_T_H
