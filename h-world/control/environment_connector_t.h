/* Copyright by Hj. Malthaner */

#ifndef ENVIRONMENT_CONNECTOR_T_H
#define ENVIRONMENT_CONNECTOR_T_H
class action_t;
class thing_t;

/**
 * Things may affect their environment or be affected by their
 * environment. This class executes all interactions between things
 * and their environments.
 *
 * @author Hj. Malthaner
 */
class environment_connector_t {
public:    

    void execute(thing_t * thing);


    /**
     * Adds an action.
     * @author Hj. Malthaner
     */
    void add_action(action_t *action);


    /**
     * Removes an action.
     * @author Hj. Malthaner
     */
    void remove_action(action_t *action);

private:

    slist_tpl <action_t *> actions;

};
#endif //ENVIRONMENT_CONNECTOR_T_H
