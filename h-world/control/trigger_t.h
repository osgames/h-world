/* 
 * trigger_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef trigger_t_h
#define trigger_t_h


#ifndef koord_h
#include "primitives/koord.h"
#endif


#ifndef CSTRING_T_H
#include "primitives/cstring_t.h"
#endif


class thing_t;
template <class T> class handle_tpl;


/**
 * This class acts as atrigger for Lua scripts. Triigers are part
 * of a square. Once a thing enters the square, the trigger is
 * activated
 * @author Hj. Malthaner 
 */
class trigger_t
{
 private:

  /**
   * Description to tell to the player if he looks at this trigger
   * @author Hj. Malthaner 
   */
  cstring_t desc;


  /**
   * Position of the trigger on the map
   * @author Hj. Malthaner 
   */
  koord pos_self;


  /**
   * Is this trigger active ?
   * @author Hj. Malthaner 
   */
  unsigned char active;


  /**
   * Does the player know about this trigger?
   * @author Hj. Malthaner 
   */
  unsigned char discovered;

  
  /**
   * User/outside defined type. Handed to the lua script.
   * @author Hj. Malthaner 
   */
  unsigned char type;


 public:


  bool is_active() const {return active;};
  bool is_discovered() const {return discovered;};


  void set_discovered(bool yesno);


  void set_type(unsigned char type);
  int get_type() const;

  void set_desc(const char * desc); 
  const char * get_desc() const;
  

  trigger_t(koord pos);


  /**
   * Starts the script
   * @author Hj. Malthaner 
   */
  void activate(const handle_tpl < thing_t > &thing);

};

#endif // trigger_t_h
