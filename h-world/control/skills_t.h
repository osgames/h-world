/* Copyright by Hj. Malthaner */

#ifndef SKILLS_T_H
#define SKILLS_T_H

class storage_t;
class iofile_t;

/**
 * A container for skills. The skills are ordered in a tree structure.
 * @author Hj. Malthaner
 */
class skills_t {
 public:

  /**
   * Constants to identify the skills
   * @author Hj. Malthaner
   */
  enum skills {
    root,
      physical,
        combat,
          offense,
            off_unarmed,
            blades,
            clubs,
            polearms,
          defense,
            def_unarmed,
            armed,
      mind,
        perception,
      social,
        trade,
      max_skills
  };

private:    

  /**
   * A single skill
   * @author Hj. Malthaner
   */
  class skill_t {
  public:
    skill_t *parent;
    int level;
    int value;
    char *name;

    skill_t();
  };


  skill_t skill_data[max_skills];
  
    
public:

  /**
   * Basic constructor, build the skills tree
   * @author Hj. Malthaner
   */
  skills_t();


  const char * get_skill_name(int i) {return skill_data[i].name;};
  const int & get_skill_level(int i) {return skill_data[i].level;};
  const int & get_skill_value(int i) {return skill_data[i].value;};


  /**
   * Adds a value to a skill (considers the skill level to eqalize
   * additions on different skill tree levels)
   * @return the new value of the skill 
   * @author Hj. Malthaner
   */
  int add_skill_value(int i, int v);


  /**
   * This is the actual value of the skill, the value all program part
   * should use for determining effevts.
   *
   * @return the sum of all skil values up to the root of the tree
   * @author Hj. Malthaner
   */
  int get_skill_sum(int i);


  /**
   * Stores/restores the object.
   * @author Hj. Malthaner
   */
  void read_write(storage_t *store, iofile_t * file);

};

#endif //ACTOR_T_H
