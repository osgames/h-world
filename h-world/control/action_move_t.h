/* 
 * action_move_t.h
 *
 * Copyright (c) 2002 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef ACTION_MOVE_T_H
#define ACTION_MOVE_T_H


#include "action_t.h"
#include "primitives/koord.h"
#include "model/thinghandle_t.h"
#include "tpl/handle_tpl.h"


class action_move_t : public action_t {
private:
  thinghandle_t thing;


  /**
   * where do we want to move?
   * @author Hj. Malthaner
   */
  koord move;


  /**
   * direction of last move
   * @author Hj. Malthaner
   */
  koord last_move;


  // we cache some attributes of thing for better performance
  int love, hate, fear;
  const char * ident;

public:


  /**
   * Get type of action performed by this object
   * @author Hansj�rg Malthaner
   */
  virtual enum atype get_type() const;


  action_move_t(thinghandle_t thing);


  /**
   * Checks if it's good to move now. If the thing is hurt badly or 
   * something important (friend, foe) must be reached it is 
   * important to move. 
   * @author Hj. Malthaner
   */
  int check();


  /**
   * Moves the thing. 
   * @return delay time for next action
   * @author Hj. Malthaner     
   */
  int execute();


  /**
   * Apply duration based effects
   * @author Hj. Malthaner
   */
  void apply_effects(int delay);


  // ---- implements persistent_t ----

  action_move_t();

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};
#endif //ACTION_MOVE_T_H
