#include "skills_t.h"

#include "persistence/iofile_t.h"

skills_t::skill_t::skill_t() {
  parent = 0;
  level = 0;
  value = 100000;
  name = "Unnamed";
};


/**
 * Basic constructor, build the skills tree
 * @author Hj. Malthaner
 */
skills_t::skills_t() {

  skill_data[root].parent = 0;
  skill_data[root].level = 0;
  skill_data[root].name = "Overall";

  skill_data[physical].parent = &skill_data[root];
  skill_data[physical].level = 1;
  skill_data[physical].name = "Physical";


  skill_data[combat].parent = &skill_data[physical];
  skill_data[combat].level = 2;
  skill_data[combat].name = "Combat";

  skill_data[offense].parent = &skill_data[combat];
  skill_data[offense].level = 3;
  skill_data[offense].name = "Offense";

  skill_data[off_unarmed].parent = &skill_data[offense];
  skill_data[off_unarmed].level = 4;
  skill_data[off_unarmed].name = "Unarmed";

  skill_data[blades].parent = &skill_data[offense];
  skill_data[blades].level = 4;
  skill_data[blades].name = "Blades";

  skill_data[clubs].parent = &skill_data[offense];
  skill_data[clubs].level = 4;
  skill_data[clubs].name = "Clubs";

  skill_data[polearms].parent = &skill_data[offense];
  skill_data[polearms].level = 4;
  skill_data[polearms].name = "Polearms";



  skill_data[defense].parent = &skill_data[combat];
  skill_data[defense].level = 3;
  skill_data[defense].name = "Defense";

  skill_data[def_unarmed].parent = &skill_data[defense];
  skill_data[def_unarmed].level = 4;
  skill_data[def_unarmed].name = "Unarmed";

  skill_data[armed].parent = &skill_data[defense];
  skill_data[armed].level = 4;
  skill_data[armed].name = "Armed";



  skill_data[mind].parent = &skill_data[root];
  skill_data[mind].level = 1;
  skill_data[mind].name = "Mind";

  skill_data[perception].parent = &skill_data[mind];
  skill_data[perception].level = 2;
  skill_data[perception].name = "Perception";


  skill_data[social].parent = &skill_data[root];
  skill_data[social].level = 1;
  skill_data[social].name = "Social";

  skill_data[trade].parent = &skill_data[social];
  skill_data[trade].level = 2;
  skill_data[trade].name = "Trading";

}


/**
 * Adds a value to a skill (considers the skill level to equalize
 * additions on different skill tree levels)
 * @return the new value of the skill 
 * @author Hj. Malthaner
 */
int skills_t::add_skill_value(int i, int v) {

  skill_t *skill = &skill_data[i];

  v *= 1 << skill->level;

  // traverse tree
  do {
    skill->value += v;
    v >>= 1;

    skill = skill->parent;
  } while(skill);

  return skill_data[i].value;
}


/**
 * This is the actual value of the skill, the value all program parts
 * should use for determining effevts.
 *
 * @return the sum of all skill values up to the root of the tree
 * @author Hj. Malthaner
 */
int skills_t::get_skill_sum(int i) {
  const skill_t *skill = &skill_data[i];
  int sum = 0;

  // traverse tree
  do {
    sum += skill->value;
    skill = skill->parent;
  } while(skill);

  return sum/(skill_data[i].level+1);
}


/**
 * Stores/restores the object.
 * @author Hj. Malthaner
 */
void skills_t::read_write(storage_t *store, iofile_t * file)
{
  for(int i=0; i<max_skills; i++) { 
    file->rdwr_int(skill_data[i].value);
  }
}
