/* 
 * thing_usage_selector_t.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "thing_usage_selector_t.h"
#include "commands.h"
#include "usage_t.h"
#include "attack_t.h"

#include "gui/multi_choice_t.h"
#include "model/thing_t.h"

#include "tpl/handle_tpl.h"
#include "tpl/slist_tpl.h"

thing_usage_selector_t::thing_usage_selector_t()
{
  // nothing to do
}


/**
 * Select an usage and activate it
 * @param inv the inventory if the things is in an inventory, null otherwise
 * @return the delay of the action
 * @author Hj. Malthaner
 */
int thing_usage_selector_t::select_and_activate(const handle_tpl<thing_t> & user,
						 inventory_t * inv,
						 const handle_tpl<thing_t> & thing)
{
  int delay = 10;        // Hajo: default value
  slist_tpl <usage_t *> & all_usages = thing->get_usages();
  slist_iterator_tpl <usage_t *> iter (all_usages);

  slist_tpl <usage_t *> usages;

  // Hajo: filter attacks

  while( iter.next() ) {
    usage_t * usg = iter.get_current();

    attack_t *atk = dynamic_cast <attack_t *> (usg);
      
    if(atk == 0 && usg != 0) {
      usages.append(usg);
    }
  }
  
  // now select ...

  if(usages.count() > 0) {

    int i = -1;

    if(usages.count() == 1) {
      i = 0;
    } else {
      int n = 0;
      char message[128];
      char buttons[1024];

      // Hajo: reuse buttons buffer .. dirty
      thing->get_ident_plain(buttons);
      sprintf(message, "Using the %s:", buttons);
      // hack end

      slist_iterator_tpl <usage_t *> iter (usages);

      i = 0; 

      while( iter.next() ) {
	const usage_t * usg = iter.get_current();
	
	if(n > 0) {
	  n += sprintf(buttons+n, "|");
	}

	n += sprintf(buttons+n, "%c: %s it", 'a'+i, usg->get_verb());
	i ++;
      }

      multi_choice_t choice ("Please select:", message, buttons);

      koord c_size = choice.get_size();
      choice.center();
      choice.hook();
      
      const char c = choice.get_choice();
    
      i = c - 'a';
    }

    if(i >= 0 && i<usages.count()) {
      delay = interactive_usage(user, inv, thing, usages.at(i));
    }
  }

  return delay;
}
