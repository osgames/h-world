/* 
 * action_speak_t.h
 *
 * Copyright (c) 2001 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef ACTION_SPEAK_T_H
#define ACTION_SPEAK_T_H


#include "action_t.h"
#include "model/thinghandle_t.h"
#include "tpl/handle_tpl.h"

class escapable_frame_t;

class action_speak_t : public action_t
{
private:
  thinghandle_t thing;

  thinghandle_t customer;

  escapable_frame_t *window;
  
  char *filename;

  int radius;

public:


  /**
   * Get type of action performed by this object
   * @author Hansj�rg Malthaner
   */
  virtual enum atype get_type() const;


  action_speak_t(thinghandle_t thing, const char *filename);

  ~action_speak_t();


  /**
   * Checks if it's good to speak now.
   * @author Hj. Malthaner
   */
  int check();


  /**
   * Do speak. 
   * @return delay time for next action
   * @author Hj. Malthaner
   */
  int execute();


  /**
   * Apply duration based effects
   * @author Hj. Malthaner
   */
  void apply_effects(int delay);


  // ---- implements persistent_t ----

  action_speak_t();

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();


};
#endif //ACTION_SPEAK_T_H

