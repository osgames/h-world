#ifndef remote_user_t_h
#define remote_user_t_h


#include "user_input_t.h"

class server_socket_t;

/**
 * This action processes user input from a remote machine
 * @author Hj. Malthaner
 */
class remote_user_t : public user_input_t
{
 private:

  class server_socket_t *socket;


 public:

  remote_user_t(thinghandle_t thing);

  virtual ~remote_user_t();


  /**
   * This method checks if the action is good to execute 
   * regarding the surrounding situation. 
   *
   * Used values so far:
   *   -1 (no attack possible)
   *  500 (attack possible)
   *    1 (move normal)
   *  999 (flee)
   *
   * @author Hansj�rg Malthaner
   */
  virtual int check();


  /**
   * This method performs the action. 
   * @return delay time for next action, a negative delay means
   *         the actor will no more be included in scheduling
   * @author Hansj�rg Malthaner
   */
  virtual int execute();


  // ---- implements persistent_t ----

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};

#endif // remote_user_t_h
