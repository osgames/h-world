/* Copyright by Hj. Malthaner */

#ifndef ACTION_T_H
#define ACTION_T_H

#ifndef PERSISTENT_T_H
#include "persistence/persistent_t.h"
#endif

#ifndef thinghandle_t_h
#include "model/thinghandle_t.h"
#endif

#ifndef tpl_handle_tpl_h
#include "tpl/handle_tpl.h"
#endif

class thing_t;
class actor_t;

/** 
 * An actor can have several actions. The actions can check whether 
 * they are appropriate to the situation and then executes the best 
 * matching action.
 * 
 * Actions are also used to determine effects of things to their environemtn or
 * effects of the environment on the things.
 * @interface
 * @author Hansj�rg Malthaner
 * @since 0.1 
 */
class action_t : public persistent_t {
 public:    

  /**
   * "other" is an unspecified type
   * @author Hansj�rg Malthaner
   */
  enum atype {move, attack, speak, trade, user_input, other};

 protected:


  /**
   * Apply duration based effects
   * @author Hj. Malthaner
   */
  void apply_effects(thinghandle_t thing, int duration);


  /**
   * The actor has the skills. Some actions ned to check the skills
   * some need to modify the skills. Thus the actor must be known by
   * the action.
   * @author Hansj�rg Malthaner
   */
  actor_t * actor;



 public:    
  
  void set_actor(actor_t *a);


  /**
   * Get type of action performed by this object
   * @author Hansj�rg Malthaner
   */
  virtual enum atype get_type() const = 0;


  virtual ~action_t();


  /**
   * This method checks if the action is good to execute 
   * regarding the surrounding situation. 
   *
   * Used values so far:
   *   -1 (no attack possible)
   *  500 (attack possible)
   *    1 (move normal)
   *  999 (flee)
   *
   * @author Hansj�rg Malthaner
   */
  virtual int check() = 0;


  /**
   * This method performs the action. 
   * @return delay time for next action, a negative delay means
   *         the actor will no more be included in scheduling
   * @author Hansj�rg Malthaner
   */
  virtual int execute() = 0;


  /**
   * Apply duration based effects
   * @author Hj. Malthaner
   */
  virtual void apply_effects(int delay) = 0;


  // ---- implements persistent_t ----

  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file) = 0;


  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid() = 0;

};
#endif //ACTION_T_H
