/* 
 * drag_thing.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "thing_drag_t.h"
#include "model/thing_t.h"

thinghandle_t thing_drag_t::get_thing() const {
  return thing;
}


void thing_drag_t::set_thing(thinghandle_t thing) {
  this->thing = thing;

  if(thing.is_bound()) {
    set_size(koord(thing->get_int("width", 5) * 16,
		   thing->get_int("height", 5) * 16));
  }
};


thing_drag_t::thing_drag_t(thinghandle_t thing) 
{
  set_thing(thing);
}


void thing_drag_t::draw(graphics_t *gr, koord pos)
{
  if(thing.is_bound()) {
    thing->inv_visual.paint(gr, pos.x, pos.y); 
  }
}
