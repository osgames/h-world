/* 
 * body_view_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef BODY_VIEW_T_H
#define BODY_VIEW_T_H

#include "swt/grid_layout_t.h"
#include "swt/drag_controller_t.h"
#include "tpl/slist_tpl.h"
#include "model/thinghandle_t.h"
#include "ifc/thing_listener_t.h"

#include "escapable_frame_t.h"
#include "props_display_t.h"

class koord;
class thing_hold_t;
class inventory_view_t;
class square_item_view_t;
class gui_component_t;

/**
 * A inspection window for the things body and inventory.
 * @author Hj. Malthaner
 */
class body_view_t : public escapable_frame_t, thing_listener_t 
{
private:    
    thinghandle_t thing;

    slist_tpl <inventory_view_t *> inventory_views;
    slist_tpl <thing_hold_t *> thing_holds;

    slist_tpl <gui_component_t *> kill_list;

    square_item_view_t *square_view;


    /**
     * Displays properties of watched items
     * @author Hj. Malthaner
     */
    props_display_t display;


    grid_layout_t layout;

    drag_controller_t drag_control;

    void create_body_ui(thinghandle_t &limb, koord ppos);


    /**
     * Create display of all inventories
     *
     * @param limb   currently inspected limb
     * @param pos    position of first inventory view
     * @param max_y  max useable height
     *
     * @author Hj. Malthaner
     */
    koord create_inventory_ui(const thinghandle_t &limb, 
			      koord pos,
			      int max_y);

    /**
     * creates new components
     */
    void do_layout();

    /**
     * deletes all components
     */
    void dispose();

public:    
    /**
     * Basic constructor.
     * @param thing the thing to display. May not be NULL!
     * @param title the window title. May not be NULL!
     * @author Hj. Malthaner
     */
    body_view_t(thinghandle_t &thing, const char *title,
		bool autodelete);

    ~body_view_t();


    /**
     * called before window is closed in response to a close event
     * @author Hj. Malthaner
     */
    virtual void on_close();


    void set_model(thinghandle_t thing);


    /**
     * Refresh with contents from square at position k.
     * @author Hj. Malthaner
     */
    void refresh(koord k);


    /**
     * Implements thing_listener_t
     * @author Hj. Malthaner
     */
    virtual void update(const handle_tpl<thing_t> &);


    /**
     * Events werden hiermit an die GUI-Komponenten
     * gemeldet
     * @author Hj. Malthaner
     */
    virtual bool process_event(const event_t &ev);
};
#endif //BODY_VIEW_T_H
