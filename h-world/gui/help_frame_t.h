#ifndef help_frame_t_h
#define help_frame_t_h

#include "modal_frame_t.h"
#include "hyperlink_listener_t.h"
 
class html_view_t;
class cstring_t;

class help_frame_t : public modal_frame_t, hyperlink_listener_t
{
 private:

  html_view_t * view;

  void init(const char * path, const char * filename);

 public:

  help_frame_t(const char * path, const char * filename, bool is_modal);


  /**
   * Creates a new help frame with fixed path "./help"
   * 
   * @author Hj. Malthaner
   */
  help_frame_t(const char * filename, bool is_modal);


  ~help_frame_t();


  /**
   * Called upon link activation
   * @param the hyper ref of the link
   * @author Hj. Malthaner
   */
  void hyperlink_activated(const cstring_t &txt);

};

#endif // help_frame_t_h
