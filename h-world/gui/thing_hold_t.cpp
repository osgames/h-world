/* 
 * thing_hold_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include <string.h>
#include <ctype.h>

#include "thing_hold_t.h"
#include "thing_drag_t.h"
#include "multi_choice_t.h"
#include "ifc/inventory_view_listener_t.h"
#include "model/thing_t.h"
#include "model/thinghandle_t.h"
#include "view/world_view_t.h"
#include "util/debug_t.h"

#include "gui_utils.h"

#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"
#include "swt/event_t.h"
#include "swt/drag_controller_t.h"
#include "swt/tileset_t.h"
#include "swt/tile_descriptor_t.h"

// #include "util/debug_t.h"

#include "fontdef.h"

#define RASTER 16


/**
 * Check required limbs
 * @author Hj. Malthaner
 */
static bool check_multi(thinghandle_t limb, 
			thinghandle_t item,
			thinghandle_t old) 
{
  char key[256];
  const int count = item->get_int("limbs_required", 0);
  bool ok = true;

  // Hajo: always use root to find limbs
  limb = thing_t::find_root(limb);

  for(int i=0; i<count && ok; i++) { 
    sprintf(key, "limbs_required[%d].type", i);
    const char * type = item->get_string(key, 0);

    sprintf(key, "limbs_required[%d].subtype", i);
    const char * subtype = item->get_string(key, 0);

    thinghandle_t req = thing_t::find_limb(limb, type, subtype);

    // Hajo: limb must exist and must be empty
    if(req.is_bound()) {
      ok &= (!req->get_item().is_bound() || req->get_item() == old); 

      dbg->message("check_multi()", "checking %s: %d", 
		   req->get_string("ident", "unnamed"),
		   ok);

    } else {
      ok = false,

      dbg->message("check_multi()", "no limb for %s, %s", 
		   type, subtype);

    }
  }

  dbg->message("check_multi()", "done, ok=%d", ok);

  return ok;
}


/**
 * Put item on all limbs
 * @author Hj. Malthaner
 */
/*
static void put_multi(thinghandle_t limb, thinghandle_t item) 
{
  char key[256];
  const int count = item->get_int("limbs_required", 0);

  // Hajo: always use root to find limbs
  limb = thing_t::find_root(limb);

  for(int i=0; i<count; i++) { 
    sprintf(key, "limbs_required[%d].type", i);
    const char * type = item->get_string(key, 0);
    
    sprintf(key, "limbs_required[%d].subtype", i);
    const char * subtype = item->get_string(key, 0);
      
    thinghandle_t req = thing_t::find_limb(limb, type, subtype);
    
    req->set_multi_item(item);
  }

  dbg->message("put_multi()", "done");
} 
*/


/**
 * Removes a multi limb item from all required limbs
 * @param limb one limb of the body, must not be null
 * @param item the item to remove, can be null
 * @authot Hj. Malthaner
 */
/*
static void remove_multi(thinghandle_t limb, thinghandle_t item) 
{
  if(item.is_bound()) {
    // Hajo: check required limbs
    const int count = item->get_int("limbs_required", 0);
    char key[256];

    // Hajo: always use root to find limbs
    limb = thing_t::find_root(limb);

    for(int i=0; i<count; i++) { 
      sprintf(key, "limbs_required[%d].type", i);
      const char * type = item->get_string(key, 0);

      sprintf(key, "limbs_required[%d].subtype", i);
      const char * subtype = item->get_string(key, 0);

      thinghandle_t req = thing_t::find_limb(limb, type, subtype);

      // Hajo: limb must exist and must hold this item
      if(req.is_bound() && req->get_item() == item) {
	req->set_multi_item(0);
      }
    }
  }
}
*/


void thing_hold_t::add_listener(inventory_view_listener_t *l)
{
  listeners->insert(l);
}


thing_hold_t::thing_hold_t(thinghandle_t &l): limb(l)
{
  font = font_t::load(FONT_SML);

  koord size = limb->get_koord("hold_size");

  if(size != koord(-1,-1)) {
    const int w = get_tileborder_width() * 2;

    set_size(size*RASTER + koord(w,w));
  } else {
    set_size(koord(8,8));
  }
 
  listeners = new slist_tpl <inventory_view_listener_t *> ();

  strncpy(title, limb->get_string("name", "unnamed"), 63);
  title[63] = '\0';
  title[0] = toupper(title[0]);

  drop_state = neutral;
  old_item = 0;
}


void thing_hold_t::set_model(thinghandle_t &limb) 
{
  this->limb = limb;

  set_size(limb->get_koord("hold_size")*RASTER);
}


thing_hold_t::~thing_hold_t()
{
  delete listeners;
  listeners = 0;
}


void thing_hold_t::draw(graphics_t *gr, koord screenpos)
{
  static color_t green (192, 232, 176);
  static color_t red   (232, 192, 176);
  static color_t grey  (208, 208, 208);
  
  const koord size = get_size();

  // dbg->debug("thing_hold_t::draw()", "Drawing '%s'", title);

  if(size.x == 0 || limb.is_bound() == false) {
    // not visible
    return;
  }

  const koord pos = get_pos()+screenpos;
  const int w = get_tileborder_width();  

  if(drop_state == neutral) {
    const tileset_t *tileset = world_view_t::get_instance()->get_tileset(2);
    const tile_descriptor_t *tile = tileset->get_tile(18);

    for(int y=0; y<size.y; y += tile->h) {
      for(int x=0; x<size.x; x += tile->w) {
	gr->draw_tile(tile, pos.x+x+w, pos.y+y+w);
      }
    }

  } else {
    color_t & color = 
      drop_state == allowed ? green : red;

    gr->fillbox_wh(pos.x+w, pos.y+w, size.x-w-w, size.y-w-w, color);
  }


  font->draw_string(gr, title, pos.x+w+1, pos.y+w, 0, color_t::BLACK);


  if(limb->get_item().is_bound()) {
    const thinghandle_t & thing = limb->get_item();

    const koord tpos = pos + (size - thing->get_inv_size()*RASTER)/2;

    thing->inv_visual.paint(gr, tpos.x, tpos.y);
  }

  tileborderframe(gr, pos, size);
}


/**
 * Implements drop_target_t
 * @author Hansj�rg Malthaner
 */
bool thing_hold_t::drop(drawable_t * item, koord /*at*/)
{
  dbg->message("thing_hold_t::drop()", "called");

  bool ok = !limb->get_item().is_bound();

  thing_drag_t *drag = dynamic_cast<thing_drag_t *>(item);

  if(drag) {
    thinghandle_t tmp = limb->get_item();
    thinghandle_t item = drag->get_thing();

    if(limb->check_item_constraints(item)) {

      if(drop_state != denied) {

	dbg->message("thing_hold_t::drop()", "removing old item");
	limb->remove_multi(tmp);

	dbg->message("thing_hold_t::drop()", "put new item");
	limb->put_multi(item);

	dbg->message("thing_hold_t::drop()", "done");

	drag->set_thing(tmp);
	fire_mouse_over_thing(item);

      } else {
	ok = false;
      }

      // redraw();
    }
    else {
      ok = false;
    }
  }

  return ok;
}


/**
 * called if item is dragged over the drop target, but not yet dropped
 * @author Hj. Malthaner
 */
void thing_hold_t::hover(drawable_t * item, koord at)
{
  const int old_state = drop_state;

  // const koord size = get_size();
  // const bool inside = at.x >= 0 && at.y >= 0 && at.x < size.x && at.y < size.y;

  if(item) {

    if(old_item != item) {

      drag_controller_t * drag_control = get_drag_control();
      drawable_t * drag = drag_control->get_drag(); 
      
      if(drag) {
	thing_drag_t * drag_thing = (thing_drag_t *)drag;
	thinghandle_t item = drag_thing->get_thing();
	
	if(item.is_bound() &&
	   limb->check_item_constraints(item) &&
	   check_multi(limb, item, limb->get_item()) &&
	   (!limb->get_item().is_bound() ||
	    *limb->get_item()->get_string("cursed", "f") != 't')) {
	  drop_state = allowed;
	} else {
	  drop_state = denied;
	}
      }
    }
  } else {
    drop_state = neutral;
  }

  old_item = item;

  if(old_state != drop_state) {
    redraw();
  }
}


/**
 * If dragging is stopped, the item needs to be put back
 * @author Hj. Malthaner
 */
void thing_hold_t::stop_dragging(drawable_t *item)
{
  drop(item, koord(0,0));
}


/**
 * Process an event.
 * @return true if event was swallowed, false otherwise
 * @author Hj. Malthaner
 */
bool thing_hold_t::process_event(const event_t &ev)
{
  static thinghandle_t null;

  if(ev.type == event_t::BUTTON_RELEASE) {
    thinghandle_t item = limb->get_item();

    if(item.is_bound()) {
      dbg->message("thing_hold_t::process_event()", 
		   "took item %s", item->get_string("ident", "unnamed")); 

      // Hajo: check for cursed items
      if(*item->get_string("cursed", "f") == 't') {

	multi_choice_t c_info("Cursed thing:", 
			      "This thing appears to be stuck!", 
			      "y: ok");

	c_info.center();
	c_info.hook();
	
      } else {
	drawable_t * drag = new thing_drag_t(item);
	start_dragging(this, drag);

      
	// Hajo: remove multi-limb items properly
	limb->remove_multi(item);

	fire_mouse_over_thing(null);


	// Hajo: unneded ?
	// redraw();
      }
    }
  }
  else if(ev.type == event_t::CONTAINER) {

    if(ev.code == event_t::ENTER) {
      // printf("ENTER %s\n", limb->get_string("name", "unnamed"));
      
      drop_state = neutral;
      fire_mouse_over_thing(limb->get_item());

    } else if(ev.code == event_t::LEAVE) {
      // printf("LEAVE %s\n", limb->get_string("name", "unnamed"));

      drop_state = neutral;
      fire_mouse_over_thing(null);
	
    }
  }
  return false;
}



void thing_hold_t::fire_mouse_over_thing(const thinghandle_t &t)
{
  slist_iterator_tpl <inventory_view_listener_t *> iter (listeners);

  while( iter.next() ) {
    iter.get_current()->mouse_over_thing(t);
  }
}
