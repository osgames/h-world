/* 
 * html_view_t.cpp
 *
 * Copyright (c) 2003 - 2005 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"
#include "swt/event_t.h"

#include "html_view_t.h"

#include "fontdef.h"

// Testing
#include "tile_font_t.h"


void html_view_t::destroy_nodes()
{
  // Hajo: clean up nodes

  while(nodes) {
    node_t * tmp = nodes->next;
    delete nodes;
    nodes = tmp;
  }

  while(links) {
    hyperlink_t * tmp = links->next;
    delete links;
    links = tmp;
  }
}


html_view_t::node_t::node_t(const cstring_t &t, int a) : text(t) 
{
  att = a;
  next = 0;                                
}

                                   
html_view_t::html_view_t() 
{
  font = font_t::load(FONT_STD);

  // font = new tile_font_t();
  // font->load("../tilefonts/tilefont.pak");
  
  // Hajo: initialitze with empty list                
  nodes     = 0;   
  links     = 0;   
  listeners = 0;
  
  title[0] = '\0';
}


html_view_t::~html_view_t() 
{
  destroy_nodes();
  
  while(listeners) {
    listener_t * tmp = listeners->next;
    delete listeners;
    listeners = tmp;
  }
}
                                 

/**
 * Sets the text to display.
 * @author Hj. Malthaner
 */
void html_view_t::set_text(const char *text) {
    
    // Hajo: danger here, longest word in text
    // must not exceed 255 chars!
    char word[256];
    int att = 0;

    const unsigned char * tail = (const unsigned char *)text;
    const unsigned char * lead = (const unsigned char *)text;

    destroy_nodes();

    node_t *node = nodes;
    hyperlink_t *link = links;

    // hyperref param
    cstring_t param;

    while(*tail) {
	if(*lead == '<') {
	    // parse a tag

	    while(*lead != '>') {
		lead++;
	    }

	    strncpy(word, (const char *)tail+1, lead-tail-1);
	    word[lead-tail-1] = '\0';
	    lead++;

	    if(word[0] == 'p' || 
	       (word[0] == 'b' && word[1] == 'r') ||
	       word[1] == 'p') {
		att = ATT_NEWLINE;
	    }
            else if(word[0] == 'a') {
	      att = ATT_A_START;

	      param = word;

	    }
            else if(word[0] == '/' && word[1] == 'a') {
		att = ATT_A_END;

		hyperlink_t * tmp = new hyperlink_t();
		tmp->param = param.substr(8, param.len()-1);

		// printf("'%s'\n", tmp->param.chars());

		if(links) {
		  // append
		  link = link->next = tmp;
		} else {         
		  // insert first
		  link = links = tmp;
		}


	    }
            else if(word[0] == 'h' && word[1] == '1') {
		att = ATT_H1_START;
	    }
            else if(word[1] == 'h' && word[2] == '1') {
		att = ATT_H1_END;
	    }
            else if(word[0] == 'e') {
		att = ATT_EM_START;
	    }
            else if(word[1] == 'e') {
		att = ATT_EM_END;
	    }
            else if(word[0] == 's') {
		att = ATT_STRONG_START;
	    }
            else if(word[1] == 's') {
		att = ATT_STRONG_END;
	    }
            else if(word[0] == 't') {
		// title tag
		const unsigned char * title_start = lead; 

		while(*lead != '<') lead++;

		strncpy(title, (const char *)title_start, lead-title_start);
	        title[lead-title_start] = '\0';

		while(*lead != '>') lead++;
		lead ++;

		att = ATT_UNKNOWN;
	    }
	    else {
		att = ATT_UNKNOWN;
	    }

	} else {
	    // parse a word

	    while(*lead > 32 && *lead != '<') {
		lead++;
	    }

	    strncpy(word, (const char *)tail, lead-tail);
	    word[lead-tail] = '\0';
	    att = ATT_NONE;
	}
	
	// printf("'%s'\n", word);       

	node_t * tmp = new node_t(word, att);
	
	if(node) {
	    // append
	    node->next = tmp;
	} else {         
	    // insert first
	    nodes = tmp;
	}

	node = tmp;


	while(*lead <= 32 && *lead > 0) {
	    lead++;
	}

	tail = lead;
    }
}


const char * html_view_t::get_title() const {
    return title;
}


koord html_view_t::get_preferred_size() const {
  return output(0, koord(0,0), false);
}


void html_view_t::add_listener(hyperlink_listener_t *call)
{
  listener_t * l = new listener_t();

  l->callback = call;
  l->next = listeners;

  listeners = l;
}


/**
 * Paints the component
 * @author Hj. Malthaner
 */
void html_view_t::draw(graphics_t *gr, koord screenpos)
{
  const koord pos = get_pos()+screenpos;
  
  output(gr, pos, true);
}


koord html_view_t::output(graphics_t *gr, koord offset, bool doit) const 
{
  const int fh = font->get_height();
  const int LINESPACE = fh + (fh >> 1);

  color_t red (255, 80, 40);
  color_t blue (25, 60, 210);
  painter_t pt(gr);

  const int width = get_size().x;

  hyperlink_t * link = links;

  node_t *node = nodes;
  int xpos = 0;
  int ypos = 0;
  color_t * color = &color_t::BLACK;
  int max_width = width;
  bool bold = false;

  const int space = font->get_string_width(" ", 0);



  if(doit) {
    // XXX
    // gr->set_font(font);
  }

  while(node) {
    if(node->att == ATT_NONE) {

      int w = font->get_string_width(node->text, bold ? 1 : 0) + space;

      int nxpos = xpos + w;

      if(nxpos >= width) {
	if(nxpos-xpos > max_width) {
	  // word too long
	  max_width = nxpos;
	}
	nxpos -= xpos;
	xpos = 0;
	ypos += LINESPACE;
      }
                                                    
      if(doit) {
	if(bold) {

	  pt.draw_string_bold(font, node->text, offset + koord(xpos,ypos), 1,
			      color_t::BLACK, color_t::GREY64);


	  /*
	  font->draw_string(gr, node->text, 
			    offset.x+xpos+1, offset.y+ypos, 2);
	  font->draw_string(gr, node->text, 
			    offset.x+xpos, offset.y+ypos, 2);
	  */
	} else {

	  font->draw_string(gr, node->text,
			    offset.x+xpos, offset.y+ypos, 0,
			    *color);


	  /*
	  font->draw_string(gr, node->text, 
			    offset.x+xpos, offset.y+ypos, 1);
	  */
	}
      }
      
      xpos = nxpos;               
    }                                  
    else if(node->att == ATT_H1_START) {
      color = &color_t::BLACK;
      bold = true;
    }
    else if(node->att == ATT_H1_END) {
      color = &color_t::BLACK;
      bold = false;
      
      /*
	if(doit) {
	gr->fillbox_wh(offset.x, offset.y+LINESPACE-2, 
	xpos-4, 1, *color);
	}
      */

      xpos = 0;
      ypos += LINESPACE;
    }
    else if(node->att == ATT_EM_START) {
      color = &color_t::WHITE;
    }
    else if(node->att == ATT_EM_END) {
      color = &color_t::BLACK;
    }
    else if(node->att == ATT_STRONG_START) {
      color = &red;
    }
    else if(node->att == ATT_STRONG_END) {
      color = &color_t::BLACK;
    }
    else if(node->att == ATT_A_START) {
      color = &blue;
      link->tl.x = xpos;
      link->tl.y = ypos;
    }
    else if(node->att == ATT_A_END) {
      link->br.x = xpos;
      link->br.y = ypos+14;
      
      if(doit) {
	gr->fillbox_wh(link->tl.x + offset.x, 
		       link->tl.y + offset.y + fh, 
		       link->br.x-link->tl.x-4, 1, 
		       *color);
      }
      
      link = link->next;
      
      color = &color_t::BLACK;
    }
    else if(node->att == ATT_NEWLINE) {
      xpos = 0;
      ypos += LINESPACE;
    }
    
    node = node->next;
  }
          
  
  // if(!doit) printf("%d\n", max_width);
  
  return koord(max_width, ypos+LINESPACE);
}



/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool html_view_t::process_event(const event_t &ev)
{
  koord pos = get_pos();

  if(ev.type == event_t:: BUTTON_RELEASE) {

    // printf("Mouse clicked at %d, %d\n", ev.cpos.x, ev.cpos.y);

    // scan links for hit


    hyperlink_t * link = links;

    while(link) {

      // printf("Checking link at %d,%d %d,%d\n", link->tl.x, link->tl.y, link->br.x, link->br.y);


      if(link->tl.x <= ev.cpos.x-pos.x && link->br.x >= ev.cpos.x-pos.x && 
	 link->tl.y <= ev.cpos.y-pos.y && link->br.y >= ev.cpos.y-pos.y) {

	// printf("Link hit '%s'\n", link->param.chars());


	// call listeners

	listener_t * l = listeners;

	while(l) {

	  l->callback->hyperlink_activated(link->param);
	  l = l->next;
	}

	
      }

      link = link->next;
    }
  }

  return false;
}
