/* 
 * book_frame_t.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "book_frame_t.h"
#include "chapter_view_t.h"

#include "hw_button_t.h"

#include "model/thing_t.h"
#include "model/thinghandle_t.h"

#include "util/debug_t.h"

book_frame_t::book_frame_t(const handle_tpl <thing_t> & model) : modal_frame_t("Book of memories: Quests", true, false)
{
  chapter_view = new chapter_view_t(model, "quest");

  chapter_view->set_pos(koord(10,10));
  chapter_view->set_size(koord(200, 320));

  set_size(koord(220, 360));

  add_component(chapter_view);

  chapter_view->show_page();

  quests = new hw_button_t("q: Quests", 'q');
  events = new hw_button_t("e: Events", 'e');
  
  quests->set_size(koord(90,16));
  events->set_size(koord(90,16));

  quests->set_pos(koord(10, 320));
  events->set_pos(koord(110, 320));

  quests->add_listener(this);
  events->add_listener(this);

  add_component(quests);
  add_component(events);
}


book_frame_t::~book_frame_t()
{
  delete chapter_view;
  chapter_view = 0;

  delete quests;
  quests = 0;

  delete events;
  events = 0;
}


void book_frame_t::action_triggered(gui_action_trigger_t * trigger)
{
  // dbg->message("book_frame_t::action_triggered()", "quests=%p events=%p trigger=%p", quests, events, trigger);

  if(trigger == quests) {
    chapter_view->change_catg("quest");
    chapter_view->show_page();
    set_title("Book of memories: Quests");
    redraw();
  }

  if(trigger == events) {
    chapter_view->change_catg("event");
    chapter_view->show_page();
    set_title("Book of memories: Events");
    redraw();
  }
}
