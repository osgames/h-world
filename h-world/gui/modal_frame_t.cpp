#include "util/debug_t.h"

#include "modal_frame_t.h"
#include "swt/event_t.h"
#include "swt/font_t.h"
#include "swt/tileset_t.h"
#include "swt/window_manager_t.h"


#include "../guidef.h"
#include "../fontdef.h"


modal_frame_t::modal_frame_t(const char *title) : 
  escapable_frame_t(title, false)
{
  is_modal = true;
  is_autodelete = false;
}


modal_frame_t::modal_frame_t(const char *title, bool m, bool ad) : 
  escapable_frame_t(title, false)
{
  is_modal = m;
  is_autodelete = ad;
}


void modal_frame_t::hook()
{
  dbg->message("modal_frame_t::hook()", "Called, this=%p", this);

  set_visible(true);

  window_manager_t *winman = window_manager_t::get_instance();

  if(winman->is_window_managed(this)) {
    winman->top_window(this);
  } else {
    winman->add_window(this);
  }

  if(is_modal) {

    do {
      window_manager_t::get_instance()->get_and_process_events();
    } while(is_modal);
  }

  if(is_autodelete) {
    kill_window(this);
  }

  dbg->message("modal_frame_t::hook()", "Done, this=%p", this);
}


/**
 * called before window is closed in response to a close event
 * @author Hj. Malthaner
 */
void modal_frame_t::on_close() {
  is_modal = false;
}
