/* 
 * lua_frame_t.h
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef lua_frame_t_h
#define lua_frame_t_h

#ifndef modal_frame_t_h
#include "modal_frame_t.h"
#endif

#ifndef ACTION_LISTENER_T_H
#include "swt/ifc/action_listener_t.h"
#endif


class hw_button_t;


/**
 * A modal frame type used by lua_system.cpp to create user-defined
 * UIs.
 * @author Hj. Malthaner
 */
class lua_frame_t : public modal_frame_t, action_listener_t
{
 private:


  /**
   * Name of Lua callback function
   * @author Hj. Malthaner
   */
  char callback[64];

  hw_button_t * buttons[128];


  /**
   * Button counter (index)
   * @author Hj. Malthaner
   */
  int bix;


 public:

  lua_frame_t(const char * title, const char * callback);


  hw_button_t * add_buttonlabel(const char * label, int img, int x, int y, int w, int h);


  void action_triggered(gui_action_trigger_t * trigger);
};

#endif // lua_frame_t_h
