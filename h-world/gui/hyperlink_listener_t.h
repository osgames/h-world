#ifndef hyperlink_listener_t_h
#define hyperlink_listener_t_h

class cstring_t;

/**
 * Listener if a hyper link was activated
 * @author Hj. Malthaner
 */

class hyperlink_listener_t
{

 public:

  /**
   * Called upon link activation
   * @param the hyper ref of the link
   * @author Hj. Malthaner
   */
  virtual void hyperlink_activated(const cstring_t &txt) = 0;

};

#endif // hyperlink_listener_t_h
