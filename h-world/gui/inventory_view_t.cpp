/* 
 * inventory_view_t.cpp
 *
 * Copyright (c) 2002 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "swt/event_t.h"
#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"
#include "swt/tileset_t.h"
#include "view/world_view_t.h"

#include "inventory_view_t.h"
#include "ifc/inventory_view_listener_t.h"
#include "thing_drag_t.h"

#include "model/inventory_t.h"
#include "model/thing_t.h"
#include "gui_utils.h"

#include "control/thing_usage_selector_t.h"
#include "control/actor_t.h"
#include "control/scheduler_t.h"


#include "util/debug_t.h"

#include "fontdef.h"

/**
 * Item raster width in pixels
 * @author Hj. Malthaner
 */
#define RASTER 16



void inventory_view_t::set_model(inventory_t *inv) 
{
  this->inv = inv;

  if(inv) {
    const int w = get_tileborder_width() * 2;
    set_size(inv->get_size()*RASTER + koord(w,w));

  } else {
    set_size(koord(10,10));     
  }
}


void inventory_view_t::add_listener(inventory_view_listener_t *l)
{
  listeners->insert(l);
}


inventory_view_t::inventory_view_t()
{
  listeners = new slist_tpl <inventory_view_listener_t *> ();
  font = font_t::load(FONT_SML);

  title = "";
}


inventory_view_t::~inventory_view_t()
{
  delete listeners;
  listeners = 0;
}


void inventory_view_t::draw(graphics_t *gr, koord screenpos)
{
  const tileset_t *tileset = world_view_t::get_instance()->get_tileset(2);
  const tile_descriptor_t *tile = tileset->get_tile(1);

  const koord size = get_size();
  const koord pos = get_pos()+screenpos;

  if(inv) {
    const int w = get_tileborder_width();

    for(int y=0; y<size.y; y+=RASTER) {
      for(int x=0; x<size.x; x+=RASTER) {
	gr->draw_tile(tile, pos.x+x+w, pos.y+y+w);
      }
    }

    tileborderframe(gr, pos, size);

    font->draw_string(gr, title, pos.x+w+1, pos.y+w+1, 0, color_t::BLACK);  

    for(int y=0; y<size.y/RASTER; y++) {
      for(int x=0; x<size.x/RASTER; x++) {
	const koord k (x,y);
	if(inv->is_root(k)) {
	  thinghandle_t  thing ( inv->get_from(k) );
	  if(thing.is_bound()) {
	    thing->inv_visual.paint(gr, pos.x+x*RASTER+w, pos.y+y*RASTER+w);
	  }
	}
      }
    }

    /*
    gr->fillbox_wh(pos.x, pos.y, size.x, 1, color_t::GREY32);
    gr->fillbox_wh(pos.x, pos.y, 1, size.y, color_t::GREY32);

    gr->fillbox_wh(pos.x+1, pos.y+size.y, size.x, 1, color_t::GREY224);
    gr->fillbox_wh(pos.x+size.x, pos.y+1, 1, size.y, color_t::GREY224);
    */

    // painter_t pt(gr);
    // pt.box_wh(pos, size, color_t::GREY64);

  }
}


/**
 * Implements drop_target_t
 */
bool inventory_view_t::drop(drawable_t * item, koord at)
{
  thing_drag_t *drag = dynamic_cast<thing_drag_t *>(item);
  bool ok = false;

  dbg->message("inventory_view_t::drop()", "drag=%p", drag);

  if(drag) {
    at += koord(RASTER/2-4, RASTER/2-4);
    at /= RASTER;
    ok = inv->add_at(drag->get_thing(), at);
  }

  if(ok) {
    drag->set_thing(0);
    fire_mouse_over_thing(inv->get_from(at));

    redraw();
  }

  return ok;
}


/**
 * If dragging is stopped, the item needs to be put back
 * @author Hj. Malthaner
 */
void inventory_view_t::stop_dragging(drawable_t *item)
{
  thing_drag_t *drag = dynamic_cast<thing_drag_t *>(item);

  if(drag) {
    inv->add(drag->get_thing());
    drag->set_thing(0);
  }  
}


/**
 * Process an event.
 * @return true if event was swallowed, false otherwise
 * @author Hj. Malthaner
 */
bool inventory_view_t::process_event(const event_t &ev)
{
  event_t ev2 = ev.translate(-get_pos());
  static thinghandle_t null;

  if(ev2.type == event_t::BUTTON_RELEASE) {

    // printf("%d %d\n", ev2.type, ev2.code);

    if(ev2.code == event_t::BUTTON_RIGHT &&
       inv->is_usage_allowed()) {

      const thinghandle_t thing = inv->get_from((ev2.mpos-koord(4,4))/RASTER);
      if(thing.is_bound()) {

	// Hajo: Shut down props display
	fire_mouse_over_thing(null);
	
	// Hajo: let player select an usage
	thing_usage_selector_t choice;

	thinghandle_t user = thing_t::find_root(inv->get_owner()->get_owner());

	const int delay = choice.select_and_activate(user, inv, thing);
	
	actor_t * actor = scheduler_t::get_instance()->get_current_actor();

	// Hajo: Check if still alive
	if(actor) {
	  actor->add_delay(delay);
	}

	// Hajo: Shut down props display again - in case of reactivation
	// by other events
	fire_mouse_over_thing(null);

	// Hajo: we assume something has changed and must be redrawn
	redraw();

      }
    } else {
      // Hajo: start dragging

      const thinghandle_t thing = inv->get_from((ev2.mpos-koord(4,4))/RASTER);
      if(thing.is_bound()) {

	fire_mouse_over_thing(null);
	  
	drawable_t * drag = new thing_drag_t(thing);

	dbg->message("inventory_view_t::process_event()",
		     "start dragging a %s/%s", 
		     thing->get_string("ident", "unnamed"), 
		     thing->get_string("name", "unnamed"));

	const bool ok = start_dragging(this, drag);

	if(!ok) {
	  dbg->error("inventory_view_t::process_event()",
		     "start dragging failed!"); 
	}

	inv->remove(thing);
	redraw();
      }
    }
  }
  else if(ev.type == event_t::CONTAINER) {

    if(ev.code == event_t::ENTER) {
    }
    else if(ev.code == event_t::LEAVE) {
      fire_mouse_over_thing(null);
    }

  }
  else if(is_inside(ev.mpos)) {
    fire_mouse_over_thing(inv->get_from((ev2.mpos-koord(4,4))/RASTER));
  }

  return false;
}


void inventory_view_t::fire_mouse_over_thing(const thinghandle_t &t)
{
  slist_iterator_tpl <inventory_view_listener_t *> iter (listeners);

  while( iter.next() ) {
    iter.get_current()->mouse_over_thing(t);
  }
}
