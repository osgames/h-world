/* 
 * skill_tree_view_t.cpp
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <stdio.h>

#include "skill_tree_view_t.h"
#include "control/skills_t.h"

#include "swt/font_t.h"
#include "swt/color_t.h"
#include "swt/graphics_t.h"
#include "fontdef.h"

skill_tree_view_t::skill_tree_view_t(skills_t * s) : skills(s) {
  font = font_t::load(FONT_SML);
}

 

/**
 * Draws the component
 * @author Hj. Malthaner
 */
void skill_tree_view_t::draw(graphics_t *gr, koord pos) 
{
  char buf[128];

  for(int i = 0; i<skills_t::max_skills; i++) {
    
    sprintf(buf, "%s (%d/%d)", 
	    skills->get_skill_name(i), 
	    skills->get_skill_value(i),
	    skills->get_skill_sum(i)); 

    font->draw_string(gr, buf, 
		      pos.x+8+skills->get_skill_level(i)*8, 
		      pos.y+8+i*12,
		      0, 
		      color_t::BLACK);
  }
}

