/* 
 * body_view_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "body_view_t.h"
#include "thing_hold_t.h"
#include "inventory_view_t.h"
#include "square_item_view_t.h"
#include "view/world_view_t.h"
#include "view/player_visual_connector_t.h"
#include "swt/tileset_t.h"
#include "swt/event_t.h"
#include "model/thing_t.h"
#include "model/inventory_t.h"
#include "model/thing_constants.h"
#include "util/debug_t.h"
#include "../guidef.h"

#define INV_MAX_WIDTH 96


void body_view_t::do_layout()
{
  dbg->message("body_view_t::do_layout()", "called");


  // Hajo: the display must be the top-level componet, so it must
  // be added first
  add_component(&display);

  create_body_ui(thing, koord(8,8));

  layout.do_layout();

  layout.shrink_vertical(6);

  koord lsize = layout.get_dimension();


  const koord inv_max = create_inventory_ui(thing, 
					    koord(lsize.x, 10), 
					    lsize.y - 8);
  
  if(inv_max.x+INV_MAX_WIDTH+6 > lsize.x) {
    lsize.x = inv_max.x+INV_MAX_WIDTH+6;
  }

  // enforece minimum size

  if(lsize.x < 320) {
    lsize.x = 320;
  }

  if(lsize.y < 320) {
    lsize.y = 320;
  }


  // ground view
  square_view = new square_item_view_t();
  square_view->refresh(thing->get_pos());
  square_view->set_pos(koord((short)6, lsize.y));
  square_view->set_size(koord(lsize.x-8, 96));

  square_view->add_listener(&display);


  drag_control.add_drop_target(square_view);
  add_component(square_view);

  set_size(lsize+koord(0, 102) + koord(4, 16));

  // Hajo: only width, height is auto-calculated
  display.set_size(koord(280,0));
  display.set_pos(lsize/2 - koord(140,120));


  dbg->message("body_view_t::do_layout()", "done");
}


void body_view_t::dispose()
{
  dbg->message("body_view_t::dispose()", "called");

  remove_all_components();
  layout.reset(16);

  drag_control.remove_drop_target(square_view);

  while(inventory_views.count()) {
    inventory_view_t *iv = inventory_views.remove_first();
    drag_control.remove_drop_target(iv);
    kill_list.append(iv);
  }

  while(thing_holds.count()) {
    thing_hold_t *hold = thing_holds.remove_first();
    drag_control.remove_drop_target(hold);
    kill_list.append(hold);
  }


  drag_control.remove_drop_target(square_view);
  kill_list.append(square_view);
  square_view = 0;

  dbg->message("body_view_t::dispose()", "done");
}


void body_view_t::set_model(thinghandle_t thing) {
  this->thing = thing;
}


/**
 * Refresh with contents from square at position k.
 * @author Hj. Malthaner
 */
void body_view_t::refresh(koord k) {
  square_view->refresh(k);
}


body_view_t::body_view_t(thinghandle_t &t, 
			 const char *title,
			 bool autodelete)
  : 
  escapable_frame_t(title, autodelete), 
  thing(t), 
  display(props_display_t::inspector), 
  layout(16)
{
  set_swallow_all(true);

  square_view = 0;

  set_size(koord(400,400));
  set_opaque(true);
  set_background(world_view_t::get_instance()->get_tileset(2)->get_tile(BACK_TILE));

  layout.set_border(6);

  drag_control.set_frame(this);

  do_layout();

  thing->add_listener(this);
}


body_view_t::~body_view_t()
{
  dbg->warning("body_view_t::~body_view_t()", "called");

  thing->remove_listener(this);

  dispose();

  while(kill_list.count()) {
    delete kill_list.remove_first();
  }


  // Hajo: this is only needed for monsters etc.

  if(thing->get_actor().is_animated()) {
    player_visual_connector_t conn (thing, thing->visual);
    thing->call_listeners();
    thing->remove_listener(&conn);
  }

  dbg->warning("body_view_t::~body_view_t()", "done");
}


/**
 * called before window is closed in response to a close event
 * @author Hj. Malthaner
 */
void body_view_t::on_close()
{
  drag_control.stop_dragging();
  display.mouse_over_thing(0);
  escapable_frame_t::on_close();
}



void body_view_t::create_body_ui(thinghandle_t &limb, koord ppos)
{
  if(limb.is_bound()) {
    thing_hold_t *lv = new thing_hold_t(limb);

    koord lpos;
    
    switch(limb->get_int("direction", 0)) {
    case -1:
      lpos = ppos;
      break;
    case 0:
      lpos = ppos + koord(1,0);
      break;
    case 1:
      lpos = ppos + koord(1,-1);
      break;
    case 2:
      lpos = ppos + koord(0,-1);
      break;
    case 3:
      lpos = ppos + koord(-1,-1);
      break;
    case 4:
      lpos = ppos + koord(-1,0);
      break;
    case 5:
      lpos = ppos + koord(-1,1);
      break;
    case 6:
      lpos = ppos + koord(0,1);
      break;
    case 7:
      lpos = ppos + koord(1,1);
      break;
    default:
      dbg->error("body_view_t::create_body_ui()", 
		 "Invalid limb direction %d", limb->get_int("direction", 0));
    }


    drag_control.add_drop_target(lv);
    layout.set_component(lpos, lv);
    add_component(lv);

    thing_holds.insert(lv);

    lv->add_listener(&display);


    if(limb->get_limbs().count()) {
      slist_iterator_tpl <thinghandle_t> iter (limb->get_limbs());

      while(iter.next()) {
	create_body_ui(iter.access_current(), lpos);
      }
    }
  }
}


koord body_view_t::create_inventory_ui(const thinghandle_t &limb, 
                                       koord pos,
                                       int max_y)
{
  if(limb.is_bound()) {

    const thinghandle_t & thing = limb->get_item();
    if(thing.is_bound() && thing->get_inventory()) {

      inventory_view_t * iv = new inventory_view_t();
      inventory_t * inv = thing->get_inventory();

      inv->set_usage_allowed(true);

      iv->set_model(inv);
      iv->set_title(thing->get_string(TH_NAME, "unnamed"));

      drag_control.add_drop_target(iv);

      if(pos.y + iv->get_size().y > max_y) {
	pos.y = 10;
	pos.x += INV_MAX_WIDTH;
      }

      iv->set_pos(pos + koord((INV_MAX_WIDTH-iv->get_size().x)/2, 0));

      add_component(iv);

      inventory_views.insert(iv);

      iv->add_listener(&display);

      pos.y += iv->get_size().y + 10;
    }

    if(limb->get_limbs().count()) {
      slist_iterator_tpl <thinghandle_t> iter (limb->get_limbs());

      while(iter.next()) {
	pos = create_inventory_ui(iter.get_current(), pos, max_y);
      }
    }
  }

  return pos;
}

/**
 * Implements thing_listener_t
 * @author Hj. Malthaner
 */
void body_view_t::update(const handle_tpl<thing_t> &)
{
  dbg->message("body_view_t::update()", "called");
  dispose();
  do_layout();
}


/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool body_view_t::process_event(const event_t &ev)
{
  bool swallowed = escapable_frame_t::process_event(ev);

  if((ev.type == event_t::KEY_PRESS && ev.code == 'i')) {
    swallowed |= kill_window(this);
  }

  return swallowed;
}
