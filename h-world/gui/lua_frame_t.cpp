/* 
 * lua_frame_t.cpp
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "lua_frame_t.h"
#include "hw_button_t.h"

#include "../lua_system.h"


lua_frame_t::lua_frame_t(const char * title, const char * call)
  : modal_frame_t(title, true, true)

{
  strncpy(callback, call, 63);
  callback[63] = '\0';

  bix = 0;
}


hw_button_t * lua_frame_t::add_buttonlabel(const char * label,
					   int img, int x, int y, int w, int h)
{
  char hotkey = ' ';

  if(label[1] == ':') {
    // Hajo: has a hotkey definition
    hotkey = label[0];
  }

  buttons[bix] = new hw_button_t(label, hotkey);
  buttons[bix]->set_pos(koord(x,y));
  buttons[bix]->set_size(koord(w,h));
  buttons[bix]->add_listener(this);

  add_component(buttons[bix]);

  return buttons[bix ++];
}


void lua_frame_t::action_triggered(gui_action_trigger_t * trigger)
{
  lua_State * L = lua_state();

  // the lua function name must be the usages verb
  lua_getglobal(L, callback);
  lua_pushusertag(L, trigger, LUA_ANYTAG);
  const int fail = lua_call(L, 1, 0);

  lua_diagnostics_error(fail);
}
