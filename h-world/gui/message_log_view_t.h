/* Copyright by Hj. Malthaner */

#ifndef MESSAGE_LOG_VIEW_T_H
#define MESSAGE_LOG_VIEW_T_H


#include "swt/gui_component_t.h"
class koord;

class message_log_t;
class graphics_t;
class font_t;


class message_log_view_t : public gui_component_t 
{
private:    
    message_log_t * model;
    font_t * font;

public:

    message_log_view_t();


    void set_model(message_log_t * model);


    /**
     * Draws the component
     * @author Hj. Malthaner
     */
    void draw(graphics_t *, koord);

};
#endif //MESSAGE_LOG_VIEW_T_H
