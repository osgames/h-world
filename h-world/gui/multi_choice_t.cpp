/* 
 * multi_choice_t.cpp
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "multi_choice_t.h"
#include "hw_button_t.h"

#include "swt/font_t.h"
#include "swt/event_t.h"
#include "swt/gui_label_t.h"


static int make_buttons(char *str,char **buttontexte)
{
  char  *start=str;
  int   i=1;

  while(*str!=0) {
    if(*str=='|') {
      (*buttontexte++)=start;
      *str=0;
      start=str+1;
      i++;
    }
    str++;
  }
  *buttontexte=start;
  return i;
}


multi_choice_t::multi_choice_t(const char * title, 
			       const char * msg,
			       const char * button_str) :
  modal_frame_t(title)
{
  choice = 127;

  button_texts = new char[strlen(button_str)+1];
  strcpy(button_texts, button_str);

  // Max 16 buttons supported
  char * labels [16]; 

  button_count = make_buttons(button_texts, labels);

  font_t * font = get_font();

  int width = 32;

  // find biggest button
  for(unsigned int i=0; i<button_count; i++) { 
    const int tmp = font->get_string_width(labels[i], 0) + 16; 

    if(tmp > width) {
      width = tmp;
    }
  }

  // Determine width of dialog box
  int button_width = button_count * (width + 16) - 16;
  int msg_width = font->get_string_width(msg, 0);

  const int dialog_width = 
    msg_width > button_width ? msg_width + 64 : button_width + 64;

  const int button_offset = (dialog_width - button_width)/2;

  for(unsigned int i=0; i<button_count; i++) { 

    buttons[i] = new hw_button_t(labels[i], labels[i][0]);
    buttons[i]->set_pos(koord(button_offset + i*(width+16), 48));
    buttons[i]->set_size(koord(width, 24));
    buttons[i]->set_font(get_font());

    buttons[i]->add_listener(this);

    add_component(buttons[i]);
  }
  
  msg_label = new gui_label_t(msg);
  msg_label->set_pos(koord(16,24));
  msg_label->set_size(koord(msg_width, 32));
  msg_label->set_font(get_font());

  add_component(msg_label);

  set_size(koord(dialog_width, 104)); 
}


multi_choice_t::~multi_choice_t()
{
  delete msg_label;
  msg_label = 0;

  for(unsigned int i=0; i<button_count; i++) { 
    delete buttons[i];
    buttons[i] = 0;
  }

  delete [] button_texts;
  button_texts = 0;
}


void multi_choice_t::action_triggered(gui_action_trigger_t * trigger)
{
  hw_button_t * button = dynamic_cast <hw_button_t *> (trigger);

  choice = button->get_text()[0];

  on_close();
  close();
}


/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool multi_choice_t::process_event(const event_t &ev)
{
  bool swallowed = false;

  /*
  if(ev.type == event_t::KEY_PRESS) {

    for(unsigned int i=0; i<button_count; i++) { 
      if(ev.code == buttons[i]->get_text()[0]) {
	swallowed = true;
	choice = ev.code;

	on_close();
	close();
	break;
      }
    }
  }
  */

  if(!swallowed) {
    modal_frame_t::process_event(ev);
  }

  return swallowed;
}
