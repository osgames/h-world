/* 
 * gui_utils.h
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef gui_utils_h
#define gui_utils_h

void ddd_frame(graphics_t *gr, 
	       koord pos, koord size,
	       int border,
	       color_t bright, color_t dark);


/**
 * Get the width of a tiled border (checks the actual tile size)
 * @author Hj. Malthaner
 */
int get_tileborder_width();


void tileborderframe(graphics_t *gr, 
		     koord pos, koord size);

#endif // gui_utils.h_h
