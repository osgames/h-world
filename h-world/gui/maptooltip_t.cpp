/* 
 * maptooltip_t.cpp
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


// #include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "maptooltip_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"

#include "fontdef.h"


/**
 * Calculate drawable size from text and font
 * @author Hj. Malthaner
 */
void maptooltip_t::recalc_size()
{
  int ttw = font->get_string_width(tt_title, 0)+4;
  int tth = font->get_height()+2;

  for(int i=0; i<16; i++) { 
    line[i] = 0;
  }

  if(*tt_text != '\0') {
    ttw = ttw < 140 ? 140 : ttw;

    // Hajo: calculate lines
    char * start = tt_text;
    int i = 0;
    line[i++] = start;

    while(i < 16 && start < tt_text+1024) {
      char * space = strchr(start, ' ');

      // Hajo: no more words?
      if(space == 0) {
	break;
      }

      *space = '\0';

      // printf("%s\n", line[i-1]);

      if(tf->get_string_width(line[i-1], 0) >= ttw-4) {
	// Hajo: new line needed, step back one word
	// and start new line
	char * p = strrchr(line[i-1], ' ');
	*p++ = '\0';
	line[i++] = p;
	start = p;

	*space = ' ';

	// printf("new line: %s\n", line[i-1]);

      } else {
	*space = ' ';
	start = space + 1;
      }
    }
    tth += i*(tf->get_height()+1) + 3; 
  }

  set_size(koord(ttw, tth));
}


void maptooltip_t::set_text(const char * title, const char * text, color_t & c)
{
  strncpy(tt_title, title, 128);
  tt_title[127] = '\0';

  strncpy(tt_text, text, 1024);
  tt_text[1022] = '\0';
  if(*tt_text != '\0') {
    // Hajo: line breaker needs a trailing space 
    strcat(tt_text, " ");
  }

  tt_title[0] = toupper(tt_title[0]);
  tt_text[0] = toupper(tt_text[0]);

  color = c;
  recalc_size();
}


maptooltip_t::maptooltip_t() : color(color_t::WHITE)
{
  tt_text[0] = '\0';

  font = font_t::load(FONT_STD);
  tf = font_t::load(FONT_SML);
}


maptooltip_t::~maptooltip_t()
{
  // Hajo: nothing to do here
}


void maptooltip_t::draw(graphics_t *gr, koord pos)
{
  const koord size = get_size();

  gr->fillbox_wh(pos.x, pos.y, size.x, size.y, color_t::BLACK);
  gr->fillbox_wh(pos.x+1, pos.y+1, size.x-2, size.y-2, color_t::GREY64);

  font->draw_string(gr, tt_title, pos.x+2, pos.y+1, 0, color);

  int yoff = font->get_height()+4;

  int i=0;
  while(line[i]) {
    tf->draw_string(gr, line[i++], pos.x+2, pos.y+yoff, 0, color);
    yoff += tf->get_height()+1;
  }
}
