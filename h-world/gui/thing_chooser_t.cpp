/* 
 * thing_chooser_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "hw_button_t.h"

#include "swt/event_t.h"
#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"

#include "util/debug_t.h"

#include "thing_chooser_t.h"
#include "model/thing_t.h"
#include "model/thing_tree_t.h"

#include "fontdef.h"

#ifndef tpl_slist_tpl_h
#include "tpl/slist_tpl.h"
#endif


static int get_max_width(font_t * font, 
			 const slist_tpl <thinghandle_t> * list)
{
  int w_max = 0;
  slist_iterator_tpl <thinghandle_t> iter (list);
  char  t_ident[1024];
  
  while(iter.next()) {
    iter.get_current()->get_ident(t_ident);

    const int w = font->get_string_width(t_ident, 0);

    if(w > w_max) {
      w_max = w;
    }
  }
  
  return w_max;
}


thinghandle_t thing_chooser_t::get_choice()
{
  return choice;
}



/**
 * Lets the player choose one thing from the tree
 *
 * @autor Hj. Malthaner
 */
thing_chooser_t::thing_chooser_t(const char * title,
				 thinghandle_t model,
				 const char *ident,
				 const char *type,
				 const char *subtype) : modal_frame_t(title)
{
  packs = new slist_tpl <thinghandle_t>;
  items = new slist_tpl <thinghandle_t>;
  buttons = new slist_tpl <hw_button_t *>;


  thing_tree_t tree;

  const int ROW = 18;

  dbg->message("thing_chooser_t::thing_chooser_t()",
	       "ident=%s type=%s subtype=%s", 
	       ident ? ident : "<null>", 
	       type ? type : "<null>", 
	       subtype ? subtype : "<null>");    

  // true means to list inventory contents, too
  tree.list_inventories(model, packs, true);
  tree.list_items(model, items);


  slist_tpl <thinghandle_t> * tmp;

  tmp = tree.filter_list(packs, ident, type, subtype);
  delete packs;
  packs = tmp;


  tmp = tree.filter_list(items, ident, type, subtype);
  delete items;
  items = tmp;


  font = font_t::load(FONT_STD);

  set_color(color_t::GREY224);

  const int r = packs->count() > items->count() ? packs->count() : items->count();

  w_left = get_max_width(font, items);
  w_right = get_max_width(font, packs);

  if(packs->count() > ROW) {
    set_size(koord(58+w_left+58+w_right+32 + w_right+48, ROW*30+20+8));
  } else {
    set_size(koord(58+w_left+58+w_right+32, r*30+20+8));
  }


  slist_iterator_tpl <thinghandle_t> i_iter (items);
  
  int i = 0;
  char buf [256];

  while( i_iter.next() ) {
    thinghandle_t & thing = i_iter.access_current();

    char iident [256];

    thing->get_ident(iident);
    sprintf(buf, "%c: %s", 'a' + i, iident);

    // printf("%c: %s\n", 'a'+i, buf);
      
    hw_button_t * but = new hw_button_t(buf, 'a' + i);
    
    but->text_offset = koord(4, 8);
    but->image_offset = koord(-44, -90);
    but->set_image(thing->visual);
    but->set_pos(koord(16, 30*i+8));
    but->set_size(koord(w_left+48, 30));
    
    but->payload = &thing;
    
    but->add_listener(this);
    
    buttons->append(but);
    
    add_component(but);
    
    i++;
  }
  

  i = 0;
  slist_iterator_tpl <thinghandle_t> p_iter (packs);

  while(p_iter.next()) {
    thinghandle_t & thing = p_iter.access_current();

    // Hajo: don't display the packs again, item list
    // already has them -> but display packs inside packs!

    if(thing->get_inventory() == 0 || 
       thing->get_owner().is_bound() == false) {

      char iident [256];

      thing->get_ident(iident);
      sprintf(buf, "%c: %s", 'A' + i, iident);
      
      hw_button_t * but = new hw_button_t(buf, 'A' + i);

      but->text_offset = koord(4, 8);
      but->image_offset = koord(-44, -90);
      but->set_image(thing->visual);
      but->set_pos(koord(w_left+56+16 + ((i/ROW) * (w_right+56)), 30*(i%ROW)+8));
      but->set_size(koord(w_right+48, 30));
      
      but->payload = &thing;

      but->add_listener(this);

      buttons->append(but);

      add_component(but);

      i++;
    }
  }
}


thing_chooser_t::~thing_chooser_t()
{
  slist_iterator_tpl < hw_button_t * > iter (buttons);

  while(iter.next()) {  
    delete iter.access_current();
  }

  delete packs;
  delete items;
  delete buttons;
}


void thing_chooser_t::action_triggered(gui_action_trigger_t * trigger)
{
  dbg->message("thing_chooser_t::action_triggered()", "called");

  choice = * ((thinghandle_t *) ((hw_button_t *)trigger)->payload);

  dbg->message("thing_chooser_t::action_triggered()", 
	       "choice is %s", choice->get_string("birth_name", "unnamed"));

  on_close();
  close();
}
