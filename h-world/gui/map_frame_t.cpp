
#include "model/world_t.h"
#include "model/level_t.h"
#include "map_frame_t.h"

#include "swt/event_t.h"

map_frame_t::map_frame_t() : escapable_frame_t("Map", false)
{
  const koord size = world_t::get_instance()->get_level()->get_size()*2;

  view.set_size(size);
  view.set_pos(koord(4,0));
  set_size(size+koord(8,20));
  set_opaque(false);

  add_component(&view);
}


void map_frame_t::draw(graphics_t *gr, koord pos)
{
  level_t * level = world_t::get_instance()->get_level();

  if(level) {
    const koord size = level->get_size()*2;
    
    if(size != view.get_size()) {
      view.set_size(size);
      set_size(size+koord(8,20));
    }
  }

  escapable_frame_t::draw(gr, pos);
}


/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool map_frame_t::process_event(const event_t &ev)
{
  bool swallowed = escapable_frame_t::process_event(ev);
  const koord pos = get_pos();
  const koord size = get_size();

  // printf("event type=%d code=%d\n", ev.type, ev.code); 
  // printf("event x=%d y=%d\n", ev.mpos.x, ev.mpos.y); 

  if(ev.type == event_t::BUTTON_RELEASE && 
     ev.mpos.x >= pos.x && ev.mpos.y >= pos.y &&
     ev.mpos.x < pos.x+size.x && ev.mpos.y < pos.y+size.y) {

    swallowed = true;
  }

  return swallowed;
}
