/* 
 * props_display_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <ctype.h>

#include "props_display_t.h"

#include "swt/color_t.h"
#include "swt/painter_t.h"
#include "swt/graphics_t.h"

#include "model/thing_t.h"
#include "util/property_t.h"
#include "util/config_file_t.h"
#include "util/debug_t.h"
#include "util/properties_t.h"
#include "primitives/cstring_t.h"

#include "swt/font_t.h"
#include "swt/painter_t.h"
#include "fontdef.h"

#include "tpl/slist_tpl.h"
#include "tpl/stringhashtable_tpl.h"


static char * filenames [2] = {
  "data/inspector.tab",
  "data/player.tab"
};

class prop_info_t {
public:
  cstring_t key;
  signed char scale;
  char op;

  prop_info_t() {
    scale = 0;
    op = ' ';
  }

  prop_info_t(const char * k, signed char s, char o) : key(k) {
    scale = s;
    op = o;
  }
};


static slist_tpl <prop_info_t> prop_keys[2];

static stringhashtable_tpl <const char *> prop_descs;
static bool props_descs_init = false;

static const char * path = "";


static void assemble_filename(char *buf, const char *filename)
{
  if(path) {
    strcpy(buf, path);
    strcat(buf, "/");
  } else {
    strcpy(buf, "./");
  }

  strcat(buf, filename);
}


void props_display_t::set_path(const char *p)
{
  path = p;
}


void props_display_t::init(enum tables which, 
			   const char *fontname, bool prop, int fw, int fh,
			   bool boxless)
{
  char namebuf[1024];

  font = font_t::load(fontname, prop, fw, fh);

  props = 0;

  table = which;
  this->boxless = boxless;

  if(prop_keys[which].count() == 0) {
    config_file_t file;

    assemble_filename(namebuf, filenames[which]);

    if(file.open(namebuf)) {
      do {
	char buf[256];
      
	file.read_line(buf, 255);
      
	if(!file.eof()) {
	  char op = ' ';
	  int  scale = 1;
	  char *p = strchr(buf, '#');

	  // Hajo: look for operator
	  if(p) {
	    *p++ = '\0';

	    if(*p == '/') {
	      op = *p;
	      sscanf(p+1, "%d", &scale);
	    } else {
	      dbg->error("props_display_t::init()", 
			 "Invalid operator '%c' found in '%s'", *p, buf); 
	    }
	  }

	  dbg->debug("props_display_t::init()", 
		     "buf='%s' op='%c' scale=%d", buf, op, scale); 

	  prop_keys[which].append(prop_info_t(buf, scale, op));
	}


      } while(!file.eof());

      file.close();

    } else {
      dbg->error("props_display_t::init()", 
		 "Could not read '%s'", namebuf); 
    }
  }

  if(props_descs_init == false) {
    config_file_t file;

    props_descs_init = true;

    assemble_filename(namebuf, "data/prop_descs.tab");

    if(file.open(namebuf)) {
      do {
	char buf1[256];
	char buf2[256];
      
	file.read_line(buf1, 255);
	file.read_line(buf2, 255);
      
	if(!file.eof()) {
	  prop_descs.put(strdup(buf1), strdup(buf2));
	}

      } while(!file.eof());

      file.close();

    } else {
      dbg->error("props_display_t::props_display_t()", "Could not read data/prop_descs.tab"); 
    }
  }
}


props_display_t::props_display_t(enum tables  which)
{
  init(which, FONT_SML, false);
  ident[0] = '\0';
}


props_display_t::props_display_t(enum tables which, 
				 const char *fontname, bool prop, 
				 int fw, int fh,
				 bool boxless)
{
  init(which, fontname, prop, fw, fh, boxless);
  ident[0] = '\0';
}



/**
 * Implements inventory_view_listener_t
 * @author Hj. Malthaner
 */
void props_display_t::mouse_over_thing(const handle_tpl<thing_t> &thing)
{
  if(thing.is_bound()) {

    const properties_t * other = thing->get_properties();

    if(props != other) {
      props = other;

      thing->get_ident(ident);

      ident[0] = toupper(ident[0]);
      
      // dbg->message("props_display_t::mouse_over_thing()", "props=%p", props); 

      if(dbg->get_level() == log_t::DEBUG) {
	thing->dump();
      }
      
      koord k = get_size();
      k.y = calc_height();
      set_size(k);
      
      redraw();
    }
  } else {

    if(props) {
      props = 0;
      redraw();
    }

  }
}


int props_display_t::calc_height()
{
  int yoff = 42;

  if(props) {

    const int linespace = font->get_height() + 2;

    slist_iterator_tpl <prop_info_t> iter (prop_keys[table]);

    while(iter.next()) {
      const int count = iter.get_current().key.char_at(0) - '0';
      iter.next();
      bool ok = true;

      for(int i=0; i<count; i++) {
	iter.next();

	const char * key = iter.get_current().key.chars();
	const property_t * p = props->get(*key == '$' ? key+1 : key);

	if(p) {
	  // do nothing
	} else {
	  ok = false;
	}
      }

      if(ok) {
	yoff += linespace;
      }
    }
  }

  return yoff;
}


/**
 * Paints the component
 * @author Hj. Malthaner
 */
void props_display_t::draw(graphics_t *gr, koord screenpos)
{
  // dbg->message("props_display_t::draw", "ident = %s", ident);

  if(props) {
    const int linespace = font->get_height() + 2;

    const koord size = get_size();
    const koord pos = get_pos()+screenpos;

    const bool unknown = thing_t::is_unknown(props);
    painter_t pt (gr);
    
    if(boxless == false) {
      painter_t pt (gr);

      pt.box_wh(pos, size, color_t::GREY64);
      gr->fillbox_wh(pos.x+1, pos.y+1, size.x-2, size.y-2, color_t::GREY224);
    }


    unsigned long arg[5];

    if(font->get_string_width(ident, 1) > size.x-20) {
      pt.draw_string_bold(font, ident, pos + koord(10, 10), 0, 
			  color_t::BLACK, color_t::GREY64);
    } else {
      pt.draw_string_bold(font, ident, pos + koord(10, 10), 1, 
			  color_t::BLACK, color_t::GREY64);
    }

    int yoff = 0;

    slist_iterator_tpl <prop_info_t> iter (prop_keys[table]);

    while(iter.next()) {
      const int count = iter.get_current().key.char_at(0) - '0';
      iter.next();

      const char * format = iter.get_current().key.chars();
      bool ok = true;
      int offset = 0;

      if(*format == '!') {
	// Hajo: do not display '!' prefix
	offset = 1;
      }

      // dbg->message("props_display_t::draw()", "count=%d, format='%s'", count, format); 

      for(int i=0; i<count; i++) {
	iter.next();

	const char * key = iter.get_current().key.chars();
	const property_t * p = props->get(*key == '$' ? key+1 : key);

	if(p) {
	  if(p->get_string()) {
	    const char * txt = p->get_string();

	    if(*key == '$') {
	      const char * cs = prop_descs.get(txt);
	      arg[i] = (unsigned long) (cs ? cs : txt); 
	    } else {
	      arg[i] = (unsigned long) txt;
	    }
	  } 
	  else {
	    arg[i] = *p->get_int();

	    if(iter.get_current().op == '/') {
	      arg[i] /= iter.get_current().scale;
	    }
	  }
	} else {
	  ok = false;
	}

	// dbg->message("props_display_t::draw()", "  arg[%d] = %lu", i, arg[i]); 
      }

      if(ok && (offset == 0 || !unknown)) {
	char buf[1024];

	sprintf(buf, format+offset, 
		arg[0], arg[1], arg[2], arg[3], arg[4], arg[5]);
	
	if(*buf == '"') {
	  pt.draw_string_bold(font, buf+1, pos + koord(10, 30+yoff), 1,
			color_t::BLACK, color_t::GREY64);
	} else {
	  font->draw_string(gr, buf, pos.x+16, pos.y+30+yoff, 0, color_t::BLACK);
	}
	yoff += linespace;
      }
    }

    // debugging
    // thing->visual.dump();
  }
}
