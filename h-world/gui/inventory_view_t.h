/* Copyright by Hj. Malthaner */

#ifndef INVENTORY_VIEW_T_H
#define INVENTORY_VIEW_T_H

#include "swt/drop_source_t.h"
#include "swt/drop_target_t.h"

class font_t;
class koord;
class inventory_t;
class thing_t;
class inventory_view_listener_t;
template <class T> class slist_tpl;
template <class T> class handle_tpl;

class inventory_view_t : public drop_target_t, drop_source_t 
{
private:    
  inventory_t * inv;

  slist_tpl <inventory_view_listener_t *> * listeners;

  void fire_mouse_over_thing(const handle_tpl <thing_t> &t);

  const char * title;
  font_t * font;

public:

  void set_title(const char * t) {title = t;};

  void add_listener(inventory_view_listener_t *l);


  void set_model(inventory_t *inv);
  inventory_t * get_model() const {return inv;};


  inventory_view_t();
  virtual ~inventory_view_t();


  void draw(graphics_t *gr, koord pos);


  /**
   * Implements drop_target_t
   * @author Hansj�rg Malthaner
   */
  virtual bool drop(drawable_t * item, koord at);


  /**
   * called if item is dragged over the drop target, but not yet dropped
   * @author Hj. Malthaner
   */
  virtual void hover(drawable_t * item, koord at) {};


  /**
   * If dragging is stopped, the item needs to be put back
   * @author Hj. Malthaner
   */
  virtual void stop_dragging(drawable_t *item);

  
  /**
   * Process an event.
   * @return true if event was swallowed, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t &);
};
#endif //INVENTORY_VIEW_T_H
