/* 
 * gui_utils.cpp
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "view/world_view_t.h"

#include "swt/graphics_t.h"
#include "swt/painter_t.h"
#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/tileset_t.h"
#include "swt/tile_descriptor_t.h"

#include "gui_utils.h"


void ddd_frame(graphics_t *gr, 
	       koord pos, koord size,
	       int border,
	       color_t bright, color_t dark)
{
  painter_t pt (gr);

  for(int i=0; i<border; i++) {
    pt.ddd_box_wh(pos+koord(i,i), size-koord(i*2, i*2), bright, dark);
  }
}


/**
 * Get the width of a tiled border (checks the actual tile size)
 * @author Hj. Malthaner
 */
int get_tileborder_width()
{
  const tileset_t * tileset = world_view_t::get_instance()->get_tileset(2);
  const tile_descriptor_t * tobo = tileset->get_tile(14);

  return tobo->h;
}


void tileborderframe(graphics_t *gr, 
		     koord pos, koord size)
{
  const tileset_t * tileset = world_view_t::get_instance()->get_tileset(2);

  const tile_descriptor_t * tobo = tileset->get_tile(14);
  const tile_descriptor_t * bobo = tileset->get_tile(15);

  const tile_descriptor_t * lebo = tileset->get_tile(16);
  const tile_descriptor_t * ribo = tileset->get_tile(17);

  for(int i=0; i<size.y; i+=16) { 
    gr->draw_tile_shaded(lebo, pos.x, pos.y+i, 32);
    gr->draw_tile_shaded(ribo, pos.x+size.x - ribo->x - ribo->w, pos.y+i, 32);
  }

  for(int i=0; i<size.x; i+=16) { 
    gr->draw_tile_shaded(tobo, pos.x+i, pos.y, 32);
    gr->draw_tile_shaded(bobo, pos.x+i, pos.y+size.y - bobo->y - bobo->h, 32);
  }

  const tile_descriptor_t * upleft = tileset->get_tile(10);
  const tile_descriptor_t * upright = tileset->get_tile(11);
  
  gr->draw_tile_shaded(upleft, pos.x, pos.y, 32);
  gr->draw_tile_shaded(upright, pos.x + size.x - upright->w - upright->x, 
		       pos.y, 32);

  const tile_descriptor_t * loleft = tileset->get_tile(12);
  const tile_descriptor_t * loright = tileset->get_tile(13);

  gr->draw_tile_shaded(loleft, pos.x, 
		       pos.y+size.y - loleft->h - loleft->y, 32);
  gr->draw_tile_shaded(loright, pos.x + size.x - loright->w - loright->x, 
		       pos.y+size.y - loright->h - loright->y, 32);
}
