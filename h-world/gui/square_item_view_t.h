#ifndef square_item_view_t_h
#define square_item_view_t_h

#include "swt/drop_source_t.h"
#include "swt/drop_target_t.h"

class font_t;
class thing_t;
class inventory_view_listener_t;

template <class T> class minivec_tpl;
template <class T> class handle_tpl;

/**
 * A class to display a list of items from a ground square
 * @author Hj. Malthaner
 */
class square_item_view_t : public drop_target_t, drop_source_t
{
 private:
  font_t *font;

  const minivec_tpl < handle_tpl <thing_t> > *list;

  koord  location;

  slist_tpl <inventory_view_listener_t *> * listeners;

  handle_tpl < thing_t > square_item_view_t::find_thing(koord cpos);

  void fire_mouse_over_thing(const handle_tpl < thing_t > &thing);

 public:


  void add_listener(inventory_view_listener_t *l);


  square_item_view_t();

  ~square_item_view_t();


  /**
   * Refresh with contents from square at position k.
   * @author Hj. Malthaner
   */
  void refresh(koord k);


  /**
   * Paints the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);


  /**
   * Implements drop_target_t
   * @author Hansj�rg Malthaner
   */
  virtual bool drop(drawable_t * item, koord at);


  /**
   * called if item is dragged over the drop target, but not yet dropped
   * @author Hj. Malthaner
   */
  virtual void hover(drawable_t * item, koord at) {};


  /**
   * If dragging is stopped, the item needs to be put back
   * @author Hj. Malthaner
   */
  virtual void stop_dragging(drawable_t *item);


  /**
   * Process an event.
   * @return true if event was swallowed, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t &);

};

#endif // square_item_view_t_h
