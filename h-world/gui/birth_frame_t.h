/* 
 * birth_frame_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef birth_frame_t_h
#define birth_frame_t_h

#include "modal_frame_t.h"

#include "swt/ifc/action_listener_t.h"

#include "model/thing_t.h"
#include "model/thinghandle_t.h"


template <class T> class minivec_tpl;
class hw_button_t;

class birth_frame_t : public modal_frame_t, action_listener_t
{
 private:

  minivec_tpl < handle_tpl<thing_t> > * players; 
  minivec_tpl < hw_button_t * > * buttons; 

  koord positions [32];

 public:

  birth_frame_t ();
  ~birth_frame_t ();


  thinghandle_t choice;


  /**
   * Adapt size to content
   * @author Hj. Malthaner
   */
  void autoresize();


  /**
   * Draws the window frame and all components
   * 
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);   


  virtual void action_triggered(gui_action_trigger_t * trigger);

};

#endif // birth_frame_t_h
