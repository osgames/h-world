#include "limb_view_t.h"
#include "model/limb_t.h"
#include "model/thing_t.h"
#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"
class koord;


limb_view_t::limb_view_t()
{
  font = font_t::load("data/hajo7x10bold.hex", true, 7, 10);
}

void limb_view_t::set_model(limb_t *limb) 
{
  this->limb=limb;

  hold.set_model(limb);
  hold.set_pos(koord(8,16));
  add_component(&hold);

  if(limb->get_thing() && limb->get_thing()->get_inventory()) {
    inv.set_model(limb->get_thing()->get_inventory());
    inv.set_pos(koord(hold.get_pos().x + hold.get_size().x + 8, 16) );
    add_component(&inv);

    
    this->set_size(koord(hold.get_pos().x + hold.get_size().x + 8 + inv.get_size().x + 8, 
			 (hold.get_size().y > inv.get_size().y ? hold.get_size().y : inv.get_size().y) + 24));
  } else {
    inv.set_model(NULL);
    remove_component(&inv);


    this->set_size(koord(hold.get_pos().x + hold.get_size().x + 8, 
			  hold.get_size().y + 24));
  }
}




void limb_view_t::draw(graphics_t *gr, koord screenpos)
{
  const koord size = get_size();
  const koord pos = get_pos()+screenpos;

  gr->fillbox_wh(pos.x, pos.y, size.x, 1, color_t::GREY224);
  gr->fillbox_wh(pos.x, pos.y, 1, size.y, color_t::GREY224);

  gr->fillbox_wh(pos.x+1, pos.y+size.y, size.x, 1, color_t::GREY32);
  gr->fillbox_wh(pos.x+size.x, pos.y+1, 1, size.y, color_t::GREY32);

  gr->fillbox_wh(pos.x+1, pos.y+1, size.x-1, size.y-1, color_t::GREY128);

  gr->set_font(font);
  gr->draw_string(limb->get_name(), pos.x+2, pos.y+2, color_t::WHITE);

  gui_container_t::draw(gr, screenpos);
}
