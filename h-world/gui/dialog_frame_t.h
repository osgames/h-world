#ifndef dialog_frame_t_h
#define dialog_frame_t_h

#include "escapable_frame_t.h"
#include "dialog_change_listener_t.h" 

class dialog_view_t;
class thing_t;
template <class T> class handle_tpl;


class dialog_frame_t : public escapable_frame_t, dialog_change_listener_t
{
 private:

  dialog_view_t * view;

 public:

  dialog_frame_t(const char * filename,
		 bool autodelete,
		 handle_tpl < thing_t > &talker,
		 handle_tpl < thing_t > &customer);


  virtual ~dialog_frame_t();


  /**
   * Called if a new dialog is displayed
   * @author Hj. Malthaner
   */
  virtual void on_dialog_changed(const char *new_dialog);

};

#endif // dialog_frame_t_h
