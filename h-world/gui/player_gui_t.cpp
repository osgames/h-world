#include "player_gui_t.h"

#include "model/thinghandle_t.h"

#include "stats_panel_t.h"
#include "usage_panel_t.h"


player_gui_t * player_gui_t::single_instance = 0;

player_gui_t * player_gui_t::get_instance()
{
  if(single_instance == 0) {
    single_instance = new player_gui_t();
  }

  return single_instance;
}


player_gui_t::player_gui_t()
{
  stats_panel = 0;
  usage_panel = 0;
}


void player_gui_t::init(stats_panel_t *s, usage_panel_t *u)
{
  stats_panel = s;
  usage_panel = u;
}


void player_gui_t::link(const handle_tpl <thing_t> &player)
{
  usage_panel->link(player);
  stats_panel->link(player);
}
