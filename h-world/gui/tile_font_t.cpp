/* 
 * tile_font_t.cpp
 *
 * Copyright (c) 2005 - 2005 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "tile_font_t.h"
#include "../view/visual_t.h"

#include "util/config_file_t.h"
#include "util/debug_t.h"

#include "swt/graphics_t.h"
#include "swt/tileset_t.h"
#include "swt/tile_descriptor_t.h"



int tile_font_t::get_height()
{
  return ascend + descend;
}


/**
 * Load glyph tileset from file
 * @autor Hj. Malthaner 
 */
void tile_font_t::load(const char * filename)
{
  glyphs = new tileset_t();
  glyphs->load(filename, 32);

  char buf[1024];
  strcpy(buf, filename);
  strcat(buf, ".kern");

  config_file_t kern;

  if(kern.open(buf)) {

    kern.read_line(buf, 256);
    sscanf(buf, "%d %d", &ascend, &descend);

    while(!kern.eof()) {
      kern.read_line(buf, 256);
      int c;
      int l,r;

      c = *(unsigned char *)buf;

      sscanf(buf+1, " %d %d", &l, &r);

      kern_left[c] = l;
      kern_right[c] = r;
    }
    
  } else {
    dbg->error("tile_font_t::load()", "Can't read '%s'", buf);
  }
}


tile_font_t::tile_font_t()
{
  glyphs = 0;

  for(int i=0; i<256; i++) { 
    kern_left[i] = 0;
    kern_right[i] = 0;
  }
}


tile_font_t::~tile_font_t()
{
  delete glyphs;
}


/**
 * Determines the width of a string written in this font
 * @autor Hj. Malthaner 
 */
int tile_font_t::get_string_width(const char * text, int spacing) const
{
  int w = 0;

  while(*text !=  '\0') {
    unsigned char c = *(unsigned char *)text;
    const tile_descriptor_t * glyph = glyphs->get_tile(c);


    if(glyph->w == 0) {
      w += kern_left[c] + kern_right[c] + spacing;
    } else {
      w += glyph->w + kern_left[c] + kern_right[c] + spacing;
    }

    text++;
  }

  return w;
}


void tile_font_t::draw_string(graphics_t * gr, const char * text, 
			      int x, int y, int spacing)
{
  const int * color_tab = visual_t::get_colorshades();

  while(*text !=  '\0') {
    unsigned char c = *(unsigned char *)text;
    const tile_descriptor_t * glyph = glyphs->get_tile(c);

    gr->draw_tile_recolored(glyph, 
			    4+x-glyph->x+kern_left[c], y, 28,
			    color_tab + 2*8);

    if(glyph->w == 0) {
      x += kern_left[c] + kern_right[c] + spacing;
    } else {
      x += glyph->w + kern_left[c] + kern_right[c] + spacing;
    }

    text++;
  }
}
