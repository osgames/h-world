/* Copyright by Hj. Malthaner */

#ifndef LIMB_VIEW_T_H
#define LIMB_VIEW_T_H
#include "swt/gui_container_t.h"
#include "inventory_view_t.h"
#include "thing_hold_t.h"
class koord;

class limb_t;
class font_t;

class limb_view_t : public gui_container_t {
private:    
    limb_t * limb;
    inventory_view_t inv;
    thing_hold_t hold;

    font_t *font;

public:
    
    thing_hold_t * get_thing_hold() {return &hold;};
    inventory_view_t * get_inventory_view() {return &inv;};

    limb_view_t();

    void set_model(limb_t *limb);

    void draw(graphics_t *gr, koord pos);
};
#endif //LIMB_VIEW_T_H
