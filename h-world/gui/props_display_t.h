#ifndef props_display_t_h
#define props_display_t_h

#include "ifc/inventory_view_listener_t.h"
#include "swt/gui_component_t.h"
#include "model/thinghandle_t.h"
#include "tpl/handle_tpl.h"


class font_t;
class properties_t;

class props_display_t : public gui_component_t, 
			public inventory_view_listener_t
{
 public:

  enum tables {inspector=0, stats=1};

  static void set_path(const char *path);

 private:

  /**
   * Properties of inspected thing
   * @author Hj. Malthaner
   */
  const properties_t *props;


  font_t *font;


  enum tables table;


  /**
   * Draw a background box ?
   * @author Hj. Malthaner
   */
  bool boxless;


  /**
   * Ident of inspected thing
   * @author Hj. Malthaner
   */
  char ident[1024];



  void init(enum tables which, 
	    const char *fontname, bool prop, int fw, int fh,
	    bool boxless);

  int calc_height();


 public:

  props_display_t(enum tables which);
  props_display_t(enum tables which, 
		  const char *fontname, bool prop, int fw, int fh,
		  bool boxless);


  /**
   * Implements inventory_view_listener_t
   * @author Hj. Malthaner
   */
  virtual void mouse_over_thing(const handle_tpl <thing_t> &thing);


  /**
   * Paints the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);

};

#endif // props_display_t_h
