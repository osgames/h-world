/* 
 * multi_choice_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef multi_choice_t_h
#define multi_choice_t_h

#ifndef modal_frame_t_h
#include "modal_frame_t.h"
#endif

#ifndef ACTION_LISTENER_T_H
#include "swt/ifc/action_listener_t.h"
#endif

class hw_button_t;
class gui_label_t;

/**
 * A box with a message text and several buttons
 * @author Hj. Malthaner
 */
class multi_choice_t : public modal_frame_t, action_listener_t
{
 private:

  /**
   * Global buffer for all button texts
   * @author Hj. Malthaner
   */
  char * button_texts;


  /**
   * Actual number of buttons
   * @author Hj. Malthaner
   */
  unsigned int button_count;

  // Max. 16 buttons
  hw_button_t * buttons[16];
  
  gui_label_t * msg_label;

  char choice;

 public:

  char get_choice() const {return choice;};


  multi_choice_t(const char * title,
		 const char * msg,
		 const char * buttons);

  virtual ~multi_choice_t();


  void action_triggered(gui_action_trigger_t * trigger);


  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t &ev);
};

#endif // multi_choice_t_h
