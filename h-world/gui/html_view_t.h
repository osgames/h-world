#ifndef html_view_t_h
#define html_view_t_h

#include "swt/gui_component_t.h"
#include "primitives/cstring_t.h"

#include "hyperlink_listener_t.h"

class font_t;
class tile_font_t; 

/**
 * A simple html viewer. It does not use any templates on purpose!
 * @author Hj. Malthaner
 */
class html_view_t : public gui_component_t
{
private:

  font_t * font;
  // tile_font_t * font;


    enum attributes {
	ATT_NONE, ATT_NEWLINE, 
	ATT_A_START, ATT_A_END,
	ATT_H1_START, ATT_H1_END,
	ATT_EM_START, ATT_EM_END,
	ATT_STRONG_START, ATT_STRONG_END,
	ATT_UNKNOWN};

    class node_t {
    public:

       cstring_t text;
       int att;

       node_t * next;

       node_t(const cstring_t &text, int att);
    };


    /**
     * Hyperlink position container
     * @author Hj. Malthaner
     */
    class hyperlink_t {
    public:
      koord tl;     // top left display position
      koord br;     // bottom right display position
      cstring_t param;
      hyperlink_t * next;

      hyperlink_t() {next = 0;};
    };


    /**
     * Node for the list of listeners
     * @author Hj. Malthaner
     */
    class listener_t {
    public:
      hyperlink_listener_t *callback;
      listener_t *next;
      listener_t() {next = 0;};
    };

    node_t      * nodes;
    hyperlink_t * links;
    listener_t  * listeners;



    char title[128];

    void destroy_nodes();

 protected:

    koord output(graphics_t *gr, koord pos, bool doit) const;
  
public:
                     
    html_view_t();

    ~html_view_t();

   
    /**
     * Sets the text to display.
     * @author Hj. Malthaner
     */
    void set_text(const char *text);

    const char * get_title() const;

    koord get_preferred_size() const;


    void add_listener(hyperlink_listener_t *callback);
    

  /**
   * Paints the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);


  /**
   * Process an event.
   * @return true if event was swallowed, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t &);
};

#endif // html_view_t_h
