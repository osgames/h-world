/* 
 * dialog_view_t.h
 *
 * Copyright (c) 2001 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef dialog_view_t_h
#define dialog_view_t_h

#include "swt/gui_container_t.h"

#ifndef ACTION_LISTENER_T_H
#include "swt/ifc/action_listener_t.h"
#endif

class sections_t;
class dialog_change_listener_t;
class thing_t;
class hw_button_t;
class html_view_t;

template <class T> class handle_tpl;

class dialog_view_t : public gui_container_t, action_listener_t
{
 private:

  enum constants {BUTTONS=16};


  /**
   * Name of the current dialog section
   * @author Hj. Malthaner
   */
  const char *current; 
  sections_t *model;
  dialog_change_listener_t *listener;


  handle_tpl<thing_t> & talker;
  handle_tpl<thing_t> & customer;

  html_view_t * speech;

  /**
   * Reply buttons
   * @author Hj. Malthaner
   */
  hw_button_t * reply[BUTTONS];

  void make_buttons();
  void kill_buttons();


 public:

  void set_dialog_change_listener(dialog_change_listener_t *l);

  dialog_view_t(sections_t *, 
		handle_tpl <thing_t> &talker,
		handle_tpl <thing_t> &customer);

  ~dialog_view_t();


  void update_dialog();

  /**
   * Sets the size
   * @author Hj. Malthaner
   */
  virtual void set_size(koord size);

  virtual void action_triggered(gui_action_trigger_t * trigger);
};

#endif // dialog_view_t_h
