
#include "attack_buttons_t.h"


#include "control/usage_t.h"
#include "model/thing_t.h"
#include "swt/tileset_t.h"
//#include "swt/gui_action_trigger_t.h"
#include "view/world_view_t.h"


void attack_buttons_t::set_player(thinghandle_t p) {
  player=p;
}


attack_buttons_t::attack_buttons_t(usage_t *u)
{
  static const koord offsets [8] = {
    koord(32,0),
    koord(64,0),
    koord(80,17),
    koord(64,34),
    koord(32,34),
    koord(0,34),
    koord(0,17),
    koord(0,0),
  };

  const tileset_t *tileset = world_view_t::get_instance()->get_tileset(0);

  usage = u;

  for(int i=0; i<8; i++) {
    buttons[i].set_size(koord(32,17));
    buttons[i].set_pos(offsets[i]);
    buttons[i].set_tile(tileset->get_tile(88+i));
    buttons[i].set_tile_pressed(tileset->get_tile(96+i));

    add_component(&buttons[i]);

    buttons[i].add_listener(this);
  }

  label.set_color(color_t::BLACK);
  label.set_pos(koord(34,21));
  label.set_text(usage->get_name());
  add_component(&label);

  set_opaque(false);
}


void attack_buttons_t::set_usage(usage_t *p) {
  usage=p;
  label.set_text(usage->get_name());
}


void attack_buttons_t::action_triggered(gui_action_trigger_t *t)
{
  static const koord offsets [8] = {
    koord( 0,-1),
    koord( 1,-1),
    koord( 1, 0),
    koord( 1, 1),
    koord( 0, 1),
    koord(-1, 1),
    koord(-1, 0),
    koord(-1,-1),
  };


  if(player.is_bound()) {
    for(int i=0; i<8; i++) {
      if(&buttons[i] == t) {
	koord pos = player->get_pos() + offsets[i];
      
	usage->activate(0, pos);
      
	break;
      }
    }
  }
}


