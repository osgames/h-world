/* 
 * tile_font_t.h
 *
 * Copyright (c) 2005 - 2005 Hansj�rg Malthaner
 *
 * This file is part of the hjmlib project and may not be used
 * in other projects without written permission of the author.
 */


/**
 * This is a font made from single images, each one defining a
 * character of the font.
 *
 * @author Hj. Malthaner
 */

#ifndef tile_font_t_h
#define tile_font_t_h

class graphics_t;
class tileset_t;
class graphics_t;

class tile_font_t
{
 private:
  int ascend;
  int descend;

  tileset_t * glyphs;
  signed char kern_left[256];
  signed char kern_right[256];

 public:

  int get_height();


  tile_font_t();
  ~tile_font_t();


  /**
   * Load glyph tileset from file
   * @autor Hj. Malthaner 
   */
  void load(const char * filename);

  /**
   * Determines the width of a string written in this font
   * @autor Hj. Malthaner 
   */
  int get_string_width(const char * text, int spacing) const;


  void draw_string(graphics_t * gr, const char * text, 
		   int x, int y, int spacing);
};

#endif // tile_font_t_h
