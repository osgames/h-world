#ifndef map_view_t_h
#define map_view_t_h

#include "swt/gui_component_t.h"

class image_t;

class map_view_t : public gui_component_t
{
 private:

 public:


  map_view_t();


  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);
};

#endif // map_view_t_h
