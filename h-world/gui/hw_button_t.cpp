/* 
 * hw_button_t.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "hw_button_t.h"

#include "view/visual_t.h"

#include "primitives/cstring_t.h"

#include "swt/graphics_t.h"
#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/event_t.h"

#include "gui_utils.h"


void hw_button_t::set_image(const visual_t & v)
{
  if(visual == 0) {
    visual = new visual_t();
  }

  *visual = v;

  visual->set_x_off(0);
  visual->set_y_off(0);

  has_image = true;
}


hw_button_t::hw_button_t(const char *txt, char hot) : gui_button_t(txt) 
{
  active = false;
  hotkey = hot;
  has_image = false;
  visual = 0;

  border = 2;
  editable = false;
  has_focus = false;

  text_offset = koord(-1, -1);
}


hw_button_t::~hw_button_t()
{
  delete visual;
}


/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool hw_button_t::process_event(const event_t &ev)
{
  if(ev.type == event_t::CONTAINER) {

    if(ev.code == event_t::ENTER) {
      if(hotkey != ' ') {
	active = true;
	redraw();
      }
    } else if(ev.code == event_t::LEAVE) {
      active = false;
      redraw();
    }
  }

  if(ev.type == event_t::KEY_PRESS) {
    if(has_focus == false && ev.code == hotkey && hotkey != ' ') {
      call_listeners();
    }

    if(has_focus == true) {
      const int l = strlen(text);
      if(ev.code == 8 || ev.code == 127) {
	if(l > 0) {
	  text[l-1] = '\0';
	}
      } else if(ev.code >= 32) {
	if(l<126) {
	  text[l] = ev.code;
	  text[l+1] = '\0';
	}
      } else if(ev.code > 0) {
	has_focus = false;
      }

      redraw();
    }
  }


  if(editable && ev.type == event_t::BUTTON_PRESS) {
    has_focus = true;

    return true;
  }

  return gui_button_t::process_event(ev) || has_focus;
}

/**
 * Draws the component
 * @author Hj. Malthaner
 */
void hw_button_t::draw(graphics_t *gr, koord screenpos)
{
  const koord pos = get_pos()+screenpos;
  const koord size = get_size();
  const int width = font->get_string_width(text, 0);

  if(active) {

    if(pressed || has_focus) {
      ddd_frame(gr, pos, size, border, color_t::GREY64, color_t::WHITE);
    } else {
      ddd_frame(gr, pos, size, border, color_t::WHITE, color_t::GREY64);
    }
  }

  // Body
  // gr->fillbox_wh(pos.x+2, pos.y+2, size.x-4, size.y-4, color_t::GREY224);

  koord off = text_offset;

  if(text_offset == koord(-1, -1)) {
    off = koord((size.x-width)/2, (size.y-font->get_height())/2);
  }

  if( editable ) {
    gr->fillbox_wh(pos.x+2, pos.y+2, size.x-4, size.y-4, color_t::GREY160);

    if( has_focus ) {
      const int width = font->get_string_width(text, 0);

      gr->fillbox_wh(pos.x+1+off.x+width, pos.y+off.y, 4, 10, color_t::GREY224);
    }
  }
  
  font->draw_string(gr, text, pos.x+off.x, pos.y+off.y, 0, color_t::BLACK);


  if(hotkey != ' ') {
    gr->fillbox_wh(pos.x + off.x, 
		   pos.y + off.y + font->get_height(),
		   width,
		   1,
		   color_t::BLACK);
  }

  if(has_image) {
    visual->paint(gr, 
		  pos.x + off.x + width + image_offset.x + 8,
		  pos.y + off.y + image_offset.y);
  }

}
