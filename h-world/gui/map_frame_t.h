#ifndef map_frame_t_h
#define map_frame_t_h

#include "escapable_frame_t.h"
#include "map_view_t.h"

class map_frame_t : public escapable_frame_t
{
 private:

  map_view_t view;

 public:

  map_frame_t();


  void draw(graphics_t *gr, koord pos);

  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  bool process_event(const event_t &ev);

};

#endif // map_frame_t_h
