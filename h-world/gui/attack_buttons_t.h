#ifndef ATTACK_BUTTONS_T_H
#define ATTACK_BUTTONS_T_H


#include "swt/gui_container_t.h"
#include "swt/gui_label_t.h"
#include "swt/gui_image_button_t.h"
#include "swt/ifc/action_listener_t.h"
#include "model/thinghandle_t.h"
#include "tpl/handle_tpl.h"

class usage_t;
class gui_action_trigger_t;

class attack_buttons_t : public gui_container_t, action_listener_t
{
 private:

  gui_image_button_t buttons[8]; 
  gui_label_t label;

  thinghandle_t player;
  usage_t *usage;
 public:

  void set_player(thinghandle_t p);
  void set_usage(usage_t *p);


  attack_buttons_t(usage_t *usage);

  void action_triggered(gui_action_trigger_t *t);
};



#endif
