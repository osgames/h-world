/* 
 * usage_panel_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef USAGE_PANEL_T_H
#define USAGE_PANEL_T_H


#include "swt/gui_container_t.h"
#include "ifc/limb_visitor_t.h"
#include "ifc/thing_visitor_t.h"
#include "ifc/thing_listener_t.h"
#include "ifc/inventory_listener_t.h"

#ifndef tpl_slist_tpl_h
#include "tpl/slist_tpl.h"
#endif


class event_t;
class graphics_t;
class font_t;
class usage_t;
class area_t;

class usage_panel_t : public gui_container_t, 
		      limb_visitor_t,
		      thing_visitor_t,
		      public thing_listener_t,
		      public inventory_listener_t
{
private:    

    font_t * font;

    /**
     * ALL usages
     * @author Hansj�rg Malthaner
     */
    slist_tpl < handle_tpl<thing_t> > items_for_use;


    /**
     * Attack usages
     * @author Hansj�rg Malthaner
     */
    slist_tpl <usage_t *> attacks;


    slist_tpl <area_t> usage_pos;
    slist_tpl <area_t> attack_pos;


    /**
     * While adding buttons, this is used to store the position of the next 
     * button to add.
     * @author Hansj�rg Malthaner
     */
    koord button_pos;

    int sel_atk;
    int sel_usg;

public:

    unsigned int def_atk;
    int def_usg;


    usage_panel_t();

      
    void link(const handle_tpl <thing_t> &model);


    /**
     * Get selected item for use
     * @author Hj. Malthaner
     */
    handle_tpl <thing_t> get_selected_item_for_use();


    /**
     * Get selected attack
     * @author Hj. Malthaner
     */
    usage_t *get_selected_attack();


    /**
     * Make next attack the default attack
     * @author Hj. Malthaner
     */
    void def_atk_next();


    /**
     * Make previous attack the default attack
     * @author Hj. Malthaner
     */
    void def_atk_prev();


    /**
     * Get a usage with certain preperties
     * @see policy_t
     * @author Hj. Malthaner
     */
    usage_t *get_usage(int effect);


    /**
     * Make next usage the default usage
     * @author Hj. Malthaner
     */
    void def_usg_next();


    /**
     * Make previous usage the default usage
     * @author Hj. Malthaner
     */
    void def_usg_prev();


    /**
     * Implements thing_visitor_t
     * @author Hj. Malthaner
     */
    virtual void visit_thing(const handle_tpl <thing_t> &);


    /**
     * Implements limb_visitor_t
     * @author Hj. Malthaner
     */
    virtual void visit_limb(const handle_tpl <thing_t> &);


    /**
     * Implements thing_listener_t
     * @author Hj. Malthaner
     */
    virtual void update(const handle_tpl <thing_t> &);


    /**
     * Implements inventory_listener_t
     * @author Hj. Malthaner
     */
    virtual void thing_added(handle_tpl < thing_t > &thing);


    /**
     * Implements inventory_listener_t
     * @author Hj. Malthaner
     */
    virtual void thing_removed(handle_tpl < thing_t > &thing);


  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);


  /**
   * Process an event.
   * @return true if event was swallowed, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t &ev);

};
#endif //MESSAGE_LOG_VIEW_T_H

