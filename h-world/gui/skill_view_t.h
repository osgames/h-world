/* 
 * skill_view_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef SKILL_VIEW_T_H
#define SKILL_VIEW_T_H

#include "swt/gui_frame_t.h"

class skills_t;
class skill_tree_view_t;

/**
 * A inspection window for the skills
 * @author Hj. Malthaner
 */
class skill_view_t : public gui_frame_t {
 private:    

    skill_tree_view_t *tree_view;

 public:

    /**
     * Basic constructor.
     * @param skills the skills to display.
     * @param title the window title. May not be NULL!
     * @author Hj. Malthaner
     */
    skill_view_t(skills_t *, const char *title);

    ~skill_view_t();
};

#endif
