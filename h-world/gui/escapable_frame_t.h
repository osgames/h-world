#ifndef escapable_frame_t_h
#define escapable_frame_t_h

#include "swt/gui_frame_t.h"


/**
 * Frame type that closes on "escape" keypress
 * @author Hj. Malthaner
 */
class escapable_frame_t : public gui_frame_t
{
 private:

  /**
   * Delete on close?
   * @author Hj. Malthaner
   */
  bool autodelete;

  
  /**
   * Swallow all events?
   * @author Hj. Malthaner
   */
  bool swallow_all;

 protected:

  static bool escapable_frame_t::kill_window(escapable_frame_t *win);

  void set_swallow_all(bool yesno);

 public:

  escapable_frame_t(const char *title, bool autodelete);


  /**
   * Position frame centered on screen
   * @author Hj. Malthaner
   */
  void center();


  /**
   * called before window is closed in response to a close event
   * @author Hj. Malthaner
   */
  virtual void on_close();


  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t &ev);


  /**
   * Draws the window frame and all components (by calling update
   * on the container).
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);    

};

#endif // escapable_frame_t_h
