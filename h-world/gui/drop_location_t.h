/* 
 * drop_location_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef DROP_LOCATION_T_H
#define DROP_LOCATION_T_H


#include "swt/drop_target_t.h"


class font_t;

/**
 * Helper class for the inventory (body view) to drop things
 * @author Hj. Malthaner
 */
class drop_location_t : public drop_target_t {
 private:

  font_t *font;

  /**
   * Drop location
   */
  koord pos;

 public:


  drop_location_t(koord p);



  bool drop(drawable_t * item, koord at);


  /**
   * called if item is dragged over the drop target, but not yet dropped
   * @author Hj. Malthaner
   */
  virtual void hover(drawable_t * item, koord at) {};


  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  void draw(graphics_t *gr, koord pos);

};


#endif // DROP_LOCATION_T_H
