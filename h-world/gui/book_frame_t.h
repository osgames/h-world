/* 
 * book_frame_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef book_frame_t_h
#define book_frame_t_h

#include "modal_frame_t.h"

#ifndef ACTION_LISTENER_T_H
#include "swt/ifc/action_listener_t.h"
#endif


class chapter_view_t;
class thing_t;
class hw_button_t;
template <class T> class handle_tpl;

/**
 * The book shows all memorized messages from all categories
 * @author Hj. Malthaner
 */
class book_frame_t : public modal_frame_t, action_listener_t
{
 private:

  /**
   * Lifetime is book_frame_t lifetime
   * @author Hj. Malthaner
   */
  chapter_view_t * chapter_view;

  hw_button_t * quests;
  hw_button_t * events;

 public:

  book_frame_t(const handle_tpl <thing_t> & model);
  virtual ~book_frame_t();

  virtual void action_triggered(gui_action_trigger_t * trigger);
};

#endif // book_frame_t_h
