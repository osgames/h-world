/* 
 * skill_view_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "skill_view_t.h"
#include "skill_tree_view_t.h"
#include "view/world_view_t.h"
#include "swt/gui_container_t.h"
#include "swt/tileset_t.h"
#include "../guidef.h"

/**
 * Basic constructor.
 * @param skills the skills to display.
 * @param title the window title. May not be NULL!
 * @author Hj. Malthaner
 */
skill_view_t::skill_view_t(skills_t * skills, const char *title) : gui_frame_t(title){
  
  tree_view = new skill_tree_view_t(skills);

  set_size(koord(200,400));
  tree_view->set_size(koord(200,400));

  set_background(world_view_t::get_instance()->get_tileset(2)->get_tile(BACK_TILE));

  get_container()->add_component(tree_view);
}



skill_view_t::~skill_view_t() {
  delete tree_view;
  tree_view = 0;
}
