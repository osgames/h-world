#ifndef player_gui_t_h
#define player_gui_t_h


class thing_t;
class stats_panel_t;
class usage_panel_t;

template <class T> class handle_tpl;

/**
 * Global object that handles the player-ui link
 * @author Hj. Malthaner
 */
class player_gui_t
{
 private:
  
  static player_gui_t * single_instance;

  stats_panel_t * stats_panel;
  usage_panel_t * usage_panel;


  player_gui_t();


 public:

  static player_gui_t * get_instance();


  void init(stats_panel_t *, usage_panel_t *);


  void link(const handle_tpl <thing_t> &player);
};

#endif // player_gui_t.h_h
