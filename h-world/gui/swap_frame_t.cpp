/* 
 * trade_view_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "swap_frame_t.h"
#include "model/inventory_t.h"
#include "model/thing_t.h"
#include "model/thing_tree_t.h"
#include "view/world_view_t.h"
#include "swt/tileset_t.h"
#include "../guidef.h"

#define RASTER 16

swap_frame_t::swap_frame_t(handle_tpl <thing_t> buyer_thing, handle_tpl <thing_t> seller_thing, bool ad) : 
  escapable_frame_t("Exchange items", ad),
  display(props_display_t::inspector)
{
  set_swallow_all(true);

  slist_tpl < handle_tpl < thing_t > > list;
  thing_tree_t tree;

  set_size(koord(480,400+16));

  set_opaque(true);
  set_background(world_view_t::get_instance()->get_tileset(2)->get_tile(BACK_TILE));


  // Hajo: the display must be the top-level componet, so it must
  // be added first
  display.set_size(koord(280,200));
  display.set_pos( (get_size() - display.get_size())/2 );
  add_component(&display);


  // false means not to list inventory contents
  tree.list_inventories(buyer_thing, &list, false);
  buyer.set_model(list.at(0)->get_inventory());

  list.clear();

  // false means not to list inventory contents
  tree.list_inventories(seller_thing, &list, false);
  seller.set_model(list.at(0)->get_inventory());


  buyer.set_pos(koord((240-buyer.get_size().x)/2,
		      (get_size().y-buyer.get_size().y)/2));

  seller.set_pos(koord(240 + (240-seller.get_size().x)/2,
		       (get_size().y-seller.get_size().y)/2));


  drag_control.set_frame(this);


  drag_control.add_drop_target(&seller);
  drag_control.add_drop_target(&buyer);


  update_labels();

  d_yours.set_pos(buyer.get_pos() - koord(0, 14));
  d_yours.set_size(koord(100,10));
  d_yours.set_text("Your items:");

  d_traders.set_pos(seller.get_pos() - koord(0, 14));
  d_traders.set_size(koord(100,10));
  d_traders.set_text("Storage:");


  add_component(&d_yours);
  add_component(&d_traders);

  add_component(&buyer);
  add_component(&seller);


  // link props display to the inventory views

  buyer.add_listener(&display);
  seller.add_listener(&display);
}


swap_frame_t::~swap_frame_t()
{
}


/**
 * called before window is closed in response to a close event
 * @author Hj. Malthaner
 */
void swap_frame_t::on_close()
{
  drag_control.stop_dragging();

  escapable_frame_t::on_close();
}


void swap_frame_t::thing_added(handle_tpl < thing_t > &thing)
{
  update_labels();
  redraw();
}


void swap_frame_t::thing_removed(handle_tpl < thing_t > &thing)
{
  update_labels();
  redraw();
}


void swap_frame_t::update_labels()
{

}
