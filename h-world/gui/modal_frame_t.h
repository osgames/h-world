#ifndef modal_frame_t_h
#define modal_frame_t_h

#include "escapable_frame_t.h"


/**
 * Modal frames
 * @author Hj. Malthaner
 */
class modal_frame_t : public escapable_frame_t
{
 private:

  bool is_modal;

  /**
   * We need to override escapable_frame_t autodelete fetuer
   * because it'd delete us to early
   * @author Hj. Malthaner
   */
  bool is_autodelete;

 public:

  modal_frame_t(const char *title);
  modal_frame_t(const char *title, bool is_modal, bool autodelete);


  void hook();


  /**
   * called before window is closed in response to a close event
   * @author Hj. Malthaner
   */
  virtual void on_close();

};

#endif // modal_frame_t_h

