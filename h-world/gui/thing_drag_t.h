/* 
 * drag_thing.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef THING_DRAG_T_H
#define THING_DRAG_T_H

#include "swt/drawable_t.h"
#include "model/thinghandle_t.h"
#include "tpl/handle_tpl.h"


/**
 * Draggable container for dragging items around
 *
 * @author Hansj�rg Malthaner 
 */
class thing_drag_t : public drawable_t 
{
private:   
   thinghandle_t thing;
   
public:

   void set_thing(thinghandle_t thing);


   thing_drag_t(thinghandle_t thing);


   thinghandle_t get_thing() const;

   void draw(graphics_t *gr, koord pos);
};

#endif
