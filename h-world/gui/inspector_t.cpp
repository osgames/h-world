/* 
 * inspector_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "inspector_t.h"
#include "thing_drag_t.h"

#include "view/world_view_t.h"
#include "model/thing_t.h"

#include "swt/tileset_t.h"
#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"
#include "swt/event_t.h"


#include "fontdef.h"


inspector_t::inspector_t() 
{
  l = 0;
  font = font_t::load(FONT_STD);
}


/**
 * Paints the component
 * @author Hj. Malthaner
 */
void inspector_t::draw(graphics_t *gr, koord screenpos)
{
  const koord size = get_size();
  const koord pos = get_pos()+screenpos;
  painter_t pt (gr);

  pt.box_wh(pos, size, color_t::GREY64);

  const tileset_t *tiles = world_view_t::get_instance()->get_tileset(0);
  gr->draw_tile(tiles->get_tile(104), pos.x+20, pos.y+12);

  //  if(thing.is_bound()) {
  //  const koord tpos = pos + (size - thing->get_inv_size()*12)/2;
  //  thing->inv_visual.paint(gr, tpos.x, tpos.y);
  //  }

  font->draw_string(gr, "Inspect item",
		    pos.x+2, pos.y+2, 0, color_t::BLACK);
}



/**
 * Implements drop_target_t
 * @author Hansj�rg Malthaner
 */
bool inspector_t::drop(drawable_t * item, koord at)
{
  return false;
}


/**
 * called if item is dragged over the drop target, but not yet dropped
 * @author Hj. Malthaner
 */
void inspector_t::hover(drawable_t * item, koord at)
{
  thing_drag_t *drag = dynamic_cast<thing_drag_t *>(item);

  if(drag) {
    thing = drag->get_thing();
    // redraw();

  } else {
    // unbind
    thing = 0;
  }

  l->update(thing);
}


/**
 * Process an event.
 * @return true if event was swallowed, false otherwise
 * @author Hj. Malthaner
 */
bool inspector_t::process_event(const event_t &ev)
{
  if(ev.type == event_t::BUTTON_RELEASE) {
    if(thing.is_bound()) {
      drawable_t * drag = new thing_drag_t(thing);

      thing = 0;

      redraw();

      l->update(thing);
    
      start_dragging(this, drag);
    }
  }

  return false;
}
