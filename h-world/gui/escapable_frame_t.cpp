#include "util/debug_t.h"

#include "escapable_frame_t.h"
#include "swt/event_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"
#include "swt/tileset_t.h"
#include "swt/tile_descriptor_t.h"
#include "swt/window_manager_t.h"

#include "view/world_view_t.h"

#include "../guidef.h"
#include "../fontdef.h"


void escapable_frame_t::set_swallow_all(bool yesno)
{
  swallow_all = yesno;
}


bool escapable_frame_t::kill_window(escapable_frame_t *win)
{
  dbg->message("escapable_frame_t::kill_window()", "Called, win=%p", win);

  bool swallowed = false;

  // just a convenient hook for subclasses
  win->on_close();
  
  // we don't want to get any more events ...
  win->set_visible(false);

  // remove window from manager
  win->close();

  if(win->autodelete) {
    delete win;
    swallowed = true;
  }

  return swallowed;
}


escapable_frame_t::escapable_frame_t(const char *title, bool ad) : gui_frame_t(title)
{
  printf("esc, title=%s\n", title); 

  swallow_all = false;
  autodelete = ad;

  set_font(font_t::load(FONT_STD));
  set_title_font(font_t::load(FONT_SML));

  set_background(world_view_t::get_instance()->get_tileset(2)->get_tile(BACK_TILE));
}


/**
 * Position frame centered on screen
 * @author Hj. Malthaner
 */
void escapable_frame_t::center()
{
  const int w = window_manager_t::get_instance()->get_width(); 
  const int h = window_manager_t::get_instance()->get_height() - 32;

  set_pos((koord(w,h) - get_size()) / 2);
}


/**
 * called before window is closed in response to a close event
 * @author Hj. Malthaner
 */
void escapable_frame_t::on_close()
{
  
}


/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool escapable_frame_t::process_event(const event_t &ev)
{
  bool swallowed = swallow_all;

  // printf("event type=%d code=%d\n", ev.type, ev.code); 


  if((ev.type == event_t::KEY_PRESS && ev.code == 27) ||
     (ev.type == event_t::WINDOW && ev.code == event_t::CLOSE)) {


    swallowed |= kill_window(this);

  } else {
    swallowed |= gui_frame_t::process_event(ev);
  }

  // printf("%p:\tevent type=%d code=%d swallowed=%d\n", this, ev.type, ev.code, swallowed); 

  return swallowed;
}


/**
 * Draws the window frame and all components (by calling update
 * on the container).
 * @author Hj. Malthaner
 */
void escapable_frame_t::draw(graphics_t *gr, koord pos)
{
  gr->set_font(get_font());
  gui_window_t::draw(gr, pos);

  const tileset_t * tileset = world_view_t::get_instance()->get_tileset(2);
  const tile_descriptor_t * hobo = tileset->get_tile(2);
  const tile_descriptor_t * vebol = tileset->get_tile(3);
  const tile_descriptor_t * vebor = tileset->get_tile(4);

  const tile_descriptor_t * left = tileset->get_tile(5);
  const tile_descriptor_t * mid = tileset->get_tile(6);
  const tile_descriptor_t * right = tileset->get_tile(7);

  const koord size ( get_size() );

  gr->set_clip_wh(pos.x, pos.y, size.x, size.y);

  gr->draw_tile_shaded(left, pos.x, pos.y, 32);
  for(int i=16; i<size.x-16; i+=16) { 
    gr->draw_tile_shaded(mid, pos.x+i, pos.y, 32);
  }
  gr->draw_tile_shaded(right, pos.x+size.x-right->w, pos.y, 32);


  painter_t pt(gr);
  pt.draw_string_bold(get_title_font(), get_title(), 
		      pos + koord(18, 3), 1, color_t::WHITE, color_t::GREY192);


  for(int i=16; i<size.y; i+=16) { 
    gr->draw_tile_shaded(vebol, pos.x, pos.y+i, 32);
    gr->draw_tile_shaded(vebor, pos.x+size.x - vebor->w, pos.y+i, 32);
  }

  for(int i=0; i<size.x; i+=16) { 
    // gr->draw_tile_shaded(hobo, pos.x+i, pos.y, 32);
    gr->draw_tile_shaded(hobo, pos.x+i, pos.y+size.y - hobo->h, 32);
  }


  const tile_descriptor_t * loleft = tileset->get_tile(8);
  const tile_descriptor_t * loright = tileset->get_tile(9);

  gr->draw_tile_shaded(loleft, pos.x, 
		       pos.y+size.y - loleft->h - loleft->y, 32);
  gr->draw_tile_shaded(loright, pos.x + size.x - loright->w - loright->x, 
		       pos.y+size.y - loright->h - loright->y, 32);
}
