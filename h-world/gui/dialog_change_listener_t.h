#ifndef dialog_change_listener_t_h
#define dialog_change_listener_t_h

class dialog_change_listener_t
{

 public:

  /**
   * Called if a new dialog is displayed
   * @author Hj. Malthaner
   */
  virtual void on_dialog_changed(const char *new_dialog) = 0;

};

#endif // dialog_change_listener_t_h
