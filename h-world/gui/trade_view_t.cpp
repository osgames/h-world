/* 
 * trade_view_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "trade_view_t.h"
#include "model/inventory_t.h"
#include "model/thing_t.h"
#include "model/thing_tree_t.h"
#include "control/skills_t.h"
#include "language/linguist_t.h"
#include "view/world_view_t.h"
#include "swt/tileset_t.h"
#include "../guidef.h"

#define RASTER 16

/**
 * Customer must pay x/256 times the value of the goods
 * @author Hj. Malthaner
 */
void trade_view_t::set_price_factor(int f)
{
  price_factor = f;
}


trade_view_t::trade_view_t(handle_tpl <thing_t> a_buyer_thing, handle_tpl <thing_t> seller_thing) : 
  escapable_frame_t("Trading", false),
  display(props_display_t::inspector), 
  doit("x: Exchange goods", 'x')
{
  set_swallow_all(true);

  buyer_thing = a_buyer_thing;

  slist_tpl < handle_tpl < thing_t > > list;
  thing_tree_t tree;

  price_factor = 0;

  set_size(koord(480+8, 480+16+8));

  set_opaque(true);
  set_background(world_view_t::get_instance()->get_tileset(2)->get_tile(BACK_TILE));


  // Hajo: the display must be the top-level component, so it must
  // be added first
  display.set_size(koord(280,240));
  display.set_pos( (get_size() - display.get_size())/2 );
  add_component(&display);


  offer_inv = new inventory_t(koord(10,10), buyer_thing);
  request_inv = new inventory_t(koord(10,10), seller_thing);
  request_inv->set_usage_allowed(false);


  offer_inv->add_listener(this);
  request_inv->add_listener(this);

  offer.set_model(offer_inv);
  request.set_model(request_inv);

  // false means not to list inventory contents
  tree.list_inventories(buyer_thing, &list, false);

  if(list.count() > 0) {
    buyer.set_model(list.at(0)->get_inventory());
  } else {
    buyer.set_model(0);
  }


  list.clear();

  // false means not to list inventory contents
  tree.list_inventories(seller_thing, &list, false);

  if(list.count() > 0) {
    seller.set_model(list.at(0)->get_inventory());
    list.at(0)->get_inventory()->set_usage_allowed(false);
  } else {
    seller.set_model(0);
  }

  buyer.set_pos(koord(6,16));
  seller.set_pos(koord(488-seller.get_size().x-6, 16));

  offer.set_pos(koord(240-RASTER*5, 16));
  request.set_pos(koord(240-RASTER*5, 480-request.get_size().y));


  seller_drag.set_frame(this);
  buyer_drag.set_frame(this);


  seller_drag.add_drop_target(&request);
  seller_drag.add_drop_target(&seller);

  buyer_drag.add_drop_target(&offer);
  buyer_drag.add_drop_target(&buyer);

  doit.set_size(koord(128, 24));
  doit.set_pos(koord(240-64, 240-12));
  doit.add_listener(this);

  offer_label.set_size(koord(160, 10));

  if(buyer_thing->get_int("money", -1) >= 0) {
    offer_label.set_pos(koord(160, 200));
  } else {
    offer_label.set_pos(koord(200, 200));
  }
  offer_label.set_text(offer_value);

  request_label.set_size(koord(100, 10));
  request_label.set_pos(koord(200, 280));
  request_label.set_text(request_value);

  d_buyer_money.set_size(koord(100, 10));
  d_buyer_money.set_pos(buyer.get_pos()+koord(0, buyer.get_size().y+10));
  d_buyer_money.set_text(buyer_money);

  update_labels();


  d_yours.set_pos(koord(6,2));
  d_yours.set_size(koord(100,10));
  d_yours.set_text("Your items:");

  d_traders.set_pos(koord(480-seller.get_size().x-1, 2));
  d_traders.set_size(koord(100,10));
  d_traders.set_text("Traders items:");

  d_offer.set_pos(koord(240-RASTER*5, 2));
  d_offer.set_size(koord(100,10));
  d_offer.set_text("Your offer:");

  d_request.set_pos(koord(240-RASTER*5, 480-request.get_size().y-14));
  d_request.set_size(koord(100,10));
  d_request.set_text("Your request:");

  d_buyer_money.set_text("");


  add_component(&d_yours);
  add_component(&d_traders);
  add_component(&d_offer);
  add_component(&d_request);
  add_component(&d_buyer_money);

  add_component(&doit);
  add_component(&offer_label);
  add_component(&request_label);
  add_component(&buyer);
  add_component(&seller);
  add_component(&offer);
  add_component(&request);


  // link props display to the inventory views

  buyer.add_listener(&display);
  seller.add_listener(&display);
  offer.add_listener(&display);
  request.add_listener(&display);
}


void trade_view_t::trade_done_place_items()
{
  slist_tpl < handle_tpl < thing_t > > list;
  offer_inv->list_things(&list);

  slist_iterator_tpl < handle_tpl < thing_t > > iter1 (list);

  // Hajo: count traded items
  int num_items = 0;

  while( iter1.next() ) {
    // drop excess items to ground
    const bool ok = buyer.get_model()->add_or_overflow( iter1.get_current() );

    if(!ok) {
      char buf[256];
      iter1.get_current()->get_ident(buf);
      linguist_t::svpo(buf, "drop", "to the", "floor");
    }

    offer_inv->remove( iter1.get_current() );
    num_items++;
  }
  

  list.clear();
  request_inv->list_things(&list);

  slist_iterator_tpl < handle_tpl < thing_t > > iter2 (list);

  while( iter2.next() ) {
    if(seller.get_model()->add_or_overflow( iter2.get_current() )) {
      request_inv->remove( iter2.get_current() );
      num_items++;
    }
  }

  if(num_items > 0) {
    buyer_thing->get_actor().skills->add_skill_value(skills_t::trade, 10);
  }
}


trade_view_t::~trade_view_t()
{
  trade_done_place_items();


  delete offer_inv;
  delete request_inv;

  offer_inv = 0;
  request_inv = 0;
}


/**
 * called before window is closed in response to a close event
 * @author Hj. Malthaner
 */
void trade_view_t::on_close()
{
  buyer_drag.stop_dragging();
  seller_drag.stop_dragging();

  trade_done_place_items();
}


void trade_view_t::action_triggered(gui_action_trigger_t * trigger)
{
  int money = buyer_thing->get_int("money", 0);

  if(offer_sum + money >= request_sum ||
     price_factor == 0) {
    // exchange inventories

    inventory_t *tmp = offer_inv;

    offer_inv = request_inv;
    request_inv = tmp;

    // re-set models
    offer.set_model(offer_inv);
    request.set_model(request_inv);

    const int diff = request_sum - offer_sum;

    if(buyer_thing->get_int("money", -1) >= 0) {
      buyer_thing->set("money", money - diff);
    }

    update_labels();

    redraw();
  }
}


void trade_view_t::thing_added(handle_tpl < thing_t > &thing)
{
  update_labels();
  redraw();
}


void trade_view_t::thing_removed(handle_tpl < thing_t > &thing)
{
  update_labels();
  redraw();
}


void trade_view_t::update_labels()
{
  int money = buyer_thing->get_int("money", -1);

  if(money >= 0) {
    sprintf(buyer_money, "Money: %d", money);
  } else {
    strcpy(buyer_money, "");
    money = 0;
  }


  slist_tpl < handle_tpl < thing_t > > list;
  offer_sum = 0;

  
  offer_inv->list_things(&list);

  slist_iterator_tpl < handle_tpl < thing_t > > iter1 (list);

  while( iter1.next() ) {
    offer_sum += iter1.get_current()->get_int("value", 0);
  }
  

  request_sum = 0;
  list.clear();

  request_inv->list_things(&list);

  slist_iterator_tpl < handle_tpl < thing_t > > iter2 (list);

  while( iter2.next() ) {
    request_sum += iter2.get_current()->get_int("value", 0);
  }

  request_sum = (request_sum * price_factor) >> 8;
 

  if(price_factor != 0) {
    if(money > 0) {
      
      sprintf(offer_value, "Value: %d, Money: %d - %s", 
	      offer_sum,
	      money,
	      offer_sum + money < request_sum ? "not enough" : "ok");
      sprintf(request_value, "Value: %d", request_sum);    

    } else {

      sprintf(offer_value, "Value: %d - %s", 
	      offer_sum,
	      offer_sum < request_sum ? "not enough" : "ok");
      sprintf(request_value, "Value: %d", request_sum);    
    }
  } else {
    sprintf(offer_value, " ");
    sprintf(request_value, " ");    
  }
}
