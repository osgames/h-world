#ifndef trade_view_t_h
#define trade_view_t_h


#include "inventory_view_t.h"
#include "escapable_frame_t.h"
#include "props_display_t.h"
#include "swt/drag_controller_t.h"
#include "hw_button_t.h"
#include "swt/gui_label_t.h"

#ifndef ACTION_LISTENER_T_H
#include "swt/ifc/action_listener_t.h"
#endif

#include "ifc/inventory_listener_t.h"
#include "model/thinghandle_t.h"


class trade_view_t : public escapable_frame_t, action_listener_t, inventory_listener_t
{
 private:

  handle_tpl <thing_t> buyer_thing;

  // Label models
  char offer_value[64];
  char request_value[32];
  char buyer_money[32];

  int offer_sum;
  int request_sum;

  int price_factor;

  /**
   * Displays properties of watched items
   * @author Hj. Malthaner
   */
  props_display_t display;

  hw_button_t doit;

  drag_controller_t buyer_drag;
  drag_controller_t seller_drag;


  inventory_t *offer_inv;
  inventory_t *request_inv;


  inventory_view_t buyer;
  inventory_view_t seller;

  inventory_view_t offer;
  inventory_view_t request;
  
  gui_label_t offer_label;
  gui_label_t request_label;


  // descriptive labels

  gui_label_t d_yours;
  gui_label_t d_traders;
  gui_label_t d_offer;
  gui_label_t d_request;

  gui_label_t d_buyer_money;


  void update_labels();

  void trade_done_place_items();

 public:


  /**
   * Customer must pay x/256 times the value of the goods
   * @author Hj. Malthaner
   */
  void set_price_factor(int f);


  trade_view_t(handle_tpl <thing_t> buyer, handle_tpl <thing_t> seller);
  virtual ~trade_view_t();


  /**
   * called before window is closed in response to a close event
   * @author Hj. Malthaner
   */
  virtual void on_close();


  virtual void action_triggered(gui_action_trigger_t * trigger);

  virtual void thing_added(handle_tpl < thing_t > &thing);
  virtual void thing_removed(handle_tpl < thing_t > &thing);

};

#endif // trade_view_t_h
