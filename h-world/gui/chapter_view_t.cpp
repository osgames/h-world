/* 
 * chapter_view_t.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "chapter_view_t.h"
#include "html_view_t.h"

#include "model/thing_t.h"
#include "model/thinghandle_t.h"

#include "util/debug_t.h"

/**
 * Show all messages for this category
 * @author Hj. Malthaner
 */
chapter_view_t::chapter_view_t(const handle_tpl <thing_t> & m, 
			       const char * catg) : model(m)
{
  current_catg = catg;
  views = new minivec_tpl <html_view_t *> (32);
}


chapter_view_t::~chapter_view_t()
{
  for(int i=0; i<views->count(); i++) { 
    delete views->at(i);
  }

  delete views;
  views = 0;
}


void chapter_view_t::change_catg(const char * catg)
{
  current_catg = catg;
}


void chapter_view_t::show_page()
{
  int y = 0;
  int n = 0;
  const char * msg = 0;

  for(int i=0; i<views->count(); i++) { 
    delete views->at(i);
  }
  views->clear();

  while( (msg = model->recall(current_catg, n++)) ) {
    html_view_t * view = new html_view_t();

    dbg->message("chapter_view_t::show_page()", "%s %d %s", 
		 current_catg, n, msg);

    view->set_text(msg);
    
    view->set_size(get_size());
    const koord pref = view->get_preferred_size();
    // view->set_size(pref);

    view->set_pos(koord(0, y));

    y += pref.y + 10;

    views->append(view);
  }
}


/**
 * Draws the component
 * @author Hj. Malthaner
 */
void chapter_view_t::draw(graphics_t *gr, koord pos)
{
  for(int i=0; i<views->count(); i++) { 
    views->at(i)->draw(gr, get_pos()+pos);
  }

}

