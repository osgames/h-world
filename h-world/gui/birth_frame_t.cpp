/* 
 * birth_frame_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "birth_frame_t.h"
#include "hw_button_t.h"

#include "factories/thing_factory_t.h"

#include "model/world_t.h"
#include "model/thing_t.h"
#include "model/memory_t.h"

#include "util/debug_t.h"
#include "swt/event_t.h"
#include "swt/font_t.h"
#include "swt/color_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"


birth_frame_t::birth_frame_t () : modal_frame_t("Birth options")
{
  char buf [256];

  thing_factory_t thing_factory;
  bool ok = true;
  int i = 0;

  set_opaque(true);

  players = new minivec_tpl <thinghandle_t> (64); 
  buttons = new minivec_tpl <hw_button_t *> (64); 

  do {
    sprintf(buf, "player-%d", i);

    ok = thing_factory.is_available(buf);
    if(ok) {
      thinghandle_t thing = thing_factory.create(buf, 0, 0);
      thing->set_memory(new memory_t());
      players->append(thing);

      sprintf(buf, "%c: %s", 'a'+i, 
	      thing->get_string("birth_name", "unnamed"));

      hw_button_t * but = new hw_button_t(buf, buf[0]);
      but->text_offset = koord(12, 44);
      but->image_offset = koord(0, -52);
      but->set_image(thing->visual);
      but->set_pos(koord(10 + (260*(i/5)), 24+106*(i%5)));
      but->set_size(koord(256, 106));

      but->payload = &(players->at(players->count()-1));

      but->add_listener(this);

      buttons->append(but);

      add_component(but);

      i++;
    }
  } while(ok);

  choice = players->at(0);

  autoresize();
}


birth_frame_t::~birth_frame_t ()
{
  for(int i=0; i<players->count(); i++) { 
    if(players->get(i) != choice) {
      players->at(i)->get_actor().delete_all_actions_but(0);
      players->at(i)->destroy();

      dbg->message("birth_frame_t::~birth_frame_t()", 
		   "%d has %d refs left", i, players->at(i).get_count() );

      players->at(i) = 0;
    }
  }

  delete players;
  delete buttons;
}


/**
 * Adapt size to content
 * @author Hj. Malthaner
 */
void birth_frame_t::autoresize()
{
  koord k;

  k.x = 280 + (players->count() / 5)*256;
  k.y = 56 + (players->count() % 5)*106;
  
  set_size(k);
}


/**
 * Draws the window frame and all components
 * 
 * @author Hj. Malthaner
 */
void birth_frame_t::draw(graphics_t *gr, koord pos)
{
  modal_frame_t::draw(gr, pos);
  painter_t pt (gr);

  pt.draw_string_bold(get_font(), "Please choose your player character", 
		      pos + koord(10, 24), 1, color_t::BLACK, color_t::GREY64);
}


void birth_frame_t::action_triggered(gui_action_trigger_t * trigger)
{
  dbg->message("birth_frame_t::action_triggered()", "called");

  choice = * ((thinghandle_t *) ((hw_button_t *)trigger)->payload);

  dbg->message("birth_frame_t::action_triggered()", 
	       "choice is %s", choice->get_string("birth_name", "unnamed"));

  on_close();
  close();
}
