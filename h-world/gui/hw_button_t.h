#ifndef hw_button_t_h
#define hw_button_t_h

#include "swt/gui_button_t.h"

class visual_t;

/**
 * Button subclass with H-World look
 * @author Hj. Malthaner
 */
class hw_button_t : public gui_button_t
{
 private:

  visual_t * visual;

  unsigned char has_image;

 public:

  unsigned char active;

  koord image_offset;
  koord text_offset;

  int border;

  /**
   * This can be used to bind some user-defined data to this button
   * @author Hj. Malthaner
   */
  void * payload;


  char  hotkey;
  char  editable;
  char  has_focus;


  void set_image(const visual_t & visual);


  hw_button_t(const char *txt, char hotkey);

  virtual ~hw_button_t();


  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  bool process_event(const event_t &ev);

  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);

};

#endif // hw_button_t.h_h
