/* 
 * dialog_frame_t.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>
#include <ctype.h>

#include "dialog_frame_t.h"
#include "dialog_view_t.h"

#include "model/thing_t.h"

#include "util/sections_t.h"
#include "util/debug_t.h"


dialog_frame_t::dialog_frame_t(const char * filename, 
			       bool autodelete,
			       handle_tpl <thing_t> &talker,
			       handle_tpl < thing_t > &customer)
  : escapable_frame_t("Bla Fasel", autodelete)
{
  set_swallow_all(true);
  set_size(koord(320,240));

  sections_t *file = new sections_t();

  dbg->message("dialog_frame_t::dialog_frame_t()", 
	       "reading dialog '%s'", filename);	    

  if(file->load(filename)) {

    char buf[1024];

    talker->get_ident(buf);
    buf[0] = toupper(buf[0]);
    set_title(strdup(buf));


    view = new dialog_view_t(file, talker, customer);

    view->set_pos(koord(15,12));
    view->set_size(get_size()-koord(25,20));
    view->set_opaque(false);

    add_component(view);

    view->set_dialog_change_listener(this);

    view->update_dialog();

  } else {
    dbg->warning("dialog_frame_t::dialog_frame_t()", 
		 "cannot read '%s'", filename);	    
    
  }
}


dialog_frame_t::~dialog_frame_t()
{
  delete(view);
  view = 0;
}


/**
 * Called if a new dialog is displayed
 * @author Hj. Malthaner
 */
void dialog_frame_t::on_dialog_changed(const char *new_dialog)
{
  if(new_dialog == 0) {
    close();
  }
}
