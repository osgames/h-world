/* Copyright by Hj. Malthaner */

#ifndef SKILL_TREE_VIEW_T_H
#define SKILL_TREE_VIEW_T_H

#include "swt/gui_component_t.h"

class font_t;
class skills_t;

class skill_tree_view_t : public gui_component_t
{
 private:

  skills_t * skills;

  font_t * font;
 public:


  skill_tree_view_t(skills_t * s); 


  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t * gr, koord pos);


};


#endif
