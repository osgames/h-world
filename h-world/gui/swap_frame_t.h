#ifndef trade_view_t_h
#define trade_view_t_h


#include "inventory_view_t.h"
#include "escapable_frame_t.h"
#include "props_display_t.h"
#include "swt/drag_controller_t.h"

#include "swt/gui_label_t.h"

#include "ifc/inventory_listener_t.h"
#include "model/thinghandle_t.h"


class swap_frame_t : public escapable_frame_t, inventory_listener_t
{

 private:

  /**
   * Displays properties of watched items
   * @author Hj. Malthaner
   */
  props_display_t display;

  drag_controller_t drag_control;

  inventory_view_t buyer;
  inventory_view_t seller;

  // descriptive labels

  gui_label_t d_yours;
  gui_label_t d_traders;


  void update_labels();

 public:

  swap_frame_t(handle_tpl <thing_t> buyer, handle_tpl <thing_t> seller, 
	       bool autodelete);

  virtual ~swap_frame_t();

  /**
   * called before window is closed in response to a close event
   * @author Hj. Malthaner
   */
  virtual void on_close();


  virtual void thing_added(handle_tpl < thing_t > &thing);
  virtual void thing_removed(handle_tpl < thing_t > &thing);

};

#endif // trade_view_t_h
