/* 
 * inspector_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef inspector_t_h
#define inspector_t_h

#include "ifc/thing_listener_t.h"
#include "swt/drop_source_t.h"
#include "swt/drop_target_t.h"
#include "model/thinghandle_t.h"
#include "tpl/handle_tpl.h"


class font_t;

/**
 * Componet to inspect things - if a thing is dragged upon this
 * the things properties will be displayed.
 *
 * @author Hj. Malthaner
 */
class inspector_t : public drop_target_t, drop_source_t
{
 private:

  font_t *font;

  /**
   * The thing to inspect
   * @author Hj. Malthaner
   */
  thinghandle_t thing;

  /**
   * The listner to call if  a thing is inspected
   * @author Hj. Malthaner
   */
  thing_listener_t *l;

 public:


  void set_listener(thing_listener_t *l) {this->l = l;};


  inspector_t();


  /**
   * Paints the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);


  /**
   * Implements drop_target_t
   * @author Hansj�rg Malthaner
   */
  virtual bool drop(drawable_t * item, koord at);


  /**
   * called if item is dragged over the drop target, but not yet dropped
   * @author Hj. Malthaner
   */
  virtual void hover(drawable_t * item, koord at);


  /**
   * Process an event.
   * @return true if event was swallowed, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t &);
};

#endif // inspector_t_h
