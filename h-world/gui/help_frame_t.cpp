#include <math.h>

#include <string.h>
#include <stdio.h>

#include "help_frame_t.h"
#include "html_view_t.h"

#include "swt/window_manager_t.h"
#include "primitives/cstring_t.h"


void help_frame_t::init(const char * path, const char * filename)
{
  set_swallow_all(true);

  view = new html_view_t();

  char buf[8192]; 

  cstring_t file_prefix(path);
  cstring_t fullname = file_prefix + "/" + filename;  
                   
  FILE * file = fopen(fullname, "rb");
  
  if(file) {
    const int len = fread(buf, 1, 8191, file);
    buf[len] = '\0';
    
    fclose(file);
  } else {

    // perror("Error:");
    sprintf(buf, "<title>Error</title>Help text '%s' not found",
	    fullname.chars());
  }

  view->set_text(buf);
  set_title(view->get_title());

  set_color(color_t::GREY224);
  set_opaque(true);

  view->set_pos(koord(10, 10));
  view->set_size(koord(260, 0));
                        
  int i = 0;
  int last_y = 0;
  koord curr;

  while(curr=view->get_preferred_size(), 
	(curr.y > 400 && curr.y != last_y && i < 9)) {
    view->set_size(koord(300+i*40, 0));
    last_y = curr.y;
    i++;
  }

  koord ps = view->get_preferred_size();

  const int A = (int)ps.x * (int)ps.y;

  ps.y = (int)sqrt(A*1.6);
  ps.x = (int)(ps.y / 1.6);

  view->set_size(ps);
  view->set_size(view->get_preferred_size());
  
  set_size(view->get_size()+koord(20, 20));

  add_component(view);

  view->add_listener(this);
}


help_frame_t::help_frame_t(const char * path, 
			   const char * filename, 
			   bool is_modal) :
  modal_frame_t("", is_modal, true)
{
  init(path, filename);
}


/**
 * Creates a new help frame with fixed path "./help"
 * 
 * @author Hj. Malthaner
 */

help_frame_t::help_frame_t(const char * filename, bool is_modal) : 
  modal_frame_t("", is_modal, true)
{
  init("help", filename);
}


help_frame_t::~help_frame_t()
{
  delete view;
  view = 0;
}


/**
 * Called upon link activation
 * @param the hyper ref of the link
 * @author Hj. Malthaner
 */
void help_frame_t::hyperlink_activated(const cstring_t &txt)
{
  help_frame_t *frame = new help_frame_t(txt, false);
  frame->set_visible(true);

  frame->set_pos(get_pos() + koord(get_size().x, 0));

  window_manager_t::get_instance()->add_window(frame);
}
