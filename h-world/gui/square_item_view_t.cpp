/* 
 * square_item_view_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"
#include "swt/event_t.h"
#include "swt/tileset_t.h"
#include "swt/tile_descriptor_t.h"

#include "square_item_view_t.h"
#include "thing_drag_t.h"
#include "multi_choice_t.h"
#include "gui_utils.h"

#include "model/world_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/thing_t.h"

#include "view/world_view_t.h"

#include "tpl/slist_tpl.h"

#include "ifc/inventory_view_listener_t.h"

#include "fontdef.h"

#define RASTER 16


void square_item_view_t::add_listener(inventory_view_listener_t *l)
{
  listeners->insert(l);
}


square_item_view_t::square_item_view_t()
{
  font = font_t::load(FONT_SML);
  listeners = new slist_tpl <inventory_view_listener_t *> ();
}


square_item_view_t::~square_item_view_t()
{
  delete listeners;
  listeners = 0;
}


/**
 * Refresh with contents from square at position k.
 * @author Hj. Malthaner
 */
void square_item_view_t::refresh(koord k)
{
  square_t * s = world_t::get_instance()->get_level()->at(k);

  list = 0;

  if(s) {
    list = &(s->get_things());
    location = k;
  }
}



/**
 * Paints the component
 * @author Hj. Malthaner
 */
void square_item_view_t::draw(graphics_t *gr, koord screenpos)
{
  static color_t grey  (208, 208, 208);

  const koord size = get_size();
  const koord pos = get_pos()+screenpos;


  if(size.x == 0) {
    // not visible
    return;
  }

  // painter_t pt (gr);
  // pt.box_wh(pos, size, color_t::GREY64);

  const int w = get_tileborder_width();


  // gr->fillbox_wh(pos.x+w, pos.y+w, size.x-w-w, size.y-w-w, grey);

  const tileset_t *tileset = world_view_t::get_instance()->get_tileset(2);
  const tile_descriptor_t *tile = tileset->get_tile(18);

  for(int y=0; y<size.y; y += tile->h) {
    for(int x=0; x<size.x; x += tile->w) {
      gr->draw_tile(tile, pos.x+x+w, pos.y+y+w);
    }
  }



  tileborderframe(gr, pos, size);

  font->draw_string(gr, "Items on ground",
		    pos.x+w+1, pos.y+w+1, 0, color_t::BLACK);


  minivec_iterator_tpl < handle_tpl <thing_t> > iter (list);
  koord ipos (pos.x+w,  (short)(pos.y+size.y-w));

  while( iter.next() ) {
    handle_tpl <thing_t> thing = iter.get_current();

    if(thing->get_string("is_player", 0) == 0) {
      const koord tsize = thing->get_inv_size();

      thing->inv_visual.paint(gr, ipos.x, ipos.y-tsize.y*RASTER);
      
      ipos.x += tsize.x*RASTER;
    }
  }
}


/**
 * Implements drop_target_t
 * @author Hansj�rg Malthaner
 */
bool square_item_view_t::drop(drawable_t * item, koord /*at*/)
{
  // dbg->message("thing_hold_t::drop()", "called");
  bool ok = false;
  thing_drag_t *drag = dynamic_cast<thing_drag_t *>(item);

  if(drag) {
    thinghandle_t thing = drag->get_thing();

    world_t::get_instance()->get_level()->at(location)->add_thing(thing, location);

    drag->set_thing(0);

    // must update
    world_view_t::get_instance()->set_dirty(true);
    redraw();

    ok = true;
  }

  return ok;
}



handle_tpl < thing_t > square_item_view_t::find_thing(koord cpos)
{
  minivec_iterator_tpl < handle_tpl <thing_t> > iter (list);
  koord ipos (8,0);

  while( iter.next() ) {
    handle_tpl <thing_t> thing = iter.get_current();
    
    if(thing->get_string("is_player", 0) == 0) {

      const koord tsize = thing->get_inv_size();
      ipos.x += tsize.x*RASTER;

      if(ipos.x > cpos.x) {
	return thing;
      }
    }
  }

  return handle_tpl <thing_t> ();
}


/**
 * If dragging is stopped, the item needs to be put back
 * @author Hj. Malthaner
 */
void square_item_view_t::stop_dragging(drawable_t *item)
{
  drop(item, koord(0,0));
}


/**
 * Process an event.
 * @return true if event was swallowed, false otherwise
 * @author Hj. Malthaner
 */
bool square_item_view_t::process_event(const event_t &ev)
{
  static thinghandle_t null;

  if(ev.type == event_t::BUTTON_RELEASE) {

    handle_tpl <thing_t> thing = find_thing(ev.cpos);

    if(thing.is_bound()) {


      // Hajo: check for cursed items
      if(*thing->get_string("pickup", "t") == 'f') {

	multi_choice_t c_info("Fixed thing:", 
			      "This thing appears to be fixed on the ground!", 
			      "y: ok");

	c_info.center();
	c_info.hook();

      } else {

	drawable_t * drag = new thing_drag_t(thing);

	world_t::get_instance()->get_level()->at(location)->remove_thing(thing);

	// must update
	world_view_t::get_instance()->set_dirty(true);
	redraw();
	
	start_dragging(this, drag);
	
	fire_mouse_over_thing(null);
      }

      return false;
    }
  } else if(ev.type == event_t::MOUSE_MOVE) {

    // printf("%d %d\n", ev.mpos.y, get_pos().y);

    if(ev.mpos.y >= get_pos().y && ev.mpos.y < get_pos().y+get_size().y) {
      fire_mouse_over_thing(find_thing(ev.mpos));
    }


  } else if(ev.type == event_t::CONTAINER) {
    if(ev.code == event_t::LEAVE) {
      // printf("LEAVE %s\n", limb->get_string("name", "unnamed"));

      fire_mouse_over_thing(null);
	
    }
  }

  return false;
}


void square_item_view_t::fire_mouse_over_thing(const handle_tpl < thing_t > &thing)
{
  slist_iterator_tpl <inventory_view_listener_t *> iter (listeners);

  while( iter.next() ) {
    iter.get_current()->mouse_over_thing(thing);
  }
}
