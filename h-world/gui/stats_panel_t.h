/* 
 * stats_panel_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef STATS_PANEL_T_H
#define STATS_PANEL_T_H


#include "swt/gui_container_t.h"
#include "ifc/property_listener_t.h"
#include "gui/props_display_t.h"

class graphics_t;
class font_t;
class thing_t;

template <class T> class handle_tpl;


class stats_panel_t : public gui_container_t, property_listener_t
{
 private:

  font_t * propfont;
  font_t * propfont_small;

  props_display_t display;

 public:


  stats_panel_t();


  /**
   * Registers this panel as listener on the model
   * @author Hj. Malthaner
   */
  void link(const handle_tpl <thing_t> &model);


  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);


  /**
   * Implements property listener
   * @author Hansj�rg Malthaner
   */
   virtual void update(const property_t *);

};

#endif

