/* 
 * maptooltip_t.h
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef maptooltip_t_h
#define maptooltip_t_h

#ifndef DRAWABLE_T_H
#include "swt/drawable_t.h"
#endif

#ifndef COLOR_T_H
#include "swt/color_t.h"
#endif

class font_t;

class maptooltip_t : public drawable_t
{
 private:
  /**
   * Tool tip title
   * @author Hj. Malthaner
   */
  char tt_title[128];

  /**
   * Tool tip text
   * @author Hj. Malthaner
   */
  char tt_text[1024];

  const char * line[16];

  /**
   * Title font
   * @author Hj. Malthaner
   */
  font_t * font;

  /**
   * Text font
   * @author Hj. Malthaner
   */
  font_t * tf;

  /**
   * Title color
   * @author Hj. Malthaner
   */
  color_t color;

  /**
   * Calculate drawable size from text and font
   * @author Hj. Malthaner
   */
  void recalc_size();

 public:

  /**
   * Set tooltip title and text
   * @author Hj. Malthaner
   */
  void set_text(const char * title, const char * text, color_t & color);

  maptooltip_t();

  virtual ~maptooltip_t();

  virtual void draw(graphics_t *gr, koord pos);
};

#endif // maptooltip_t_h
