/* Copyright by Hj. Malthaner */

#include "fontdef.h"

#include "message_log_view_t.h"
#include "model/message_log_t.h"
#include "swt/graphics_t.h"
#include "swt/font_t.h"
#include "swt/color_t.h"

void message_log_view_t::set_model(message_log_t * model)
{
  this->model = model;
  model->set_view(this);
}



message_log_view_t::message_log_view_t()
{
  model = 0;
  font = font_t::load(FONT_STD);
}


void message_log_view_t::draw(graphics_t *gr, koord screenpos)
{
  if(model) {
    const koord size = get_size(); 
    const int lines = size.y/font->get_height();

    const koord pos = 
      get_pos() 
      + screenpos 
      + koord(0, (size.y - lines*font->get_height()) >> 1);


    for(int line=0; line < lines; line ++) {
      font->draw_string(gr, model->get_tail(lines-line-1), 
			pos.x+8, pos.y+line*font->get_height(),
			0,
			color_t::BLACK);
    }
  }
}
