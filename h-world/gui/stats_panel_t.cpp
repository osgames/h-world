/* 
 * stats_panel_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "stats_panel_t.h"
#include "util/property_t.h"
#include "swt/graphics_t.h"
#include "swt/font_t.h"
#include "swt/color_t.h"
#include "tpl/handle_tpl.h"

// #include "control/scheduler_t.h"

#include "model/thing_t.h"
#include "model/thing_constants.h"

#include "fontdef.h"

stats_panel_t::stats_panel_t() : 
  display(props_display_t::stats, FONT_STD, true)
{
  // fixfont  = font_t::load("data/hajo7x10bold.hex", false, 7, 10);
  propfont = font_t::load(FONT_STD);
  propfont_small = font_t::load(FONT_SML);

  set_opaque(false);

  display.set_pos(koord(0,100));
  display.set_size(koord(140,200));
  
  add_component(&display);
}


/**
 * Registers this panel as listener on the model
 * @author Hj. Malthaner
 */
void stats_panel_t::link(const handle_tpl <thing_t> &model)
{
  model->add_property_listener(this);

  // Hajo: display does not store model, only pointer to
  // models properties
  display.mouse_over_thing(model);

  redraw();
}


/**
 * Draws the component
 * @author Hj. Malthaner
 */
void stats_panel_t::draw(graphics_t *gr, koord pos)
{
  propfont_small->draw_string(gr, "Welcome to the H-World",
		  pos.x+4, pos.y+8, 0, color_t::BLACK);  

  propfont_small->draw_string(gr, "CRPG engine, � 2001-2005",
		  pos.x+4, pos.y+20, 0, color_t::BLACK);  

  propfont_small->draw_string(gr, "by Hansj�rg Malthaner",
		  pos.x+4, pos.y+32, 0, color_t::BLACK);  

  propfont_small->draw_string(gr, "hansjoerg.malthaner@gmx.de", 
		  pos.x+4, pos.y+44, 0, color_t::BLACK);  

  gui_container_t::draw(gr, pos);

  /*
  char buf[32];
  sprintf(buf, "Time: %d", scheduler_t::get_instance()->get_time());

  propfont_small->draw_string(gr, buf, pos.x+4, pos.y+240, color_t::BLACK);  
  */
}


/**
 * Implements property listener
 * @author Hansj�rg Malthaner
 */
void stats_panel_t::update(const property_t *prop)
{
  redraw();
}


