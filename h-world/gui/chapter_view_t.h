/* 
 * chapter_view_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef chapter_view_t_h
#define chapter_view_t_h

#include "swt/gui_component_t.h"

class thing_t;
class html_view_t;
template <class T> class handle_tpl;
template <class T> class minivec_tpl;


/**
 * The chapter shows all memorized messages from one category
 * @author Hj. Malthaner
 */
class chapter_view_t : public gui_component_t
{
 private:

  const char * current_catg;

  const handle_tpl <thing_t> & model;

  minivec_tpl <html_view_t *> * views;

 public:

  /**
   * Show all messages for this category
   * @author Hj. Malthaner
   */
  chapter_view_t(const handle_tpl <thing_t> & model, const char * catg);

  virtual ~chapter_view_t();


  void change_catg(const char * catg);

  void show_page();


  /**
   * Draws the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);
};

#endif // chapter_view_t_h
