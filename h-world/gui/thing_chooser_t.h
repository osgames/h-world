/* 
 * thing_chooser_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef THING_CHOOSER_T_H
#define THING_CHOOSER_T_H
   
#ifndef modal_frame_t_h
#include "modal_frame_t.h"
#endif

#ifndef THING_T_H
#include "model/thing_t.h"
#endif

#ifndef thinghandle_t_h
#include "model/thinghandle_t.h"
#endif

#ifndef ACTION_LISTENER_T_H
#include "swt/ifc/action_listener_t.h"
#endif


class font_t;
class hw_button_t;

template <class T> class slist_tpl;

/**
 * Lets the user choose one item
 *
 * @autor Hj. Malthaner
 */
class thing_chooser_t : public modal_frame_t, action_listener_t
{
 private:

  slist_tpl <thinghandle_t> * packs;
  slist_tpl <thinghandle_t> * items;
  slist_tpl <hw_button_t *> * buttons;

  font_t *font;

  thinghandle_t choice;


  int w_left;
  int w_right;


 public:


  thinghandle_t get_choice();


  /**
   * Lets the player choose one thing from the tree
   *
   * @autor Hj. Malthaner
   */
  thing_chooser_t(const char * title,
		  thinghandle_t model,
		  const char *ident,
		  const char *type,
		  const char *subtype);

  virtual ~thing_chooser_t();

  virtual void action_triggered(gui_action_trigger_t * trigger);
};


#endif
