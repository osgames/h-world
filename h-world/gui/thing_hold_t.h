/*
 * thing_hold_t.h
 *
 * Copyright (c) 2001 Hansj�rg Malthaner
 *
 */

#ifndef THING_HOLD_T_H
#define THING_HOLD_T_H
   
#include "primitives/koord.h"
#include "swt/drop_source_t.h"
#include "swt/drop_target_t.h"
#include "model/thinghandle_t.h"

class font_t;
class inventory_view_listener_t;
template <class T> class slist_tpl;

/**
 * 
 *
 * @autor Hj. Malthaner
 * @version $Revision$
 */
class thing_hold_t : public drop_target_t, drop_source_t
{
private:
  enum drop_state_t {neutral, allowed, denied};
  drop_state_t drop_state;

  thinghandle_t & limb;
  koord size;
  font_t *font;

  slist_tpl <inventory_view_listener_t *> * listeners;

  drawable_t * old_item;

  char title[64];

  void fire_mouse_over_thing(const handle_tpl <thing_t> &t);

public:

  thing_hold_t(thinghandle_t &limb);
  ~thing_hold_t();

  void add_listener(inventory_view_listener_t *l);

  void set_model(thinghandle_t &limb);


  /**
   * Paints the component
   * @author Hj. Malthaner
   */
  virtual void draw(graphics_t *gr, koord pos);


  /**
   * Implements drop_target_t
   * @author Hansj�rg Malthaner
   */
  virtual bool drop(drawable_t * item, koord at);


  /**
   * called if item is dragged over the drop target, but not yet dropped
   * @author Hj. Malthaner
   */
  virtual void hover(drawable_t * item, koord at);


  /**
   * If dragging is stopped, the item needs to be put back
   * @author Hj. Malthaner
   */
  virtual void stop_dragging(drawable_t *item);


  /**
   * Process an event.
   * @return true if event was swallowed, false otherwise
   * @author Hj. Malthaner
   */
  virtual bool process_event(const event_t &);
};

#endif
