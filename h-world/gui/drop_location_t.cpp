/* 
 * drop_location_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */



#include "drop_location_t.h"
#include "thing_drag_t.h"

#include "model/world_t.h"
#include "model/level_t.h"
#include "model/square_t.h"
#include "model/thing_t.h"
#include "model/thinghandle_t.h"

#include "swt/font_t.h"
#include "swt/color_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"

#include "language/linguist_t.h"

#include "fontdef.h"


drop_location_t::drop_location_t(koord p) {
  pos = p;

  font = font_t::load(FONT_STD);
  set_size(koord(64,32));
}



bool drop_location_t::drop(drawable_t * item, koord at) {

  level_t *level = world_t::get_instance()->get_level();


  thing_drag_t *drag = dynamic_cast<thing_drag_t *>(item);
  
  
  if(drag) {
    const thinghandle_t & tmp = drag->get_thing();

    linguist_t::pvo("You", "drop", false, 
		    tmp->get_string("ident", "unnamed"));

    level->at(pos)->add_thing(tmp, pos);
  }

  return true;
}


/**
 * Draws the component
 * @author Hj. Malthaner
 */
void drop_location_t::draw(graphics_t *gr, koord screenpos) {
  const koord pos = get_pos()+screenpos;
  const koord & size = get_size();
  painter_t pt (gr);

  pt.box_wh(pos, size, color_t::GREY64);

  font->draw_string(gr, "Drop item",
		    pos.x+2, pos.y+2, 0, color_t::BLACK);
}
