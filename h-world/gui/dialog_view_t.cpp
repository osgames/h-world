/* 
 * dialog_view_t.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>


#include "dialog_view_t.h"
#include "html_view_t.h"
#include "hw_button_t.h"
#include "dialog_change_listener_t.h"

#include "swt/color_t.h"
#include "swt/font_t.h"
#include "swt/graphics_t.h"
#include "swt/event_t.h"

#include "fontdef.h"

#include "model/thing_t.h"
#include "model/dialog_t.h"
#include "control/lua_call_t.h"
#include "control/action_trade_t.h"

#include "util/debug_t.h"
#include "util/sections_t.h"
#include "util/properties_t.h"


static const char * ltrim(const char *p)
{
  while(*p != 0 && *p < 32) {
    p++;
  }

  return p;
}


void dialog_view_t::make_buttons()
{
  for(int i=0; i<BUTTONS; i++) { 
    reply[i] = new hw_button_t("", 'a');
    reply[i]->border = 1;
  }
}


void dialog_view_t::kill_buttons()
{
  for(int i=0; i<BUTTONS; i++) { 
    reply[i]->remove_listener(this);
    reply[i]->active = false;
    remove_component(reply[i]);
  }
}


void dialog_view_t::set_dialog_change_listener(dialog_change_listener_t *l)
{
  listener = l;
}


void dialog_view_t::update_dialog()
{
  if(model && current) {
    // printf("'%s'\n", ltrim(model->speech));
    const int LINESPACE = 18;

    properties_t * props = model->get_section(current);

    if(props) {
      speech->set_text(props->get_string("NPC"));

      const koord speech_size = speech->get_preferred_size();

      printf("speech_size=%d,%d\n", speech_size.x, speech_size.y);

      // speech->set_size(speech_size);

      kill_buttons();

      int i = 0;
      const char * text = 0;
      char key[4];
      char buf[128];

      do{
	// build key;
	key[0] = 'a';
	key[1] = '1' + i;
	key[2] = 0;
	
	text = props->get_string(key);

	if(text) {

          const char hot = 'a'+i; 
	  sprintf(buf, "%c: %s", hot, text);

	  reply[i]->set_text(buf);
	  reply[i]->hotkey = hot;
	  reply[i]->set_pos(koord(12, speech_size.y + 8 + i*LINESPACE));
	  reply[i]->set_size(koord(speech_size.x-32, LINESPACE-1));	
	  reply[i]->text_offset = koord(8,2);
	  reply[i]->add_listener(this);

	  add_component(reply[i]);

	  i++;
	} 

      } while(text);
      
      redraw();

    } else {
      speech->set_text("Dialog section not found!");
      dbg->warning("dialog_view_t::update_dialog()", 
		   "section '%s' not found", current);	    

    }

  } else {
    speech->set_text("null");
  }
}


dialog_view_t::dialog_view_t(sections_t *d, 
			     handle_tpl <thing_t> &t,
			     handle_tpl <thing_t> &c
			     ) : talker(t), customer(c)
{
  model = d;
  listener = 0;
  current = "main";

  speech = new html_view_t();
  speech->set_text("null");

  add_component(speech);

  make_buttons();
}


dialog_view_t::~dialog_view_t()
{
  current = 0;
  listener = 0;

  delete(model);
  model = 0;

  delete speech;
  speech = 0;

  for(int i=0; i<BUTTONS; i++) { 
    remove_component(reply[i]);
    delete reply[i];
    reply[i] = 0;
  }
}


/**
 * Sets the size
 * @author Hj. Malthaner
 */
void dialog_view_t::set_size(koord size)
{
  gui_container_t::set_size(size);
  speech->set_size(size);
}


void dialog_view_t::action_triggered(gui_action_trigger_t * trigger)
{
  unsigned int choice = 0xFFFF;

  for(int i=0; i<16; i++) { 
    // printf("Reply pos[%d]=%d, my=%d\n", i, reply_pos[i], ev.cpos.y);

    if(reply[i] == trigger) {
      choice = i;
      break;
    }
  }

  dbg->message("dialog_view_t::action_triggered()", 
	       "Button %d pressed, current=%s", choice, current);	    

  
  char key[4];

  // build key;
  key[0] = 'a';
  key[1] = '1' + choice;
  key[2] = 0;


  if(current) { 
    properties_t * props = model->get_section(current);
  
    const char * text = props->get_string(key);

    if(text) {
      action_trade_t *action = 0;
      char buf[32];
      
      sprintf(buf, "%s.hook", key);

      const char * hook = props->get_string(buf);

      
      if(hook) {
	if(strcmp(hook, "Trade") == 0) {
	  
	  action = dynamic_cast <action_trade_t *> (talker->get_actor().find_action_for(action_t::trade));
	  
	  // Hajo: no more talk
	  current = 0;
	}      

	if(strcmp(hook, "Lua") == 0) {
	  sprintf(buf, "%s.function", key);
	  const char * function = props->get_string(buf);
	  
	  if(function) {
	    lua_call_t call(function);
	    call.activate(customer, talker);
	  }
	  
	  sprintf(buf, "%s.next", key);
	  current = props->get_string(buf);
	}
	
      } else {
	sprintf(buf, "%s.next", key);
	current = props->get_string(buf);
      }
      
      dbg->message("dialog_view_t::process_event()", 
		   "current section is now '%s'", current);	    
      
      
      if(current) {
	update_dialog();
      } else {
	for(int i=0; i<BUTTONS; i++) { 
	  reply[i]->active = false;
	}
      }
      
      if(listener) {
	listener->on_dialog_changed(current);
      }

      if(action) {
	action->set_customer(customer);
	action->execute();
      }
    }
  }
}

