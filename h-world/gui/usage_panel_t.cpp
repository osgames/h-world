/* 
 * usage_panel_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <ctype.h>
#include <string.h>

#include "primitives/area_t.h"

#include "usage_panel_t.h"
#include "model/inventory_t.h"
#include "model/world_t.h"
#include "model/thing_t.h"
#include "model/thing_constants.h"
#include "model/thing_traversor_t.h"

#include "control/thing_usage_selector_t.h"
#include "control/actor_t.h"
#include "control/scheduler_t.h"

#include "control/usage_t.h"
#include "control/attack_t.h"
#include "swt/event_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"
#include "swt/font_t.h"
#include "swt/color_t.h"
#include "util/debug_t.h"

#include "fontdef.h"

#include "tpl/pair_tpl.h"


static bool has_non_attack_usage(const thinghandle_t & thing)
{
  bool ok = false;

  slist_tpl <usage_t *> & usages = thing->get_usages();  

  slist_iterator_tpl <usage_t *> iter (usages);

  while(iter.next()  && !ok) {
    usage_t * usg = iter.get_current(); 
    attack_t * atk = dynamic_cast <attack_t *> (usg);

    if(atk == 0 && usg != 0) {
      ok = true;
    }
  }
  
  return ok;
}


usage_panel_t::usage_panel_t()
{
  font = font_t::load(FONT_SML);

  set_opaque( false );

  // reset default attack
  def_atk = 0;

  // reset default usage
  def_usg = 0;

  sel_atk = -1;
  sel_usg = -1;
}


void usage_panel_t::link(const handle_tpl <thing_t> &model)
{
  model->add_listener(this);
  update(model);
}


/**
 * Get selected attack
 * @author Hj. Malthaner
 */
usage_t *usage_panel_t::get_selected_attack()
{
  return attacks.at(def_atk);
}


/**
 * Make next attack the default attack
 * @author Hj. Malthaner
 */
void usage_panel_t::def_atk_next()
{
  def_atk = (def_atk + 1) % attacks.count();
  redraw();
}


/**
 * Make previous attack the default attack
 * @author Hj. Malthaner
 */
void usage_panel_t::def_atk_prev()
{
  def_atk = (def_atk - 1);

  if(def_atk > attacks.count()) {
    def_atk = attacks.count() - 1;
  }

  redraw();
}


/**
 * Get selected usage
 * @author Hj. Malthaner
 */
handle_tpl <thing_t> usage_panel_t::get_selected_item_for_use()
{
  handle_tpl <thing_t> result (0);

  if(def_usg >= 0 
     && def_usg < items_for_use.count()
     ) {
     result = items_for_use.at(def_usg);  
  }

  return result;
}


/**
 * Make next usage the default usage
 * @author Hj. Malthaner
 */
void usage_panel_t::def_usg_next()
{
  def_usg = (def_usg + 1) % items_for_use.count();

  redraw();
}


/**
 * Make previous usage the default usage
 * @author Hj. Malthaner
 */
void usage_panel_t::def_usg_prev()
{
  def_usg = (def_usg - 1);
  if(def_usg < 0) {
    def_usg = items_for_use.count()-1;
  }

  redraw();
}



/**
 * Get a usage with certain preperties
 * @see policy_t
 * @author Hj. Malthaner
 */
usage_t * usage_panel_t::get_usage(const int effect)
{
  slist_iterator_tpl <thinghandle_t> iter (items_for_use);

  while(iter.next()) {
    slist_tpl <usage_t *> & usages = iter.get_current()->get_usages();

    slist_iterator_tpl <usage_t *> iter2 (usages);

    while( iter2.next() ) {
      usage_t *u = iter2.get_current();

      // printf("%x %x\n", u->get_policy().effect, effect);

      if( (u->get_policy().effect & effect) ) {
	return u;
      }
    }
  }

  return 0;
}


void usage_panel_t::update(const handle_tpl <thing_t> &thing)
{
  dbg->message("usage_panel_t::update()", "called"); 

  thing_traversor_t trav;
  trav.set_model(thing);
  trav.set_limb_visitor(this);
  trav.set_thing_visitor(this);


  // clear our usage list
  items_for_use.clear();
  attacks.clear();

  button_pos = koord(10,20);

  // build new list
  trav.traverse();


  // check for valid range
  if(def_atk >= (unsigned int)attacks.count()) {
    def_atk = attacks.count() - 1; 
  }

  if(def_usg >= items_for_use.count()) {
    def_usg = items_for_use.count() - 1;
  }

  // might happen if there are no usages at all
  if(def_usg < 0) {
    def_usg = 0;
  }

  redraw();
}


void usage_panel_t::visit_thing(const handle_tpl <thing_t> &thing)
{
}


/**
 * Implements limb_visitor_t
 * @author Hj. Malthaner
 */
void usage_panel_t::visit_limb(const handle_tpl <thing_t> &limb)
{
  /*
  dbg->message("usage_panel_t::visit_limb()", "%s (%s)", 
	       limb->get_string("ident", "unnamed"),
	       limb->get_string("name", "unnamed")); 
  */

  const handle_tpl <thing_t> & thing = limb->get_item();

  // check attacks given by limbs

  // only available if limb does not hold an item

  if(!thing.is_bound() || 
     (limb->get_string("usages_always", 0) && 
      *thing->get_string("alive", "f") != 't')) {

    slist_tpl <usage_t *> & thing_usages = limb->get_usages();
    slist_iterator_tpl <usage_t *> iter (thing_usages);
  
    while(iter.next()) {
      usage_t * usg = iter.get_current();
      attack_t *atk = dynamic_cast <attack_t *> (usg);

      if(atk) {
	attacks.append(atk);
      }
      // dbg->message("usage_panel_t::visit_limb()", "Usage %s %s", 
      //  	   usg->get_name(), usg->get_verb());
    }

    if(thing_usages.count() > 0 && has_non_attack_usage(limb)) {
      items_for_use.append(limb);
    }
  }


  // check attacks/usages from items
  if(thing.is_bound() && *thing->get_string("alive", "f") != 't') {

    slist_tpl <usage_t *> & thing_usages = thing->get_usages();
    slist_iterator_tpl <usage_t *> iter (thing_usages);
  
    while(iter.next()) {
      usage_t * usg = iter.get_current();
      attack_t *atk = dynamic_cast <attack_t *> (usg);

      if(atk) {

	button_pos += koord(100,0);

	attacks.append(atk);
      }

      // dbg->message("usage_panel_t::visit_limb()", "Usage %s %s", 
      //	   usg->get_name(), usg->get_verb());
    }

    if(thing_usages.count() > 0 && has_non_attack_usage(thing)) {
      items_for_use.append(thing);
    }


    // check usages from items in 'quick' inventories

    inventory_t * inv = thing->get_inventory();

    if(inv && thing->get_string(TH_QUICK_INV, 0) ) {
      slist_tpl <thinghandle_t> inv_items;
	
      // inv->add_listener(this);
      inv->list_things(&inv_items);
      

      slist_iterator_tpl <thinghandle_t> inv_iter (inv_items);
      
      while( inv_iter.next() ) {
	
	slist_tpl <usage_t *> & thing_usages = 
	  inv_iter.get_current()->get_usages();


	if(thing_usages.count() > 0 && has_non_attack_usage(inv_iter.get_current())) {
	  items_for_use.append(inv_iter.get_current());
	}
      }
    }
  }
}



/**
 * Implements inventory_listener_t
 * @author Hj. Malthaner
 */
void usage_panel_t::thing_added(handle_tpl < thing_t > &thing)
{
  // Hack: we know we're watching the player
  world_t * world = world_t::get_instance();
  update(world->get_player());
}


/**
 * Implements inventory_listener_t
 * @author Hj. Malthaner
 */
void usage_panel_t::thing_removed(handle_tpl < thing_t > &thing)
{
  // Hack: we know we're watching the player
  world_t * world = world_t::get_instance();
  update(world->get_player());
}



/**
 * Draws the component
 * @author Hj. Malthaner
 */
void usage_panel_t::draw(graphics_t *gr, koord pos)
{
  char buf[256];
  koord k;
  int i;
  painter_t pt (gr);

  k = pos + koord(10,20);

  pt.draw_string_bold(font, "Select attack (a, s)", k-koord(6,16), 1, color_t::BLACK, color_t::GREY64);

  attack_pos.clear();
  usage_pos.clear();

  slist_iterator_tpl <usage_t *> iter (attacks);

  i = 0;
  while(iter.next()) {
    attack_t *atk = dynamic_cast <attack_t *> (iter.get_current());

    if(atk) {

      attack_pos.append(area_t(k-koord(2,3)-pos, k+koord(62, 58)-pos));

      if((unsigned int)i == def_atk) {
	gr->fillbox_wh(k.x-1, k.y-2, 62, 59, color_t::GREY192);
	pt.ddd_box_wh(k - koord(2,3), koord(64, 61), color_t::GREY64, color_t::WHITE);

      } else {
	// gr->fillbox_wh(k.x-1, k.y-2, 62, 59, color_t::GREY224);
      }

      if(i == sel_atk) {
	painter_t pt (gr);
	pt.ddd_box_wh(k - koord(2,3), koord(64, 61), color_t::WHITE, color_t::GREY64);
      }

      font->draw_string(gr, atk->get_name(), k.x, k.y-2, 0, color_t::BLACK);  

      font->draw_string(gr, "Impact:", k.x, k.y+9, 0, color_t::BLACK);  
      sprintf(buf, "%dd%d", atk->get_blunt().dice, atk->get_blunt().sides);
      font->draw_string(gr, buf, k.x+41, k.y+9, 0, color_t::BLACK);  

      font->draw_string(gr, "Cut:", k.x, k.y+21, 0, color_t::BLACK);  
      sprintf(buf, "%dd%d", atk->get_cut().dice, atk->get_cut().sides);
      font->draw_string(gr, buf, k.x+41, k.y+21, 0, color_t::BLACK);  

      font->draw_string(gr, "Pierce:", k.x, k.y+33, 0, color_t::BLACK);  
      sprintf(buf, "%d%%", atk->get_pierce()); 
      font->draw_string(gr, buf, k.x+41, k.y+33, 0, color_t::BLACK);  

      font->draw_string(gr, "Delay:", k.x, k.y+45, 0, color_t::BLACK);  
      sprintf(buf, "%d", atk->get_delay()); 
      font->draw_string(gr, buf, k.x+41, k.y+45, 0, color_t::BLACK);  
    }

    i++;

    k += koord(80, 0);
  }

  if(k.x < 180) {
    k.x = 180;
  } else {
    k.x += 10;
  }

  const koord usage_start = k;

  pt.draw_string_bold(font, "Select to use (q, w, u)", 
		      k - koord(6, 16), 1, color_t::BLACK, color_t::GREY64);

  slist_iterator_tpl <thinghandle_t> iter2 (items_for_use);
  i = 0;

  while(iter2.next()) {
    iter2.get_current()->get_ident_plain(buf);

    buf[0] = toupper(buf[0]);
    buf[255] = 0;

    // Hajo: truncate too long descriptions ...
    if(strlen(buf) > 16) {
      buf[15] = '.';
      buf[16] = '.';
      buf[17] = '.';
      buf[18] = '\0';
    }


    const int width = gr->get_font()->get_string_width(buf, 0);

    usage_pos.append(area_t(k-koord(2,3)-pos, k+koord(width+2, 9)-pos));


    if(def_usg == i) {
      painter_t pt (gr);
      gr->fillbox_wh(k.x-1, k.y-2, width+4, 12, color_t::GREY192);
      pt.ddd_box_wh(k - koord(2,3), koord(width+6, 14), color_t::GREY64, color_t::WHITE);
    } else {
      // gr->fillbox_wh(k.x-1, k.y-2, width+4, 12, color_t::GREY224);
    }

    if(sel_usg == i) {
      painter_t pt (gr);
      pt.ddd_box_wh(k - koord(2,3), koord(width+6, 14), color_t::WHITE, color_t::GREY64);
    }

    font->draw_string(gr, buf, k.x, k.y-2, 0, color_t::BLACK);  

    k += koord(width + 16, 0);

    if(k.x >= get_size().x - 96) {
      k.x = usage_start.x;
      k.y += 16;
    }
    
    i ++;

  }

  gui_container_t::draw(gr, pos);
}


static void use_item(usage_panel_t * panel)
{
  thinghandle_t item = panel->get_selected_item_for_use();
  thinghandle_t user = world_t::get_instance()->get_player();

  // Hajo: let player select an usage
  thing_usage_selector_t choice;

  const int delay = choice.select_and_activate(user, 
					       item->get_container(),
					       item);
	
  actor_t * actor = scheduler_t::get_instance()->get_current_actor();

  // Hajo: Check if still alive
  if(actor) {
    actor->add_delay(delay);
  }
}


/**
 * Process an event.
 * @return true if event was swallowed, false otherwise
 * @author Hj. Malthaner
 */
bool usage_panel_t::process_event(const event_t &ev)
{
  gui_container_t::process_event(ev);
  bool swallowed = false;

  const bool old_selection = (sel_atk != -1 || sel_usg != -1);

  sel_atk = -1;
  sel_usg = -1;


  if(ev.type == event_t::BUTTON_PRESS) {
    
    int i = 0;
    slist_iterator_tpl <area_t> aiter (attack_pos);

    while( swallowed == false && aiter.next() ) {
      const area_t & a = aiter.get_current();

      // printf("Click at %d %d, area.tl is %d %d\n", ev.cpos.x, ev.cpos.y, a.tl.x, a.tl.y);  

      if(a.tl.x <= ev.cpos.x && a.tl.y <= ev.cpos.y &&
	 a.br.x >= ev.cpos.x && a.br.y >= ev.cpos.y) {
	
	def_atk = i;
	swallowed = true;
	dbg->message("usage_panel_t::process_event()", "Selecting attack %d", i); 
      }

      i++;
    }

    i = 0;
    slist_iterator_tpl <area_t> uiter (usage_pos);

    while( swallowed == false && uiter.next() ) {
      const area_t & a = uiter.get_current();

      if(a.tl.x <= ev.cpos.x && a.tl.y <= ev.cpos.y &&
	 a.br.x >= ev.cpos.x && a.br.y >= ev.cpos.y) {
	
	def_usg = i;
	swallowed = true;
	dbg->message("usage_panel_t::process_event()", "Selecting usage %d", i); 
	if(ev.code == event_t::BUTTON_RIGHT) {
	  dbg->message("usage_panel_t::process_event()", "Using %d", i);
 	  use_item(this);
	}
      }

      i++;
    }

    if(swallowed) {
      redraw();
    }
    
    // Hajo: We consume all clicks
    swallowed = true;
  }


  if(ev.type == event_t::BUTTON_RELEASE) {
    // Hajo: We consume all clicks
    swallowed = true;
  }


  if(ev.type == event_t::MOUSE_MOVE) {

    // printf("Move to %d %d\n", ev.mpos.x, ev.mpos.y);  
    
    int i = 0;
    slist_iterator_tpl <area_t> aiter (attack_pos);

    while( swallowed == false && aiter.next() ) {
      const area_t & a = aiter.get_current();

      if(a.tl.x <= ev.mpos.x && a.tl.y <= ev.mpos.y &&
	 a.br.x >= ev.mpos.x && a.br.y >= ev.mpos.y) {
	
	sel_atk = i;
	swallowed = true;
	// dbg->message("usage_panel_t::process_event()", "Cursor on attack %d", i); 
      }

      i++;
    }

    i = 0;
    slist_iterator_tpl <area_t> uiter (usage_pos);

    while( swallowed == false && uiter.next() ) {
      const area_t & a = uiter.get_current();

      if(a.tl.x <= ev.mpos.x && a.tl.y <= ev.mpos.y &&
	 a.br.x >= ev.mpos.x && a.br.y >= ev.mpos.y) {
	
	sel_usg = i;
	swallowed = true;
	// dbg->message("usage_panel_t::process_event()", "Cursor on usage %d", i); 
      }

      i++;
    }

    if(swallowed) {
      redraw();
    }
    
    // Hajo: We consume all clicks
    swallowed = true;
  }

  const bool new_selection = (sel_atk != -1 || sel_usg != -1);

  if(old_selection != new_selection) {
    redraw();
  }

  return swallowed;
}
