#include "map_view_t.h"

#include "swt/graphics_t.h"

#include "model/map_model_t.h"

#include "view/world_view_t.h"


map_view_t::map_view_t()
{
}


/**
 * Draws the component
 * @author Hj. Malthaner
 */
void map_view_t::draw(graphics_t *gr, koord screenpos)
{
  const koord pos = get_pos()+screenpos;

  gr->draw_image(map_model_t::get_instance()->get_image(), pos.x, pos.y);
}
