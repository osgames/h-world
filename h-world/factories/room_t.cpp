/* 
 * room_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "room_t.h"
#include "external_t.h"
#include "hdoor_creator_t.h"
#include "vdoor_creator_t.h"
#include "feature_factory_t.h"
#include "thing_factory_t.h"

#include "model/thing_t.h"
#include "model/feature_t.h"
#include "model/feature_door_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/pprops_t.h"
#include "util/debug_t.h"



/**
 * Basic constructor.
 * @param level the level to build the room in
 * @param a room area
 * @param floor floor image number
 * @param wall wall feature type
 * @param keep_entries don't build walls on empty squares
 * @author Hansj�rg Malthaner
 */
room_t::room_t(level_t * level,
	       const area_t & a, 
	       int floor, 
	       const char * wall,
	       bool keep_entries) : structure_t(a)
{
  // dbg->message("rooms_t::room_t()", "new room at %d %d - %d %d", a.tl.x, a.tl.y, a.br.x, a.br.y);
  feature_factory_t feature_factory;

  feature_t * feat_wall = feature_factory.create(wall);
  feat_wall->visual.set_y_off(0);
  feat_wall->visual.set_x_off(0);

  set_wall_feat(feat_wall);

  set_ground_look(floor);

  area_iterator_t iter (area);

  while(iter.next()) {
    const koord k = iter.get_current();

    if(k.x == a.tl.x || 
       k.y == a.tl.y ||
       k.x == a.br.x || 
       k.y == a.br.y) {

      if(keep_entries && level->at(k)->get_feature().is_bound() == false) {
	set_grid_at(k, structure_t::open);
      } else {
	set_grid_at(k, structure_t::wall);
      }

    } else {
      set_grid_at(k, structure_t::open);
      level->at(k)->room_type = 0;
    }
  }


  // creates just an empty walled sqaure
  type = plain;
}


void room_t::choose_type()
{
  const int d = abs(area.get_width() - area.get_height());
  const int min = area.get_width() < area.get_height() ? area.get_width() : area.get_height();
  const int max = area.get_width() > area.get_height() ? area.get_width() : area.get_height();

  // printf("Room: min=%d, max=%d, diff=%d\n", min, max, d);

  type = big;

  if(min > 9) {
    if(d > 15 && min < 19) {
      type = hallway;
    } else if((d > 6 || min > 15) && max < 38) {
      type = chambers;
    } else {
      type = big;
    }
  }
}


/**
 * Realizes this room in the level
 * @author Hansj�rg Malthaner
 */
void room_t::realize(level_t *level)
{
  structure_t::realize(level);

  switch(type) {
  case big:
    realize_simple(level);
    break;
  case hallway:
    realize_hallway(level);
    break;
  case chambers:
    realize_chambers(level);
    break;
  default:
    // realize_simple(level);
    break;
  }
}


// place a door
bool room_t::realize_door(level_t *level, koord k)
{
  // no doors in outer walls!

  if(k.x == 0 || k.x == level->get_size().x-1 ||
     k.y == 0 || k.y == level->get_size().y-1) { 
    return false;
  }


  properties_t * props = level->get_properties();


  if(k.x == area.tl.x || k.x == area.br.x) { 

    const char * open = props->get_string("vdoor.open", "c_vdoor.open");
    const char * closed = props->get_string("vdoor.closed", "c_vdoor.closed");
    const char * vtop = props->get_string("vdoor.top", "c_vdoor.top");
    const char * center = closed;
    const char * vbot = props->get_string("vdoor.bot", "c_vdoor.bot"); 

    vdoor_creator_t::realize(level,
			     open, closed, 
			     -40, 0,
			     vtop, center, vbot,
			     k);

  } else {

    const char * open = props->get_string("hdoor.open", "c_hdoor.open");
    const char * closed = props->get_string("hdoor.closed", "c_hdoor.closed");
    const char * left = props->get_string("hdoor.left", "c_hdoor.left");
    const char * center = closed;
    const char * right = props->get_string("hdoor.right", "c_hdoor.right"); 

    hdoor_creator_t::realize(level,
			     open, closed, 
			     +40, 0,
			     left, center, right,
			     k);
  }

  return true;
}


void room_t::realize_simple(level_t *level)
{
  koord zv, p1, p2;


  if(area.get_width() > area.get_height()) {
    zv = koord(3,0);
    p1 = area.tl+koord(2,2);
    p2 = koord(area.tl.x+2, area.br.y-2);

    realize_door(level, koord(area.tl.x, (short)((area.tl.y+area.br.y)/2)));
    realize_door(level, koord(area.br.x, (short)((area.tl.y+area.br.y)/2)));

  } else {
    zv = koord(0,3);
    p1 = area.tl+koord(2,2);
    p2 = koord(area.br.x-2, area.tl.y+2);

    realize_door(level, koord((short)((area.tl.x+area.br.x)/2), area.tl.y));
    realize_door(level, koord((short)((area.tl.x+area.br.x)/2), area.br.y));
  }
}


void room_t::realize_hallway(level_t *level)
{
  koord zv, p1, p2;
  properties_t *props = level->get_properties();
  const char * column = props->get_string("column");
  feature_factory_t feature_factory;


  if(column == 0) {
    dbg->fatal("room_t::realize_hallway()", 
	       "Missing 'column' string property in level template.");
  }


  if(area.get_width() > area.get_height()) {
    zv = koord(3,0);
    p1 = area.tl+koord(2,2);
    p2 = koord(area.tl.x+2, area.br.y-2);

    realize_door(level, koord(area.tl.x, (short)((area.tl.y+area.br.y)/2)));
    realize_door(level, koord(area.br.x, (short)((area.tl.y+area.br.y)/2)));

  } else {
    zv = koord(0,3);
    p1 = area.tl+koord(2,2);
    p2 = koord(area.br.x-2, area.tl.y+2);

    realize_door(level, koord((short)((area.tl.x+area.br.x)/2), area.tl.y));
    realize_door(level, koord((short)((area.tl.x+area.br.x)/2), area.br.y));
  }

  featurehandle_t feature (feature_factory.create(column));
  feature->visual.set_y_off(0);
  feature->visual.set_x_off(0);

  while(p1.x < area.br.x && p1.y < area.br.y) {

    level->at(p1)->set_feature(feature);
    level->at(p2)->set_feature(feature);

    p1 += zv;
    p2 += zv;
  }
}


void room_t::realize_chambers(level_t *level)
{
  dbg->message("room_t::realize_chambers()", "called");

  koord zv;
  area_t r1;
  area_t r2;

  if(area.get_width() > area.get_height()) {
	       
    zv = koord(0,7);
    koord p = koord((short)((area.tl.x + area.br.x)/2), area.tl.y);

    r1.tl=area.tl;
    r1.br=p+zv-koord(2,0);

    r2.tl=p+koord(2,0);
    r2.br=koord(area.br.x, (short)(area.tl.y+zv.y));

    
    realize_door(level, p-koord(1,0));
    realize_door(level, koord((short)(p.x-1), area.br.y));

  } else {
    zv = koord(7,0);

    koord p = koord(area.tl.x, (short)((area.tl.y + area.br.y)/2));

    r1.tl=area.tl;
    r1.br=p+zv-koord(0,2);

    r2.tl=p+koord(0,2);
    r2.br=koord((short)(area.tl.x+zv.x), area.br.y);

    realize_door(level, p-koord(0,1));
    realize_door(level, koord(area.br.x, (short)(p.y-1)));
  }  

  const char * wall = level->get_properties()->get_string("wall");

  while(r1.br.y < area.br.y && r1.br.x < area.br.x) {
    room_t room1 (level, r1, ground_look, wall, false);
    room_t room2 (level, r2, ground_look, wall, false);
    
    room1.realize(level);
    room2.realize(level);

    const char * chamber = level->get_properties()->get_string("chamber");
    
    external_t ex1 (area_t(r1.tl+koord(1,1), r1.br+koord(1,1)), 
		    chamber,
		    external_t::topleft,
		    external_t::topleft);

    ex1.realize(level);

    external_t ex2 (area_t(r2.tl+koord(1,1), r2.br+koord(1,1)),
		    chamber,
		    external_t::topleft,
		    external_t::topleft);

    ex2.realize(level);

    if(area.get_width() > area.get_height()) {
      room1.realize_door(level, koord(r1.br - koord(0,3)));
      room2.realize_door(level, koord(r1.br + koord(4,-3)));
    } else {
      room1.realize_door(level, koord(r1.br - koord(3,0)));
      room2.realize_door(level, koord(r1.br + koord(-3,4)));
    }

    r1.tl += zv;
    r1.br += zv;
    
    r2.tl += zv;
    r2.br += zv;

  }
    
}
