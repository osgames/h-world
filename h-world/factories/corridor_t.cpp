/* 
 * corridor_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "corridor_t.h"
#include "hdoor_creator_t.h"
#include "vdoor_creator_t.h"

#include "util/nsow.h"
#include "util/rng_t.h"
#include "util/debug_t.h"

#include "model/square_t.h"
#include "model/level_t.h"
#include "model/feature_t.h"
#include "model/pprops_t.h"

#include "tpl/minivec_tpl.h" 

/**
 * Convert arbitary vector to pure horizontal or vertical
 * @author Hansj�rg Malthaner
 */
static koord calc_zv(koord k1, koord k2)
{
  // create pure coordinates in one step length

  koord zv ((k1.x == k2.x) ? 0 : (k1.x < k2.x) ? 1 : -1, 
	    (k1.y == k2.y) ? 0 : (k1.y < k2.y) ? 1 : -1);
	    

  // But never move diagonally
  if (zv.x != 0 && zv.y != 0) {

    if (rng_t::get_the_rng().get_int(2)) {
      zv.x = 0;
    } else {
      zv.y = 0;
    }
  }

  return zv;
}


/**
 * Basic constructor
 * @author Hansj�rg Malthaner
 */
corridor_t::corridor_t(const area_t & a) : structure_t(area_t())
{
  area = a;
}



/**
 * Realizes this structure in the level
 * @author Hansj�rg Malthaner
 */
void corridor_t::realize(level_t *level)
{
  // dbg->message("corridor_t::realize()", "%d %d - %d %d", area.tl.x, area.tl.y, area.br.x, area.br.y);

  const koord size = level->get_size();
  bool  prev_type = false; // false = open, true = wall

  koord k (area.tl);
  koord zv = calc_zv(k, area.br);
  koord tmp;

  rng_t & rng = rng_t::get_the_rng();

  // emergency breakout counter
  int count = 0; 

  // max minivec capacity is 255!
  minivec_tpl < koord> tunnel (250);
  minivec_tpl < koord> doors  (250);


  // Avoid excessivley long or otherwise hopeless corridors
  while (k != area.br && 
	 tunnel.count() < 250 && 
	 count ++ < 750) {
    
    const int distance = k.apx_distance_to(area.br);

    // Random direction changes or forced changes near destination
    if (distance <= 10 || rng.get_int(100) < 20) {

      // Get the correct direction
      zv = calc_zv(k, area.br);
      
      // Choose a random direction if far from destination
      if (distance > 10 && rng.get_int(100) < 20) {
	const int i = rng.get_int(4);

	zv = nsow[i];
      }
    }

    tmp = k + zv;


    // ensure valid coordinates
    while (tmp.x < 1 || tmp.y < 1 || 
	   tmp.x >= size.x-1 || tmp.y >= size.y-1) {

      zv = calc_zv(k, area.br);
      tmp = k + zv;
    }

    
    if(flags[tmp.x + tmp.y*size.x] == 0) {

      k = tmp;
      tunnel.append(k);

      if(level->at(k)->get_feature().is_bound()) {
	// normal tunnel
	prev_type = true;

      } else {
	// hit room or tunnel

	if(prev_type) {
	  area_t a (k-koord(1,1), k+koord(1,1));
	  
	  area_iterator_t iter (a);
	  
	  // Avoid another tunnel entering nearby
	  while( iter.next() ) {
	    const koord pos = iter.get_current();
	    flags[pos.x + pos.y*size.x] = 32;

	    // printf("%d, %d\n", pos.x, pos.y);
	  }

	  // add door
	  if(tunnel.count() > 1) {
	    doors.append(tunnel.get(tunnel.count()-2));
	  }
	}

	prev_type = false;
      }
    }
  }

  for(int i=0; i<tunnel.count(); i++) {
    square_t * square = level->at(tunnel.get(i));
    square->set_feature(0);
    square->room_type = square_t::corridor;
  }


  properties_t * props = level->get_properties();

  for(int i=0; i<doors.count(); i++) {
    const koord k = doors.get(i);

    const koord top (k - koord(0,1));
    const koord bot (k + koord(0,1));

    if(level->at(top) && level->at(top)->get_feature().is_bound() &&
       level->at(bot) && level->at(bot)->get_feature().is_bound()){

      const char * open = props->get_string("vdoor.open", "c_vdoor.open");
      const char * closed = props->get_string("vdoor.closed", "c_vdoor.closed");
      const char * vtop = props->get_string("vdoor.top", "c_vdoor.top");
      const char * center = closed;
      const char * vbot = props->get_string("vdoor.bot", "c_vdoor.bot"); 

      vdoor_creator_t::realize(level,
			       open, closed, 
			       -40, 0,
			       vtop, center, vbot,
			       top);
      
    } else {
      const koord left (k - koord(1,0));
      const koord right (k + koord(1,0));

      if(level->at(left) && level->at(left)->get_feature().is_bound() &&
	 level->at(right) && level->at(right)->get_feature().is_bound()) {

	const char * open = props->get_string("hdoor.open", "c_hdoor.open");
	const char * closed = props->get_string("hdoor.closed", "c_hdoor.closed");
	const char * left = props->get_string("hdoor.left", "c_hdoor.left");
	const char * center = closed;
	const char * right = props->get_string("hdoor.right", "c_hdoor.right"); 

	hdoor_creator_t::realize(level,
				 open, closed, 
				 +40, 0,
				 left, center, right,
				 k-koord(1,0));
      }
    }
  }
}



void corridor_t::realize(level_t *l, unsigned char *flags)
{
  this->flags = flags;
  realize(l);
}
