/* Copyright by Hj. Malthaner */

#ifndef ROOM_T_H
#define ROOM_T_H

#include "structure_t.h"
#include "tpl/handle_tpl.h"

class room_t : public structure_t {
public:

  enum type {plain, big, hallway, chambers};

private:
   
  enum type type;

  void realize_simple(level_t *level);
  void realize_hallway(level_t *level);
  void realize_chambers(level_t *level);

  /**
   * Place a door for this room at k.
   * @return false if k is part of the levels outer wall, true otherwise
   * @author Hansj�rg Malthaner
   */
  bool realize_door(level_t *level, koord k);


public:

  enum type get_type() const {return type;};


  /**
   * Basic constructor.
   * @param level the level to build the room in
   * @param a room area
   * @param floor floor image number
   * @param wall wall feature type
   * @param keep_entries don't build walls on empty squares
   * @author Hansj�rg Malthaner
   */
  room_t(level_t * level,
	 const area_t & a, int floor, const char * wall, bool keep_entries);


  void choose_type();


  /**
   * Realizes this room in the level
   * @author Hansj�rg Malthaner
   */
  virtual void realize(level_t *);

};
#endif //ROOM_T_H
