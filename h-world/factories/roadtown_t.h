/* 
 * roadtown_t.h
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef roadtown_t_h
#define roadtown_t_h


#ifndef koord_h
#include "primitives/koord.h"
#endif


class level_t;


/**
 * A town with roads
 * @author Hj. Malthaner
 */
class roadtown_t
{
 private:

  koord player_pos;

  void road(level_t * level, koord p1, int len, int dir);

 public:


  /**
   * Checks if a player position if given in one of the templates
   * @return player position or -1,-1 if no position is given
   * @author Hj. Malthaner
   */
  koord get_player_pos();


  roadtown_t();


  /**
   * Realizes this structure in the level
   * @return player position or -1,-1 if no player position could be
   * found in templates.
   * @author Hj. Malthanr
   */
  virtual void realize(level_t *);
};

#endif // roadtown_t_h
