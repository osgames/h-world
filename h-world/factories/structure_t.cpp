/* 
 * structure_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "structure_t.h"
#include "primitives/area_t.h"
#include "model/groundhandle_t.h"
#include "model/featurehandle_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/ground_t.h"
#include "model/feature_t.h"
#include "util/debug_t.h"



int structure_t::get_grid_at(koord pos) const
{
  if(pos.x >= area.tl.x && pos.x <= area.br.x && 
     pos.y >= area.tl.y && pos.y <= area.br.y) {

    pos -= area.tl;
    return grid[pos.y*area.get_width()+pos.x];
  } else {

    //    return wall;
    return open;
  } 
}


void structure_t::set_grid_at(koord pos, int f)
{
  if(pos.x >= area.tl.x && pos.x <= area.br.x && 
     pos.y >= area.tl.y && pos.y <= area.br.y) {

    pos -= area.tl;
    grid[pos.y*area.get_width()+pos.x] = f;
  }
}


void structure_t::realize_square(level_t *level, koord pos, int f)
{
  square_t * square = level->at(pos);

  if(ground_look >= 0) {
    groundhandle_t gr (new ground_t());
    gr->visual.set_image(ground_look);
    square->set_ground(gr);      
  }

  square->access_env().lightness = 0;
  square->access_env().def_lightness = 0;

	
  switch(f) {
  case wall:
    // wall
    square->set_feature(wall_feat);
    break;

  case open:
    // floor
    square->set_feature(0);
    break;

  default:
    dbg->fatal("structure_t::realize_square()", 
	       "Unknown feature %d, 0=wall, 1=floor", f);

  }
}


void structure_t::set_wall_feat(feature_t *wall_feat)
{
  this->wall_feat = wall_feat;
}


void structure_t::set_ground_look(int n)
{
  ground_look = n;
}


/**
 * Basic constructor
 * @author Hansj�rg Malthaner
 */
structure_t::structure_t(const area_t & a)
{
  area = a;

  // init look to 0
  ground_look=0;
  wall_feat = 0;

  const int n = a.get_width()*a.get_height();

  grid = new int [n];

  for(int i=0; i<n; i++) {
    grid[i] = 0;
  }
}


/**
 * Destructor
 * @author Hansj�rg Malthaner
 */
structure_t::~structure_t()
{
  delete [] grid;
  grid = 0;
}


/**
 * Returns true if this structure intersects with the area
 * @author Hansj�rg Malthaner
 */
bool structure_t::intersect(area_t & a)
{
  const int left = a.tl.x > area.tl.x ? a.tl.x : area.tl.x;
  const int top = a.tl.y > area.tl.y ? a.tl.y : area.tl.y;

  const int right = a.tl.x < area.tl.x ? a.tl.x : area.tl.x;
  const int bottom = a.tl.y < area.tl.y ? a.tl.y : area.tl.y;

  return left <= right && top <= bottom;
}


/**
 * Returns true if this structure intersects with the other structure
 * @author Hansj�rg Malthaner
 */
bool structure_t::intersect(structure_t & str)
{
  return intersect(str.area);
}


/**
 * Returns true if a inner space this structure intersects with the
 * other structure
 * @author Hansj�rg Malthaner
 */
bool structure_t::intersect_space(structure_t & str)
{
  // XXX todo
  return false;
}


/**
 * Realizes this structure in the level
 * other structure
 * @author Hansj�rg Malthaner
 */
void structure_t::realize(level_t *level)
{

  dbg->message("structure_t::realize()", 
  	       "realizing %d,%d - %d,%d, ground=%d", 
  	       area.tl.x, area.tl.y, area.br.x, area.br.y,
	       ground_look);


  area_iterator_t iter (area);

  while(iter.next()) {
    koord pos = iter.get_current();
    realize_square(level, pos, get_grid_at(pos));
  }

  //  dbg->message("structure_t::realize()", "done"); 
}
