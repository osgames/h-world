/* 
 * level_factory_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <string.h>

#include "level_factory_t.h"
#include "thing_factory_t.h"
#include "feature_factory_t.h"

#include "primitives/area_t.h"
#include "primitives/bitmap_t.h"

#include "control/trigger_t.h"

#include "model/dice_t.h"
#include "model/ground_t.h"
#include "model/thing_t.h"
#include "model/feature_t.h"
#include "model/feature_stairs_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/height_level_t.h"
#include "model/pprops_t.h"

#include "view/world_view_t.h"

#include "util/property_t.h"
#include "util/properties_t.h"
#include "util/rng_t.h"
#include "util/fov_t.h"
#include "util/fov_destination_t.h"
#include "util/debug_t.h"

#include "structure_t.h"
#include "cave_t.h"
#include "room_t.h"
#include "town_t.h"
#include "roadtown_t.h"
#include "forest_t.h"
#include "external_t.h"
#include "corridor_t.h"


static const unsigned char slope_img_tab[16] =
{
    14, 6, 7, 2, 4,
    11, 3, 12, 5, 1, 13, 10,
    0, 8, 9, 14
};


/**
 * @param k tile koordinate of ground to calculate
 * @author Hj. Malthaner 
 */
static void calc_slope_image(level_t *level, 
			     koord k, 
			     height_level_t *hl,
			     int height_scale)
{
  const koord size = level->get_size();

  // dbg->message("calc_slope_image()", "w=%d h=%d x=%d y=%d", size.x, size.y, k.x, k.y); 


  /* tiled version

  if(k.x >= 0 && k.y >= 0 && k.x < size.x && k.y < size.y) {
    const int slope = calc_slope(k);

    visual_t & v =  level->at(k)->get_ground()->visual;


    v.set_image(0x8000 + 
		ground_base + 
		slope_img_tab[slope]);

    v.set_y_off(get_height(k)*9);
  }

  */


  if(k.x >= 0 && k.y >= 0 && k.x <= size.x && k.y <= size.y) {

    ground_visual_t & v =  level->at(k)->get_ground()->visual;


    // v.set_image(0);

    v.shade[0] = hl->calc_shade(k);
    // v.shade[0] = 0;
    v.shade[1] = hl->calc_shade(k+koord(0, 1));
    // v.shade[1] = 0;
    v.shade[2] = hl->calc_shade(k+koord(1, 1));
    // v.shade[2] = 0;
    v.shade[3] = hl->calc_shade(k+koord(1, 0));
    // v.shade[3] = 0;

    v.height[0] = (((int)hl->get_height(k)) - 128) * height_scale;
    v.height[1] = (((int)hl->get_height(k+koord(0, 1))) - 128) * height_scale;
    v.height[2] = (((int)hl->get_height(k+koord(1, 1))) - 128) * height_scale;
    v.height[3] = (((int)hl->get_height(k+koord(1, 0))) - 128) * height_scale;

  }
}


static void create_height_level(level_t *level, int strength)
{
  const koord size = level->get_size();
  const int bright_base = level->get_properties()->get("lit") ? 20 : 0;
  height_level_t hl (bright_base, strength, size, 0.6, 24.0);
  const int height_scale = level->get_properties()->get_int("height_scale", 4);

  koord k;

  for(k.y=0; k.y<size.y; k.y++) {
    for(k.x=0; k.x<size.x; k.x++) {
      calc_slope_image(level, k, &hl, height_scale);
    }
  }
}


static int random_flip(int flip_rand)
{
  int flip_fix = 0;
  rng_t rng = rng_t::get_the_rng();

  if(flip_rand & external_t::FLIP_H && rng.get_int(100) < 50) {
    flip_fix |= external_t::FLIP_H;
  }
  
  if(flip_rand & external_t::FLIP_V && rng.get_int(100) < 50) {
    flip_fix |= external_t::FLIP_V;
  }

  return flip_fix;
}


static bool check_templates(level_t * level, 
			    const area_t & area,
			    int ground_image,
			    const char * wall_room,
			    bool excarvate)
{
  bool ok = false;

  area_t inner (area);
  inner.tl.x += 1;
  inner.tl.y += 1;
  inner.br.x -= 1;
  inner.br.y -= 1;

  minivec_tpl <int> matching_templates (200);

  properties_t *props = level->get_properties();

  const int * p = props->get_int("templates");

  char buf [128];
      

  if(p) {

    for(int i=0; i <*p; i++) { 
      int w_min = -1;
      int h_min = -1;
      int w_max = -1;
      int h_max = -1;

      const int * ip = 0;

      sprintf(buf, "template[%d].width.min", i);
      if((ip = props->get_int(buf))) {
	w_min = *ip;
      } else {
	dbg->fatal("check_templates()", 
		   "property '%s' is missing in level props", buf);
      }

      sprintf(buf, "template[%d].height.min", i);
      if((ip = props->get_int(buf))) {
	h_min = *ip;
      } else {
	dbg->fatal("check_templates()", 
		   "property '%s' is missing in level props", buf);
      }


      sprintf(buf, "template[%d].width.max", i);
      if((ip = props->get_int(buf))) {
	w_max = *ip;
      } else {
	dbg->fatal("check_templates()", 
		   "property '%s' is missing in level props", buf);
      }


      sprintf(buf, "template[%d].height.max", i);
      if((ip = props->get_int(buf))) {
	h_max = *ip;
      } else {
	dbg->fatal("check_templates()", 
		   "property '%s' is missing in level props", buf);
      }



      if(inner.get_width() >= w_min && inner.get_height() >= h_min &&
	 inner.get_width() <= w_max && inner.get_height() <= h_max) { 

	matching_templates.append(i);

      }
    }

    if(matching_templates.count() > 0) {

      rng_t & rng = rng_t::get_the_rng();

      const int choice = rng.get_int(matching_templates.count());
      const int i = matching_templates.get(choice); 

      sprintf(buf, "template[%d].chance", i);
      const int chance = *props->get_int(buf);
      
      if(rng_t::get_the_rng().get_int(100) < chance) {

	external_t::align alx = external_t::topleft;
	external_t::align aly = external_t::topleft;

	// Hajo: Check alignment inside room

	sprintf(buf, "template[%d].align.x", i);
	if(props->get_string(buf)) {
	  const char *type = props->get_string(buf);

	  if(*type == 'c') {
	    alx = external_t::center;
	  } 
	}

	sprintf(buf, "template[%d].align.y", i);
	if(props->get_string(buf)) {
	  const char *type = props->get_string(buf);

	  if(*type == 'c') {
	    aly = external_t::center;
	  } 
	}

	sprintf(buf, "template[%d].flip_random", i);
	const int flip_random = props->get_int(buf, 0);
	int flip_fix = random_flip(flip_random);

	sprintf(buf, "template[%d].flip_fix", i);
	if(props->get_int(buf)) {
	  flip_fix = *props->get_int(buf);
	}

	sprintf(buf, "template[%d].file", i);

      
	external_t ex (inner, props->get_string(buf), alx, aly);
	ex.set_flip(flip_fix);

	if(excarvate && !ex.does_excarvate()) {
	  room_t room (level, area, ground_image, wall_room, true);
	  room.realize(level);
	}


	ex.realize(level);

	// Hajo: we got a match!
	ok = true;
      }
    }
  }

  return ok;
}


void rooms_factory::subdivide(area_t &a)
{
  //  dbg->message("rooms_factory::subdivide()", "area is %d,%d - %d,%d", 
  //	       a.tl.x, a.tl.y, a.br.x, a.br.y);

  const int width = a.get_width();
  const int height = a.get_height();

  const int MSIZE = 15;

  if(width > MSIZE && height > MSIZE) { 

    const int wr = width - 14;
    const int hr = height - 14;

    rng_t &rng = rng_t::get_the_rng();

    const int wc = rng.get_int(wr);
    const int hc = rng.get_int(hr);

    const koord pos = a.tl + koord(7 + wc, 7 + hc);

    area_t a2;

    // new top left area
    a2.tl = a.tl;
    a2.br = pos+koord(-2,-2);
    subdivide(a2);
    
    // new top right area
    a2.tl.x = pos.x+2;
    a2.tl.y = a.tl.y;
    a2.br.x = a.br.x;
    a2.br.y = pos.y-2;
    subdivide(a2);

    // new bottom left area
    a2.tl.x = a.tl.x;
    a2.tl.y = pos.y+2;
    a2.br.x = pos.x-2;
    a2.br.y = a.br.y;
    subdivide(a2);

    // new bottom right area
    a2.tl = pos+koord(2,2);
    a2.br = a.br;
    subdivide(a2);




  } else {
    //    dbg->message("rooms_factory::subdivide()", "new room at %d,%d - %d,%d", 
    //		 a.tl.x, a.tl.y, a.br.x, a.br.y);

    // leaf node -> room
    areas.insert(new area_t (a));
  }
}


level_t * rooms_factory::create(pprops_t *props, 
				thinghandle_t player)
{
  koord size (100,100);

  koord pos;
  area_t a;
  int floor = 0;
  int floors = 0;
  const char * wall = 0;


  if(props->get_int("floor")) {
    floor = *props->get_int("floor");
  } else {
    dbg->fatal("rooms_factory::create()", "Missing 'floor' property in level template.");
  }

  if(props->get_int("floors")) {
    floors = *props->get_int("floors");
  } else {
    dbg->fatal("rooms_factory::create()", "Missing 'floors' property in level template.");
  }


  wall = props->get_string("wall");

  if(wall == 0) {
    dbg->fatal("rooms_factory::create()", "Missing 'wall' string property in level template.");
  }


  if(props->get_int("width")) {
    size.x = *props->get_int("width");
  }

  if(props->get_int("height")) {
    size.y = *props->get_int("height");
  }


  level = new level_t(size);
  level->set_properties(props);


  a.tl = koord(0,0);
  a.br = size - koord(1,1);

  subdivide(a);


  groundhandle_t gr[4];

  gr[0] = new ground_t();
  gr[1] = new ground_t();
  gr[2] = new ground_t();
  gr[3] = new ground_t();

  gr[0]->visual.set_image(floor);
  gr[1]->visual.set_image(floor+1);
  gr[2]->visual.set_image(floor+2);
  gr[3]->visual.set_image(floor+3);

  for(pos.y=0; pos.y<size.y; pos.y++) {
    for(pos.x=0; pos.x<size.x; pos.x++) {
      int idx ;
      if(floors == 1) {
	idx = 0;
      } else {
	idx = (pos.x & 1)+((pos.y & 1)*2);
      }

      level->at(pos)->set_ground(gr[idx]);
    }
  }


  // surrounding wall
  room_t room (level, a, -1, wall, false);
  room.realize(level);


  slist_iterator_tpl <area_t *> iter (areas);

  while(iter.next()) {
    area_t a = *iter.get_current();

    room_t room (level, a, -1, wall, false);

    room.choose_type();
    room.realize(level);

    if(room.get_type() == room_t::plain ||
       room.get_type() == room_t::big) {
      check_templates(level, a, -1, wall, false);
    }
  }


  while(areas.count()) {
    delete areas.remove_first();
  }

  return level;
}


// move this into an object XXX
level_t * create_cave(const pprops_t *props)
{
  koord size (100,100);

  int ground_image = 0;
  int density = 0;
  const char * wall = 0;

  if(props->get("floor") == NULL) {
    dbg->fatal("create_cave()", "Missing 'floor' property in level template.");
  } else {
    ground_image = *props->get("floor")->get_int();
    dbg->message("create_cave()", "'floor' image is %d", ground_image);
  }


  if(props->get("density") == NULL) {
    dbg->fatal("create_cave()", "Missing 'density' property in level template.");
  } else {
    density = *props->get("density")->get_int();
    dbg->message("create_cave()", "'density' value is %d", density);
  }


  wall = props->get_string("wall");
  if(wall == NULL) {
    dbg->fatal("create_cave()", "Missing 'wall' string property in level template.");
  } else {
    dbg->message("create_cave()", "'wall' value is %s", wall);
  }


  if(props->get_int("width")) {
    size.x = *props->get_int("width");
  }

  if(props->get_int("height")) {
    size.y = *props->get_int("height");
  }


  level_t *level = new level_t(size);


  area_t area;

  area.tl = koord(0,0);
  area.br = size - koord(1,1);


  cave_t * cave = new cave_t(area, density, ground_image, wall);
  // room_t * cave = new room_t(area, ground_image);

  cave->realize(level);

  return level;
}



/**
 * Rooms and corridors dungeon type
 * @author Hj. Malthaner
 */
static level_t * create_room_cave(pprops_t *props)
{
  koord size;
  rng_t & rng = rng_t::get_the_rng();

  minivec_tpl <koord> rooms (64);


  int ground_image = 0;
  const char * wall = 0;
  const char * wall_room = 0;

  if(props->get("floor") == NULL) {
    dbg->fatal("create_room_cave()", "Missing 'floor' property in level template.");
  } else {
    ground_image = *props->get("floor")->get_int();
    dbg->message("create_room_cave()", "'floor' image is %d", ground_image);
  }


  wall = props->get_string("wall");
  if(wall == NULL) {
    dbg->fatal("create_room_cave()", "Missing 'wall' string property in level template.");
  } else {
    dbg->message("create_room_cave()", "'wall' value is %s", wall);
  }


  wall_room = props->get_string("wall_room");
  if(wall_room == NULL) {
    dbg->fatal("create_room_cave()", "Missing 'wall_room' string property in level template.");
  } else {
    dbg->message("create_room_cave()", "'wall_room' value is %s", wall_room);
  }


  size.x = props->get_int("width", 100);
  size.y = props->get_int("height", 100);


  level_t *level = new level_t(size);
  level->set_properties(props);

  unsigned char flags[size.x * size.y];


  // clear flag set

  for(int i=0; i<size.x*size.y; i++) { 
    flags[i] = 0;
  }

  koord pos;

  // Hajo: create wall feature prototype
  feature_factory_t feature_factory;
  feature_t * wall_proto = feature_factory.create(wall);


  // Hajo: fill level with walls
  for(pos.y=0; pos.y<size.y; pos.y++) {
    for(pos.x=0; pos.x<size.x; pos.x++) {
      groundhandle_t gr (new ground_t());

      gr->visual.set_image(ground_image);
      level->at(pos)->set_ground(gr);      


      featurehandle_t f (new feature_t(wall_proto));
      level->at(pos)->set_feature(f);      
    }
  }

  const int raster = 24;

  if(raster > 0) {
    // Hajo: create rooms on a raster

    for(int y=0; y<size.y-raster; y+=raster) { 
      for(int x=0; x<size.x-raster; x+=raster) { 

	const koord rs (rng.get_int(12)+8, 
			rng.get_int(12)+8);

	const koord pos (x + 1 + rng.get_int(raster - rs.x - 2),
			 y + 1 + rng.get_int(raster - rs.y - 2));


	rooms.append(pos);
	rooms.append(rs);
      }
    }
    

  } else {
    // Hajo: random room positions

    for(int i=0; i<10; i++) {
      
      koord rs (rng.get_int(12)+8, 
		rng.get_int(12)+8);
      
      koord pos (1+rng.get_int(size.x - rs.x - 2),
		 1+rng.get_int(size.y - rs.y - 2));
      

      rooms.append(pos);
      rooms.append(rs);
    }
  }


  for(int i=0; i<rooms.count()-2; i+=2) {
    // printf("corridor %d/%d\n", i, rooms.count());

    const koord p1 = rooms.get(i) + rooms.get(i+1)/2;
    const koord p2 = rooms.get(i+2) + rooms.get(i+3)/2;

    corridor_t corr (area_t(p1, p2));

    corr.realize(level, flags);


    if(rng.get_int(100) < 40) {
      const int n1 = rng.get_int(rooms.count()-2) & 0xFFFE;
      const int n2 = rng.get_int(rooms.count()-2) & 0xFFFE;

      if(n1 != n2) {
	const koord p1 = rooms.get(n1) + rooms.get(n1+1)/2;
	const koord p2 = rooms.get(n2) + rooms.get(n2+1)/2;

	corridor_t corr (area_t(p1, p2));

	corr.realize(level, flags);
      }
    }
  }

  for(int i=0; i<rooms.count(); i+=2) {
    const koord pos = rooms.get(i);
    const koord rs = rooms.get(i+1);

    area_t area (pos, pos+rs);


    // Hajo: stuff rooms according to templates
    if(check_templates(level, area, ground_image, wall_room, true) == false) {

      // Hajo: otherwise try ordinary room
      room_t room (level, area, ground_image, wall_room, true);
      room.realize(level);
    }
  }


  create_height_level(level, 1);

  delete wall_proto;
  wall_proto = 0;

  return level;
}



// move this into an object XXX
level_t * create_forest(pprops_t *props)
{
  // koord size (30,30);
  koord size (100,100);

   int ground_image = 0;
  int density = 0;

  if(props->get("floor") == NULL) {
    dbg->fatal("create_forest()", "Missing 'floor' property in level template.");
  } else {
    ground_image = *props->get("floor")->get_int();
    dbg->message("create_forest()", "'floor' image is %d", ground_image);
  }


  if(props->get("density") == NULL) {
    dbg->fatal("create_forest()", "Missing 'density' property in level template.");
  } else {
    density = *props->get("density")->get_int();
    dbg->message("create_forest()", "'density' value is %d", density);
  }


  if(props->get_int("width")) {
    size.x = *props->get_int("width");
  }

  if(props->get_int("height")) {
    size.y = *props->get_int("height");
  }

  level_t *level = new level_t(size);
  level->set_properties(props);

  area_t area;

  area.tl = koord(0,0);
  area.br = size - koord(1,1);

  koord offset(props->get_int("offset.x", 0), props->get_int("offset.y", 0));

  dbg->message("create_forest()", "'offset' is %d %d", offset.x, offset.y);

  forest_t forest (area, offset, density, ground_image);
  forest.realize(level);
  
  create_height_level(level, 2);

  return level;
}


level_t * create_external(pprops_t *props)
{
  koord size (*props->get_int("width"), *props->get_int("height"));

  level_t *level = new level_t(size);
  level->set_properties(props);

  area_t area;

  area.tl = koord(0,0);
  area.br = size - koord(1,1);

  external_t ex (area, props->get_string("file"),
		 external_t::topleft,
		 external_t::topleft);

  ex.realize(level);

  if(props->get_string("tiled") == 0) {
    create_height_level(level, 1);
  }
  
  return level;
}


/**
 * Add random traps to level
 * @author Hj. Malthaner
 */
static void add_traps(level_t *level, rng_t & rng)
{
  properties_t *props = level->get_properties();
  const koord size = level->get_size();
  const int * traps_p = props->get_int("traps");
  koord k;

  if(traps_p) {
    int chances [*traps_p];
    int types   [*traps_p];
    char buf[128];

    for(int i=0; i<*traps_p; i++) { 
      sprintf(buf, "trap[%d].chance", i);
      chances[i] = *props->get_int(buf);

      sprintf(buf, "trap[%d].type", i);
      types[i] = *props->get_int(buf);
    }
    
    for(k.y=0; k.y < size.y; k.y++) { 
      for(k.x=0; k.x < size.x; k.x++) { 
	square_t * square = level->at(k);

	if(!square->get_feature().is_bound() &&
	   square->get_things().count() == 0) {
	  bool placed = false;

	  for(int i=0; i<*traps_p && ! placed; i++) { 

	    if(rng.get_int(10000) < chances[i]) {
	      
	      trigger_t * trigger = new trigger_t( k );
	      trigger->set_type(types[i]);
	      square->set_trigger( trigger );
	      placed = true;
	    }

	    // printf("Placing trap at %d,%d\n", k.x, k.y);
	  }
	}
      }
    }
  }
}


#define MAX_LIGHT 1900000


/**
 * Helper class tp process lit area of light sources by fov_t
 * @author Hj. Malthaner
 */
class lighter_t : public fov_destination_t {
public:
  level_t * level;
  koord center;
  int light_rad;
  
  bitmap_t * map;
  
  void set_viewable(koord pos) {
    square_t * square = level->at(pos);

    if(square) {
      const koord c2 = center*2 + koord(1,1);
      const koord k2 = pos*2;


      if(map->get_bit(pos.x, pos.y)) {
	map->clr_bit(pos.x, pos.y);
	level->set_light(pos, 
			 level->get_light(pos) + 
			 ((MAX_LIGHT*light_rad / (c2.apx_distance_to(k2)+light_rad)) >> 16));
      }
      
      if(map->get_bit(pos.x, pos.y+1)) {
	map->clr_bit(pos.x, pos.y+1);
	level->set_light(pos + koord(0, 1), 
			 level->get_light(pos + koord(0, 1)) + 
			 ((MAX_LIGHT*light_rad / (c2.apx_distance_to(k2+koord(0, 2))+light_rad)) >> 16));
      }
      
      if(map->get_bit(pos.x+1, pos.y+1)) {
	map->clr_bit(pos.x+1, pos.y+1);
	level->set_light(pos + koord(1, 1), 
			 level->get_light(pos + koord(1, 1)) + 
			 ((MAX_LIGHT*light_rad / (c2.apx_distance_to(k2+koord(2, 2))+light_rad)) >> 16));
      }
      
      if(map->get_bit(pos.x+1, pos.y)) {
	map->clr_bit(pos.x+1, pos.y);
	level->set_light(pos + koord(1, 0), 
			 level->get_light(pos + koord(1, 0)) + 
			 ((MAX_LIGHT*light_rad / (c2.apx_distance_to(k2+koord(2, 0))+light_rad)) >> 16));
      }
      
      const int dist = center.apx_distance_to(pos);
      const int light = square->access_env().lightness + (MAX_LIGHT*light_rad / (dist+light_rad)) >> 16;
      
      // printf("light=%d\n", light);
      
      square->access_env().lightness = light > 32 ? 32 : light;
    }
  }
};




/**
 * Process light sources, light the surroundings
 * @author Hj. Malthaner
 */
void process_lights(level_t * level)
{
  const koord size = level->get_size();
  fov_t fov;
  lighter_t lighter;
  koord k;
  bitmap_t map (size.x+1, size.y+1);

  lighter.level = level;
  lighter.map = &map;

  for(k.y = 0; k.y < size.y; k.y++) { 
    for(k.x = 0; k.x < size.x; k.x++) {
	
      square_t * square = level->at(k);
      const featurehandle_t & feature = square->get_feature();
      
      if(feature.is_bound() && feature->light_rad > 0) {

	map.set_map();

	lighter.center = k;
	lighter.light_rad = feature->light_rad;

	fov.set_destination(&lighter);
	fov.set_source(level);

	fov.calc_field_of_view(k, feature->light_rad+4);
      }
    }
  }

  for(k.y = 0; k.y < size.y; k.y++) { 
    for(k.x = 0; k.x < size.x; k.x++) {
	
      square_t * square = level->at(k);
      const featurehandle_t & feature = square->get_feature();
      
      if(feature.is_bound() && feature->light_rad > 0) {
	square->access_env().lightness = 32;
      }
    }
  }
}


void level_factory_t::place_thing(level_t * level,
				  const koord &k,
				  const thinghandle_t &thing,
				  rng_t &rng)
{
  square_t * square = level->at(k);

  if(square) {
    square->add_thing(thing, k);

    //printf("Thing %s has no jitter\n", thing->get_string("ident", "unnamed"));
	
    const int jitter = thing->get_int("jitter", 1);
    thing->visual.calc_jitter(jitter);


    if(thing->get_string("friends", 0)) {
      // create similar beings
      
      thing_factory_t thing_factory;
      dice_t dice(thing->get_string("friends", "%0d0")+1);
      const char * name = thing->get_string("section.name", 
					    "unnamed");

      const int n = dice.roll();

      for(int i=0; i<n; i++) { 
	level->add_thing_near(k, thing_factory.create(name, 0, 0));
      }
    }
  }

  // printf(">> %s\n", thing->get_string("ident", "unnamed"));
}


// Hajo: to allow a safe start, we remove enemies close to
// the players starting point
static void remove_enemies(level_t * level, koord k)
{
  dbg->message("remove_enemies()", "clearing area around (%d, %d)", k.x, k.y);

  for(int j=k.y-4; j<=k.y+4; j++) { 
    for(int i=k.x-4; i<=k.x+4; i++) { 
      koord pos(i,j);

      square_t * square = level->at(pos);

      if(square) {
	const minivec_tpl <thinghandle_t> & things = square->get_things();

	for(int n=things.count()-1; n>=0; n--) { 
	  const thinghandle_t & thing = things.get(n);

	  if(thing->get_actor().is_animated() &&
	     *thing->get_string("status", "enemy") == 'e') {
	    dbg->message("remove_enemies()", 
			 "removing %s from (%d, %d)", 
			 thing->get_string("ident", "unknown"), pos.x, pos.y);

	    thing->get_actor().delete_all_actions_but(0);
	    square->remove_thing(thing);
	  }
	}	
      }
    }
  }
}


/**
 * process monster types
 * @author Hj. Malthaner
 */
static void distribute_monsters(level_t * level, pprops_t * props)
{
  const int monsters =  props->get_int("monsters", 0);
  const koord size = level->get_size();

  thing_factory_t thing_factory;
  rng_t & rng = rng_t::get_the_rng();

  for(int n=0; n<monsters; n++) {
    char buf[256];

    sprintf(buf, "monster[%d].type", n);
    if(props->get_string(buf) == NULL) {
      dbg->fatal("level_factory::create_level()", "Missing '%s' property in level template.", buf);
    }
    const char * type = props->get_string(buf);

    sprintf(buf, "monster[%d].chance", n);
    if(props->get_int(buf) == NULL) {
      dbg->fatal("level_factory::create_level()", "Missing '%s' property in level template.", buf);
    }
    const int chance = *props->get_int(buf);

    koord k;

    for(k.y = 0; k.y < size.y; k.y++) { 
      for(k.x = 0; k.x < size.x; k.x++) {
	
	square_t * square = level->at(k);

	if(square->get_feature().is_bound() == false ||
	   square->get_feature()->is_blocking() == false) {

	  if(rng.get_int(100000) < chance) {
	    const thinghandle_t & thing = thing_factory.create(type, 0, 0);

	    level_factory_t::place_thing(level, k, thing, rng);
	  }
	}
      }
    }
  }  
}


/**
 * process item types
 * @author Hj. Malthaner
 */
static void distribute_items(level_t * level, pprops_t * props,
			     int magic_max, int magic_chance)
{
  const int items = props->get_int("items", 0);
  const koord size = level->get_size();

  thing_factory_t thing_factory;
  rng_t & rng = rng_t::get_the_rng();


  for(int n=0; n<items; n++) {
    char buf[256];
    
    sprintf(buf, "item[%d].type", n);
    if(props->get_string(buf) == NULL) {
      dbg->fatal("level_factory::create_level()", "Missing '%s' property in level template.", buf);
    }
    const char * type = props->get_string(buf);
    
    sprintf(buf, "item[%d].chance", n);
    if(props->get_int(buf) == NULL) {
      dbg->fatal("level_factory::create_level()", "Missing '%s' property in level template.", buf);
    }
    const int chance = *props->get_int(buf);
      
    koord k;

    for(k.y = 0; k.y < size.y; k.y++) { 
      for(k.x = 0; k.x < size.x; k.x++) {
	
	square_t * square = level->at(k);

	if(square->get_feature().is_bound() == false ||
	   square->get_feature()->is_blocking() == false) {

	  if(rng.get_int(100000) < chance) {
	    const thinghandle_t & thing = 
	      thing_factory.create(type, magic_max, magic_chance);
	    
	    level_factory_t::place_thing(level, k, thing, rng);
	  }
	}
      }
    }
  }
}


/**
 * process random features
 * @author Hj. Malthaner
 */
static void distribute_features(level_t * level, pprops_t * props)
{
  const int features = props->get_int("features", 0);
  const koord size = level->get_size();

  feature_factory_t feature_factory;
  rng_t & rng = rng_t::get_the_rng();


  for(int n=0; n<features; n++) {
    char buf[256];
    
    sprintf(buf, "feature[%d].type", n);
    if(props->get_string(buf) == NULL) {
      dbg->fatal("level_factory::create_level()", "Missing '%s' property in level template.", buf);
    }
    const char * type = props->get_string(buf);
    
    sprintf(buf, "feature[%d].chance", n);
    if(props->get_int(buf) == NULL) {
      dbg->fatal("level_factory::create_level()", "Missing '%s' property in level template.", buf);
    }
    const int chance = *props->get_int(buf);
      
    koord k;

    for(k.y = 0; k.y < size.y; k.y++) { 
      for(k.x = 0; k.x < size.x; k.x++) {
	
	square_t * square = level->at(k);

	if(!square->get_feature().is_bound() && 
	   square->get_things().count() == 0 &&
	   rng.get_int(100000) < chance) {

	  feature_t * feature = feature_factory.create(type);
	  square->set_feature(feature);
	}
      }
    }
  }
}


/**
 * generate random items and monsters
 * @author Hj. Malthaner
 */
static void distribute_random(level_t * level, pprops_t * props,
			      int magic_max, int magic_chance)
{
  if(props->get_int("random_chance")) {
    const int chance = *props->get_int("random_chance");
    const koord size = level->get_size();

    thing_factory_t thing_factory;
    rng_t & rng = rng_t::get_the_rng();


    int ilevel = 0;
    if(props->get_int("ILevel")) {
      ilevel = *props->get_int("ILevel");
    }

    int irange = 5;
    if(props->get_int("IRange")) {
      irange = *props->get_int("IRange");
    }
    
    koord k;

    for(k.y = 0; k.y < size.y; k.y++) { 
      for(k.x = 0; k.x < size.x; k.x++) {
	
	square_t * square = level->at(k);
	
	if(!square->get_feature().is_bound()) {
	  if(rng.get_int(100000) < chance) {
	    const thinghandle_t thing = 
	      thing_factory.create_random(0, ilevel, irange, 
					  magic_max, magic_chance);
	      
	    level_factory_t::place_thing(level, k, thing, rng);
	  }
	}
      }
    }
  }
}

/**
 * Place the player at given position. If neccesary remove player
 * from former position.
 * @author Hj. Malthaner
 */
static bool place_player(level_t * level, 
			 thinghandle_t & player,
			 koord pos)
{
  if(level->at(player->get_pos())) {
    level->at(player->get_pos())->remove_thing(player);
  }

  level->at(pos)->add_thing(player, pos);

  return true;
}


level_t * level_factory_t::create_level(pprops_t *props,
					thinghandle_t player)
{
  rng_t &rng = rng_t::get_the_rng();
  level_t *level = NULL;
  feature_factory_t feature_factory;

  const int magic_max = props->get_int("magic.max", 0);
  const int magic_chance = props->get_int("magic.chance", 0);

  // This wil be calculated while placing exits
  // the player will be placed besides the last exit
  koord player_pos;

  if(props->get("type") == NULL) {
    dbg->fatal("level_factory_t::create_level()",
	       "Properties of level contain no level type!");


  } else {
    const char * type = props->get("type")->get_string();

    dbg->message("level_factory_t::create_level()", "type is '%s'", type);

    if(strcmp(type, "cave") == 0) {
      level = create_cave(props);

    } else if(strcmp(type, "dungeon") == 0) {
      level = create_room_cave(props);

    } else if(strcmp(type, "forest") == 0) {
      level = create_forest(props);

    } else if(strcmp(type, "house") == 0) {
      rooms_factory rf;
      level = rf.create(props, player);

    } else if(strcmp(type, "file") == 0) {
      
      level = create_external(props);

    } else {
      dbg->error("level_factory_t::create_level()",
		 "Unknown level type '%s'.", 
		 props->get("type")->get_string());
    }

    level->set_properties(props);


    distribute_features(level, props);


    // ???
    // level->outside_visual.set_image(*props->get_int("unseen"));

    const int partition_count = level->create_partitions();
    const int largest_partition = level->get_largest_partition();
    koord pos;

    area_t area;



    // Towns and exits are only created in level pieces at offset (0,0)
    
    bool is_origin = true;

    if(props->get_string("endless")) {
      if(*props->get_int("offset.x") != 0 ||
	 *props->get_int("offset.y") != 0) {
	is_origin = false;
      }
    }



    player_pos = level->get_size()/2;

    // Player is not yet placed
    bool placed = false;


    if(is_origin && props->get_int("towns")) {
      const int towns = *props->get_int("towns");

      minivec_tpl <koord> *list = 
	level->get_empty_area(koord(30,30),
			      level->get_size()-koord(30,30),
			      koord(15,15),
			      false,
			      largest_partition, 255);

      if(list->count() < towns) {
	delete list;
	list = level->get_empty_area(koord(20,20),
				     level->get_size()-koord(20,20),
				     koord(5,5), 
				     false,
				     largest_partition, 255);
      }

      for(int i=0; i<list->count() && i<towns; i++) { 

	koord pos = list->get(rng.get_int(list->count()));
	pos += koord(3,3);

	// town_t town(pos);
	// town.realize(level, 8000);

	roadtown_t roadtown;
	roadtown.realize(level);

	koord new_pos = roadtown.get_player_pos();

	if(new_pos != koord(-1,-1)) {
	  placed = place_player(level, player, new_pos);
	}
      }

      delete list;
      list = 0;
    }


    if(props->get_int("rooms")) {
      const int rooms = *props->get_int("rooms");
      char buf[256];

      for(int i=0; i<rooms; i++) { 

	// currently only 'external' room type is available

	area_t area;

	sprintf(buf, "room[%d].width", i);
	if(*props->get_int(buf) == 0) {
	  dbg->fatal("level_factory_t::create_level()",
		     "'%s' is missing in level template", buf);
	} else {
	  area.br.x = *props->get_int(buf);
	}

	sprintf(buf, "room[%d].height", i);
	if(*props->get_int(buf) == 0) {
	  dbg->fatal("level_factory_t::create_level()",
		     "'%s' is missing in level template", buf);
	} else {
	  area.br.y = *props->get_int(buf);
	}


	sprintf(buf, "room[%d].flip", i);
	const int flip_random = props->get_int(buf, 0);
	int flip_fix = random_flip(flip_random);


	koord room_pos;

	sprintf(buf, "room[%d].pos.x", i);
	if(props->get_int(buf, -1) == -1) {

	   minivec_tpl <koord> *list = 
	   level->get_empty_area(koord(0,0),
				 level->get_size()-koord(1,1),
				 area.br,
				 true, 
				 largest_partition, 64);

	   if(list->count() == 0) {
	     dbg->fatal("level_factory_t::create_level()",
			"no suitable location for room[%d] found", i);
	   } else {
	     room_pos = list->get(rng.get_int(list->count()));
	   }

	} else {
	  sprintf(buf, "room[%d].pos.x", i);
	  const int xpos = props->get_int(buf, 0);
	  
	  sprintf(buf, "room[%d].pos.y", i);
	  const int ypos = props->get_int(buf, 0);

	  room_pos = koord(xpos, ypos);
	}

	  
	area.tl += room_pos;
	area.br += room_pos - koord(1,1);


	sprintf(buf, "room[%d].file", i);
	const char *filename = props->get_string(buf);

	external_t room (area, filename,
			 external_t::topleft,
			 external_t::topleft);

	room.set_flip(flip_fix);
	room.realize(level);
	
	
	sprintf(buf, "room[%d].player.x", i);
	if(props->get_int(buf)) {
	  // Hajo: force player to location
	  
	  const int x = *props->get_int(buf);
	  sprintf(buf, "room[%d].player.y", i);
	  const int y = *props->get_int(buf);
	  
	  koord new_pos (area.tl.x + x, area.tl.y + y);

	  placed = place_player(level, player, new_pos);
	}
      }
    }



    const int exits = props->get_int("exits", 0);

    if(is_origin && exits > 0) {

      koord margin;

      if(level->get_size().x <= 30) {
	// no margin
      } else if(level->get_size().x < 60) {
	margin = koord(10,10);
      } else {
	margin = koord(20,20);
      }

      minivec_tpl <koord> *list = 
	level->get_empty_area(margin,
			      level->get_size()-margin,
			      koord(3,3),
			      true,
			      largest_partition, 
			      255);

      if(list->count() < exits) {
	int i = 0;

	while(i < partition_count && list->count() < exits) {
	  delete list;
	  list = 
	    level->get_empty_area(margin,
				  level->get_size()-margin,
				  koord(3,3),
				  false,
				  i,
				  255);
	  i++;
	}
      }

      if(list->count() < exits) {
	delete list;
	list = 
	  level->get_empty_area(margin,
				level->get_size()-margin,
				koord(1,1),
				false,
				largest_partition, 
				255);
      }

      for(int i=0; i<exits; i++) {
	char buf[256];
      
	sprintf(buf, "exit[%d].level", i);
      
	feature_stairs_t * stairs = 
	  new feature_stairs_t(props->get_string(buf));
      
	const koord pos = list->get(rng.get_int(list->count()));

	dbg->message("level_factory_t::create_level()", 
		     "placing exit to '%s' at %d,%d", 
		     props->get_string(buf), pos.x, pos.y);



	sprintf(buf, "exit[%d].type", i);

	featurehandle_t feature = feature_factory.create(props->get_string(buf));
	feature->visual.set_opaque(false);
	feature->set_peer(stairs);
	
	level->at(pos)->set_feature(feature);
	
	player_pos = pos;
      }

      delete list;
      list = 0;
    }


    // where to place the player ???


    if(level->get_properties()->get_string("other_area") &&
       level->get_properties()->get_string("endless")) {
      // player position is allright, just place him

      level->at(player->get_pos())->add_thing(player, player->get_pos());

      dbg->message("level_factory_t::create_level()",
		   "placing player at %d,%d", 
		   player->get_pos().x, player->get_pos().y);
    } else {

      if(level->get_properties()->get_string("endless")) {
	area.tl = koord(11,11);
	area.br = level->get_size() - koord(12, 12);
      } else {
	area.tl = koord(0,0);
	area.br = level->get_size() - koord(1, 1);
      }



      // Check area around entrance for a suitable place for the player
      for(pos.y=0; pos.y<3; pos.y++) {
	for(pos.x=0; pos.x<3; pos.x++) {

	  const koord k = pos + player_pos;

	  // place player if not yet placed
	  if(!placed &&
	     // level->partition_at(k)==largest_partition &&
	     level->at(k) &&
	     level->at(k)->can_enter(player)) {

	    level->at( k )->add_thing(player, k);

	    dbg->message("level_factory_t::create_level()",
			 "placing player at %d,%d", 
			 player->get_pos().x, player->get_pos().y);
	    
	    placed = true;
	  }
	}
      }



      if(!placed) {
	// emergency case - no place besides entrance. 

	minivec_tpl <koord> *list = 
	  level->get_empty_area(koord(0,0),
				level->get_size() - koord(1,1),
				koord(3,3), 
				false,
				largest_partition, 255);

	player_pos = list->get(0);

	delete list;

	level->at(player_pos)->add_thing(player, player_pos);

	// Place player on
	// entrance, allowing him to leave
	
	// player->set_pos( player_pos );
	// level->at(player_pos)->add_thing(player);
	
	placed = true;
      }
    }


    distribute_monsters(level, props);

    distribute_items(level, props, magic_max, magic_chance);

    distribute_random(level, props, magic_max, magic_chance);
  }


  // Hajo: to allow a safe start, we remove enemies close to
  // the players starting point
  remove_enemies(level, player->get_pos());
  

  // Hajo: generate traps
  add_traps(level, rng);

  // Hajo: process light sources
  process_lights(level);

  return level;
}
