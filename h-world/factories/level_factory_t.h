/* 
 * level_factory_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef LEVEL_FACTORY_T_H
#define LEVEL_FACTORY_T_H

#include "tpl/slist_tpl.h"
#include "model/thinghandle_t.h"

class area_t;
class level_t;
class pprops_t;
class square_t;
class rng_t;
class koord;

class rooms_factory {
 private:

  slist_tpl <area_t *> areas;
  level_t * level;


  void subdivide(area_t &a);
 public:

  level_t * create(pprops_t *, thinghandle_t player);
};



/**
 * A factory class which generates complete levels, or restocks
 * existing levels with content. 
 * @author Hj. Malthaner
 */
class level_factory_t {

 public:

  static void place_thing(level_t * level,
			  const koord &k,
			  const thinghandle_t &thing,
			  rng_t &rng);
    


 public:

  /**
   * Creates a new level.
   * @author Hj. Malthaner
   */
  level_t * create_level(pprops_t *props, thinghandle_t player);
};
#endif //LEVEL_FACTORY_T_H
