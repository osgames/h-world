/* 
 * cave_t.h
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef CAVE_T_H
#define CAVE_T_H
#include "structure_t.h"

class cave_t : public structure_t {
private:
  int count_neighbours(const koord pos) const;

public:

  cave_t(const area_t &a, int density, int ground, const char * wall);

};
#endif //ROOM_T_H
