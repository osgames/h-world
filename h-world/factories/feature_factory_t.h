/* 
 * feature_factory_t.h
 *
 * Copyright (c) 2001 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef feature_factory_t_h
#define feature_factory_t_h


#ifndef image_meta_t_h
#include "view/image_meta_t.h"
#endif


class cstring_t;
class feature_t;
class properties_t;

class feature_factory_t
{
 public:

  /**
   * Reads configuration data from file
   * @author Hj. Malthaner
   */
  static void init(cstring_t & dirname);


  /**
   * Gets the properties of a given feature
   * @author Hj. Malthaner
   */
  static properties_t * get_props(const char * key);


  /**
   * Load a feature by name
   * @author Hj. Malthaner
   */
  static image_meta_t load(const char * name);


 private:


 public:

  feature_factory_t();



  /**
   * Create a feature by name
   * @author Hj. Malthaner
   */
  feature_t * create(const char * name) const;

};

#endif // feature_factory_t_h
