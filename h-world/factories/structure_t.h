/* 
 * structure_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef STRUCTURE_T_H
#define STRUCTURE_T_H

#include "primitives/area_t.h"

class level_t;
class feature_t;

/**
 * Base class for dungeon structures like rooms and corridors.
 * @author Hansj�rg Malthaner
 */
class structure_t {
public:

  enum type {wall=0, open=1};

private:

  int * grid;


protected:

  /**
   * Ground image number
   * @author Hansj�rg Malthaner
   */
  int ground_look;


  /**
   * Wall feature prototype
   * @author Hansj�rg Malthaner
   */
  feature_t * wall_feat;


  /**
   * Covered area, bounding rect.
   * @author Hansj�rg Malthaner
   */
  area_t area;


  /**
   * If a subclass uses different features than wall and floors only
   * it needs to override this method and place the features in the 
   * level.
   *
   * @author Hansj�rg Malthaner
   */
  virtual void realize_square(level_t *l, koord pos, int f);

  int get_grid_at(koord pos) const;

  void set_grid_at(koord pos, int f);

  /**
   * Set wall look
   * @author Hansj�rg Malthaner
   */
  void set_wall_feat(feature_t *wall_feat);

  void set_ground_look(int n);

public:

  /**
   * Returns true is the given koord is a space in this structure
   * @author Hansj�rg Malthaner
   */
  bool is_space(koord k);

  /**
   * Returns true is the given koord is a wall in this structure
   * @author Hansj�rg Malthaner
   */
  bool is_wall(koord k);


  /**
   * Basic constructor
   * @author Hansj�rg Malthaner
   */
  structure_t(const area_t & a);

  /**
   * Destructor
   * @author Hansj�rg Malthaner
   */
  virtual ~structure_t();


  /**
   * Returns true if this structure intersects with the area
   * @author Hansj�rg Malthaner
   */
  bool intersect(area_t & area);

  /**
   * Returns true if this structure intersects with the other structure
   * @author Hansj�rg Malthaner
   */
  bool intersect(structure_t & str);

  /**
   * Returns true if a inner space this structure intersects with the
   * other structure
   * @author Hansj�rg Malthaner
   */
  bool intersect_space(structure_t & str);


  /**
   * Realizes this structure in the level
   * @author Hansj�rg Malthaner
   */
  virtual void realize(level_t *);
};
#endif //STRUCTURE_T_H
