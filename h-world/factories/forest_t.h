/* 
 * forest_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef FOREST_T_H
#define FOREST_T_H
#include "structure_t.h"


class forest_t : public structure_t {
private:

  int density;
  int ground;

  koord offset;

public:

  forest_t(const area_t &a, const koord &offset, int density, int ground);


  /**
   * Realizes this forest in the level
   * @author Hansj�rg Malthaner
   */
  virtual void realize(level_t *);
};

#endif //ROOM_T_H
