#ifndef feature_peer_creator_t_h
#define feature_peer_creator_t_h

#include "primitives/koord.h"

class feature_peer_t;

/**
 * Base class for feature_peer creation.
 * @see feature_peer_broker_t
 * @author Hj. Malthaner
 */
class feature_peer_creator_t
{
 public:
  virtual feature_peer_t * create(koord k, const char * parameters) = 0;
  virtual const char * get_name() = 0;

};

#endif // feature_peer_creator_t_h
