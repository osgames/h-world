/* Copyright by Hj. Malthaner */

#ifndef CORRIDOR_T_H
#define CORRIDOR_T_H
#include "structure_t.h"
class koord;

/**
 * A dungeon structure that creates a winded corridor between 
 * two locations. The both coordinates of the area describe
 * the entrance and exit, not the tl and br corner as usual.
 *
 * @author Hansj�rg Malthaner
 */
class corridor_t : public structure_t {

  /**
   * must point to enough space for [size.x*size.y];
   *
   * @author Hansj�rg Malthaner
   */
  unsigned char *flags;

public:


  /**
   * Basic constructor
   * @author Hansj�rg Malthaner
   */
  corridor_t(const area_t & a);


  /**
   * Realizes this structure in the level
   * @author Hansj�rg Malthaner
   */
  virtual void realize(level_t *);


  void realize(level_t *, unsigned char *flags);

};
#endif //CORRIDOR_T_H
