/* 
 * external_t.h
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef external_t_h
#define external_t_h

#include "structure_t.h"


class square_t;
class properties_t;
class config_file_t;


/**
 * Read a level or room from a file and realize it 
 * @author Hj. Malthaner
 */
class external_t : public structure_t
{
public:

  enum flips {FLIP_H=1, FLIP_V=2};
  enum align {topleft, center};

private:

  const char *filename;
  int flip;
  enum align alx;
  enum align aly;

  properties_t * props;

  koord map_flip(koord k) const;


  /**
   * Retrieves a square. Applies flip during lookup
   * @author Hansj�rg Malthaner
   */ 
  square_t * lookup(koord k, level_t * level);


  /**
   * Reads properties. Adjusts area size
   * @author Hansj�rg Malthaner
   */ 
  void read_properties(config_file_t * file);


  /**
   * Reads properties. Adjusts area size
   * @author Hansj�rg Malthaner
   */ 
  void read_properties();


public:


  /**
   * Checks if a player position if given for this template
   * @return player position or -1,-1 if no position is given
   * @author Hj. Malthaner
   */
  koord get_player_pos();


  /**
   * Checks if this structure does excarvation
   * (check for 'clear' flag in properties)
   * @author Hansj�rg Malthaner
   */
  bool does_excarvate();


  void set_flip(int flip);

  /**
   * Basic constructor
   * @author Hansj�rg Malthaner
   */
  external_t(const area_t & a, const char *filename, 
	     enum align alx, enum align aly);


  virtual ~external_t();

  /**
   * Realizes this structure in the level
   * @author Hansj�rg Malthaner
   */
  virtual void realize(level_t *);

};

#endif // external_t_h
