/* 
 * hdoor_creator_t.cpp
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "hdoor_creator_t.h"
#include "feature_factory_t.h"

#include "model/feature_door_t.h"
#include "model/feature_t.h"
#include "model/featurehandle_t.h"
#include "model/square_t.h"
#include "model/level_t.h"

#include "primitives/koord.h"


hdoor_creator_t::hdoor_creator_t()
{
}


feature_peer_t * hdoor_creator_t::create(koord k, const char * parameters)
{
  feature_peer_t * peer = 0;

  if(parameters == 0 || *parameters <= 32) {

    // pos, open, closed, open_x, open_y
    peer = new feature_door_t(k-koord(1,0), 0, 106, 0, 111, +40, 0);
  } else {

    feature_factory_t feature_factory;
    char buf[128];

    sprintf(buf, "%s.open", parameters);
    featurehandle_t open = feature_factory.create(buf);

    printf("buf=%s, img=%d\n", buf, open->visual.access_image(0).img);
    
    sprintf(buf, "%s.closed", parameters);
    featurehandle_t closed = feature_factory.create(buf);
  
    printf("buf=%s, img=%d\n", buf, closed->visual.access_image(0).img);
    
    peer = new feature_door_t(k-koord(1,0), 
			      open->visual.access_image(0).tileset, 
			      open->visual.access_image(0).img, 
			      closed->visual.access_image(0).tileset, 
			      closed->visual.access_image(0).img, 
			      +40, 0);
  }

  return peer;
}


const char * hdoor_creator_t::get_name()
{
  return "hdoor";
}


void hdoor_creator_t::realize(level_t *level, 
			      const char * open, 
			      const char * closed, 
			      int open_x, int open_y,
			      const char * left, 
			      const char * center, 
			      const char * right,
			      const koord &left_pos)
{
  feature_factory_t fefac;

  koord k = left_pos;

  const image_meta_t img_open = fefac.load(open);
  const image_meta_t img_closed = fefac.load(closed);

  featurepeerhandle_t door (new feature_door_t(k+koord(1,0), 
					       img_open.tileset, 
					       img_open.img, 
					       img_closed.tileset, 
					       img_closed.img, 
					       open_x, open_y));

  featurehandle_t feature (fefac.create(left));
  feature->set_peer(door);
  feature->set_flag(feature_t::F_DOORPOST);
  level->at(k)->set_feature(feature);
    
  k += koord(1,0);
  feature = fefac.create(center);
  feature->set_peer(door);
  feature->set_flag(feature_t::F_DOOR);
  level->at(k)->set_feature(feature);
  
  k += koord(1,0);
  feature = fefac.create(right);
  feature->set_peer(door);
  feature->set_flag(feature_t::F_DOORPOST);
  level->at(k)->set_feature(feature);
}
