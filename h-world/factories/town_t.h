/* 
 * town_t.h
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef FACTORIES_TOWN_T_H
#define FACTORIES_TOWN_T_H


#include "primitives/koord.h"
#include "primitives/area_t.h"

class level_t;
template <class T> class slist_tpl;

/**
 * A town generator. Creates reoad network and buildings 
 * in a level structure.
 *
 * @author Hj. Malthaner
 * @date 08-Mar-2002
 */
class town_t {
private:


  area_t a;

  int   best_road_value;
  koord best_road_pos;

  /**
   * mirror y
   * @author Hj. Malthaner
   */
  void trans_y(const char *rule, char *tr);


  /**
   * rotate left
   * @author Hj. Malthaner
   */
  void trans_l(const char *rule, char *tr);


  /**
   * rotate right
   * @author Hj. Malthaner
   */
  void trans_r(const char *rule, char *tr);


  /**
   * Symbols in rules:
   * S = must not be road
   * s = must be road
   * n = must be nature
   * . = matches anything
   *
   * @param pos position to check
   * @param regel the rule to evaluate
   * @return true on match, false otherwise
   * @author Hj. Malthaner
   */
  bool evaluate_loc(const level_t * l, koord pos, const char *regel);


  /**
   * Check rule in all transformations at given position
   * @author Hj. Malthaner
   */
  int evaluate_pos(const level_t *level, koord pos, const char *rule);


  void evaluate_road(const level_t * level, koord pos, 
		     int chance, const char *rule);

  void town_t::evaluate(const level_t *level);


  void grow_borders(level_t * l, const koord k);

  void build_road(level_t * l, const koord pos, const int road_image);


  /**
   * Build a 3x3 house at pos
   * @author Hj. Malthaner
   */
  void build_house(level_t * l, const koord pos, 
		     slist_tpl <koord> * house_list);

public:

  /**
   * Basic constrcutor
   *
   * @param pos the location within the level to generate the town 
   */
  town_t(koord pos);


  /**
   * Realizes this town in the level
   * @author Hansj�rg Malthaner
   */
  void realize(level_t *, int size);

};

#endif // FACTORIES_TOWN_T_H


