/* 
 * cave_t.cpp
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "feature_factory_t.h"
#include "cave_t.h"
#include "util/rng_t.h"

int cave_t::count_neighbours(const koord pos) const 
{
  koord k;
  int count = 0;

  for(k.y=pos.y-1; k.y<=pos.y+1; k.y++) {
    for(k.x=pos.x-1; k.x<=pos.x+1; k.x++) {
      if(k != pos) {
	count += get_grid_at(k) == 0;
      }
    }
  }

  return count;    
}



cave_t::cave_t(const area_t & a, int density, int ground, const char * wall) : structure_t(a)
{
  // Hajo: create wall feature prototype
  feature_factory_t feature_factory;
  feature_t * wall_proto = feature_factory.create(wall);
  
  set_wall_feat(wall_proto);

  set_ground_look(ground);


  const int height = a.get_height();
  const int width = a.get_width();
  rng_t &rng = rng_t::get_the_rng();
  koord pos;

  // init cave array

  for(pos.y = a.tl.y; pos.y <= a.br.y; pos.y++) {
    for(pos.x = a.tl.x; pos.x <= a.br.x; pos.x++) {
      // set to floor or wall
      if(pos.y <= 1 || pos.x <= 1 || pos.y >= a.br.y-1 || pos.x >= a.br.x-1) {
	set_grid_at(pos, 0);
      } else {
	set_grid_at(pos, 1);
      }
    }
  }


  for(int i=0; i<density; i++) {
    pos.x = rng.get_int(width);
    pos.y = rng.get_int(height);
    // set to wall
    set_grid_at(a.tl+pos, 0);
  }  

  // then 'grow' the caves

  for(int i=0; i<20; i++) {
    for(pos.y = a.tl.y; pos.y <= a.br.y; pos.y++) {
      for(pos.x = a.tl.x; pos.x <= a.br.x; pos.x++) {
	int count = count_neighbours(pos);

	if(count < 4 ) {
	  // set to floor
	  set_grid_at(pos, 1);
	} else if(count > 5 ) {
	  // set to wall
	  set_grid_at(pos, 0);
	}
      }
    }
  }

  // add outer walls

  for(pos.x=a.tl.x; pos.x<=a.br.x; pos.x++) {
    pos.y = a.tl.y;
    set_grid_at(pos, 0);

    pos.y = a.br.y;
    set_grid_at(pos, 0);
  }

  for(pos.y=a.tl.y; pos.y<=a.br.y; pos.y++) {
    pos.x = a.tl.x;
    set_grid_at(pos, 0);

    pos.x = a.br.x;
    set_grid_at(pos, 0);
  }
}
