/* 
 * exit_creator_t.h
 *
 * Copyright (c) 2001 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef exit_creator_t_h
#define exit_creator_t_h

#include "feature_peer_creator_t.h"

class level_t;
class koord;

class exit_creator_t : public feature_peer_creator_t
{
 private:

 public:


  virtual feature_peer_t * create(koord k, const char * parameters);
  virtual const char * get_name();

  exit_creator_t();


};

#endif // exit_creator_t_h
