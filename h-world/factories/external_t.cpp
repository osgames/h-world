/* 
 * external_t.cpp
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "external_t.h"

#include "util/config_file_t.h"
#include "util/debug_t.h"
#include "util/rng_t.h"

#include "control/trigger_t.h"

#include "model/featurehandle_t.h"
#include "model/groundhandle_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/ground_t.h"
#include "model/feature_t.h"
#include "model/feature_peer_t.h"
#include "model/feature_door_t.h"
#include "model/thing_t.h"
#include "model/pprops_t.h"

#include "thing_factory_t.h"
#include "level_factory_t.h"
#include "feature_factory_t.h"

#include "feature_peer_broker_t.h"


/**
 * Checks if a player position if given for this template
 * @return player position or -1,-1 if no position is given
 * @author Hj. Malthaner
 */
koord external_t::get_player_pos()
{
  read_properties();
  koord pos (-1, -1);

  if(props->get_int("player.x")) {
    
    const int x = *props->get_int("player.x");
    const int y = *props->get_int("player.y");
    
    pos = area.tl + koord(x, y);
  }
  
  dbg->message("external_t::player_pos()", "pos=%d,%d", pos.x, pos.y); 

  return pos;
}


/**
 * Checks if this structure does excarvation
 * (check for 'clear' flag in properties)
 * @author Hansj�rg Malthaner
 */
bool external_t::does_excarvate()
{
  read_properties();
  
  bool ok = (*props->get_string("clear", "f") == 't');

  return ok;
}


void external_t::set_flip(int flip)
{
  this->flip = flip;
}


koord external_t::map_flip(koord k) const
{
  if((flip & FLIP_H)) {
    k.x = area.br.x - (k.x - area.tl.x);
  }
  
  if((flip & FLIP_V)) {
    k.y = area.br.y - (k.y - area.tl.y);
  }

  return k;
}


/**
 * Retrieves a square. Applies flip during lookup
 * @author Hansj�rg Malthaner
 */ 
square_t * external_t::lookup(koord k, level_t * level)
{
  return level->at(map_flip(k));
}


external_t::external_t(const area_t & a, const char *fn,
	     enum align alx, enum align aly) : structure_t(a)
{
  filename = fn;
  props = 0;

  flip = 0;
  this->alx = alx;
  this->aly = aly;
}


external_t::~external_t()
{
  delete props;
  props = 0;
}


// line
// M:f:0:
// <char>:<type>:<opaque>:<blocking>:<frames>:<peer>

class griddescriptor_t
{
public:
  int  img;
  unsigned char opaque;
  unsigned char blocking;
  unsigned char frames;
  unsigned char prob_chain;

  char * peer;

  char feat[16];  // Feature name if named feature

  griddescriptor_t () {
    img = -1;
    opaque = true;
    blocking = true;
    frames = 1;
    peer = 0;
  }
  
};


class itemondescriptor_t : public griddescriptor_t
{
public:
  koord ai_lair;
  koord ai_work;
  koord ai_wash;

  itemondescriptor_t() : griddescriptor_t () {
    ai_lair.x = -1;
    ai_lair.y = -1;

    ai_work.x = -1;
    ai_work.y = -1;

    ai_wash.x = -1;
    ai_wash.y = -1;
  }
  
};


void external_t::read_properties(config_file_t * file)
{
  props = new properties_t();

  props->load_from_section(file);

  // Hajo: special entries
  
  const int * ip;
  
  ip = props->get_int("width");
  if(ip) {
    // Hajo: center in area?
    if(alx == center) {
      area.tl.x += (area.get_width() - *ip)/2;
    }
    
    area.br.x = area.tl.x + *ip - 1;
  } else {
    dbg->fatal("external_t::read_properties()", 
	       "property 'width' is missing in template '%s'", filename);
  }
  
  ip = props->get_int("height");
  if(ip) {
    // Hajo: center in area?
    if(aly == center) {
      area.tl.y += (area.get_height() - *ip)/2;
    }
    
    area.br.y = area.tl.y + *ip - 1;
  } else {
    dbg->fatal("external_t::read_properties()", 
	       "property 'height' is missing in template '%s'", filename);
  }
}


/**
 * Reads properties. Adjusts area size
 * @author Hansj�rg Malthaner
 */ 
void external_t::read_properties()
{
  if(props == 0) {

    config_file_t file;

    if(file.open(filename)) {
      read_properties(&file);
    } else {
      dbg->error("external_t::read_properties()", 
		 "cannot read file '%s'", filename);
    }
  }
}


static void read_mappings(config_file_t * file,
			  griddescriptor_t * grounds,
			  griddescriptor_t * features,
			  itemondescriptor_t * itemons)
{
  char buf[256];

  do {
    memset(buf, 0, 256);
    file->read_line(buf, 255);
    char key = buf[0];
    
      
    // Hajo: "x:..." formatted entries
    
    if(key == 'Q') {
      break;
    } else {
      const char type = buf[2]; 
      char * peer = 0;
      bool opaque = true;
      bool blocking = true;
      int  frames = 1;
      
      char * p = buf + 3;

      int img = 0;
	
      if(type == 'f') {
	// Hajo: feature data

	features[key].feat[0] = '\0';  // save default
	
	if(*p++ == ':') {
	  if(*p == '0' || *p == '1') {
	    opaque = (*p++ == '1');  

	    dbg->fatal("external_t::realize()",
		       "'unnamed' feature '%s' no longer supported", buf);

	  } else {
	    // Named feature

	    // scan for next ':'
	    
	    int idx = 0;

	    while(p[idx] != ':' && p[idx] > 32) {
	      features[key].feat[idx] = p[idx];
	      idx ++;
	    }
	    features[key].feat[idx] = '\0';
	    
	    p += idx;
	    
	    // Now go on with usual attributes
	    if(*p++ == ':') {
	      opaque = (*p++ == '1');  
	    }
	  }
	}
	
	if(*p++ == ':') {
	  blocking = (*p++ == '1');
	}
	
	if(*p++ == ':') {
	  frames = (*p++ - '0');
	}

	if(*p++ == ':') {
	  peer = strdup(p);
	}
	
	file->read_line(buf, 255);
	sscanf(buf, "img=%d", &img);
	
	
	features[key].img = img;
	features[key].opaque = opaque;
	features[key].blocking = blocking;
	features[key].frames = frames;
	features[key].peer = peer;

	/*
	  printf("Feature key=%c name='%s' opaque=%d blocking=%d frames=%d peer='%s'\n",
	    key, 
	    features[key].feat, 
	    features[key].opaque, 
	    features[key].blocking, 
	    features[key].frames, 
	    features[key].peer); 
	*/

      } else if (type=='g') {
	// Hajo: ground data
	file->read_line(buf, 255);
	sscanf(buf, "img=%d", &img);
	
	grounds[key].img = img;
      } else {
	// Hajo: item/monster data
	
	itemons[key].prob_chain = (type == 'c');
	
	// scan name
	itemons[key].feat[0] = '\0';  // save default
	
	p++;  // skip ':'

	int idx = 0;
	
	while(p[idx] != ':' && p[idx] > 32) {
	  itemons[key].feat[idx] = p[idx];
	  idx ++;
	}
	itemons[key].feat[idx] = '\0';
	p += idx;
	
	// Hajo: this is checked against -1 later, 0 is safe default
	itemons[key].img = 0;
	
	do {
	  // Hajo: read additional data
	  file->read_line(buf, 255);
	  
	  if(strncmp(buf, "img=feature_default", 18) == 0) {
	    dbg->warning("external_t::realize()",
			 "obsolete '%s', use 'end' to end list", buf);
	    sprintf(buf, "end");
	    
	  } else if(strncmp(buf, "end", 3) == 0) {
	    // Hajo: thats fine, end processing
	    // -> nothing to do
	  } else if (strncmp(buf, "AI.lair", 7) == 0) {
	    int x, y;
	    sscanf(buf, "AI.lair = %d,%d", &x, &y); 
	    itemons[key].ai_lair = koord(x, y); 
	    
	  } else if (strncmp(buf, "AI.work", 7) == 0) {
	    int x, y;	    
	    sscanf(buf, "AI.work = %d,%d", &x, &y);  
	    itemons[key].ai_work = koord(x, y); 
	    
	  } else if (strncmp(buf, "AI.wash", 7) == 0) {
	    int x, y;	    
	    sscanf(buf, "AI.wash = %d,%d", &x, &y);  
	    itemons[key].ai_wash = koord(x, y); 
	    
	  } else {
	    dbg->fatal("external_t::realize()",
		       "Unknown item/monster definition '%s' (maybe there is an 'end' statement missing?)", buf);
	  }
	} while(strncmp(buf, "end", 3) != 0);
      }
    }
  } while(true);

  // Hajo: Space means nothing 
  features[' '].img = 0;
  grounds[' '].img = 0;
}


/**
 * Realizes this structure in the level
 * @author Hansj�rg Malthaner
 */
void external_t::realize(level_t *level)
{
  config_file_t file;

  thing_factory_t tf;
  feature_factory_t feature_factory;

  griddescriptor_t grounds  [256];
  griddescriptor_t features [256];
  itemondescriptor_t itemons  [256];    // Hajo: items or monster data


  bool lit = level->get_properties()->get("lit") != 0;

  const int magic_max = level->get_properties()->get_int("magic.max", 0);
  const int magic_chance = level->get_properties()->get_int("magic.chance", 0);

  if(file.open(filename)) {
    char buf[256];

    dbg->message("external_t::realize()", "reading template '%s'", filename);

    read_properties(&file);
    read_mappings(&file, grounds, features, itemons);

    const bool clear_area = *props->get_string("clear", "f") == 't';
    const bool keep_entry = *props->get_string("keep_entry", "f") == 't';
    const int  room_type = props->get_int("room_type", 0);

    
    dbg->message("external_t::realize()", 
		 "size=%d %d, magic_max=%d, magic_chance=%d, clear=%d",
		 area.get_width(), area.get_height(), 
		 magic_max, magic_chance, clear_area);


    for(int y=area.tl.y; y<=area.br.y; y++) {
      file.read_line(buf, 255);

      if((int)strlen(buf) != area.get_width()) {
	dbg->fatal("external_t::realize()", "level is %d cells wide, but object line %d contains %d characters!", area.get_width(), y-area.tl.y, strlen(buf));
      }

      // dbg->message("external_t::realize()", buf);

      for(int x=area.tl.x; x<=area.br.x; x++) {
	
	const char c = buf[x-area.tl.x];
	const koord k (x,y);

	if(clear_area) {
	  lookup(k, level)->set_feature(0);
	}

	
	if(features[c].img != -1) {

	  const bool keep =
	    keep_entry &&
	    (k.x == area.tl.x ||
	     k.y == area.tl.y ||
	     k.x == area.br.x ||
	     k.y == area.br.y) &&
	    lookup(k, level)->room_type == square_t::corridor;

	  if(keep == false && c != ' ') {
	    featurehandle_t feature;

	    if(features[c].feat[0] == '\0') {
	      // feature = new feature_t();
	      // feature->visual.set_image(0, features[c].img, 0, 0, 0, 0, false);

	      dbg->fatal("external_t::realize()",
			 "'unnamed' features are no longer supported");

	    } else {
	      feature = feature_factory.create(features[c].feat);
	    }

	    feature->visual.set_opaque(features[c].opaque);
	    feature->set_blocking(features[c].blocking);
	    feature->visual.set_frames(features[c].frames);

	    if(features[c].peer) {

	      char buf[256];
	      strcpy(buf, features[c].peer);

	      char * peer_name = buf;
	      char * peer_param = strstr(buf, ":");


	      dbg->message("external_t::realize()", "key=%c peer='%s' param='%s'", c, peer_name, peer_param);

	      if(peer_param) {
		*peer_param ++ = '\0';
	      }


	      featurepeerhandle_t peer (feature_peer_broker_t::create(peer_name, map_flip(k), peer_param));
	      feature->set_peer(peer);


	      // Doors are multi-tile features
	      if(strncmp(features[c].peer , "hdoor", 5) == 0) {
		lookup(k-koord(1,0), level)->get_feature()->set_peer(peer);
		lookup(k-koord(2,0), level)->get_feature()->set_peer(peer);

		feature->set_flag(feature_t::F_DOORPOST);
		lookup(k-koord(1,0), level)->get_feature()->set_flag(feature_t::F_DOOR);
		lookup(k-koord(2,0), level)->get_feature()->set_flag(feature_t::F_DOORPOST);

		dynamic_cast <feature_door_t *> (peer.get_rep())->set_door_pos(map_flip(k-koord(1,0)));
	      }

	      // Doors are multi-tile features
	      if(strncmp(features[c].peer , "vdoor", 5) == 0) {

		lookup(k-koord(0,1), level)->get_feature()->set_peer(peer);
		lookup(k-koord(0,2), level)->get_feature()->set_peer(peer);

		feature->set_flag(feature_t::F_DOORPOST);
		lookup(k-koord(0,1), level)->get_feature()->set_flag(feature_t::F_DOOR);
		lookup(k-koord(0,2), level)->get_feature()->set_flag(feature_t::F_DOORPOST);
		dynamic_cast <feature_door_t *> (peer.get_rep())->set_door_pos(map_flip(k-koord(0,1)));
	      }

	    }
	    
	    lookup(k, level)->set_feature(feature);

	  } else {

	    lookup(k, level)->set_feature(0);
	  }

	} else if(itemons[c].img != -1) {
	  thinghandle_t thing = 
	    itemons[c].prob_chain ? 
	    tf.create_by_chain(props, itemons[c].feat) : 
	    tf.create(itemons[c].feat, magic_max, magic_chance);

	  const koord pos = map_flip(k);


	  if(thing.is_bound()) {
	    level_factory_t::place_thing(level, pos, 
					 thing, rng_t::get_the_rng());

	    // Hajo: AI helper data
	    if(itemons[c].ai_lair.x != -1) {
	      const koord k = map_flip(area.tl + itemons[c].ai_lair);
	      thing->set("AI.lair.x", k.x);
	      thing->set("AI.lair.y", k.y);
	    }
	    if(itemons[c].ai_work.x != -1) {
	      const koord k = map_flip(area.tl + itemons[c].ai_work);
	      thing->set("AI.work.x", k.x);
	      thing->set("AI.work.y", k.y);
	    }
	    if(itemons[c].ai_wash.x != -1) {
	      const koord k = map_flip(area.tl + itemons[c].ai_wash);
	      thing->set("AI.wash.x", k.x);
	      thing->set("AI.wash.y", k.y);
	    }
	  }

	} else {
	  dbg->fatal("external_t::realize()", "no binding for key '%c' at %d,%d (%d,%d)", c, x-area.tl.x, y-area.tl.y, x, y);	
	}

	lookup(k, level)->room_type = room_type;
      }

      
      file.read_line(buf, 255);

      if((int)strlen(buf) != area.get_width()) {
	dbg->fatal("external_t::realize()", "level is %d cells wide, but ground line %d contains %d characters!", area.get_width(), y-area.tl.y, strlen(buf));
      }


      for(int x=area.tl.x; x<=area.br.x; x++) {
	
	const char c = buf[x-area.tl.x];

	if(grounds[c].img == -1) {
	  dbg->fatal("external_t::realize()", "no ground for key '%c' at %d,%d", c, x,y);
	} else {

	  if(c != ' ') {
	    square_t * square = lookup(koord(x,y), level);
	    groundhandle_t gr = square->get_ground();

	    if(gr.is_bound() == false) {
	      gr = new ground_t();

	      if(lit) {
		gr->visual.shade[0] = 20;
		gr->visual.shade[1] = 20;
		gr->visual.shade[2] = 20;
		gr->visual.shade[3] = 20;
	      } else {
		gr->visual.shade[0] = 0;
		gr->visual.shade[1] = 0;
		gr->visual.shade[2] = 0;
		gr->visual.shade[3] = 0;
	      }
	    }

	    // Hajo: Hack to enforce tiled floor
	    if(grounds[c].img < 10000) {
	      gr->visual.set_image(0x8000 + grounds[c].img);
	    } else {
	      gr->visual.set_image(grounds[c].img - 10000);	    
	    }

	    // printf("x=%d y=%d  img=%X\n", x, y, gr->visual.get_image(0));

	    square->set_ground(gr);
	  }
	}
      }
    }
    
    // Read things and triggers
    
    while(!file.eof()) {
      file.read_line(buf, 255);
      
      if(!file.eof()) {
	char key = buf[0];

	if(key == '^' || key == '%') {
	  // ^ = a trap
	  // % = a hot spot (room type)
	  
	  int type;
	  sscanf(buf+1,"%d", &type);
	  
	  // details
	  file.read_line(buf, 255);
	  int i,j;
	  sscanf(buf,"%d, %d", &i, &j);
	  
	  const koord k = map_flip(area.tl + koord(i,j));
	    
	  square_t *square = level->at(k);
	  if(square) {
	    if(key == '^') {
	      trigger_t * trigger = new trigger_t( k );
	      trigger->set_type(type);
	      square->set_trigger( trigger );
	    } else {
	      square->hots_type = type;
	    }
	  } 
	} else {
	  
	  // legacy itemon support

	  thinghandle_t thing = tf.create(buf, magic_max, magic_chance);
	  
	  if(thing.is_bound()) {
	    file.read_line(buf, 255);
	    int i,j;
	    sscanf(buf,"%d, %d", &i, &j);
	    
	    const koord k = map_flip(area.tl + koord(i,j));
	    
	    square_t *square = level->at(k);
	    if(square) {
	      level_factory_t::place_thing(level, k, thing, rng_t::get_the_rng());
	    } else {
	      dbg->warning("external_t::realize()", 
			   "pos %d,%d for thing is outside level", i, j);	    
	    }
	  }
	}
      }
    }
    file.close();  
  } else {
    dbg->error("external_t::realize()", "file '%s' not found!", filename);
  }
}
