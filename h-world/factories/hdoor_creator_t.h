#ifndef hdoor_creator_t_h
#define hdoor_creator_t_h

#include "feature_peer_creator_t.h"

class level_t;
class koord;

class hdoor_creator_t : public feature_peer_creator_t
{
 public:

  static void hdoor_creator_t::realize(level_t *level, 
				       const char * open, 
				       const char * closed, 
				       int open_x, int open_y,
				       const char * left, 
				       const char * center, 
				       const char * right,
				       const koord &left_pos);


 private:

 public:


  virtual feature_peer_t * create(koord k, const char * parameters);
  virtual const char * get_name();

  hdoor_creator_t();


};

#endif // hdoor_creator_t_h
