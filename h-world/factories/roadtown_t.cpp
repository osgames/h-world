/* 
 * roadtown_t.cpp
 *
 * Copyright (c) 2004 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "roadtown_t.h"

#include "primitives/area_t.h"

#include "model/ground_t.h"
#include "model/groundhandle_t.h"
#include "model/level_t.h"
#include "model/square_t.h"
#include "model/feature_t.h"
#include "model/pprops_t.h"

#include "util/rng_t.h"
#include "util/debug_t.h"

#include "external_t.h"
#include "feature_factory_t.h"


static koord dv[2] =
{
  koord(1,0), koord(0,1)
};

static const int RASTER = 16;


static void build_road(level_t * level, 
		       const koord pos, 
		       const int road_image)
{          
  const bool border = true;
  const koord size = level->get_size();

  if(border &&
     (pos.x <= 0 || pos.y <= 0 || pos.x >= size.x-1 || pos.y >= size.y-1)) {
    // Hajo: don't pierce impenetrable border
  } else { 
    
    square_t * square = level->at(pos);
    if(square) {

      groundhandle_t gr = square->get_ground();
      
      if(!gr.is_bound()) {
	gr = new ground_t();
	gr->visual.shade[0] = 20;
	gr->visual.shade[1] = 20;
	gr->visual.shade[2] = 20;
	gr->visual.shade[3] = 20;
	
	square->set_ground(gr);
      }
      
      gr->type = ground_t::ROAD;
      gr->visual.set_image(road_image); 
      
      square->set_feature(0);
      
      const int decos = level->get_properties()->get_int("town[0].road_decos", 0);
      
      for(int i=0; i<decos; i++) { 
	char buf[128];
	sprintf(buf, "town[0].road_deco[%d].chance", i);
	const int chance = level->get_properties()->get_int(buf, 0);
	
	sprintf(buf, "town[0].road_deco[%d].feature", i);
	const char * fname = level->get_properties()->get_string(buf, 0);
	
	if(fname == 0) {
	  dbg->error("roadtown_t::build_road()", 
		     "'%s' is missing in level template", buf);
	} else {
	  
	  if(rng_t::get_the_rng().get_int(100) < chance) {
	    feature_factory_t ff;
	    feature_t * feature = ff.create(fname);
	    
	    square->set_feature(feature);
	    
	    // Hajo: don't try other features, first one wins
	    break;
	  }
	}
      }
    }
  }

  // printf("%d, %d\n", pos.x, pos.y);
}


void roadtown_t::road(level_t * level, koord p1, int len, int dir)
{
  if(len < 5) {
    // Hajo: end recursion on too short roads
    return;
  }

  const int tex = level->get_properties()->get_int("town[0].road_texture", 0);
  const koord size = level->get_size();
  rng_t & rng = rng_t::get_the_rng();
  koord p = p1;

  for(int i=0; i<len; i++) { 

    area_t area (p, p+koord(2,2));
    area_iterator_t iter (area);
    while( iter.next() ) {
      build_road(level, iter.get_current(), tex);
    }


    // Hajo: spread more roads
    if((p.x % RASTER) == 0 && (p.y % RASTER) == 0 && 
       i > 0 && i<len-1 && rng.get_int(100) < 80) {
      int r = rng.get_int(4);

      if(r < 3) {
	r = rng.get_int(4);
      }

      if(r & 1) {
	int newlen = len/3 + rng.get_int(len/3);
	newlen -= newlen % RASTER;
	road(level, p, newlen, !dir);
      }

      if(r & 2) {
	int newlen = len/3 + rng.get_int(len/3);
	newlen -= newlen % RASTER;
	road(level, p - (dv[!dir] * newlen), newlen, !dir);
      }
    }
    
    p += dv[dir];

    if(p.x < 0 || p.y < 0 || p.x >= size.x || p.y >= size.y) {
      return;
    } 
  }
}


/**
 * Checks if a player position if given in one of the templates
 * @return player position or -1,-1 if no position is given
 * @author Hj. Malthaner
 */
koord roadtown_t::get_player_pos()
{
  return player_pos;
}


roadtown_t::roadtown_t()
{
  player_pos = koord(-1, -1);
}


static bool pos_for_house(level_t * level, koord pos)
{
  bool ok = false;
  const koord size = level->get_size();

  if(pos.x >= 0 && pos.y >= 0 && 
     pos.x < size.x - RASTER && pos.y < size.y - RASTER) {

    square_t * square = level->at(pos + koord(0, RASTER));
    if(square) {
      groundhandle_t gr = square->get_ground();
      
      ok |= gr->type == ground_t::ROAD;
    }

    square = level->at(pos + koord(RASTER, RASTER));
    if(square) {
      groundhandle_t gr = square->get_ground();
      
      ok |= gr->type == ground_t::ROAD;
    }
  }

  return ok;
}


/**
 * Realizes this structure in the level
 * @return player position or -1,-1 if no player position could be
 * found in templates.
 * @author Hj. Malthanr
 */
void roadtown_t::realize(level_t * level)
{
  properties_t * props = level->get_properties();
  const koord size = level->get_size();
  rng_t & rng = rng_t::get_the_rng();

  koord p1 ((size.x/2/RASTER)*RASTER, 1*RASTER);
  int len = props->get_int("town[0].size", 5) * RASTER;
  int dir = 1;
  
  road(level, p1, len, dir);

  vector_tpl <koord, unsigned int> places (1024); 

  for(int j=0; j<size.y; j+=RASTER) { 
    for(int i=0; i<size.x; i+=RASTER) { 

      koord pos (i,j);

      if(pos_for_house(level, pos)) {
	places.append(pos);
      }
    }
  }

  const int fixed = props->get_int("town[0].fixed", 0);


  for(int i=0; i<fixed; i++) { 
    char buf[128];

    sprintf(buf, "town[0].fixed[%d].file", i);

    const unsigned int r = rng.get_int(places.count());

    const koord pos = places.at(r);
    places.remove_at(r);


    external_t ex (area_t(pos+koord(3,3), 
			  pos+koord(RASTER, RASTER)),
		   props->get_string(buf),
		   external_t::topleft,
		   external_t::topleft);
	
    ex.realize(level);

    if(player_pos == koord(-1,-1)) {
      player_pos = ex.get_player_pos();
    }
  }


  const int random = props->get_int("town[0].random", 0);

  while(places.count() > 0) {
    const int r = places.count()-1;
    const koord pos = places.at(r);
    places.remove_at(r);

    if(rng.get_int(100) > 20) {
      char buf[128];
      sprintf(buf, "town[0].random[%d].file", rng.get_int(random));

      external_t ex (area_t(pos+koord(3,3), 
			    pos+koord(RASTER, RASTER)),
		     props->get_string(buf),
		     external_t::topleft,
		     external_t::topleft);
      
      ex.realize(level);

      if(player_pos == koord(-1,-1)) {
        player_pos = ex.get_player_pos();
      }
    }
  }
}
