/* Copyright by Hj. Malthaner */

#ifndef THING_FACTORY_T_H
#define THING_FACTORY_T_H

#ifndef thinghandle_t_h
#include "model/thinghandle_t.h"
#endif

class properties_t;
class cstring_t;
class limb_t;
class usage_t;
class sections_t;

struct prefix_t;


/**
 * Factory class to create things (monsters, items)
 *
 * @author Hj. Malthaner
 */
class thing_factory_t {
public:

  /**
   * Adds the specified usages to the thing, including attacks
   * @author Hj. Malthaner
   */
  static void add_usages(thinghandle_t &t, const properties_t *props);


  /**
   * Reads configuration data from file
   * @author Hj. Malthaner
   */
  static void init(cstring_t & dirname);


    thing_factory_t();


    /**
     * Checks if a thing can be created by this factory
     * @param name the identifier (section name)
     * @author Hj. Malthaner
     */
    bool is_available(const char *name);


    /**
     * Create a thing.
     * @param n        max number of magic attributes
     * @param chance   chance to add a magic attribute
     * @author Hj. Malthaner
     */
    thinghandle_t  create(const char *name,
			  int n,
			  const int chance);


    /**
     * Create a random thing.
     * @param type only generate items of a certain type, pass 0 for any type
     * @param ibase    base item level
     * @param irange   item level range (added to base, randomized)
     * @param n        max number of magic attributes
     * @param chance   chance to add a magic attribute
     * @author Hj. Malthaner
     */
    thinghandle_t  create_random(const char *type,
				 int ibase,
				 int irange,
				 int n,
				 const int chance);


    /**
     * Creates a random thing given by an probability chain of identtifiers
     * It may return an unbound handle if the chain yields no item!
     * @param props contains the chain
     * @param base  the base name of the chain
     * @author Hj. Malthaner
     */
    thinghandle_t create_by_chain(properties_t *props, 
				  const char * base);



private:    

    static sections_t *beings;
    static sections_t *limbs;


    /**
     * Item attribute generation - if an attribute for this item type is
     * possible, this function creates one
     *
     * @author Hj. Malthaner
     */
    static bool add_attribute(thinghandle_t & thing);


    /**
     * builds an usage object for things which have an usage
     * @author Hj. Malthaner
     */
    static usage_t * build_usage(const properties_t *props, int n);


    /**
     * builds an attack object for things which have an attack usage
     * @param n build nth attack for thing
     * @author Hj. Malthaner
     */
    static usage_t * build_attack(thinghandle_t & item,
			   const properties_t *props, int n);


    /**
     * Constructs a thing from the properties
     */
    thinghandle_t  build_thing(thinghandle_t & root, properties_t *); 


    void equip_thing(thinghandle_t & thing, const properties_t *props,
		     int n,
		     const int chance);

};
#endif //THING_FACTORY_T_H
