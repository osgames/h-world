#ifndef fountain_creator_t_h
#define fountain_creator_t_h

#include "feature_peer_creator_t.h"

class level_t;
class koord;

class fountain_creator_t : public feature_peer_creator_t
{
 public:

  static void fountain_creator_t::realize(level_t *level, 
					  const koord &pos);


 private:

 public:


  virtual feature_peer_t * create(koord k, const char * parameters);
  virtual const char * get_name();

  fountain_creator_t();


};

#endif // fountain_creator_t_h
