/* 
 * thing_factory_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "model/inventory_t.h"
#include "model/memory_t.h"
#include "model/thing_t.h"
#include "model/world_t.h"
#include "model/thing_constants.h"
#include "model/thing_tree_t.h"

#include "view/world_view_t.h"
#include "view/player_visual_connector_t.h"

#include "control/attack_t.h"
#include "control/pick_thing_t.h"
#include "control/lua_call_t.h"
#include "control/action_speak_t.h"
#include "control/action_trade_t.h"
#include "control/action_swap_t.h"
#include "control/action_move_t.h"
#include "control/action_imove_t.h"
#include "control/action_attack_t.h"
#include "control/action_lua_t.h"


#include "thing_factory_t.h"

#include "util/rng_t.h"
#include "util/properties_t.h"
#include "util/sections_t.h"
#include "util/debug_t.h"
#include "util/config_file_t.h"

#include "primitives/cstring_t.h"

#include "tpl/pair_tpl.h"
#include "tpl/stringhashtable_tpl.h"
#include "tpl/vector_tpl.h"

/*
 * Global thing config data
 * @author Hj. Malthaner
 */
sections_t * thing_factory_t::beings = 0;
sections_t * thing_factory_t::limbs = 0;


/*
 * Magic item attributes
 * @author Hj. Malthaner
 */
static sections_t * attributes = 0;
static sections_t * item_attributes = 0;


/*
 * Items ordered by level
 * @author Hj. Malthaner
 */
static vector_tpl <slist_tpl <const char *> *, unsigned int> * items_by_level = 0; 


static int get_int(const properties_t *props, const char * key)
{
  const int * i = props->get_int(key);

  if(i == 0) {
    dbg->fatal("get_int()", "Entry '%s' is missing", key);
  }

  return *i;
}


/**
 * Item attribute generation - if an attribute for this item type is
 * possible, this function creates one
 *
 * @author Hj. Malthaner
 */
bool thing_factory_t::add_attribute(thinghandle_t & thing)
{
  bool ok = false;
  const char *type = thing->get_string("type", "X");
  rng_t & rng = rng_t::get_the_rng();
  

  properties_t *attribute_words = item_attributes->get_section(type);

  //  dbg->message("thing_factory_t::add_prefix()", "found %p prefixes for type '%s'", pref_words, type);
  

  if(attribute_words) {

    const slist_tpl<const char *> * keys = attribute_words->get_keys();

    if(keys->count() > 0) {

      // We try seven times to add an attribute
      for(int trie=0; trie<7 && ok==false; trie++) {

	const char * pref_key = keys->at(rng.get_int(keys->count()));
	const char * att_word = attribute_words->get_string(pref_key);

	const properties_t * att = attributes->get_section(att_word);

	dbg->message("thing_factory_t::add_prefix()", "attribute '%s' defs %p", att_word, att); 


	if(att) {
	  int n = -1;
	  char buf [128];
	  const char * effect;
	  bool skip = false;

	  do{
	    n++;
	    sprintf(buf, "att-%d.effect", n);
	    effect = thing->get_string(buf, 0);
	    
	    if(effect && strcmp(effect, att->get_string("effect")) == 0) {
	      skip = true;
	    }

	  } while(effect && !skip);


	  if(skip) {
	    // double effect -> don't use it
	  } else {

	    const int power = 
	      get_int(att, "modifier.base") + rng.get_int(get_int(att, "modifier.range"));
	    
	    sprintf(buf, "att-%d.prefix", n);
	    thing->set(buf, att->get_string("prefix"));

	    sprintf(buf, "att-%d.postfix", n);
	    thing->set(buf, att->get_string("postfix"));

	    sprintf(buf, "att-%d.target", n);
	    thing->set(buf, att->get_string("target"));
	  
	    sprintf(buf, "att-%d.effect", n);
	    thing->set(buf, att->get_string("effect"));

	    sprintf(buf, "att-%d.value", n);
	    thing->set(buf, power);
      
	    const int price = 
	      thing->get_int("value", 0) + 
	      power * (get_int(att, "modifier.price"));
	  
	    thing->set("value", price);


	    // Hajo: check curse
	    if(get_int(att, "curse_chance") > rng.get_int(100)) {
	      thing->set("cursed", "true");      
	    }
	    
	    ok = true;
	  }
	} else {
	  dbg->error("thing_factory_t::add_prefix()", 
		     "prefix '%s' not found in prefix table",
		     pref_key);
	}
      }
    }

    delete keys;
    keys = 0;
  }

  return ok;
}

 
/**
 * builds an usage object for things which have an usage
 * @author Hj. Malthaner
 */
usage_t * thing_factory_t::build_usage(const properties_t *props, 
				       const int n)
{
  const char * mid =  "thing_factory_t::build_usage()";

  char buf [128];
  usage_t *usage = 0;

  sprintf(buf, "usage[%d].name", n);
  if(!props->get_string(buf)) {
    dbg->fatal(mid, "usage %d has no name!", n);
  }

  if(strcmp(props->get_string(buf), "pick_thing") == 0) {
    usage = new pick_thing_t();
  } 
  else if(strcmp(props->get_string(buf), "lua_call") == 0) {

    sprintf(buf, "usage[%d].call", n);
    if(props->get_string(buf)) {
      lua_call_t * call = new lua_call_t(props->get_string(buf));
      usage = call;

    } else {
      dbg->fatal(mid, "%s is missing for %s/%s", 
		 buf, props->get_string("ident"), props->get_string("name"));
    }
  }
  else {
    dbg->fatal(mid, "unknown usage '%s'", props->get_string(buf));
  }


  sprintf(buf, "usage[%d].verb", n);
  if(props->get_string(buf)) {
    usage->set_verb(props->get_string(buf));
  }

  return usage;
}



/**
 * builds an attack object for things which have an attack usage
 * @author Hj. Malthaner
 */
usage_t * thing_factory_t::build_attack(thinghandle_t &item,
					const properties_t *props, int n)
{
  const char * mid =  "thing_factory_t::build_attack()";

  char buf [128];
  attack_t *attack;

  sprintf(buf, "attack[%d].name", n);

  attack = new attack_t(props->get_string(buf), item);
  policy_t &policy = attack->get_policy();
  policy.effect = policy_t::none;


  sprintf(buf, "attack[%d].verb", n);
  if(props->get_string(buf)) {
    attack->set_verb(props->get_string(buf));
  }

  sprintf(buf, "attack[%d].delay", n);
  if(props->get_int(buf)) {
    attack->set_delay(*props->get_int(buf));
  } else {
    dbg->fatal(mid, "delay is missing for attack '%s'", 
	       attack->get_name());
  }

  sprintf(buf, "attack[%d].pierce", n);
  if(props->get_int(buf)) {
    attack->set_pierce(*props->get_int(buf));
  } else {
    dbg->fatal(mid, "pierce is missing for attack '%s'", 
	       attack->get_name());
  }



  sprintf(buf, "attack[%d].damages", n);

  const int damages = get_int(props, buf);

  for(int i=0; i<damages; i++) {
    sprintf(buf, "attack[%d].damage[%d].type", n, i);
  
    const char *type = props->get_string(buf);
    if(type) {
      sprintf(buf, "attack[%d].damage[%d].damage", n, i);
      const char * p = props->get_string(buf);

      if(strcmp(type, "blunt") == 0) {

	attack->set_blunt(dice_t(p+1));
	policy.effect += policy_t::damage_blunt;

      } else if(strcmp(type, "cut") == 0) {
	attack->set_cut(dice_t(p+1));
	policy.effect |= policy_t::damage_cut;	

      } else {
	dbg->fatal(mid, "unknown damage type '%s' for attack '%s'", 
		   type, attack->get_name());
      }

    } else {
	dbg->fatal(mid, "attack '%s' without damage type", attack->get_name());
    }
  }


  policy.target = policy_t::thing;
  policy.range = 1;

  sprintf(buf, "attack[%d].range", n);
  if(props->get_int(buf)) {
    policy.range = *props->get_int(buf);
  }

  return attack;
}


/**
 * Adds the specified usages to the thing, including attacks
 * @author Hj. Malthaner
 */
void thing_factory_t::add_usages(thinghandle_t &thing, 
				 const properties_t *props)
{
  if(props->get_int("attacks")) {
    const int n = *props->get_int("attacks");

    for(int i=0; i<n; i++) {
      thing->add_usage(build_attack(thing, props, i));
    }
  }

  if(props->get_int("usages")) {
    const int n = *props->get_int("usages");

    for(int i=0; i<n; i++) {
      thing->add_usage(build_usage(props, i));
    }
  }
}


thinghandle_t  thing_factory_t::build_thing(thinghandle_t & root, 
					    properties_t *props)
{
  const char * mid =  "thing_factory_t::build_thing()";

  thinghandle_t thing( new thing_t(root) );

  
  // Hajo: build the body

  if(props->get_int("limbs")) {
    const int limb_count = * props->get_int("limbs");
    for(int i=0; i<limb_count; i++) {
      char buf[256];
      
      sprintf(buf, "limb[%d]", i);
      
      if(props->get_string(buf)) {
	properties_t * section = limbs->get_section(props->get_string(buf)); 

	if(section) {
	  thinghandle_t limb = build_thing(thing, section);
	  thing->add_limb(limb);

	  // Hajo: check direction override
	  sprintf(buf, "limb[%d].direction", i);
	  if(props->get_int(buf)) {
	    limb->set("direction", *props->get_int(buf));
	  }

	} else {
	  dbg->fatal(mid, "No limb dscription for '%s'!", props->get_string(buf));
	}				   
	
      } else {
	dbg->warning(mid, "'%s' is missing", buf);
      }
    }
  }


  // Hajo: Does this thing have an inventory?
  if(props->get_string("inventory")) {
    koord size;
    size.x = get_int(props, "inventory.width");
    size.y = get_int(props, "inventory.height");

    inventory_t *inventory = new inventory_t(size, thing);

    thing->set_inventory(inventory);
  }


  // Hajo: Does this thing have a memory?
  if(props->get_string("memory")) {
    memory_t *memory = new memory_t();
    thing->set_memory(memory);
  }


  add_usages(thing, props);


  // Hajo: Cache size for efficiency

  if(props->get_int("width")) {
    if(props->get_int("height")) {
      thing->set_inv_size(koord(*props->get_int("width"),
				*props->get_int("height")));;
    } else {
      dbg->fatal(mid, "height entry is missing in thing '%s'",
		 props->get_string("ident"));
    }  
  } else {
      dbg->fatal(mid, "width entry is missing in thing '%s'",
		 props->get_string("ident"));
  }


  // Hajo: Add actions


  if(props->get_string("dialog.auto")) {
    thing->get_actor().add_action(new action_speak_t(thing,
						     props->get_string("dialog")
						     )
				  );
  }


  // Hajo: set properties ... doing some checks here?

  const slist_tpl<const char*> *keys = props->get_keys();

  slist_iterator_tpl<const char *> iter (keys);

  while(iter.next()) {
    const char *key = iter.get_current();
    
    if(props->get_int(key)) {
      thing->set(key, *props->get_int(key));
    } else {
      thing->set(key, props->get_string(key));
    }
  }

  delete keys;
  keys = 0;

  // Hajo: randomize sleep

  const int sleep = thing->get_int("sleep", 0);
  if(sleep > 0 ) {
    thing->set("sleep", rng_t::get_the_rng().get_int(sleep));
  }


  // Hajo: set flavour

  world_t *world = world_t::get_instance();

  const int flavor = thing->get_int(TH_FLAVOR, 0);
  int kind = thing->get_int(TH_KIND, 0);
 
  // randomize color?
  const char * random_kind = props->get_string(TH_KIND, 0);

  if(random_kind) {
    dice_t dice (random_kind+1);
    kind = dice.roll();
  }


  // Hajo: kind can override flavour
  const int colorset = kind ? kind : world->flavors[flavor];

  thing->set("colorset", colorset);


  player_visual_connector_t::prepare_images(thing.get_rep(), 
					    thing->get_properties());


  if(props->get_string("alive")) {
    
    // Hajo: monsters move
    thing->get_actor().add_action(new action_move_t(thing));
    thing->get_actor().add_action(new action_imove_t(thing));

    // Hajo: monsters fight
    thing->get_actor().add_action(new action_attack_t(thing));


    // Hajo: process AI lua calls
    int n = thing->get_int("AI", 0);

    for(int i=0; i<n; i++) { 
      char buf[256];

      sprintf(buf, "AI[%d]", i);

      const char * call = thing->get_string(buf, 0);

      if(call) {
	thing->get_actor().add_action(new action_lua_t(thing, call));
      }
    }

    
    // Hajo: randomize movement a bit
    thing->get_actor().add_time(rng_t::get_the_rng().get_int(10));

    // Hajo: forget about monsters if out of sight
    thing->visual.set_transient(true);
  }  


  return thing;
} 


void thing_factory_t::equip_thing(thinghandle_t & thing, 
				  const properties_t *props,
				  int magic_max,
				  const int magic_chance)
{
  const char * mid = "thing_factory_t::equip_thing()";
  rng_t & rng = rng_t::get_the_rng();

  const int n = props->get_int("things", 0);

  // const int magic_max = props->get_int("magic.max", 0);
  // const int magic_chance = props->get_int("magic.chance", 0);

  for(int i=0; i<n; i++) {
    char buf[256];

    sprintf(buf, "thing[%d].variants", i);
      
    int variants = props->get_int(buf, 1);
    int number = rng_t::get_the_rng().get_int(variants);

    sprintf(buf, "thing[%d][%d]", i, number);

    if(props->get_string(buf)) {
      thinghandle_t equipment = 
	create(props->get_string(buf), magic_max, magic_chance);

      // randomize color?
      sprintf(buf, "thing[%d][%d].kind", i, number);
      const char * kind = props->get_string(buf);

      if(kind) {
	dice_t dice (kind+1);
	equipment->set("colorset", dice.roll());

	player_visual_connector_t::prepare_images(equipment.get_rep(), 
						  equipment->get_properties());

	player_visual_connector_t::calc_overlays(equipment);
      }


      // put item in inventory ?
      sprintf(buf, "thing[%d][%d].inventory.type", i, number);

      if(props->get_string(buf)) {
	const char *type = props->get_string(buf);

	thing_tree_t tree;
	slist_tpl <thinghandle_t> list;
	
	// false means not to list inventory contents
	tree.list_inventories(thing, &list, false);
	
	slist_iterator_tpl <thinghandle_t> iter (list);

	while( iter.next() ) {
	  const thinghandle_t & cont = iter.get_current();

	  if(strcmp(cont->get_string("type", "xxx"), type) == 0) {
	    cont->get_inventory()->add(equipment);
	    
	    equipment->set("is_unknown", "false");
	    equipment->set("curse_known", "true");

	    break;
	  }
	}
      } else {

	// put item on body part ?

	sprintf(buf, "thing[%d][%d].location.type", i, number);

	if(props->get_string(buf)) {
	  const char *type = props->get_string(buf);
	  
	  sprintf(buf, "thing[%d][%d].location.subtype", i, number);
	  
	  thinghandle_t limb = thing_t::find_limb(thing, 
						  type,
						  props->get_string(buf));
	  
	  if(limb != NULL) {
	    dbg->message(mid, "Adding a %s to %s", 
			 equipment->get_string("name", "unnamed"),
			 limb->get_string("name", "unnamed"));
	    limb->set_item(equipment, true);
	    
	    sprintf(buf, "thing[%d][%d].cur_stack", i, number);
	    if(props->get_int(buf)) {
	      equipment->set("cur_stack", *props->get_int(buf));
	    }

	  } else {
	    dbg->fatal(mid, "limb %s, %s not found in thing %s"
		       " while equipping %s", 
		       type, 
		       props->get_string(buf), 
		       thing->get_string("ident", "unnamed"),
		       equipment->get_string("ident", "unnamed"));
	  }
	} else {
	  dbg->fatal(mid, 
		     "either location.type or inventory.type entry is requried for thing %s", 
		     buf, props->get_string("ident"));
	}
      }
    } else {
      dbg->fatal(mid, "%s entry is missing in thing", 
		 buf, props->get_string("ident"));
    }
  }


  if(props->get_int("treasures") && thing->get_inventory()) {
    const int n = *props->get_int("treasures");
    for(int i=0; i<n; i++) {
      char buf[256];
      const char * key = 0;

      // Hajo: random chance
      sprintf(buf, "treasure[%d].chance", i);
      const int chance = *props->get_int(buf);

      if(rng.get_int(100) < chance) {

	// Hajo: there are two options: type or ident
	sprintf(buf, "treasure[%d].type", i);
	
	key = props->get_string(buf);

	if(key) {
	  thinghandle_t treasure = create_random(key, -1, -1, 
						 magic_max, magic_chance);
	  if(treasure.is_bound()) {
	    thing->get_inventory()->add(treasure);
	  }
	} else {
	  sprintf(buf, "treasure[%d].ident", i);
	  key = props->get_string(buf);
	  
	  thinghandle_t treasure = create(key, magic_max, magic_chance);
	  if(treasure.is_bound()) {
	    thing->get_inventory()->add(treasure);
	  }
	}
      }
    }
  }


  // traders trade
  if(props->get_string("trader") && *props->get_string("trader") == 't') {
    thing_tree_t tree;

    action_trade_t *trade = new action_trade_t(thing);
    trade->set_price_factor(340);  // 75 %
    thing->get_actor().add_action(trade);

    // add goods for trader
    
    slist_tpl < handle_tpl < thing_t > > list;
    char buf [256];

    if(props->get_int("goods")) {
      const int goods = *props->get_int("goods");
      
      for(int i=0; i<goods; i++) { 
	sprintf(buf, "good[%d]", i);
	const char * type = props->get_string(buf);
      
	// false means not to list inventory contents
	tree.list_inventories(thing, &list, false);
	inventory_t * inv = list.at(0)->get_inventory();
	list.clear();
      
	const int amount = rng.get_int(25/goods + 1) + 9/goods;

	for(int j=0; j<amount; j++) {
	  thinghandle_t thing = create_random(type, -1, -1, 
					      magic_max, magic_chance);
	
	  // Hajo: don't offer things that have negative values
	  if(thing.is_bound() && thing->get_int("value", 0) >= 0) {

	    // Traders initially only have identified items
	    thing->set("is_unknown", "false");

	    // Traders items are either plain or uncursed
	    // but state is always known
	    thing->set("curse_known", "true");


	    inv->add( thing );
	  }
	}
      }
    }
  }
}


/**
 * Checks if a thing can be created by this factory
 * @param name the identifier (section name)
 * @author Hj. Malthaner
 */
bool thing_factory_t::is_available(const char *name)
{
  return (beings->get_section(name) != 0);
}


/**
 * Create a thing.
 * @param n        max number of magic attributes
 * @param chance   chance to add a magic attribute
 * @author Hj. Malthaner
 */
thinghandle_t  thing_factory_t::create(const char *name,
				       int n,
				       const int chance)
{
  /*
  dbg->message("thing_factory_t::create()",
	       "building a '%s'", name);
  */

  thinghandle_t thing;

  if(beings->get_section(name)) {
    thing = build_thing(thing, beings->get_section(name));

    thing->set("section.name", name);

    /*
    dbg->message("thing_factory_t::create()",
		 "stuffing a '%s' (%s)", 
		 name, 
		 thing->get_string("name", "unnamed"));
    */

    equip_thing(thing, beings->get_section(name), n, chance);


    // magic items

    bool ok = false;
    do {
      if(n > 0 && rng_t::get_the_rng().get_int(100) < chance) {
	ok = add_attribute(thing);
      }
      n--;
    } while(ok && n > 0);

    player_visual_connector_t::calc_overlays(thing);


    // post creation Lua hook - game specific customizations
    lua_call_t call("on_creation");
    call.activate(thing, 0);

  }
  else {
    dbg->fatal("thing_factory_t::create()", "no section found for '%s'", name);
    
  }

  return thing;
}


/**
 * Create a random thing.
 * @param type only generate items of a certain type, pass 0 for any type
 * @param ibase    base item level
 * @param irange   item level range (added to base, randomized)
 * @param n        max number of magic attributes
 * @param chance   chance to add a magic attribute
 * @author Hj. Malthaner
 */
thinghandle_t  thing_factory_t::create_random(const char *type,
					      int ibase,
					      int irange,
					      int n,
					      const int chance)
{
  slist_tpl <const char *> * keys = 0;
  vector_tpl <const char *, unsigned int> geno (8192);

  // Hajo: modify item level by range
  int ilevel = rng_t::get_the_rng().get_int(ibase + irange);
    
  do {

    if(ibase == -1) {
      // Hajo: allow all item levels
      keys = beings->get_sections()->get_keys();
    } else {
    
      // reduce level until an item is found
      while(ilevel > 0 && 
	    (items_by_level->at(ilevel) == 0 ||
	     items_by_level->at(ilevel)->count() == 0)) {
	ilevel --;
      }

      // Hajo: XXX there must be at least one level 0, non special item!!!

      keys = items_by_level->at(ilevel);

      dbg->debug("thing_factory_t::create_random()", 
		 "Item level %d, keys=%p, count=%d, type=%s", 
		 ilevel, keys, keys->count(), type);
    }

    slist_iterator_tpl <const char *> iter (keys);

    while(iter.next()) {
      const char *key = iter.get_current();
      properties_t *props = beings->get_sections()->get(key);

      if(! props->get("special")) {
	
	if(type == 0 || strcmp(type, props->get_string("type")) == 0) {

	  int commonness = props->get_int("commonness", 10);

	  for(int i=0; i<commonness; i++) { 
	    geno.append(key);
	  }
	}
      }
    }
  } while(ibase != -1 && geno.count() == 0 && ilevel-- > 0);


  thinghandle_t thing;

  if(geno.count()) {
    const int i = rng_t::get_the_rng().get_int(geno.count());

    dbg->debug("thing_factory_t::create_random()", 
	       "Choosing %s (%d) out of %d choices",
	       geno.at(i), i, geno.count());

    thing = create(geno.at(i), n, chance);
  } else {
    dbg->fatal("thing_factory_t::create_random()", 
	       "Couldn't create item for " 
	       "type=%s ibase=%d irange=%d (keys=%d)",
	       type, ibase, irange, keys->count());
  }

  if(ibase == -1) {
    delete keys;
  }

  return thing;
}



/**
 * Creates a random thing given by an probability chain of identtifiers
 * It may return an unbound handle if the chain yields no item!
 * @param props contains the chain
 * @param base  the base name of the chain
 * @author Hj. Malthaner
 */
thinghandle_t thing_factory_t::create_by_chain(properties_t *props, 
					       const char * base)
{
  thinghandle_t result;
  rng_t & rng = rng_t::get_the_rng();
  int i = 0;
  const int * chance;
  char key [256];

  const int magic_max = props->get_int("magic.max", 0);
  const int magic_chance = props->get_int("magic.chance", 0);


  do {
    sprintf(key, "%s-%d.chance", base, i);
    chance = props->get_int(key);

    if(chance != 0) {

      // printf("Trying '%s-%d', chance=%d\n", base, i, *chance);

      if(rng.get_int(100) < *chance) {
	sprintf(key, "%s-%d.ident", base, i);
	const char * ident = props->get_string(key);

	if(ident) {
	  result = create(ident, magic_max, magic_chance);
	} else {
	  sprintf(key, "%s-%d.type", base, i);
	  const char * ident = props->get_string(key);
	  if(ident) {
	    result = create_random(ident, -1, -1, magic_max, magic_chance);
	  } else {
	    dbg->fatal("thing_factory_t::create_by_chain()",
		       "neither %s-%d.type nor %s-%d.ident could be found!",
		       base, i, base, i);
	  }
	}

	// Hajo: visual offset
	sprintf(key, "%s-%d.xoff", base, i);
	const int xoff = props->get_int(key, 0);

	sprintf(key, "%s-%d.yoff", base, i);
	const int yoff = props->get_int(key, 0);
	
	result->visual.set_x_off(xoff);
	result->visual.set_y_off(yoff);

	break;
      }
    }
    i++;
  } while(chance != 0);

  return result;
}



/**
 * Reads configuration data from file
 * @author Hj. Malthaner
 */
void thing_factory_t::init(cstring_t & dirname)
{
  if(limbs == 0) {
    limbs = new sections_t();
    if(limbs->load(dirname + "limbs.sects")) {
      if(beings == 0) {
	beings = new sections_t();
	if(beings->load(dirname + "beings.sects")) {

	  // Hajo: try to read custom things
	  beings->load(dirname + "/custom/things.sects");

	} else {
	  dbg->warning("thing_factory_t::init()",
		       "Can't read '%sbeings.sects'\n", dirname.chars());
	}
      }
    } else {
      dbg->warning("thing_factory_t::init()",
		   "Can't read 'data/limbs.sects'\n");
    }
  }

  if(attributes == 0) {
    cstring_t filename = dirname + "attributes.sects";

    attributes = new sections_t();

    if(attributes->load(filename)) {
      dbg->message("thing_factory_t::thing_factory_t()", 
		   "loading attributes ... success.");
    } else {
      dbg->fatal("thing_factory_t::thing_factory_t()", 
		 "error while reading file '%s'", filename.chars());
    }
  }

  if(item_attributes == 0) {

    item_attributes = new sections_t();

    cstring_t filename = dirname + "item_attributes.sects";

    if(item_attributes->load(filename)) {
      dbg->message("thing_factory_t::thing_factory_t()", 
		   "loading item attribute links ... success.");
    } else {
      dbg->fatal("thing_factory_t::thing_factory_t()", 
		 "error while reading file '%s'", filename.chars());
    }
    
  }

  // sort items by level
  if(items_by_level == 0) {
    items_by_level = new vector_tpl <slist_tpl <const char *> *, unsigned int> (128);

    for(int i=0; i<128; i++) { 
      items_by_level->append(0);
    }

    slist_tpl <const char *> * keys = beings->get_sections()->get_keys();

    slist_iterator_tpl <const char *> iter (keys);

    while(iter.next()) {
      properties_t * props = beings->get_section(iter.get_current());

      int ilevel = 0;
      const int * ip;
      if( (ip=props->get_int("ILevel")) ) {
	ilevel = *ip;
      }

      ilevel = (ilevel < 0) ? 0 : (ilevel > 127) ? 127 : ilevel;

      if(items_by_level->at(ilevel) == 0) {
	items_by_level->at(ilevel) = new slist_tpl<const char *> ();
      }

      items_by_level->at(ilevel)->append(iter.get_current());

      dbg->debug("thing_factory_t::init()", 
		   "Adding '%s' at item level %d, now %d items", 
		   iter.get_current(), 
		   ilevel,
		   items_by_level->at(ilevel)->count());


    }

    
    delete keys;
  }
}


thing_factory_t::thing_factory_t()
{
}
