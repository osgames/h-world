/* 
 * town_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "model/groundhandle_t.h"
#include "model/featurehandle_t.h"
#include "model/feature_fountain_t.h"
#include "model/level_t.h"
#include "model/square_t.h"
#include "model/ground_t.h"
#include "model/feature_t.h"
#include "model/inventory_t.h"
#include "model/thing_t.h"
#include "model/pprops_t.h"

#include "util/rng_t.h"
#include "util/nsow.h"

#include "view/player_visual_connector_t.h"

#include "tpl/slist_tpl.h"

#include "thing_factory_t.h"
#include "feature_factory_t.h"
#include "town_t.h"


/**
 * mirror y
 * @author Hj. Malthaner
 */
void town_t::trans_y(const char *rule, char *tr)
{
    for(int x=0; x<3; x++) {
	for(int y=0; y<3; y++) {
	    tr[x + (2-y)*3] = rule[x+y*3];
	}
    }
}
                

/**
 * rotate left
 * @author Hj. Malthaner
 */
void town_t::trans_l(const char *rule, char *tr)
{
    for(int x=0; x<3; x++) {
	for(int y=0; y<3; y++) {
	    const int xx = y;
	    const int yy = 2-x;
	    tr[xx + yy*3] = rule[x+y*3];
	}
    }
}                      


/**
 * rotate right
 * @author Hj. Malthaner
 */
void town_t::trans_r(const char *rule, char *tr)
{
    for(int x=0; x<3; x++) {
	for(int y=0; y<3; y++) {
	    const int xx = 2-y;
	    const int yy = x;
	    tr[xx + yy*3] = rule[x+y*3];
	}
    }
}


/**
 * Symbols in rules:
 * S = must not be road
 * s = must be road
 * n = must be nature
 * . = matches anything
 *
 * @param pos position to check
 * @param rule the rule to evaluate
 * @return true on match, false otherwise
 * @author Hj. Malthaner
 */
bool town_t::evaluate_loc(const level_t *level,
			  koord pos, 
			  const char *rule)
{       
  koord k;
  bool ok=true;

  for(k.y=pos.y-1; ok && k.y<=pos.y+1; k.y++) {
    for(k.x=pos.x-1; ok && k.x<=pos.x+1; k.x++) {
      const square_t *square = level->at(k);
      
      ok = false;

      if(square && !square->get_feature().is_bound()) {
	groundhandle_t gr = square->get_ground();
	
	if(gr.is_bound()) {
	  
	  switch(rule[(k.x-pos.x+1) + (k.y-pos.y+1)*3]) {
	  case 's':
	    ok = gr->type == ground_t::ROAD;
	    break;
	  case 'S':
	    ok = gr->type != ground_t::ROAD;
	    break;
	  case 'n':
	    ok = gr->type == ground_t::LAND;
	    break;      
	  default:
	    ok = true;
	  }
	}
      }
    }
  }              
  return ok;
}


/**
 * Check rule in all transformations at given position
 * @author Hj. Malthaner
 */
int town_t::evaluate_pos(const level_t *level, koord pos, const char *rule)
{   
    char tr[9];		// fuer die 'transformierte' Rule
    int w;
    
    w = evaluate_loc(level, pos, rule);
    trans_y(rule, tr);
    w += evaluate_loc(level, pos, tr);
    trans_l(rule, tr);
    w += evaluate_loc(level, pos, tr);
    trans_r(rule, tr);
    w += evaluate_loc(level, pos, tr);

    return w;
}


void town_t::evaluate_road(const level_t * level, koord pos, 
			   int chance, const char *rule)
{                    
    rng_t & rng = rng.get_the_rng();
           
    if(rng.get_int(chance) == 0) {
	const int sw = evaluate_pos(level, pos, rule);
	if(sw > best_road_value) {
	    best_road_value = sw;
	    best_road_pos = pos;
	}                 
    }
}


void town_t::evaluate(const level_t *level)
{
    rng_t & rng = rng.get_the_rng();
    koord k;
    
    const int chance = 1;

    k.x = a.tl.x + rng.get_int(a.get_width());	// Zufallspos im Stadtgebiet
    k.y = a.tl.y + rng.get_int(a.get_height());

    best_road_value = 0;
    best_road_pos = k;      

    evaluate_road(level, k, chance+80,
		    "..."
		    "SnS"
		    "ssS");	// Kurve 1

    evaluate_road(level, k, chance+80,
		    "..."
		    "SnS"
		    "Sss");	// Kurve 2

    evaluate_road(level, k, chance+200,
		    "..."
		    "SnS"
		    "Ss.");	// Roadnende weiterbauen
    
    evaluate_road(level, k, chance+200,
		    "..."
		    "SnS"
		    ".sS");	// Roadnende weiterbauen

    evaluate_road(level, k, chance,  
		    "..."
		    ".n."
		    "SsS");	// Roadnende weiterbauen
    
    evaluate_road(level, k, chance+100,
		    "..."
		    "nnn"
		    "sss");    // Einmuendung in Natur


    //    printf("Testing %d,%d -> %d\n", k.x, k.y, best_road_value);

}               


void town_t::grow_borders(level_t * l, const koord k)
{          
    if(k.x <= a.tl.x && k.x > 0) {
	a.tl.x = k.x - 1;
    }          

    if(k.x >= a.br.x && k.x <= l->get_size().x) {
	a.br.x = k.x+1;
    }

    if(k.y <= a.tl.y && k.y > 0) {
	a.tl.y = k.y-1;
    }
               
    if(k.y >= a.br.y && k.y <= l->get_size().y) {
	a.br.y = k.y+1;
    }
}


void town_t::build_road(level_t * level, const koord pos, const int road_image)
{          
  groundhandle_t gr = level->at(pos)->get_ground();

  if(!gr.is_bound()) {
    gr = new ground_t();
    gr->visual.shade[0] = 20;
    gr->visual.shade[1] = 20;
    gr->visual.shade[2] = 20;
    gr->visual.shade[3] = 20;

    level->at(pos)->set_ground(gr);
  }

  gr->type = ground_t::ROAD;
  gr->visual.set_image(road_image); 
}


/**
 * Build a 3x3 house at pos
 * @author Hj. Malthaner
 */
void town_t::build_house(level_t * level, 
			 const koord pos, 
			 slist_tpl <koord> *house_list) 
{
  rng_t & rng = rng.get_the_rng();
           
  if(rng.get_int(100) < 30) {


    // sanity check
    if(level->at(pos-koord(1,1)) && 
       level->at(pos+koord(2,2))) {
      
      for(int j=-1; j<=2; j++) { 
	for(int i=-1; i<=2; i++) { 
	  if(level->at(pos+koord(i, j))->get_feature().is_bound()) {
	    // no place here
	    return;
	  }
	}
      }

      /* old code for building a house
    
      for(int j=0; j<3; j++) { 
	for(int i=0; i<3; i++) { 
	  
	  featurehandle_t feature (new feature_t() );   
	  feature->visual.set_image(-1);

	  level->at(pos+koord(i,j))->set_feature(feature);
	}
      }
 

      featurehandle_t feature = level->at(pos)->get_feature();
      feature->visual.set_tileset(1);
      feature->visual.set_image(image);
      feature->visual.set_x_off(-32);
      feature->visual.set_y_off(-32);

      */

      house_list->insert(pos);
    }
  }
}


/**
 * Basic constrcutor
 *
 * @param pos the location within the level to generate the town 
 */
town_t::town_t(koord pos) {
  a.tl = pos-koord(1,1);
  a.br = pos+koord(2,1);
}


/**
 * Realizes this town in the level
 * @author Hansj�rg Malthaner
 */
void town_t::realize(level_t *level, int size) {
  // XXX make this configurable!
  const int road_image = *level->get_properties()->get_int("town_road");
  slist_tpl <koord> house_list;
  rng_t & rng = rng_t::get_the_rng();

  koord center = a.tl+koord(1,1);
  build_road(level, center, road_image);

  // build roads
  for(int i=0; i<size; i++) { 
    evaluate(level);

    if(best_road_value > 0) {
      build_road(level, best_road_pos, road_image);

      grow_borders(level, best_road_pos);
    }
  }

  slist_tpl <koord> list;

  // widen roads
  for(int j=a.tl.y; j<=a.br.y; j++) { 
    for(int i=a.tl.x; i<=a.br.x; i++) { 
      // check neighbours
      koord pos (i,j);

      for(int n=0; n<4; n++) { 
	const koord k = pos+nsow[n];

	square_t *square = level->at(k);
	if(square) {
	  groundhandle_t gr = square->get_ground();
	  if(gr.is_bound() && gr->type == ground_t::ROAD) {
	    list.insert(pos);
	    break;
	  }
	}
      }
    }
  }
  
  slist_iterator_tpl <koord> iter (list);

  while( iter.next() ) {
    const koord &k = iter.get_current();
    // printf("road %d, %d\n", k.x, k.y);
    build_road(level, k, road_image);    
  }


  iter.begin();
  
  while( iter.next() ) {
    for(int n=0; n<4; n++) { 
      const koord k = iter.get_current()+nsow[n];

      square_t *square = level->at(k);
      if(square) {
	groundhandle_t gr = square->get_ground();
	if(gr.is_bound() && gr->type != ground_t::ROAD) {
	
	  // Test building
	  build_house(level, k, &house_list);
	}
      }
    }
  }


  // place a fountain
  feature_factory_t feature_factory;
  square_t *square = level->at(center);

  if(square) {
    featurehandle_t feature (feature_factory.create("fountain"));

    feature->visual.set_opaque(false);
    feature->set_peer(new feature_fountain_t());

    square->set_feature(feature);
  }
  

  thing_factory_t factory;

  int n = 0;
  for(int i=0; n<6 && i<house_list.count(); i++) {

    // Place a trader instead of a house
   
    const koord pos = house_list.at(rng.get_int(house_list.count()));
    house_list.remove(pos);

    if(level->at(pos)->get_things().count() == 0) {
      const char *what = 0;

      switch(n++ % 3) {
      case 0:
	what = "trader_temple";
	break;
      case 1:
	what = "trader_weapons";
	break;
      case 2:
	what = "trader_clothes";
	break;
      }

      thinghandle_t trader = factory.create(what, 0, 0);

      player_visual_connector_t conn (trader, trader->visual);
      trader->call_listeners();
      trader->remove_listener(&conn);


      level->at(pos)->add_thing(trader, pos);
    }
  }
}

