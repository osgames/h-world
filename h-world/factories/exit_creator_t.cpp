/* 
 * exit_creator_t.cpp
 *
 * Copyright (c) 2001 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "exit_creator_t.h"

#include "model/feature_stairs_t.h"
#include "model/feature_t.h"
#include "model/level_t.h"

#include "primitives/koord.h"


exit_creator_t::exit_creator_t()
{
}


feature_peer_t * exit_creator_t::create(koord k, const char * parameters)
{
  return new feature_stairs_t(parameters);
}


const char * exit_creator_t::get_name()
{
  return "exit";
}
