/* 
 * feature_factory_t.cpp
 *
 * Copyright (c) 2001 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "feature_factory_t.h"

#include "model/feature_t.h"

#include "view/world_view_t.h"

#include "util/properties_t.h"
#include "util/sections_t.h"
#include "util/debug_t.h"

#include "tpl/stringhashtable_tpl.h"

#include "primitives/cstring_t.h"


/*
 * Global feature config data - static private
 * @author Hj. Malthaner
 */
static sections_t *features = 0;


/**
 * Reads configuration data from file
 * @author Hj. Malthaner
 */
void feature_factory_t::init(cstring_t & dirname)
{
  if(features == 0) {
    features = new sections_t();
    if(features->load(dirname + "/data/features.sects")) {
      dbg->message("feature_factory_t::init()",
		   "Reading '%s/data/features.sects'\n", dirname.chars());


      // preload all images
      const slist_tpl<const char *> * keys = features->get_sections()->get_keys();

      slist_iterator_tpl <const char *> iter ( keys );

      while( iter.next() ) {
	properties_t * props = features->get_section(iter.get_current());

	const char * file = props->get_string("img");

	if(file) {
	  cstring_t filename = dirname + "/" + file;
	  world_view_t::get_instance()->load_tile_from_image(filename);
	  props->set("img", filename);
	}

	props->set("sct", iter.get_current());
      }

      delete keys;
      keys = 0;
      
    } else {
      dbg->fatal("feature_factory_t::init()",
		 "Can't read '%s/data/features.sects'\n", dirname.chars());
    }
  }
}


/**
 * Gets the properties of a given feature
 * @author Hj. Malthaner
 */
properties_t * feature_factory_t::get_props(const char * key)
{
  return features->get_section(key);
}


feature_factory_t::feature_factory_t()
{
  
}


/**
 * Load a feature by name
 * @author Hj. Malthaner
 */
image_meta_t feature_factory_t::load(const char * name)
{
  image_meta_t meta;

  meta.xoff = 0;
  meta.yoff = 0;
  meta.colorset = 0;
  meta.trans = false;

  properties_t * props = features->get_section(name);

  if(props) {
    const int *ip = props->get_int("img");

    if(ip) {
      meta.tileset = 0;
      meta.img = *ip;
    } else {
      meta.tileset = 2;
      meta.img = 
	world_view_t::get_instance()->load_tile_from_image(props->get_string("img"));
    }
  } else {
    dbg->fatal("feature_factory_t::load()", 
	       "no section found for '%s'", name);
  }

  return meta;
}


/**
 * Create a feature by name
 * @author Hj. Malthaner
 */
feature_t * feature_factory_t::create(const char * name) const
{
  feature_t * feature = new feature_t();

  const properties_t * props = features->get_section(name);

  if(props == 0) {
    dbg->fatal("feature_factory_t::create()", 
	       "no section found for '%s'", name);
  }

  feature->set_props(props);

  return feature;
}
