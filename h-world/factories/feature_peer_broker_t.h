#ifndef feature_peer_broker_t_h
#define feature_peer_broker_t_h

#include "tpl/stringhashtable_tpl.h"
#include "primitives/koord.h"

class feature_peer_creator_t;
class feature_peer_t;

/*
 * Abstract factory for feature peers
 *
 * @see feature_peer_creator_t
 * @author Hj. Malthaner
 */
class feature_peer_broker_t
{
 private:

  static stringhashtable_tpl <feature_peer_creator_t *> table;

 public:


  /**
   * Register a creator at this broker
   * @author Hj. Malthaner
   */
  static void add_creator(feature_peer_creator_t *);


  /**
   * Abstract factory
   * @author Hj. Malthaner
   */
  static feature_peer_t * create(const char *name, koord k, const char *param);

};

#endif // feature_peer_broker_t_h
