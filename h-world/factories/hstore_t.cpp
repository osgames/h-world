/* Copyright by Hansj�rg Malthaner */

#include "hstore_t.h"
#include "model/pprops_t.h"
#include "model/world_t.h"
#include "model/level_t.h"
#include "model/square_t.h"
#include "model/thing_t.h"
#include "model/ground_t.h"
#include "model/feature_t.h"
#include "model/inventory_t.h"
#include "model/feature_door_t.h"
#include "model/feature_stairs_t.h"
#include "model/feature_fountain_t.h"
#include "model/feature_lua_t.h"
#include "model/memory_t.h"
#include "control/square_constraint_t.h"
#include "control/action_move_t.h"
#include "control/action_attack_t.h"
#include "control/action_trade_t.h"
#include "control/action_speak_t.h"
#include "control/action_swap_t.h"
#include "control/action_lua_t.h"
#include "control/action_imove_t.h"
#include "control/user_input_t.h"
#include "control/pick_thing_t.h"
#include "control/attack_t.h"
#include "control/lua_call_t.h"
#include "util/debug_t.h"


#define TALK_DEBUG 0


/**
 * Must be overriden by application specific subclass to
 * instantiate the appropriate object types
 * @param cid class id of the object to create
 * @author Hj. Malthaner
 */
persistent_t * hstore_t::create(perid_t cid)
{
  persistent_t *result = 0;

  switch(cid) {
  case t_world:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> world_t", cid);
    result = world_t::get_instance();
    break;
  case t_level:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> level_t", cid);
    result = new level_t();
    break;
  case t_square:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> sqaure_t", cid);
    result = new square_t();
    break;
  case t_thing:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> thing_t", cid);
    result = new thing_t();
    break;
  case t_ground:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> ground_t", cid);
    result = new ground_t();
    break;
  case t_feature:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> feature_t", cid);
    result = new feature_t();
    break;
    //  case t_square_constraint:
    //    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> square_constraint_t", cid);
    //    result = new square_constraint_t();
    //    break;
  case t_feature_door:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> feature_door_t", cid);
    result = new feature_door_t();
    break;
  case t_feature_stairs:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> feature_stairs_t", cid);
    result = new feature_stairs_t();
    break;
  case t_feature_fountain:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> feature_fountain_t", cid);
    result = new feature_fountain_t();
    break;
  case t_feature_lua:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> feature_lua_t", cid);
    result = new feature_lua_t();
    break;
  case t_action_move:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> action_move_t", cid);
    result = new action_move_t();
    break;
  case t_action_attack:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> action_attack_t", cid);
    result = new action_attack_t();
    break;
  case t_action_trade:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> action_trade_t", cid);
    result = new action_trade_t();
    break;
  case t_user_input:
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> user_input_t", cid);
    result = new user_input_t();
    break;
  case t_pprops:
    result = new pprops_t();
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> pprops_t %p", cid, result);
    break;
  case t_attack:
    result = new attack_t();
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> attack_t %p", cid, result);
    break;
  case t_pick_thing:
    result = new pick_thing_t();
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> pick_thing_t %p", cid, result);
    break;
  case t_lua_call:
    result = new lua_call_t("");
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> lua_call_t %p", cid, result);
    break;
  case t_inventory:
    result = new inventory_t();
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> inventory_t %p", cid, result);
    break;

  case t_action_speak:
    result = new action_speak_t();
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> action_speak_t %p", cid, result);
    break;

  case t_action_swap:
    result = new action_swap_t();
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> action_swap_t %p", cid, result);
    break;
  case t_action_lua:
    result = new action_lua_t();
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> action_lua_t %p", cid, result);
    break;
  case t_action_imove:
    result = new action_imove_t();
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> action_imove_t %p", cid, result);
    break;
  case t_memory:
    result = new memory_t();
    if(TALK_DEBUG) dbg->message("hstore_t::create()", "%d -> memory_t %p", cid, result);
    break;

  default:
    dbg->fatal("hstore_t::create()", "unknown object type %ld !", cid);
    break;
  }


  //  if(result == 0) {
  //    dbg->fatal("hstore_t::create()", "object type %ld could not be created!", cid);
  //  }

  return result;
}
