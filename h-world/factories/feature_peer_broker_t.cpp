#include "feature_peer_broker_t.h"
#include "feature_peer_creator_t.h"

#include "util/debug_t.h"

stringhashtable_tpl <feature_peer_creator_t *> feature_peer_broker_t::table;


/**
 * Register a creator at this broker
 * @author Hj. Malthaner
 */
void feature_peer_broker_t::add_creator(feature_peer_creator_t *c)
{
  if(table.get(c->get_name()) == 0) {
    table.put(c->get_name(), c);

    dbg->message("feature_peer_broker_t::add_creator()",
		 "'%s' -> %p", c->get_name(), c);
  }
}


/**
 * Abstract factory
 * @author Hj. Malthaner
 */
feature_peer_t * feature_peer_broker_t::create(const char *name, 
					       koord k,
					       const char *param)
{
  feature_peer_creator_t *c = table.get(name);

  dbg->message("feature_peer_broker_t::create()", "'%s' -> %p", name, c);

  feature_peer_t *p = 0;

  if(c) {

    p = c->create(k, param);

  } else {
    dbg->fatal("feature_peer_broker_t::create()", 
	       "No creator for feature '%s'!", name);
  }

  return p;
}
