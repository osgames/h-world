#ifndef vdoor_creator_t_h
#define vdoor_creator_t_h

#include "feature_peer_creator_t.h"

class level_t;
class koord;

class vdoor_creator_t : public feature_peer_creator_t
{
 public:

  static void vdoor_creator_t::realize(level_t *level, 
				       const char * open, 
				       const char * closed, 
				       int open_x, int open_y,
				       const char * top, 
				       const char * center, 
				       const char * bottom,
				       const koord &top_pos);

 private:

 public:


  virtual feature_peer_t * create(koord k, const char * parameters);
  virtual const char * get_name();

  vdoor_creator_t();
};

#endif // vdoor_creator_t_h
