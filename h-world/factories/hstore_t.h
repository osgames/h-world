/* Copyright by Hansj�rg Malthaner */


#ifndef HSTORE_T_H
#define HSTORE_T_H

#include "persistence/storage_t.h"


/**
 * H-World specific subclass to instanciate object for the persistent
 * storage system. 
 *
 * @author Hj. Malthaner
 */
class hstore_t : public storage_t
{

 public:

  enum cids 
  {
    t_world,             // 0
    t_level,             // 1
    t_square,            // 2
    t_thing,             // 3
    t_visual,            // 4
    t_ground_visual,     // 5
    t_ground,            // 6
    t_feature,           // 7
    t_environment,       // 8
    t_square_constraint, // 9
    t_feature_door,      // 10
    t_feature_stairs,    // 11
    t_feature_trader,    // 12
    t_feature_fountain,  // 13
    t_actor,             // 14
    t_action_move,       // 15
    t_action_attack,     // 16
    t_action_trade,      // 17
    t_user_input,        // 18
    t_pprops,            // 19
    t_pick_thing, 
    t_attack, 
    t_lua_call,
    t_inventory,
    t_action_speak,
    t_action_swap,
    t_memory,
    t_feature_lua,
    t_action_lua,
    t_action_imove,
  };



  /**
   * Must be overriden by application specific subclass to
   * instantiate the appropriate object types
   * @param cid class id of the object to create
   * @author Hj. Malthaner
   */
  virtual persistent_t * create(perid_t cid);

};

#endif
