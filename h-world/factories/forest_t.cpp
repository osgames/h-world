/* 
 * forest_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include "forest_t.h"
#include "feature_factory_t.h"
#include "model/groundhandle_t.h"
#include "model/square_t.h"
#include "model/level_t.h"
#include "model/ground_t.h"
#include "model/feature_t.h"
#include "model/pprops_t.h"
#include "util/perlin_t.h"
#include "util/rng_t.h"
#include "model/height_level_t.h"


forest_t::forest_t(const area_t &a, const koord &off,
		   int d, int g) : structure_t(a)
{
  offset = off;
  density = d;
  ground = g;
}


/**
 * Realizes this forest in the level
 * @author Hansj�rg Malthaner
 */
void forest_t::realize(level_t *level)
{
  feature_factory_t feature_factory;

  const char * tree_0c = level->get_properties()->get_string("tree[0]");
  const char * tree_1c = level->get_properties()->get_string("tree[1]");
  const char * tree_2c = level->get_properties()->get_string("tree[2]");
  const char * tree_3c = level->get_properties()->get_string("tree[3]");

  // Hajo: tree prototype objects
  feature_t * tree [4];

  tree[0] = feature_factory.create(tree_0c);
  tree[1] = feature_factory.create(tree_1c);
  tree[2] = feature_factory.create(tree_2c);
  tree[3] = feature_factory.create(tree_3c);

  tree[0]->set_blocking(false);

  perlin_t perlin;
  perlin_t p_rand;

  perlin_t x_off;
  perlin_t y_off;

  perlin.set_seed(0);
  p_rand.set_seed(10001);

  x_off.set_seed(4232199);
  y_off.set_seed(9127434);


  double min = 999;
  double max = -999;

  area_iterator_t iter (area);

  while(iter.next()) {
    const koord k = iter.get_current();

    groundhandle_t gr(new ground_t());

    level->at(k)->set_ground(gr);

    const double rand = p_rand.int_noise(k.x + offset.x, k.y + offset.y);

    // printf("%f\n", rand); 


    gr->visual.set_image(ground + (int)((rand + 1.0) * 1.1));
    /*
    gr->visual.shade[0] = 20;
    gr->visual.shade[1] = 20;
    gr->visual.shade[2] = 20;
    gr->visual.shade[3] = 20;
    */

    double f = perlin.noise_2D((double)(k.x+offset.x),
			       (double)(k.y+offset.y), 0.8)*14.0 + density;

    /*
    const int intf = (int)(f + perlin.int_noise((k.x+offset.x),
						(k.y+offset.y)) - 0.5);
    */
    const int intf = (int)f;


    if(f<min) min = f;
    if(f>max) max = f;


    if(intf < -3) {
      gr->visual.set_image(ground + 2);
    }

    const int tree_idx = intf - (int)((rand + 1.0) * 8.0);

    if(tree_idx >= 0) {

      if(tree_idx >= 1) {
	gr->visual.set_image(ground + 3);
	/*
	gr->visual.shade[0] = 16;
	gr->visual.shade[1] = 16;
	gr->visual.shade[2] = 16;
	gr->visual.shade[3] = 16;
	*/
      }

      featurehandle_t feature ( new feature_t( tree[(tree_idx>3) ? 3 : tree_idx] ));

      feature->visual.set_opaque(tree_idx > 2);

      const int xoff = (int)(x_off.int_noise(k.x + offset.x, 
					     k.y + offset.y) * 7.0);
      const int yoff = (int)(y_off.int_noise(k.x + offset.x, 
					     k.y + offset.y) * 3.0);

      feature->visual.set_x_off(xoff);
      feature->visual.set_y_off(yoff);

      level->at(k)->set_feature(feature);
    }
  }

  if(level->get_properties()->get_string("endless") == 0) {
    area_iterator_t iter (area);

    while(iter.next()) {
      const koord k = iter.get_current();
      
      if(k.x == 0 || k.y == 0 || k.x == area.br.x || k.y == area.br.y) {
	featurehandle_t feature ( new feature_t( tree[3] ));
	level->at(k)->set_feature(feature);
      }
    }
  }


  // printf("min=%f max=%f\n", min, max);

  delete tree[0];
  delete tree[1];
  delete tree[2];
  delete tree[3];
}
