/* 
 * fountain_creator_t.cpp
 *
 * Copyright (c) 2003 - 2003 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "fountain_creator_t.h"

#include "model/feature_fountain_t.h"
#include "model/feature_t.h"
#include "model/featurehandle_t.h"
#include "model/square_t.h"
#include "model/level_t.h"

#include "primitives/koord.h"


fountain_creator_t::fountain_creator_t()
{
}


feature_peer_t * fountain_creator_t::create(koord k, const char * parameters)
{
  return new feature_fountain_t();
}


const char * fountain_creator_t::get_name()
{
  return "fountain";
}
