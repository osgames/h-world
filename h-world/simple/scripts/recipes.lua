
----------------------------------------------------------------
-- recipes.lua - SIMPLE MODULE
--
-- H-World recipes initialition script, purpose is to contain all
-- recipes for item creation
--
-- author: ABCGi
-- date:   19-Nov-2004
-- update: 
----------------------------------------------------------------



------------------------------------------------------------------
-- Helper function to add sockets or gems
--
-- author: Hj. Malthaner
-- date:   07-Jul-04
--
-- param item:      type thing_t, the item to gem/socket
-- param sock_cur:  type number,  the number of the gem/socket
-- param image:     type number,  the image overlay number
-- param colorset:  type number,  the image colorset
-- return:          nothing
------------------------------------------------------------------

function add_socket_or_gem(item, sock_cur, image, colorset)

	-- actual number of sockets available
	local sock_act = thing_get_value(item, "sock_act") or 0

	-- max number of socket positions
	local width  = thing_get_value(item, "width") or 1
	local height = thing_get_value(item, "height") or 1

	-- we want to center sockets

	local sock_width = thing_get_value(item, "sock_width") or 1
	local sock_height = ceil(sock_act / sock_width) 

	if(sock_act < sock_width) then
		sock_width = sock_act
	end

	local col = mod(sock_cur, sock_width)
	local row = floor(sock_cur / sock_width)

	local xoff = (width - sock_width) * 8
	local yoff = (height - sock_height) * 8

	-- _ALERT("lua: add_socket_or_gem() colorset=" .. colorset .. "\n")

	-- parameters:
	-- item, map_or_inventory, index, image, colorset, xoff, yoff, transparent, redraw

	thing_visual_set_image(item, 1, 1+sock_cur, image, colorset, 
			       xoff+col*16, yoff+row*16, 1, 1);

end


------------------------------------------------------------------
-- Helper function to determine the number of magic attribues
--
-- author: Hj. Malthaner
-- date:   10-Jul-04
--
-- param thing:      type thing_t, the item to check
-- return:           the number of magic attributes
------------------------------------------------------------------

function thing_count_attributes(thing)

	local n = 0
	local effect = nil

	repeat
		local att_n = "att-" .. n

		effect = thing_get_value(thing, att_n .. ".effect")

		if(effect) then
			n = n+1 
		end
	until (effect == nil)

	return n
end


------------------------------------------------------------------
-- Helper function to add a magic attribute to a thing
--
-- author: Hj. Malthaner
-- date:   08-Oct-04
--
-- param thing:      type thing_t, the thing to modify
-- param effect:     type string, name of the effect
-- param value:      type number, strength of the effect
-- param target:     type string, "self" or "owner"
-- return:           nothing
------------------------------------------------------------------

function thing_add_attribute(thing, effect, value, target)

	local n = thing_count_attributes(thing)

	thing_set_value(thing, "att-" .. n .. ".effect", effect)
	thing_set_value(thing, "att-" .. n .. ".value",  value)
	thing_set_value(thing, "att-" .. n .. ".target", target)
end


------------------------------------------------------------------
-- Helper function to set the attack effect for a thing
--
-- author: Hj. Malthaner
-- date:   08-Oct-04
--
-- param thing:      type thing_t, the thing to modify
-- param effect:     type string, name of the effect
-- param dice:       type number, number of dice to roll
-- param sides:      type number, sides of each dice
-- return:           nothing
------------------------------------------------------------------

function thing_set_attack_effect(thing, effect, dice, sides)

	thing_set_value(item, "attack.effect", effect)
	thing_set_value(item, "attack.effect.dice", dice)
	thing_set_value(item, "attack.effect.sides", sides)
end


------------------------------------------------------------------
-- Items are combined by dropping one onto the other
--
-- author: Hj. Malthaner
-- date:   27-Dec-2002
-- update: 02-May-2004 - ignore magic item prefixes
-- update: 19-May-2004 - added inventory parameter
-- update: 19-Jul-2004: Hj. Malthaner - fixed a bug in colorset handling
--                                      of gemmed items
--
-- param user:      type thing_t, the user
-- param inventory: type inventory_t, the inventory that contains the item
-- param item:      type thing_t, the item
-- param drop:      type thing_t, the thing that is dropped onto the item
-- return:          1 on success, 0 on failure
------------------------------------------------------------------

function on_drop_item_on_item(user, inventory, item, drop)
	_ALERT("lua: on_drop_item_on_item() called\n")

	-- Recipes work, if the player has identified the items
        -- or not, thus we pass 1 for 'memorized' to get the
        -- real identifier always

	local id1 = calculate_plain_ident_string(item, 1)
	local id2 = calculate_plain_ident_string(drop, 1)

	_ALERT("lua: id1=" .. id1 .. " id2=" .. id2 .."\n")

	-- recipes based on a healing potion

	if id1 == "healing potion" then

		-- ATM everything transform it to poison

		if id2 == "rat tail" or id2 == "bitter leaves" then

			local potion = thing_create("potion_of_poison", 0, 0)
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			return 1
		end
	end


	-- recipes based on a weak healing potion

	if id1 == "weak healing potion" then

		if id2 == "rat tail" then

			local potion = thing_create("healing_potion", 0, 0)
			thing_set_value(potion, "is_unknown", "false")
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			if(thing_recall(user, "recipe_healing_potion", 0) == nil) then
				thing_memorize(user, "recipe_healing_potion", "")
				thing_memorize(user, "event", "<h1>Discovery:</h1>Dropping a rat tail into a weak healing potion gives a healing potion.")


			end

			return 1
		end

		if id2 == "bitter leaves" then

			local potion = thing_create("potion_of_poison", 0, 0)
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			return 1
		end
	end


	-- recipes based on a bottle of water

	if id1 == "bottle of water" then

		if id2 == "bitter leaves" then

			local potion = thing_create("weak_healing_potion", 0, 0)
			thing_set_value(potion, "is_unknown", "false")
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			if(thing_recall(user, "recipe_weak_healing_potion", 0) == nil) then
				thing_memorize(user, "recipe_weak_healing_potion", "")
				thing_memorize(user, "event", "<h1>Discovery:</h1>Dropping bitter leaves into a bottle of water gives a weak healing potion.")


			end


			return 1
		end

		if id2 == "rat tail" then

			local potion = thing_create("poisonous_potion", 0, 0)
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			return 1
		end
	end


	if((thing_get_value(drop, "type") or "") == "gem") then

		local sock_act = thing_get_value(item, "sock_act") or 0
		local sock_cur = thing_get_value(item, "sock_cur") or 0

		if (sock_act > 0 and sock_cur < sock_act) then

			-- get image meta data
			local image, xoff, yoff, colorset, trans
			image, xoff, yoff, colorset, trans = thing_visual_get_image(drop, 1, 0)

			add_socket_or_gem(item, sock_cur, sym_gem_inv, colorset);
			thing_set_value(item, "sock_cur", sock_cur + 1)

			local subtype = thing_get_value(drop, "subtype") or ""

			if(subtype == "blood") then
				thing_add_attribute(item, "HPmax", 10, "owner")
			end

			if(subtype == "swift") then
				thing_add_attribute(item, "hinderance", -2, "owner")
			end

			if(subtype == "stamina") then
				thing_add_attribute(item, "stamina", 10, "owner")
			end

			if(subtype == "light") then
				thing_add_attribute(item, "light_rad", 2, "owner")
			end

			if(subtype == "penetration") then
				thing_add_attribute(item, "penetration", 5, "self")
			end

			if(subtype == "block") then
				thing_add_attribute(item, "block", 8, "self")
			end

			if(subtype == "poison") then
				thing_set_attack_effect(thing, "effect.poison", 2, 3)
			end

			return 1
		end
	end

	return 0
end
