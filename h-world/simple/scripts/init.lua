----------------------------------------------------------------
-- init.lua - SIMPLE MODULE
--
-- H-World script initialition script, purpose is to load
-- all required scripts
--
-- Author: ABCGi
-- Created: 19-Nov-2004
-- Update: 
----------------------------------------------------------------

-- global values

-- skin definitions, used by engine -> order is fixed!

load_tile_from_image(MODULE_PATH .. "data/skin_menuback.png")
load_tile_from_image(MODULE_PATH .. "data/skin_inveback.png")
load_tile_from_image(MODULE_PATH .. "data/skin_hoborder.png")
load_tile_from_image(MODULE_PATH .. "data/skin_veborder.png")

-- global overlay images, used by scripts

sym_hurt   = load_tile_from_image(MODULE_PATH .. "data/sym_hurt.png")
sym_hunger = load_tile_from_image(MODULE_PATH .. "data/sym_hunger.png")
sym_thirst = load_tile_from_image(MODULE_PATH .. "data/sym_thirst.png")
sym_poison = load_tile_from_image(MODULE_PATH .. "data/sym_poison.png")
sym_frozen = load_tile_from_image(MODULE_PATH .. "data/sym_frozen.png")
sym_sleep  = load_tile_from_image(MODULE_PATH .. "data/sym_sleep.png")


sym_go_fountain = load_tile_from_image(MODULE_PATH .. "data/sym_go_fountain.png")
sym_go_person   = load_tile_from_image(MODULE_PATH .. "data/sym_go_person.png")
sym_go_sleep    = load_tile_from_image(MODULE_PATH .. "data/sym_go_sleep.png")
sym_go_wash     = load_tile_from_image(MODULE_PATH .. "data/sym_go_wash.png")
sym_go_work     = load_tile_from_image(MODULE_PATH .. "data/sym_go_work.png")


sym_gem_inv = load_tile_from_image(MODULE_PATH .. "data/custom/gem_inv.png")
sym_sock_inv = load_tile_from_image(MODULE_PATH .. "data/custom/socket.png")

-- read all functions

local fail = nil

fail = dofile(MODULE_PATH .. "scripts/core.lua")
if (fail == nil) then error(nil) end

fail = dofile(MODULE_PATH .. "scripts/usages.lua")
if (fail == nil) then error(nil) end

fail = dofile(MODULE_PATH .. "scripts/attacks.lua")
if (fail == nil) then error(nil) end

fail = dofile(MODULE_PATH .. "scripts/recipes.lua")
if (fail == nil) then error(nil) end

fail = dofile(MODULE_PATH .. "scripts/effects.lua")
if (fail == nil) then error(nil) end

fail = dofile(MODULE_PATH .. "scripts/commands.lua")
if (fail == nil) then error(nil) end

fail = dofile(MODULE_PATH .. "scripts/traps.lua")
if (fail == nil) then error(nil) end

fail = dofile(MODULE_PATH .. "scripts/dialogs.lua")
if (fail == nil) then error(nil) end

fail = dofile(MODULE_PATH .. "scripts/ai.lua")
if (fail == nil) then error(nil) end

_ALERT("Lua: init.lua done\n")
 