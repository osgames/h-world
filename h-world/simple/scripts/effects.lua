----------------------------------------------------------------
-- effects.lua - SIMPLE MODULE
--
-- H-World per-round effects  initialition script, purpose is to
-- contain effects that might happen per turn
--
-- author: ABCGi
-- date:   19-Nov-2004
----------------------------------------------------------------



------------------------------------------------------------------
-- called every turn, with the duration of the last executed action
--
-- author: Hj. Malthaner
-- date:   23-Apr-2003
-- update: 18-Apr-2004
-- update: 07-Aug-2004 - Hj. Malthaner: negative stamina now lowers HP
--
-- param user:     type thing_t, the player character
-- param duration: type number, the duration of the last turn 
-- return:         type number, dummy value
------------------------------------------------------------------


function per_turn_effects(user, duration)

	-- _ALERT("per_turn_effects(): thing " .. thing_get_ident(user) .. " duration=" .. duration .. "\n")


	local is_player = thing_get_value(user, "is_player")


	--
	-- Treat poison
	--

  	local poison = thing_get_value(user, "effect.poison") or 0

  	if poison > 0 then
    		_ALERT("per_turn_effects() " .. "poison=" .. poison .. "\n")


    		thing_set_value(user, 
                	        "HPcurr", 
                    		(thing_get_value(user, "HPcurr") or 0) - duration/4);

    		thing_set_value(user, "effect.poison", poison-1);

	end


	--
	-- Treat paralysm
	--

  	local para = thing_get_value(user, "effect.paralyzed") or 0

  	if para > 0 then
    		_ALERT("per_turn_effects() " .. "para=" .. para .. "\n")

    		thing_set_value(user, "effect.paralyzed", para-1);
	end


	--
	-- Treat blindness
	--

  	local blind = thing_get_value(user, "effect.blind") or 0

  	if blind > 0 then
    		_ALERT("per_turn_effects() " .. "blind=" .. blind .. "\n")

    		thing_set_value(user, "effect.blind", blind-1);

		if(blind < 2 and thing_get_value(user, "is_player")) then
			translate("You can see again.")
		end
	end


	--
	-- Treat regeneration
	--

	local hp = thing_get_value(user, "HPcurr")
	local maxhp = thing_get_value(user, "HPmax")
	
	local stamina = thing_get_value(user, "stamina") or 0
	

	-- restore hit point up to max

	if stamina > 0 and 
	   rng_get_int(350 - stamina) <= duration and hp < maxhp then

		hp = hp+1

		-- set values
		thing_set_value(user, "HPcurr", hp)
	end


	if stamina < 0 and 
           rng_get_int(200 + stamina) <= duration and 
           hp > 0 then

		hp = hp-1

		-- set values
		thing_set_value(user, "HPcurr", hp)
	end

	--
	-- Treat hunger and thirst, ATM player only because
	-- player can't feed followers
	--
	
	local hunger = 0
	local thirst = 0
	
	if is_player then

		hunger = thing_get_value(user, "hunger") or 0
	
		if rng_get_int(800) <= duration then
			hunger = hunger +1

			if hunger > 100 then
				hunger = 100
				hp = hp -1
				-- hunger weakens
				thing_set_value(user, "HPcurr", hp)
			end

			thing_set_value(user, "hunger", hunger)
		end


		thirst = thing_get_value(user, "thirst") or 0
	
		if rng_get_int(400) <= duration then
			thirst = thirst +1

			if thirst > 100 then
				thirst = 100
				hp = hp -1
				-- thirst weakens
				thing_set_value(user, "HPcurr", hp)
			end

			thing_set_value(user, "thirst", thirst)
		end
	end

	-- 
	-- treat sleep/wake
	--

	local sleep = thing_get_value(user, "sleep")
	if(sleep) then
		if(sleep > 0) then
			if(rng_get_int(100) <= duration) then
				sleep = sleep - 1
			end
		end

		if(sleep < 0) then
			if(rng_get_int(100) <= duration) then
				sleep = sleep + 1
			end
		end


		thing_set_value(user, "sleep", sleep)	
	end


	--
	-- AI state
	--

	local ai_state = thing_get_value(user, "AI.state") or "nothing"
	local search_room = nil

	if(ai_state == "follow_path") then
		search_room = thing_get_value(user, "AI.room_type") or 0
	end
	

	--
	-- status icons
	--

	-- non-existant icon is treated as invisible icon
	local status_icon = thing_get_value(user, "status_icon") or 9999


	-- clear old icon, 0 = delayed redraw

	thing_visual_remove_image(user, status_icon, 0);
	status_icon = 9999
	
	-- cascade, from least important to most important


	-- show AI state
	if(search_room) then
		if(search_room == 1) then
			status_icon = sym_go_fountain
		end
		if(search_room == 2) then
			status_icon = sym_go_sleep
		end
		if(search_room == 3) then
			status_icon = sym_go_wash
		end
		if(search_room == 6) then
			status_icon = sym_go_work
		end
		if(search_room == 7) then
			status_icon = sym_go_person
		end
	end

	if sleep ~= nil and sleep > 0 then
		status_icon = sym_sleep
	end

	if hp < 2 or hp < maxhp/5 then
		status_icon = sym_hurt
	end

	if hunger > 80 then
		status_icon = sym_hunger
	end

	if thirst > 80 then
		status_icon = sym_thirst
	end

	if poison > 0 then
		status_icon = sym_poison
	end

	if para > 0 then
		status_icon = sym_frozen
	end

	-- set new icon, 0 = delayed redraw

	thing_visual_add_image(user, status_icon, 0);


	-- remember value

	thing_set_value(user, "status_icon", status_icon)


	return 0
end
