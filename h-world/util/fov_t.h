/* 
 * fov_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef FOV_T_H
#define FOV_T_H

#include "primitives/matrix.h"

class koord;
class fov_source_t;
class fov_destination_t;
class shadow_list;

/**
 * Generic field of view calculator. Uses fov_source_t and fov_destination_t
 * objects to communicate with the application.
 * @author Hj. Malthaner
 */
class fov_t {

 private:

  matrix transform;

  fov_source_t *source;
  fov_destination_t *destination;

  /** @link dependency 
   * @stereotype use*/
  /*#  shadow_list lnkshadow_list; */

  bool fov_pos(const shadow_list &former_shadows, 
	       shadow_list &shadows,
	       const koord center, const koord pos);


 public:

  fov_t();

  void set_source(fov_source_t * source) {this->source = source;};
  void set_destination(fov_destination_t * destination) {this->destination = destination;};

  void calc_field_of_view(const koord center, const int distance);
};



#endif //FOV_T_H
