/* Copyright by Hj. Malthaner */

#ifndef PERLIN_T_H
#define PERLIN_T_H


// this is enough for a argument range of 0..100 in x and y 
// of noise_2D
#define PE_CACHE 101

/**
 * Perlin noise (2D) implementation
 * @author Hj. Malthaner
 */
class perlin_t {
private:    

  unsigned long noise_seed;


  /**
   * Cache for smoothed noise results
   */
  double cache [PE_CACHE][PE_CACHE];


  /**
   * Cross-interpolated noise for int coordinates
   * @author Hj. Malthaner
   */
  double smoothed_noise(const int x, const int y) const ;

  double smoothed_noise_cached(const int x, const int y);

  /**
   * Cross-interpolated noise for double coordinates
   * @author Hj. Malthaner
   */
  double interpolated_noise(const double x, const double y);

public:


  perlin_t();



  /**
   * Sets noise seed.
   * @author Hj. Malthaner
   */
  void set_seed(int seed);


  /**
   * Noise function
   * @author Hj. Malthaner
   */
  double int_noise(const long x, const long y) const;


  /**
   * Perlin noise over 6 oktaves
   *
   * @param x x-Koordinate des Punktes
   * @param y y-Koordinate des Punktes
   * @param p   Persistenz
   * @author Hj. Malthaner
   */
  double noise_2D(const double x, const double y, const double p);

};


#endif
