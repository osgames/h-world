/* Copyright by Hj. Malthaner */

#ifndef FOV_DESTINATION_T_H
#define FOV_DESTINATION_T_H
class koord;

/** @interface */
class fov_destination_t {
public:    

    virtual void set_viewable(koord pos) = 0;
};
#endif //FOV_DESTINATION_T_H
