/* Copyright by Hj. Malthaner */

#include <string.h>

#include "tpl/stringhashtable_tpl.h"
#include "tpl/slist_tpl.h"

#include "ifc/property_listener_t.h"

#include "property_t.h"
#include "properties_t.h"

#include "config_file_t.h"


properties_t::properties_t()
{
  properties = new stringhashtable_tpl <property_t *>;
  listeners = new slist_tpl <property_listener_t *>;
}


properties_t::~properties_t()
{
  const slist_tpl <property_t *> * plist = properties->get_elements();

  slist_iterator_tpl <property_t *> iter (plist);

  while( iter.next() ) {
    delete iter.get_current();
  }
  
  
  delete plist;
  plist = 0;

  delete properties;
  properties = 0;

  delete listeners;
  listeners = 0;
}


/**
 * Gets a list containing all keys. The list is allocated on the heap
 * @author Hj. Malthaner
 */
const slist_tpl<const char *> * properties_t::get_keys() const 
{
  return properties->get_keys();
}


/**
 * Adds a property listener
 * @author Hansj�rg Malthaner
 */     
void properties_t::add_listener(property_listener_t * l) {
  listeners->insert(l);
}


/**
 * Copies all entries into the other object
 * @author Hj. Malthaner
 */
void properties_t::copy_into(properties_t *other) const
{
  const slist_tpl<const char*> *keys = get_keys();

  slist_iterator_tpl<const char *> iter (keys);

  while(iter.next()) {
    const char *key = iter.get_current();
    
    if(get_int(key)) {
      other->set(key, *get_int(key));
    } else {
      other->set(key, get_string(key));
    }
  }

  delete keys;
}


/**
 * @return pointer to requested property or NULL if property does not exist
 * @author Hansj�rg Malthaner
 */
const property_t * properties_t::get(const char * name) const 
{
  return properties->get(name);
}


/**
 * gets property string value.
 * @return NULL if property is non-existent or if it has no string value
 * @author Hansj�rg Malthaner
 */
const char * properties_t::get_string(const char * name) const
{
  const char *result = NULL;
  const property_t *prop = properties->get(name);

  if(prop) {
    result = prop->get_string();
  }

  return result;
}


/**
 * gets property string value.
 * @return NULL if property is non-existent or if it has no string value
 * @author Hansj�rg Malthaner
 */
const char * properties_t::get_string(const char * name, const char * def) const
{
  const char * result = def;
  const property_t *prop = properties->get(name);

  if(prop && prop->get_string()) {
    result = prop->get_string();
  }

  return result;
}


/**
 * gets property int value.
 * @return NULL if property is non-existent or if it has no int value
 * @author Hansj�rg Malthaner
 */
const int * properties_t::get_int(const char * name) const
{
  const int *result = NULL;
  const property_t *prop = properties->get(name);

  if(prop) {
    result = prop->get_int();
  }

  return result;
}


/**
 * gets property int value.
 * @return value or default if property isn't set
 * @author Hansj�rg Malthaner
 */
const int properties_t::get_int(const char * name, int def) const
{
  int result = def;
  const property_t *prop = properties->get(name);

  if(prop && prop->get_int()) {
    result = *prop->get_int();
  }

  return result;
}


void properties_t::set(const char * name, int value)
{
  property_t * prop = properties->get(name);

  if(prop) {
    // already there
    prop->set_int(value);

  } else {
    // new entry

    prop = new property_t(name, value);

    properties->put(prop->get_key(), prop);
  }
  
  call_listeners(prop);
}


void properties_t::set(const char * name, const char * value)
{
  property_t * prop = properties->get(name);

  if(prop) {
    // already there
    prop->set_string(value);

  } else {
    // new entry
    prop = new property_t(name, value);
    properties->put(prop->get_key(), prop);
  
  }

  call_listeners(prop);
}


bool properties_t::load(const char *filename)
{
  config_file_t file;
  bool ok = false;

  if( file.open(filename) ) {
    printf("reading properties file '%s'\n", filename);
    load_from_section(&file);
    ok = true;
  } else {
    printf("Cannot open file '%s'\n", filename);
  }

  return ok;
}


/**
 * Loads properties from a section of a sectioned file.  
 * @author Hansj�rg Malthaner
 */
void properties_t::load_from_section(config_file_t * file)
{
  const char *include = "include";
  char line [4096];
  char key  [256];
  // char sval [256];
  int  ival;

  while(! file->eof() ) {
    file->read_line(line, 4090);
    line[4091] = 0;
    
    if(! file->eof() ) {
      if(strncmp(line, "end section", 11) == 0) {
	break;
      }

      if(strncmp(include, line, strlen(include)) == 0) {
	char name [128];
         
	if(sscanf(line, "include %s", name) == 1) {
	  load( name );
	}
      } else { 
	int n = sscanf(line, "%s = %d", key, &ival);
      
	if(n == 2) {
	  set(key, ival);
	
	} else {
	  n = sscanf(line, "%s", key);
	  char *p = strchr(line, '=');

	  if(p) {
	    set(key, p+2);	  
	  } 
	  else {
	    // property without value
	    set(key, (const char *)NULL);
	  }         
	}  
      }
    }
  } // while
}


/**
 * Calls all listeners
 * @author Hansj�rg Malthaner
 */
void properties_t::call_listeners(property_t *prop)
{
  slist_iterator_tpl <property_listener_t *> iter ( listeners );
  
  while(iter.next()) {
    iter.get_current()->update(prop);
  }
}
