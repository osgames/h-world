#include <string.h>
#include <stdlib.h>

#include "property_t.h"  
#include "debug_t.h"          


/**
 * @param key the name (key) of this property, must not be 0
 * @author Hj. Malthaner
 */
property_t::property_t(const char* key, int value)
{
  assert(key != 0);
  this->key = strdup(key);

  int_value = new int(value);
  char_value = 0;


  // dbg->message("property_t::property_t", "key=%s int_value=%d", 
  //	       this->key, *int_value); 
}


/**
 * @param key the name (key) of this property, must not be 0
 * @author Hj. Malthaner
 */
property_t::property_t(const char *key, const char * value)
{
  assert(key != 0);
  this->key = strdup(key);
  
  if(value) {
    char_value = strdup(value);
  } else {
    char_value = 0;
  }
  
  int_value = 0;
  
  // dbg->message("property_t::property_t", "key=%s char_value='%s'", this->key, char_value); 
}


property_t::~property_t()
{
    free((void *)key);
    key = 0;

    free((void *)char_value);
    char_value = 0;

    delete(int_value);
    int_value = 0;
}


void property_t::set_int(int value)
{
  if(int_value) {
    // reuse memory
    *int_value = value;
  } else {
    delete int_value;
    free((void *)char_value);

    int_value = new int(value);
    char_value = 0;
  }
}


void property_t::set_string(const char *value)
{
  delete int_value;
  free((void *)char_value);

  int_value = 0;
  if(value) {
    char_value = strdup(value);
  }
}
