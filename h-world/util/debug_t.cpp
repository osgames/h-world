/* Copyright by Hj. Malthaner */

#include "debug_t.h"


debug_t *dbg = NULL;


debug_t::debug_t(const char *logname, 
		 enum log_t::log_level l,
		 bool force_flush) : log_t(logname,
					   l, 
					   force_flush) 
{
}


void debug_t::leave(const char *name) 
{
}


void debug_t::enter(const char *name)
{
}
