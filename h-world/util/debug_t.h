/* Copyright by Hj. Malthaner */

#ifndef DEBUG_T_H
#define DEBUG_T_H


#include "log_t.h"
#include <assert.h>

class debug_t : public log_t {

  const char *stack[100];
    
public:    

  debug_t(const char *logname, enum log_t::log_level l, bool force_flush);

  void leave(const char *name);

  void enter(const char *name);
};

                
extern debug_t *dbg;

#define ENTER() dbg->enter(__PRETTY_FUNCTION__);
#define LEAVE() dbg->leave(__PRETTY_FUNCTION__);

#endif //DEBUG_T_H
