/* 
 * fov_t.cpp
 *
 * Copyright (c) 2001 - 2002 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include <math.h>

#include "primitives/koord.h"
#include "fov_t.h"
#include "fov_source_t.h"
#include "fov_destination_t.h"
#include "interval_t.h"
#include "tpl/slist_tpl.h"


//  Without table

static inline double angle(int x1, int y1, int x2, int y2)
{
    const double xd = x2-x1;
    const double yd = y2-y1;

    // return atan2(yd, xd);

    return yd/xd;
} 


/*
static double atan_tab[200][200];
static bool need_atan_tab = true;


static void init_atan_tab()
{
  for(int y=0; y<200; y++) {
    for(int x=0; x<200; x++) {
      atan_tab[y][x] = atan2(y-100, x-100);
    }
  }
}
*/

/**
 * to get max performance wee use a lookup table for atan2
 */
/*
static inline double angle(const int x1, const int y1, const int x2, const int y2)
{
  const int xd = x2-x1;
  const int yd = y2-y1;

  return atan_tab[yd+100][xd+100];
}
*/

class shadow_list
{    
private:

    slist_tpl <interval_t> list;


public:


    bool is_fully_shadowed(const double low, const double high) const {
	slist_iterator_tpl <interval_t> iter (list);

	while(iter.next()) {
	    const interval_t & i = iter.get_current();
	    if(i.low <= low && i.high >= high) {
		return true;
	    }
	}            

	return false;
    }


    bool is_partially_shadowed(const double low, const double high) const {
	slist_iterator_tpl<interval_t> iter (list);

	while(iter.next()) {
	    const interval_t &i = iter.get_current();
	    if((i.low < low && i.high > low) ||
	       (i.low < high && i.high > high)) {
		return true;
	    }
	}            

	return false;
    }


    void merge(const double low, const double high) {
	if(list.is_empty()) {
	    list.insert( interval_t(low, high));
	} else {
	    const double eps = 0.01;
	    slist_iterator_tpl<interval_t> iter (list);
                               
	    // try to merge first
	    bool new_shadow = true;
	    while(iter.next()) {               
		interval_t &i = iter.access_current();
		if((low <= i.low && high >= i.low-eps) || 
                   (low <= i.high+eps && high >= i.high)) {
                 
		    if(low < i.low) {
			i.low = low; 
		    }

		    if(high > i.high) {
			i.high = high; 
		    }

		    new_shadow = false;
		}
	    }
                                 
	    if(new_shadow) {
	      list.insert(interval_t(low, high));
	    }
	}
    }


    void deep_copy(const shadow_list &src) {
	list.clear();

	slist_iterator_tpl<interval_t> iter (src.list);

	while(iter.next()) {
	    list.append(iter.get_current());
	}
    }


    void dump() {
	slist_iterator_tpl<interval_t> iter (list);

	while(iter.next()) {
	    interval_t i = iter.get_current();

	    printf("Interval: %f..%f\n", i.low, i.high);
	}
    }

    void clear() {
	list.clear();
    }

};


fov_t::fov_t()
{
  source = NULL;
  destination = NULL;

  /*
  if(need_atan_tab) {
    init_atan_tab();
  }
  */
}


bool fov_t::fov_pos(const shadow_list &former_shadows, 
                    shadow_list &shadows,
                    const koord center, const koord pos)
{          
  const koord k = center+pos;

  const double min_w = angle(center.x*2+1, center.y*2+1, k.x*2, k.y*2);
  const double max_w = angle(center.x*2+1, center.y*2+1, k.x*2+2, k.y*2+2);

  bool visible;
        
  // if shadowed by colums shadow list we can't see this square

  if(former_shadows.is_fully_shadowed(min_w, max_w)) {
    
    // we can't see this grid, because other grids already 
    // blocked the line of sight
    
    visible = false;

    // merge new shadowed area into shadows list
    // only needed of check was for partial shadow!
    // shadows.merge(min_w, max_w);

  } else {
    // this is a viewable grid
    visible = true;

    // is this grid a line of sight blocker ?
    if(source->is_blocking((transform*pos)+center)) {
      
      // merge new shadowed area into shadows list
      shadows.merge(min_w, max_w);
    }
  } // if(shaded ...
  
  return visible;
}



void fov_t::calc_field_of_view(const koord center, const int distance)
{
  int x_off, y_off;
  
  shadow_list shadows;
  shadow_list former_shadows;
  
  static const matrix trans [8] = 
  {
    matrix(1,0,0,1),
    matrix(0,-1,-1,0),
    matrix(0,1,-1,0),
    matrix(-1,0,0,1),
    matrix(-1,0,0,-1),
    matrix(0,1,1,0),
    matrix(0,-1,1,0),
    matrix(1,0,0,-1),
      
      };

  for(int octant=0; octant < 8; octant ++) {
    transform = trans[octant];

    former_shadows.clear();
    shadows.clear();


    x_off = 1;


    while(x_off < distance) {
      koord pos (x_off-1, 0);


      // save old shadows, we need last columns shadows while building 
      // new shadows
      // we modify angles so we need a real copy

      former_shadows.deep_copy(shadows); 

      bool visible_row = false;
      y_off = 0;
      while(y_off < x_off) {
	bool visible_pos = fov_pos(former_shadows, shadows, 
				   center, pos);
                               
	if(visible_pos) {
	  visible_row |= visible_pos;
	  destination->set_viewable((transform * pos)+center);
	}

	pos.y --;      
	y_off ++;
      }        

      if(!visible_row) {
	break;
      }

      x_off ++;
    }
  } // for
}

