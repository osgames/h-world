/* Copyright by Hj. Malthaner */

#ifndef INTERVAL_T_H
#define INTERVAL_T_H


/**
 * Intervals (ranges) in the mathematical sense. 
 * No operations are defined yet.
 * @author Hj. Malthaner
 */
class interval_t {
 public:
  double low;
  double high;
    
  interval_t() {
    high = low = 0.0;
  };
  
  interval_t(double l, double h) {
    low = l;
    high = h;
  };

};



#endif //INTERVAL_T_H
