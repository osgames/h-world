#include "primitives/koord.h"


/**
 * Useful to calculate 4-neighbourhood
 * @author Hj. Malthaner
 * @date 10-Mar-2002
 */
extern const koord nsow[4];


/**
 * Useful to calculate 8-neighbourhood
 * Starting north (0, -1) going clockwise
 * @author Hj. Malthaner
 * @date 03-Sep-2003
 */
extern const koord neighbour_8[8];


/**
 * Keypad directions
 * @author Hj. Malthaner
 * @date 19-Dec-2003
 */
extern const koord * directions;


extern const koord directions_2d[9];
extern const koord directions_iso[9];

