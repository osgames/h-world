#include <string.h>
#include "string_helper.h"


/**
 * Removes newlinbes from end of string. Modifies the
 * argument.
 * @author Hj. Malthaner
 */
void strip_newline(char * str)
{
  int i = strlen(str) - 1;

  while(i >= 0 && str[i] <= 13) {
    str[i--] = '\0';
  }
}


/**
 * C++ conformant replacement of strdup().
 * uses new char[] to allocate the memory
 * @author Hj. Malthaner
 */
char * str_duplicate(const char * str)
{
  const int len = strlen(str) + 1;
  char * result = new char [len];

  strcpy(result, str);
  
  return result;
}

