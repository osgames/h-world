/* 
 * sections_t.cpp
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#include <stdio.h>
#include <string.h>

#include "sections_t.h"
#include "properties_t.h"
#include "config_file_t.h"
#include "debug_t.h"

#include "tpl/stringhashtable_tpl.h"


sections_t::sections_t()
{
  sections = new stringhashtable_tpl <properties_t *>;
}


sections_t::~sections_t()
{
  delete sections;
  sections = 0;
}


bool sections_t::load(const char *filename)
{
   bool ok = false;
   config_file_t file;
   
   if(file.open(filename)) {
      const char *delimiter = "begin section";
      const char *include = "include";
      char buf[256];
      
      while(!file.eof()) {
            
         file.read_line(buf, 250);

         if(!file.eof()) {
            if(strncmp(include, buf, strlen(include)) == 0) {
               char name [128];
         
               if(sscanf(buf, "include %s", name) == 1) {
		 load( name );
	       }
	    } 
            else if(strncmp(delimiter, buf, strlen(delimiter)) == 0) {
               char name [128];
	       char parent [128];
	       char tmp[256];

               if(sscanf(buf, "begin section %s", name) == 1) {
		 // dbg->message("sections_t::load()", "Reading section '%s'", name);
		 properties_t *props = new properties_t();

		 // treat inheritance
		 sprintf(tmp, "begin section %s extends %%s", name);

		 if(sscanf(buf, tmp, parent) == 1) {
		   properties_t * p = sections->get(parent);
		   if(p) {
		     dbg->debug("sections_t::load()", 
				"section %s extends section %s successfully",
				name, parent);   
		     p->copy_into(props);
		   } else {
		     dbg->fatal("sections_t::load()", 
				"extended section '%s' not found", parent);   
		   }

		 }

		 props->load_from_section(&file);            

		 if(sections->get(name)) {
		   dbg->fatal("sections_t::load()", 
			      "Double section '%s'", name);   
		 } else {
		   sections->put(strdup(name), props);
		 }

               } else {
                  dbg->error("sections_t::load()", "Can't read section name!");   
                  return false;
               }
            } else {
               dbg->error("sections_t::load()", "No section begin, got '%s' instead", buf);   
               return false;
            }
         }
      } // while
      file.close();
 
      ok = true;
   } else {
     dbg->error("sections_t::load()", "Can't open '%s'", filename);   
   }
   return ok;
}


properties_t * sections_t::get_section(const char *name)
{
   return sections->get(name);   
}
