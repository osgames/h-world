/* Copyright by Hj. Malthaner */

#ifndef PROPERTIES_T_H
#define PROPERTIES_T_H

class property_listener_t;
class property_t;
class config_file_t;

template <class T> class stringhashtable_tpl;
template <class T> class slist_tpl;


/**
 * Sets of properties (name-value pairs).
 * @author Hansj�rg Malthaner 
 */
class properties_t {
private:

    stringhashtable_tpl <property_t *> * properties;


    /**
     * If this properties are changed, we need to inform the listeners
     * @author Hansj�rg Malthaner
     */
    slist_tpl <property_listener_t *> * listeners;


    /**
     * Calls all listeners
     * @author Hansj�rg Malthaner
     */
    void call_listeners(property_t *prop);

public:    

    properties_t();
    ~properties_t();


    /**
     * Gets a list containing all keys. The list is allocated on the heap
     * @author Hj. Malthaner
     */
    const slist_tpl<const char *> * get_keys() const;


    /**
     * Adds a property listener
     * @author Hansj�rg Malthaner
     */     
    void add_listener(property_listener_t * l);
    

    /**
     * Copies all entries into the other object
     * @author Hj. Malthaner
     */
    void copy_into(properties_t *other) const;


    /**
     * @return pointer to requested property or NULL if property does not exist
     * @author Hansj�rg Malthaner
     */
    const property_t * get(const char * name) const;


    /**
     * gets property string value.
     * @return NULL if property is non-existent or if it has no string value
     * @author Hansj�rg Malthaner
     */
    const char * get_string(const char * name) const;


    /**
     * gets property string value.
     * @return NULL if property is non-existent or if it has no string value
     * @author Hansj�rg Malthaner
     */
    const char * get_string(const char * name, const char * def) const;


    /**
     * gets property int value.
     * @return NULL if property is non-existent or if it has no int value
     * @author Hansj�rg Malthaner
     */
    const int * get_int(const char * name) const;


    /**
     * gets property int value.
     * @return value or default if property isn't set
     * @author Hansj�rg Malthaner
     */
    const int get_int(const char * name, int def) const;


    /**
     * sets property 'name' to an int value
     * @author Hansj�rg Malthaner
     */
    void set(const char * name, int value);


    /**
     * sets property 'name' to a string value
     * @author Hansj�rg Malthaner
     */
    void set(const char * name, const char * vlaue);


    /**
     * Loads properties from a file.  
     * @return true on success, false otherwise
     * @author Hansj�rg Malthaner
     */
    bool load(const char *filename);


    /**
     * Loads properties from a section of a sectioned file.  
     * @author Hansj�rg Malthaner
     */
    void load_from_section(config_file_t * file);

};
#endif //PROPERTIES_T_H
