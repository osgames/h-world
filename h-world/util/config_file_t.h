/* Copyright by Hj. Malthaner */

#ifndef CONFIG_FILE_T_H
#define CONFIG_FILE_T_H

#include <stdio.h>

/**
 * A config file is a text file with config data, empty lines and comment
 * lines. This class can read config data from such files and skips
 * empty lines and comment lines. Comments are introduced by '#' in the
 * first column of a line
 *
 * @author Hansj�rg Malthaner 
 */
class config_file_t {
private:

  FILE *file;

public:    
  
  config_file_t();

  ~config_file_t();


  /**
   * Opens a file for reading.
   * @author Hansj�rg Malthaner 
   */
  bool open(const char* filename);

  void close();


  /**
   * Checks if the end of the file has been reached.
   * @author Hansj�rg Malthaner 
   */
  bool config_file_t::eof() const;


  /**
   * Reads a config line from this file. Skips emtpy and comment lines.
   * @author Hansj�rg Malthaner 
   */
  char * read_line(char *buf, int len);
};

#endif //CONFIG_FILE_T_H
