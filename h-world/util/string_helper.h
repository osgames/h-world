#ifndef string_helper_h
#define string_helper_h


/**
 * Removes newlinbes from end of string. Modifies the
 * argument.
 * @author Hj. Malthaner
 */
void strip_newline(char * str);


/**
 * C++ conformant replacement of strdup().
 * uses new char[] to allocate the memory
 * @author Hj. Malthaner
 */
char * str_duplicate(const char * str);



#endif // string_helper_h
