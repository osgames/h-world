#include <math.h>
#include <stdio.h>

#include "perlin_t.h"



/**
 * Sets noise seed.
 * @author Hj. Malthaner
 */
void perlin_t::set_seed(int seed)
{
  noise_seed = seed*15731; 
}


/**
 * Noise function
 * @author Hj. Malthaner
 */
double perlin_t::int_noise(const long x, const long y) const
{               
    long n = x + y*101 + noise_seed;

    n = (n<<13) ^ n;
    return ( 1.0 - (double)((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}


/**
 * Cross-interpolated noise for int coordinates
 * @author Hj. Malthaner
 */
double perlin_t::smoothed_noise(const int x, const int y) const
{                         
/* this gives a very smooth world */
    const double corners = ( int_noise(x-1, y-1)+int_noise(x+1, y-1)+
                             int_noise(x-1, y+1)+int_noise(x+1, y+1) );

    const double sides   = ( int_noise(x-1, y) + int_noise(x+1, y) +
                             int_noise(x, y-1) + int_noise(x, y+1) );

    const double center  =  int_noise(x, y);     

    //    printf("%d, %d ->  %f\n", x, y, (corners + sides+sides + center*4.0) / 16.0);

    return (corners + sides+sides + center*4.0) / 16.0;


/* a hilly world
    const double sides   = ( int_noise(x-1, y) + int_noise(x+1, y) +
                             int_noise(x, y-1) + int_noise(x, y+1) );

    const double center  =  int_noise(x, y);     

    return (sides+sides + center*4) / 8.0;
*/

// this gives very hilly world
//   return int_noise(x,y);
}


double perlin_t::smoothed_noise_cached(const int x, const int y)
{
  double result;

  if(x<0 || y<0 || x>=PE_CACHE || y>=PE_CACHE) {
    result = smoothed_noise(x, y);

    // printf("%d,%d not cacheable!", x, y);
  } else {
    result = cache[x][y];
    if(result == -256.0) {
      result = smoothed_noise(x, y);
      cache[x][y] = result;
    }
  }

  return result;
}



// helper function
static inline double
linear_interpolate(const double a, const double b, const double x)
{
  return  a + x*(b-a);
}       


double perlin_t::interpolated_noise(const double x, const double y)
{
    const int    integer_X    = (int)x;
    const int    integer_Y    = (int)y;

    const double fractional_X = x - (double)integer_X;
    const double fractional_Y = y - (double)integer_Y;

    const double v1 = smoothed_noise_cached(integer_X,     integer_Y);
    const double v2 = smoothed_noise_cached(integer_X + 1, integer_Y);
    const double v3 = smoothed_noise_cached(integer_X,     integer_Y + 1);
    const double v4 = smoothed_noise_cached(integer_X + 1, integer_Y + 1);

    const double i1 = linear_interpolate(v1 , v2 , fractional_X);
    const double i2 = linear_interpolate(v3 , v4 , fractional_X);
    
    return linear_interpolate(i1 , i2 , fractional_Y);
}



perlin_t::perlin_t() 
{
  noise_seed = 0;


  for(int j=0; j<PE_CACHE; j++) {
    for(int i=0; i<PE_CACHE; i++) {
      cache[i][j] = -256.0;
    }
  }
}

                                             
/**
 * Perlin noise over 6 oktaves
 *
 * @param x x-Koordinate des Punktes
 * @param y y-Koordinate des Punktes
 * @param p   Persistenz
 * @author Hj. Malthaner
 */
double perlin_t::noise_2D(const double x, const double y, const double p)
{
    double total = 0.0;
      
    for(int i=0; i<6; i++) {

	const double frequency = (double)(1 << i);
	const double amplitude = pow(p, (double)i);

	total += interpolated_noise((x * frequency) / 64.0, 
                                    (y * frequency) / 64.0) * amplitude;
    }

    return total;
}
