/* Copyright by Hj. Malthaner */

#ifndef RNG_T_H
#define RNG_T_H


/**
 * A random number generator (rng). No singleton, but there is a single rng
 * which can be got from this class, but everyone is free to create other rng 
 * instances.
 * @author Hj. Malthaner
 */
class rng_t {
private:    

  /**
   * The single, global rng.
   * @author Hj. Malthaner
   */
  static rng_t the_rng;


  /**
   * The seed fort the next random number
   * @author Hj. Malthaner
   */
  unsigned long seed;


public:

  /**
   * Get the single, global rng.
   * @author Hj. Malthaner
   */
  static rng_t & get_the_rng();


  void set_seed(unsigned long s);

  
  /**
   * Get a random int in the range (0...max-1)
   * @author Hj. Malthaner
   */
  int get_int(int max);


  int roll(int dice, int sides);

  /**
   * Constructs a rng with seed 0.
   * @author Hj. Malthaner
   */
  rng_t();


  /**
   * Constructs a rng with a given seed.
   * @author Hj. Malthaner
   */
  rng_t(unsigned long seed);

};
#endif //RNG_T_H
