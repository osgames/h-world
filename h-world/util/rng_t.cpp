/* Copyright by Hj. Malthaner */

#include "rng_t.h"

rng_t rng_t::the_rng;


// Hajo: if you have mersenne twister sources available,
// you can use them

//#define USE_TWISTER


#ifdef USE_TWISTER

#include "/usr/local/src/andere/mersenne_twister/cokus.c"

void rng_t::set_seed(unsigned long s)
{
  seedMT(s);
}


int rng_t::get_int(int max) 
{
  return randomMT() % max;
}

#else

void rng_t::set_seed(unsigned long s)
{
  seed = s;
}


int rng_t::get_int(int max) 
{
  seed *= 3141592621u;
  seed ++;
  
  return (seed >> 8) % max;
}

#endif


rng_t::rng_t()
{
  set_seed(1);
}


rng_t::rng_t(unsigned long seed)
{
  set_seed(seed);
}


int rng_t::roll(int dice, int sides)
{
  int sum = 0;

  for(int i=0; i<dice; i++) {
    sum += get_int(sides)+1;
  } 

  return sum;
}


rng_t & rng_t::get_the_rng() {
  return the_rng;
}


