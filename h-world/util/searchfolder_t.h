/*
 *
 *  searchfolder_t.h
 *
 *  Copyright (c) 1997 - 2002 by Volker Meyer & Hansj�rg Malthaner
 *
 *  This file is part of the Simutrans project and may not be used in other
 *  projects without written permission of the authors.
 *
 */

#ifndef searchfolder_t_h
#define searchfolder_t_h

#include "primitives/cstring_t.h"
#include "tpl/slist_tpl.h"


/*
 *  class:
 *      searchfolder_t()
 *
 *  Autor:
 *      Volker Meyer
 *
 *  Beschreibung:
 *      ...
 */
class searchfolder_t {
    slist_tpl<cstring_t> files;

public:
    int search(const char *filepath, const char *extension);

    static cstring_t complete(const char *filepath, const char *extension);
    const cstring_t &at(int i);
};

#endif // searchfolder_t_h


