#include "nsow.h"

const koord nsow[4] = {
  koord(-1, 0), // north
  koord( 1, 0), // south
  koord( 0, 1), // east
  koord( 0,-1), // west
};


/**
 * Useful to calculate 8-neighbourhood
 * Starting north (0, -1) going clockwise
 * @author Hj. Malthaner
 * @date 03-Sep-2003
 */
extern const koord neighbour_8[8] = {
  koord( 0,-1),
  koord( 1,-1),
  koord( 1, 0),
  koord( 1, 1),

  koord( 0, 1),
  koord(-1, 1),
  koord(-1, 0),
  koord(-1, -1)
};


/**
 * Keypad directions
 * @author Hj. Malthaner
 * @date 19-Dec-2003
 */
extern const koord * directions = 0; 


extern const koord directions_iso[9] = 
{
  koord( 0,+1),
  koord(+1,+1),
  koord(+1,0),
  koord(-1,+1),
  koord( 0,0),
  koord(+1,-1),
  koord(-1,0),
  koord(-1,-1),
  koord( 0,-1),
};


extern const koord directions_2d[9] = 
{
  koord(-1,+1),
  koord( 0,+1),
  koord(+1,+1),
  koord(-1,0),
  koord( 0,0),
  koord(+1,0),
  koord(-1,-1),
  koord( 0,-1),
  koord(+1,-1),
};

