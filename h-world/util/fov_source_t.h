/* Copyright by Hj. Malthaner */

#ifndef FOV_SOURCE_T_H
#define FOV_SOURCE_T_H
class koord;

/** @interface */
class fov_source_t {
public:    

    virtual bool is_blocking(koord pos) = 0;
};
#endif //FOV_SOURCE_T_H
