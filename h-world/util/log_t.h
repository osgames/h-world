/* 
 * log.h
 *
 * Copyright (c) 1997 - 2001 Hansj�rg Malthaner
 *
 * This file is part of the Simutrans project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef tests_log_h
#define tests_log_h

#include <stdio.h>

class log_t
{
public:

  enum log_level {DEBUG=0, MESSAGE=1, WARN=2};


private:
    /**
     * Primary log file.
     * @author Hj. Malthaner
     */
    FILE *log;

    
    /**
     * Secondary log file, currently fixed to stderr
     * @author Hj. Malthaner
     */
    FILE * tee;

    bool force_flush;


    /**
     * Logging level. Only message of equal or higher priority 
     * are logged.
     * @author Hj. Malthaner
     */
    enum log_level level;
public:


    enum log_level get_level() const {return level;};
              

    /**
     * writes a debug message into the log.
     * @author Hj. Malthaner
     */
    void debug(const char *who, const char *format, ...);


    /**
     * writes a message into the log.
     * @author Hj. Malthaner
     */
    void message(const char *who, const char *format, ...);


    /**
     * writes a warning into the log.
     * @author Hj. Malthaner
     */
    void warning(const char *who, const char *format, ...);


    /**
     * writes an error into the log.
     * @author Hj. Malthaner
     */
    void error(const char *who, const char *format, ...);


    /**
     * writes an error into the log, aborts the program.
     * @author Hj. Malthaner
     */
    void fatal(const char *who, const char *format, ...);



    void close();


    log_t(const char *logname, enum log_level level, bool force_flush);    
    ~log_t();
};


#endif
