/* Copyright by Hj. Malthaner */


#include <string.h>
#include "config_file_t.h"

config_file_t::config_file_t()
{
  file = NULL;
}


config_file_t::~config_file_t()
{
  close();
}

bool config_file_t::open(const char * filename)
{
  if(file) {
    close();
  }

  file = fopen(filename, "rb");

  return file != NULL;
}


void config_file_t::close()
{
  if(file) {
    fclose(file);
    file = NULL;
  }
}


/**
 * Checks if the end of the file has been reached.
 * @author Hansj�rg Malthaner 
 */
bool config_file_t::eof() const
{
  return (feof(file) != 0);
}


/**
 * Reads a config line from this file. Skips emtpy and comment lines.
 * @author Hansj�rg Malthaner 
 */
char * config_file_t::read_line(char *buf, int len)
{
    char *r = NULL;

    if(file) {
      do {
        r = fgets(buf, len, file);
	buf[len] = 0;

      } while(r != NULL && (*buf == '#' || strlen(buf) < 2));
    }

    if(r) {
      // strip trailing whitespace

      int l = strlen(buf)-1;

      while(l >= 0 && buf[l] >= 0 && buf[l] < 32) {
	buf[l--] = 0;
      }
    }


    return r;
}

