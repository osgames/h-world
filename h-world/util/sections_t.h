/* 
 * sections_t.h
 *
 * Copyright (c) 2001 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */


#ifndef SECTIONS_T_H
#define SECTIONS_T_H

template <class T> class stringhashtable_tpl;

class properties_t;

/**
 * Sections are like properties but in a one-level hierearchy. Each section has a set
 * properties and each section has a name. So a sections file is like a list of data 
 * objects referenced by their names.
 *
 * Format is:
 * <pre>
 * begin section a_name
 * prop1 = value1
 * prop2 = value2
 * ...
 * end section
 * begin section another_name
 * ...
 * end section
 * </pre>
 */
class sections_t
{
private: 

    /**
     * Table of sections.
     * @author Hansj�rg Malthaner
     */
    stringhashtable_tpl <properties_t *> * sections;

public:

    const stringhashtable_tpl <properties_t *> * get_sections() const {return sections;};

    sections_t();

    ~sections_t();


    /**
     * load all sections from a file
     * @param filename the name (including path) of the file to read.
     * @author Hansj�rg Malthaner
     */
    bool load(const char *filename);


    /**
     * Get a section as properties table
     * @param name the name of the section to get
     * @author Hansj�rg Malthaner
     */
    properties_t * get_section(const char *name);
};

#endif
