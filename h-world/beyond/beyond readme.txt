BEYOND H-WORLD (BHW)
--------------------
Last Update: 12/5/2004 Ver 0.0.7 (H-World 0.3.1-2)

http://codemonkey.sunsite.dk/projects/beyond/hworld.html
Beyond H-World is a collaboration with Hajo's H-World CRPG Engine. It uses this engine, which is still evolving, to bring to life a version of the Beyond Rogue Like game. These pages will be updated as progress is being made. The project started in April 2004. You can see below a story for the game and graphics as they come to hand.

http://codemonkey.sunsite.dk/projects/beyond/hworld/intro.html
Game Start Intro - The intro that greets you when you start Beyond H-World.

http://www.simugraph.com/forum/viewtopic.php?id=5&t_id=9
Beyond Heroes - Pick heroes instead of class and race, here is a list of potentials.

http://www.simugraph.com/forum/viewtopic.php?id=5&t_id=6
Beyond HW Spec - The evolving specifications for BHW, and an informative dev diary.

Beyond HWorld Intro Story
-------------------------
I was on my way to be covered in glory and the gold of the Inca's! I had heard, even in my remote village, of the fabulous wealth and power of the Inca nation. Having just come of age I resolved to seek out an Inca King or Queen, to serve their court, and find high adventure - and be paid plenty of gold!

Fortune, however, smiles only on a whim, and makes you earn her favour. Traveling the Jivaran Indian river, Kun, the snow god, chose just that time to punish the whole of humanity for their arrogance. Ironic now that I look back, I hope it wasn't me that pushed us over the edge.

The whole of creation was covered with snow and ice, with a massive ice flood. My puny raft, and most of my equipment and stores, washed away, at the whim of the gods! Washed many leagues away I woke, alive, by the Lake Titicaca where the Eagle Men, descended from the gods of fertility, were busy creating a new people - the Paka-Jakes. The flood was gone and the bright new sun of the god Chasca was rising on a bright new people.

Looking around I could tell I was hopelessly lost, deep in the jungle, with no chance of finding home. I could see most of humanity had been washed away, leaving ruins, and temples, and dungeons, full of gold, all for me and only a few other souls! Unfortunately Kun had also graced the lands with all manner of foul beasts, undead fiends and ravenous monsters to finish the job he started with the ice flood.

The Paka-Jakes had set up a trading community nearby, from which I could secure supplies. Their chief tells me that if I am able to defeat Supai's servant, the master of the undead in the region: the fearsome Skeleton King, then she will assign her best tracker to lead me back out of this jungle. I set out on my quest, with my remaining supplies, and a deserted house I had claimed, and my trusty new friend "Donkey", but all the while the back of my mind was asking: "Back out to what?". 

Thanks/Credits
--------------
http://h-world.simugraph.com/
To Hajo the creator of the H-World engine (Most of the graphics above come from his The Jungle setting) - and for helping me use his engine.
To R.Dan.Henry for design consultation and excellent redirection of my wayward ideas.
Sheep for excellent graphics specially written for The Jungle and Beyond H-World.
To the crew at R.G.R.D.
To David E. Gervais for giving his permission to use his tiles from Dungeon Odyessey and others.
To M.Itakura for use of some of his excellent public domain tilesets.


Copyright Notices
-----------------
Part of the graphic tiles used in this program is the public 
domain roguelike tileset created by M.Itakura( itakura @ users.sf.net)
and contributors.   M.Itakura representatively holds the copyright.
Some of the tiles have been modified by ABCGi.

You can find the original tileset at:
http://www.geocities.co.jp/SiliconValley-SanJose/9606/nh/tile.html