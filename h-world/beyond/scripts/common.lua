-- ABCGi 0.1.1 3/6/2004 (HW 0.3.4)

----------------------------------------------------------------
-- common.lua
--
-- Common code for other scripts to call
--
-- Author: ABCGi
-- Created: 3-6-2004
--
-- FUNCTIONS;
-- local result = heal_being(user,10)
----------------------------------------------------------------

------------------
-- ABCGi FUNCTIONS
------------------
-- A collection of useful functions called by other scripts
-----------------------------------------------------------

-- heal_being
-- ABCGi 0.1.1 3/6/04 (HW 0.3.4)
-- heal, or deduct, amount but only up to max or zero, for player or monster
function heal_being(user,amount)
        local hp = thing_get_value(user, "HPcurr") or 0
        local maxhp = thing_get_value(user, "HPmax") or 0
        local new_hp = hp + amount

        if maxhp > 0 then
          if new_hp < 0 then
            new_hp = 0
          end
          if new_hp > maxhp then
            new_hp = maxhp
          end
          -- if a change then set value
          local return_value = new_hp - hp
          if return_value ~= 0 then
            thing_set_value(user, "HPcurr", new_hp)
            return return_value
          else
            return 0  
          end
        end       
end
