
----------------------------------------------------------------
-- recipes.lua
--
-- H-World recipes initialition script, purpose is to contain all
-- recipes for item creation
--
-- author: Hj. Malthaner
-- date:   27-Dec-2002
-- update: 02-May-2004
----------------------------------------------------------------




------------------------------------------------------------------
-- Items are combined by dropping one onto the other
--
-- author: Hj. Malthaner
-- date:   27-Dec-2002
-- update: 02-May-2004 - ignore magic item prefixes
-- update: 19-May-2004 - added inventory parameter
--
-- param user:      type thing_t, the user
-- param inventory: type inventory_t, the inventory that contains the item
-- param item:      type thing_t, the item
-- param drop:      type thing_t, the thing that is dropped onto the item
-- return:          1 on success, 0 on failure
------------------------------------------------------------------



function drop_item(user, inventory, item, drop)
	_ALERT("lua: drop_item called\n")

	-- Recipes work, if the player has identified the items
        -- or not, thus we pass 1 for 'memorized' to get the
        -- real identifier always

	local id1 = calculate_plain_ident_string(item, 1)
	local id2 = calculate_plain_ident_string(drop, 1)

	_ALERT("lua: id1=" .. id1 .. " id2=" .. id2 .."\n")

	-- recipes based on a healing potion

	if id1 == "healing potion" then

		-- ATM everything transform it to poison

		if id2 == "rat tail" or id2 == "bitter leaves" then

			local potion = thing_create("potion_of_poison")
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			return 1
		end
	end


	-- recipes based on a weak healing potion

	if id1 == "weak healing potion" then

		if id2 == "rat tail" then

			local potion = thing_create("healing_potion")
			thing_set_value(potion, "is_unknown", "false")
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			return 1
		end

		if id2 == "bitter leaves" then

			local potion = thing_create("potion_of_poison")
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			return 1
		end
	end


	-- recipes based on a bottle of water

	if id1 == "bottle of water" then

		if id2 == "bitter leaves" then

			local potion = thing_create("weak_healing_potion")
			thing_set_value(potion, "is_unknown", "false")
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			return 1
		end

		if id2 == "rat tail" then

			local potion = thing_create("poisonous_potion")
			inventory_remove_thing(inventory, item)
			inventory_add_thing(inventory, potion)

			return 1
		end
	end


	return 0
end
