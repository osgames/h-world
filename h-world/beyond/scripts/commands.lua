-- ABCGi 0.0.9 24/5/2004 (HW 0.3.3-2)

----------------------------------------------------------------
-- commands.lua
--
-- H-World user commands script. Called if the user presses a key
-- this scrip can handle all user input that is not mapped to a
-- default action
--
-- Author: Hj. Malthaner
-- Created: 11-May-2002
-- Last update: 11-May-2003
----------------------------------------------------------------


------------------------------------------------------------------
-- Called if the user presses a key
--
-- author: Hj. Malthaner
-- date:   11-May-2003
-- update: 11-May-2003
--
-- param user:   type thing_t, the player character
-- param code:   key code
-- return:       delay (duration of action)
------------------------------------------------------------------


function key_pressed(user, code)
	local delay = 0       -- 0 means not to quit the input loop yet

	-- just an example how to use commands
	-- code 32 is SPACE

	if(code == 32) then
		translate("Example user defined command script for key SPACE was triggered.")
                -- ten is the standard duration, it equals
		-- roughly the time to make one step while 
		-- walking

		local x, y
		x,y = thing_get_location(user)

		translate("You are at location " .. x .. ", " .. y .. ".");

                -- ABCGi 0.0.9 this should't take any time off player 
                delay = 0
		-- delay = 10
	end


	-- code 'D'
	if code == 68 then
		-- Disband what ?

		local x, y, xd, yd

		x, y = thing_get_location(user)

		xd, yd = user_ask_direction()

		x = x + xd
		y = y + yd


		-- find thing there

		local n = 0


			local thing = square_get_thing(x, y, n)

			if(thing ~= nil and 
                           thing_get_value(thing, "status") == "friend") then

				if(thing == user) then
					translate("You can't disband yourself.")
				else
					translate("You disband the " .. thing_get_ident(thing) .. ".")
					thing_set_value(thing, "status", "neutral")
				end
			end
		
	end


	-- code '^', place a trap
	if code == 94 then
		translate("Which type of trap do you want to place [0,1]?")
		local type = user_ask_key() - 48

		local x, y
		x,y = thing_get_location(user)

		square_set_trigger(x, y, type)

		translate("Placing a trap of type " .. type .. ".")

		delay = 10
	end


	return delay
end


------------------------------------------------------------------
-- Execute special attacks
--
-- author: Hj. Malthaner
-- date:   19-Oct-2003
-- update: 19-Oct-2003
--
-- param attack_call: the name (key) of the attack to execute
-- param attacker: type thing_t, the attacker
-- param target:   type thing_t, the target of the attack
-- return:         nothing
------------------------------------------------------------------

function execute_attack_call(attack_call, attacker, target)

	-- Not yet used in "The Jungle"
	-- this is just here as example where to put 
	-- special attack code

end