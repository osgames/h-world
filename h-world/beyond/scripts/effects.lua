-- ABCGi 0.1.1 3/6/2004 (HW 0.3.4)
-- 0.0.9 24/05/04 update global values player_x and player_y each turn (defined in init.lua)

----------------------------------------------------------------
-- effects.lua
--
-- H-World per-round effects  initialition script, purpose is to
-- contain effects that might happen per turn
--
-- author: Hj. Malthaner
-- date:   23-Apr-2003
-- update: 18-Apr-2003
----------------------------------------------------------------



------------------------------------------------------------------
-- called every turn, with the duration of the last executed action
--
-- author: Hj. Malthaner
-- date:   23-Apr-2003
-- update: 18-Apr-2004
--
-- param user:     type thing_t, the player character
-- param duration: type number, the duration of the last turn 
-- return:         type number, dummy value
------------------------------------------------------------------

------------------
-- ABCGi FUNCTIONS
------------------

-- Hajo's sample code
-- local x, y   
-- x, y = thing_get_location(user)   
-- local item = nil   
-- local n = 0   
-- repeat   
--   item = square_get_item(x, y, n)   
--   if item then   
--     if thing_get_value(item, "type") == "altar" and thing_get_value(item, "subtype") == "headstone cross" then   
--       -- heal the player, or whatever shoudl happen here   
--     end   
--     n = n + 1   
--   end   
-- until item == nil   

function per_turn_effects(user, duration)

--	_ALERT("per_turn_effects()" .. " duration=" .. duration .. "\n")


	local is_player = thing_get_value(user, "is_player")

	-- ABCGi 0.0.9 update global player position
	if is_player then		
		player_x,player_y = thing_get_location(user) 
		_ALERT("player_x = " .. player_x .. " player_y = " .. player_y .. "\n")

		-- ABCGi 0.1.1 - add blank line to message between each turn
		translate("________________________________________________________________")
	end

	--
        -- ABCGi 0.1.1 3/6/04 (HW 0.3.4)
        -- Treat special effects standing on certain items
        --  * heal bonus on any altar/cross items
        --      
	if is_player then
 	  local item = nil   
	  local n = 0   
	  repeat   
	  item = square_get_thing(player_x, player_y, n)
	  if item then   
	    if thing_get_value(item, "type") == "altar" and thing_get_value(item, "subtype") == "heal" then   
	      -- heal the player bonus calling init.lua function
	      local result = heal_being(user,10)
	      if result ~= 0 then
	        translate("You feel the power and the glory, the coming of the sun, the setting of tommorow (+" .. (result) .. ")")              
	      end
	    end   
	    n = n + 1   
	  end   
	  until item == nil   
	end

	--
	-- Treat poison ABCGi 0.1.1 add feel ill message and rng chance
	-- the poison is most frequent when strong and hard to get rid off near end
	--

  	local poison = thing_get_value(user, "effect.poison") or 0

  	if poison > 0 then

                if rng_get_int(100) < (poison + 10) then
      		  _ALERT("per_turn_effects() " .. "poison=" .. poison .. "\n")

                  local poison_effect = (duration/5) + 1;
                  local new_hp = (thing_get_value(user, "HPcurr") or 0) - poison_effect;
    		  thing_set_value(user, "HPcurr", new_hp);
    		  thing_set_value(user, "effect.poison", poison-1);
                  if is_player then
    		    translate("The poison in your system makes you ill (-" .. poison_effect .. " hp)")
    		  end
    		end

	end


	--
	-- Treat paralysm (ABCGi 0.0.9 now cumulative)
	--

  	local para = thing_get_value(user, "effect.paralyzed") or 0

  	if para > 0 then
    		_ALERT("per_turn_effects() " .. "para=" .. para .. "\n")

		-- ABCGi 0.0.9 if paralyse reaches 100 you turn to stone!
		if is_player then		
			if (thing_get_value(user, "effect.paralyzed") > 99) then
				translate("You are overcome with paralysis and turn to stone!")
				thing_kill(user, 0)
			end
		end	

    		thing_set_value(user, "effect.paralyzed", para-1);
	end


	--
	-- Treat regeneration
	--
	local hp = thing_get_value(user, "HPcurr")
	local maxhp = thing_get_value(user, "HPmax")

	-- restore hit point up to max
	if rng_get_int(300) <= duration and hp < maxhp then
		hp = hp+1
		-- set values
		thing_set_value(user, "HPcurr", hp)
	end


	--
	-- Treat hunger and thirst, ATM player only because
	-- player can't feed followers
	--
	
	local hunger = 0
	local thirst = 0
	
	if is_player then

		hunger = thing_get_value(user, "hunger") or 0
	
		if rng_get_int(800) <= duration then
			hunger = hunger +1

			if hunger > 100 then
				hunger = 100
				hp = hp -1
				-- hunger weakens
				thing_set_value(user, "HPcurr", hp)
			end

			thing_set_value(user, "hunger", hunger)
		end


		thirst = thing_get_value(user, "thirst") or 0
	
		if rng_get_int(400) <= duration then
			thirst = thirst +1

			if thirst > 100 then
				thirst = 100
				hp = hp -1
				-- thirst weakens
				thing_set_value(user, "HPcurr", hp)
			end

			thing_set_value(user, "thirst", thirst)
		end
	end

	--
	-- status icons
	--

	-- non-existant icon is treated as invisible icon
	local status_icon = thing_get_value(user, "status_icon") or 9999


	-- clear old icon, 0 = delayed redraw

	thing_visual_remove_image(user, status_icon, 0);
	status_icon = 9999
	
	-- cascade, from least important to most important

	if hp < 2 or hp < maxhp/5 then
		status_icon = sym_hurt
	end

	if hunger > 80 then
		status_icon = sym_hunger
	end

	if thirst > 80 then
		status_icon = sym_thirst
	end

	if poison > 0 then
		status_icon = sym_poison
	end

	if para > 0 then
		status_icon = sym_frozen
	end

	-- set new icon, 0 = delayed redraw

	thing_visual_add_image(user, status_icon, 0);


	-- remember value

	thing_set_value(user, "status_icon", status_icon)


	return 0
end
