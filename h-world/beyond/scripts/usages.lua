-- ABCGi 0.0.9 24/5/2004 (HW 0.3.3-2)
-- 0.0.8 21/5/04 Cumulative poison from potion and message (+pois)
-- 0.0.9 24/5/04 Add some +scores to usage messages

----------------------------------------------------------------
-- usages.lua
--
-- H-World usages initialition script, purpose is to contain all
-- usages for items
--
-- author: Hj. Malthaner
-- date:   20-Mar-2002
-- update: 24-Mar-2002
-- update: 27-Apr-2004 - Hj. Malthaner: new handling of magic item attributes
-- update: 12-May-2004 - Hj. Malthaner: handle non-thirsty beings correctly
-- update: 13-May-2004 - Hj. Malthaner: handle non-hungry beings correctly
----------------------------------------------------------------


------------------------------------------------------------------
-- basic item wield function, should be called by all functions
-- that are called upon a wield event
--
-- author: Hj. Malthaner
-- date:   08-Aug-2002
-- update: 23-Dec-2002
-- update: 27-Apr-2004 (magic item attributes)
--
-- param owner:  type thing_t, the one who wields the item
-- param limb:   type thing_t, the limb that holds the item
-- param item:   type thing_t, the item to wield
-- return:       nothing
------------------------------------------------------------------


function base_wield(owner, limb, item)
	-- _ALERT("lua: base_wield called\n")

	-- make cursed state known

	thing_set_value(item, "curse_known", "true")


	-- check item attributes

	local effect = nil
	local n = 0

	repeat
		local att_n = "att-" .. n

		effect = thing_get_value(item, att_n .. ".effect")

		if(effect) then

			value  = thing_get_value(item, att_n .. ".value");
			target = thing_get_value(item, att_n .. ".target");


			_ALERT("lua: value="..value.."\n")
			_ALERT("lua: target="..target.."\n")
			_ALERT("lua: effect="..effect.."\n")

			if(target == "self") then
				app = item
			else
				app = owner
			end

			v = thing_get_value(app, effect) or 0
		 
			thing_set_value(app, effect, v+value)
		end

		n = n+1 

	until (effect == nil)


	-- check item bulk

	effect = thing_get_value(item, "bulk");

	if(effect) then
		v = thing_get_value(owner, "hinderance") or 0
		thing_set_value(owner, "hinderance", v+effect)
	end

	return 0
end


------------------------------------------------------------------
-- basic item unwielding function, should be called by all functions
-- that are called upon a unwield event
--
-- author: Hj. Malthaner
-- date:   08-Aug-2002
-- update: 23-Dec-2002
-- update: 27-Apr-2004 (magic item attributes)
--
-- param owner:  type thing_t, the one who wields the item
-- param limb:   type thing_t, the limb that holds the item
-- param item:   type thing_t, the item to unwield
-- return:       nothing
------------------------------------------------------------------


function base_unwield(owner, limb, item)
	-- _ALERT("lua: base_unwield called\n")


	-- check item attributes

	local effect = nil
	local n = 0

	repeat
		local att_n = "att-" .. n

		effect = thing_get_value(item, att_n .. ".effect")

		if(effect) then

			value  = thing_get_value(item, att_n .. ".value");
			target = thing_get_value(item, att_n .. ".target");


			_ALERT("lua: value="..value.."\n")
			_ALERT("lua: target="..target.."\n")
			_ALERT("lua: effect="..effect.."\n")

			if(target == "self") then
				app = item
			else
				app = owner
			end

			v = thing_get_value(app, effect) or 0
		 
			thing_set_value(app, effect, v-value)
		end

		n = n+1 

	until (effect == nil)


	-- check item bulk

	effect = thing_get_value(item, "bulk");

	if(effect) then
		v = thing_get_value(owner, "hinderance") or 0
		thing_set_value(owner, "hinderance", v-effect)
	end

	return 0
end





------------------------------------------------------------------
-- quaff a potion
--
-- Determine potion type, subtype, calculate effect and apply effect to user
--
-- author: Hj. Malthaner
-- date:   20-Mar-2002
-- update: 24-Mar-2002
-- update: 12-May-2004 - Hj. Malthaner: handle non-thirsty beings correctly
--
-- param user:   type thing_t, the one who quaffs the potion (player or other)
-- param potion: type thing_t, the potion to be quaffed
-- return:       stack count reduction
------------------------------------------------------------------

function quaff (user, potion)
	_ALERT("quaff called\n")

	local is_player = thing_get_value(user, "is_player")

	if (is_player) then
		translate("You are quaffing a " .. thing_get_value(potion, "ident") .. ".")
	end

	local hp = thing_get_value(user, "HPcurr")
	local maxhp = thing_get_value(user, "HPmax")
	local power = thing_get_value(potion, "power")
	local subtype = thing_get_value(potion, "subtype")


	-- process healing potions
	
	if subtype == "heal" then
		_ALERT("  Healing: hp=".. hp .."\n")

		local rb = rng_get_int(power)+power/3

		-- restore hit point up to max

		if hp+rb > maxhp then
			hp = maxhp
		else
			hp = hp+rb
		end

		-- set values

		thing_set_value(user, "HPcurr", hp)

		-- tell user about effect

		if (is_player) then
			if rb < 10 then
				translate("You feel little better. (+" .. rb .. ")")
			elseif rb < 20 then
				translate("You feel better. (+" .. rb .. ")")
			elseif rb < 30 then
				translate("You feel much better. (+" .. rb .. ")")
			else
				translate("You feel much better! (+" .. rb .. ")")
			end

		else
			translate("The " .. thing_get_ident(user) .. " looks healthier.")
		end
	end


	-- process poisoning potions ABCGi 0.0.8

	if subtype == "poison" then
		_ALERT("  Poison")

                local rb = rng_get_int(power)+power/3

                -- ABCGi cumulative poison added 0.0.8
                local poison = thing_get_value(user, "effect.poison") or 0
                poison = poison + rb 
                thing_set_value(user, "effect.poison", poison)
		-- thing_set_value(user, "effect.poison", rb)

		-- tell user about cumulative effects ABCGi 0.0.8
                if poison > rb then
			if (is_player) then
				translate("You feel even more sick. (+" .. rb .. ")")
			else 
				translate("The " .. thing_get_ident(user) .. " gets even greener! (+" .. rb .. ")")
			end
                else
			if (is_player) then
				translate("You feel very sick. (+" .. rb .. ")")
			else 
				translate("The " .. thing_get_ident(user) .. " gets somewhat green. (+" .. rb .. ")")
			end
		end
		
	end


	-- process antidotes

	if subtype == "antidote" then
		_ALERT("  Antidote")

		local rb = rng_get_int(power)+power/3

		local poison = thing_get_value(user, "effect.poison") or 0

		if (is_player) then
			if rb >= poison then
				poison = 0
				translate("You feel perfectly well. (-" .. rb .. ")")
			else
				-- fixed Hajo type
				poison = poison - rb
				translate("You feel less poisoned. (-" .. rb .. ")")
			end
		else
			translate("The " .. thing_get_ident(user) .. " looks better.")
		end

		thing_set_value(user, "effect.poison", poison)
	end


	-- modify bottle
	
	local bottle = thing_create("empty_bottle")
	if bottle ~= nil then
		thing_replace_item(user, potion, bottle)
	end


	-- reduce thirst

	local thirst = thing_get_value(user, "thirst")

	-- only handle thirsty beings

	if(thirst) then
		thirst = thirst - power

		if thirst < 0 then 
			thirst = 0
		end

		thing_set_value(user, "thirst", thirst)

		-- tell user about effect ABCGi 0.0.9 
		if(is_player) then
			translate("You feel less thirsty. (-" .. power .. ")")
		end
	end


	return 0
end


------------------------------------------------------------------
-- Empty a (filled) bottle
--
-- Replaces bottle with an empty bottle
--
-- author: Hj. Malthaner
-- date:   13-Apr-2004
-- update: 13-Mar-2004
--
-- param user:   type thing_t, the one who empties the bottle (player or other)
-- param potion: type thing_t, the bottle to be emptied
-- return:       stack count reduction
------------------------------------------------------------------

function empty (user, potion)
	_ALERT("quaff called\n")
	
	if thing_get_value(user, "is_player") ~= nil then
		translate("You empty the " .. thing_get_value(potion, "ident") .. ".")
	end

	local hp = thing_get_value(user, "HPcurr")
	local maxhp = thing_get_value(user, "HPmax")
	local power = thing_get_value(potion, "power")
	local subtype = thing_get_value(potion, "subtype")


	-- process poisoning potions ABCGi 0.0.8

	if subtype == "poison" then
		_ALERT("  Poison")

		translate("The bottles content smells strange.")

                -- ABCGi 0.0.8 correcting bug in math to make less potent for emptying than swallowing
                local rb = (rng_get_int(power)+power/3) / 10
		-- local rb = rng_get_int(power)+power/30

		if rb >= 1 then
			-- set values
			thing_set_value(user, "effect.poison", rb)

			-- tell user about effect ABCGi 0.0.8
			translate("You feel somewhat sick. (+" .. rb .. ")")
		end
	end


	-- modify bottle
	
	local bottle = thing_create("empty_bottle")
	if bottle ~= nil then
		thing_replace_item(user, potion, bottle)
	end


	return 0
end



------------------------------------------------------------------
-- Eat something
--
-- Determine food type, subtype, calculate effect and apply effect to user
--
-- author: Hj. Malthaner
-- date:   22-Dec-2003
-- update: 13-May-2004 - Hj. Malthaner: handle non-hungry beings correctly
--
-- param user:   type thing_t, the one who eats
-- param potion: type thing_t, the food to be eaten
-- return:       stack count reduction
------------------------------------------------------------------

function eat (user, food)
	_ALERT("eat called\n")

	if thing_get_value(user, "is_player") ~= nil then
		translate("You are eating the " .. thing_get_value(food, "ident") .. ".")
	end

	local hunger = thing_get_value(user, "hunger")
	local power = thing_get_value(food, "power")
	local subtype = thing_get_value(food, "subtype")

	-- only handle hungry beings

	if(hunger) then

		-- process ordinary food
	
		local nutrition = rng_get_int(power)+power/3

		-- lower hunger

		hunger = hunger - nutrition

		if hunger < 0 then 
			hunger = 0
		end

		thing_set_value(user, "hunger", hunger)

		-- tell user about effect ABCGi 0.0.9 
		if hunger < 10 then
			translate("You are full. (-" .. nutrition .. ")")
		else
			translate("You feel less hungry. (-" .. nutrition .. ")")
		end
	end

	return 1
end


------------------------------------------------------------------
-- Throw an item, put it on target square
--
-- author: Hj. Malthaner
-- date:   22-Apr-2004
-- update: 25-Apr-2004
--
-- param user:   type thing_t, the thrower
-- param ammo:   type thing_t, the item to throw
-- return:       nothing
------------------------------------------------------------------

function throw (user, ammo)
	_ALERT("throw called\n")

	local delay = thing_throw(user, ammo)


	-- potion bottles are thrown to break and free the contents
	if(thing_get_value(ammo, "type") == "potion") then
		-- 1 means to kill silently
		thing_kill(ammo, 1)
	end


	return delay;
end


------------------------------------------------------------------
-- Wield a light source
--
-- Determine light type, subtype, calculate effect and apply effect to user
--
-- author: Hj. Malthaner
-- date:   24-Jun-2002
-- update: 24-Jun-2002
--
-- param user:   type thing_t, the one who wields the light source
-- param limb:   type thing_t, the limb that holds the item
-- param light:  type thing_t, the light source
-- return:       nothing
------------------------------------------------------------------

function wield_light (user, limb, light)
	_ALERT("wield_light called\n")

	radius = thing_get_value(light, "light_rad");
	old_rad = thing_get_value(user, "light_rad");

	-- ABCGi 0.0.9
	translate(thing_get_value(user, "ident") .. " is wielding a " .. thing_get_value(light, "ident") .. " with radius " .. radius .. ".")
	_ALERT(thing_get_value(user, "ident") .. " is wielding a " .. thing_get_value(light, "ident") .. " with radius " .. radius .. "\n")


	-- set value
	thing_set_value(user, "light_rad", old_rad+radius);

	return 0
end


------------------------------------------------------------------
-- Unwield a light source
--
-- Determine light type, subtype, calculate effect and apply effect to user
--
-- author: Hj. Malthaner
-- date:   24-Jun-2002
-- update: 24-Jun-2002
--
-- param user:   type thing_t, the one who wields the light source
-- param limb:   type thing_t, the limb that holds the item
-- param light:  type thing_t, the light source
-- return:       nothing
------------------------------------------------------------------

function unwield_light (user, limb, light)
	_ALERT("unwield_light called\n")

	radius = thing_get_value(light, "light_rad");
	old_rad = thing_get_value(user, "light_rad");

	_ALERT(thing_get_value(user, "ident") .. " is unwielding a " .. thing_get_value(light, "ident") .. " with radius " .. radius .. "\n")

	-- set default value

	thing_set_value(user, "light_rad", old_rad-radius)

	return 0
end


------------------------------------------------------------------
-- Activate a ground prober (drill)
--
-- Generate a ground sample
--
-- author: Hj. Malthaner
-- date:   24-Jun-2002
-- update: 24-Jun-2002
--
-- param user:   type thing_t, the user of the ground prober
-- param prober: type thing_t, the ground prober
-- return:       nothing
------------------------------------------------------------------

function drill (user, prober)
	_ALERT("drill called\n")

	-- generate ground sample

	local sample = thing_create("ground_sample")
	
	thing_add_item_to_inv(prober, sample)

	translate("You drill into the ground and take a sample.")

	return 0
end


------------------------------------------------------------------
-- Identify an item
--
-- author: Hj. Malthaner
-- date:   14-Aug-2003
-- date:   14-Aug-2003
--
-- param user:   type thing_t, the user 
-- param prober: type thing_t, the identify gadget
-- return:       nothing
------------------------------------------------------------------

function identify (user, prober)
	_ALERT("identify called\n")


	local thing = thing_choose("Identify what?", user, nil, nil, nil)

	if thing ~= nil then
		-- set flag to identified
		thing_set_value(thing, "is_unknown", "false")
		-- set curses to known
		thing_set_value(thing, "curse_known", "true")

		translate("You identify a " .. thing_get_ident(thing) .. ".")

		-- some item types are memorized by type
                -- 'know one, know all'

		local type = thing_get_value(thing, "type")

		if(type == "potion" or type == "scroll") then 
			-- _ALERT("identify item class\n")
			thing_set_item_known(user, thing)
		end

		return 1
 	else

		return 0
	end
end


------------------------------------------------------------------
-- remove curse from an item
--
-- author: Hj. Malthaner
-- date:   16-Nov-2003
-- date:   16-Nov-2003
--
-- param user:   type thing_t, the user 
-- param thing:  type thing_t, the scroll
-- return:       nothing
------------------------------------------------------------------

function remove_curse (user, thing)
	_ALERT("identify called\n")


	local thing = thing_choose("Remove curse from which item?", user, nil, nil, nil)

	if thing ~= nil then

		translate("You uncurse the " .. thing_get_ident(thing) .. ".")

		thing_set_value(thing, "cursed", "false");
		-- set curses to known
		thing_set_value(thing, "curse_known", "true")

		return 1
 	else

		return 0
	end
end
