-- ABCGi 0.1.1 3/6/2004 (HW 0.3.4)
-- 0.0.9 24/05/04 Cumulative damage for special attacks

----------------------------------------------------------------
-- attacks.lua
--
-- H-World/The Jungle attacks script. Implements all kinds of attacks
--
--
-- Author: Hj. Malthaner
-- Created: 08-Jul-2002
----------------------------------------------------------------


------------------------------------------------------------------
-- Attack a target, step 1
--
-- author: Hj. Malthaner
-- date:   08-Jul-2002
-- update: 20-Jul-2002, Hj. Malthaner
--
-- param attacker:   type thing_t, the attacker
-- param target:     type thing_t, the target
-- param weapon:     type thing_t, the used weapon
--
-- return: 1 if attack was blocked, 0 if hit
------------------------------------------------------------------

function attack1 (attacker, target, weapon)

	-- _ALERT("Lua: attack1 called\n")

	-- allow target to block the attack, if it has a defender
        -- search for limb which usually holds the shield
        -- then determine blocking value of shield and try
        -- to block

    	sh = thing_find_limb(target, "hold", "shield");

    	if(sh ~= nil) then
      		shield = thing_get_item(sh);

      		if(shield ~= nil) then
			block = thing_get_value(shield, "block");

			if(block ~= nil and rng_get_int(100) <= block) then
				-- _ALERT("Lua: ... blocked\n")

	  			-- quit attack
				-- attack was  blocked
	 	 		return 1
			end
      		end
    	end


	-- allow target to evade an attack
	-- currently only considers target size and weapon weight
	--
	-- there is always a 5% chance to hit a target

	local width  = thing_get_value(target, "width") or 1
	local height = thing_get_value(target, "height") or 1
	local size_chance = width * height / 4 	

	local weapon_chance = (thing_get_value(weapon, "weight") or 1) / 1000

	local chance = 65 - weapon_chance + size_chance;

	-- _ALERT("Lua: chance=" .. chance .. "\n")

	if(chance <= 5) then 
		chance = 5 
	end

	if(rng_get_int(100) > chance) then
		-- quit attack
		-- attack misses target
		return 2
	end


	-- attack was successful
	return 0
end


------------------------------------------------------------------
-- Attack a target, step 2
--
-- author: Hj. Malthaner
-- date:   08-Jul-2002
-- update: 20-Jul-2002, Hj. Malthaner
--
-- param attacker:   type thing_t, the attacker
-- param weapon:     type thing_t, the weapon
-- param target:     type thing_t, the target
-- param armor:      type thing_t, the armor
-- param limb:       type thing_t, the hit limb
-- param b_dice:     type number, the blunt hit dice
-- param c_dice:     type number, the cut hit dice
-- param b_sides:    type number, the blunt hit dice sides
-- param c_sides:    type number, the cut hit dice sides
-- param pierce:     type number, the chance to penetrate the armor
--
-- return: nothing
------------------------------------------------------------------

function attack2 (attacker, weapon, target, armor, limb,
                  b_dice, b_sides, c_dice, c_sides, pierce)

	-- _ALERT("Lua: attack2 called\n")

    	-- roll damage, include damage modifiers

    	local bp = rng_roll(b_dice, b_sides)
    	local cp = rng_roll(c_dice, c_sides)

	local Cdam = thing_get_value(weapon, "Cdamage") or 0
	Cdam = Cdam + 100

	cp = (cp * Cdam)/100

	local Bdam = thing_get_value(weapon, "Bdamage") or 0
	Bdam = Bdam + 100

	bp = (bp * Bdam)/100

	local Pdam = thing_get_value(weapon, "pierce") or 0
	Pdam = Pdam + 100

	pierce = (pierce * Pdam)/100


	-- check piercing

	if(rng_get_int(100) > pierce) then

		-- check protection by armor

		if(armor ~= nil) then
    			local Bprot = thing_get_value(armor, "Bprotection")
    			local Cprot = thing_get_value(armor, "Cprotection")

			if(Bprot == nil or Bprot == 0) then Bprot = 1 end
			if(Cprot == nil or Cprot == 0) then Cprot = 1 end

			Bprot = rng_get_int(Bprot)
			Cprot = rng_get_int(Cprot)

			bp = bp - Bprot
			cp = cp - Cprot

			if(bp < 0) then 
				bp = 0 
				translate("The " .. thing_get_ident(armor) .. " absorbs a lot! (" .. Bprot .. " blunt damage absorbed)")
			end

			if(cp < 0) then 
				cp = 0 
				translate("The " .. thing_get_ident(armor) .. " protects well! (" .. Cprot .. " cut damage resisted)")
			end
		end

		-- check protection by nature

		if(limb ~= nil) then
    			local Bprot = thing_get_value(limb, "Bprotection")
    			local Cprot = thing_get_value(limb, "Cprotection")

			if(Bprot == nil or Bprot == 0) then Bprot = 1 end
			if(Cprot == nil or Cprot == 0) then Cprot = 1 end

			Bprot = rng_get_int(Bprot)
			Cprot = rng_get_int(Cprot)

			bp = bp - Bprot
			cp = cp - Cprot

			if(bp < 0) then 
				bp = 0 
				if(armor == nil) then
					translate("The " .. thing_get_ident(target) .. " is very firm! (" .. Bprot .. " blunt damage absorbed by hide)")
				end
			end

			if(cp < 0) then 
				cp = 0 
				if(armor == nil) then
					translate("The " .. thing_get_ident(target) .. " is very hard! (" .. Cprot .. " cut damage resisted by hide)")
				end
			end
		end

	end

	local star = 95

	if(thing_get_value(target, "is_player") ~= nil) then
		star = 90
	end

	local damage = (bp+cp) / 3

	if(damage > 3) then
		damage = 3
	end 

	star = star + damage

    	-- show damage
        -- a flashing star image overlay
    	thing_visual_add_image(target, star, 1);

	-- reduce hitpoints ABCGi 0.1.1 show damage message to player
        local damage_taken = bp + cp
      	if damage_taken ~= 0 then
    		currHP = (thing_get_value(target, "HPcurr") or 0) - damage_taken;
    		thing_set_value(target, "HPcurr", currHP);
		if (thing_get_value(target, "is_player") ~= nil) then
		  translate("You take " .. damage_taken .. " damage! (" .. currHP .. " hp)")
		end
	end

    	-- unshow damage
        -- remove flashing star image overlay
    	thing_visual_remove_image(target, star, 1);

	-- special damage
	local effect = thing_get_value(weapon, "attack.effect")
	if(effect ~= nil) then
		local dice = thing_get_value(weapon, "attack.effect.dice") or 1
		local sides = thing_get_value(weapon, "attack.effect.sides") or 1
		local power = rng_roll(dice, sides)

                -- ABCGi 0.0.9, add effect cumulatively, should only do for poison?
                local cum_effect = thing_get_value(target, effect) or 0     
                cum_effect = cum_effect + power                      
                thing_set_value(target, effect, cum_effect)
		-- thing_set_value(target, effect, power)

                -- ABCGi 0.0.9 cumulative message
		if(thing_get_value(target, "is_player") ~= nil) then
			if (cum_effect > power) then
				translate("You are more " .. (thing_get_value(weapon, "attack.effect.message") or "gizmogrified") .. "! (+" .. power .. " = " .. cum_effect .. ")")
			else
				translate("You are " .. (thing_get_value(weapon, "attack.effect.message") or "gizmogrified") .. "! (+" .. power .. ")")
			end
		end
	end


	-- special attack action -> calling a lua procedure
	-- to handle the attack
	local attack_call = thing_get_value(weapon, "attack.call")
	if(attack_call ~= nil) then
		execute_attack_call(attack_call, attacker, target)
	end


	-- _ALERT("Lua: ... strike\n")
end
