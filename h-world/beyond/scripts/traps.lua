-- ABCGi 0.0.9 24/5/2004 (HW 0.3.3-2)
-- 0.0.8 21/5/04 cumulative poison effect of trap
-- 0.0.9 24/5/04 range check for distant click message

----------------------------------------------------------------
-- traps.lua
--
-- H-World traps script. All trap efffects are scripted here.
-- Traps may include switches and non-harmful effects
--
--
-- Author:  Hj. Malthaner
-- Created: 08-Nov-2003
-- Update:  08-Nov-2003
-- Update:  12-May-2004: Hj. Malthaner - added feature activations
----------------------------------------------------------------




------------------------------------------------------------------
-- Called if a thing triggers a trap
--
-- author: Hj. Malthaner
-- date:   08-Nov-2003
-- update: 08-Nov-2003
--
-- param thing:  type thing_t, the thing that triggered the trap
-- param x:      x position of trap
-- param y:      y position of trap
-- param type:   type of trap
------------------------------------------------------------------



function trap_triggered (thing, x, y, type)
	-- translate("Trap triggered x=" .. x .. " y=" .. y .. " type=" .. type)

	-- damage trap
	if type == 0 then
		local damage = 5 + rng_roll(2, 6)

    		thing_set_value(thing, 
                	        "HPcurr", 
                    		(thing_get_value(thing, "HPcurr") or 0) - damage)
		
		-- Let player and only player know what happened
		if(thing_get_value(thing, "is_player")) then
			translate("Something snaps your feet!")

			-- ABCGi 0.0.9
			if(damage > 12) then
				translate("It really hurts! (-" .. damage .. ")")
			elseif(damage > 8) then
				translate("It hurts! (-" .. damage .. ")")
			else 
				translate("It hurts somewhat. (-" .. damage .. ")")
			end
		end
	end


	-- poison trap ABCGi 0.0.8
	if type == 1 then
		local damage = 5 + rng_roll(2, 3)

                -- ABCGi cumulative poison added 0.0.8
                local poison = thing_get_value(user, "effect.poison") or 0
                poison = poison + damage 
                thing_set_value(thing, "effect.poison", poison)
		-- thing_set_value(thing, "effect.poison", damage)

		-- Let player know what happened ABCGi 0.0.8
		if(thing_get_value(thing, "is_player")) then
                	if (poison > damage) then
				translate("A small dart hits you. You feel even more sick. (+" .. damage .. ")")
	                else
				translate("A small dart hits you. You feel sick. (+" .. damage .. ")")
			end
		else
			-- ABCGi 0.0.9 range test, use global player pos and local x,y trap position, +y is south, +x is east
			local eastx, westx, northy, southy
			eastx = x - player_x
			westx = player_x - x
			northy = player_y - y
			southy = y - player_y

			-- hearing_near = 10 (see init.lua for this global)
			if ((eastx > hearing_near) and (eastx > westx) and (eastx > northy) and (eastx > southy)) then
				translate("You hear a distant click to the East. (" .. eastx .. ")")
 			elseif ((westx > hearing_near) and (westx > eastx) and (westx > northy) and (westx > southy)) then
 				translate("You hear a distant click to the West. (" .. westx .. ")")
			elseif ((northy > hearing_near) and (northy > eastx) and (northy > westx) and (northy > southy)) then
				translate("You hear a distant click to the North. (" .. northy .. ")")
			elseif ((southy > hearing_near) and (southy > eastx) and (southy > westx) and (southy > northy)) then
				translate("You hear a distant click to the South. (" .. southy .. ")")
			else 
				translate("You hear a nearby click!")
			end
		end
	end


	-- confusion trap
	if type == 2 then

	end


end


------------------------------------------------------------------
-- Called if a thing runs into a tree (see data/features.sects)
--
-- author: Hj. Malthaner
-- date:   12-May-2004
--
-- param thing:  type thing_t, the thing that triggered the trap
-- param n:      type number, always 0 so far 
-- return:       always 0 so far
------------------------------------------------------------------

function bump_tree(user, n)

	translate("A tree is blocking your way!")
	return 0
end


------------------------------------------------------------------
-- Called if a thing runs into a wall (see data/features.sects)
--
-- author: Hj. Malthaner
-- date:   12-May-2004
--
-- param thing:  type thing_t, the thing that triggered the trap
-- param n:      type number, always 0 so far 
-- return:       always 0 so far
------------------------------------------------------------------

function bump_wall(user, n)

	translate("A wall is blocking your way!")
	return 0
end

------------------------------------------------------------------
-- Called if a thing runs into someting immovable (see data/features.sects)
--
-- author: Hj. Malthaner
-- date:   12-May-2004
--
-- param thing:  type thing_t, the thing that triggered the trap
-- param n:      type number, always 0 so far 
-- return:       always 0 so far
------------------------------------------------------------------

function bump_immovable(user, n)

	translate("This is either fixed or too heavy to be moved.")
	return 0
end
