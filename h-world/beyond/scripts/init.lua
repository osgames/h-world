-- ABCGi 0.1.1 3/6/04 (HW 0.3.4)
-- 0.0.9 24/5/04 add player_x and player_y globals

----------------------------------------------------------------
-- init.lua
--
-- H-World script initialition script, purpose is to load
-- all required scripts
--
-- Author: Hj. Malthaner
-- Created: 20-Mar-2002
-- Last update: 28-Apr-2004
----------------------------------------------------------------

---------
-- ABCGi
---------

-- ABCGi 0.0.9
player_x = 1
player_y = 1
-- sounds heard within this are "nearby" otherwise they are "distant"
hearing_near = 10

-------
-- HAJO
-------

-- global values

-- *new* HW 0.3.4 = skin definitions, used by engine -> MUST COME FIRST OF LOAD_TILE commands
load_tile_from_image("./beyond/data/skin_menuback.png")

sym_hurt = load_tile_from_image("./beyond/data/sym_hurt.png")
sym_hunger = load_tile_from_image("./beyond/data/sym_hunger.png")
sym_thirst = load_tile_from_image("./beyond/data/sym_thirst.png")
sym_poison = load_tile_from_image("./beyond/data/sym_poison.png")
sym_frozen = load_tile_from_image("./beyond/data/sym_frozen.png")


-- read all functions

dofile("beyond/scripts/core.lua")
dofile("beyond/scripts/usages.lua")
dofile("beyond/scripts/attacks.lua")
dofile("beyond/scripts/recipes.lua")
dofile("beyond/scripts/effects.lua")
dofile("beyond/scripts/commands.lua")
dofile("beyond/scripts/traps.lua")
dofile("beyond/scripts/dialogs.lua")
-- ABCGi 0.1.1 include my common.lua functions
dofile("beyond/scripts/common.lua")

-- init values from null to zero ABCGi 0.0.8 21/5/04 not needed if use or 0
-- thing_set_value(player, "effect.poison", 0)


_ALERT("Lua: init.lua done\n")
 