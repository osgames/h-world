This are the release notes for H-World 0.0.4


Since the last release a few more features have been finished:

- extended player visual connector: the player character image now displays
  if the following equipment is worn:
  + shoes
  + trousers 
  + belt
  + jacket
  + shield
  + weapon
- added new items: leather shoes, leather pants, leather belt, leather jacket
- basic Lua scripting support added: items can be configured that using them
  calls a Lua function, that can both affect the users and the items 
  properties. Currently only one script for a healing potions exists.
- changed the display so that the ground looks less tiled
- changed to another lighting method to make the lit area look less tiled
- added delays/durations for actions


For more info, please read the readme.txt file and the release notes
of H-World 0.0.2 and 0.0.3

The player can't die in this release, if one of BP or CP drop to 0
the game will restore them to 9999.


**
Chronological list of changes since the last release:


16-Jun-02  NEW: new items: leather shoes, leather pants, leather belt,
                leather jacket
           NEW: expanded player_visual_connector to modify player
                character image according to worn equipment: newly
                recognised item types: shoes, trousers, belts, jackets
           NEW: concept art for leather shoes, leather pants, leather belt,
                leather jacket
           FIX: fixed a clipping bug in triangle rendering
           TIME: 2:30 h

08-Jun-02: NEW: started work on thing inspector
           TIME: 0:30 h

05-Jun-02: NEW: continued to work on map window
           TIME: 1:00 h

03-Jun-02: NEW: started to work on map window
           TIME: 2:00 h

02-Jun-02: CHANGE: unified lighting for walls and floors
           FIX: fixed a bug in field of view code
           TIME: 1:00 h

31-May-02: CHANGE: tested alternative light falloff calculation for 
                   shaded triangles. It's slower than before but looks 
                   better
           TIME: 0:30 h

29-May-02: CHANGE: reworked triangle rendering - it can now render arbitrary 
                   large triangles
           TIME: 1:00 h

26-May-02: CHANGE: changed image format from PPM to PNG
	   FIX: fixed a bug in triangle shading code
           TIME: 2:30 h
 
25-May-02: CHANGE: prepared floor tiles to use image list from world_view_t
                   class.
	   TIME: 0:30 h

20-May-02: CHANGE: changed shading from tables to calculations. This saves
                   about 7*32K of memory, and seems to be equally fast.
           FIX: objects on features (i.e. open doors) are now displayed
                again
	   TIME: 1:30 h

16-May-02: NEW: shaded triangle rendering
	   TIME: 1:30 h

06-Apr-02: FIX: fixed open door alignment
	   TIME: 0:30 h

05-Apr-02: CHANGE: moved copyright message from log panel to stats panel
           NEW: images for the stone club "Crusher"
           CHANGE: improved a few image icons
	   TIME: 0:45 h

04-Apr-02: NEW: time delays added to all actions
           CHANGE: Piercing damage removed. Pierce is now a scalar value
                   to determine armor penetration. There are only blunt
                   and cutting damage left. All formerly piercing damage
                   is now cutting damage with a high piercing value
           FIX: usage panel displays item names now with upper case first char 
           TIME: 1:00 h

03-Apr-02: NEW: scheduler prepared for real timeline
           TIME: 0:30 h

02-Apr-02: NEW: item chooser window
           TIME: 1:00 h

30-Mar-02: CHANGE: Improved monster movement. Even non-intelligent monsters
                   can now move around small obstacles, and better surround
                   the target if attacking in a group.

27-Mar-02: NEW: usages now can delete things (i.e. quaffing a potion deletes
                the potion).
           NEW: usage panel extended for useable things UI control.
	   FIX: fixed a bug in drag&drop handling in inventory view.
           TIME: 2:00 h

25-Mar-02: NEW: 'equip from ground' functionality for pickup usage added
           TIME: 1:00 h

24-Mar-02: FIX: extensions and bugfix for Lua bridging code
           TIME: 1:00 h


written by Hansj�rg Malthaner, June 2002
Email: hansjoerg.malthaner@gmx.de
