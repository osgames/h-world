This are the release notes for H-World 0.1.0

(Please also read the former release notes!)

Since the last release two major features have been finished:

- Saving/loading of games is possible. This allows to start a game,
  save it and continue later on.
- Items can now have have prefixes that determine special effects and 
  abilities.
  * battlesome: raises max HP by 1-5
  * heroic: raises max HP by 6-10
  * glowing: raises light radius by 1-3
  * shining: raises light radius by 4-6
  * protective: raises the blocking chance by 1-5
  * hard: raises protection against blunt damage by 1-5
  * solid: raises protection against cutting damage by 1-5
  (In H-World 0.1.0 all items have prefixes for testing purposes.
   In future releases only some items will have prefixes.)

The version jump from 0.0.7 to 0.1.0 is because of the ability to load
and save games which is a core feature IMO. Lost of minor details have
ben imporved, too, check the change log below for a complete list of
changes and additions.


A few minor changes:

- Cave dungeon graphics completely reworked
- All walls are much lower now. Thus the interior of rooms is much better
  visible. Looks a bit strange first, but plays well.
- Attack star size now reflects damage. Different colors for player and 
  monster attacks.
- Graphics/display code has become slower due to the preparations for 
  multi-player network/client-server architecture.
- Inventory and birth options windows can now be closed by pressing escape


For more info, please read the readme.txt file and the release notes
of H-World 0.0.2, 0.0.3, 0.0.4, 0.0.5, 0.0.6 and 0.0.7

The player can't die in this release, if the hitpoints (HP) drop to 0
the game will restore them to 9999.


**
Chronological list of changes since the last release:


14-Aug-02: FIX: Monster attacks are now restored properly when loading a
                saved game.
           CHANGE: improved 'teleporter' graphics
	   released H-World 0.1.0
           TIME: 2:00 h

13-Aug-02: NEW: different sized 'attack stars' to display approximate damage
                (steps of 3 damage points)
           NEW: usages and subclasses are now persistent, too.
           CHANGE: improved some graphics (cave wall, cave floor, teleporter)
           TIME: 3:00 h

12-Aug-02: NEW: finished code for loading/saving a game
           NEW: different colored 'attack stars' for player/monster hits
           TIME: 2:00 h

11-Aug-02: NEW: Inventory view can be closed with ESC
           FIX: fixed a bug in window closing code
           CHANGE: changed UI layout, moved message area to top
           FIX: fixed many problems in memory management
           TIME: 8:00 h

10-Aug.02: NEW: more item prefixes: glowing, shining, protective, 
                hard and solid.
           FIX: fixed a bug in lua scripts for wielding/taking off items
                with prefixes
           CHANGE: tested 'low' walls instead of transparent walls
           TIME: 2:30 h
 
09-Aug-02: NEW: continued work on item prefixes. Now fully user
                configurable
           TIME: 1:00 h

08-Aug-02: NEW: continued work on item prefixes. Included lua scripts
                to calculate effects on wielding and unwielding items.
                Started to externalize prefix data to "data/prefixes.tab"
                and "data/types_prefixes.tab"
           TIME: 1:00 h

07-Aug-02: NEW: started wotk on item prefixes. HP affecting prefixes
                'battlesome' and 'heroic'
           TIME: 0:30 h

05-Aug-02: CHANGE: optimized graphics/display code a bit
           TIME: 0:30 h

04-Aug-02: CHANGE: started to separate client and server world model
                   to allow migration to a client server system more
                   easily later on.
           FIX: fixed some flaws in reference counting and some
                container templates
           TIME: 5:00 h

31-Jul-02: NEW: continued work on persistence layer
           FIX: improved reference counting template
           TIME: 1:30 h

30-Jul-02: NEW: continued work on persistence layer
           TIME: 1:30 h

29-Jul-02: NEW: continued work on persistence layer
           TIME: 1:00 h


written by Hansj�rg Malthaner, Aug. 2002
Email: hansjoerg.malthaner@gmx.de
