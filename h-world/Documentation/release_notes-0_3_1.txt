This are the release notes for H-World 0.3.1


The main reason for this release are bug fixes. Some serious
problems in accessing the main memory have been found and solved.
The problems could've caused serious stability problems, so I think
a new release is urgently neccesary.

Besides the bug fixes mostly internal changes have been made to enhance
the extensibility and customizability of the H-World engine core.

The most notably change in gameplay is probably the new handling of
items with multiple usages. Instead of cluttering the usage panel with
all item/usage combinations, only the items are listed and usages are
chosen in a second step, after the player chose to use an item.

The same approach is used for items used from the inventory view.


**
Chronological list of changes since the last release:

08-May-04: FIX: fixed an illegal memory access in tile loading code
           FIX: flower patch image caused an illegal memory write
           FIX: fountain image caused an illegal memory write
           FIX: item chooser window height is now calculated correctly
           NEW: included new knife images by Radomir "Sheep" Dopieralski

07-May-04: CHANGE: updated castle wall images with raytraced brick
                   walls
           FIX: fixed some accesses to uninitialized variables
                during saving in action_speak_t and feature_door_t
 
06-May-04: NEW: thing factory can now equip PC/NPCs with item stacks
           CHANGE: moved "check_item_constraints" into core.lua
                   so that game designers can implement their own
                   size/type checks
           CHANGE: changed birth frame to support more than 6 players
                   (untested)
           CHANGE: player names are now centered in birth frame

05-May-04: CHANGE: attack type usages are now filtered out before
                   offering item usages through the 'u'se function
           CHANGE: included improved fountain image

04-May-04: CHANGE: using items with multiple usages now is done by 
                   a popup dialog, both from the usage panel and the
                   inventory view

03-May-04: NEW: code can now create short names and long names 
                (including all magical attributes) for items via
                functions in core.lua
                -> changed usage panel to use short names

02-May-04: FIX: another fix for wielding/unwielding multi-limb items
                with special attributes


written by Hansj�rg Malthaner, May 2004
Email: hansjoerg.malthaner@gmx.de
