Overview
--------

The goal of the H-World project is to create a game engine to build
simple computer role playing games. H-World is currently in beta 
stage. This means the basics are complete but many features are still
in development - i.e. the magic system is mostly missing. Please read
the release notes and the history.txt file for further information
about the current state of development.


The windows version should run with all Windows versions, from Windows 95
and newer.

The Linux version needs glibc 2.1 and the SDL library 1.2 installed.
See http://www.libsdl.org for details about SDL.


The example game "The Jungle" 
-----------------------------

That's what you currently play if you start H-World without 
parameters.

To win the game, you must kill the skeleton captain in the ruined
castle cellars. The game is lost, if your hitpoints drop below 0.

It is hard to win. Choose your equipment carefully. Try to get
followers and pets to help.


Estimated min. system requirements
----------------------------------

- Pentium 200MHz or better
- 16 bit SVGA in 800x600
- 16 MB ram
- 6 MB HD space


Recommended system requirements
----------------------------------

- Pentium 350MHz or better
- 16 bit SVGA in 800x600
- 32 MB ram
- 6 MB HD space

The fonts and images are designed for a resolution of 800x600 or
1024x768 on a 15" or 17" screen. If you use larger resolutions or
smaller screens, the fonts will be difficult to read.


Keyboard configuration
----------------------

see: "help/keyboard.html"


A few words about monsters and combat
-------------------------------------

see: "help/combat.html"


The tiny little howto section
-----------------------------

see: "help/howto.html"


Command line options
--------------------

-screensize WxH (i.e. -screensize 900x600) 

 sets the window size to W x H pixels


-game <path> 

 loads configuration files and images from <path>
 If someone creates a new game for the H-World engine he can
 store all files for his game in that subdirectory and the player
 can select different games using this option


-level <filename>

 loads a certain level. Useful for testing single levels (also
 might be used for cheating ...)

-fullscreen

 tries to run the game in fullscreen mode


written by Hansj�rg Malthaner, Dec. 2001
updated by Hansj�rg Malthaner, Apr. 2004

Email: hansjoerg.malthaner@gmx.de
