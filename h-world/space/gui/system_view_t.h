#ifndef system_view_t_h
#define system_view_t_h

#include "swt/gui_button_t.h"
#include "swt/ifc/action_listener_t.h"

#include "gui/escapable_frame_t.h"
#include "space/model/sunsys_t.h"
#include "space/model/vec.h"

#include "tpl/minivec_tpl.h"


#include "galaxy_view_t.h"

class image_t;
class system_view_t;

class circle_view_t : public gui_component_t
{

  double scale;
  double xoff;
  double yoff;

  sunsys_t ** model;
  image_t ** planet_img;
  image_t ** sun_img;

  void show_sunsys(graphics_t *gr,
		   koord windowpos,
		   double xoff,
		   double yoff,
		   const sunsys_t *sys);


 public:

  VEC target;
  const sunsys_t *target_sys;

  void set_model(sunsys_t **);

  sunsys_t ** const get_model() const {return model;};

  circle_view_t(image_t ** p, image_t ** s, sunsys_t **model);

  void draw(graphics_t *gr, koord pos);

  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  bool process_event(const event_t &ev);
};


class systable_view_t : public gui_component_t
{
  class lookup {
  public:
    koord pos;
    const sunsys_t *sys;
    lookup() {sys=0;};

    lookup(koord k, const sunsys_t *s) : pos(k), sys(s) {};
  };



  sunsys_t ** model;
  image_t ** planet_img;
  image_t ** sun_img;


  /**
   * Remember last user selection
   * @author Hj. Malthaner
   */
  const sunsys_t * current;


  void show_sunsys(graphics_t *gr,
		   koord windowpos,
		   koord parentpos,
		   koord zv,
		   const sunsys_t *sys);



  /**
   * Save positions while drawing for lookup later on
   * @author Hj. Malthaner
   */
  minivec_tpl <lookup> positions;


 public:

  void set_model(sunsys_t **);

  systable_view_t(image_t ** p, image_t ** s, sunsys_t **model);

  void draw(graphics_t *gr, koord pos);

  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  bool process_event(const event_t &ev);
};



class system_view_t : public escapable_frame_t, public action_listener_t
{
 private:

  image_t * planet_img[sunsys_t::P_MAX];
  image_t * sun_img[sunsys_t::S_MAX];


  circle_view_t circles;
  systable_view_t table;
  galaxy_view_t galaxy;

  gui_button_t bt_toggle_current;
  gui_button_t bt_tabular;
  gui_button_t bt_circles;
  gui_button_t bt_land;
  gui_button_t bt_start;
  gui_button_t bt_go;


 public:

  system_view_t(koord size);
  ~system_view_t();


  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  bool process_event(const event_t &ev);


  virtual void action_triggered(gui_action_trigger_t * trigger);

};

#endif // system_view_t_h
