/* 
 * galaxy_view_t.h
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#ifndef galaxy_view_t_h
#define galaxy_view_t_h

#include "swt/gui_component_t.h"

class system_view_t;
class image_t;

template <class T> class slist_tpl;


class galaxy_view_t : public gui_component_t
{
 private:

  class sys_info_t {
 public:
    int xoff, yoff;   // Hajo: Offset inside sector
    int xpos, ypos;   // Hajo: Position on map
    int seed;         // Hajo: System RNG seed
  };


  slist_tpl <sys_info_t> *sys_info_list;

  int zoom;
  koord center;


  image_t ** sun_img;
  system_view_t *system_view;


  /*
   * Get nearest solar system
   * @author Hj. Malthaner
   */
  sys_info_t gimme_gala_sys(int x, int y) const;


  /*
   * Each galactical sector contains up to 6 solar systems
   * @author Hj. Malthaner
   */
  void show_gala_sek(graphics_t *gr, 
		     int xpos, 
		     int ypos, 
		     int i, 
		     int j);

 public:


  galaxy_view_t(system_view_t *, image_t ** s);
  ~galaxy_view_t();

  void draw(graphics_t *gr, koord pos);

  /**
   * Events werden hiermit an die GUI-Komponenten
   * gemeldet
   * @author Hj. Malthaner
   */
  bool process_event(const event_t &ev);
};

#endif // galaxy_view_t_h
