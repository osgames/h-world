#include <stdio.h>
#include <math.h>

#include "system_view_t.h"

#include "swt/graphics_t.h"
#include "swt/color_t.h"
#include "swt/event_t.h"
#include "swt/image_t.h"
#include "swt/font_t.h"

#include "fontdef.h"

#include "language/linguist_t.h"

#include "space/control/landing_t.h"
#include "space/model/game_globals_t.h"
#include "space/model/galaxy_t.h"
#include "space/util/textutil.h"


#define PATH_PRE "./hspace/data/"


void circle_view_t::set_model(sunsys_t ** m)
{
  model = m;
}


circle_view_t::circle_view_t(image_t ** p, image_t ** s, sunsys_t ** m)
  : model (m)
{
  planet_img = p;
  sun_img = s;

  scale = 1E-6;
  xoff = 0.0;
  yoff = 0.0;

  target_sys = 0;
}


void
draw_planetenbahn(graphics_t *gr,
                  int xpos,int ypos, int rad, color_t color)
{
    double u;

    for(u=0; u <= 2*M_PI; u += M_PI/128) {
	gr->draw_pixel(xpos + (int)(cos(u)*rad), 
		       ypos + (int)(sin(u)*rad), 
		       color);
    }
}


void circle_view_t::show_sunsys(graphics_t *gr,
				koord windowpos,
				double xoff,
				double yoff,
				const sunsys_t *sys) 
{
  // printf("%s %f %f\n", sys->get_name().chars(), xoff, yoff);


  // Hajo: too small bodies shouldn't be displayed
  if(sys->btype == sunsys_t::B_SUN || 
     sys->get_bahnrad() * scale > 10.0) {

    int i;

    koord center = windowpos + get_size() / 2; 
    
    const int w = 2 + (int)(sys->get_rad() * scale);

    if(sys->get_parent()) {
      gr->draw_image(planet_img[sys->ptype],
		     center.x - w/2 + (int)(xoff * scale), 
		     center.y - w/2 + (int)(yoff * scale), 
		     w, 
		     w,
		     0); 
    } else {
      gr->draw_image(sun_img[sys->stype],
		     center.x - w/2 + (int)(xoff * scale), 
		     center.y - w/2 + (int)(yoff * scale), 
		     w, 
		     w,
		     0); 
    }

    const font_t * font = gr->get_font();
    font->draw_string(gr, sys->get_name().chars(),
		      center.x + (int)(xoff * scale), 
		      center.y + (int)(yoff * scale), 
		      0, color_t::WHITE);


    for(i=0; i<sys->get_child_count(); i++) {    
      
      const sunsys_t * body = sys->get_child(i);
      
      draw_planetenbahn(gr,
			center.x + (int) (xoff * scale), 
			center.y + (int) (yoff * scale), 
			(int) (body->get_bahnrad() * scale), 
			color_t::GREY192 /* body->color */); 
      
      show_sunsys(gr,
		  windowpos,
		  (xoff + body->pos.x),
		  (yoff + body->pos.y),
		  body);

    }
  }
}


void circle_view_t::draw(graphics_t *gr, koord pos)
{
  static color_t d_mark (100, 255, 100);

  pos += get_pos();

  const font_t * font = gr->get_font();
  font->draw_string(gr, "Click mouse to center map", 
		  pos.x+10, pos.y+10, 0, color_t::WHITE);
  font->draw_string(gr, "+ : Zoom in", pos.x+10, pos.y+23, 0, color_t::WHITE);
  font->draw_string(gr, "- : Zoom out", pos.x+10, pos.y+36, 0, color_t::WHITE);
  font->draw_string(gr, "d : Set destination", 
		  pos.x+10, pos.y+49, 0, color_t::WHITE);

  font->draw_string(gr, "Current destination:", 
		  pos.x+10, pos.y+70, 0, color_t::WHITE);

  if(target_sys) {
    font->draw_string(gr, target_sys->get_name(), 
		    pos.x+120, pos.y+70, 0, color_t::WHITE);
    
  } else {
    font->draw_string(gr, "Position", 
		    pos.x+120, pos.y+70, 0, color_t::WHITE);

  }


  show_sunsys(gr, pos, xoff, yoff, *model);  


  const koord center = pos + get_size() / 2; 

  
  gr->fillbox_wh(center.x - 2 + (int)((xoff+target.x)*scale), 
		 center.y - 2 + (int)((yoff+target.y)*scale), 
		 4, 
		 4,
		 d_mark); 

}


bool circle_view_t::process_event(const event_t &ev)
{
  bool changed = false;

  if(ev.type == event_t::KEY_PRESS) {
    switch(ev.code) {
    case '+':
      scale += scale / 6;
      changed = true;
      break;
    case '-':
      scale -= scale / 6;
      changed = true;
      break;
    case 'd':
      {
	target = VEC(-xoff, -yoff, 0.0);

	koord center = get_pos() + get_size()/2;

	target.x += (ev.mpos.x-center.x) / (scale);
	target.y += (ev.mpos.y-center.y) / (scale);


	// printf("scale=%f\n", scale);

	target_sys = (*model)->find_in_range(target, 18.0/scale );

	changed = true;
      }
    }
  }

  if(ev.type == event_t::BUTTON_PRESS) {
    if(ev.code == event_t::BUTTON_LEFT) {

      if(ev.mpos.y > get_pos().y + 16) { 
	koord center = get_pos() + get_size()/2;

	xoff -= (ev.mpos.x-center.x) / (scale);
	yoff -= (ev.mpos.y-center.y) / (scale);

	changed = true;
      }
    }
  }

  if(changed) {
    redraw();
  }

  return changed;
}




void systable_view_t::set_model(sunsys_t **m)
{
  model = m;
}


systable_view_t::systable_view_t(image_t ** p, image_t ** s, sunsys_t ** m)
  : model (m)
{
  planet_img = p;
  sun_img = s;

  current = 0;
}


void systable_view_t::show_sunsys(graphics_t *gr,
				  koord windowpos,
				  koord parentpos,
				  koord zv,
				  const sunsys_t *sys) 
{
  const double scale = 0.0012;
  const int w = 2 + (int)(sys->get_rad() * scale);

  const int spacing = get_size().x / 66;

  const font_t * font = gr->get_font();

  // Hajo: determine bounding box width
  const int step = spacing + w;
  const int strw = spacing + font->get_string_width(sys->get_name(), 0);
  const int bwidth = step < strw ? strw : step;


  const koord pos ( windowpos.x + parentpos.x,
		    windowpos.y + parentpos.y-w/2 );


  if(sys->get_parent() == 0) {
    positions.clear();

    gr->draw_image(sun_img[sys->stype],
		   pos.x + (bwidth - w)/2,
		   pos.y,
		   w, 
		   w,
		   0); 
  } else {
    gr->draw_image(planet_img[sys->ptype],
		   pos.x + (bwidth - w)/2,
		   pos.y,
		   w, 
		   w,
		   0); 
  }

  positions.append(lookup(pos + koord((bwidth - w)/2 + w/2, w/2-16), sys));

  font->draw_string(gr, sys->get_name().chars(),
		  pos.x + (bwidth - strw+spacing)/2,
		  pos.y - 13,
		  0, color_t::WHITE);

    
  koord k = parentpos;

  if(zv.x != 0) {
    k.x += bwidth;
  } else {
    k.y += zv.y + w/2;
  }


  for(int i=0; i<sys->get_child_count(); i++) {    
      
    const sunsys_t * body = sys->get_child(i);

    const int step = spacing + 2 + (int)(body->get_rad() * scale);
    const int strw = spacing + font->get_string_width(body->get_name(), 0);
    const int bwidth = step < strw ? strw : step;
    

    // printf("%s %d %d\n", body->get_name().chars(), k.x, bwidth);
    
    show_sunsys(gr,
		windowpos,
		k,
		koord(zv.y/2, zv.x/2),
		body);

    
    if(zv.x != 0) {
      k.x += bwidth;
    } else {
      k.y += step+6;
    }
  }
}


void systable_view_t::draw(graphics_t *gr, koord pos)
{
  const font_t * font = gr->get_font();

  pos += get_pos();
  show_sunsys(gr, pos, 
	      koord(120 - (int)( (*model)->get_rad()*0.0012), 120), 
	      koord(get_size().x / 12, 0), *model);  


  char buf[256];

  if(current) {

    // Show planet details

    font->draw_string(gr, current->get_name(), 
		      pos.x+200, pos.y+398, 0, color_t::WHITE);


    if(current->btype == sunsys_t::B_PLANET) {

      font->draw_string(gr, sunsys_t::planet_desc[current->ptype],
		      pos.x+200, pos.y+413, 0, color_t::WHITE); 

      
      sprintf(buf, "Radius: %s km", dt_int(current->get_rad()));
      font->draw_string(gr, buf,
		      pos.x+200, pos.y+426, 0, color_t::WHITE); 

      sprintf(buf, "Temperature: %s k", dt_int(current->get_eet()));
      font->draw_string(gr, buf,
		      pos.x+200, pos.y+439, 0, color_t::WHITE); 
      
    }
    if(current->btype == sunsys_t::B_STATION) {
      sprintf(buf, "A space station with less than 1000 inhabitants");
      font->draw_string(gr, buf,
		      pos.x+200, pos.y+413, 0, color_t::WHITE); 

    }
    
  } else {

    // Show system summary

    font->draw_string(gr, (*model)->get_name(), 
		    pos.x+200, pos.y+398, 0, color_t::WHITE);
    
    font->draw_string(gr, sunsys_t::sun_desc[(*model)->stype],
		     pos.x+200, pos.y+413, 0, color_t::WHITE); 

  }
}


bool systable_view_t::process_event(const event_t &ev)
{
  bool swallowed = false;

  if(ev.type == event_t::BUTTON_PRESS) {
    if(ev.code == event_t::BUTTON_LEFT) {
      swallowed = true;

      int dist = 999;
      const sunsys_t * best = 0;

      for(int i=0; i<positions.count(); i++) { 
	const koord zv = ev.mpos + get_screenpos() - koord(0,16) - positions.get(i).pos;

	const int d = zv.x*zv.x + zv.y*zv.y;

	if(d < dist) {
	  dist = d;
	  best = positions.get(i).sys;
	}
      }
      
      // printf("%s clicked\n", best->get_name().chars());

      if(best != current) {
	current = best;
	redraw();
      }
    }
  }

  return swallowed;
}



system_view_t::system_view_t(koord size) : 
  escapable_frame_t("System information", true), 
  circles(planet_img, sun_img, &game_globals_t::h_space.current_system), 
  table(planet_img, sun_img, &game_globals_t::h_space.current_system),
  galaxy(this, sun_img),
  bt_toggle_current("(v) View hyperjump destination system"),
  bt_tabular("(i) System information"),
  bt_circles("(n) System navigation"),
  bt_land("(l) Land/dock ship"),
  bt_start("(g) Galactical map"),
  bt_go("(h) Hyperjump")
{
  set_size(size - koord(0, 60));
  set_pos(koord(0, 60));

  set_font(font_t::load(FONT_SML));

  game_globals_t::h_space.current_system = 
    new sunsys_t(galaxy_t::calc_sec_number(0 ,0), true);

  planet_img[0] = new image_t(PATH_PRE "t_bare_rock.png");
  planet_img[1] = new image_t(PATH_PRE "t_ice.png");
  planet_img[2] = new image_t(PATH_PRE "t_atm_rock.png");
  planet_img[3] = new image_t(PATH_PRE "t_venus.png");
  planet_img[4] = new image_t(PATH_PRE "t_earth.png");
  planet_img[5] = new image_t(PATH_PRE "t_small_gas.png");
  planet_img[6] = new image_t(PATH_PRE "t_big_gas.png");
  planet_img[7] = new image_t(PATH_PRE "t_rings.png");
  planet_img[8] = new image_t(PATH_PRE "t_station.png");


  sun_img[0] = new image_t(PATH_PRE "s_yel.png");
  sun_img[1] = new image_t(PATH_PRE "s_ora.png");
  sun_img[2] = new image_t(PATH_PRE "s_whi.png");
  sun_img[3] = new image_t(PATH_PRE "s_red.png");
  sun_img[4] = new image_t(PATH_PRE "s_blu.png");


  const int b_yoff = get_size().y - 44;


  bt_toggle_current.set_size(koord(200,18));
  bt_toggle_current.set_pos(koord(50,b_yoff-24));
  bt_toggle_current.add_listener(this);
  bt_toggle_current.set_font(get_font());
  add_component(&bt_toggle_current);

  bt_tabular.set_size(koord(110,18));
  bt_tabular.set_pos(koord(50,b_yoff));
  bt_tabular.add_listener(this);
  add_component(&bt_tabular);

  bt_circles.set_size(koord(110,18));
  bt_circles.set_pos(koord(200,b_yoff));
  bt_circles.add_listener(this);
  add_component(&bt_circles);

  bt_land.set_size(koord(110,18));
  bt_land.set_pos(koord(350,b_yoff));
  bt_land.add_listener(this);
  add_component(&bt_land);

  bt_start.set_size(koord(110,18));
  bt_start.set_pos(koord(500,b_yoff));
  bt_start.add_listener(this);
  add_component(&bt_start);

  bt_go.set_size(koord(110,18));
  bt_go.set_pos(koord(650,b_yoff));
  bt_go.add_listener(this);
  add_component(&bt_go);


  circles.set_pos(koord(5,5));
  circles.set_size(koord(size.x-10, b_yoff-26));

  galaxy.set_pos(koord(5,5));
  galaxy.set_size(koord(size.x-10, b_yoff-26));

  table.set_pos(koord(5,5));
  table.set_size(koord(size.x-10, b_yoff-26));
  add_component(&table);


  set_background(0);
  set_color(color_t::BLACK);
  set_visible(true);
}


system_view_t::~system_view_t()
{
  delete planet_img[0];
  delete planet_img[1];
  delete planet_img[2];
  delete planet_img[3];
  delete planet_img[4];
  delete planet_img[5];
  delete planet_img[6];
  delete planet_img[7];
}


/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool system_view_t::process_event(const event_t &ev)
{
  bool swallowed = escapable_frame_t::process_event(ev);

  if(!swallowed) {
    if(ev.type == event_t::KEY_PRESS) {
      switch(ev.code) {

        case 0:
        case 27:
        case '+':
        case '-':
          break;
          
        case 'v':
          action_triggered(&bt_toggle_current);
          break;
        case 'i':
          action_triggered(&bt_tabular);
          break;
        case 'n':
          action_triggered(&bt_circles);
          break;
        case 'l':
          action_triggered(&bt_land);
          break;
        case 'g':
          action_triggered(&bt_start);
          break;
        case 'h':
          action_triggered(&bt_go);
          break;
      }
    }
  }

  return true;
}



void system_view_t::action_triggered(gui_action_trigger_t * trigger)
{
  if(trigger == &bt_circles) {
    remove_component(&circles);
    remove_component(&table);
    remove_component(&galaxy);
    add_component(&circles);
    
    redraw();
  } else if(trigger == &bt_tabular) {
    remove_component(&circles);
    remove_component(&table);
    remove_component(&galaxy);
    add_component(&table);
    
    redraw();
  } else if(trigger == &bt_start) {
    remove_component(&circles);
    remove_component(&table);
    remove_component(&galaxy);

    add_component(&galaxy);
    
    redraw();
  } else if(trigger == &bt_land) {

    if( circles.target_sys ) {

      if(circles.target_sys->btype == sunsys_t::B_STATION) {
	linguist_t::translate("You navigate the ship into the space stations docking area.");
	
      } else {
	linguist_t::translate("You try to land the ship on the planets surface.");	
      }

      landing_t l;
      bool ok = l.land( circles.target_sys );
      
      if(!ok) {
	linguist_t::translate("There is no suitable ground to land the ship.");	
      } else {
	linguist_t::translate("You've landed the ship successfully.");	
      }

      close();
    } else {
      linguist_t::translate("You must select a destination before you can land the ship.");
    }

  } else if(trigger == &bt_go) {
    sunsys_t *current = game_globals_t::h_space.hyperjump_destination;

    if(current) {

      game_globals_t::h_space.current_system = current;
      game_globals_t::h_space.hyperjump_destination = 0;

      circles.set_model(&game_globals_t::h_space.current_system);
      table.set_model(&game_globals_t::h_space.current_system);

      // Hajo: change to tabular view
      action_triggered(&bt_tabular);

    } else {
      linguist_t::translate("No hyperjump destination set.");
    }

  } else if(trigger == &bt_toggle_current) {

    if(circles.get_model() == &game_globals_t::h_space.hyperjump_destination) {
      bt_toggle_current.set_text("(v) View hyperjump destination system");

      circles.set_model(&game_globals_t::h_space.current_system);
      table.set_model(&game_globals_t::h_space.current_system);
    } else {
      if(game_globals_t::h_space.hyperjump_destination) {
        bt_toggle_current.set_text("(v) View current location");

	circles.set_model(&game_globals_t::h_space.hyperjump_destination);
	table.set_model(&game_globals_t::h_space.hyperjump_destination);
      } else {
	linguist_t::translate("No hyperjump destination set.");
      }
    }

    redraw();
  }
}
