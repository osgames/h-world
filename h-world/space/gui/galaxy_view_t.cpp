/* 
 * galaxy_view_t.cpp
 *
 * Copyright (c) 2003 - 2004 Hansj�rg Malthaner
 *
 * This file is part of the H-World project and may not be used
 * in other projects without written permission of the author.
 */

#include "swt/font_t.h"
#include "swt/graphics_t.h"
#include "swt/painter_t.h"
#include "swt/color_t.h"
#include "swt/event_t.h"
#include "swt/image_t.h"

#include "galaxy_view_t.h"
#include "system_view_t.h"

#include "language/linguist_t.h"

#include "util/rng_t.h"

#include "space/model/galaxy_t.h"
#include "space/model/sunsys_t.h"
#include "space/model/game_globals_t.h"
#include "space/util/textutil.h"

#include "tpl/slist_tpl.h"

galaxy_view_t::galaxy_view_t(system_view_t *v, image_t **s)
{
  zoom = 1;

  sun_img = s;

  system_view = v;

  sys_info_list = new slist_tpl <sys_info_t> ();
}


galaxy_view_t::~galaxy_view_t()
{
  delete sys_info_list;
  sys_info_list = 0;
}


/*
 * Get nearest solar system
 * @author Hj. Malthaner
 */
galaxy_view_t::sys_info_t galaxy_view_t::gimme_gala_sys(int x, int y) const
{
  slist_iterator_tpl <sys_info_t> iter (sys_info_list);
  unsigned int distance = 99999999; 
  sys_info_t best;

  x += get_screenpos().x;
  y += get_screenpos().y;

  while( iter.next() ) {
    const sys_info_t &inf = iter.get_current();

    unsigned int d = (inf.xpos - x) * (inf.xpos - x) + 
                     (inf.ypos - y) * (inf.ypos - y);


    if(d < distance) {
      distance = d;
      best = inf;
    }
  }
  
  return best;
}


/*
 * Each galactical sector contains up to 6 solar systems
 * @author Hj. Malthaner
 */
void galaxy_view_t::show_gala_sek(graphics_t *gr, 
				  int xpos, 
				  int ypos, 
				  int i, 
				  int j)

{                  
  const int seed = galaxy_t::calc_sec_number(i ,j);
  painter_t pt (gr);

  pt.box_wh(koord(xpos,ypos), koord(120/zoom,120/zoom), color_t::GREY64 );
    
  rng_t rng (seed); 
               
  const int anz = rng.get_int(6);

  for(int n=0; n<anz; n++) {                                        
    const int x = xpos+rng.get_int(120)/zoom;
    const int y = ypos+rng.get_int(120)/zoom;

    sys_info_t inf;

    inf.xoff = x-xpos;
    inf.yoff = y-ypos;
    inf.xpos = x;
    inf.ypos = y;
    // inf.seed = seed + n;
    inf.seed = rng.get_int(0xFFFF);

    sys_info_list->append(inf);

    sunsys_t sunsys (inf.seed, false);


    const int rad = (sunsys.get_rad() >> 20) + 2;

    gr->draw_image(sun_img[sunsys.stype], x-rad, y-rad, rad+rad+1, rad+rad+1, 0);
    
    const font_t * font = gr->get_font();
    font->draw_string(gr, sunsys.get_name(), x+2, y+2, 0, color_t::WHITE);
  }
}


void galaxy_view_t::draw(graphics_t *gr, koord pos)
{
  char buf [256];

  const font_t * font = gr->get_font();
  font->draw_string(gr, "2,4,6,8 : Scroll map", pos.x+10, pos.y+10, 0, color_t::WHITE);
  font->draw_string(gr, "+,- : Zoom in/out", pos.x+10, pos.y+22, 0, color_t::WHITE);
  font->draw_string(gr, "d: set hyperjump destination", pos.x+10, 0, pos.y+34, color_t::WHITE);

  font->draw_string(gr, "Sektor:", pos.x+10, pos.y+60, 0, color_t::WHITE);
  sprintf(buf,"[%d,%d]", center.x, center.y);
  font->draw_string(gr, buf, pos.x+55, pos.y+60, 0, color_t::WHITE);
  
  // Hajo: list is filled in show_gala_sek()
  sys_info_list->clear();

  koord size = get_size();

  for(int y = center.y-zoom*2; y <= center.y+zoom*2; y++) {
    for(int x = center.x-zoom*3; x <= center.x+zoom*3; x++) {
      show_gala_sek(gr, 
		    pos.x + size.x/2 + ((x-center.x)*120-60)/zoom, 
		    pos.y + size.y/2 + ((y-center.y)*120-60)/zoom,
		    x,
		    y);
    }
  }
}


/**
 * Events werden hiermit an die GUI-Komponenten
 * gemeldet
 * @author Hj. Malthaner
 */
bool galaxy_view_t::process_event(const event_t &ev)
{
  if(ev.type == event_t::KEY_PRESS) {
    switch(ev.code) {
    case '+': 
      if(zoom > 1)
	zoom -= 1;
      break;
    case '-': 
      if(zoom < 4)
	zoom += 1;
      break;
    case '4':
      center.x -= 1;
      break;
    case '6':
      center.x += 1;
      break;
    case '8':
      center.y -= 1;
      break;
    case '2':
      center.y += 1;
      break;
    }

    redraw();
  }

  if((ev.type == event_t::BUTTON_PRESS && ev.code == event_t::BUTTON_LEFT) 
     || (ev.type == event_t::KEY_PRESS && ev.code == 'd')) {

    sunsys_t * sunsys = 0;
    
    sys_info_t inf = gimme_gala_sys(ev.mpos.x, ev.mpos.y);

    sunsys = new sunsys_t(inf.seed, true);

    delete(game_globals_t::h_space.hyperjump_destination);

    game_globals_t::h_space.hyperjump_destination = sunsys;


    {
      rng_t rng;
      char buf [256];

      rng.set_seed(inf.seed);
      rng.get_int(10);
      sprintf(buf, "Setting hyperjump destination to %s.", gen_name(&rng));

      linguist_t::translate(buf);
    }
  }

  return true;
}
