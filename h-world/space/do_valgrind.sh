#! /bin/bash

valgrind --leak-check=yes -v --logfile-fd=1 --leak-resolution=high --num-callers=20 H-Space -game hspace $* | grep == | less