#! /bin/bash

BASE=h-space-demo

# set up directories

mkdir -p $BASE/data/dialogs
rm -rf   $BASE/data/*

mkdir -p $BASE/scripts
rm -rf   $BASE/scripts/*


# copy content

cp       data/*.sects $BASE/data
cp       data/*.props $BASE/data
cp       data/*.lvl $BASE/data
cp       data/*.png $BASE/data
cp       data/*.tab $BASE/data
cp       data/*.hex $BASE/data

cp       data/dialogs/*.txt $BASE/data/dialogs

cp       scripts/*.lua $BASE/scripts

cp       images128.pak $BASE


EXPORT SIM_OPTIMIZE=true
EXPORT SIM_CROSS=true


EXPORT SIM_CROSS=false
