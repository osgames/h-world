----------------------------------------------------------------
-- dialogs.lua
--
-- H-World dialog effects and callbacks.
--
-- author: Hj. Malthaner
-- date:   21-Apr-2004
-- update: 21-Apr-2004
----------------------------------------------------------------


------------------------------------------------------------------
-- NPC wants to join the party -> make him a friend
--
-- author: Hj. Malthaner
-- date:   21-Apr-2004
-- update: 21-Apr-2004
--
-- param user:   type thing_t, the player
-- param npc:    type thing_t, the npc
-- return:       int, unused
------------------------------------------------------------------

function dialog_join_party (user, npc)
	_ALERT("dialog_join_party() called\n")

	translate("The " .. thing_get_ident(npc) .. " joins you.");

	thing_set_value(npc, "status", "friend")

	-- no more talking
	thing_set_value(npc, "dialog.radius", 0)

	return 0
end


------------------------------------------------------------------
-- Make NPC hostile
--
-- author: Hj. Malthaner
-- date:   21-Apr-2004
-- update: 21-Apr-2004
--
-- param user:   type thing_t, the player
-- param npc:    type thing_t, the npc
-- return:       int, unused
------------------------------------------------------------------

function dialog_hostile_npc (user, npc)
	_ALERT("dialog_hostile_npc() called\n")

	translate("The " .. thing_get_ident(npc) .. " turns to fight!");

	thing_set_value(npc, "status", "enemy")

	-- no more talking
	thing_set_value(npc, "dialog.radius", 0)

	return 0
end
