----------------------------------------------------------------
-- usages.lua
--
-- H-World usages initialition script, purpose is to contain all
-- usages for items
--
-- author: Hj. Malthaner
-- date:   20-Mar-2002
-- update: 24-Mar-2002
-- update: 27-Apr-2004 - Hj. Malthaner: new handling of magic item attributes
-- update: 12-May-2004 - Hj. Malthaner: handle non-thirsty beings correctly
-- update: 13-May-2004 - Hj. Malthaner: handle non-hungry beings correctly
-- update: 23-May-2004 - Hj. Malthaner: usages now also get an inventory as
--                                      parameter
-- update: 11-Aug-2004 - Hj. Malthaner: modified on_wield/on_unwield so
--                                      that items can change their inventory
--                                      image to another image upon wielding/
--                                      unwielding
-- update: 11-Sep-2004 - Hj. Malthaner: functions for magic sound effects
----------------------------------------------------------------


------------------------------------------------------------------
-- Helper function to replace an item by another one, e.g. quaffing
-- a potion will replace it by an empty bottle
--
-- author: Hj. Malthaner
-- date:   23-May-2004
-- update:   23-May-2004
--
-- param owner:      type thing_t, the one who wields the item
-- param inventory:  type inventory_t, the invenotry that held the old iten
-- param old:        type thing_t, the item to remove
-- param new:        type thing_t, the item to add instead of old
-- return:           nothing
------------------------------------------------------------------
function replace_item(user, inventory, old, new)
	if(inventory) then

		if(old) then
			inventory_remove_thing(inventory, old)
		end

		if(new) then
			inventory_add_thing(inventory, new)
		end
	else
		thing_replace_item(user, old, new)
	end
end



------------------------------------------------------------------
-- Wield a light source
--
-- Determine light type, subtype, calculate effect and apply effect to user
--
-- author: Hj. Malthaner
-- date:   24-Jun-2002
-- update: 24-Jun-2002
--
-- param user:   type thing_t, the one who wields the light source
-- param limb:   type thing_t, the limb that holds the item
-- param light:  type thing_t, the light source
-- return:       nothing
------------------------------------------------------------------

function do_wield_light (user, limb, light)
	_ALERT("wield_light called\n")

	radius = thing_get_value(light, "light_rad");
	old_rad = thing_get_value(user, "light_rad");

	_ALERT(thing_get_value(user, "ident") .. " is wielding a " .. thing_get_value(light, "ident") .. " with radius " .. radius .. "\n")


	-- set value
	thing_set_value(user, "light_rad", old_rad+radius);

	return 0
end


------------------------------------------------------------------
-- Unwield a light source
--
-- Determine light type, subtype, calculate effect and apply effect to user
--
-- author: Hj. Malthaner
-- date:   24-Jun-2002
-- update: 24-Jun-2002
--
-- param user:   type thing_t, the one who wields the light source
-- param limb:   type thing_t, the limb that holds the item
-- param light:  type thing_t, the light source
-- return:       nothing
------------------------------------------------------------------

function do_unwield_light (user, limb, light)
	_ALERT("unwield_light called\n")

	local radius = thing_get_value(light, "light_rad");
	local old_rad = thing_get_value(user, "light_rad");

	_ALERT(thing_get_value(user, "ident") .. " is unwielding a " .. thing_get_value(light, "ident") .. " with radius " .. radius .. "\n")

	-- set default value

	thing_set_value(user, "light_rad", old_rad-radius)

	return 0
end


------------------------------------------------------------------
-- helper table to call usage functions by name
------------------------------------------------------------------
wield_by_name =
{
	wield_light = do_wield_light,
	unwield_light = do_unwield_light
}


------------------------------------------------------------------
-- basic item wield function, should be called by all functions
-- that are called upon a wield event
--
-- author: Hj. Malthaner
-- date:   08-Aug-2002
-- update: 23-Dec-2002
-- update: 27-Apr-2004 - Hj. Malthaner: (magic item attributes)
-- update: 11-Aug-2004 - Hj. Malthaner: modified on_wield/on_unwield so
--                                      that items can change their inventory
--                                      image to another image upon wielding/
--                                      unwielding
--
-- param owner:  type thing_t, the one who wields the item
-- param limb:   type thing_t, the limb that holds the item
-- param item:   type thing_t, the item to wield
-- return:       nothing
------------------------------------------------------------------


function on_wield(owner, limb, item)
	-- _ALERT("lua: on_wield called\n")

	-- check for Lua call for "wielding" an item
    	local func = thing_get_value(item, "lua_wield")
    	if(func) then
		wield_by_name [func] (owner, limb, item)
    	end


	-- make cursed state known

	thing_set_value(item, "curse_known", "true")


	-- check item attributes

	local effect = nil
	local n = 0

	repeat
		local att_n = "att-" .. n

		effect = thing_get_value(item, att_n .. ".effect")

		if(effect) then

			value  = thing_get_value(item, att_n .. ".value");
			target = thing_get_value(item, att_n .. ".target");


			_ALERT("lua: value="..value.."\n")
			_ALERT("lua: target="..target.."\n")
			_ALERT("lua: effect="..effect.."\n")

			if(target == "self") then
				app = item
			else
				app = owner
			end

			v = thing_get_value(app, effect) or 0
		 
			thing_set_value(app, effect, v+value)
		end

		n = n+1 

	until (effect == nil)


	-- check item bulk

	effect = thing_get_value(item, "bulk");

	if(effect) then
		v = thing_get_value(owner, "hinderance") or 0
		thing_set_value(owner, "hinderance", v+effect)
	end


	-- check image change

	local file = thing_get_value(item, "image.worn")

	if(file) then
		local colorset = thing_get_value(item, "colorset")
		local image = load_tile_from_image(file)

		-- parameters:
		-- item, map_or_inventory, index, image, colorset, xoff, yoff, transparent, redraw
	
		thing_visual_set_image(item, 1, 0, image, colorset, 
			       	       0, 0, 0, 0);
		
	end


	return 0
end


------------------------------------------------------------------
-- basic item unwielding function, should be called by all functions
-- that are called upon a unwield event
--
-- author: Hj. Malthaner
-- date:   08-Aug-2002
-- update: 23-Dec-2002
-- update: 27-Apr-2004 - Hj. Malthaner: (magic item attributes)
-- update: 11-Aug-2004 - Hj. Malthaner: modified on_wield/on_unwield so
--                                      that items can change their inventory
--                                      image to another image upon wielding/
--                                      unwielding
--
-- param owner:  type thing_t, the one who wields the item
-- param limb:   type thing_t, the limb that holds the item
-- param item:   type thing_t, the item to unwield
-- return:       nothing
------------------------------------------------------------------


function on_unwield(owner, limb, item)
	-- _ALERT("lua: on_unwield called\n")


	-- check item attributes

	local effect = nil
	local n = 0

	repeat
		local att_n = "att-" .. n

		effect = thing_get_value(item, att_n .. ".effect")

		if(effect) then

			value  = thing_get_value(item, att_n .. ".value");
			target = thing_get_value(item, att_n .. ".target");


			_ALERT("lua: value="..value.."\n")
			_ALERT("lua: target="..target.."\n")
			_ALERT("lua: effect="..effect.."\n")

			if(target == "self") then
				app = item
			else
				app = owner
			end

			v = thing_get_value(app, effect) or 0
		 
			thing_set_value(app, effect, v-value)
		end

		n = n+1 

	until (effect == nil)


	-- check item bulk

	effect = thing_get_value(item, "bulk");

	if(effect) then
		v = thing_get_value(owner, "hinderance") or 0
		thing_set_value(owner, "hinderance", v-effect)
	end


	-- check image change

	local file = thing_get_value(item, "image.worn")

	if(file) then
		local colorset = thing_get_value(item, "colorset")
		local image = load_tile_from_image(thing_get_value(item, "image.inventory"))

		-- parameters:
		-- item, map_or_inventory, index, image, colorset, xoff, yoff, transparent, redraw
	
		thing_visual_set_image(item, 1, 0, image, colorset, 
			       	       0, 0, 0, 0);
		
	end


	-- check for Lua call for "unwielding" an item
    	local func = thing_get_value(item, "lua_unwield")
    	if(func) then
		wield_by_name [func] (owner, limb, item)
    	end

	return 0
end


------------------------------------------------------------------
-- quaff a potion
--
-- Determine potion type, subtype, calculate effect and apply effect to user
--
-- author: Hj. Malthaner
-- date:   20-Mar-2002
-- update: 24-Mar-2002
-- update: 12-May-2004 - Hj. Malthaner: handle non-thirsty beings correctly
-- update: 23-May-2004 - Hj. Malthaner: added inventory parameter
-- update: 30-May-2004 - Hj. Malthaner: poison is now cumulative
--
-- param user:       type thing_t, 
--                   the one who quaffs the potion (player or other)
-- param inventory:  type inventory_t, 
--                   the inventory that stores the potion (may be nil)
-- param potion:     type thing_t, the potion to be quaffed
-- return:           time delay
------------------------------------------------------------------

function use_quaff (user, inventory, potion)
	_ALERT("quaff called\n")

	local is_player = thing_get_value(user, "is_player")

	if (is_player) then
		translate("You are quaffing a " .. thing_get_value(potion, "ident") .. ".")
	end

	local hp = thing_get_value(user, "HPcurr")
	local maxhp = thing_get_value(user, "HPmax")
	local power = thing_get_value(potion, "power")
	local subtype = thing_get_value(potion, "subtype")


	-- process healing potions
	
	if subtype == "heal" then
		_ALERT("  Healing: hp=".. hp .."\n")

		local rb = rng_get_int(power)+power/3

		-- restore hit point up to max

		if hp+rb > maxhp then
			hp = maxhp
		else
			hp = hp+rb
		end

		-- set values

		thing_set_value(user, "HPcurr", hp)

		-- tell user about effect

		if (is_player) then
			if rb < 10 then
				translate("You feel little better.")
			elseif rb < 20 then
				translate("You feel better.")
			elseif rb < 30 then
				translate("You feel much better.")
			else
				translate("You feel much better!")
			end

		else
			translate("The " .. thing_get_ident(user) .. " looks healthier.")
		end
	end


	-- process poisoning potions

	if subtype == "poison" then
		_ALERT("  Poison")

		local rb = rng_get_int(power)+power/3

		-- set values

		local prev = thing_get_value(user, "effect.poison") or 0
		thing_set_value(user, "effect.poison", prev + rb)

		-- tell user about effect

		if (is_player) then
			translate("You feel very sick.")
		else 
			translate("The " .. thing_get_ident(user) .. " gets somewhat green.")
		end
		
	end


	-- process antidotes

	if subtype == "antidote" then
		_ALERT("  Antidote")

		local rb = rng_get_int(power)+power/3

		local poison = thing_get_value(user, "effect.poison") or 0

		if (is_player) then
			if rb >= poison then
				poison = 0
				translate("You feel perfectly well.")
			else
				poison = poison - rb
				translate("You feel less poisoned.")
			end
		else
			translate("The " .. thing_get_ident(user) .. " looks better.")
		end

		thing_set_value(user, "effect.poison", poison)
	end


	-- process blinding potions

	if subtype == "blind" then
		_ALERT("  blind")

		local rb = rng_get_int(power)+power/3

		-- set values

		local prev = thing_get_value(user, "effect.blind") or 0
		thing_set_value(user, "effect.blind", prev + rb)

		-- tell user about effect

		if (is_player) then
			translate("You are blind!")
		else 
			translate("The " .. thing_get_ident(user) .. " stumbles around.")
		end
	end


	-- modify bottle
	
	local bottle = thing_create("empty_bottle", 0, 0)
	if bottle ~= nil then
		replace_item(user, inventory, potion, bottle)
	end


	-- reduce thirst

	local thirst = thing_get_value(user, "thirst")

	-- only handle thirsty beings

	if(thirst) then
		thirst = thirst - power

		if thirst < 0 then 
			thirst = 0
		end

		thing_set_value(user, "thirst", thirst)

		-- tell user about effect
		if(is_player) then
			translate("You feel less thirsty.")
		end
	end


	return 4
end


------------------------------------------------------------------
-- Empty a (filled) bottle
--
-- Replaces bottle with an empty bottle
--
-- author: Hj. Malthaner
-- date:   13-Apr-2004
-- update: 13-Mar-2004
-- update: 23-May-2004 - added inventory parameter
--
-- param user:       type thing_t, 
--                   the one who empties the bottle (player or other)
-- param inventory:  type inventory_t, 
--                   the inventory that stores the potion (may be nil)
-- param potion:     type thing_t, the bottle to be emptied
-- return:           time delay
------------------------------------------------------------------

function use_empty (user, inventory, potion)
	_ALERT("empty called\n")
	
	local is_player = thing_get_value(user, "is_player")

	if is_player then
		translate("You empty the " .. thing_get_ident(potion) .. ".")
	end

	local hp = thing_get_value(user, "HPcurr")
	local maxhp = thing_get_value(user, "HPmax")
	local power = thing_get_value(potion, "power") or 0
	local subtype = thing_get_value(potion, "subtype") or "unset"


	-- process poisoning potions

	if subtype == "poison" then
		_ALERT("  Poison")

		if is_player then
			translate("The bottles content smells strange.")
		end

		local rb = rng_get_int(power)+power/30

		if rb >= 1 then
			-- set values

			thing_set_value(user, "effect.poison", rb)

			-- tell user about effect

			if is_player then
				translate("You feel somewhat sick.")
			end
		end
	end


	-- modify bottle
	
	local bottle = thing_create("empty_bottle", 0, 0)
	if bottle ~= nil then
		replace_item(user, inventory, potion, bottle)
	end


	return 3
end



------------------------------------------------------------------
-- Eat something
--
-- Determine food type, subtype, calculate effect and apply effect to user
--
-- author: Hj. Malthaner
-- date:   22-Dec-2003
-- update: 13-May-2004 - Hj. Malthaner: handle non-hungry beings correctly
-- update: 23-May-2004 - added inventory parameter
--
-- param user:       type thing_t, the one who eats
-- param inventory:  type inventory_t, 
--                   the inventory that stores the food (may be nil)
-- param potion:     type thing_t, the food to be eaten
-- return:           time delay
------------------------------------------------------------------

function use_eat (user, inventory, food)
	_ALERT("eat called\n")

	if thing_get_value(user, "is_player") ~= nil then
		translate("You are eating the " .. thing_get_value(food, "ident") .. ".")
	end

	local hunger = thing_get_value(user, "hunger")
	local power = thing_get_value(food, "power")
	local subtype = thing_get_value(food, "subtype")

	-- only handle hungry beings

	if(hunger) then

		-- process ordinary food
	
		local nutrition = rng_get_int(power)+power/3

		-- lower hunger

		hunger = hunger - nutrition

		if hunger < 0 then 
			hunger = 0
		end

		thing_set_value(user, "hunger", hunger)

		-- tell user about effect

		if hunger < 10 then
			translate("You are full.")
		else
			translate("You feel less hungry.")
		end
	end

	thing_set_value(food, "cur_stack", 0)	

	return 30
end


------------------------------------------------------------------
-- Throw an item, put it on target square
--
-- author: Hj. Malthaner
-- date:   22-Apr-2004
-- update: 25-Apr-2004
-- update: 23-May-2004 - added inventory parameter
--
-- param user:       type thing_t, the thrower
-- param inventory:  type inventory_t, 
--                   the inventory that stores the ammo (may be nil)
-- param ammo:       type thing_t, the item to throw
-- return:           time delay
------------------------------------------------------------------

function use_throw (user, inventory, ammo)
	_ALERT("throw called\n")

	local delay = thing_throw(user, ammo, 1)

	-- potion bottles are thrown to break and free the contents
	if(thing_get_value(ammo, "type") == "potion") then
		-- 1 means to kill silently
		thing_kill(ammo, 1)
	end


	return delay;
end


------------------------------------------------------------------
-- Shoot an item, put it on target square
--
-- author: Hj. Malthaner
-- date:   16-Aug-2004
-- update: 16-Aug-2004
--
-- param shootist:   type thing_t, the shootist
-- param inventory:  type inventory_t, 
--                   the inventory that stores the launcher (may be nil)
-- param ammo:       type thing_t, the launcher
-- return:           time delay
------------------------------------------------------------------

function use_shoot (shootist, inventory, launcher)
	_ALERT("shoot called\n")

	local delay = 1

    	local ammo_hold = thing_find_limb(shootist, "hold", "ammo")
	local multi = thing_get_value(launcher, "damage_multiplier") or 1
	local ammotype = thing_get_value(launcher, "ammo.type") or "ammo"
	local ammosub = thing_get_value(launcher, "ammo.subtype") or "XXX"

	local is_player = thing_get_value(shootist, "is_player")

	if (ammo_hold) then
		local ammo = thing_get_item(ammo_hold)

		if (ammo) then
			if (thing_get_value(ammo, "type") == ammotype and
                            thing_get_value(ammo, "subtype") == ammosub) then

				if (is_player) then
					 translate("You are shooting ...")
				end

				delay = thing_throw(shootist, ammo, multi)

			else

				if (is_player) then
					translate("The " ..
					          thing_get_ident(launcher) ..
					          "needs " ..
					          ammosub .. "s as ammo.")
				end

			end
		else
			if (is_player) then
				translate("You have no ammo for this weapon.")
			end
		end

	else
		translate("Error: There is no ammo holder in this body")
	end


	return delay;
end


------------------------------------------------------------------
-- Identify an item
--
-- author: Hj. Malthaner
-- date:   14-Aug-2003
-- date:   14-Aug-2003
-- update: 23-May-2004 - added inventory parameter
--
-- param user:       type thing_t, the user 
-- param inventory:  type inventory_t, 
--                   the inventory that stores the prober (may be nil)
-- param prober:     type thing_t, the identify gadget
-- return:           time delay
------------------------------------------------------------------

function use_identify (user, inventory, prober)
	_ALERT("identify called\n")


	local thing = thing_choose("Identify what?", user, nil, nil, nil)

	if thing ~= nil then
		-- set flag to identified
		thing_set_value(thing, "is_unknown", "false")
		-- set curses to known
		thing_set_value(thing, "curse_known", "true")

		translate("You identify a " .. thing_get_ident(thing) .. ".")

		-- some item types are memorized by type
                -- 'know one, know all'

		local type = thing_get_value(thing, "type")

		if(type == "potion" or type == "scroll") then 
			-- _ALERT("identify item class\n")
			thing_set_item_known(user, thing)
		end

		thing_set_value(prober, "cur_stack", 0)
	end

	return 20
end


------------------------------------------------------------------
-- remove curse from an item
--
-- author: Hj. Malthaner
-- date:   16-Nov-2003
-- date:   16-Nov-2003
-- update: 23-May-2004 - added inventory parameter
--
-- param user:       type thing_t, the user 
-- param inventory:  type inventory_t, 
--                   the inventory that stores the scroll (may be nil)
-- param scroll:     type thing_t, the scroll
-- return:           time delay
------------------------------------------------------------------

function use_remove_curse (user, inventory, scroll)
	_ALERT("remove_curse called\n")


	local thing = thing_choose("Remove curse from which item?", user, nil, nil, nil)

	if thing ~= nil then

		translate("You uncurse the " .. thing_get_ident(thing) .. ".")

		thing_set_value(thing, "cursed", "false");
		-- set curses to known
		thing_set_value(thing, "curse_known", "true")

		thing_set_value(scroll, "cur_stack", 0)
	end

	return 10
end


------------------------------------------------------------------
-- detect curse on an item
--
-- author:   Hj. Malthaner
-- creation: 21-May-2003
-- update:   21-May-2003
-- update:   23-May-2004 - added inventory parameter
--
-- param user:       type thing_t, the user 
-- param inventory:  type inventory_t, 
--                   the inventory that stores the scroll (may be nil)
-- param scroll:     type thing_t, the scroll
-- return:           time delay
------------------------------------------------------------------

function use_detect_curse (user, inventory, scroll)
	_ALERT("detect_curse called\n")


	local thing = thing_choose("Probe which item?", user, nil, nil, nil)

	if thing ~= nil then

		translate("You probe the " .. thing_get_ident(thing) .. ".")

		-- set curses to known
		thing_set_value(thing, "curse_known", "true")

		thing_set_value(scroll, "cur_stack", 0)
	end

	return 5
end


------------------------------------------------------------------
-- play a sound
--
-- author:   Hj. Malthaner
-- creation: 11-Sep-2004
-- update:   11-Sep-2004
--
-- param user:       type thing_t, the user 
-- param inventory:  type inventory_t, 
--                   the inventory that stores the scroll (may be nil)
-- param sound:      type thing_t, the sound to play
-- return:           time delay
------------------------------------------------------------------


function use_play (user, inventory, sound)

	local subtype = thing_get_value(sound, "subtype")

	if(subtype == "sleep") then
		return sound_sleep(user, inventory, sound)
	end


	translate("Nothing happens.")

	return 10
end


--
-- Effects of playing a sound
--

------------------------------------------------------------------
-- play a sleep sound
--
-- author:   Hj. Malthaner
-- creation: 11-Sep-2004
-- update:   11-Sep-2004
--
-- param user:       type thing_t, the user 
-- param inventory:  type inventory_t, 
--                   the inventory that stores the scroll (may be nil)
-- param sound:      type thing_t, the sound to play
-- return:           time delay
------------------------------------------------------------------

function sound_sleep(user, inventory, sound)

	translate("You play a sound of sleep.")	

	local x, y
	x,y = thing_get_location(user)

	for j = y-3, y+3, 1 do
		for i = x-3, x+3, 1 do 

			local n = 0
			local thing

			repeat

				thing = square_get_thing(i, j, n)
				n = n + 1

				-- exclude empty squares and player position
				if(thing and i~=x and j~=y ) then

					local anim = thing_is_animated(thing)

					if(anim) then

						local sleep = thing_get_value(thing, "sleep")
					
						if(sleep == nil) then
							translate("The " .. thing_get_ident(thing) .. " resists the effects.")
						else
							if(sleep <= 0) then
		
								thing_set_value(thing, "sleep", 100)
								translate("The sound lets the " .. thing_get_ident(thing) .. " fall asleep.")

							end
						end
					end
				end

			until (thing == nil)

		end
	end

	-- can use only once
	thing_set_value(sound, "cur_stack", 0)


	return 15
end


------------------------------------------------------------------
-- Activate a ground prober (drill)
--
-- Generate a ground sample
--
-- author: Hj. Malthaner
-- date:   24-Jun-2002
-- update: 24-Jun-2002
--
-- param user:   type thing_t, the user of the ground prober
-- param inv:    type inventory_t
-- param prober: type thing_t, the ground prober
-- return:       nothing
------------------------------------------------------------------

function use_drill (user, inv, prober)
	_ALERT("drill called\n")

	-- generate ground sample

	local sample = thing_create("ground_sample", 0, 0)
	
	thing_add_item_to_inv(prober, sample)

	translate("You drill into the ground and take a sample.")

	return 0
end
