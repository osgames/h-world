#include <stdio.h>

#include "swt/window_manager_t.h"
#include "system_view_trigger_t.h"
#include "model/thing_t.h"
#include "model/thinghandle_t.h"
#include "factories/feature_peer_broker_t.h"


#include "space/gui/system_view_t.h"


system_view_trigger_t::system_view_trigger_t()
{
  
}


feature_peer_t * system_view_trigger_t::create(koord k, const char *paramters)
{
  return new system_view_trigger_t();
}


const char * system_view_trigger_t::get_name()
{
  return "system_view_trigger";
}


bool system_view_trigger_t::activate(thinghandle_t thing)
{
  window_manager_t * winman = window_manager_t::get_instance();
  const koord size (winman->get_width(), winman->get_height());
    
  winman->add_window(new system_view_t(size));

  return true;
}


// ---- implements persistent_t ----
  
/**
 * Stores/restores the object. Must be overridden by subclass.
 * This method is supposed to call store->store() on all associated
 * objects!
 * @author Hj. Malthaner
 */
void system_view_trigger_t::read_write(storage_t *store, iofile_t * file)
{
  
}

  
/**
 * Gets a unique ID for this objects class.
 * @author Hj. Malthaner
 */
perid_t system_view_trigger_t::get_cid()
{
  // XXX todo

  return 1000000;
}

