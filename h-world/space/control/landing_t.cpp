#include "landing_t.h"

#include "model/pprops_t.h"
#include "model/world_t.h"
#include "model/level_t.h"
#include "model/thing_t.h"

#include "space/model/sunsys_t.h"

#define PATH_PRE "./hspace/data/"

bool landing_t::land(const sunsys_t *planet)
{
  thinghandle_t player = world_t::get_instance()->get_player();

  // Hajo: check ptype here and determine which level 
  // description to load

  char * name = 0;

  switch(planet->btype) {

  case sunsys_t::B_STATION:
    name = PATH_PRE "station001.props";
    break;

  case sunsys_t::B_PLANET:

    switch(planet->ptype) {
    case sunsys_t::P_VENUS:
      name =PATH_PRE "venus.props";
      break;

    case sunsys_t::P_EARTH:
      name =PATH_PRE "earthlike.props";
      break;

    case sunsys_t::P_BARE_ROCK:
      name =PATH_PRE "planet_bare_rock.props";
      break;

    case sunsys_t::P_ATM_ROCK:
      name =PATH_PRE "planet_atm_rock.props";
      break;
      
    case sunsys_t::P_ICE:
      name =PATH_PRE "crystal.props";
      break;

    default:
      // Hajo: can't land here
      name = 0;
      break;
    }
    break;

  default:
    // Hajo: can't land here
    name = 0;
    break;
  }

  /*

  if(props) {
    props->set("rooms", 1); 
    props->set("room[0].type", "external");
    props->set("room[0].file", PATH_PRE"/ship.lvl");
    props->set("room[0].width", 40);
    props->set("room[0].height", 24);
    props->set("room[0].player.x", player->get_pos().x);
    props->set("room[0].player.y", player->get_pos().y);
    
    world_t::get_instance()->new_level(props);
  }

  */

  // If we found a suitable destination, set the exit level
  if(name) {
    world_t::get_instance()
      ->get_level()
      ->get_properties()
      ->set("$EXIT_1", name);
  }

  return name != 0;
}
