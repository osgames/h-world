#ifndef system_view_trigger_t_h
#define system_view_trigger_t_h

#ifndef FEATURE_PEER_T_H
#include "model/feature_peer_t.h"
#endif

#ifndef feature_peer_creator_t_h
#include "factories/feature_peer_creator_t.h"
#endif

#ifndef thinghandle_t_h
#include "model/thinghandle_t.h"
#endif

class system_view_trigger_t : public feature_peer_t, public feature_peer_creator_t 
{
 private:


 public:

  system_view_trigger_t();


  virtual bool activate(thinghandle_t thing);

  virtual feature_peer_t * create(koord k, const char *paramters);

  virtual const char * get_name();


  // ---- implements persistent_t ----
  
  /**
   * Stores/restores the object. Must be overridden by subclass.
   * This method is supposed to call store->store() on all associated
   * objects!
   * @author Hj. Malthaner
   */
  virtual void read_write(storage_t *store, iofile_t * file);

  
  /**
   * Gets a unique ID for this objects class.
   * @author Hj. Malthaner
   */
  virtual perid_t get_cid();

};

#endif // system_view_trigger_t_h
