#ifndef landing_t_h
#define landing_t_h


class sunsys_t;


/**
 * Creates surface level, places the ship there and
 * sets the surface level as current level of the world
 *
 * @autor Hj. Malthaner
 */
class landing_t
{

 public:

  bool land(const sunsys_t *planet);

};

#endif // landing_t_h
