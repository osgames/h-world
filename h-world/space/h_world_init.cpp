#include "factories/hdoor_creator_t.h"
#include "factories/vdoor_creator_t.h"
#include "factories/exit_creator_t.h"
#include "factories/fountain_creator_t.h"
#include "factories/feature_peer_broker_t.h"

#include "control/system_view_trigger_t.h"

// Hajo: set up feature creators
static hdoor_creator_t hdoor_creator;
static vdoor_creator_t vdoor_creator;
static exit_creator_t exit_creator;
static fountain_creator_t fountain_creator;
static system_view_trigger_t system_view_trigger;

void h_world_init()
{
  // Hajo: register at broker
  feature_peer_broker_t::add_creator(&hdoor_creator);
  feature_peer_broker_t::add_creator(&vdoor_creator);
  feature_peer_broker_t::add_creator(&exit_creator);
  feature_peer_broker_t::add_creator(&fountain_creator);
  feature_peer_broker_t::add_creator(&system_view_trigger);
}
