/*
 * textutil.cpp (formerly fly_text.c)
 *
 * Texterzeugungs und -konvertierungfunktionen
 *
 * Hj. Malthaner, 1998
 */
                     
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "util/rng_t.h"


const char * dt_int(int f)
{
    static char buf[128];
    char tmp[128];
    char *p;
    int i,l;

    sprintf(tmp,"%d",f);

    l = strlen(tmp);
    
    i = l % 3;
    memcpy(buf,tmp, 3);
    p = buf + i;

    if(i != 0)
	*p++ = '.';

    while(i < l) {
	*p++ = tmp[i++];
	*p++ = tmp[i++];
	*p++ = tmp[i++];
	*p++ = '.';
    } 
    --p;
    *p = 0;

    return buf;
}


const char * dt_float(double f)
{
    static char buf[128];
    char tmp[128];
    char *p;
    int i,l;

    sprintf(tmp,"%.2f",f);

    l = strchr(tmp,'.') - tmp;
    
    i = l % 3;
    memcpy(buf,tmp, 3);
    p = buf + i;

    if(i != 0)
	*p++ = '.';

    while(i < l) {
	*p++ = tmp[i++];
	*p++ = tmp[i++];
	*p++ = tmp[i++];
	*p++ = '.';
    } 
    --p;
    i = l+1;

    *p++ = ',';
    *p++ = tmp[i++];
    *p++ = tmp[i++];
    *p = 0;

    return buf;
}

#define MAX_SILBEN  65

static const char *silbe[MAX_SILBEN] = {

  "al",
    "de",
    "gel",
    "ri",
    "be",
    "tei",
    "geu",
    "ze",

    "ge",
    "sil",
    "ste",
    "gro",
    "ker",
    "ti",
    "pel",
    "kon",
    "ons",
    "da",
    "tor",
    "se",
    "lie",
    "pe",
    "ben",
    "rei",
    "tur",
    "sel",
    "fie",
    "pam",
    "mer",
    "ni",
    "dri",
    "zin",
    "art",
    "wer",
    "den",
    "ke",
    "an",
    "sam",
    "ren",
    "di",
    "ma",
    "bar",
    "wei",
    "tel",
    "big",
    "ra",
    "ver",
    "mi",
    "ro",
    "la",
    "re",
    "zep",
    "tor",
    "tak",
    "ker",
    "ve",
    "der",
    "ri",
    "bo",
    "nis",
    "ta",
    "si",
    "min",
    "us",
    "run"
};
                
const char *gen_name(rng_t *rng)
{
    static char buf[128];
    int i;
    const int l = 2 + rng->get_int(2);
            
    buf[0] = 0;
    for(i=0; i<l; i++) {
	strcat(buf, silbe[rng->get_int(MAX_SILBEN)]);
    }                 

    buf[0] = toupper(buf[0]);
    return buf;
}


static const char *pers_namen[36] = {
    "Kopernikus",
    "Brahe",
    "Kepler",
    "Galilei",
    "Scheiner",
    "Huygens",
    "Cassini",
    "Halley",
    "Newton",
    "Messier",
    "Herschel",
    "Olbers",
    "Bessel",
    "Struve",
    "Schwabe",
    "Fizeau",
    "Foucault",
    "Leverrier",
    "Kirchhoff",
    "Schiaparelli",
    "Pickering",
    "Hale",
    "Wolf",
    "Kapteyn",
    "Leavitt",
    "Russel",
    "Einstein",
    "Curtis",
    "Eddington",
    "Hubble",
    "Tombaugh",
    "Lyot",
    "Jansky",
    "Oort",
    "van Allen",
    "Bell"    
};

static const char *statdesc[4] = {
    "Depot",
    "Station",
    "Structure",
    "Fort"
};    

const char *gen_pname(rng_t *rng)
{
    static char buf[128];
    int desc = rng->get_int(4);
    
    if(desc < 2) {
	strcpy(buf, pers_namen[rng->get_int(36)]);
	strcat(buf," ");
	strcat(buf, statdesc[desc]);
    } else {
	strcpy(buf, statdesc[desc]);
	strcat(buf," ");
	strcat(buf, pers_namen[rng->get_int(36)]);
    }

    return buf;
}
