#ifndef sunsys_t_h
#define sunsys_t_h

#include "util/rng_t.h"
#include "tpl/minivec_tpl.h"
#include "primitives/cstring_t.h"

#include "vec.h"

class planet_t;
class ship_t;


#ifndef M_PI
# define M_PI           3.14159265358979323846
#endif 


/**
 * A class for stellar systems
 *
 * @author Hj. Malthaner
 */
class sunsys_t
{
 public:
  /**
   * Type of a body in stellar systems  'body type'
   *
   * @author Hj. Malthaner
   */
  enum btype {
    B_SUN,
    B_PLANET,
    B_STATION,
    B_MAX
  };


  /**
   * Type of planets 'planet type'
   *
   * @author Hj. Malthaner
   */
  enum ptype {
    P_BARE_ROCK,                /* Tr. + Fels. */
    P_ICE,			/* Eiskoerper */
    P_ATM_ROCK,			/* Fels. + Atm. */
    P_VENUS,			/* Venustyp */
    P_EARTH,			/* Erdtyp */
    P_SMALL_GAS,		/* kl. Gasriese */
    P_BIG_GAS,			/* Gr. Gasiese */
    P_RINGS,			/* Ringplanet */
    P_STATION_1,                /* Type 1 space station */
    P_MAX
  };


  /**
   * Type of suns 'sun type'
   *
   * @author Hj. Malthaner
   */
  enum stype {
    S_YELLOW, 
    S_ORANGE, 
    S_WHITE_DWARF, 
    S_RED_GIANT, 
    S_BLUE_GIANT,
    S_MAX
  };


  static const char * planet_desc[P_MAX];
  static const char * sun_desc[S_MAX];

 private:

  /**
   * Calculate sun radius from sun type
   * @author Hj. Malthaner
   */
  static int sun_rad(enum stype);


  // Hajo: double linked tree structure.
  sunsys_t *parent;
  minivec_tpl <sunsys_t *> children;


  minivec_tpl <ship_t *> ships;

  /**
   * The rng to create this system. All bodies share the same rng.
   *
   * @author Hj. Malthaner
   */
  rng_t *rng;


  /**
   * radius of this stellar body
   * @author Hj. Malthaner
   */
  int rad;


  double bahnrad;

  /* position des planeten */
  // PKT   pos;

  /* Earth Equiv. Temp. : Temperatur der Erde, waere sie */
  /* an Stelle dieses Planeten */
  int  eet;


  cstring_t name;


  /**
   * Determine a type for the sun of this system
   * May only be called once!
   * @author Hj. Malthaner
   */
  enum stype sunsys_t::random_type();


  void calc_ptype();


  /**
   * Build a new planet/body for a stellar system
   *
   * @param min_rad minimum orbital radius for this body
   * @param size    amount of random spreading in orbit radius
   * @param number  this is the number'th child of parent, counting from 0
   * @paran anz     total number of children of parent
   *
   * @author Hj. Malthaner
   */
  sunsys_t(sunsys_t *p, int min_rad, int size, int number, int anz, 
	   enum btype btype);


 public:


  /**
   * Body type
   * @author Hj. Malthaner
   */
  enum btype btype;
 

  /**
   * If btype == P_PLANET or btype == P_STATION then ptype will tell 
   * the type of the planet
   * @author Hj. Malthaner
   */
  enum ptype ptype;


  /**
   * If btype == P_SUN then stype will tell the type of the sun
   * @author Hj. Malthaner
   */
  enum stype stype;


  const sunsys_t * get_parent() const {return parent;};

  const sunsys_t * get_child(int i) const {return children.get(i);};
  int get_child_count() const {return children.count();}; 

  double get_bahnrad() const {return bahnrad;};

  const cstring_t & get_name() const {return name;};

  int get_rad() const {return rad;};

  int get_eet() const {return eet;};


  /**
   * Position reltiv to parent body
   * @author Hj. Malthaner
   */
  VEC pos;

  
  /**
   * Build a new stellar system from the seed
   *
   * @author Hj. Malthaner
   */
  sunsys_t(int seed, bool create_children);


  /**
   * Searches a body withing range around pos
   *
   * @author Hj. Malthaner
   */
  const sunsys_t * find_in_range(VEC pos, double range) const; 

};


#endif // sunsys_t_h
