#ifndef galaxy_t_h
#define galaxy_t_h

#include "primitives/koord.h"

class galaxy_t
{
 public:

  static koord calc_sec(const int number);
  static int calc_sec_number(int i, int j);



};

#endif // galaxy_t_h
