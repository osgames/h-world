#include <math.h>

// debug
#include <stdio.h>
#include <limits.h>

#include "util/debug_t.h"

#include "sunsys_t.h"
#include "space/util/textutil.h"

#define SQR(a) ((a)*(a))
#define MIN(a,b) ((a) < (b) ? (a) : (b))


const char * sunsys_t::planet_desc[P_MAX] = {
  "A bare rock body",
  "An ice body",
  "A rock body with thin atmosphere",
  "A rock body with a dense, corrosive atmosphere",
  "An earthlike planet",
  "A small gas giant",
  "A big gas giant",
  "A big planet with rings"
};


const char * sunsys_t::sun_desc[S_MAX] = {
  "A medium size yellow sun", 
  "A small orange sun", 
  "A white dwarf sun", 
  "A red giant star", 
  "A giant blue star",
};


static double normverteilung(double x, double u, double s)
{         
    const double e = exp(- (SQR(x-u)) / (2.0*s*s));
    const double d = sqrt(2.0*M_PI*s*s);

    return e/d;
}


/**
 * Eart Equiv. Temp.
 * 
 * Berechnet Temperatur der Erde in Kelvin, waere sie an Stelle dieses Planeten
 * Das ist eine grobe Schaetzung.
 */
static double calc_EET(double sun_rad, double sun_dist)
{
    return atan(sun_rad/sun_dist)*62000.0;
}


/**
 * Calculate sun radius from sun type
 * @author Hj. Malthaner
 */
int sunsys_t::sun_rad(enum stype type)
{   
  int rad = 0;
            
  switch(type) {
  case S_YELLOW:
    rad = 1390600/4;           /* gelb, normal */
    break;
  case S_ORANGE:                         /* dunkelgelb, klein */
    rad = 1390600/6;
    break;
  case S_BLUE_GIANT:                        /* blauer Ueberriese */
    rad = (int) (1390600*1.5);
    break;
  case S_RED_GIANT:                         /* roter Riese */
    rad = 1390600;
    break;
  case S_WHITE_DWARF:
    rad = 1390600/10;
    break;
  case S_MAX:
    // Should never happen ...
    rad = 0;
  }

  return rad;
}


/**
 * Determine a type for the sun of this system
 * May only be called once!
 * @author Hj. Malthaner
 */
enum sunsys_t::stype sunsys_t::random_type()
{                
  return (enum stype) rng->get_int(S_MAX);
}


void sunsys_t::calc_ptype()
{
  ptype = P_BARE_ROCK; // default

  if(rad < 2500) {     

    if(eet > 200) {
      ptype = P_BARE_ROCK;                    /* Tr. + Fels. */
    } else {
      ptype = P_ICE;			/* Eiskoerper */
    }
  } else if(rad < 3600) {
    if(eet > 190) {
      ptype = P_ATM_ROCK;                    /* Fels. + Atm. */
    } else {
      ptype = P_ICE;			/* Eiskoerper */
    }
  } else if(rad < 10000) {
    if(eet > 190 && eet < 350) {
      if((rng->get_int(0xFFFFFF) & 3) == 0) {
	ptype = P_VENUS;                /* Venustyp */
      } else {
	ptype = P_EARTH;                /* Erdtyp */
      }
    } else {
      ptype = P_VENUS;                    /* Venustyp */
    }
    
  } else if(rad < 30000) {
    ptype = P_SMALL_GAS;			/* kl. Gasriese */
  } else if(rad < 65000) {
    ptype = P_RINGS;			/* Ringplanet */
  } else {
    ptype = P_BIG_GAS;			/* Gr. Gasiese */
  }

  // check for space station

  if(btype == B_STATION) {
    ptype = P_STATION_1;
  }
}



/**
 * Build a new planet/body for a stellar system
 *
 * @param min_rad minimum orbital radius for this body
 * @param size    amount of random spreading in orbit radius
 * @param number  this is the number'th child of parent, counting from 0
 * @paran anz     total number of children of parent
 *
 * @author Hj. Malthaner
 */
sunsys_t::sunsys_t(sunsys_t *p, int min_rad, int size, int number, int anz, enum btype a_btype) : children (8)
{
  dbg->message("sunsys_t::sunsys_t()",
	       "min_rad=%d size=%d number=%d anz=%d a_btype=%d",
	       min_rad, size, number, anz, a_btype);

  parent = p;
  rng = parent->rng;

  btype = a_btype;

  if(btype == B_STATION) {
    // name = "Space station";
    name = gen_pname(rng);
    ptype = P_STATION_1;

  } else {
    name = parent->name + " " + (long)(number+1);
  }

  dbg->message("sunsys_t::sunsys_t()","Name: %s", name.chars());

  rad = 500 + rng->get_int((int)(normverteilung(number, anz/1.8, 1.5) * size));
                                             
  bahnrad = min_rad*1.25 + rng->get_int(MIN(min_rad*2, 1000000000));

  /* Nur position auf kreis */


  double u = rng->get_int(360) * M_PI / 180.0;

  pos.x = cos(u) * bahnrad;
  pos.y = sin(u) * bahnrad;
  pos.z = 0;


  sunsys_t * the_sun = this;
  VEC zv;

  while(the_sun->parent) {
    zv += the_sun->pos; 

    // printf("%f %f\n", zv_x, zv_y);

    the_sun = the_sun->parent;
  }


  const double sun_dist = sqrt(zv.length2());


  // printf("-- %s %f\n", name.chars(), sun_dist);

  eet = (int)calc_EET(the_sun->rad, sun_dist);

  calc_ptype();


  if(min_rad > 20000000.0 && rad > 2000) {


    if(rng->get_int(1000) > 500) {
      // PKT pos = parent->pos;
      // vecadd(&pos, &pln->pos);

      const int anz = rng->get_int(5);
      
      for(int i=0; i<anz; i++) { 
	sunsys_t *planet = new sunsys_t(this, rad*2, rad/2, i, anz, B_PLANET);
	children.append(planet);
      }
    }

    sunsys_t *planet = new sunsys_t(this, rad, rad/4, 0, 1, B_STATION);
    children.append(planet);
  }
}


/**
 * Build a new stellar system from the seed
 *
 * @author Hj. Malthaner
 */
sunsys_t::sunsys_t(int seed, bool create_children) : children(16)
{   
  parent = 0;
  rng = new rng_t(seed);


  // Type of the sun
  stype = random_type();
  btype = B_SUN;
  ptype = P_BARE_ROCK;

  name = cstring_t(gen_name(rng));


  rad = (int) (sun_rad(stype) * ((rng->get_int(15)+5) / 10.0));
  
  const int anz = 1 + rng->get_int(12);
  int min_rad = 36*rad;

  if(create_children) {
    for(int i=0; i<anz && min_rad > 0; i++) { 
      sunsys_t *planet = new sunsys_t(this, min_rad, 380000, i, anz, B_PLANET);
      min_rad = planet->bahnrad;

      children.append(planet);
    }
  }
    
  // sys->ewz = calc_einwohnerzahl(sys, number);
} 


/**
 * Searches a body withing range around pos
 *
 * @author Hj. Malthaner
 */
const sunsys_t * sunsys_t::find_in_range(VEC point_pos, double range) const
{
  VEC v = pos - point_pos;

  if(v.length2() < range*range) {
    return this;
  }

  VEC npos = point_pos - pos;

  for(int i=0; i<children.count(); i++) { 
    const sunsys_t * result = children.get(i)->find_in_range(npos, range);

    if(result) {
      return result;
    }
  }

  return 0;
}
