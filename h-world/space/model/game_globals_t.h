#ifndef game_globals_t_h
#define game_globals_t_h

class sunsys_t;


/**
 * This class is supposed to be a collection of gloabal game objects
 * for H-Space.
 *
 * @author Hj. Malthaner
 */
class game_globals_t {


 public:


  /**
   * Global game status object for h_space
   * @author Hj. Malthaner
   */
  static game_globals_t h_space;


  /**
   * The solar system that is the current location of the player
   * It's NULL if the player is in deep space without any bodies
   * around.
   *
   * @author Hj. Malthaner
   */
  sunsys_t *current_system;

  sunsys_t *hyperjump_destination;


  game_globals_t();

};


#endif
