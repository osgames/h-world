#define GAMANUM 1

#include <stdlib.h>

#include "galaxy_t.h"

koord galaxy_t::calc_sec(const int number)
{
  koord k;
  int n;

  n = (number - GAMANUM) / 20;
  k.x =  (n % 10000)- 5000;

  n = (number - GAMANUM) / 20;
  k.y = (n / 10000) - 5000;

  return k;
}

int galaxy_t::calc_sec_number(int i, int j)
{
    return (abs(i+4999) + abs(j+5000)*10000) * 20 + GAMANUM;
}
