#ifndef VEC_h
#define VEC_h

#include "primitives/vec3.h"

typedef vec3<double> VEC;

#endif // vec_h
