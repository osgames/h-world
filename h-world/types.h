#ifndef types_t_h
#define types_t_h


typedef unsigned char  bool8; 

typedef unsigned char  uint8;
typedef signed char    sint8;

typedef unsigned short uint16;
typedef signed short   sint16;

typedef unsigned int   uint32;
typedef signed int     sint32;


#endif // types_t_h
