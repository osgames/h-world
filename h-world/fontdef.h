/*
 * fontdef.h
 *
 * Global font definitions
 */

// Hajo: font strings are defined in h_world_main.cpp !!!

extern char font_small [];
extern char font_standard [];

#define FONT_SML font_small, true, 6, 11
#define FONT_STD font_standard, true, 7, 13
